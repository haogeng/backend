## ## env文件说明
根目录有一个env文件，可以设置三个参数:

* EVN_NAME 环境的唯一标识(编译后二进制文件后缀)，可以为空，用于区分不同的环境，比如test, testflight
* ROOT_NAME 编译后二进制文件前缀，用于避免多个项目命名冲突
* APP_PATH 部署的路径(最好设置成跟线上一致)

> 说明：编译出来二进制文件名称格式形如 {ENV_NAME}\_{cmd下目录名}\_{ROOT_NAME}

## 测试
* 使用make local在根目录编译生成local_xx_$osarch

## 部署

* git clone 后端代码
* cd到根目录
* make deploy 编译二进制文件，并且可以将env里的参数作为make参数单独设置
* sh scripts/init.sh 生成supervisor文件和部署， 这个步骤可以自行配置supervisor文件
* 将make deploy后的文件拷贝到部署目录
* 手工配置supervisor文件或者拷贝init.sh生成的文件到supervisor目录
* supervisorctl 启动部署的服务

## 约定

1. 使用game.CheckConfData检查静态数据
2. 类似于英雄等级，建筑等级数据，一般需要封个函数，比如game.GetTalentLevelConfData。直接访问
3. 功能函数(game.XX)应该传入err *msg.ErrorInfo作为传入参数，调用者通过err.Id 判断是否成功
4. 功能函数里不应该调用data.MustUpdate持久化数据。持久化数据都是在事件函数(handler.XX)中执行
    
    > 类似于好友这种多玩家交互数据，采用了另一种思路，使用功能函数内即时存档的方式，不是每个消息里最终存档
    
5. 事件函数中一旦有数据错误，应该直接返回错误值，打断调用，data.MustUpdate函数在事件函数中应该只调用一次
6. consume 通过check & sub两步完成，最后通过final sync推送给客户端
7. 新增并提交proto协议文件的时候，最好同步提交客户端的GameXClient/Lua/Managers/ProtoManager.lua
8. 上线版日志只在真正需要的时候使用error和warning，这两个日志会打印一堆的调用堆栈
9. 玩家数据拆分为base, ext & safe. base是高频变化的数据，ext是低频变化的数据，safe是需求分布式线程安全的数据。
10. 逻辑里的取当前时间，统一使用time3.Now, 方便策划测试
11. 分表机制： 如PlayerInfoExt，需要通过MustGetPlayerInfoExt获得访问(获得后会缓存到PlayerInfo里的Invalide临时对象中)。一旦缓存，则在基础表MustUpdate里会自动级联Update。在一次事务处理中，如果曾缓存，则级联存档，否则不存档。可依据此机制灵活横向拆表。注意，横向拆表的表明不能有包含关系，比如Ext,Ext2。
12. 配置约定： base:app.toml, 根据环境变量GMX_CONFIG_POSTFIX，加载custom的，通过viper 合并。需要热更的配置，走hotconfig.json。 
13. 错误处理，通用的错误处理，如条件不足，货币不足，在内部逻辑通过internalErr传出。外部逻辑在defer里根据策划需求做二次加工，传给客户端。
14. 服务器框架升级checklist：
    1. 需要做本地测试，使用tclient或者unity，尽可能使用unity
    2. 测试流程：用户登录->testDebugAddObj，注意是否包含级联协议
15. health-check: http://addr/api/info/health
16. Clan, 单独一个模块，处理类似于Game, Clan单项依赖于Game
17. redis 区分cache和rank，启用两套redis。cache类的不需落地，而rank的需要做持久化。***需要告知运维***。
18. 新增.pb文件的时候，需要修改前端ProtoManager.lua文件，并svn ci
19. 需要交叉修改的玩家数据，统一放到PlayerShareXxx，针对交叉数据提供单独接口，不采用redis统一机制
20. 所有GetBySql 需要加limit限制
21. 上线代码里需要加ulimit检测，规避文件句柄数不够的低级错误
22. 关于gm: 核心把控是通过secretToken控制，配合skip-whiteip；Unity客户端不能把secretToken打到真机包里。然后editor模式下，skip统一为1，真机模式统一为0.
23. 所有mysql select操作需要加limit。
24. 如果本地(非dev3)migrations 出错了，删除本地versions目录下的所有py，但不删除versions目录。
25. 关于红点和推送，如果是给自己的红点，调用AddRedPointForMyself； 如果是向别人发送，则调用RTMNotifyRedPoint(不会存档)。
26. 关于进程安全，通过hotconfig bug协议过滤名单，可以暂时关闭某一个api服务（熔断）。
27. 前端错误处理约定：
    > error.proto:
    需要重登陆的code，判断错误值，不区分Msg.Errorinfo和Netutil.ErrorResponse    
    > * TOKEN_FAILED = 1022;//    token failed and need relogin  
    > * SERVER_PANIC = 1023;//    server internal panic and need relogin   
    > * 然后其他错误，不影响游戏