package gconst

import (
	"errors"
	"time"
)

var (
	ErrNoUser      = errors.New("not found user in db")
	ErrNoChapter   = errors.New("not found chapter in db")
	ErrGetLock     = errors.New("clan can not get lock")
	ErrNoUserShare = errors.New("not found user share in db")
)

const (
	// HardLimit 对于极端的极端做的处理
	// 程序中有一套规则，针对不同上线做的柔性处理
	// Hard & Soft 判定并存！
	// 最大英雄上限
	HardMaxLimitOfTroops = 1000

	// 最大item数
	HardMaxLimitOfItems = 1000

	// 最大装备数
	HardMaxLimitOfEquips = 1000

	//
	DailyResetToleranceSeconds = 10
	// 商店的刷新依赖于每日，，需要大于每日的tolerance
	StoreResetToleranceSeconds = 30

	DLockTtl        = time.Second * 3
	DLockGetTimeout = time.Second * 4

	// 最大邮件数
	MaxMailNums = 300
	// Gm可以发的邮件数
	MaxGmMailNums = 50

	ShareMaxMailNums = 100

	// 邮件过期时间
	MailDeadDuration = int64(3600 * 24 * 15)

	// 队伍最大英雄数
	MaxHeroNumInTeam = 6
)

const (
	DaySeconds = int64(24 * 3600)
)

// 活动类型
const (
	REGISTER    = "REGISTER"
	SERVER_OPEN = "SERVER_OPEN"
	FIX_TIME    = "FIX_TIME"
	WEEK_CYCLE  = "WEEK_CYCLE"
)

// MAIL ID
const (
	// 活动补发邮件id
	ActivityReimburseMail = int32(22)

	TroopDismissStar = int32(2)
)

func TroopCanDismiss(baseStar int32) bool {
	//dismissMaxStar read config if necessary
	return baseStar <= TroopDismissStar
}
