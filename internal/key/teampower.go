package key

import "fmt"

// 保存最近两
func GetTeamPowerKey(serverId int32) string {
	return fmt.Sprintf("gmx:rank:teampower:s{%d}", serverId)
}
