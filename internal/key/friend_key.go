package key

import "fmt"

// 推荐好友Key
func GetFriendRecommendKey(serverId int32) string {
	return fmt.Sprintf("gmx:friend:recommend:{%d}", serverId)
}
