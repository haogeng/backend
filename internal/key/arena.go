package key

import (
	"fmt"
	"server/internal/util/time3"
	"strconv"
	"time"
)

// gmx:rank:业务名...

// 竞技场默认Key
func GetArenaDefaultKey(serverId int32, seasonId uint64) string {
	return fmt.Sprintf("gmx:rank:arena:{%d}.{%d}", serverId, seasonId)
}

// 竞技场每日备份榜Key
func GetArenaDayKeyByMillis(serverId int32, seasonId uint64, unixTime int64) string {
	dayTime := time.Unix(unixTime, 0)
	return GetArenaDayKeyByTime(serverId, seasonId, dayTime)
}
func GetArenaDayKeyByTime(serverId int32, seasonId uint64, dayTime time.Time) string {
	formatDay := time3.TimeFormatYMD(dayTime)
	return fmt.Sprintf("gmx:rank:arena_dayback:{%d}.{%d}.{%s}", serverId, seasonId, formatDay)
}

// 竞技场赛季备份榜Key
func GetArenaSeasonKey(serverId int32, seasonId uint64, seasonNumber int32) string {
	formatSeason := strconv.FormatInt(int64(seasonNumber), 10)
	return fmt.Sprintf("gmx:rank:arena_seasonback:{%d}.{%d}.{%s}", serverId, seasonId, formatSeason)
}
