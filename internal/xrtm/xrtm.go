package xrtm

// RTM ERROR CODE
//https://github.com/highras/fpnn-sdk-go/blob/master/src/fpnn/errorCodes.go
//code: 200008, ex: invalid server time, please adjust it 服务器时间错1分钟就不行...
import (
	"bitbucket.org/funplus/golib/encoding2/protobuf"
	"bitbucket.org/funplus/sandwich"
	"bitbucket.org/funplus/sandwich/message"
	metadata2 "bitbucket.org/funplus/sandwich/metadata"
	_ "fmt"
	proto "github.com/gogo/protobuf/proto"
	"github.com/highras/rtm-server-sdk-go/src/rtm"
	"server/internal/log"
	"server/internal/util/time3"
	"server/pkg/gen/msg"
	"server/pkg/gen_redis/redisdb"
	"strconv"
)

const (
	// 缺省的管理员Id
	// 用于广播用
	KDefaultAdminId = int64(111)
)

var Conn *rtm.RTMServerClient
var config *RtmConfig

type RtmConfig struct {
	Pid               int32
	SecretKey         string
	EndPointBackgate  string
	EndPointFrontgate string
}

func GetCachedConfig() *RtmConfig {
	return config
}

func MustInit(c *RtmConfig) {
	if Conn != nil {
		//panic("Conn != nil")
		return
	}

	config = c

	log.Infof("RTM Server-End Go SDK Version: %v", rtm.SDKVersion)

	Conn = rtm.NewRTMServerClient(c.Pid, c.SecretKey, c.EndPointBackgate)

	setCallBack(Conn)
	// 业务发起的时候，会自动连接
	// Auto Reconnect 不是保活，只是说需要的时候连接？
	// 实际测试结果：关闭网络后，再打开，，能自动重连，很美好~
	Conn.SetAutoReconnect(true)
}

func setCallBack(client *rtm.RTMServerClient) {
	//client.SetOnConnectedCallback(func(connId uint64, endpoint string, connected bool, autoReconnect bool, connectState *rtm.RtmRegressiveState) {
	//	if connected {
	//		fmt.Printf("connect success connId:= %d, endpoint:= %s.\n", connId, endpoint)
	//	} else {
	//		info := fmt.Sprintf("RTM last connect time at %d, currentFailedCount = %d", connectState.ConnectStartMilliseconds, connectState.CurrentFailedCount)
	//		fmt.Printf("connect result connId:= %d, endpoint:= %s, connected:= %t, autoReconnect:= %t, reconnectinfo = %s.\n", connId, endpoint, connected, autoReconnect, info)
	//	}
	//})
	//
	//client.SetOnClosedCallback(func(connId uint64, endpoint string, autoReconnect bool, connectState *rtm.RtmRegressiveState) {
	//	if connectState != nil {
	//		info := fmt.Sprintf("RTMReconnect time at %d, currentFailedCount = %d", connectState.ConnectStartMilliseconds, connectState.CurrentFailedCount)
	//		fmt.Printf("connect close connId:= %d, endpoint:= %s, autoReconnect:= %t, reconnectinfo:= %s.\n", connId, endpoint, autoReconnect, info)
	//	}
	//})
}

func SendProto(targetUid uint64, protoMsg proto.Message) {
	// 如果对方不在线，则不推送
	var c redisdb.PlayerInfoCache
	r := c.Find(targetUid)
	if r == nil {
		return
	}

	// 30分钟之前登录的
	if r.LastLogoutTime+30*60 < time3.Now().Unix() {
		log.Debugf("rtm send proto failed cause target logout too long")
		return
	}

	seqNum := 0
	md := metadata2.Pairs("seq_num", strconv.Itoa(seqNum))
	rawPacket, err := sandwich.PacketWith(protobuf.Codec, message.SliceWith(protoMsg), md, int32(seqNum))
	if err != nil {
		log.Error(err)
		return
	}
	body, err := protobuf.Codec.Marshal(rawPacket)
	if err != nil {
		log.Error(err)
		return
	}

	_, err = Conn.SendMessage(KDefaultAdminId,
		int64(targetUid),
		int8(msg.RTMMsgType_RTMMT_Proto),
		string(body),
		func(mtime int64, errorCode int, errInfo string) {
			if errorCode != 0 {
				log.Errorf("rtm send message to %d err %d, %s", targetUid, errorCode, errInfo)
			} else {
				log.Debugf("rtm send message %+v", protoMsg)
			}
		})
	if err != nil {
		log.Error(err)
	}
}

func GetRTMInfo(uid uint64) (ret *msg.RTMInfo) {
	token, tokenErr := Conn.GetToken(int64(uid))
	ret = &msg.RTMInfo{
		Pid:               GetCachedConfig().Pid,
		Uid:               int64(uid),
		Token:             token,
		EndPointFrontgate: GetCachedConfig().EndPointFrontgate,
	}
	if tokenErr != nil {
		ret.TokenErr = tokenErr.Error()
	}
	log.Debugf("rtm info: %+v", ret)
	return
}
