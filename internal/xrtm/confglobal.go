package xrtm

// gmx default global test
var GmxGlobalTestConf = &RtmConfig{
	Pid:       80000057,
	SecretKey: "9cedf49b-913f-4722-a116-8a16f9eec5c3",
	// 注意前后端区别
	EndPointBackgate:  "rtm-intl-backgate.ilivedata.com:13315",
	EndPointFrontgate: "rtm-intl-frontgate.ilivedata.com:13325",
}
