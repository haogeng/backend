package xrtm

import (
	"fmt"
	"testing"
	"time"
)

func TestGetToken(t *testing.T) {
	MustInit(GmxGlobalTestConf)

	for i := 0; i < 10; i++ {
		t, err := Conn.GetToken(int64(1))
		fmt.Println(time.Now(), t, err)
	}

	for j := 0; j < 100; j++ {
		time.Sleep(time.Second * 5)

		for i := 0; i < 10; i++ {
			t, err := Conn.GetToken(int64(1))
			fmt.Println(time.Now(), t, err)
		}
	}

	time.Sleep(time.Hour)
}
