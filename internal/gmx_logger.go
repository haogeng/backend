package internal

import (
	"bitbucket.org/funplus/ark"
	"bitbucket.org/funplus/arkmid"
	"bitbucket.org/funplus/golib/httputil/accesslog"
	"bitbucket.org/funplus/golib/internal_export"
	"bitbucket.org/funplus/golib/ip"
	"bitbucket.org/funplus/golib/time2"
	"bitbucket.org/funplus/golib/zaplog"
	"bitbucket.org/funplus/sandwich/current"
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin/binding"

	//"github.com/gin-gonic/gin/binding"
	"github.com/google/uuid"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"server/internal/log"
	"server/pkg/fungin/xmiddleware"
	"server/pkg/gen_redis/redisdb"

	"strings"
	"sync/atomic"
	"time"
	//"strconv"

	//"strconv"

	//"go.uber.org/zap/zapcore"
	"os"
	"server/internal/util"
)

var (
	LogPath     = ""
	Prefix      = "gmx"
	Tag         = "gmx-server"
	HostName, _ = os.Hostname()
	UUID        = uuid.New().String()

	// never direct use these logger.
	zapLogger      *zap.Logger
	zapLoggerSugar *zap.SugaredLogger

	// 用于冗余的对access日志再记录一份，记录所有信息，而且json indent输出
	accessZapLogger      *zap.Logger
	accessZapLoggerSugar *zap.SugaredLogger
)

func mustInstallAccessZapLogger(development bool, logWithServerInfo bool) {
	var zapConfig zap.Config
	if development {
		zapConfig = zaplog.DefaultDevelopmentZapLoggerConfig
		// 简化时间显示
		zapConfig.EncoderConfig.EncodeTime = func(t time.Time, enc zapcore.PrimitiveArrayEncoder) {
			enc.AppendString(t.Format("15:04:05"))
		}
		zapConfig.EncoderConfig.EncodeLevel = zapcore.CapitalLevelEncoder

		//zapConfig.Encoding = "json"
		// Just only output to log file in debug mode.
		zapConfig.OutputPaths = []string{LogPath + Prefix + "-access" + ".log"}
	} else {
		zapConfig = zaplog.DefaultZapLoggerConfig
		zapConfig.EncoderConfig.EncodeLevel = zapcore.CapitalLevelEncoder
	}

	var err error
	accessZapLogger, err = serverLogger(&zapConfig, Tag, UUID, logWithServerInfo, false)
	util.PanicIfErr(err)
	accessZapLoggerSugar = accessZapLogger.Sugar()
}

func mustInstallZapLogger(development bool, logWithServerInfo bool) {
	var zapConfig zap.Config
	if development {
		zapConfig = zaplog.DefaultDevelopmentZapLoggerConfig
		// 简化时间显示 -- 开始跑起来后不够了
		//zapConfig.EncoderConfig.EncodeTime = func(t time.Time, enc zapcore.PrimitiveArrayEncoder) {
		//	enc.AppendString(t.Format("15:04:05"))
		//}
		zapConfig.EncoderConfig.EncodeLevel = zapcore.CapitalLevelEncoder

		//zapConfig.Encoding = "json"
		// Just only output to log file in debug mode.
		zapConfig.OutputPaths = append(zapConfig.OutputPaths, LogPath+Tag+".log")
	} else {
		zapConfig = zaplog.DefaultZapLoggerConfig
		zapConfig.EncoderConfig.EncodeLevel = zapcore.CapitalLevelEncoder
	}

	//YY 可以将配置输出到文件，或kafka队列，后续根据需求再完善
	// 单点转储：//zapConfig.OutputPaths = append(zapConfig.OutputPaths, "/tmp/gmx-server.log")
	// 单点系统：单个文件的切片，可以通过logrotate等其他工具完成 (组合组合！)
	// 分布式系统：logagent(zap) -> queue(kafka) -> store&search(elasticsearch) -> show(kibala)

	var err error
	zapLogger, err = serverLogger(&zapConfig, Tag, UUID, logWithServerInfo, true)
	util.PanicIfErr(err)
	zapLoggerSugar = zapLogger.Sugar()
}

// 目前约定，线上版本必须使用zapLogger
func MustInstallLogger(development bool, logWithServerInfo bool) {
	LogPath = Config.LogPath
	err := os.MkdirAll(LogPath, 0700)

	if err != nil {
		log.Errorf("Create log dir [%s] err: %v", LogPath, err)
		panic(err)
	}
	// add /
	if len(LogPath) > 0 && LogPath[len(LogPath)-1] != '/' {
		LogPath = LogPath + "/"
	}

	mustInstallZapLogger(development, logWithServerInfo)
	mustInstallAccessZapLogger(development, logWithServerInfo)

	if development {
		//defaultLogger := &log.DefaultLogger{Logger: log2.New(os.Stderr, "----[gmx] ", log2.Ltime|log2.Lshortfile)}
		//log.SetGmxLogger(defaultLogger, zapLoggerSugar)
		log.SetGmxLogger(zapLoggerSugar, zapLoggerSugar)
	} else {
		log.SetGmxLogger(zapLoggerSugar, zapLoggerSugar)
	}
}

func serverLogger(config *zap.Config, tag string, uuid string, logWithServerInfo bool, setLib bool) (logger *zap.Logger, err error) {
	if logger, err = zaplog.NewLogger(config); err != nil {
		return
	}
	if logWithServerInfo {
		logger = logger.Named(tag).With(
			zap.String("host_name", HostName),
			zap.String("server_tag", tag),
			zap.String("server_id", uuid),
			zap.String("server_ip", ip.GetLocalIP()),
			zap.Int64("server_birth", time2.Unix()),
		)
	}

	// must copy one to add caller skip
	if setLib {
		internal_export.SetLibLogger(zaplog.NewLibLoggerZap(logger.WithOptions(zap.AddCallerSkip(1))))

		//ark.WithLogger(log.FileLogger)
	}

	// 不直接使用，需要加一层call depth
	logger = logger.WithOptions(zap.AddCallerSkip(1))
	return
}

// 单机环境下的日志索引
var accessLogId int64

func genAccessLogId() int64 {
	return atomic.AddInt64(&accessLogId, 1)
}

func ArkAccessLog(skipper arkmid.Skipper) ark.MiddlewareFunc {
	return func(ctx *ark.Context, next ark.HandlerFunc) error {
		if skipper != nil && skipper(ctx) {
			return next(ctx)
		}
		apiCall := &accesslog.Call{}
		recoder := accesslog.NewResponseRecorder(ctx.Response().Writer, ctx.GetHeader("Content-Type") != ark.MIMEProtobuf)
		ctx.Response().Writer = recoder
		accesslog.Before(apiCall, ctx.Request())
		err := next(ctx)
		accesslog.After(apiCall, recoder, ctx.Request())
		//accessZapLogger.Debug("access-http", zap.Object("api", apiCall))
		diyAccessLog(ctx, apiCall, err)
		return err
	}
}

func diyAccessLog(ctx *ark.Context, api *accesslog.Call, err error) {
	errStr := ""
	if err != nil {
		errStr = fmt.Sprintf(" with error: [%v]", err)
	}
	if Config.Development {
		logId := genAccessLogId()
		apiObj := zap.Object("api_call", api)
		if ctx.GetHeader("Content-Type") == binding.MIMEPROTOBUF {
			//logger.Debug("access_dev", append(current.MustGetLogFields(ctx), apiObj)...)

			//key := time3.Now().Format(time.RFC3339) + "\t"
			//c := current.MustFromContext(ctx)
			a := current.MustGetIncomingMetadata(ctx)
			b := current.MustGetIncomingMessages(ctx)
			c := current.MustGetOutgoingMetadata(ctx)
			d := current.MustGetOutgoingMessages(ctx)
			mapForJson := make(map[string]interface{})
			mapForJson["InComingMeta"] = a
			mapForJson["InComingMsg"] = b
			mapForJson["OutgoingMeta"] = c
			mapForJson["OutgoingMsg"] = d

			uid, _ := xmiddleware.GetUid(ctx)
			deviceName := ""
			{
				var c redisdb.PlayerInfoCache
				r := c.Find(uid)
				if r != nil {
					deviceName = r.DeviceNameDup
				}
			}

			ip := ctx.ClientIP()

			// 区别打印
			// add call depth.
			b1, _ := json.Marshal(mapForJson["InComingMsg"])
			b2, _ := json.Marshal(mapForJson["OutgoingMsg"])
			s1 := string(b1[:util.MinInt(1024, len(b1))])
			s2 := string(b2[:util.MinInt(4096, len(b2))])
			reqUri := ""
			if b.Len() > 0 {
				reqUri = strings.Replace(b[0].Uri, "msg.", "", -1)
			}

			seqId := current.MustGetIncomingSequenceID(ctx)
			reqUri += fmt.Sprintf(" device:[%s] seqId:[%d]", deviceName, seqId)
			s1 = strings.Replace(s1, "Uri", "\t----Uri", -1)
			s2 = strings.Replace(s2, "Uri", "\t----Uri", -1)

			// send length
			sendLength := ctx.Response().Size
			encoding := ctx.Response().Header().Get("Content-Encoding")

			log.Debugf("access_dev(%d) [[%s]]\tu%d \tIn:%v\t Out:%v %s\t UncomDataSize:%d\t Encode:%s\t \n",
				logId, reqUri, uid, s1, s2, errStr, sendLength, encoding)

			if accessZapLoggerSugar != nil {
				j1, _ := json.MarshalIndent(api, "", "    ")
				j2, _ := json.MarshalIndent(mapForJson, "", "    ")
				accessZapLoggerSugar.Debugf("access_dev(%d) User: %d  (Ip: \"%s\"):\n%s\n%s %s\n\n",
					logId, uid, ip, j1, j2, errStr)
			}
		} else {
			log.Debugf("access_dev %v %s", apiObj, errStr)
			if accessZapLogger != nil {
				accessZapLogger.Debug("access-http", zap.Object("api", api))
			}
		}
	} else {
		// 优先考虑访问日志
		destLogger := accessZapLogger
		if destLogger == nil {
			destLogger = zapLogger
		}

		fields := []zapcore.Field{}
		if ctx.GetHeader("Content-Type") == binding.MIMEPROTOBUF {
			fields = append(fields, current.MustGetLogFields(ctx)...)
			apiObj := zap.Object("api_call", api)
			fields = append(fields, apiObj)
			if len(errStr) > 0 {
				fields = append(fields, zap.String("error", errStr))
			}
			destLogger.Debug("access", fields...)
		} else {
			destLogger.Debug("access-http", zap.Object("api", api))
		}
	}
}
