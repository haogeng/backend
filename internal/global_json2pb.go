package internal

import (
	gocache "github.com/patrickmn/go-cache"
	"server/internal/log"
)

// 应用层面的6元组优化
// 两个cache，分别用于json2staticObj数组和map
// 当数据reload的时候，手动清空cache
var Json2pbAryCache = gocache.New(gocache.NoExpiration, -1)
var Json2pbMapCache = gocache.New(gocache.NoExpiration, -1)

//TODO 需要注册到热加载资源的通知函数中
// 热更资源，尚未测试和支持
// 比较鲁莽，但凡有配置变化时，该函数都会调用，，虽然冗余，但不犯错！
func HotloadCallback_ClearJson2PbCache(names []string) {
	log.Debug("Call HotloadCallback_ClearJson2PbCache")

	Json2pbAryCache.Flush()
	Json2pbMapCache.Flush()

	//c, _ := conf.GetObjList(1)
	//fmt.Printf("obj list c %v\n", c.ObjList)
}
