package internal

import (
	"bitbucket.org/funplus/golib/fun/pmloader3"
	"server/internal/game/activity"
	"server/internal/log"
	"server/pkg/gen/conf"
	"sync"
)

// 用于蹩脚的清缓存,,额外需要做的事
// 更复杂的应该使用broker模式订阅和广播
type pmConf struct {
	transactionEndExChainsMutex sync.RWMutex
	transactionEndExChains      []func(names []string)

	// 特化任务相关
	activeTaskTypeParam2IdMapMutex sync.RWMutex
	activeTaskTypeParam2IdMap      map[int64][]int32

	completeTaskTypeParam2IdMapMutex sync.RWMutex
	completeTaskTypeParam2IdMap      map[int64][]int32

	// 英雄id 反查 羁绊id
	troopId2RelationshipIdMapMutex sync.RWMutex
	troopId2RelationshipIdMap      map[int32]int32
}

var PmConf pmConf = pmConf{}

func (p *pmConf) Reload() {
	p.reloadTask()
	p.reloadTroopRelationship()

	activity.GetMgr().ReloadOnConfChange()
}

// 反查id 激活
func (p *pmConf) GetTaskIdsByActiveCond(taskCond int32, taskParam int32) []int32 {
	if p.activeTaskTypeParam2IdMap == nil {
		panic("activeTaskTypeParam2IdMap should not be nil")
	}

	p.activeTaskTypeParam2IdMapMutex.RLock()
	a, _ := p.activeTaskTypeParam2IdMap[taskTypeParamKey(taskCond, taskParam)]
	p.activeTaskTypeParam2IdMapMutex.RUnlock()
	return a
}

// 反查id 完成类
func (p *pmConf) GetTaskIdsByCompleteCond(taskCond int32, taskParam int32) []int32 {
	if p.completeTaskTypeParam2IdMap == nil {
		panic("completeTaskTypeParam2IdMap should not be nil")
	}

	p.completeTaskTypeParam2IdMapMutex.RLock()
	a, _ := p.completeTaskTypeParam2IdMap[taskTypeParamKey(taskCond, taskParam)]
	p.completeTaskTypeParam2IdMapMutex.RUnlock()
	return a
}

func taskTypeParamKey(taskCond int32, taskParam int32) int64 {
	return (int64(taskCond) << 32) | (int64(taskParam))
}

func (p *pmConf) reloadTask() {
	// 生成反查表
	{
		p.activeTaskTypeParam2IdMapMutex.Lock()

		p.activeTaskTypeParam2IdMap = make(map[int64][]int32)
		var allTasks = conf.GetAllTaskConfigConf()
		for _, v := range allTasks.AllTaskConfigs {
			typeKey := taskTypeParamKey(v.ActiveCondType, v.ActiveCondParam)
			p.activeTaskTypeParam2IdMap[typeKey] = append(p.activeTaskTypeParam2IdMap[typeKey], v.Id)

		}

		p.activeTaskTypeParam2IdMapMutex.Unlock()
	}

	{
		p.completeTaskTypeParam2IdMapMutex.Lock()

		p.completeTaskTypeParam2IdMap = make(map[int64][]int32)
		var allTasks = conf.GetAllTaskConfigConf()
		for _, v := range allTasks.AllTaskConfigs {
			typeKey := taskTypeParamKey(v.CompleteCondType, v.CompleteCondParam)
			p.completeTaskTypeParam2IdMap[typeKey] = append(p.completeTaskTypeParam2IdMap[typeKey], v.Id)

		}

		p.completeTaskTypeParam2IdMapMutex.Unlock()
	}

	log.Debugf("active task table: %+v", p.activeTaskTypeParam2IdMap)
	log.Debugf("complete task table: %+v", p.completeTaskTypeParam2IdMap)
}

// config id
func (p *pmConf) GetRelationshipIdByTroopConfigId(troopConfigId int32) int32 {
	if p.troopId2RelationshipIdMap == nil {
		panic("troopId2RelationshipIdMap should not be nil")
	}

	p.troopId2RelationshipIdMapMutex.RLock()
	a, ok := p.troopId2RelationshipIdMap[troopConfigId]
	if !ok {
		log.Errorf("not found relationship by %d", troopConfigId)
	}
	p.troopId2RelationshipIdMapMutex.RUnlock()
	return a
}

func (p *pmConf) reloadTroopRelationship() {
	p.troopId2RelationshipIdMapMutex.Lock()

	p.troopId2RelationshipIdMap = make(map[int32]int32)
	var all = conf.GetTroopRelationshipConfigConf()
	for _, v := range all.TroopRelationshipConfigs {
		for _, troopId := range v.NeedHero {
			if _, ok := p.troopId2RelationshipIdMap[troopId]; ok {
				log.Errorf("Found repeated troop id(%d) in TroopRelationshipConfig(%d)", troopId, v.Id)
			}

			p.troopId2RelationshipIdMap[troopId] = v.Id
		}
	}

	p.troopId2RelationshipIdMapMutex.Unlock()
	log.Debugf("troop relationship table: %+v", p.troopId2RelationshipIdMap)
}

func (p *pmConf) AddTransactionEndExChains(f func(names []string)) {
	p.transactionEndExChainsMutex.Lock()
	p.transactionEndExChains = append(p.transactionEndExChains, f)
	p.transactionEndExChainsMutex.Unlock()
}

func (p *pmConf) GetTransactionEndExChains() []func(names []string) {
	p.transactionEndExChainsMutex.RLock()
	ret := p.transactionEndExChains
	p.transactionEndExChainsMutex.RUnlock()
	return ret
}

// 覆盖transend callback
func myTransactionEnd(names []string) {
	log.Debugf("Call transactionEnd with args:%v", names)
	conf.TransactionEnd(names)

	exChains := PmConf.GetTransactionEndExChains()
	for _, v := range exChains {
		v(names)
	}

	PmConf.Reload()
}

//YY 备注： liveLoading 只检测create和write, 并且如果整个文件夹都删除的化，watch将直接失效
func MustInitPmConf() {
	// configs
	conf.LogWarn = func(v ...interface{}) {
		log.Debug(v...)
	}
	conf.LogError = func(v ...interface{}) {
		log.Error(v...)
	}

	PmConf.AddTransactionEndExChains(HotloadCallback_ClearJson2PbCache)

	if Config.EtcdConf {
		pmloader3.MustInitEtcd(Config.EtcdEndpoint, Config.PmConfPath, true, conf.GenerateLoaders(), conf.TransactionStart, myTransactionEnd)
	} else {
		pmloader3.MustInitLocal(Config.PmConfPath, true, conf.GenerateLoaders(), conf.TransactionStart, myTransactionEnd)
	}
}
