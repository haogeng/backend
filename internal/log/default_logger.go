package log

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
)

var (
	defaultLogger = &DefaultLogger{Logger: log.New(os.Stderr, "----[gmx] ", log.Ltime|log.Lshortfile)}
	discardLogger = &DefaultLogger{Logger: log.New(ioutil.Discard, "", 0)}
)

func init() {
	defaultLogger.EnableDebug()
	discardLogger.Debug()
}

const (
	callDepth = 3
)

// DefaultLogger is a default implementation of the logger interface.
type DefaultLogger struct {
	*log.Logger
	debug bool
}

func (l *DefaultLogger) EnableTimestamps() { l.SetFlags(l.Flags() | log.Ldate | log.Ltime) }
func (l *DefaultLogger) EnableDebug()      { l.debug = true }
func prefix(lvl, msg string) string        { return fmt.Sprintf("%s: %s", lvl, msg) }

func (l *DefaultLogger) Debug(v ...interface{}) {
	if l.debug {
		_ = l.Output(callDepth, prefix("DEBUG", fmt.Sprint(v...)))
	}
}

func (l *DefaultLogger) Debugf(format string, v ...interface{}) {
	if l.debug {
		_ = l.Output(callDepth, prefix("DEBUG", fmt.Sprintf(format, v...)))
	}
}

func (l *DefaultLogger) Info(v ...interface{}) {
	_ = l.Output(callDepth, prefix("INFO", fmt.Sprint(v...)))
}

func (l *DefaultLogger) Infof(format string, v ...interface{}) {
	_ = l.Output(callDepth, prefix("INFO", fmt.Sprintf(format, v...)))
}

func (l *DefaultLogger) Error(v ...interface{}) {
	_ = l.Output(callDepth, prefix("ERROR", fmt.Sprint(v...)))
}

func (l *DefaultLogger) Errorf(format string, v ...interface{}) {
	_ = l.Output(callDepth, prefix("ERROR", fmt.Sprintf(format, v...)))
}

func (l *DefaultLogger) Warn(v ...interface{}) {
	_ = l.Output(callDepth, prefix("WARN", fmt.Sprint(v...)))
}

func (l *DefaultLogger) Warnf(format string, v ...interface{}) {
	_ = l.Output(callDepth, prefix("WARN", fmt.Sprintf(format, v...)))
}

func (l *DefaultLogger) Fatal(v ...interface{}) {
	_ = l.Output(callDepth, prefix("FATAL", fmt.Sprint(v...)))
	os.Exit(1)
}

func (l *DefaultLogger) Fatalf(format string, v ...interface{}) {
	_ = l.Output(callDepth, prefix("FATAL", fmt.Sprintf(format, v...)))
	os.Exit(1)
}

func (l *DefaultLogger) Panic(v ...interface{}) {
	l.Logger.Panic(v...)
}

func (l *DefaultLogger) Panicf(format string, v ...interface{}) {
	l.Logger.Panicf(format, v...)
}

func (l *DefaultLogger) Sync() error {
	// do nth.
	return nil
}
