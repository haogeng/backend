package log

type Logger interface {
	Debug(v ...interface{})
	Debugf(format string, v ...interface{})

	Error(v ...interface{})
	Errorf(format string, v ...interface{})

	Info(v ...interface{})
	Infof(format string, v ...interface{})

	Warn(v ...interface{})
	Warnf(format string, v ...interface{})

	Fatal(v ...interface{})
	Fatalf(format string, v ...interface{})

	Panic(v ...interface{})
	Panicf(format string, v ...interface{})

	Sync() error
}

var (
	//
	ConsoleLogger = Logger(defaultLogger)

	FileLogger = Logger(defaultLogger)

	// 可以热开关
	EnableDebug = true
)

// 开发阶段，console简要已读信息，file复杂信息, console != file
// 线上版本，console和file是一个东西
func SetGmxLogger(console Logger, file Logger) {
	ConsoleLogger = console
	FileLogger = file
}

func Debug(v ...interface{}) {
	if EnableDebug {
		ConsoleLogger.Debug(v...)
		if FileLogger != nil && FileLogger != ConsoleLogger {
			FileLogger.Debug(v...)
		}
	}
}

func Debugf(format string, v ...interface{}) {
	if EnableDebug {
		ConsoleLogger.Debugf(format, v...)
		if FileLogger != nil && FileLogger != ConsoleLogger {
			FileLogger.Debugf(format, v...)
		}
	}
}

func Info(v ...interface{}) {
	ConsoleLogger.Info(v...)
	if FileLogger != nil && FileLogger != ConsoleLogger {
		FileLogger.Info(v...)
	}
}

func Infof(format string, v ...interface{}) {
	ConsoleLogger.Infof(format, v...)
	if FileLogger != nil && FileLogger != ConsoleLogger {
		FileLogger.Infof(format, v...)
	}
}

func Error(v ...interface{}) {
	ConsoleLogger.Error(v...)
	if FileLogger != nil && FileLogger != ConsoleLogger {
		FileLogger.Error(v...)
	}
}

func Errorf(format string, v ...interface{}) {
	ConsoleLogger.Errorf(format, v...)
	if FileLogger != nil && FileLogger != ConsoleLogger {
		FileLogger.Errorf(format, v...)
	}
}

func Warn(v ...interface{}) {
	ConsoleLogger.Warn(v...)
	if FileLogger != nil && FileLogger != ConsoleLogger {
		FileLogger.Warn(v...)
	}
}

func Warnf(format string, v ...interface{}) {
	ConsoleLogger.Warnf(format, v...)
	if FileLogger != nil && FileLogger != ConsoleLogger {
		FileLogger.Warnf(format, v...)
	}
}

func Fatal(v ...interface{}) {
	ConsoleLogger.Fatal(v...)
	if FileLogger != nil && FileLogger != ConsoleLogger {
		FileLogger.Fatal(v...)
	}
}

func Fatalf(format string, v ...interface{}) {
	ConsoleLogger.Fatalf(format, v...)
	if FileLogger != nil && FileLogger != ConsoleLogger {
		FileLogger.Fatalf(format, v...)
	}
}

func Panic(v ...interface{}) {
	ConsoleLogger.Panic(v...)
	if FileLogger != nil && FileLogger != ConsoleLogger {
		FileLogger.Panic(v...)
	}
}

func Panicf(format string, v ...interface{}) {
	ConsoleLogger.Panicf(format, v...)
	if FileLogger != nil && FileLogger != ConsoleLogger {
		FileLogger.Panicf(format, v...)
	}
}

func Sync() error {
	ConsoleLogger.Sync()
	if FileLogger != nil && FileLogger != ConsoleLogger {
		return FileLogger.Sync()
	}
	return nil
}

type LogWriter struct{}

// 针对ark提供的一个wrapper接口，callstack不对，，但影响不大
func (l *LogWriter) Write(p []byte) (n int, err error) {
	Infof("%s", p)
	return len(p), nil
}
