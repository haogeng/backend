package log

type WrapperLogger struct {
}

func (l *WrapperLogger) Debug(v ...interface{}) {
	Debug(v)
}

func (l *WrapperLogger) Debugf(format string, v ...interface{}) {
	Debugf(format, v)
}

func (l *WrapperLogger) Info(v ...interface{}) {
	Info(v)
}

func (l *WrapperLogger) Infof(format string, v ...interface{}) {
	Infof(format, v)
}

func (l *WrapperLogger) Error(v ...interface{}) {
	Error(v)
}

func (l *WrapperLogger) Errorf(format string, v ...interface{}) {
	Errorf(format, v)
}

func (l *WrapperLogger) Warn(v ...interface{}) {
	Warn(v)
}

func (l *WrapperLogger) Warnf(format string, v ...interface{}) {
	Warnf(format, v)
}

func (l *WrapperLogger) Fatal(v ...interface{}) {
	Fatal(v)
}

func (l *WrapperLogger) Fatalf(format string, v ...interface{}) {
	Fatalf(format, v)
}

func (l *WrapperLogger) Panic(v ...interface{}) {
	Panic(v...)
}
func (l *WrapperLogger) Panicf(format string, v ...interface{}) {
	Panicf(format, v...)
}

func (l *WrapperLogger) Sync() {
	Sync()
}

func (l *WrapperLogger) Printf(format string, v ...interface{}) {
	Debugf(format, v)
}
