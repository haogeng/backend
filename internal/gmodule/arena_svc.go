package gmodule

import (
	"server/internal/game"
	"server/internal/log"
	"server/pkg/dsingletonsvc"
)

type ArenaSvc struct {
	// 开始运行服务
	// 通过chan关闭服
	// closeChan 调用者通知svc 该关闭了，比如程序退出
	// svcDoneChan 当svc自身情况，比如panic了，需要通知mgr自身状态变化了
}

func (a *ArenaSvc) Run(closeChan chan struct{}) (svcDoneChan chan struct{}) {
	svcDoneChan = make(chan struct{})
	go func(closeChan chan struct{}, svcDoneChan chan struct{}) {
		defer func() {
			if r := recover(); r != nil {
				log.Info("catch panic in ArenaSvc:defer\n")
			}
			close(svcDoneChan)
		}()

		// do your logic
		a.run(closeChan)
	}(closeChan, svcDoneChan)

	return
}

func (a *ArenaSvc) run(closeChan chan struct{}) {
	log.Infof("start run ArenaSvc")
	game.StartArenaSeasonInfo()
	log.Infof("end run ArenaSvc")
	// 没啥用，此处其实忽略了
	// 不受外界控制
	//select {
	//case <-closeChan:
	//	fmt.Printf("close arenasvc by closeChan\n")
	//	return
	//}
}

type ArenaSvcProvider struct {
}

func (a *ArenaSvcProvider) NewSvc() dsingletonsvc.Svc {
	return &ArenaSvc{}
}
