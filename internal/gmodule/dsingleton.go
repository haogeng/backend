package gmodule

import (
	"bitbucket.org/funplus/golib/module"
	"server/internal"
	"server/internal/log"
	"server/pkg/cache"
	"server/pkg/dlock"
	"server/pkg/dsingletonsvc"
)

func GetSingletonSvc() module.Module {
	if dsingletonsvc.Mgr != nil {
		return dsingletonsvc.Mgr
	}

	svcData := initSvcData()

	dsingletonsvc.Init(&dlock.DLockWrapper{}, cache.Redis, svcData, log.Infof, internal.UUID)
	return dsingletonsvc.Mgr
}

func initSvcData() map[string]dsingletonsvc.SvcProvider {
	// test..
	svcData := map[string]dsingletonsvc.SvcProvider{}

	//svcData["examsvc"] = &dsingletonsvc.ExamSvcProvider{}
	svcData["arenasvc"] = &ArenaSvcProvider{}
	svcData["schedulesvc"] = &ScheduleSvcProvider{}
	return svcData
}
