package gmodule

import (
	"server/internal/game"
	"server/internal/log"
	"server/internal/util/time3"
	"server/pkg/dsingletonsvc"
	"server/pkg/gen/msg"
	"server/pkg/gen_redis/redisdb"
	"time"
)

type ScheduleSvc struct {
	// 开始运行服务
	// 通过chan关闭服
	// closeChan 调用者通知svc 该关闭了，比如程序退出
	// svcDoneChan 当svc自身情况，比如panic了，需要通知mgr自身状态变化了
}

func (a *ScheduleSvc) Run(closeChan chan struct{}) (svcDoneChan chan struct{}) {
	svcDoneChan = make(chan struct{})
	go func(closeChan chan struct{}, svcDoneChan chan struct{}) {
		defer func() {
			if r := recover(); r != nil {
				log.Info("catch panic in ScheduleSvc:defer\n")
			}
			close(svcDoneChan)
		}()

		// do your logic
		a.run(closeChan)
	}(closeChan, svcDoneChan)

	return
}

func (a *ScheduleSvc) run(closeChan chan struct{}) {
	log.Infof("start run ScheduleSvc")
	// 每秒定时扫描
	tick := time.NewTicker(time.Second)
	defer tick.Stop()
loopFor:
	for {
		// 是否关闭
		select {
		case <-closeChan:
			break loopFor
		default:
			break
		}

		// do sth
		a.doTick()

		// 等待tick
		select {
		case <-closeChan:
			break loopFor
		case <-tick.C:
			break
		}
	}

	log.Infof("end run ScheduleSvc")
	// 没啥用，此处其实忽略了
	// 不受外界控制
	//select {
	//case <-closeChan:
	//	fmt.Printf("close ScheduleSvc by closeChan\n")
	//	return
	//}
}

type ScheduleSvcProvider struct {
}

func (a *ScheduleSvcProvider) NewSvc() dsingletonsvc.Svc {
	return &ScheduleSvc{}
}

func (a *ScheduleSvc) doTick() {
	var c redisdb.ScheduleInfoCache
	//迷宫id:1
	r := c.Find(game.MazeRefreshConfId)
	if r == nil {
		log.Debugf("doTick error %+v", game.MazeRefreshConfId)
		r := &msg.ScheduleInfo{
			Id: game.MazeRefreshConfId,
		}
		r.OrderNum, r.EndTime, r.StartTime = game.GetOrderNum()
		c.MustCreate(game.MazeRefreshConfId, r)
		return
	}
	now := time3.Now().Unix()

	if now >= r.EndTime {
		r.OrderNum, r.EndTime, r.StartTime = game.GetOrderNum()
		c.MustUpdate(game.MazeRefreshConfId, r)
	}
}
