package gmodule

import (
	"bitbucket.org/funplus/ark"
	"context"
	"fmt"
	"net/http"
	"server/internal"
	"server/internal/handlers"
	"server/internal/log"
	"time"
)

type Http struct {
	Ark *ark.Ark
}

type nilWriter struct {
}

func (nw *nilWriter) Write(p []byte) (n int, err error) {
	// do nth
	return len(p), nil
}

func (s *Http) OnInit() {
	isArkDebug := internal.Config.Development
	s.Ark = ark.New(ark.WithDebug(isArkDebug),
		ark.WithDefaultLogWriter(&nilWriter{}))
	//ark.WithDefaultLogWriter(&log.LogWriter{}))

	handlers.InitHttpRouter(s.Ark)
}

func (s *Http) OnClose() {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*60)
	defer cancel()
	err := s.Ark.Shutdown(ctx)
	log.Infof("Ark shutdown with err:%v", err)
}

func (s *Http) Run(closeChan chan struct{}) {
	//Ark
	go func() {
		addr := fmt.Sprintf(":%d", internal.Config.HttpPort)

		ss := &http.Server{
			Addr:           addr,
			ReadTimeout:    10 * time.Second,
			WriteTimeout:   10 * time.Second,
			MaxHeaderBytes: 1 << 20,
		}
		// 客户端体验有一些提升 对服务器来说会增大一些压力，毕竟要维持更多的连接
		//ss.SetKeepAlivesEnabled(false)
		ss.SetKeepAlivesEnabled(true)
		ss.IdleTimeout = time.Second * 60 * 5

		_ = s.Ark.StartServer(ss)
	}()

	<-closeChan
}

func (s *Http) Name() string {
	return "module-http-server"
}
