package clan

import (
	"server/pkg/gen/msg"
	"server/pkg/gen_redis/redisdb"
)

// 做一些时序无关的逻辑的初始化工作
func init() {
	//log.Debug("game::init()")

	// 设置最后一次的近似在线时间
	redisdb.SetOnBeforeClanInfoMustUpdate(func(_data *msg.ClanInfo) {
		// ORDER
		//_data.LastLogoutTime = time.Now().Unix()

		MustUpdateClanBrief(_data)
	})
}
