package clan

import (
	"fmt"
	"github.com/go-redis/redis/v8"
	"math/rand"
	"server/internal/global_info"
	"server/internal/log"
	"server/internal/share"
	"server/internal/util/time3"
	"server/internal/util/xsensitive"
	"server/pkg/cache"
	"server/pkg/dbutil"
	"server/pkg/gen/conf"
	"server/pkg/gen/msg"
	"server/pkg/gen_db/dao"
	"server/pkg/gen_redis/redisdb"
	"server/pkg/persistredis"
	"server/pkg/xsnowflake"
	"sort"
	"strconv"
	"unicode/utf8"
)

const (
	clanBaseId      = 100000
	ClanCache       = "clan_info_cache"
	HourSeconds     = 3600
	days            = 7
	DaySeconds      = 24 * HourSeconds
	ClanBossFixTime = 8
)

func ValidPlayerClan(r *msg.PlayerInfo) {
	var flag bool
	if r.Clan == nil {
		r.Clan = MustGetPlayerInfoClan(r)
		flag = true
	}
	var dirty bool
	rs := share.GetClanId(r.Id)
	if rs != nil {
		if rs.ClanId > 0 {
			//验证数据完整性
			clanInfo := Find(rs.ClanId)
			if clanInfo == nil {
				ok := share.SetClanId(r.Id, clanInfo.Id, 0)
				if !ok {
					log.Errorf("clan id is may be 0")
					return
				}
			}
			if len(r.Clan.ApplyList) > 0 {
				//delete(r.Clan.ApplyList, r.ClanId)
				r.Clan.ApplyList = nil
				dirty = true
			}
			dirty = RemoveApplyList(clanInfo, r)
		} else {
			now := time3.Now().Unix()

			for k, v := range r.Clan.ApplyList {
				if v < now {
					delete(r.Clan.ApplyList, k)
					dirty = true
				}
				clanInfo := Find(k)
				dirty = RemoveApplyList(clanInfo, r)
			}
		}
	}

	if dirty {
		var c redisdb.PlayerInfoClanCache
		c.MustUpdate(r.Id, r.Clan)
	}

	if flag {
		r.Clan = nil
	}
}

// 刷新公会相关信息
func ResetClanInfo(pClanInfo *msg.PlayerInfoClan) {
	pClanInfo.DonateTimes = 0
	pClanInfo.MailSendCount = 0
	//pClanInfo.BossAttackTimes = 0 //读表+vip
	// TODO

}

func IncrClanId() (clanId int32, err error) {
	lastId, err := global_info.GetGlobalInfo().IncrementClanCount()
	if err != nil {
		return
	}
	clanId = clanBaseId + lastId
	return
}

func GetClanLockKey(id uint64) (key string) {
	key = cache.GetLockKeyFor(fmt.Sprintf("clan_lock:{%d}", id))
	return
}

func GetClanInfoLockKey(id uint64) (key string) {
	key = cache.GetLockKeyFor(redisdb.LockKeyPostfixOfClanInfo(id))
	return
}

func GetClanMemberLockKey(id uint64) (key string) {
	key = cache.GetLockKeyFor(redisdb.LockKeyPostfixOfClanInfoMember(id))
	return
}

func GetClanExtLockKey(id uint64) (key string) {
	key = cache.GetLockKeyFor(redisdb.LockKeyPostfixOfClanInfoExt(id))
	return
}

func GetOneClanId() (id uint64) {
	mgr := dao.ClanInfo
	tableName := mgr.TableName()
	//TODO...

	globalConf, ok := conf.GetGlobalConfig(1)
	if !ok {
		panic("globalConf is nil")
	}

	now := time3.Now().Unix()
	unionActiveTime := now - int64(globalConf.UnionLastRequestInDays*DaySeconds)
	sqlStr := fmt.Sprintf("select id from %s where last_active_time >=%d and apply_type=0 and member_count > 0 and member_count < %d limit 1", tableName, unionActiveTime, globalConf.UnionMemberLimit)
	tmp := &msg.ClanInfo{}
	err := mgr.GetBySql(tmp, sqlStr, nil)
	if err == nil {
		id = tmp.Id
	}
	return id
}

func SearchClanInfo(escapedContent string, err *msg.ErrorInfo) (clanInfos []*msg.ClanInfo) {
	clan := GetClanInfoByName(escapedContent)
	if clan != nil {
		clanInfos = append(clanInfos, clan)
	}
	clanIncreId, err1 := strconv.Atoi(escapedContent)
	if err1 != nil { //允许出错...
		//err.Id, err.DebugMsg = msg.Ecode_INVALID, "SearchClanInfo id error"
	}
	clan2 := GetClanInfoByIncreId(uint32(clanIncreId))
	if clan2 != nil {
		clanInfos = append(clanInfos, clan2)
	}
	return
}

func GetClanInfoByName(escapedContent string) (clanInfo *msg.ClanInfo) {
	mgr := dao.ClanInfo
	tableName := mgr.TableName()

	sqlStr := fmt.Sprintf("select id from %s where name='%s' and member_count > 0  limit 1",
		tableName,
		escapedContent)
	tmp := &msg.ClanInfo{}
	err := mgr.GetBySql(tmp, sqlStr, nil)

	var clanId uint64
	if err == nil {
		clanId = tmp.Id
	} else {
		return
	}
	clanInfo = Find(clanId)
	return
}

func GetClanInfoByIncreId(id uint32) (clanInfo *msg.ClanInfo) {
	if id == 0 {
		return nil
	}
	mgr := dao.ClanInfo
	tableName := mgr.TableName()
	sqlStr := fmt.Sprintf("select id from %s where clan_increment_id=%d and member_count > 0 limit 1",
		tableName,
		id)
	tmp := &msg.ClanInfo{}
	err := mgr.GetBySql(tmp, sqlStr, nil)

	var clanId uint64
	if err == nil {
		clanId = tmp.Id
	}
	clanInfo = Find(clanId)
	return
}

func GetClanIdByName(escapedName string) (id uint64) {
	mgr := dao.ClanInfo
	tableName := mgr.TableName()
	sqlStr := fmt.Sprintf("select id from %s where name='%s' limit 1",
		tableName,
		escapedName)
	tmp := &msg.ClanInfo{}
	err := mgr.GetBySql(tmp, sqlStr, nil)
	if err == nil {
		id = tmp.Id
	}
	return id
}

func HasBadWord(err *msg.ErrorInfo, reqName string) {
	badWord, hasBad := xsensitive.ContainsBadWord(reqName)
	if hasBad {
		err.Id, err.DebugMsg = msg.Ecode_HAS_BAD_WORD, "has bad word"
		err.Params = append(err.Params, badWord)
		return
	}
	return
}

func HasNameExisted(err *msg.ErrorInfo, escapedName string) {
	existedId := GetClanIdByName(escapedName)
	if existedId != 0 {
		err.Id, err.DebugMsg = msg.Ecode_HAS_EXIST_NAME, "has exist name"
		return
	}
	return
}

func MustCreateClan(r *msg.PlayerInfo, clanName string, applyType int32, badge int32) (clan *msg.ClanInfo) {
	/*if r.ClanId != 0 {
		return Find(r.ClanId)
	}*/

	uid := xsnowflake.NextIdUint64()

	var c redisdb.ClanInfoCache
	clan = c.Find(uid)
	if clan != nil {
		log.Errorf("repeated clan %d", uid)
		panic(fmt.Errorf("repeated clan %d", uid))
		return
	}

	clanId, err := IncrClanId()
	if err != nil {
		return
	}
	// Init clan
	clan = &msg.ClanInfo{
		Id:                 uid,
		PresidentId:        r.Id,
		VicePresidentId:    0,
		PresidentBrief:     nil,
		ClanIncrementId:    clanId,
		Name:               clanName,
		Badge:              badge,
		ApplyType:          applyType,
		Announcement:       "",
		Motto:              "",
		LanguageId:         0,
		MemberCount:        1,
		LastActiveTime:     time3.Now().Unix(),
		ActiveValue:        make(map[int64]int32),
		AverageActiveValue: make(map[int64]int32),
	}
	createdId := c.MustCreate(uid, clan)
	if createdId != uid {
		log.Errorf("create clan fail %d %d", createdId, uid)
	}
	return
}

func MustGetPlayerInfoClan(r *msg.PlayerInfo) *msg.PlayerInfoClan {
	if r.Clan != nil {
		return r.Clan
	}

	var c redisdb.PlayerInfoClanCache
	r2 := c.Find(r.Id)
	//log.Infof("PlayerInfoClan %+v", r2)

	if r2 == nil {
		bossInfos := initBoss()

		r2 = &msg.PlayerInfoClan{
			ApplyList: make(map[uint64]int64),
			BossInfos: bossInfos,
		}
		id := c.MustCreate(r.Id, r2)
		if id != r.Id {
			panic(fmt.Errorf("GetPlayerInfoClan fail %d,%d,%+v", id, r.Id, r2))
		}
	}

	if r2.ApplyList == nil {
		r2.ApplyList = make(map[uint64]int64)
	}
	if r2.BossInfos == nil {
		r2.BossInfos = initBoss()
	}

	r.Clan = r2
	return r2
}

// redis 里的find都是每次unmarshal，相当于每次都是新的，不是纯正的缓存
// 可以分方便的实现事务回滚
// 此处，需要延续性，，将其和playerInfo联系起来，，这样数据的生命期和有效期完全依托于playerInfo
// 有线程安全问题，但实际上没事，，业务逻辑做了seq的串行化
func Find(clanId uint64) (clanInfo *msg.ClanInfo) {
	if clanId == 0 {
		return
	}
	//log.Debugf("find clan id %d", clanId)
	var c redisdb.ClanInfoCache
	clanInfo = c.Find(clanId)
	if clanInfo.ActiveValue == nil {
		clanInfo.ActiveValue = make(map[int64]int32)
	}
	if clanInfo.AverageActiveValue == nil {
		clanInfo.AverageActiveValue = make(map[int64]int32)
	}

	//log.Debugf("find ClanInfo: %u, %+v", clanId, clanInfo)
	return
}

func MustGetClanInfoMember(clanInfo *msg.ClanInfo) *msg.ClanInfoMember {
	if clanInfo == nil {
		return nil
	}

	if clanInfo.Member != nil {
		return clanInfo.Member
	}

	var c redisdb.ClanInfoMemberCache
	r2 := c.Find(clanInfo.Id)
	//log.Debugf("ClanInfoMember %+v", r2)

	if r2 == nil {
		r2 = &msg.ClanInfoMember{
			MemberList: make([]*msg.MemberDBInfo, 0),
		}
		id := c.MustCreate(clanInfo.Id, r2)
		if id != clanInfo.Id {
			panic(fmt.Errorf(" GetClanInfoMember fail  %+u,%+u,%+v", id, clanInfo.Id, r2))
		}
	}
	if r2.MemberList == nil {
		r2.MemberList = make([]*msg.MemberDBInfo, 0)
	}
	clanInfo.Member = r2
	return r2
}

func MustGetClanInfoExt(clanInfo *msg.ClanInfo) *msg.ClanInfoExt {
	if clanInfo == nil {
		return nil
	}

	if clanInfo.Ext != nil {
		return clanInfo.Ext
	}

	var c redisdb.ClanInfoExtCache
	r2 := c.Find(clanInfo.Id)
	//log.Debugf("MustGetClanInfoExt %+v", r2)

	if r2 == nil {
		r2 = &msg.ClanInfoExt{
			LogDbInfos: make([]*msg.LogDbInfo, 0),
			ApplyList:  make(map[uint64]int64),
			//MailList:   make([]*msg.ClanMailInfo, 0),
		}
		id := c.MustCreate(clanInfo.Id, r2)
		if id != clanInfo.Id {
			panic(fmt.Errorf(" MustGetClanInfoExt fail  %+u,%+u,%+v", id, clanInfo.Id, r2))
		}
	}
	if r2.LogDbInfos == nil {
		r2.LogDbInfos = make([]*msg.LogDbInfo, 0)
	}
	if r2.ApplyList == nil {
		r2.ApplyList = make(map[uint64]int64)
	}
	//	if r2.MailList == nil {
	//	r2.MailList = make([]*msg.ClanMailInfo, 0)
	//	}
	clanInfo.Ext = r2
	return r2
}

func getAllClan(count int64) (rangeValues []string) {
	rangeValues, ok := persistredis.Redis.ZRevRange(ClanCache, 0, int64(count))
	if !ok {
		return
	}
	return
}

func getRandClan(count int64) (rangeValues []string) {
	if count == 0 {
		return nil
	}
	curTime := time3.Now().UnixNano()
	rand.Seed(curTime)
	random := rand.Perm(int(count))

	globalConf, ok := conf.GetGlobalConfig(1)
	if !ok {
		panic("globalConf is nil")
	}

	index := 0
	for {
		randNum := int64(random[index])
		oneItem, ok := persistredis.Redis.ZRevRange(ClanCache, randNum, randNum)
		if !ok {
			return
		}
		rangeValues = append(rangeValues, oneItem...)
		index = index + 1
		if int32(len(rangeValues)) >= globalConf.UnionRecommendNum {
			break
		}
	}
	return
}

func GetRecommend() (rangeValues []string) {
	globalConf, ok := conf.GetGlobalConfig(1)
	if !ok {
		panic("globalConf is nil")
	}

	now := time3.Now().Unix()
	unionActiveTime := now - int64(globalConf.UnionLastRequestInDays*DaySeconds)
	unionActiveTimeStr := fmt.Sprintf("%d", unionActiveTime)
	nowStr := fmt.Sprintf("%d", now)
	//log.Debugf("unionActiveTime start %s", unionActiveTimeStr)
	//log.Debugf("unionActiveTime end %s", nowStr)
	count, ok := persistredis.Redis.ZCount(ClanCache, unionActiveTimeStr, nowStr)
	if !ok {
		return
	}
	//log.Debugf("zCount is %u", count)
	if count <= int64(globalConf.UnionRecommendNum) {
		rangeValues = getAllClan(int64(globalConf.UnionRecommendNum))
	} else {
		rangeValues = getRandClan(count)
	}

	return
}

func GetClanInfo(clanIds []string) (clanInfo []*msg.ClanInfo) {
	for _, v := range clanIds {
		clanId, err1 := strconv.ParseInt(v, 10, 64)
		if err1 != nil {
			err1 = fmt.Errorf("clan id is change error%s", v)
			return
		}
		clan := Find(uint64(clanId))
		if clan == nil {
			err1 = fmt.Errorf("clan id is not exist%d", clanId)
			return
		}
		clanInfo = append(clanInfo, clan)
	}
	return
}

func ApplyJoin(clanInfo *msg.ClanInfo, r *msg.PlayerInfo, err *msg.ErrorInfo) bool {
	if clanInfo == nil {
		err.Id, err.DebugMsg = 2002, "ApplyJoin can not find the clan"
		return false
	}

	globalConf, ok := conf.GetGlobalConfig(1)
	if !ok {
		panic("globalConf is nil")
	}
	if clanInfo.MemberCount >= globalConf.UnionMemberLimit {
		err.Id, err.DebugMsg = 2003, "clan memberCount is limited"
		return false
	}

	playerInfoClan := MustGetPlayerInfoClan(r)
	if playerInfoClan == nil {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "get playerInfoClan nil"
		return false
	}
	if 0 == clanInfo.ApplyType {
		clanMember := MustGetClanInfoMember(clanInfo)
		if clanMember == nil {
			err.Id, err.DebugMsg = msg.Ecode_INVALID, "get clanInfoMember nil"
			return false
		}

		if int32(len(clanMember.MemberList)) >= globalConf.UnionMemberLimit {
			err.Id, err.DebugMsg = 2003, "clan clanMember.MemberList is limited"
			return false
		}

		clanInfoExt := MustGetClanInfoExt(clanInfo)
		if clanInfoExt == nil {
			err.Id, err.DebugMsg = msg.Ecode_INVALID, "clanInfoExt is nil"
			return false
		}

		AddMember(r.Id, msg.PositionType_PT_MEMBER, clanInfo, clanMember, clanInfoExt, err)
		if err.Id != 0 {
			log.Debugf("apply join addMember is error")
			return false
		}
		//改成了,最后设置 clanId 为了统一判断一个用户是否已经加入另外一个公会或者被同意加入另外一个公会
		ok := share.SetClanId(r.Id, clanInfo.Id, 0)
		if !ok {
			err.Id, err.DebugMsg = msg.Ecode_INVALID, "set clan id err"
			return false
		}

	} else {
		if int32(len(playerInfoClan.ApplyList)) >= globalConf.UnionApplyMax {
			err.Id, err.DebugMsg = 2006, "player applyList is max"
			return false
		}
		curTime := time3.Now().Unix()
		playerInfoClan.ApplyList[clanInfo.Id] = curTime + +int64(globalConf.UnionApplyValidTime*HourSeconds)

		clanInfoExt := MustGetClanInfoExt(clanInfo)
		if clanInfoExt == nil {
			err.Id, err.DebugMsg = msg.Ecode_INVALID, "clanInfoExt is nil"
			return false
		}
		if int32(len(clanInfoExt.ApplyList)) >= globalConf.UnionAcceptApplyMax {
			err.Id, err.DebugMsg = 2007, "clanInfoExt ApplyList is max"
			return false
		}
		// 已申请，直接覆盖可能更好一些
		now := time3.Now().Unix()

		for k, v := range clanInfoExt.ApplyList {
			if k == r.Id {
				if v+int64(globalConf.UnionApplyValidTime*HourSeconds) > now {
					err.Id, err.DebugMsg = 2005, "player had apply this clan"
					return false
				}
			}
		}
		clanInfoExt.ApplyList[r.Id] = now + +int64(globalConf.UnionApplyValidTime*HourSeconds)
	}

	return true
}

func AgreeJoin(r *msg.PlayerInfo, clanInfo *msg.ClanInfo, playerId uint64, isAgree int32, err *msg.ErrorInfo) (playerIds []uint64) {
	if clanInfo == nil {
		err.Id, err.DebugMsg = 2015, "has no clan"
		return
	}

	clanInfoMember := MustGetClanInfoMember(clanInfo)
	if clanInfoMember == nil {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "clanInfoMember is nil"
		return
	}

	hasPrivileges := CheckPrivileges(clanInfoMember.MemberList, r.Id)
	if !hasPrivileges {
		err.Id, err.DebugMsg = 2019, "has no privileges"
		return
	}

	clanInfoExt := MustGetClanInfoExt(clanInfo)
	if clanInfoExt == nil {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "clanInfoExt is nil"
		return
	}

	//playerInfoClan := MustGetPlayerInfoClan(r)
	//if playerInfoClan == nil {
	//	err.Id, err.DebugMsg = msg.Ecode_INVALID, "playerInfoClan is nil"
	//	return
	//}

	globalConf, ok := conf.GetGlobalConfig(1)
	if !ok {
		panic("globalConf is nil")
	}

	now := time3.Now().Unix()

	if 0 == playerId {
		for k, v := range clanInfoExt.ApplyList {
			if v < now {
				//log.Debugf("playerId %d", playerId)
				//log.Debugf("clanInfoExt ApplyList is expired k=%d,v=%d", k, v)
				delete(clanInfoExt.ApplyList, k)
				continue
			}
			if 1 == isAgree {
				AddMember(k, msg.PositionType_PT_MEMBER, clanInfo, clanInfoMember, clanInfoExt, err)
				if err.Id != 0 {
					continue
				}
			}

			playerIds = append(playerIds, k)
			//delete(playerInfoClan.ApplyList, clanInfo.Id)
		}
		clanInfoExt.ApplyList = nil
	} else {
		//log.Debugf("playerId %d", playerId)
		if clanInfo.MemberCount >= globalConf.UnionMemberLimit {
			err.Id, err.DebugMsg = 2034, "Clan Member Limit"
		}

		hasThePlayer := false

		for k, v := range clanInfoExt.ApplyList {
			if v < now {
				delete(clanInfoExt.ApplyList, k)
				//log.Debugf("clanInfoExt ApplyList is expired k=%d,v=%d", k, v)
				//err.Id, err.DebugMsg = msg.Ecode_INVALID, "playerInfoClan is nil"
				continue
			}

			if k == playerId {
				hasThePlayer = true
				delete(clanInfoExt.ApplyList, k)
				break
			}
		}

		if false == hasThePlayer {
			err.Id, err.DebugMsg = msg.Ecode_INVALID, "playerInfoClan is nil"
			return
		}

		if 1 == isAgree {
			AddMember(playerId, msg.PositionType_PT_MEMBER, clanInfo, clanInfoMember, clanInfoExt, err)
			if err.Id != 0 {
				return
			}
		}

		//delete(playerInfoClan.ApplyList, clanInfo.Id)
		playerIds = append(playerIds, playerId)
	}

	return
}

func AppointPosition(r *msg.PlayerInfo, clanInfo *msg.ClanInfo, playerId uint64, position msg.PositionType, err *msg.ErrorInfo) (members []*msg.MemberDBInfo) {
	if clanInfo.PresidentId != r.Id {
		err.Id, err.DebugMsg = 2016, "11 has no privileges"
		return
	}

	clanInfoMember := MustGetClanInfoMember(clanInfo)
	if clanInfoMember == nil {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "clanInfoMember is nil"
		return
	}
	HasMember(clanInfoMember.MemberList, playerId, err)
	if err.Id != 0 {
		return
	}

	var logDbInfo *msg.LogDbInfo

	if position == msg.PositionType_PT_VICE_PRESIDENT {
		if clanInfo.VicePresidentId == playerId {
			err.Id, err.DebugMsg = 2028, "already is president"
			return
		}
		for _, v := range clanInfoMember.MemberList {
			if v.Position == int32(msg.PositionType_PT_VICE_PRESIDENT) {
				err.Id, err.DebugMsg = 2029, "has two presidents"
				return
			}
		}

		clanInfo.VicePresidentId = playerId

		logDbInfo = OperateLog(msg.ClanLogType_LOG_TYPE_APPOINT, playerId)

	} else if position == msg.PositionType_PT_PRESIDENT {
		if clanInfo.PresidentId == playerId {
			err.Id, err.DebugMsg = 2028, "already is president"
			return
		}

		clanInfo.PresidentId = playerId
		for _, v := range clanInfoMember.MemberList {
			if v.PlayerId == r.Id {
				v.Position = int32(msg.PositionType_PT_MEMBER)
				break
			}
		}
		if clanInfo.VicePresidentId == playerId {
			clanInfo.VicePresidentId = 0
		}

		logDbInfo = OperateLog(msg.ClanLogType_LOG_TYPE_TRANSFER, r.Id, playerId)
	} else if position == msg.PositionType_PT_MEMBER {
		//降职副会长为成员后处理clan.VicePresidentId
		if clanInfo.VicePresidentId == playerId {
			clanInfo.VicePresidentId = 0
		}
		logDbInfo = OperateLog(msg.ClanLogType_LOG_TYPE_VICE_PRESIDENT_NULL, r.Id)
	}

	for _, v := range clanInfoMember.MemberList {
		if v.PlayerId == playerId {
			v.Position = int32(position)
			members = append(members, v)
		} else if v.PlayerId == r.Id {
			members = append(members, v)
		}
	}

	ext := MustGetClanInfoExt(clanInfo)
	if ext == nil {
		err.Id = msg.Ecode_INVALID
		return
	}

	if logDbInfo != nil {
		ext.LogDbInfos = append(ext.LogDbInfos, logDbInfo)
		ext.LogDbInfos = ValidLog(ext)
	}

	return
}

func ExpelClanMember(r *msg.PlayerInfo, clan *msg.ClanInfo, playerId uint64, err *msg.ErrorInfo) {
	clanInfoMember := MustGetClanInfoMember(clan)
	if clanInfoMember == nil {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "clanInfoMember is nil"
		return
	}

	hasPrivileges := CheckPrivileges(clanInfoMember.MemberList, r.Id)
	if !hasPrivileges {
		err.Id, err.DebugMsg = 2019, "has no privileges"
		return
	}

	ext := MustGetClanInfoExt(clan)
	if ext == nil {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "ext is nil"
		return
	}

	var hasMember bool
	for i := 0; i < len(clanInfoMember.MemberList); i++ {
		if clanInfoMember.MemberList[i].PlayerId == playerId {
			hasMember = true
			if int32(msg.PositionType_PT_VICE_PRESIDENT) == clanInfoMember.MemberList[i].Position {
				clan.VicePresidentId = 0
				logDbInfo := OperateLog(msg.ClanLogType_LOG_TYPE_VICE_PRESIDENT_NULL, playerId)
				ext.LogDbInfos = append(ext.LogDbInfos, logDbInfo)
			}
			clanInfoMember.MemberList[i].Position = int32(msg.PositionType_PT_MEMBER)
			clanInfoMember.MemberList = append(clanInfoMember.MemberList[:i], clanInfoMember.MemberList[i+1:]...)
			break
		}
	}

	if hasMember == false {
		err.Id, err.DebugMsg = 2025, "has no member"
	}

	clan.MemberCount = int32(len(clanInfoMember.MemberList))
	if clan.MemberCount <= 0 {
		if !RemoveRedis(clan.Id) {
			return
		}
	}

}

func Quit(r *msg.PlayerInfo, clan *msg.ClanInfo, err *msg.ErrorInfo) (newPresidentId uint64) {
	clanInfoMember := MustGetClanInfoMember(clan)
	if clanInfoMember == nil {
		err.Id = msg.Ecode_INVALID
		return
	}

	ext := MustGetClanInfoExt(clan)
	if ext == nil {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "ext is nil"
		return
	}

	if clan.PresidentId == r.Id {
		if clan.VicePresidentId > 0 {
			//副会长变成了会长后,副会长置空即可
			newPresidentId = clan.VicePresidentId
			//副会长退出没有自动转让置空即可
			clan.VicePresidentId = 0
			logDbInfo := OperateLog(msg.ClanLogType_LOG_TYPE_VICE_PRESIDENT_NULL, r.Id)
			ext.LogDbInfos = append(ext.LogDbInfos, logDbInfo)
		} else {
			reverseActiveSlice := SevenDayAllActive(clan)
			if len(reverseActiveSlice) == 0 {
				for _, v := range clanInfoMember.MemberList {
					if int32(msg.PositionType_PT_MEMBER) == v.Position {
						newPresidentId = v.PlayerId
						break
					}
				}
			} else {
				for _, v := range reverseActiveSlice {
					if v.id == clan.PresidentId {
						continue
					}
					newPresidentId = v.id
				}
			}
		}
		//log.Debugf("newPresidentId :%+v", newPresidentId)
		if newPresidentId > 0 {
			AppointPosition(r, clan, newPresidentId, msg.PositionType_PT_PRESIDENT, err)
			if err.Id > 0 {
				err.Id = msg.Ecode_INVALID
				return
			}
		}
	}

	for i := 0; i < len(clanInfoMember.MemberList); i++ {
		if clanInfoMember.MemberList[i].PlayerId == r.Id {
			clanInfoMember.MemberList = append(clanInfoMember.MemberList[:i], clanInfoMember.MemberList[i+1:]...)
			break
		}
	}
	clan.MemberCount = int32(len(clanInfoMember.MemberList))

	if clan.MemberCount <= 0 {
		if !RemoveRedis(clan.Id) {
			return
		}
	}

	logDbInfo := OperateLog(msg.ClanLogType_LOG_TYPE_QUIT, r.Id)

	ext.LogDbInfos = append(ext.LogDbInfos, logDbInfo)

	ext.LogDbInfos = ValidLog(ext)
	return
}

func CheckPrivileges(memberList []*msg.MemberDBInfo, playerId uint64) bool {
	for _, v := range memberList {
		if v.PlayerId == playerId &&
			v.Position > int32(msg.PositionType_PT_MEMBER) {
			return true
		}
	}
	return false
}

func ModifyInfo(_type msg.ModifyClanType, content string, clanInfo *msg.ClanInfo, err *msg.ErrorInfo) {

	length := int32(utf8.RuneCountInString(content))
	globalConf, ok := conf.GetGlobalConfig(1)
	if !ok {
		panic("globalConf is nil")
	}

	switch _type {
	//1.name名字2.徽章 badge id 3.公告announcement4.宣言motto5.语言languageId
	case msg.ModifyClanType_MODIFY_TYPE_NAME:
		escapedName := CheckName(content, err)
		if err.Id != 0 {
			return
		}
		clanInfo.Name = escapedName
	case msg.ModifyClanType_MODIFY_TYPE_BADGE:
		badge, _err := strconv.Atoi(content)
		if _err != nil {
			err.Id = msg.Ecode_INVALID
			return
		}
		clanInfo.Badge = int32(badge)
	case msg.ModifyClanType_MODIFY_TYPE_ANNOUNCEMENT:
		if length == 0 {
			err.Id, err.DebugMsg = 2022, "content is nil"
			return
		}
		HasBadWord(err, content)
		if err.Id != 0 {
			err.Id, err.DebugMsg = 2011, "clan name is not ok"
			return
		}
		if length > globalConf.UnionAnnouncementWordsLimit {
			err.Id, err.DebugMsg = 2023, "content is limit"
			return
		}
		clanInfo.Announcement = content
	case msg.ModifyClanType_MODIFY_TYPE_MOTTO:
		if length == 0 {
			err.Id, err.DebugMsg = 2022, "content is nil"
			return
		}
		HasBadWord(err, content)
		if err.Id != 0 {
			err.Id, err.DebugMsg = 2011, "clan name is not ok"
			return
		}
		if length > globalConf.UnionDeclareWordsLimit {
			err.Id, err.DebugMsg = 2023, "content is limit"
			return
		}

		clanInfo.Motto = content
	case msg.ModifyClanType_MODIFY_TYPE_LANGUAGE:
		languageId, _err := strconv.Atoi(content)
		if _err != nil {
			err.Id = msg.Ecode_INVALID
			return
		}
		clanInfo.LanguageId = int32(languageId)
	case msg.ModifyClanType_MODIFY_TYPE_JOIN:
		applyType, _err := strconv.Atoi(content)
		if _err != nil {
			err.Id = msg.Ecode_INVALID
			return
		}
		clanInfo.ApplyType = int32(applyType)
	default:
		break
	}
	return
}

func OperateLog(_type msg.ClanLogType, playerId uint64, params ...uint64) (logInfo *msg.LogDbInfo) {

	time := time3.Now().Unix()

	logInfo = &msg.LogDbInfo{
		LogTime: time,
		OpType:  _type,
	}

	switch _type {
	case msg.ClanLogType_LOG_TYPE_JOIN:
		logInfo.Params = append(logInfo.Params, playerId)
		//XX加入工会
	case msg.ClanLogType_LOG_TYPE_QUIT:
		//XX退出工会
		logInfo.Params = append(logInfo.Params, playerId)
	case msg.ClanLogType_LOG_TYPE_DONATE:
		//XX捐献了YY(档位)，工会活跃增加了ZZ
		logInfo.Params = append(logInfo.Params, playerId)
		logInfo.Params = append(logInfo.Params, params[0])
		//logInfo.Params = append(logInfo.Params, params[1])
	case msg.ClanLogType_LOG_TYPE_TRANSFER:
		//XX将会长移交给了YY
		logInfo.Params = append(logInfo.Params, playerId)
		logInfo.Params = append(logInfo.Params, params[0])
	case msg.ClanLogType_LOG_TYPE_APPOINT:
		//XX被任命副会长
		logInfo.Params = append(logInfo.Params, playerId)
	case msg.ClanLogType_LOG_TYPE_ATTACK_BOSS:
		//XX挑战精英BOSS，获得了YY
		logInfo.Params = append(logInfo.Params, playerId)
		logInfo.Params = append(logInfo.Params, params[0])
	case msg.ClanLogType_LOG_TYPE_VICE_PRESIDENT_NULL:
		logInfo.Params = append(logInfo.Params, playerId)
	default: //
		logInfo.Params = append(logInfo.Params, playerId)
	}
	return
}

func Donate(r *msg.PlayerInfo, clanInfo *msg.ClanInfo, playerInfoClan *msg.PlayerInfoClan, staticObjs []*msg.StaticObjInfo, donateType int32, err *msg.ErrorInfo) {
	day := time3.GetDayNum()
	//log.Debugf("day num :%+v", day)

	var logDbInfo *msg.LogDbInfo

	clanMember := MustGetClanInfoMember(clanInfo)
	if clanMember == nil {
		err.Id = msg.Ecode_INVALID
		return
	}
	memberCount := len(clanMember.MemberList)

	globalConf, ok := conf.GetGlobalConfig(1)
	if !ok {
		panic("globalConf is nil")
	}

	for _, v := range staticObjs {
		//log.Debugf("v.Id :%+v", v.Id)
		//log.Debugf("v.Count :%+v", v.Count)
		if v.Id == int32(msg.CurrencyId_CI_ClanActivePoint) {
			clanInfo.ActiveValue[day] = clanInfo.ActiveValue[day] + int32(v.Count)
			clanInfo.AverageActiveValue[day] = (clanInfo.ActiveValue[day]) / int32(memberCount)
			var count int32
			count = clanInfo.AllActiveValue / globalConf.UnionBossNeedActive

			clanInfo.AllActiveValue = clanInfo.AllActiveValue + int32(v.Count)

			if clanInfo.AllActiveValue >= globalConf.UnionActiveLimit {
				clanInfo.AllActiveValue = globalConf.UnionActiveLimit
			}

			if clanInfo.AllActiveValue > 0 {
				if clanInfo.AllActiveValue/globalConf.UnionBossNeedActive == count+1 {
					for _, v := range playerInfoClan.BossInfos {
						if int32(msg.ClanBossType_CLAN_ELITE_BOSS) != v.Id {
							continue
						} //精英boss
						v.Attacktimes++
					}
					addEliteBossTimes(r.Id, clanMember)
				}
			}
		} else if v.Id == int32(msg.CurrencyId_CI_ClanCoin) {
			for _, v2 := range clanMember.MemberList {
				if v2.PlayerId != r.Id {
					continue
				}
				if v2.Contribution == nil {
					v2.Contribution = make(map[int64]int32)
				}
				v2.Contribution[day] = v2.Contribution[day] + int32(v.Count)
				if v2.Contribution[day] >= globalConf.UnionTokenLimit {
					v2.Contribution[day] = globalConf.UnionTokenLimit
				}
				for k3, _ := range v2.Contribution {
					if day-k3 <= int64(days) {
						continue
					}
					delete(v2.Contribution, k3)
				}
				break
			}
		}
	}
	logDbInfo = OperateLog(msg.ClanLogType_LOG_TYPE_DONATE, r.Id, uint64(donateType))

	//log.Debugf("clanInfo.AllActiveValue :%+v", clanInfo.AllActiveValue)
	playerInfoClan.DonateTimes = playerInfoClan.DonateTimes + 1

	ext := MustGetClanInfoExt(clanInfo)
	if ext == nil {
		err.Id = msg.Ecode_INVALID
		return
	}
	if logDbInfo != nil {
		ext.LogDbInfos = append(ext.LogDbInfos, logDbInfo)
		ext.LogDbInfos = ValidLog(ext)
	}
	return
}

func GetJoinId(id uint64) (clanId uint64) {
	if id == 0 {
		clanIds := getAllClan(-1)
		//log.Debugf("GetJoinId clanIds:%+v", clanIds)
		if len(clanIds) == 0 {
			clanId = GetOneClanId()
		} else {
			clanInfos := GetClanInfo(clanIds)

			globalConf, ok := conf.GetGlobalConfig(1)
			if !ok {
				panic("globalConf is nil")
			}

			for _, v := range clanInfos {
				//log.Debugf("GetJoinId v:%+v", v)
				sevenDayActive := SevenDayActive(v)
				if 0 == v.ApplyType &&
					v.MemberCount > 0 &&
					v.MemberCount < globalConf.UnionMemberLimit &&
					sevenDayActive >= globalConf.UnionRandomJoinActiveRequire {
					clanId = v.Id
					break
				}
			}
			//如果都不满足UnionRandomJoinActiveRequire则随机加自由加入一个
			if 0 == clanId {
				for _, v := range clanInfos {
					//log.Debugf("222 GetJoinId v:%+v", v)
					if 0 == v.ApplyType &&
						v.MemberCount > 0 &&
						v.MemberCount < globalConf.UnionMemberLimit {
						clanId = v.Id
						break
					}
				}
			}
		}
	} else {
		clanId = id
	}
	//log.Debugf("GetJoinId final clanId:%+v", clanId)
	return
}

type Pair struct {
	id    uint64
	value int32
}

type PairList []Pair

func (p PairList) Len() int {
	return len(p)
}

func (p PairList) Less(i, j int) bool {
	return p[i].value < p[j].value
}

func (p PairList) Swap(i, j int) {
	p[i], p[j] = p[j], p[i]
}

func SevenDayAllActive(clanInfo *msg.ClanInfo) (p PairList) {
	clanMember := MustGetClanInfoMember(clanInfo)
	if clanMember == nil {
		return
	}
	playerActive := make(map[uint64]int32)
	for _, v := range clanMember.MemberList {
		for _, v2 := range v.Contribution {
			playerActive[v.PlayerId] = playerActive[v.PlayerId] + v2
		}
	}

	p = make(PairList, len(playerActive))
	i := 0
	for k, v := range playerActive {
		p[i] = Pair{k, v}
	}
	sort.Reverse(p)
	//log.Debugf("SevenDayActive :%+v", p)
	return
}

func AddMember(playerId uint64, pos msg.PositionType, clanInfo *msg.ClanInfo, member *msg.ClanInfoMember, ext *msg.ClanInfoExt, err *msg.ErrorInfo) {

	rs := share.GetClanId(playerId)
	if rs == nil {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "rs is nil"
		return
	}
	if rs.ClanId > 0 {
		err.Id, err.DebugMsg = 2033, "the player has joined a clan"
		return
	}

	memDbInfo := &msg.MemberDBInfo{
		PlayerId:     playerId,
		Position:     int32(pos),
		Contribution: make(map[int64]int32),
		JoinTime:     time3.Now().Unix(),
	}

	member.MemberList = append(member.MemberList, memDbInfo)
	clanInfo.MemberCount = int32(len(member.MemberList))

	logDbInfo := OperateLog(msg.ClanLogType_LOG_TYPE_JOIN, playerId)
	ext.LogDbInfos = append(ext.LogDbInfos, logDbInfo)
	ext.LogDbInfos = ValidLog(ext)

	return
}

func RemoveRedis(clanId uint64) bool {
	z := &redis.Z{
		Score:  float64(0),
		Member: clanId,
	}
	//log.Debugf("RemoveRedis clan id :%d", clanId)
	return persistredis.Redis.ZRemove(ClanCache, z)
}

func ValidLog(ext *msg.ClanInfoExt) (logs []*msg.LogDbInfo) {
	if ext == nil {
		log.Debugf("ext is nil")
		return
	}

	globalConf, ok := conf.GetGlobalConfig(1)
	if !ok {
		panic("globalConf is nil")
	}

	if int32(len(ext.LogDbInfos)) > globalConf.UnionLogSavedLimit {
		logs = ext.LogDbInfos[1 : globalConf.UnionLogSavedLimit+1]
	} else {
		logs = ext.LogDbInfos
	}
	return
}

func MemberSevenDayContribution(member *msg.MemberDBInfo) (allActive int32) {
	for _, v := range member.Contribution {
		allActive = allActive + v
	}
	//log.Debugf("MemberSevenDayContribution allActive %d", allActive)
	return
}

func SevenDayActive(clanInfo *msg.ClanInfo) (allActive int32) {
	for _, v := range clanInfo.ActiveValue {
		allActive = allActive + v
	}
	//log.Debugf("SevenDayActive allActive %d", allActive)
	return
}

func CheckMailStr(title, content string, err *msg.ErrorInfo) {
	length := int32(utf8.RuneCountInString(title))

	globalConf, ok := conf.GetGlobalConfig(1)
	if !ok {
		panic("globalConf is nil")
	}
	if title == "" {
		err.Id, err.DebugMsg = 2036, "title is nil"
		return
	}

	if length > globalConf.UnionMailTitleWordsLimit {
		err.Id, err.DebugMsg = 2037, "title WordsLimit"
		return
	}

	HasBadWord(err, title)
	if err.Id != 0 {
		err.Id, err.DebugMsg = 2039, "clan mail title has bad word"
		return
	}

	if content == "" {
		err.Id, err.DebugMsg = 2040, "content is nil"
		return
	}

	contentLength := int32(utf8.RuneCountInString(content))
	if contentLength > globalConf.UnionMailContentWordsLimit {
		err.Id, err.DebugMsg = 2041, "content WordsLimit"
		return
	}

	HasBadWord(err, content)
	if err.Id != 0 {
		err.Id, err.DebugMsg = 2043, "clan mail content has bad word"
		return
	}
}

func CheckName(name string, err *msg.ErrorInfo) (escapedName string) {
	globalConf, ok := conf.GetGlobalConfig(1)
	if !ok {
		panic("globalConf is nil")
	}

	length := int32(utf8.RuneCountInString(name))

	if name == "" ||
		length < globalConf.UnionNameCharacterLimit[0] ||
		length > globalConf.UnionNameCharacterLimit[1] {
		err.Id, err.DebugMsg = 2010, "clan name must be 4-16"
		return
	}

	HasBadWord(err, name)
	if err.Id != 0 {
		err.Id, err.DebugMsg = 2011, "clan name is not ok"
		return
	}

	escapedName = dbutil.Escape(name)

	HasNameExisted(err, escapedName)
	if err.Id != 0 {
		err.Id, err.DebugMsg = 2013, "has same clan name "
		return
	}
	return
}

func HasMember(members []*msg.MemberDBInfo, playerId uint64, err *msg.ErrorInfo) (hasMember bool) {
	for _, v := range members {
		if v.PlayerId == playerId {
			hasMember = true
		}
	}
	if !hasMember {
		err.Id, err.DebugMsg = 2025, "has no member"
	}
	return
}

func RemoveApplyList(clan *msg.ClanInfo, player *msg.PlayerInfo) bool {
	if clan == nil {
		log.Debugf("RemoveApplyList clan is nil")
		return false
	}
	if player == nil {
		log.Debugf("RemoveApplyList player is nil")
		return false
	}

	ext := MustGetClanInfoExt(clan)
	if ext == nil {
		log.Debugf("RemoveApplyList clan ext is nil")
		return false
	}

	now := time3.Now().Unix()

	playerClan := MustGetPlayerInfoClan(player)
	if playerClan == nil {
		log.Debugf("RemoveApplyList playerClan is nil")
		return false
	}

	_, ok := ext.ApplyList[player.Id]
	if ok {
		if ext.ApplyList[player.Id] < now {
			delete(ext.ApplyList, player.Id)
			delete(playerClan.ApplyList, clan.Id)
			return true
		}
	} else {
		delete(playerClan.ApplyList, clan.Id)
		return true
	}
	return false
}

func NDayAverageActive(clan *msg.ClanInfo) (allActive int32) {
	for _, v := range clan.AverageActiveValue {
		allActive = allActive + v
	}
	//log.Debugf("NDayAverageActive %d", allActive)
	return
}

func FakeBossDropObjs(playerInfoClan *msg.PlayerInfoClan, clanInfo *msg.ClanInfo, bossId int32, bossDropStaticObjs []*msg.BossDropStaticObj, err *msg.ErrorInfo) {

	if playerInfoClan == nil {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "FakeDropObjs r nil"
		return
	}

	now := time3.Now().Unix()

	for k, v := range playerInfoClan.BossInfos {
		if v.Id != bossId {
			continue
		}
		if v.StartAttackTime > now {
			//log.Debugf("FakeBossDropObjs has started")
			continue
		}
		bossInfo := &msg.BossInfo{
			Id:                 bossId,
			Attacktimes:        v.Attacktimes,
			StartAttackTime:    now,
			LeftBlood:          v.LeftBlood,
			AllDamage:          v.AllDamage,
			BossDropStaticObjs: bossDropStaticObjs,
			LastBloodId:        v.LastBloodId,
		}
		playerInfoClan.BossInfos[k] = bossInfo
	}
	return
}

func CheckBattleTime(boosId int32, r *msg.PlayerInfo, clan *msg.ClanInfo, err *msg.ErrorInfo) {

	bossBaseConf, ok := conf.GetUnionBossConfig(boosId)
	if !ok {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "CheckBattleTime bossBaseConf error"
		return
	}

	instanceConf, ok := conf.GetInstanceConfig(bossBaseConf.InstanceId)
	if !ok {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "CheckBattleTime instanceConf error"
		return
	}

	now := time3.Now().Unix()

	playerInfoClan := MustGetPlayerInfoClan(r)

	for _, v := range playerInfoClan.BossInfos {
		if v.Id != boosId {
			continue
		}
		if v.StartAttackTime == 0 {
			err.Id, err.DebugMsg = msg.Ecode_INVALID, "normal boss attack time is error"
			return
		}
		if v.Id == int32(msg.ClanBossType_CLAN_BOSS) {
			if now-v.StartAttackTime > int64(instanceConf.Time+ClanBossFixTime) {
				err.Id, err.DebugMsg = msg.Ecode_INVALID, "normal boss attack time is error"
				return
			}
		} else if v.Id == int32(msg.ClanBossType_CLAN_ELITE_BOSS) {
			if now-v.StartAttackTime > int64(instanceConf.Time+ClanBossFixTime) {
				err.Id, err.DebugMsg = msg.Ecode_INVALID, "boss 2 attack time is error "
				return
			}
		}
	}
}

func CheckAttackTimes(boosId int32, playerInfoClan *msg.PlayerInfoClan, clan *msg.ClanInfo, err *msg.ErrorInfo) {

	globalConf, ok := conf.GetGlobalConfig(1)
	if !ok {
		panic("globalConf is nil")
	}

	bossInfos := playerInfoClan.BossInfos
	for _, v := range bossInfos {
		if boosId == int32(msg.ClanBossType_CLAN_ELITE_BOSS) &&
			v.Id == int32(msg.ClanBossType_CLAN_ELITE_BOSS) {
			if v.Attacktimes <= 0 {
				err.Id, err.DebugMsg = 2052, " clan elite boss attack times is not enough"
				return
			}

		} else if boosId == int32(msg.ClanBossType_CLAN_BOSS) &&
			v.Id == int32(msg.ClanBossType_CLAN_BOSS) {
			if v.Attacktimes <= 0 ||
				v.Attacktimes > globalConf.UnionNormalBossFightTimes {
				err.Id, err.DebugMsg = 2051, " clan boss attack times is not enough"
				return
			}
		}
	}
	return
}

func EndBattle(req *msg.ReqEndBattleR, playerInfoClan *msg.PlayerInfoClan, clanInfo *msg.ClanInfo, logRewardIds []int32, bossDropStaticObjs []*msg.BossDropStaticObj, lastBloodId int32, err *msg.ErrorInfo) {
	globalConf, ok := conf.GetGlobalConfig(1)
	if !ok {
		panic("globalConf is nil")
	}

	bossInfos := playerInfoClan.BossInfos
	for _, v := range bossInfos {
		if req.ClanBossId == int32(msg.ClanBossType_CLAN_ELITE_BOSS) &&
			v.Id == int32(msg.ClanBossType_CLAN_ELITE_BOSS) {
			if v.Attacktimes <= 0 {
				err.Id, err.DebugMsg = msg.Ecode_INVALID, " clan elite boss attack times is not enough"
				return
			}
			v.Attacktimes--
			v.StartAttackTime = 0
			v.LeftBlood = req.LeftBlood
			//只扫荡用
			v.AllDamage = req.ClanBossAllDamage
			v.BossDropStaticObjs = bossDropStaticObjs
			v.LastBloodId = lastBloodId

			var logDbInfo *msg.LogDbInfo

			ext := MustGetClanInfoExt(clanInfo)
			if ext == nil {
				err.Id = msg.Ecode_INVALID
				return
			}

			for _, v := range logRewardIds {
				logDbInfo = OperateLog(msg.ClanLogType_LOG_TYPE_ATTACK_BOSS, playerInfoClan.Id, uint64(v))
				if logDbInfo != nil {
					ext.LogDbInfos = append(ext.LogDbInfos, logDbInfo)
				}
			}
			ext.LogDbInfos = ValidLog(ext)

		} else if req.ClanBossId == int32(msg.ClanBossType_CLAN_BOSS) &&
			v.Id == int32(msg.ClanBossType_CLAN_BOSS) {
			if v.Attacktimes <= 0 ||
				v.Attacktimes > globalConf.UnionNormalBossFightTimes {
				err.Id, err.DebugMsg = msg.Ecode_INVALID, " clan boss attack times is not enough"
				return
			}
			v.Attacktimes--
			v.StartAttackTime = 0
			v.LeftBlood = req.LeftBlood
			v.AllDamage = req.ClanBossAllDamage
			v.BossDropStaticObjs = bossDropStaticObjs
			v.LastBloodId = lastBloodId
		}
	}
	return
}

func initBoss() (bossInfos []*msg.BossInfo) {

	globalConf, ok := conf.GetGlobalConfig(1)
	if !ok {
		panic("globalConf is nil")
	}

	bossInfos = make([]*msg.BossInfo, 0)

	bossInfo := &msg.BossInfo{
		Id:          int32(msg.ClanBossType_CLAN_BOSS),
		Attacktimes: globalConf.UnionNormalBossFightTimes,
	}
	bossInfos = append(bossInfos, bossInfo)

	bossInfo2 := &msg.BossInfo{
		Id:          int32(msg.ClanBossType_CLAN_ELITE_BOSS),
		Attacktimes: 0,
	}
	bossInfos = append(bossInfos, bossInfo2)
	return
}

func addEliteBossTimes(id uint64, member *msg.ClanInfoMember) {
	for i := 0; i < len(member.MemberList); i++ {
		if member.MemberList[i].PlayerId == id {
			continue
		}
		member.MemberList[i].EliteBossTimes++
	}
}

func CheckClanActive(r *msg.PlayerInfo, count int64) bool {
	if r == nil {
		return false
	}
	rs := share.GetClanId(r.Id)
	if rs == nil {
		return false
	}
	if rs.ClanId <= 0 {
		return false
	}

	clanInfo := Find(rs.ClanId)
	if clanInfo == nil {
		return false
	}

	var s []int
	for key, _ := range clanInfo.ActiveValue {
		s = append(s, int(key))
	}
	if len(s) == 0 {
		return false
	}
	sort.Ints(s)
	//在将key输出
	//for _, v := range s {
	//fmt.Printf("key=%v value=%v\n", v, m[v])
	key := s[len(s)-1]
	todayActive := clanInfo.ActiveValue[int64(key)]
	//log.Debugf("all value %+v", clanInfo.ActiveValue)
	//log.Debugf("all day is %+v", s)
	//log.Debugf("todayActive is %d", todayActive)
	//log.Debugf("count is %d", count)

	//}
	if int64(todayActive) < count {
		return false
	}
	return true
}
