package clan

import (
	"bitbucket.org/funplus/golib/encoding2/protobuf"
	"context"
	"server/pkg/gen_redis/redisdb"
	"time"

	"server/internal"
	"server/pkg/cache"
	"server/pkg/gen/msg"
)

var (
	briefCacheTtl = time.Hour * 24
)

func keyOfBrief(clanId uint64) string {
	return cache.GetDbKey(internal.Config.DbVer(), "no_db_clan_brief", clanId)
}

func FindClanBrief(clanId uint64) (ret *msg.ClanBrief) {
	if clanId == 0 {
		return
	}

	key := keyOfBrief(clanId)

	b, ok := cache.Redis.Get(key)
	if !ok || len(b) == 0 {
		// load player & update
		var c redisdb.ClanInfoCache
		r := c.Find(clanId)
		if r == nil {
			return nil
		}

		ret = MustUpdateClanBrief(r)
		return
	}

	ret = unmarshalClanBrief(key, b)
	return
}

// 会更新缓存时间戳
func unmarshalClanBrief(key string, b []byte) *msg.ClanBrief {
	if len(key) == 0 || len(b) == 0 {
		return nil
	}

	ret := &msg.ClanBrief{}
	err := protobuf.Codec.Unmarshal(b, ret)
	if err == nil {
		// update redis expire
		cache.Redis.Expire(key, briefCacheTtl)
	} else {
		ret = nil
	}
	return ret
}

func FindClanBriefs(clanIds []uint64) (ret []*msg.ClanBrief) {
	// key name
	keys := make([]string, len(clanIds))
	for i := 0; i < len(clanIds); i++ {
		keys[i] = keyOfBrief(clanIds[i])
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()

	sc := cache.Redis.Cmd.MGet(ctx, keys...)
	results, err := sc.Result()
	if err != nil {
		return nil
	}

	ret = make([]*msg.ClanBrief, 0, len(clanIds))
	for i := 0; i < len(clanIds); i++ {
		var d *msg.ClanBrief
		b, ok := results[i].([]byte)
		if ok && len(b) > 0 {
			d = unmarshalClanBrief(keys[i], b)
		}

		if d == nil {
			d = FindClanBrief(clanIds[i])
		}

		if d != nil {
			ret = append(ret, d)
		}
	}
	return
}

func MustUpdateClanBrief(source *msg.ClanInfo) *msg.ClanBrief {
	if source == nil {
		return nil
	}

	data := &msg.ClanBrief{
		Id:   source.Id,
		Name: source.Name,
		Icon: source.Badge,
	}

	key := keyOfBrief(data.Id)
	b, err := protobuf.Codec.Marshal(data)
	if err != nil {
		panic("ClanBrief marshal err")
	}

	cache.Redis.Set(key, b, briefCacheTtl)
	return data
}
