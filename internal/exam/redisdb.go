package exam

import (
	"fmt"
	"server/pkg/gen/msg"
	"server/pkg/gen_db/dao"
	"server/pkg/gen_redis/redisdb"
	"time"
)

func ExamRedisDb() {
	//test
	go func() {
		fmt.Printf("Test\n")

		time.Sleep(time.Second * 10)
		var c redisdb.TestStateCache
		for i := 0; i < 100; i++ {
			id := uint64(i)
			d := &msg.TestState{
				Id:    id,
				Alias: fmt.Sprintf("aa%d", i),
				Gold:  0,
			}

			x := c.Find(id)
			if x == nil {
				fmt.Printf("create %d\n", id)
				dao.TestState.Create(id, d)
			}

			x = c.Find(id)
			if x != nil {
				x.Gold = x.Gold + 100
				c.MustUpdate(id, x)
			}

			x = c.Find(id)
			if x != nil {
				x.Gold = x.Gold + 1
				c.MustUpdate(id, x)
			}
		}
	}()
}
