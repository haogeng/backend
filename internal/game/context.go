package game

import (
	"server/pkg/fungin/xmiddleware"
	"server/pkg/gen/msg"
)

const (
	KeyCtxNewTroopConfigIds = "KeyCtxNewTroopConfigIds"
)

// 在game里可用，，类似于login，可能存在uid和ctx没有绑定的情况，用了之后没效果
// 强逻辑耦合
func GetCtxValue(r *msg.PlayerInfo, key string, defaultValue interface{}) interface{} {
	ctx := xmiddleware.GetCtxByUid(r.Id)
	if ctx == nil {
		return nil
	}

	v := ctx.Value(key)
	if v == nil {
		ctx.Set(key, defaultValue)
		return defaultValue
	}

	return v
}

func SetCtxValue(r *msg.PlayerInfo, key string, v interface{}) {
	ctx := xmiddleware.GetCtxByUid(r.Id)
	if ctx == nil {
		return
	}

	ctx.Set(key, v)
}
