package game

import (
	"server/pkg/gen/msg"
	"server/pkg/gen_redis/redisdb"
	"time"
)

// 做一些时序无关的逻辑的初始化工作
func init() {
	//log.Debug("game::init()")

	// 设置最后一次的近似在线时间
	redisdb.SetOnBeforePlayerInfoMustUpdate(func(_data *msg.PlayerInfo) {
		// ORDER
		// 20s的容错，，没必要因为这个多余存档
		// 20s误差
		t := int64(time.Now().Unix())
		if t-_data.LastLogoutTime > 20 {
			_data.LastLogoutTime = t
		}

		MustUpdatePlayerBrief(_data)
	})
}
