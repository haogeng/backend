package fixmath

import "fmt"

// 参考golang.org/x/image/math/fixed

// 未完全实现
type Num int64

const (
	shift = Num(32)
	mask  = Num(1)<<32 - 1

	OneInt64 = int64(1) << shift
	One      = Num(OneInt64)
	Half     = One / 2
)

func FromInt(i int) Num {
	return Num(int64(i) * OneInt64)
}
func FromFloat(f float32) Num {
	return Num(int64(float64(f) * float64(OneInt64)))
}
func (x Num) ToInt() int {
	return int(int64(x) / OneInt64)
}
func (x Num) ToFloat() float32 {
	return float32(float64(x) / float64(OneInt64))
}

func (x Num) String() string {
	if x >= 0 {
		return fmt.Sprintf("%d:%02d(%v)", int64(x>>shift), int64(x&mask), float64(x)/float64(OneInt64))
	}
	x = -x
	if x >= 0 {
		return fmt.Sprintf("-%d:%02d(%v)", int64(x>>shift), int64(x&mask), float64(x)/float64(OneInt64))
	}
	return "should never go here"
}

// ref photon/Fix64.cs
func (x Num) Mul(y Num) Num {
	xlo := int64(x & 0x00000000FFFFFFFF)
	xhi := int64(x >> shift)
	ylo := int64(y & 0x00000000FFFFFFFF)
	yhi := int64(y >> shift)

	lolo := xlo * ylo
	lohi := xlo * yhi
	hilo := xhi * ylo
	hihi := xhi * yhi

	loResult := lolo >> shift
	midResult1 := lohi
	midResult2 := hilo
	hiResult := hihi << shift

	sum := loResult + midResult1 + midResult2 + hiResult

	return Num(sum)
}

func (x Num) Div(y Num) Num {
	// Too hard...
	panic("not support")
	return x / y * One
}
