package fixmath

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestNumInt(t *testing.T) {
	i, j := 7, 2
	ni, nj := FromInt(i), FromInt(j)

	t.Log(ni, nj, ni.Mul(nj))

	a := i + j
	na := (ni + nj).ToInt()
	assert.Equal(t, a, na)

	b := i - j
	nb := (ni - nj).ToInt()
	assert.Equal(t, b, nb)

	c := i * j
	nc := (ni.Mul(nj)).ToInt()
	assert.Equal(t, c, nc)

	d := i / j
	nd := (ni.Div(nj)).ToInt()
	assert.Equal(t, d, nd)
}

func compareFloat(a, b float32) bool {
	diff := a - b
	if diff < 0 {
		diff = -diff
	}
	if diff < 0.00001 {
		return true
	}
	return false
}

func TestNumFloat(t *testing.T) {
	i, j := float32(7.3), float32(2.4)
	ni, nj := FromFloat(i), FromFloat(j)

	t.Log(ni, nj, ni.Mul(nj))

	a := i + j
	na := (ni + nj).ToFloat()
	assert.True(t, compareFloat(a, na))

	b := i - j
	nb := (ni - nj).ToFloat()
	assert.True(t, compareFloat(b, nb))

	c := i * j
	nc := (ni.Mul(nj)).ToFloat()
	assert.True(t, compareFloat(c, nc))

	d := i / j
	nd := (ni.Div(nj)).ToFloat()
	t.Log(d, nd)
	assert.True(t, compareFloat(d, nd))
}
