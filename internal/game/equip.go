package game

import (
	"bitbucket.org/funplus/sandwich/current"
	"math/rand"
	"server/internal/log"
	"server/internal/util"
	"server/pkg/fungin/xmiddleware"
	"server/pkg/gen/conf"
	"server/pkg/gen/msg"
	"server/pkg/gen/rawdata"
)

// 生成装备
func genEquip(r *msg.PlayerInfo, confId int32, err *msg.ErrorInfo) *msg.ObjInfo {
	confData, _ := conf.GetEquipConfig(confId)
	if !CheckConfData(confData, confId, err) {
		return nil
	}

	o := &msg.ObjInfo{
		Id:       0,
		Type:     msg.ObjType_OT_Equipment,
		ConfigId: confId,
		Num:      1,
		Level:    1,
		Star:     confData.Star,
		Extend1:  0,
		Exp:      0,
		OwnerId:  0,
		Pairs:    nil,
	}

	// 生成副属性
	o.Pairs = make([]*msg.IntPair, 0)
	for i := 0; i < len(confData.SideAttrs); i++ {
		op := genEquipSideAttr(confData.SideAttrs[i], err)
		if err.Id != 0 {
			return nil
		}
		if op == nil {
			err.Id, err.DebugMsg = msg.Ecode_INVALID, "op is nil"
			return nil
		}

		o.Pairs = append(o.Pairs, op)
	}

	// 分配Id
	o.Id = DispatchObjIdUnsafe(r, msg.ObjType_OT_Equipment)
	return o
}

func genEquipSideAttr(sideConfId int32, err *msg.ErrorInfo) *msg.IntPair {
	confData, _ := conf.GetEquipSideAttrConfig(sideConfId)
	if !CheckConfData(confData, sideConfId, err) {
		return nil
	}

	log.Debugf("confData: %v", confData)
	if len(confData.PropWeight) == 0 ||
		len(confData.PropWeight) != len(confData.PropList) ||
		len(confData.PropWeight) != len(confData.PropMax) ||
		len(confData.PropWeight) != len(confData.PropMin) {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "conf data err of equip side"
		return nil
	}

	index := util.GetRandomKeyByWeights(confData.PropWeight, true)
	ret := &msg.IntPair{
		Key: confData.PropList[index],
		// 闭区间
		Value: confData.PropMin[index] + rand.Int31n(confData.PropMax[index]-confData.PropMin[index]+1),
	}

	return ret
}

func GetEquip(r *msg.PlayerInfo, id int32, err *msg.ErrorInfo) *msg.ObjInfo {
	if len(ExtEquip(r).Equips) > 0 {
		if d, ok := ExtEquip(r).Equips[id]; ok {
			return d
		}
	}

	err.Id, err.DebugMsg = msg.Ecode_INVALID, "no equip"
	return nil
}

// 返回已重置的东西
func DelEquip(r *msg.PlayerInfo, id int32, err *msg.ErrorInfo) {
	delete(ExtEquip(r).Equips, id)
	return
}

func CheckEquipSlotType(slotType int32, err *msg.ErrorInfo) bool {
	if slotType > int32(msg.EquipSlotType_EST_None) && slotType < int32(msg.EquipSlotType_EST_Max) {
		return true
	}

	err.Id, err.DebugMsg = msg.Ecode_INVALID, "equip slot type err"
	return false
}

// 会有级联反应
// 设置老装备和新装备的owner
func SetEquipIdBySlotType(r *msg.PlayerInfo, troopId int32, slotType int32, equipId int32) (oldEquipId int32) {
	var ext *msg.TroopExtInfo
	// 卸下装备，只有在有装备的时候才做
	if equipId == 0 {
		ext = GetTroopExt(r, troopId)
	} else {
		ext = MustGetTroopExt(r, troopId)
	}

	if ext == nil {
		return
	}

	if ext.Equips == nil {
		ext.Equips = make(map[int32]int32)
	}

	oldEquipId, _ = ext.Equips[slotType]
	if oldEquipId == equipId {
		return
	}

	// change owner
	SetEquipOwner(r, oldEquipId, 0)
	SetEquipOwner(r, equipId, troopId)

	// set equip of this slot
	ext.Equips[slotType] = equipId
	// 客户端需求，，如果没装备，不要kv，服务器无所谓，默认0就好
	if equipId == 0 {
		delete(ext.Equips, slotType)
	}

	return oldEquipId
}

func SetEquipOwner(r *msg.PlayerInfo, equipId int32, ownerId int32) {
	if equipId != 0 {
		// skip this error
		e := GetEquip(r, equipId, &msg.ErrorInfo{})
		if e != nil {
			e.OwnerId = ownerId
		}
	}
}
func UnEquipOnDelTroop(r *msg.PlayerInfo, troopExt *msg.TroopExtInfo) (objInfos []*msg.ObjInfo) {
	changeIds := []int32{}
	if troopExt != nil && len(troopExt.Equips) > 0 {
		tempErr := &msg.ErrorInfo{}
		for _, id := range troopExt.Equips {
			if id != 0 {
				e := GetEquip(r, id, tempErr)
				if e != nil {
					e.OwnerId = 0
					changeIds = append(changeIds, id)
					objInfos = append(objInfos, e)
				}
			}
		}
	}
	if len(changeIds) > 0 {
		NotifyEquipOwnerChange(r, changeIds)
	}
	return
}

func NotifyEquipOwnerChangeWithMap(r *msg.PlayerInfo, idMap map[int32]bool) {
	changeIds := []int32{}
	// map equipId -> x
	for equipId, _ := range idMap {
		changeIds = append(changeIds, equipId)
	}
	if len(changeIds) > 0 {
		NotifyEquipOwnerChange(r, changeIds)
	}
}

func NotifyEquipOwnerChange(r *msg.PlayerInfo, ids []int32) {
	c := xmiddleware.GetCtxByUid(r.Id)
	if c == nil {
		return
	}

	n := &msg.EquipOwnerChangeNotify{
		EquipOwnerInfo: make(map[int32]int32),
	}

	for _, equipId := range ids {
		// dont care err
		e := GetEquip(r, equipId, &msg.ErrorInfo{})
		if e != nil {
			n.EquipOwnerInfo[e.Id] = e.OwnerId
		}
	}

	current.MustAppendAdditionalOutgoingMessage(c, n)
}

func GetEquipLevelConfData(equipConfigId int32, level int32, err *msg.ErrorInfo) *rawdata.EquipLevelConfig {
	equipConf, _ := conf.GetEquipConfig(equipConfigId)
	if !CheckConfData(equipConf, equipConfigId, err) {
		return nil
	}

	levelId, ok := equipConf.LevelCfgs[level]
	if !ok {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "equip levelcfgs err"
		log.Debugf("equip err info: level:%d equipConf:%v", level, equipConf)
		return nil
	}

	levelConf, _ := conf.GetEquipLevelConfig(levelId)
	if !CheckConfData(levelConf, levelId, err) {
		return nil
	}
	return levelConf
}

// TODO 需要提供一个数据修复的功能，类似极端情况的装备和英雄信息对不上了，
// 不一定上线检查，，可以是个gm工具
