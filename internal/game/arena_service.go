package game

import (
	"bitbucket.org/funplus/golib/rand2"
	"bitbucket.org/funplus/golib/time2"
	"bitbucket.org/funplus/sandwich/current"
	"github.com/hashicorp/go-uuid"
	"math"
	"math/rand"
	"server/internal/key"
	"server/internal/log"
	"server/internal/util/time3"
	"server/pkg/fungin/xmiddleware"
	"server/pkg/gen/conf"
	"server/pkg/gen/msg"
	"server/pkg/gen/rawdata"
	"server/pkg/gen_redis/redisdb"
	"server/pkg/persistredis"
	"strconv"
	"time"
)

const RobotMaxId uint64 = 100000    // 机器人最大Id
const ArenaConfigId = 1             // 积分竞技场模板Id
const RewardDailyMailId int32 = 15  // 每日奖励邮件模板
const RewardSeasonMailId int32 = 16 // 赛季奖励邮件模板
const ArenaMatchNumber = 4          // 竞技场匹配人数
const ArenaServerIdIncrement = 20   // 竞技场服务器Id增量

// 检查用户是否为机器人
func CheckPlayerIdIsRobot(playerId uint64) bool {
	if playerId <= RobotMaxId {
		return true
	} else {
		return false
	}
}

// 发送竞技场奖励
func SendArenaRewardInfo(r *msg.PlayerInfo, err *msg.ErrorInfo) {
	var redisArena = persistredis.Rank32
	rArena := MustGetPlayerInfoArena(r)
	if rArena == nil {
		return
	}
	// 赛季信息
	seasonInfo := arenaCache.Find(rArena.SeasonId)
	if seasonInfo == nil {
		// 赛季结束
		return
	}
	curTime := time.Now()
	// 发送竞技场每日奖励邮件
	{
		for _, vTime := range seasonInfo.DayRewards {
			err.Id = 0
			// 检查玩家是否已发放该时间点的奖励
			if status, ok := rArena.DayReward[vTime]; ok && status == msg.RewardStatus_RS_INSEND {
				// 已发送
				continue
			}
			// 此处的排行榜应该获取昨日结算时间点的数据
			dayTimeKey := key.GetArenaDayKeyByMillis(r.ServerId, seasonInfo.Id, vTime)
			myRank, _, _ := redisArena.InfoById(dayTimeKey, r.Id)
			if myRank <= 0 {
				rArena.DayReward[vTime] = msg.RewardStatus_RS_INSEND
				// 未参赛
				continue
			}
			rankData := getArenaRankingConfigByRank(int32(myRank), err)
			if rankData == nil {
				continue
			}
			params := []string{strconv.FormatInt(myRank, 10)}
			sendTime := time.Unix(vTime, 0)
			dayMailItem := NewMailFromConfByTime(RewardDailyMailId, "", rankData.RewardDaily, params, nil, sendTime, err)
			if err.Id != 0 {
				continue
			}
			AddMail(r, dayMailItem, err)

			if err.Id != 0 {
				log.Error("赛季奖励发送失败=%v\n", err)
				continue
			}
			// 更新数据
			rArena.DayReward[vTime] = msg.RewardStatus_RS_INSEND
		}
	}
	// 发送竞技场赛季结束奖励邮件
	{
		// 赛季奖励时间
		seasonRewardTime := time.Unix(seasonInfo.SeasonEndTime, 0)
		// 是否可以领奖
		var reward bool
		if seasonInfo.SeasonNumber == rArena.SeasonNumber {
			if !curTime.Before(seasonRewardTime) {
				reward = true
			}
		} else {
			reward = true
		}
		if reward {
			for sNumber, status := range rArena.SeasonReward {
				err.Id = 0
				// 判断玩家是否已发放该赛季的奖励
				if status == msg.RewardStatus_RS_INSEND {
					continue
				}
				// 此处的排行榜应该获取赛季结算期数的数据
				seasonNumberKey := key.GetArenaSeasonKey(r.ServerId, seasonInfo.Id, sNumber)
				_, myScore, _ := redisArena.InfoById(seasonNumberKey, r.Id)
				stageData := getArenaStageConfigByScore(myScore, err)
				if stageData == nil {
					continue
				}
				params := []string{strconv.FormatInt(int64(stageData.Name), 10)}
				seasonMailItem := NewMailFromConf(RewardSeasonMailId, "", stageData.RewardSeason, params, nil, err)

				AddMail(r, seasonMailItem, err)

				if err.Id != 0 {
					log.Error("赛季奖励发送失败=%v\n", err)
					continue
				}
				// 更新数据
				rArena.SeasonReward[sNumber] = msg.RewardStatus_RS_INSEND
			}
		}
	}
}

// 刷新竞技场信息
func ResetArenaInfos(r *msg.PlayerInfo, err *msg.ErrorInfo) {
	// 普通竞技场刷新
	rArena := MustGetPlayerInfoArena(r)
	if rArena == nil {
		return
	}
	confData, _ := conf.GetArenaConfig(int32(rArena.SeasonId))
	if !CheckConfData(confData, int32(rArena.SeasonId), err) {
		return
	}
	// 当前时间
	curTime := time.Now()
	// 重置时间
	resetTime := time3.TimeOfHoursLater(curTime, int(confData.CountRefresh[0]))
	if curTime.Before(resetTime) {
		// 重置时间未到

	} else if time2.IsToday(rArena.LastResetTime, curTime) {
		// 今日已重置

	} else {
		// 重置竞技场挑战次数,挑战购买次数
		rArena.ArenaBattleNum = 0
		rArena.ArenaBuyNum = 0
		rArena.LastResetTime = curTime.Unix()
		// 刷新红点
		AddRedPointForMyself(r, msg.RedPointType_RPT_ArenaSeason, 0, 0, false)
	}
	// 发送普通竞技场奖励
	SendArenaRewardInfo(r, err)
}

// 初始化竞技场机器人信息
func initRankInfoByRobot(serverId int32, seasonId uint64, robot []int32, err *msg.ErrorInfo) {
	var redisArena = persistredis.Rank32
	for i := robot[0]; i < robot[1]; i++ {
		confData, _ := conf.GetRobotConfig(i)
		if !CheckConfData(confData, i, err) {
			continue
		}
		// 初始化竞技场排行榜积分
		score := rand2.RandomInt(int(confData.JuniroArenaPoint[0]), int(confData.JuniroArenaPoint[1]))
		arenaRankKey := key.GetArenaDefaultKey(serverId, seasonId)
		redisArena.Add(arenaRankKey, uint64(i), int32(score))
	}
}

// 初始化竞技场用户信息
func initArenaUserInfo(r *msg.PlayerInfo, seasonInfo *msg.ArenaSeasonInfo, err *msg.ErrorInfo) (addedObjs []*msg.ObjInfo) {
	// 初始化竞技场防守阵容
	mainChapter := SelectFormation(r, msg.FormationType_FT_MainChapter, err)
	// 初始化竞技场防守阵容
	formation := &msg.Formation{
		Type:     msg.FormationType_FT_ArenaDef,
		TroopIds: mainChapter.TroopIds,
	}
	ChangeFormation(r, formation, err)
	// sync to client
	n := &msg.FormationSync{
		Formations: r.Formations,
	}
	ctx := xmiddleware.GetCtxByUid(r.Id)
	if ctx != nil {
		current.MustAppendAdditionalOutgoingMessage(ctx, n)
		log.Infof("sync formation in initArena: %v", n)
	}

	// 竞技场基础信息
	confData, _ := conf.GetArenaConfig(int32(seasonInfo.Id))
	if !CheckConfData(confData, int32(seasonInfo.Id), err) {
		return
	}
	// 添加赛季号
	r.Arena.SeasonNumber = seasonInfo.SeasonNumber
	// 添加赛季奖励节点
	r.Arena.SeasonReward[seasonInfo.SeasonNumber] = msg.RewardStatus_RS_NOSEND
	// 赠送挑战劵
	objs, _ := JsonToPb_StaticObjs(confData.InitTicket, confData, confData.Id, err)
	// 添加挑战劵
	addedObjs, _ = AddStaticObjs(r, objs, err)
	// 初始化竞技场排行
	var redisArena = persistredis.Rank32
	arenaRankKey := key.GetArenaDefaultKey(r.ServerId, seasonInfo.Id)
	redisArena.Add(arenaRankKey, r.Id, confData.InitPts)

	return
}

// 根据积分获取竞技场段位基础信息
func getArenaStageConfigByScore(score int32, err *msg.ErrorInfo) *rawdata.ArenaRankStageConfig {
	count := conf.CountArenaRankStageConfigMap()
	for i := 1; i <= count; i++ {
		confData, _ := conf.GetArenaRankStageConfig(int32(i))
		if !CheckConfData(confData, int32(i), err) {
			continue
		}
		if score >= confData.Pts[0] && score < confData.Pts[1] {
			return confData
		}
	}
	// 最低段位
	return nil
}

// 根据排名获取竞技场排名基础信息
func getArenaRankingConfigByRank(rank int32, err *msg.ErrorInfo) *rawdata.ArenaRankingConfig {
	count := conf.CountArenaRankingConfigMap()
	for i := 1; i <= count; i++ {
		confData, _ := conf.GetArenaRankingConfig(int32(i))
		if !CheckConfData(confData, int32(i), err) {
			continue
		}
		if rank >= confData.Rank[0] && rank <= confData.Rank[1] {
			return confData
		}
	}
	return nil
}

// 构建竞技场返回信息
func buildArenaInfoResult(r *msg.PlayerInfo, arenaInfo *msg.ArenaSeasonInfo, err *msg.ErrorInfo) *msg.ArenaInfoResult {
	confData, _ := conf.GetArenaConfig(int32(arenaInfo.Id))
	if !CheckConfData(confData, int32(arenaInfo.Id), err) {
		return nil
	}
	// 判断竞技场解锁状态
	var opening msg.ArenaLockStatus
	funData, _ := conf.GetFunctionConfig(confData.FunctionId)
	if !CheckConfData(funData, confData.FunctionId, err) {
		return nil
	}
	// 检查竞技场解锁状态
	if !CheckConditions(r, funData.UnlockCondition, &msg.ErrorInfo{}) {
		opening = msg.ArenaLockStatus_ALS_LOCK
	} else {
		opening = msg.ArenaLockStatus_ALS_UNLOCK
	}
	var result = &msg.ArenaInfoResult{
		SeasonId:        arenaInfo.Id,
		SeasonBeginTime: arenaInfo.SeasonBeginTime,
		SeasonEndTime:   arenaInfo.SeasonEndTime,
		ConditionId:     funData.UnlockCondition,
		Opening:         opening,
	}
	if r.Arena != nil {
		if r.Arena.SeasonNumber == arenaInfo.SeasonNumber {
			// 赛季号相同,执行正常逻辑
			var redisArena = persistredis.Rank32
			arenaRankKey := key.GetArenaDefaultKey(r.ServerId, arenaInfo.Id)
			myRank, myScore, _ := redisArena.InfoById(arenaRankKey, r.Id)
			userInfoResult := buildArenaUserInfoResult(r, myRank, myScore, err)
			result.MyStage = userInfoResult.MyStage
			result.MyRank = userInfoResult.MyRank
		} else {
			r.Arena.KeepSuc = 0
			r.Arena.ArenaBattleNum = 0
			r.Arena.ArenaBuyNum = 0
		}
	}
	return result
}

// 构建竞技场用户返回信息
func buildArenaUserInfoResultById(playerId uint64, myRank int64, myScore int32, err *msg.ErrorInfo) *msg.ArenaUserInfoResult {
	if CheckPlayerIdIsRobot(playerId) {
		value, _ := buildRobotUserInfoResult(playerId, myRank, myScore, err)
		return value
	} else {
		var c redisdb.PlayerInfoCache
		r := c.Find(playerId)
		if r == nil {
			return nil
		}
		return buildArenaUserInfoResult(r, myRank, myScore, err)
	}
}

// 构建竞技场用户返回信息
func buildArenaUserInfoResult(r *msg.PlayerInfo, myRank int64, myScore int32, err *msg.ErrorInfo) *msg.ArenaUserInfoResult {
	// 根据积分查询段位Id
	myStageData := getArenaStageConfigByScore(myScore, err)
	if myStageData == nil {
		return nil
	}
	// 防守阵容里战力最高的英雄ID
	troopId := SelectTroopIdByFormation(r, msg.FormationType_FT_ArenaDef)
	troopConfig := GetTroop(r, troopId, err)
	var troopConfigId int32
	if troopConfig != nil {
		troopConfigId = troopConfig.ConfigId
	}
	return &msg.ArenaUserInfoResult{
		UserId:        r.Id,
		HeadUrl:       r.Icon,
		MyLevel:       int32(GetCount_Currency(r, msg.CurrencyId_CI_PlayerLevel)),
		MyName:        r.Name,
		DefPower:      r.DefPower,
		MyRank:        int32(myRank),
		MyPoint:       myScore,
		MyStage:       myStageData.Id,
		TroopConfigId: troopConfigId,
	}
}

// 构建竞技场机器人返回信息
func buildRobotUserInfoResult(robotId uint64, myRank int64, myScore int32, err *msg.ErrorInfo) (*msg.ArenaUserInfoResult, bool) {
	// 根据积分查询段位Id
	myStageData := getArenaStageConfigByScore(myScore, err)
	if myStageData == nil {
		return nil, false
	}
	// 机器人配置信息
	robotConfig, _ := conf.GetRobotConfig(int32(robotId))
	if !CheckConfData(robotConfig, int32(robotId), err) {
		return nil, false
	}
	return &msg.ArenaUserInfoResult{
		UserId:        robotId,
		HeadUrl:       robotConfig.Avatar,
		MyLevel:       robotConfig.Level,
		MyName:        strconv.FormatInt(int64(robotConfig.Name), 10),
		DefPower:      robotConfig.FormationForce,
		MyRank:        int32(myRank),
		MyPoint:       myScore,
		MyStage:       myStageData.Id,
		TroopConfigId: robotConfig.ShowTroop,
	}, true
}

// redis 里的find都是每次unmarshal，相当于每次都是新的，不是纯正的缓存
// 可以分方便的实现事务回滚
// 此处，需要延续性，，将其和playerInfo联系起来，，这样数据的生命期和有效期完全依托于playerInfo
// 有线程安全问题，但实际上没事，，业务逻辑做了seq的串行化
func MustGetPlayerInfoArena(r *msg.PlayerInfo) *msg.PlayerInfoArena {
	if r.Arena == nil {
		var c redisdb.PlayerInfoArenaCache
		r.Arena = c.Find(r.Id)
		if r.Arena == nil {
			// 初始化竞技场用户
			r.Arena = &msg.PlayerInfoArena{
				Id:           r.Id,
				SeasonId:     1,
				DayReward:    map[int64]msg.RewardStatus{},
				SeasonReward: map[int32]msg.RewardStatus{},
				BattleLog:    []*msg.ArenaBattleLog{},
			}
		}
	}
	if r.Arena.DayReward == nil {
		r.Arena.DayReward = make(map[int64]msg.RewardStatus)
	}
	if r.Arena.SeasonReward == nil {
		r.Arena.SeasonReward = make(map[int32]msg.RewardStatus)
	}
	if r.Arena.BattleLog == nil {
		r.Arena.BattleLog = make([]*msg.ArenaBattleLog, 0)
	}
	return r.Arena
}

// 查询战报
func GetArenaBattleRecord(r *msg.PlayerInfo, err *msg.ErrorInfo) (battleLogs []*msg.ArenaBattleLog) {
	rArena := MustGetPlayerInfoArena(r)
	if rArena == nil {
		return
	}
	for _, v := range rArena.BattleLog {
		if v == nil {
			continue
		}
		battleLogs = append(battleLogs, v)
	}
	rArena.BattleLog = battleLogs
	// 刷新战报查看时间
	r.Arena.LastBattleTime = time.Now().Unix()
	return
}

// 保存战报
func UpdateBattleRecord(r *msg.PlayerInfo, record *msg.ArenaBattleLog, err *msg.ErrorInfo) {
	rArena := MustGetPlayerInfoArena(r)
	if rArena == nil {
		return
	}
	confData, _ := conf.GetGlobalConfig(1)
	if !CheckConfData(confData, 1, err) {
		return
	}
	// 记录最大保存条数
	recordMax := confData.JuniorArenaRecordMax
	// 记录最大保存时间
	recordRemain := confData.JuniorArenaRecordRemain
	{
		// 判断战报是否过期
		currentTime := time.Now().Unix()
		var newBL []*msg.ArenaBattleLog
		for _, v := range rArena.BattleLog {
			keepTime := v.BattleTime + int64(recordRemain)*int64(time.Hour.Seconds())
			if keepTime > currentTime {
				newBL = append(newBL, v)
			}
		}
		rArena.BattleLog = newBL
	}
	{
		// 判断战报是否超限
		if len(rArena.BattleLog) >= int(recordMax) {
			rArena.BattleLog = rArena.BattleLog[1:len(rArena.BattleLog)]
		}
	}
	rArena.BattleLog = append(rArena.BattleLog, record)
}

// 获取竞技场挑战劵剩余购买次数/剩余免费挑战次数
func GetArenaBattleNum(r *msg.PlayerInfo, seasonId uint64, err *msg.ErrorInfo) (freeNum, buyNum int32) {
	rArena := MustGetPlayerInfoArena(r)
	if seasonId == 1 {
		// 赛季基础信息
		confData, _ := conf.GetArenaConfig(int32(seasonId))
		if !CheckConfData(confData, int32(seasonId), err) {
			return
		}
		// 挑战劵剩余购买次数
		buyNum = confData.Buy - rArena.ArenaBuyNum
		// 剩余免费挑战次数
		if r.Arena.ArenaBattleNum >= confData.Free {
			freeNum = 0
		} else {
			freeNum = confData.Free - r.Arena.ArenaBattleNum
		}
	}
	return
}

// 获取竞技场列表
func GetArenaInfos(r *msg.PlayerInfo, err *msg.ErrorInfo) (arenaInfos []*msg.ArenaInfoResult) {
	var c redisdb.ArenaSeasonInfoCache
	MustGetPlayerInfoArena(r)
	// 竞技场基础数据条数
	count := conf.CountArenaConfigMap()
	for i := 1; i <= count; i++ {
		value := c.Find(uint64(i))
		if value == nil {
			continue
		}
		m := buildArenaInfoResult(r, value, err)
		arenaInfos = append(arenaInfos, m)
	}
	return
}

// 获取竞技场主页
func GetArenaMainInfo(r *msg.PlayerInfo, seasonId uint64) ([]*msg.ObjInfo, *msg.ArenaInfoA) {
	res := &msg.ArenaInfoA{
		Err: &msg.ErrorInfo{},
	}
	var addedObjs []*msg.ObjInfo
	var c redisdb.ArenaSeasonInfoCache
	var redisArena = persistredis.Rank32
	seasonInfo := c.Find(seasonId)
	arenaRankKey := key.GetArenaDefaultKey(r.ServerId, seasonInfo.Id)
	// 检查竞技场开启状态
	if !CheckArenaOpenStatus(r, seasonInfo, res.Err) {
		return addedObjs, res
	}
	// 从缓存获取用户竞技场数据
	MustGetPlayerInfoArena(r)
	// 判断缓存是否有用户竞技场数据
	if r.Arena.SeasonNumber != seasonInfo.SeasonNumber {
		addedObjs = initArenaUserInfo(r, seasonInfo, res.Err)
		if res.Err.Id != 0 {
			return addedObjs, res
		}
	}
	// 获取竞技场排行榜信息
	myRank, myScore, _ := redisArena.InfoById(arenaRankKey, r.Id)
	myUserInfo := buildArenaUserInfoResult(r, myRank, myScore, res.Err)
	// 获取排名前50的用户信息
	var arenaUserInfos []*msg.ArenaUserInfoResult
	rankInfos, ok := redisArena.RangeByRank(arenaRankKey, 0, 49)
	if ok {
		for orderRank := 1; orderRank <= len(rankInfos); orderRank++ {
			rankInfo := rankInfos[orderRank-1]
			orderUserId := rankInfo.Id
			orderScore := rankInfo.Score
			// 获取竞技场排行榜用户信息
			var tmpErr msg.ErrorInfo
			value := buildArenaUserInfoResultById(orderUserId, int64(orderRank), orderScore, &tmpErr)
			if tmpErr.Id != 0 {
				continue
			}
			arenaUserInfos = append(arenaUserInfos, value)
		}
	}
	res.MyUserInfo = myUserInfo
	res.ArenaUserInfos = arenaUserInfos
	res.ServerId = r.ServerId
	res.FreeNum, _ = GetArenaBattleNum(r, seasonId, res.Err)
	res.SeasonBeginTime = seasonInfo.SeasonBeginTime
	res.SeasonEndTime = seasonInfo.SeasonEndTime
	return addedObjs, res
}

// 积分区间
type ScoreInterval struct {
	min float32
	max float32
}

var (
	ScoreIntervalArray = []ScoreInterval{
		{min: 1.2, max: 1.3},
		{min: 1.05, max: 1.2},
		{min: 0.95, max: 1.05},
		{min: 0.8, max: 0.95},
		{min: 0.65, max: 0.8},
		{min: 0.5, max: 0.65},
		{min: 0.35, max: 0.5},
		{min: 0.2, max: 0.35},
		{min: 0, max: 0.2},
	}
)

func GetMatchInfos(r *msg.PlayerInfo, seasonId uint64, err *msg.ErrorInfo) (matchInfos []*msg.ArenaUserInfoResult) {
	rArena := MustGetPlayerInfoArena(r)
	if rArena == nil {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "未找到用户竞技场信息"
		return
	}
	var c redisdb.ArenaSeasonInfoCache
	var redisArena = persistredis.Rank32
	seasonInfo := c.Find(seasonId)
	// 检查竞技场开启状态
	if !CheckArenaOpenStatus(r, seasonInfo, err) {
		return
	}
	// 特殊判断,新赛季未开始不允许匹配
	if seasonInfo.SeasonEndTime < seasonInfo.SeasonBeginTime {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "新赛季未开始,无法进行匹配"
		return
	}
	arenaRankKey := key.GetArenaDefaultKey(r.ServerId, seasonInfo.Id)
	myRank, myScore, _ := redisArena.InfoById(arenaRankKey, r.Id)
	// 获取竞技场排行榜信息
	myUserInfo := buildArenaUserInfoResult(r, myRank, myScore, err)
	tmpArray := ScoreIntervalArray
	// 已匹配玩家
	tmpMatch := make(map[uint64]uint64)
	for i := 0; i < ArenaMatchNumber; i++ {
		var match bool
		// 匹配玩家
		for index, v := range tmpArray {
			if index < i {
				continue
			}
			// 设置积分段
			start := float32(myUserInfo.MyPoint) * v.min
			stop := float32(myUserInfo.MyPoint) * v.max
			battle, ok := redisArena.RevRangeByScore(arenaRankKey, int32(start), int32(stop), ArenaMatchNumber)
			if !ok {
				continue
			}
			// 筛除自己和已匹配玩家
			var tempArray []uint64
			for i := 0; i < len(battle); i++ {
				playerId := battle[i].Id
				if r.Id == playerId {
					// 删除自己
					continue
				} else if _, ok := tmpMatch[playerId]; ok {
					// 已匹配过
					continue
				} else {
					tempArray = append(tempArray, playerId)
				}
			}
			// 匹配一个玩家
			if len(tempArray) > 0 {
				id := tempArray[rand.Intn(len(tempArray))]
				arenaRankKey := key.GetArenaDefaultKey(r.ServerId, seasonInfo.Id)
				myRank, myScore, _ := redisArena.InfoById(arenaRankKey, id)
				value := buildArenaUserInfoResultById(id, myRank, myScore, err)
				matchInfos = append(matchInfos, value)
				tmpMatch[id] = id
				match = true
			}
			if match {
				break
			}
		}
	}
	return
}

// 检查竞技场开启状态
func CheckArenaOpenStatus(r *msg.PlayerInfo, seasonInfo *msg.ArenaSeasonInfo, err *msg.ErrorInfo) bool {
	if seasonInfo == nil {
		return false
	}
	confData, _ := conf.GetArenaConfig(int32(seasonInfo.Id))
	if !CheckConfData(confData, int32(seasonInfo.Id), err) {
		return false
	}
	funData, _ := conf.GetFunctionConfig(confData.FunctionId)
	if !CheckConfData(funData, confData.FunctionId, err) {
		return false
	}
	// 检查竞技场解锁状态
	if !CheckConditions(r, funData.UnlockCondition, err) {
		return false
	}
	return true
}

// 检查竞技场挑战状态
func CheckArenaBattleStatus(r *msg.PlayerInfo, seasonId uint64, playerId uint64, err *msg.ErrorInfo) bool {
	var c redisdb.ArenaSeasonInfoCache
	seasonInfo := c.Find(seasonId)
	// 检查赛季开启状态
	if !CheckArenaOpenStatus(r, seasonInfo, err) {
		return false
	}
	// 检查赛季开启状态
	currentTime := time.Now().Unix()
	if seasonInfo.SeasonEndTime < seasonInfo.SeasonBeginTime {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "新赛季未开始,无法进行挑战"
		return false
	} else if currentTime < seasonInfo.SeasonBeginTime || currentTime > seasonInfo.SeasonEndTime {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "竞技场已结束,无法进行挑战"
		return false
	}
	var rArena = persistredis.Rank32
	arenaRankKey := key.GetArenaDefaultKey(r.ServerId, seasonInfo.Id)
	myRank, myScore, _ := rArena.InfoById(arenaRankKey, r.Id)
	// 检查竞技场挑战方是否存在
	battlePlayerInfo := buildArenaUserInfoResultById(playerId, myRank, myScore, err)
	if battlePlayerInfo == nil {
		return false
	}
	return true
}

func ArenaBattle(r *msg.PlayerInfo, battleId uint64, battleR *msg.PlayerInfo, seasonId uint64, battleResult msg.BattleResult, err *msg.ErrorInfo) (addedObjs []*msg.ObjInfo, arenaBattleInfoResult *msg.ArenaBattleInfoResult) {
	// 赛季基础信息
	confData, _ := conf.GetArenaConfig(int32(seasonId))
	if !CheckConfData(confData, int32(seasonId), err) {
		return
	}
	var rArena = persistredis.Rank32
	var rSeason redisdb.ArenaSeasonInfoCache
	seasonInfo := rSeason.Find(seasonId)
	arenaRankKey := key.GetArenaDefaultKey(r.ServerId, seasonInfo.Id)
	myRank, myScore, _ := rArena.InfoById(arenaRankKey, r.Id)
	myOldInfo := buildArenaUserInfoResult(r, myRank, myScore, err)
	otherRank, otherScore, _ := rArena.InfoById(arenaRankKey, battleId)
	otherOldInfo := buildArenaUserInfoResultById(battleId, otherRank, otherScore, err)
	// 段位基础信息
	myStageData, _ := conf.GetArenaRankStageConfig(myOldInfo.MyStage)
	if !CheckConfData(myStageData, myOldInfo.MyStage, err) {
		return
	}
	rankData := getArenaRankingConfigByRank(myOldInfo.MyRank, err)
	if rankData == nil {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "error is rank ="+myOldInfo.String()
		return
	}
	MustGetPlayerInfoArena(r)
	// 积分预期变化
	attPd := 1.0 / (1.0 + math.Pow(10, (float64(otherOldInfo.MyPoint)-float64(myOldInfo.MyPoint))/400.0))
	// 胜利积分
	sucScore := float32(rankData.ConstantK) * rankData.ConstantA0 * (1.0 - float32(attPd)) * rankData.ConstantA1
	sucScore = float32(math.Round(float64(sucScore)))
	defPd := 1.0 / (1.0 + math.Pow(10, (float64(myOldInfo.MyPoint)-float64(otherOldInfo.MyPoint))/400.0))
	// 失败积分
	failScore := float32(rankData.ConstantK) * rankData.ConstantA0 * (0 - float32(defPd)) * rankData.ConstantA2
	failScore = float32(math.Round(float64(failScore)))

	var myChangeScore float32
	var myBattleResult msg.BattleResult
	var myKeepSuc int32
	var myKeepSucScore int32
	var otherChangeScore float32
	var otherBattleResult msg.BattleResult
	if battleResult == msg.BattleResult_BR_Success {
		// 战斗结果
		myBattleResult = msg.BattleResult_BR_Success
		otherBattleResult = msg.BattleResult_BR_Fail
		// 更新连胜次数
		r.Arena.KeepSuc = r.Arena.KeepSuc + 1
		// 连胜积分加成
		globalData, _ := conf.GetGlobalConfig(1)
		if !CheckConfData(globalData, 1, err) {
			return
		}
		if r.Arena.KeepSuc >= globalData.JuniorArenaWinStreak {
			sucScore = sucScore + float32(myStageData.ConsistentWin)
			// 连胜增加积分
			myKeepSucScore = myStageData.ConsistentWin
		}
		// 发送大礼包
		tempAdded, _ := Drop(r, myStageData.RewardWin, err)
		if err.Id != 0 {
			return
		}
		addedObjs = append(addedObjs, tempAdded...)
		// 胜利
		myChangeScore = sucScore
		otherChangeScore = failScore
		myKeepSuc = r.Arena.KeepSuc
	} else {
		// 战斗结果
		myBattleResult = msg.BattleResult_BR_Fail
		otherBattleResult = msg.BattleResult_BR_Success
		// 失败
		myChangeScore = failScore
		otherChangeScore = sucScore
		// 清空连胜次数
		r.Arena.KeepSuc = 0
	}
	// 设置新积分
	myOldInfo.MyPoint = myOldInfo.MyPoint + int32(myChangeScore)
	otherOldInfo.MyPoint = otherOldInfo.MyPoint + int32(otherChangeScore)

	rArena.Add(arenaRankKey, myOldInfo.UserId, myOldInfo.MyPoint)
	rArena.Add(arenaRankKey, otherOldInfo.UserId, otherOldInfo.MyPoint)
	// 查询排行榜
	myNewRank, _, _ := rArena.InfoById(arenaRankKey, myOldInfo.UserId)
	// 查询段位
	myNewStageData := getArenaStageConfigByScore(myOldInfo.MyPoint, err)
	otherNewStageData := getArenaStageConfigByScore(otherOldInfo.MyPoint, err)
	if err.Id != 0 {
		return
	}
	if myNewStageData == nil || otherNewStageData == nil {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "竞技场积分未查询到对应的段位基础信息"
		return
	}
	otherNewRank, _, _ := rArena.InfoById(arenaRankKey, otherOldInfo.UserId)
	arenaBattleInfoResult = &msg.ArenaBattleInfoResult{
		AttInfo: &msg.ArenaBattleUserResult{
			Uinfo:        myOldInfo,
			ChangeScore:  int32(myChangeScore),
			ChangeRank:   myOldInfo.MyRank - int32(myNewRank),
			NewRanking:   int32(myNewRank),
			ChangeStage:  myNewStageData.Id - myOldInfo.MyStage,
			NewStageing:  myNewStageData.Id,
			KeepSuc:      myKeepSuc,
			KeepSucScore: myKeepSucScore,
		},
		DefInfo: &msg.ArenaBattleUserResult{
			Uinfo:       otherOldInfo,
			ChangeScore: int32(otherChangeScore),
			ChangeRank:  otherOldInfo.MyRank - int32(otherNewRank),
			NewRanking:  int32(otherNewRank),
			ChangeStage: otherNewStageData.Id - otherOldInfo.MyStage,
			NewStageing: otherNewStageData.Id,
		},
	}
	// 记录战斗记录
	{
		// 战报ID
		battleUuid, _ := uuid.GenerateUUID()
		// 进攻方战报
		myBattleLog := &msg.ArenaBattleLog{
			Id:            myOldInfo.UserId,
			SeasonId:      int32(seasonId),
			BattleUuid:    battleUuid,
			BattleType:    msg.BattleMessageStatus_BATTLE_ATT,
			BattleUserId:  otherOldInfo.UserId,
			BattleName:    otherOldInfo.MyName,
			BattleHeadUrl: otherOldInfo.HeadUrl,
			BattleLevel:   otherOldInfo.MyLevel,
			BattlePoint:   otherOldInfo.MyPoint,
			BattlePower:   otherOldInfo.DefPower,
			BattleStage:   otherOldInfo.MyStage,
			UpdatePoint:   int32(myChangeScore),
			BattleTime:    time.Now().Unix(),
			BattleResult:  myBattleResult,
		}
		UpdateBattleRecord(r, myBattleLog, err)
		// 防守方战报
		if !CheckPlayerIdIsRobot(battleId) {
			// 非机器人记录战报
			otherBattleLog := &msg.ArenaBattleLog{
				Id:            otherOldInfo.UserId,
				SeasonId:      int32(seasonId),
				BattleUuid:    battleUuid,
				BattleType:    msg.BattleMessageStatus_BATTLE_DEF,
				BattleUserId:  myOldInfo.UserId,
				BattleName:    myOldInfo.MyName,
				BattleHeadUrl: myOldInfo.HeadUrl,
				BattleLevel:   myOldInfo.MyLevel,
				BattlePoint:   myOldInfo.MyPoint,
				BattlePower:   myOldInfo.DefPower,
				BattleStage:   myOldInfo.MyStage,
				UpdatePoint:   int32(otherChangeScore),
				BattleTime:    time.Now().Unix(),
				BattleResult:  otherBattleResult,
			}
			UpdateBattleRecord(battleR, otherBattleLog, err)
			// 非机器人推送战报红点
			RTMNotifyRedPoint(battleR.Id, msg.RedPointType_RPT_TriggerArenaBeChallenge, 0, 0)
		}
	}
	return
}

// 竞技场挑战消耗
func CostArenaBattleTicket(r *msg.PlayerInfo, seasonId uint64, err *msg.ErrorInfo) (consumes []*msg.StaticObjInfo) {
	rArena := MustGetPlayerInfoArena(r)
	if rArena == nil {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "未找到用户竞技场信息"
		return
	}
	// 赛季基础信息
	confData, _ := conf.GetArenaConfig(int32(seasonId))
	if !CheckConfData(confData, int32(seasonId), err) {
		return
	}
	if rArena.ArenaBattleNum < confData.Free {
		// 免费挑战次数
		rArena.ArenaBattleNum = rArena.ArenaBattleNum + 1
		return
	}
	consumes, _ = JsonToPb_StaticObjs(confData.Cost, confData, confData.Id, err)
	if err.Id != 0 {
		return
	}
	// 扣除挑战劵
	result := CheckConsumes(r, consumes, err)
	if !result {
		return
	}
	consumesResult := SubConsumes(r, consumes, err)
	if !consumesResult {
		return
	}
	return
}

// 购买竞技场挑战劵
func BuyArenaBattleTicket(r *msg.PlayerInfo, seasonId uint64, costNum int32, err *msg.ErrorInfo) (addedObjs []*msg.ObjInfo) {
	if costNum < 1 {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "购买个数不在限制范围"
		return
	}
	rArena := MustGetPlayerInfoArena(r)
	if rArena == nil {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "未找到用户竞技场信息"
		return
	}
	// 赛季基础信息
	confData, _ := conf.GetArenaConfig(int32(seasonId))
	if !CheckConfData(confData, int32(seasonId), err) {
		return
	}
	// 检查购买次数
	if rArena.ArenaBuyNum+costNum > confData.Buy {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "购买次数超过限制"
		return
	}
	// 累计购买次数从0开始
	currentBuyNum := rArena.ArenaBuyNum + 1
	// TODO 就这样吧,最大购买次数也就10次
	for i := 0; i < int(costNum); i++ {
		checkObjs := GetStepPriceConsume(confData.BuyCost, currentBuyNum, err)
		if err.Id != 0 {
			return
		}
		// 扣除钻石
		result := CheckConsume(r, checkObjs, err)
		if !result {
			return
		}
		objs, _ := JsonToPb_StaticObjs(confData.Cost, confData, confData.Id, err)
		if err.Id != 0 {
			return
		}
		// 添加挑战劵
		addedObjs, _ = AddStaticObjs(r, objs, err)
		if err.Id != 0 {
			return
		}
		// 增加购买次数
		rArena.ArenaBuyNum = rArena.ArenaBuyNum + 1
	}
	return
}

// 刷新竞技场赛季红点
func ClearArenaSeasonRedPoint(r *msg.PlayerInfo, notify bool) {

	_, rp := FindRedPointAnyway(r, &msg.RedPoint{
		Type: msg.RedPointType_RPT_ArenaSeason,
	})

	// 不用做判断,点进来就是消除红点
	rp.Count = 0

	if notify {
		NotifyRedPoint(r, rp)
	}
}

// 刷新竞技场战报红点
func RefreshArenaLogRedPoint(r *msg.PlayerInfo, notify bool) {
	rArena := MustGetPlayerInfoArena(r)

	_, rp := FindRedPointAnyway(r, &msg.RedPoint{
		Type: msg.RedPointType_RPT_BattleLog,
	})

	var curCount = int32(0)
	if rArena.LastBattleTime != 0 {
		for _, v := range rArena.BattleLog {
			if rArena.LastBattleTime < v.BattleTime {
				// 新战报
				curCount++
			}
			if curCount > 0 {
				break
			}
		}
	}
	if rp.Count != curCount {
		rp.Count = curCount
	}

	if notify {
		NotifyRedPoint(r, rp)
	}
}
