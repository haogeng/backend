package activity

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestUnmarshalActivityTime(t *testing.T) {
	s := `{
	"0": {
		"one": "1",
		"delay": "0",
		"duration": "88"
	}
	}`
	a, err := UnmarshalActivityTime(s)
	assert.NotEqual(t, nil, err)
	assert.Equal(t, (*ActivityTime)(nil), a)

	s = `{
	"0": {
		"one": 1,
		"delay": 0,
		"duration": 88
	}
	}`
	a, err = UnmarshalActivityTime(s)
	assert.Equal(t, nil, err)
	assert.Equal(t, int32(88), a.DurationDay)

	s = `{
		"one": 1,
		"delay": 0,
		"duration": 88
	}`
	a, err = UnmarshalActivityTime(s)
	assert.NotEqual(t, nil, err)
	assert.Equal(t, (*ActivityTime)(nil), a)
}
