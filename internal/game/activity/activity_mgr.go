package activity

import (
	"fmt"
	jsoniter "github.com/json-iterator/go"
	"server/internal/log"
	"server/internal/util"
	"server/pkg/gen/conf"
	"server/pkg/gen/rawdata"
	"sync"
)

//暂时只支持REGISTER，，
//TODO 其他

// 活动模块
// 活动时间
// 形如：
/*
s = `{
	"0": {
		"one": 1,
		"delay": 0,
		"duration": 88
	}
	}`
*/
type ActivityTime struct {
	// 所有时间类型里，该值必须填数字类型1
	IntOne int32 `json:"one"`

	//= REGISTER
	// 创角后天数
	DelayDay int32 `json:"delay"`
	// 激活后持续天数
	DurationDay int32 `json:"duration"`

	// TODO 其他
}

func UnmarshalActivityTime(j string) (at *ActivityTime, err error) {
	var json = jsoniter.ConfigCompatibleWithStandardLibrary
	data := make(map[string]*ActivityTime)
	// 先尝试数组
	err = json.Unmarshal(util.StringToBytes(j), &data)
	if err != nil {
		// 再拿单体试试
		at = &ActivityTime{}
		err = json.Unmarshal(util.StringToBytes(j), at)
		if err != nil {
			at = nil
			return
		}

		data = make(map[string]*ActivityTime)
		data["0"] = at
	}

	// 统一按数组处理
	at, ok := data["0"]
	if !ok || at == nil || at.IntOne != 1 {
		at = nil
		err = fmt.Errorf("load activity json err")
	}

	return
}

type Unit struct {
	Id       int32
	TimeInfo *ActivityTime
	Config   *rawdata.ActivityConfig
}

type Mgr struct {
	mutex sync.RWMutex
	// 需要藏起来
	data map[int32]*Unit
}

var once sync.Once
var mgr *Mgr

// 单例
func GetMgr() *Mgr {
	once.Do(func() {
		if mgr == nil {
			mgr = &Mgr{
				data: make(map[int32]*Unit),
			}
		}
	})

	return mgr
}

// reload in pmConf
func (m *Mgr) ReloadOnConfChange() {
	log.Info("activity.Mgr ReloadOnConfChange")

	m.mutex.Lock()
	defer m.mutex.Unlock()

	// TODO 细化
	// 先简单的copy过来，后续根据时间类型扩展做细化

	m.data = make(map[int32]*Unit)
	configs := conf.GetActivityConfigConf().ActivityConfigs
	for id, v := range configs {
		timeInfo, err := UnmarshalActivityTime(v.TimeConfig)
		if err != nil {
			log.Errorf("ActivityConfig(%d) load err:%v", id, err)
			continue
		} else {
			log.Debugf("activity(%d) timeInfo: %v", id, timeInfo)
		}

		m.data[id] = &Unit{
			Id:       id,
			Config:   v,
			TimeInfo: timeInfo,
		}
	}

	//log.Debugf("ActivityMgr: \n%+v", m.data)
}

func (m *Mgr) GetAllUnits() map[int32]*Unit {
	var ret map[int32]*Unit

	m.mutex.RLock()
	ret = m.data
	m.mutex.RUnlock()

	return ret
}

func (m *Mgr) GetUnitsByTimeType(timeType string) map[int32]*Unit {
	ret := make(map[int32]*Unit)

	m.mutex.RLock()
	for k, v := range m.data {
		if v.Config.TimeType == timeType {
			ret[k] = v
		}
	}
	m.mutex.RUnlock()

	return ret
}

func (m *Mgr) GetUnit(id int32) *Unit {
	var ret *Unit

	m.mutex.RLock()
	if d, ok := m.data[id]; ok {
		ret = d
	}
	m.mutex.RUnlock()

	return ret
}
