package game

import (
	"server/internal/gconst"
	"server/internal/global_info"
	key2 "server/internal/key"
	"server/pkg/gen/msg"
	"server/pkg/persistredis"
)

// 队伍战力（竞技场防守阵容）非完全排行榜
// 用途：通过队伍战力匹配并拉取玩家数据用
// 只更新最近2个服的玩家战力
// 拉取的时候，只获得上一个服的战力排行。如果上一个服没有，则用当前服(到目前为止只有一个服)。
func TryUpdateTeamPowerRankOfRecentServer(r *msg.PlayerInfo, defTeamHeroNums int32, defPower int32) {
	serverId := global_info.GetGlobalInfo().UnsafeServerId()
	// 只更新当前服和上一个服的玩家
	if r.ServerId != serverId && r.ServerId != serverId-1 {
		// do nth
		return
	}

	// 更新防守整容战力
	key := key2.GetTeamPowerKey(r.ServerId)

	if defTeamHeroNums == gconst.MaxHeroNumInTeam {
		// 满员，更新
		persistredis.Rank32.Add(key, r.Id, defPower)
	} else {
		// 否则删除
		persistredis.Rank32.Remove(key, r.Id)
	}
}

// 获得一个特定的服的战力榜的键值
// 用于异界迷宫等作为玩家的数据
func GetRecentTeamPowerRankKeyName() string {
	serverId := global_info.GetGlobalInfo().UnsafeServerId()
	if serverId-1 > 0 {
		serverId = serverId - 1
	}
	return key2.GetTeamPowerKey(serverId)
}
