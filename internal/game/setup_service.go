package game

import (
	"server/internal/gconst"
	"server/pkg/dlock"
	"server/pkg/gen/conf"
	"server/pkg/gen/msg"
	"server/pkg/gen/rawdata"
	"server/pkg/gen_redis/redisdb"
)

// 计算修改用户名字的价格
func ComputerModifyPriceByName(r *msg.PlayerInfo, err *msg.ErrorInfo) (priceObjInfo *msg.StaticObjInfo) {
	globalConfig, _ := conf.GetGlobalConfig(1)
	if !CheckConfData(globalConfig, 1, err) {
		return
	}
	// 修改次数从1开始计算
	priceObjInfo = GetStepPriceConsume(globalConfig.ChangeUserNameCostId, r.ModifyNameNum+1, err)
	return priceObjInfo
}

// 获取头像列表
func SelectPlayerIconInfos(r *msg.PlayerInfo) (iconInfos map[int32]msg.IconStatus) {
	iconInfos = map[int32]msg.IconStatus{}
	conf.RangeUserPortraitsMap(func(key int32, val *rawdata.UserPortraits) bool {
		if r.Icon == key {
			// 使用中
			iconInfos[key] = msg.IconStatus_IS_INUSE
		} else if CheckConditions(r, val.Codition, &msg.ErrorInfo{}) {
			// 已解锁
			iconInfos[key] = msg.IconStatus_IS_UNLOCK
		} else {
			// 未解锁
			iconInfos[key] = msg.IconStatus_IS_LOCK
		}
		return true
	})
	return
}

// 修改用户头像
func ModifyPlayerIcon(r *msg.PlayerInfo, icon int32, err *msg.ErrorInfo) {
	iconInfos := SelectPlayerIconInfos(r)
	val, ok := iconInfos[icon]
	if !ok {
		err.Id, err.DebugMsg = 2323, "icon not found"
		return
	}
	if val == msg.IconStatus_IS_LOCK {
		err.Id, err.DebugMsg = 2323, "icon is lock"
		return
	}
	r.Icon = icon
	return
}

func ModifyFriendRecommend(rId uint64, closeRecommend bool, err *msg.ErrorInfo) {
	rKeyLock := keyOfPlayerFriendInfos(rId)
	lockValue, ok := dlock.AcquireLockWithTimeoutX(rKeyLock, gconst.DLockTtl, gconst.DLockGetTimeout)
	if !ok {
		return
	}
	defer dlock.ReleaseLockX(rKeyLock, lockValue)

	var c redisdb.PlayerInfoCache
	r := c.Find(rId)
	if r == nil {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, gconst.ErrNoUser.Error()
		return
	}

	rFriend := MustGetPlayerInfoFriend(r)
	rFriend.CloseRecommend = closeRecommend

	MustUpdatePlayerInfoFriend(rId, rFriend)
}
