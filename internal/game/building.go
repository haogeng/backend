package game

import (
	"fmt"
	"server/internal/util/time3"
	"server/pkg/gen/conf"
	"server/pkg/gen/msg"
	"server/pkg/gen/rawdata"
)

func DealBuildingsForClient(m map[int32]*msg.BuildingInfo) {
	nowTime := time3.Now().Unix()
	for _, v := range m {
		DealBuildingForClient(v, nowTime)
	}
}

func DealBuildingForClient(m *msg.BuildingInfo, nowTime int64) {
	buildingConf, _ := conf.GetBuildingConfig(m.Id)
	if !CheckConfData(buildingConf, m.Id, nil) {
		return
	}

	c, _ := GetLevelConf(buildingConf, m.Level, nil)
	if !CheckConfData(buildingConf, m.Level, nil) {
		return
	}

	//startTime := time3.Now().Unix()
	if m.State == msg.BuildingState_UPGRADING {
		m.PassedTime = int32(nowTime - m.StartTime)
		m.UpgradeTotalTime = c.Time
	}
}

// 检查建筑队列
func CheckBuildingQueue(building *msg.BuildingInfo, r *msg.PlayerInfo, err *msg.ErrorInfo) bool {
	if r.BuildQueueCount >= r.MaxBuildQueueCount {
		err.Id, err.DebugMsg = msg.Ecode_BUILD_LEVELUP_QUEUE_FULL, "building queue upto max"
		return false
	}
	return true
}

// 会覆盖err
func CheckBuildingExist(building *msg.BuildingInfo, bconf *rawdata.BuildingConfig, err *msg.ErrorInfo) bool {
	// ORDER!!!
	if building == nil {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "data err"
		return false
	}

	if !CheckConfData(bconf, building.Id, err) {
		return false
	}

	return true
}

func CheckBuildingState(building *msg.BuildingInfo, needState msg.BuildingState, err *msg.ErrorInfo) bool {
	if building.State != needState {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "state err"
		return false
	}
	return true
}

//建筑队列检查

func CheckBuildCondition(building *msg.BuildingInfo, bconf *rawdata.BuildingConfig, r *msg.PlayerInfo, err *msg.ErrorInfo) bool {
	levelConf, ok := GetLevelConf(bconf, building.Level, err)
	if !ok {
		return false
	}

	return CheckConditions(r, levelConf.UnlockCondition, err)
}

func CalcBuildConsume(building *msg.BuildingInfo, bconf *rawdata.BuildingConfig, err *msg.ErrorInfo) (objs []*msg.StaticObjInfo) {
	levelConf, _ := GetLevelConf(bconf, building.Level, err)
	if err.Id != 0 {
		return
	}

	if !CheckConfData(levelConf, building.Level, err) {
		return
	}

	if err.Id != 0 {
		return
	}

	objs, _ = JsonToPb_StaticObjs(levelConf.Cost, levelConf, levelConf.Id, err)
	if err.Id != 0 {
		return
	}
	return
}

func CheckBuildConsume(consumeObjs []*msg.StaticObjInfo, r *msg.PlayerInfo, err *msg.ErrorInfo) bool {
	return checkAndSubBuildConsume(consumeObjs, r, err, false)
}
func SubBuildConsume(consumeObjs []*msg.StaticObjInfo, r *msg.PlayerInfo, err *msg.ErrorInfo) bool {
	return checkAndSubBuildConsume(consumeObjs, r, err, true)
}

func checkAndSubBuildConsume(consumeObjs []*msg.StaticObjInfo, r *msg.PlayerInfo, err *msg.ErrorInfo, doSub bool) bool {
	CheckConsumes(r, consumeObjs, err)
	if err.Id != 0 {
		return false
	}

	if doSub {
		SubConsumes(r, consumeObjs, err)
		if err.Id != 0 {
			return false
		}
	}

	return true
}

func GetLevelConfByBuilding(building *msg.BuildingInfo, err *msg.ErrorInfo) (levelConf *rawdata.BuildingLevelConfig, ok bool) {
	if building == nil {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "not find building"
		return
	}

	buildingConf, _ := conf.GetBuildingConfig(building.Id)
	return GetLevelConf(buildingConf, building.Level, err)
}

// levelup: 0->1 用0去查
func GetLevelConf(bconf *rawdata.BuildingConfig, level int32, err *msg.ErrorInfo) (levelConf *rawdata.BuildingLevelConfig, ok bool) {
	levelConfId, ok := bconf.LevelId[level]
	if ok {
		levelConf, ok = conf.GetBuildingLevelConfig(levelConfId)
	}

	if !ok {
		if err != nil {
			err.Id, err.DebugMsg = msg.Ecode_INVALID, "conf err"
		}
	}

	return
}

func GetBuilding(r *msg.PlayerInfo, buildingId int32) (ret *msg.BuildingInfo) {
	if r.Buildings == nil {
		return
	}

	if v, ok := r.Buildings[buildingId]; ok {
		ret = v
		return
	}
	return
}

func GetBuildIdByType(t int32) int32 {
	return mapBuildingTypeToId[t]
}

// 根据类型获得id
var mapBuildingTypeToId = map[int32]int32{
	1:  1,
	5:  2,
	6:  3,
	7:  4,
	8:  5,
	9:  6,
	10: 7,
	11: 8,
	12: 9,
	13: 10,
	14: 11,
	15: 12,
	16: 13,
	17: 14,
	18: 15,
	19: 16,
	20: 17,
}

//func GetBuildingIdByType(t int32) int32 {
//	return mapBuildingTypeToId[t]
//}
func GetBuildingByType(r *msg.PlayerInfo, t int32) *msg.BuildingInfo {
	id, ok := mapBuildingTypeToId[t]
	if !ok {
		panic(fmt.Errorf("Not find building by type %d", t))
		return nil
	}

	ret, ok := r.Buildings[id]
	if !ok {
		return nil
	}

	return ret
}

// 条件： 0->1 1->n
// 会触发一些后续时间
func OnBuildingUpgraded(r *msg.PlayerInfo, building *msg.BuildingInfo) {
	confData, _ := conf.GetBuildingConfig(building.Id)
	//
	if confData == nil {
		panic("building conf is nil")
	}
	t := msg.BuildingType(confData.Type)
	switch t {
	case msg.BuildingType_BT_QuestHall:
		// from 0 -> 1
		if building.Level == 1 {
			RefreshBounty(r, true, false, false)
			NotifyBountyInfo(r)
		}

		break
	case msg.BuildingType_BT_LevyHouse:
		// from 0 ->777777 1
		if building.Level == 1 {
			LevyHouseRefresh(r)
		}
		break
	case msg.BuildingType_BT_CityHall:
		TryTriggerNewStore(r, true)
		break
	case msg.BuildingType_BT_Academy:
		if building.Level == 1 {
			//AcademyRefresh(r)
		}
		break
	}
}
