package game

import (
	"server/internal/log"
	"server/pkg/gen/msg"
)

type ActivityDetailDummyOpt msg.ActivityData

func (d *ActivityDetailDummyOpt) InitDetail(r *msg.PlayerInfo) {
	log.Errorf("unknown activity opt: %v", d)
}

func (d *ActivityDetailDummyOpt) ShouldGetDetail(r *msg.PlayerInfo) (detail interface{}) {
	log.Errorf("unknown activity opt: %v", d)
	return nil
}

func (d *ActivityDetailDummyOpt) GetConfig(err *msg.ErrorInfo) (confData interface{}) {
	log.Errorf("unknown activity opt: %v", d)
	return nil
}

// 根据时间刷新一下状态
func (d *ActivityDetailDummyOpt) TryUpdateActivityByTime(r *msg.PlayerInfo) {
	log.Errorf("unknown activity opt: %v", d)
}

// 判断根据完成即结束去尝试关闭
func (d *ActivityDetailDummyOpt) TryCloseByDirectEnd(r *msg.PlayerInfo) {
	log.Errorf("unknown activity opt: %v", d)
}

// 尝试结束的时候，补发未领取的邮件
func (d *ActivityDetailDummyOpt) TrySendSupplyRewards(r *msg.PlayerInfo) {
	log.Errorf("unknown activity opt: %v", d)
}

func (d *ActivityDetailDummyOpt) DoCleanOnClosed(r *msg.PlayerInfo) {
	log.Errorf("unknown activity opt: %v", d)
}
func (d *ActivityDetailDummyOpt) Close(r *msg.PlayerInfo) {
	log.Errorf("unknown activity opt: %v", d)
}
func (d *ActivityDetailDummyOpt) TryClearWhenClosed(r *msg.PlayerInfo) {
	log.Errorf("unknown activity opt: %v", d)
}
