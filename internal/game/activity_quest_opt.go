package game

import (
	"server/internal/game/activity"
	"server/internal/gconst"
	"server/internal/log"
	"server/internal/util"
	"server/internal/util/time3"
	"server/pkg/gen/conf"
	"server/pkg/gen/msg"
	"server/pkg/gen/rawdata"
)

type ActivityDetailQuestOpt msg.ActivityData

func (d *ActivityDetailQuestOpt) InitDetail(r *msg.PlayerInfo) {
	if d.Quest != nil {
		return
	}
	d.Quest = &msg.ActivityDailyQuestDetail{
		FinishedTaskIds: make(map[int32]int32),
	}
	d.refreshDayNo(r)
}

func (d *ActivityDetailQuestOpt) ShouldGetDetail(r *msg.PlayerInfo) (detail interface{}) {
	u := activity.GetMgr().GetUnit(d.Id)
	if u.Config.Type != int32(msg.ActivityType_AT_DailyQuest) {
		return nil
	}

	if d.Quest == nil {
		d.Quest = &msg.ActivityDailyQuestDetail{
			FinishedTaskIds: make(map[int32]int32),
		}
	}

	if d.Quest.FinishedTaskIds == nil {
		d.Quest.FinishedTaskIds = make(map[int32]int32)
	}

	return d.Quest
}

func (d *ActivityDetailQuestOpt) GetConfig(err *msg.ErrorInfo) (confData interface{}) {
	au := GetActivityConfig(d.Id, err)
	if err.Id != 0 {
		return nil
	}
	if au.Config.Type != int32(msg.ActivityType_AT_DailyQuest) {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "activity type err"
		return nil
	}

	questConf, _ := conf.GetActivityQuestConfig(au.Config.Data)
	if !CheckConfData(questConf, au.Config.Data, err) {
		return nil
	}

	return questConf
}

// 根据时间刷新一下状态
func (d *ActivityDetailQuestOpt) TryUpdateActivityByTime(r *msg.PlayerInfo) {
	if d.Quest == nil || len(d.Quest.FinishedTaskIds) == 0 {
		return
	}
	if d.Status == msg.ActivityStatus_AS_Closed {
		return
	}

	d.refreshDayNo(r)
}

func (d *ActivityDetailQuestOpt) refreshDayNo(r *msg.PlayerInfo) {
	oldDayNo := d.Quest.UnlockDayNo
	newDayNo := time3.GetCrossDays(d.OpenedTimestamp, time3.Now().Unix()) + 1
	if newDayNo > oldDayNo {
		d.Quest.UnlockDayNo = newDayNo
		log.Debugf("activity quest id:%d, oldDay:%d, newDay:%d", d.Id, oldDayNo, newDayNo)

		// red point! 不需要实时推送，一般伴随登录或每日重置
		for i := oldDayNo + 1; i <= newDayNo; i++ {
			AddRedPointForMyself(r, msg.RedPointType_RPT_Activity, d.Id, i, false)
		}
	}
}

// 判断根据完成即结束去尝试关闭
func (d *ActivityDetailQuestOpt) TryCloseByDirectEnd(r *msg.PlayerInfo) {
	if d.Quest == nil || len(d.Quest.FinishedTaskIds) == 0 {
		return
	}

	questConf, ok := d.GetConfig(&msg.ErrorInfo{}).(*rawdata.ActivityQuestConfig)
	if !ok {
		return
	}
	if questConf == nil {
		return
	}
	total := 0
	for _, v := range questConf.Quest {
		total += len(v.Value)
	}
	if len(d.Quest.FinishedTaskIds) >= total {
		d.Close(r)
		return
	}
}

// 尝试结束的时候，补发未领取的邮件
func (d *ActivityDetailQuestOpt) TrySendSupplyRewards(r *msg.PlayerInfo) {
	var rewards []*msg.StaticObjInfo
	// fill supply rewards
	if d.Quest != nil {
		// 初始化detail, 保证map存在
		d.ShouldGetDetail(r)

		questConf, ok := d.GetConfig(&msg.ErrorInfo{}).(*rawdata.ActivityQuestConfig)
		if !ok {
			return
		}
		if questConf == nil {
			return
		}

		for _, v := range questConf.Quest {
			for _, taskId := range v.Value {
				tempRewards := PrepareDoTaskAward(r, taskId, &msg.ErrorInfo{})
				if len(tempRewards) > 0 {
					rewards = append(rewards, tempRewards...)
					d.Quest.FinishedTaskIds[taskId] = 1
				}
			}
		}
	}

	if len(rewards) > 0 {
		NewSimpleMailFromRewards(r, gconst.ActivityReimburseMail, rewards)
	}
}

func (d *ActivityDetailQuestOpt) Close(r *msg.PlayerInfo) {
	if d.Status == msg.ActivityStatus_AS_Closed {
		return
	}
	d.Status = msg.ActivityStatus_AS_Closed
	if d.Quest != nil && d.Quest.FinishedTaskIds != nil {
		for taskId, _ := range d.Quest.FinishedTaskIds {
			delTask(r, taskId)
		}
	}
	// 因为要下推客户端数据，当时不重置sign, 在登录的时候，刷新closed，清空
	//d.Quest = nil
}

func (d *ActivityDetailQuestOpt) TryClearWhenClosed(r *msg.PlayerInfo) {
	d.Quest = nil
}

// 得到特定活动里的任务所在的天数，从1开始
// 找不到返回-1
func GetActivityDayInQuestDetail(ad *msg.ActivityData, taskId int32, err *msg.ErrorInfo) int32 {
	questConf := ActivityOpt(ad).GetConfig(err).(*rawdata.ActivityQuestConfig)
	if err.Id != 0 {
		return -1
	}

	for dayNo, taskIdList := range questConf.Quest {
		if util.InArray32(taskIdList.Value, taskId) {
			return dayNo
		}
	}

	err.Id, err.DebugMsg = msg.Ecode_INVALID, "not found day no for task"
	return -1
}
