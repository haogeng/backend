package game

import (
	"server/internal/log"
	"server/pkg/gen/conf"
	"server/pkg/gen/msg"
	"server/pkg/gen/rawdata"
)

type TFuncCondition func(r *msg.PlayerInfo, c *rawdata.ConditionConfig, err *msg.ErrorInfo) bool

var condProcessors map[msg.ConditionType]TFuncCondition

func init() {
	condProcessors = make(map[msg.ConditionType]TFuncCondition)

	// init tables
	condProcessors[msg.ConditionType_CT_PlayerLevel] = cfPlayerLevel
	condProcessors[msg.ConditionType_CT_BuildingLevel] = cfBuildingLevel
	condProcessors[msg.ConditionType_CT_OwnProsperity] = cfOwnProsperity
	condProcessors[msg.ConditionType_CT_TypeBuildingCount] = cfTypeBuildingCount
	condProcessors[msg.ConditionType_CT_MainChapter] = cWarCopyChapter
	condProcessors[msg.ConditionType_CT_TechnologyLevel] = cfTechLevel
	condProcessors[msg.ConditionType_CT_OwnHero] = cfOwnHero

}

func cfBuildingLevel(r *msg.PlayerInfo, c *rawdata.ConditionConfig, err *msg.ErrorInfo) bool {
	buildType := c.Param1
	buildLevel := c.Param2

	b := GetBuildingByType(r, buildType)
	if b == nil {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "cfBuildingLevel"
		return false
	}

	if b.Level < buildLevel {
		//err.Id, err.DebugMsg = msg.Ecode_INVALID, "cfBuildingLevel"

		err.Id, err.DebugMsg = msg.Ecode_UNENOUGH_BUILDING_LEVEL, "cfBuildingLevel"
		// 前置建筑名	所需等级
		err.IntParams = []int64{int64(GetBuildIdByType(buildType)), int64(buildLevel)}

		err.InternalErrId = InternalErrCondBuildingLevel
		err.InternalParam1, err.InternalParam2 = int64(GetBuildIdByType(buildType)), int64(buildLevel)
		return false
	}

	return true
}

func cfPlayerLevel(r *msg.PlayerInfo, c *rawdata.ConditionConfig, err *msg.ErrorInfo) bool {
	need := int64(c.Param1)
	if GetCount_Currency(r, msg.CurrencyId_CI_PlayerLevel) < need {
		err.Id, err.DebugMsg = msg.Ecode_UNENOUGH_ITEM_OR_CURRENCY, "cfPlayerLevel fail"
		fakeId := GetFakeId(msg.ObjType_OT_Currency, int32(msg.CurrencyId_CI_PlayerLevel))
		err.IntParams = []int64{int64(fakeId)}

		return false
	}
	return true
}

func cfOwnProsperity(r *msg.PlayerInfo, c *rawdata.ConditionConfig, err *msg.ErrorInfo) bool {
	need := int64(c.Param1)
	if GetCount_Currency(r, msg.CurrencyId_CI_Prosperity) < need {
		err.Id, err.DebugMsg = msg.Ecode_UNENOUGH_ITEM_OR_CURRENCY, "cfOwnProsperity fail"
		fakeId := GetFakeId(msg.ObjType_OT_Currency, int32(msg.CurrencyId_CI_Prosperity))
		err.IntParams = []int64{int64(fakeId)}

		return false
	}
	return true
}

func cWarCopyChapter(r *msg.PlayerInfo, c *rawdata.ConditionConfig, err *msg.ErrorInfo) bool {
	// 判断主线副本章节通关条件
	r2 := MustGetPlayerInfoExt(r)
	// 章节信息
	confData, _ := conf.GetWarInstanceMapConfig(c.Param1)
	if !CheckConfData(confData, c.Param1, err) {
		return false
	}
	for _, dValue := range r2.WarDiffInfos {
		for _, cValue := range dValue.WarCopyInfos {
			strongHoldInfo, ok := cValue.StrongholdInfos[confData.FinalBattleStronghold]
			if !ok {
				// 据点未开启
				continue
			}
			if strongHoldInfo.StrongholdStatus == msg.StrongholdStatus_SS_OPEN {
				// 已完成
				return true
			}
		}
	}
	return false
}

func cfTypeBuildingCount(r *msg.PlayerInfo, c *rawdata.ConditionConfig, err *msg.ErrorInfo) bool {
	buildType := c.Param1
	buildLevel := c.Param2
	//buildCount := c.Param3 // no use???

	b := GetBuildingByType(r, buildType)
	if b == nil {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "cfBuildingLevel"
		return false
	}

	if b.Level < buildLevel {
		//err.Id, err.DebugMsg = msg.Ecode_INVALID, "cfBuildingLevel"

		err.Id, err.DebugMsg = msg.Ecode_UNENOUGH_BUILDING_LEVEL, "cfBuildingLevel"
		// 前置建筑名	所需等级
		err.IntParams = []int64{int64(GetBuildIdByType(buildType)), int64(buildLevel)}

		err.InternalErrId = InternalErrCondBuildingLevel
		err.InternalParam1, err.InternalParam2 = int64(GetBuildIdByType(buildType)), int64(buildLevel)
		return false
	}

	return false
}

func CheckConditions(r *msg.PlayerInfo, condIds []int32, err *msg.ErrorInfo) bool {
	for i := 0; i < len(condIds); i++ {
		if !CheckCondition(r, condIds[i], err) {
			return false
		}
	}

	return true
}
func CheckCondition(r *msg.PlayerInfo, condId int32, err *msg.ErrorInfo) bool {
	// 无条件
	if condId == 0 {
		return true
	}

	condConf, _ := conf.GetConditionConfig(condId)
	if !CheckConfData(condConf, condId, err) {
		return false
	}

	t := msg.ConditionType(condConf.EnumType)
	f, ok := condProcessors[t]
	if !ok {
		log.Errorf("not support cond:%v", t)
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "not support cond"
		return false
	}

	return f(r, condConf, err)
}

func cfTechLevel(r *msg.PlayerInfo, c *rawdata.ConditionConfig, err *msg.ErrorInfo) bool {
	techId := c.Param1
	techLevel := c.Param2

	b := GetTechInfo(r, techId)
	if b == nil {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "cfTechLevel"
		return false
	}

	if b.Level < techLevel {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "cfTechLevel"
		return false
	}

	return true
}

func cfOwnHero(r *msg.PlayerInfo, c *rawdata.ConditionConfig, err *msg.ErrorInfo) bool {
	heroId := c.Param1

	if _, ok := r.TroopBook[heroId]; ok {
		return true
	}
	return false
}
