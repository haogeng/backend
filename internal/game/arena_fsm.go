package game

import (
	"bitbucket.org/funplus/golib/time2"
	"server/internal/key"
	"server/internal/log"
	"server/internal/util/time3"
	"server/pkg/gen/conf"
	"server/pkg/gen/msg"
	"server/pkg/gen/rawdata"
	"server/pkg/gen_redis/redisdb"
	"server/pkg/persistredis"
	"sync"
	"time"
)

var arenaFSMInfos = make(map[uint64]*ArenaFSMInfo)
var arenaCache redisdb.ArenaSeasonInfoCache
var arenaRedis = persistredis.Rank32

// 竞技场状态机
type ArenaFSMInfo struct {
	*FSM
}

// 实例化竞技场
func NewArenaFSMInfo(initState FSMState) *ArenaFSMInfo {
	return &ArenaFSMInfo{
		FSM: newFSM(initState),
	}
}

var (
	// 服务器开始Id，保险从0开始
	kMinServerId = int32(0)
)

var (
	ArenaInit   = FSMState(0)   // 初始化
	ArenaBegin  = FSMState(1)   // 开始
	ArenaDayIng = FSMState(2)   // 战斗
	ArenaEnd    = FSMState(3)   // 结束
	ArenaStop   = FSMState(110) // 停止

	ArenaInitEvent   = FSMEvent("init arena")
	ArenaBeginEvent  = FSMEvent("begin arena")
	ArenaDayIngEvent = FSMEvent("battle arena")
	ArenaEndEvent    = FSMEvent("end arena")
	ArenaStopEvent   = FSMEvent("stop arena")

	ArenaInitHandler = FSMHandler(func(curMaxServerId int32) FSMState {
		log.Info("init arena")
		seasonId := ArenaConfigId
		confData, _ := conf.GetArenaConfig(int32(seasonId))
		if !CheckConfData(confData, int32(seasonId), nil) {
			return ArenaStop
		}
		// 竞技场信息
		seasonInfo := arenaCache.Find(uint64(confData.Id))
		if seasonInfo == nil {
			var beginTime, endTime int64
			// 初始化竞技场时间周期
			if !confData.IsLaunch {
				beginTime, endTime = ComputerSeasonTimeInfo(confData)
			}

			// 初始化竞技场
			seasonInfo = &msg.ArenaSeasonInfo{
				Id:              uint64(confData.Id),
				ArenaType:       msg.ArenaTypeStatus_ATS_ARENA,
				SeasonBeginTime: beginTime,
				SeasonEndTime:   endTime,
				SeasonNumber:    1,
				DayRewards:      []int64{},
			}
			arenaId := arenaCache.MustCreate(seasonInfo.Id, seasonInfo)
			if arenaId <= 0 {
				return ArenaStop
			}
		}
		// 最大服务器ID
		maxServerId := curMaxServerId + ArenaServerIdIncrement
		var beginServerId int32
		for beginServerId = kMinServerId; beginServerId < maxServerId; beginServerId++ {
			log.Info("arena start rank -", beginServerId)
			// 初始化排行榜
			initRankInfoByRobot(beginServerId, seasonInfo.Id, confData.Robot, &msg.ErrorInfo{})
		}
		return ArenaDayIng
	})

	ArenaBeginHandler = FSMHandler(func(curMaxServerId int32) FSMState {
		log.Info("reset begin arena")
		seasonId := ArenaConfigId
		// 设置新赛季时间周期
		confData, _ := conf.GetArenaConfig(int32(seasonId))
		if !CheckConfData(confData, int32(seasonId), nil) {
			return ArenaStop
		}
		// 赛季信息
		seasonInfo := arenaCache.Find(uint64(seasonId))
		if seasonInfo == nil {
			// 赛季结束
			return ArenaStop
		}
		// 赛季号+1
		seasonInfo.SeasonNumber = seasonInfo.SeasonNumber + 1
		// 设置新赛季结束时间
		_, endTime := ComputerSeasonTimeInfo(confData)
		seasonInfo.SeasonEndTime = endTime
		// 判断赛季是否正常
		if seasonInfo.SeasonEndTime < time.Now().Unix() {
			panic("竞技场新赛季启动失败")
		}
		// 更新
		arenaCache.MustUpdate(seasonInfo.Id, seasonInfo)
		// 最大服务器ID
		maxServerId := curMaxServerId + ArenaServerIdIncrement
		var beginServerId int32
		for beginServerId = kMinServerId; beginServerId < maxServerId; beginServerId++ {
			log.Info("arena reset rank -", beginServerId)
			// 删除排行榜
			arenaRankKey := key.GetArenaDefaultKey(beginServerId, seasonInfo.Id)
			arenaRedis.DelAll(arenaRankKey)
			// 初始化排行榜
			initRankInfoByRobot(beginServerId, seasonInfo.Id, confData.Robot, &msg.ErrorInfo{})
		}
		return ArenaDayIng
	})
	ArenaDayIngHandler = FSMHandler(func(curMaxServerId int32) FSMState {
		log.Info("day reward arena")
		seasonId := ArenaConfigId
		// 赛季信息
		seasonInfo := arenaCache.Find(uint64(seasonId))
		if seasonInfo == nil {
			// 赛季结束
			return ArenaStop
		}
		// 当前时间节点
		curTime := time.Now().Unix()
		// 保存每日奖励节点
		seasonInfo.DayRewards = append(seasonInfo.DayRewards, curTime)
		// 更新
		arenaCache.MustUpdate(seasonInfo.Id, seasonInfo)
		// 最大服务器ID
		maxServerId := curMaxServerId + ArenaServerIdIncrement
		var beginServerId int32
		// 备份每日排行榜
		for beginServerId = kMinServerId; beginServerId < maxServerId; beginServerId++ {
			log.Info("arena back day rank -", beginServerId)
			arenaRankKey := key.GetArenaDefaultKey(beginServerId, seasonInfo.Id)
			arenaDayRankKey := key.GetArenaDayKeyByMillis(beginServerId, seasonInfo.Id, curTime)
			arenaRedis.CopyAll(arenaRankKey, arenaDayRankKey)
			// 过期时间=赛季周期*2
			dayRankKeyExpire := time.Duration((seasonInfo.SeasonEndTime-seasonInfo.SeasonBeginTime)*2) * time.Millisecond
			// 设置备份Key过期时间
			arenaRedis.Expire(arenaDayRankKey, dayRankKeyExpire)
		}
		return ArenaDayIng
	})
	ArenaEndHandler = FSMHandler(func(curMaxServerId int32) FSMState {
		log.Info("arena season reward")
		seasonId := ArenaConfigId
		// 赛季信息
		seasonInfo := arenaCache.Find(uint64(seasonId))
		if seasonInfo == nil {
			// 赛季结束
			return ArenaStop
		}
		// 竞技场基础信息
		confData, _ := conf.GetArenaConfig(int32(seasonId))
		if !CheckConfData(confData, int32(seasonId), nil) {
			return ArenaStop
		}
		// 最大服务器ID
		maxServerId := curMaxServerId + ArenaServerIdIncrement
		var beginServerId int32
		// 备份赛季排行榜
		for beginServerId = kMinServerId; beginServerId < maxServerId; beginServerId++ {
			log.Info("arena back season rank -", beginServerId)
			arenaRankKey := key.GetArenaDefaultKey(beginServerId, seasonInfo.Id)
			arenaSeasonRankKey := key.GetArenaSeasonKey(beginServerId, seasonInfo.Id, seasonInfo.SeasonNumber)
			arenaRedis.CopyAll(arenaRankKey, arenaSeasonRankKey)
			// 过期时间=赛季周期*2
			seasonRankKeyExpire := time.Duration(seasonInfo.SeasonEndTime-seasonInfo.SeasonBeginTime) * time.Millisecond
			// 设置备份Key过期时间
			arenaRedis.Expire(arenaSeasonRankKey, seasonRankKeyExpire)
		}
		// 设置新赛季开始时间
		beginTime, _ := ComputerSeasonTimeInfo(confData)
		seasonInfo.SeasonBeginTime = beginTime
		// 更新
		arenaCache.MustUpdate(seasonInfo.Id, seasonInfo)
		return ArenaBegin
	})
)

type FSMState int32                                 // 状态
type FSMEvent string                                // 事件
type FSMHandler func(curMaxServerId int32) FSMState // 处理方法,返回新的状态
type Handlers map[FSMState]map[FSMEvent]FSMHandler  // 事件集合

// 有限状态机
type FSM struct {
	mu       sync.Mutex
	state    FSMState
	handlers Handlers
}

// 获取当前状态
func (f *FSM) getState() FSMState {
	return f.state
}

// 设置当前状态
func (f *FSM) setState(state FSMState) {
	f.state = state
}

// 某状态添加时间处理方法
func (f *FSM) AddHandler(state FSMState, event FSMEvent, handler FSMHandler) *FSM {
	if _, ok := f.handlers[state]; !ok {
		f.handlers[state] = make(map[FSMEvent]FSMHandler)
	}
	if _, ok := f.handlers[state][event]; !ok {
		f.handlers[state][event] = handler
	}
	return f
}

// 启动竞技场
func StartArenaSeasonInfo() {
	// 当前最大服务器ID
	var c redisdb.GlobalInfoCache
	r := c.Find(1)
	curMaxServerId := r.CurServerId
	// 初始化状态机
	arenaFSMInfo := InitArenaFSMStateInfo()
	for true {
		state := arenaFSMInfo.CallEvent(curMaxServerId)
		if state == ArenaStop {
			// 竞技场停止
			break
		}
	}
}

// 执行任务
func (f *FSM) CallEvent(curMaxServerId int32) FSMState {
	// 计算竞技场当前状态
	event, delay := computerSeasonCurrentStatus()
	if event == ArenaStopEvent {
		// 竞技场停止
		log.Info("状态从 [", f.getState(), "] 变成 [", ArenaStop, "]")
		return ArenaStop
	}
	// 休眠
	time.Sleep(delay)
	// 处理事件
	events := f.handlers[f.getState()]
	if events == nil {
		return f.getState()
	}
	if fn, ok := events[event]; ok {
		oldState := f.getState()
		f.setState(fn(curMaxServerId))
		newState := f.getState()
		log.Info("状态从 [", oldState, "] 变成 [", newState, "]")
	}
	return f.getState()
}

// 实例化FSM
func newFSM(initState FSMState) *FSM {
	return &FSM{
		state:    initState,
		handlers: Handlers{},
	}
}

// 初始化竞技场状态
func InitArenaFSMStateInfo() *ArenaFSMInfo {
	ret := NewArenaFSMInfo(ArenaInit)

	ret.AddHandler(ArenaInit, ArenaInitEvent, ArenaInitHandler)
	ret.AddHandler(ArenaInit, ArenaBeginEvent, ArenaBeginHandler)
	ret.AddHandler(ArenaInit, ArenaDayIngEvent, ArenaDayIngHandler)
	ret.AddHandler(ArenaInit, ArenaEndEvent, ArenaEndHandler)

	ret.AddHandler(ArenaBegin, ArenaBeginEvent, ArenaBeginHandler)
	ret.AddHandler(ArenaBegin, ArenaDayIngEvent, ArenaDayIngHandler)
	ret.AddHandler(ArenaBegin, ArenaEndEvent, ArenaEndHandler)

	ret.AddHandler(ArenaDayIng, ArenaDayIngEvent, ArenaDayIngHandler)
	ret.AddHandler(ArenaDayIng, ArenaEndEvent, ArenaEndHandler)

	ret.AddHandler(ArenaEnd, ArenaEndEvent, ArenaEndHandler)
	ret.AddHandler(ArenaEnd, ArenaBeginEvent, ArenaBeginHandler)

	arenaFSMInfos[ArenaConfigId] = ret

	return ret
}

// 检查竞技场每日奖励赛季的发放状态
func checkTodayRewardStatus(seasonInfo *msg.ArenaSeasonInfo) bool {
	curTime := time.Now()
	for _, v := range seasonInfo.DayRewards {
		if time2.IsToday(v, curTime) {
			return true
		}
	}
	return false
}

// 计算赛季周期的当前状态
func computerSeasonCurrentStatus() (FSMEvent, time.Duration) {
	seasonId := uint64(ArenaConfigId)
	value := arenaCache.Find(seasonId)
	if value == nil {
		return ArenaInitEvent, 0
	}
	confData, _ := conf.GetArenaConfig(int32(seasonId))
	if !CheckConfData(confData, int32(seasonId), nil) {
		return ArenaStopEvent, -1
	}
	curTime := time.Now()
	currentTime := curTime.Unix()
	if value.SeasonBeginTime > value.SeasonEndTime {
		// 新赛季未开始,判断老赛季的状态
		if currentTime < value.SeasonEndTime {
			// 赛季进行中,判断赛季结束时间是否为今天
			if time2.IsToday(value.SeasonEndTime, curTime) {
				if checkTodayRewardStatus(value) {
					endTime := time.Unix(value.SeasonEndTime, 0)
					return ArenaEndEvent, endTime.Sub(curTime)
				} else {
					return ArenaDayIngEvent, 0
				}
			}
			// 计算赛季天的结束时间
			endOfDay := time3.EndOfDay(curTime)
			seasonOfDay := time3.TimeOfHoursLater(endOfDay, int(confData.Daily[0]))
			return ArenaDayIngEvent, seasonOfDay.Sub(curTime)
		} else {
			// 赛季未开始
			beginTime := time.Unix(value.SeasonBeginTime, 0)
			return ArenaBeginEvent, beginTime.Sub(curTime)
		}
	}
	// 老赛季已结束,判断新赛季的状态
	if currentTime < value.SeasonBeginTime {
		// 赛季未开始
		beginTime := time.Unix(value.SeasonBeginTime, 0)
		return ArenaBeginEvent, beginTime.Sub(curTime)
	} else if currentTime > value.SeasonBeginTime && currentTime < value.SeasonEndTime {
		// 赛季进行中,判断赛季结束时间是否为今天
		if time2.IsToday(value.SeasonEndTime, curTime) {
			if checkTodayRewardStatus(value) {
				endTime := time.Unix(value.SeasonEndTime, 0)
				return ArenaEndEvent, endTime.Sub(curTime)
			} else {
				return ArenaDayIngEvent, 0
			}
		}
		// 计算赛季天的结束时间
		endOfDay := time3.EndOfDay(curTime)
		seasonOfDay := time3.TimeOfHoursLater(endOfDay, int(confData.Daily[0]))
		return ArenaDayIngEvent, seasonOfDay.Sub(curTime)
	} else {
		// 赛季已结束
		if checkTodayRewardStatus(value) {
			return ArenaEndEvent, 0
		} else {
			return ArenaDayIngEvent, 0
		}
	}
}

// 计算赛季周期时间
func ComputerSeasonTimeInfo(confData *rawdata.ArenaConfig) (beginTimeSecond int64, endTimeSecond int64) {
	curTime := time.Now()
	// 计算开始时间
	nextOfDay := confData.Opening[1]
	openHourOfDay := confData.Opening[2]
	beginTime := time.Date(curTime.Year(), curTime.Month(), curTime.Day()+int(nextOfDay), int(openHourOfDay), 0, 0, 0, curTime.Location())
	beginTimeSecond = beginTime.Unix()

	// 计算结束时间
	keepOfDay := confData.Ending[1]
	endHourOfDay := confData.Ending[2]
	endTIme := time.Date(beginTime.Year(), beginTime.Month(), beginTime.Day()+int(keepOfDay), int(endHourOfDay), 0, 0, 0, beginTime.Location())
	endTimeSecond = endTIme.Unix()
	return
}
