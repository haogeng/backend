package game

import (
	"fmt"
	"server/pkg/gen/msg"
	"server/pkg/gen_redis/redisdb"
)

// redis 里的find都是每次unmarshal，相当于每次都是新的，不是纯正的缓存
// 可以分方便的实现事务回滚
// 此处，需要延续性，，将其和playerInfo联系起来，，这样数据的生命期和有效期完全依托于playerInfo
// 有线程安全问题，但实际上没事，，业务逻辑做了seq的串行化
func MustGetPlayerInfoExt(r *msg.PlayerInfo) *msg.PlayerInfoExt {
	if r.Ext != nil {
		return r.Ext
	}

	var c redisdb.PlayerInfoExtCache
	r2 := c.Find(r.Id)
	//log.Infof("PlayerInfoExt %+v", r2)

	if r2 == nil {
		r2 = &msg.PlayerInfoExt{}
		id := c.MustCreate(r.Id, r2)
		if id != r.Id {
			panic(fmt.Errorf("u%d GetPlayerInfoExt fail", r2.Id))
		}
	}
	makeSureStoreExist(r2)
	r.Ext = r2

	return r2
}

// ExtTroop
func ExtTroop(r *msg.PlayerInfo) *msg.PlayerInfoTroop {
	if r.Troop != nil {
		return r.Troop
	}

	var c redisdb.PlayerInfoTroopCache
	r2 := c.Find(r.Id)
	//log.Infof("PlayerInfoTroop %+v", r2)

	if r2 == nil {
		r2 = &msg.PlayerInfoTroop{}
		id := c.MustCreate(r.Id, r2)
		if id != r.Id {
			panic(fmt.Errorf("u%d GetPlayerInfoTroop fail", r2.Id))
		}
	}

	// 方便外界访问
	if r2.Troops == nil {
		r2.Troops = make(map[int32]*msg.ObjInfo)
	}
	if r2.TroopsExt == nil {
		r2.TroopsExt = make(map[int32]*msg.TroopExtInfo)
	}

	r.Troop = r2

	return r2
}

// MustGetPlayerInfoEquip
func ExtEquip(r *msg.PlayerInfo) *msg.PlayerInfoEquip {
	if r.Equip != nil {
		return r.Equip
	}

	var c redisdb.PlayerInfoEquipCache
	r2 := c.Find(r.Id)
	//log.Infof("PlayerInfoEquip %+v", r2)

	if r2 == nil {
		r2 = &msg.PlayerInfoEquip{}
		id := c.MustCreate(r.Id, r2)
		if id != r.Id {
			panic(fmt.Errorf("u%d GetPlayerInfoEquip fail", r2.Id))
		}
	}

	// 方便外界访问
	if r2.Equips == nil {
		r2.Equips = make(map[int32]*msg.ObjInfo)
	}

	r.Equip = r2

	return r2
}

// 修改r，携带客户端专有信息
// 应该是调用链里的最后一个函数，会清空不要的字段
func WithClientDataInFinal(r *msg.PlayerInfo, myself bool, formationType msg.FormationType) {
	r.CTroops = GetTroopsByFlag(r, 0)
	r.CTroopsExt = GetTroopsExtByFlag(r, 0)
	r.CEquips = GetEquipsByFlag(r, 0)

	ext := MustGetPlayerInfoExt(r)
	if ext != nil {
		r.CTroopRelationships = ext.TroopRelationships
	}
	if r.CTroopRelationships == nil {
		r.CTroopRelationships = make(map[int32]*msg.TroopRelationship)
	}

	r.Troop = nil
	r.Equip = nil
	r.Ext = nil
	r.Arena = nil
	r.Maze = nil

	// 英雄限制:查询自己可以返回所有英雄,查询非自己只能返回指定阵容的英雄
	// TODO 暂时不管货币限制:查询自己可以返回所有货币,查询非自己只能返回指定类型的货币
	if !myself {
		r.Friend = nil
		r.Buildings = nil
		r.Items = nil
		r.Tasks = nil
		r.Bounties = nil
		r.LevyHouses = nil
		r.TechInfos = nil
		r.RedPointAry = nil
		r.TreasureRecvedData = nil
		r.TroopBook = nil
		r.SurveyHistory = nil

		// 英雄信息发送指定阵容信息
		if formationType != msg.FormationType_FT_Count {
			troopInfos := GetTroopInfosForClient(r, r.Formations[int32(formationType)].TroopIds)
			if len(troopInfos.Troops) == 0 {
				troopInfos = GetTroopsIdByMaxPower(r, 5)
			}
			r.CTroops = troopInfos.Troops
			r.CTroopsExt = troopInfos.TroopExts
			r.CEquips = troopInfos.Equips
		} else {
			// TODO 测试阶段先不处理,等待客户端新增参数后再开启限制
			//r.CTroops = nil
			//r.CTroopsExt = nil
			//r.CEquips = nil
			//r.CWarDiffInfos = nil
		}
		// 货币信息发送非私密信息
		//var currency = map[int32]int64{}
		//// 等级
		//playerLevel := int32(msg.CurrencyId_CI_PlayerLevel)
		//currency[playerLevel] = r.Currency[playerLevel]
		//r.Currency = currency

	} else {
		// 英雄信息发送指阵容信息
		if formationType != msg.FormationType_FT_Count {
			troopInfos := GetTroopInfosForClient(r, r.Formations[int32(formationType)].TroopIds)
			if len(troopInfos.Troops) == 0 {
				troopInfos = GetTroopsIdByMaxPower(r, 5)
			}
			r.CTroops = troopInfos.Troops
			r.CTroopsExt = troopInfos.TroopExts
			r.CEquips = troopInfos.Equips
		}
	}

	//r.Clan = nil

	// 玩家登录会把活动一起发下去，便于客户端做红点处理
	//r.Activity = nil
	//TODO other to be clear for client
}
