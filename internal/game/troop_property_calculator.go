package game

import (
	"math"
	"server/internal/log"
	"server/internal/util"
	"server/pkg/gen/conf"
	"server/pkg/gen/msg"
	"server/pkg/gen/rawdata"
)

// TODO：考虑羁绊技能对战力的影响

type enhanceUnit struct {
	index int32
	// 前端此处是表达式，主要用于战斗，后端一个具体值即可
	value float32
}

// ref HeroPropertyCalculator.lua
type HeroPropertyCalculator struct {
	tBase map[int32]float32
	// enhance 需要是个数组，，会多重
	tEnhance map[int32]float32
	// 先放到列表里，最后再合到tEnhance里
	enhanceUnits []*enhanceUnit

	r     *msg.PlayerInfo
	troop *msg.ObjInfo

	troopConfig     *rawdata.TroopConfig
	troopStarConfig *rawdata.TroopStarConfig

	FightPower int32
}

// Note：只在一次handle过程有用
// 如果有数据变化，需要重新new对象计算，不可重用
func NewHeroPropertyCalculator(r *msg.PlayerInfo, troop *msg.ObjInfo) *HeroPropertyCalculator {
	h := &HeroPropertyCalculator{
		tBase:    make(map[int32]float32),
		tEnhance: make(map[int32]float32),
	}
	h.r = r
	h.troop = troop

	h.troopConfig, _ = conf.GetTroopConfig(h.troop.ConfigId)
	err := &msg.ErrorInfo{}
	if !CheckConfData(h.troopConfig, h.troop.ConfigId, err) {
		log.Error(err)
		return nil
	}

	starConfigId := h.troopConfig.StarCfgID[h.troop.Star]
	h.troopStarConfig, _ = conf.GetTroopStarConfig(starConfigId)
	if !CheckConfData(h.troopStarConfig, starConfigId, err) {
		log.Error(err)
		return nil
	}

	return h
}

func (h *HeroPropertyCalculator) CalcWithFightPower() int32 {
	if h.FightPower > 0 {
		return h.FightPower
	}
	h.calc()
	h.calcFightPower()
	return h.FightPower
}

func (h *HeroPropertyCalculator) GetBase(index int32) float32 {
	d, _ := h.tBase[index]
	return d
}
func (h *HeroPropertyCalculator) SetBase(index int32, v float32) {
	h.tBase[index] = v
}

func (h *HeroPropertyCalculator) GetEnhance(index int32) float32 {
	d, _ := h.tEnhance[index]
	return d
}
func (h *HeroPropertyCalculator) SetEnhance(index int32, v float32) {
	h.tEnhance[index] = v
}

func (h *HeroPropertyCalculator) GetConfigProperty(index int32) float32 {
	d, _ := h.troopConfig.Properties[int32(index)]
	return d
}

// 不可重入函数
func (h *HeroPropertyCalculator) calc() {
	h.calcFormula3()
	h.calcFormula1()
	h.calcFormula2()
	h.calcFormula4()
	h.calcFormula5()
	h.calcPropertyModifiers()
	h._calcResult()
}

func (h *HeroPropertyCalculator) calcFightPower() int32 {
	base := h.GetBase(IndexHp)*N0_05 + h.GetBase(IndexHpAddPerS)*N4_5 + h.GetBase(IndexDefM)*N1 +
		h.GetBase(IndexDefP)*N1 +
		(h.GetBase(IndexAtt)*N1)*h.GetBase(IndexAttSpd)/N1000 +
		h.GetBase(IndexAttRng) + h.GetBase(IndexMoveSpd)*N100

	a := (N1+N2*h.GetBase(IndexAntiBlockLevel)/N1000)*
		(N1+N3*h.GetBase(IndexHitLevel)/N1000)*
		(N1+N4*h.GetBase(IndexBounceDamageRatio)/N1000)*
		(N1+N8*h.GetBase(IndexAddDamageLevel)/N5000)*
		(N1+N4*h.GetBase(IndexEffectHitLevel)/N1000)*
		(N1+N8*h.GetBase(IndexAddDamageLevelP)/N5000)*
		(N1+N8*h.GetBase(IndexAddDamageLevelM)/N5000)*
		(N2*h.GetBase(IndexCritLevel)/N1000*(N1_5+N3*h.GetBase(IndexCritStrength)/N2000)+N1-N2*h.GetBase(IndexCritLevel)/N1000) - N1
	fight := float32(math.Pow(float64(a), float64(N0_5))) *
		(h.GetBase(IndexAtt)) *
		(N1_5)

	b := (N1+N3*h.GetBase(IndexDodgeLevel)/N1000)*(N1+N4*h.GetBase(IndexBloodRatio)/N1000)*
		(N1+N8*h.GetBase(IndexReliefDamageLevel)/N5000)*
		(N1+N4*h.GetBase(IndexAntiEffectHitLevel)/N1000)*
		(N1+N2*h.GetBase(IndexAntiCritLevel)/N1000)*
		(N1+N8*h.GetBase(IndexReliefDamageLevelP)/N5000)*
		(N1+N8*h.GetBase(IndexReliefDamageLevelM)/N5000)*
		(N2*h.GetBase(IndexBlockLevel)/N1000*(N1+N0_1+N3*h.GetBase(IndexBlockStrength)/N2000)+N1-N2*h.GetBase(IndexBlockLevel)/N1000) - N1
	relief := float32(math.Pow(float64(b), float64(N0_5))) *
		(h.GetBase(IndexHp) + h.GetBase(IndexHpAddPerS)) * N0_1

	h.FightPower = int32(base + fight + relief)
	return h.FightPower
}

func (h *HeroPropertyCalculator) calcFormula5() {
	for _, index := range Formula5 {
		v := h.GetConfigProperty(index) / N1000
		h.SetBase(index, v)
	}
}

func (h *HeroPropertyCalculator) calcFormula4() {
	for _, index := range Formula4 {
		v := h.GetConfigProperty(index)
		h.SetBase(index, v)
	}
}

func (h *HeroPropertyCalculator) calcFormula3() {
	for _, index := range Formula3 {
		v := h._calcFormula3(index)
		h.SetBase(index, v)
	}
}

func (h *HeroPropertyCalculator) calcFormula1() {
	for _, index := range Formula1 {
		v := h._calcFormula1(index)
		h.SetBase(index, v)
	}
}

func (h *HeroPropertyCalculator) calcFormula2() {
	for _, indices := range Formula2 {
		index := indices[0]
		rateIndex := indices[1]

		v := h._calcFormula2(index, h.GetBase(rateIndex))
		h.SetBase(index, v)
	}
}

func (h *HeroPropertyCalculator) _calcFormula2(index int32, rate float32) float32 {
	base := h.GetConfigProperty(index)
	lvup, _ := h.troopConfig.LevelUpCfg[int32(index)]
	starProperty, _ := h.troopStarConfig.StarPropertyCfg[int32(index)]
	//starlevelProp := float32(0)

	return (base + float32(h.troop.Level-1)*lvup + starProperty)
}

func (h *HeroPropertyCalculator) _calcFormula1(index int32) float32 {
	advProp := h.GetConfigProperty(index)
	starProperty, _ := h.troopStarConfig.StarPropertyCfg[int32(index)]

	return advProp + starProperty
}

func (h *HeroPropertyCalculator) _calcFormula3(index int32) float32 {
	advProp := h.GetConfigProperty(index)
	starProperty, _ := h.troopStarConfig.StarPropertyCfg[int32(index)]

	return (advProp + starProperty) / N100
}

func (h *HeroPropertyCalculator) IsPercent(index int32) bool {
	return util.InArray32(Formula3, index)
}

func (h *HeroPropertyCalculator) getEquipEnhanceUnitsByEquip(e *msg.ObjInfo) []*enhanceUnit {
	var ret []*enhanceUnit
	//-- 基础属性
	c, _ := conf.GetEquipConfig(e.ConfigId)
	if c == nil {
		return ret
	}
	for id, baseValue := range c.BaseAttrs {
		finalValue := baseValue
		// 每升一级增量
		deltaValue, ok := c.LevelUpAttrs[id]
		if ok && e.Level >= 2 {
			finalValue += deltaValue * (e.Level - 1)
		}

		u := &enhanceUnit{
			index: id,
			value: float32(finalValue),
		}
		ret = append(ret, u)
	}

	//-- 副属性
	for pairIndex, needLevel := range c.SideAttrsLevel {
		if needLevel <= e.Level {
			// 此处索引取巧了，小心
			if pairIndex < len(e.Pairs) {
				pairAtt := e.Pairs[pairIndex]
				u := &enhanceUnit{
					index: pairAtt.Key,
					value: float32(pairAtt.Value),
				}
				ret = append(ret, u)
			}
		} else {
			break
		}
	}
	return ret
}

func (h *HeroPropertyCalculator) getEquipEnhanceUnits() []*enhanceUnit {
	var ret []*enhanceUnit

	// configId -> count
	suitMap := make(map[int32]int32)

	ext := MustGetTroopExt(h.r, h.troop.Id)
	for _, equipId := range ext.Equips {
		if equipId == 0 {
			continue
		}
		e := GetEquip(h.r, equipId, &msg.ErrorInfo{})
		if e == nil {
			continue
		}

		ec, _ := conf.GetEquipConfig(e.ConfigId)
		if ec == nil {
			continue
		}
		if _, ok := suitMap[ec.SuitId]; ok {
			suitMap[ec.SuitId] = suitMap[ec.SuitId] + 1
		} else {
			suitMap[ec.SuitId] = 1
		}

		units := h.getEquipEnhanceUnitsByEquip(e)
		ret = append(ret, units...)
	}

	// 套装
	// configId -> count
	for suitId, count := range suitMap {
		sc, _ := conf.GetEquipSuitConfig(suitId)
		if sc == nil {
			continue
		}
		if count >= sc.TotalNum {
			scale := count / sc.TotalNum
			for addI, addV := range sc.AddAttr {
				u := &enhanceUnit{
					index: addI,
					value: addV * float32(scale),
				}
				//log.Debug(u, count, sc.TotalNum, scale, addV)
				ret = append(ret, u)
			}
		}
	}

	return ret
}

func (h *HeroPropertyCalculator) getTalentEnhanceUnits() []*enhanceUnit {
	var ret []*enhanceUnit

	ext := MustGetTroopExt(h.r, h.troop.Id)
	trees := ext.TalentTrees
	for _, v := range trees {
		for talentId, level := range v.Talents {
			if level < 1 {
				continue
			}

			talentConfig, _ := conf.GetTalentDataConfig(talentId)
			if talentConfig == nil {
				continue
			}
			// lua 和 golang 索引号不一致，golang需要减去1
			unit := &enhanceUnit{
				index: talentConfig.TalentLevelPropertyId,
				value: float32(talentConfig.TalentLevelPropertyValues[level-1]),
			}
			//log.Debugf("talent, talentConfig:%v Level %v, u %v", talentConfig, level, unit)
			ret = append(ret, unit)
		}
	}
	return ret
}

func (h *HeroPropertyCalculator) getSpecialtyEnhanceUnits() []*enhanceUnit {
	var ret []*enhanceUnit

	// key value -> id, level
	pairs := h.troop.Pairs
	for _, v := range pairs {
		specialConf, _ := conf.GetSpecialtyConfig(v.Key)
		if specialConf == nil {
			continue
		}
		if v.Value-1 >= int32(len(specialConf.SpecialtyLevelPropertyValues)) {
			continue
		}
		value := specialConf.SpecialtyLevelPropertyValues[v.Value-1]

		// lua 和 golang 索引号不一致，golang需要减去1
		unit := &enhanceUnit{
			index: specialConf.SpecialtyLevelPropertyId,
			value: float32(value),
		}
		//log.Debugf("talent, talentConfig:%v Level %v, u %v", talentConfig, level, unit)
		ret = append(ret, unit)
	}
	return ret
}

func (h *HeroPropertyCalculator) calcPropertyModifiers() {
	h.enhanceUnits = nil
	// 装备
	{
		us := h.getEquipEnhanceUnits()
		if us != nil {
			h.enhanceUnits = append(h.enhanceUnits, us...)
		}
	}
	// 天赋
	{
		us := h.getTalentEnhanceUnits()
		if us != nil {
			h.enhanceUnits = append(h.enhanceUnits, us...)
		}
	}
	// 专长
	{
		us := h.getSpecialtyEnhanceUnits()
		if us != nil {
			h.enhanceUnits = append(h.enhanceUnits, us...)
		}
	}

	// 已经计算完装备天赋修改器后，整合修改器
	for _, unit := range h.enhanceUnits {
		v := unit.value
		if h.IsPercent(unit.index) {
			v = v / N1000
		}
		h.SetEnhance(unit.index, h.GetEnhance(unit.index)+v)
	}
}

func (h *HeroPropertyCalculator) _calcResult() {
	// Note: 原地修改
	// 可能存在问题
	// 如果有问题，copy一份新的数据，最后赋值给hBase
	for k, v := range h.tBase {
		ev, eOk := h.tEnhance[k]
		if eOk {
			h.SetBase(k, v+ev)
		}
	}

	for _, indices := range Formula2 {
		index := indices[0]
		rateIndex := indices[1]
		h.SetBase(index, h.GetBase(index)*(N1+h.GetBase(rateIndex)))
	}
}
