package game

import (
	"bitbucket.org/funplus/golib/deepcopy"
	"fmt"
	"server/internal/log"
	"server/internal/util"
	"server/internal/util/time3"
	"server/pkg/gen/conf"
	"server/pkg/gen/msg"
	"server/pkg/gen/rawdata"
	"server/pkg/gen_redis/redisdb"
	"server/pkg/persistredis"
	"sort"
)

const (
	basePoint          = 100000
	lastLayerId        = 3
	maxPoint           = 15
	curEventCount      = 3
	battleEventCount   = 15
	ResurrectPotionId  = 1002
	eventCountPerLayer = 15
	maxPowerNum        = 5
	fixOwnPower        = 100000
	MazeRefreshConfId  = 1
	TravelSellerShop   = 4
)

func MustGetPlayerInfoMaze(r *msg.PlayerInfo) (playerMaze *msg.PlayerInfoMaze) {
	if r.Maze != nil {
		return r.Maze
	}

	var c redisdb.PlayerInfoMazeCache
	playerMaze = c.Find(r.Id)

	err := &msg.ErrorInfo{}
	if playerMaze == nil {
		playerMaze = &msg.PlayerInfoMaze{
			Id: r.Id,
		}
		makeSureMazeExist(playerMaze)
		r.Maze = playerMaze
		initMaze(r, playerMaze, err)
		if err.Id != 0 {
			log.Errorf("MustGetPlayerInfoMaze initMaze error ")
			return
		}
		id := c.MustCreate(r.Id, playerMaze)
		if id != r.Id {
			panic(fmt.Errorf("u%d GetPlayerInfoMaze fail", r.Id))
		}
	} else {
		makeSureMazeExist(playerMaze)
		r.Maze = playerMaze
	}
	return
}

func makeSureMazeExist(maze *msg.PlayerInfoMaze) {
	if maze == nil {
		return
	}

	if maze.TroopReduceHp == nil {
		maze.TroopReduceHp = make(map[int32]int32)
	}

	if maze.EventRandomPoint == nil {
		maze.EventRandomPoint = make(map[int32]int64)
	}
	if maze.LevelEvent == nil {
		maze.LevelEvent = make(map[int32]int32)
	}
	if maze.BattleFormation == nil {
		maze.BattleFormation = make(map[int32]*msg.PlayerBattleFormation)
	}
	if maze.WillRecruitTroop == nil {
		maze.WillRecruitTroop = make(map[int32]*msg.MazeTroopInfo)
	}
	if maze.RecruitRandomPoint == nil {
		maze.RecruitRandomPoint = make(map[int32]int64)
	}
	if maze.TroopEnergy == nil {
		maze.TroopEnergy = make(map[int32]int32)
	}
	if maze.RobotInfos == nil {
		maze.RobotInfos = make(map[int32]*msg.MazeRobotInfo)
	}

	if maze.StaticRelicObjs == nil {
		maze.StaticRelicObjs = make(map[int32]*msg.MazeRelicInfo)
	}
	if maze.TroopInfo == nil {
		maze.TroopInfo = &msg.MazeTroopInfo{}
	}
	if maze.TroopInfo.CTroops == nil {
		maze.TroopInfo.CTroops = make(map[int32]*msg.ObjInfo)
	}
	if maze.TroopInfo.CEquips == nil {
		maze.TroopInfo.CEquips = make(map[int32]*msg.ObjInfo)
	}
	if maze.TroopInfo.CTroopsExt == nil {
		maze.TroopInfo.CTroopsExt = make(map[int32]*msg.TroopExtInfo)
	}
}

func resetLayerData(maze *msg.PlayerInfoMaze) {
	if maze == nil {
		return
	}
	maze.LevelEvent = nil
	maze.BattleFormation = nil
	maze.WillRecruitTroop = nil
	maze.RobotInfos = nil
	maze.StaticRelicObjs = nil
}

func ResetMaze(r *msg.PlayerInfo, err *msg.ErrorInfo) {

	maze := MustGetPlayerInfoMaze(r)

	mazeLayerConf, ok := conf.GetMazeLayerConfig(maze.LayerId)
	if !ok {
		log.Errorf("ResetMaze GetMazeLayerConfig error ：%+v", maze.LayerId)
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "GetMazeLayerConfig error"
		return
	}

	funcConfData, ok := conf.GetFunctionConfig(mazeLayerConf.FunctionId)
	if !ok {
		log.Errorf("ResetMaze GetFunctionConfig error ：%+v", mazeLayerConf.FunctionId)
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "GetFunctionConfig error"
		return
	}
	if !CheckConfData(funcConfData, funcConfData.Id, err) {
		log.Errorf("ResetMaze CheckConfData funcConfData error ：%+v", mazeLayerConf.FunctionId)
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "funcConfData error"
		return
	}
	if !CheckConditions(r, funcConfData.UnlockCondition, err) {
		log.Errorf("ResetMaze CheckConfData CheckConditions error ")
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "error condition unlock"
		return
	}

	var c redisdb.ScheduleInfoCache
	ret := c.Find(MazeRefreshConfId)
	if ret == nil {
		log.Errorf("ResetMaze ScheduleInfoCache error")
		return
	}

	maze.ResetTime = ret.EndTime
	if ret.OrderNum != maze.OrderNum && maze.OrderNum > 0 {
		delete(r.Formations, int32(msg.FormationType_FT_Maze))
		initMaze(r, maze, err)
	}
	if err.Id != 0 {
		log.Errorf("ResetMaze initMaze error")
		return
	}
}

func initMaze(r *msg.PlayerInfo, maze *msg.PlayerInfoMaze, err *msg.ErrorInfo) {
	var c redisdb.ScheduleInfoCache
	ret := c.Find(MazeRefreshConfId)
	if ret == nil {
		log.Errorf("ScheduleInfoCache error")
		return
	}
	maze.LayerId = 1
	maze.Mode = int32(msg.MazeModeType_MM_Simple)
	maze.OrderNum = ret.OrderNum
	maze.ResetTime = ret.EndTime
	maze.Times = maze.Times + 1

	layerConf := ConfByLayerMode(maze.LayerId, maze.Mode)
	if layerConf == nil {
		log.Debugf("initMaze layerConf error ")
		return
	}

	MakeSureMazeFormation(r)

	initLayerLevels(r, maze, layerConf.LevelId, err)
	if err.Id != 0 {
		log.Errorf("initLayerLevels error %+v", layerConf.LevelId)
		return
	}
	base := calcBase(maze.LayerId, maze.Mode)

	for i := 0; i < len(maze.EventIds); i++ {
		maze.LevelEvent[int32(i+1)+base] = maze.EventIds[i]
		if len(maze.LevelEvent) >= curEventCount {
			maze.CurLevelId = base + curEventCount
			break
		}
	}
	return
}

func initLayerLevels(r *msg.PlayerInfo, maze *msg.PlayerInfoMaze, levelIds []int32, err *msg.ErrorInfo) {

	var recruitCount int32
	var battleCount int32
	var playerId uint64
	var EventIds []int32
	ownPower := GetFormationPower(r, msg.FormationType_FT_Maze)
	if ownPower == 0 {
		ownPower = GetFormationPower(r, msg.FormationType_FT_MainChapter)
	}
	if ownPower == 0 {
		ownPower = fixOwnPower
		log.Errorf("data may be error ownPower is 0")
	}
	log.Debugf("ownPower :%+v", ownPower)

	key := GetRecentTeamPowerRankKeyName()
	rank, ok := persistredis.Rank32.RevRangeByScore(key, 0, ownPower, int64(battleEventCount))
	if !ok {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "persistredis RevRangeByScore error"
		log.Debugf("initLayerLevels rank error ")
		return
	}

	for i := 0; i < len(levelIds); i++ {
		levelId := levelIds[i]

		refreshConf, ok := conf.GetMazeRefreshConfig(levelId)
		if !ok {
			err.Id, err.DebugMsg = msg.Ecode_INVALID, "persistredis RevRangeByScore error"
			log.Errorf("data levelId may be error%+v", levelId)
			continue
		}

		var curEventId int32
		probMap := make(map[int32]int32)

		for eventId, EventInfo := range refreshConf.Event {
			if len(EventInfo.Value) != 2 {
				continue
			}
			prob := EventInfo.Value[0]
			probMap[eventId] = prob
			//log.Debugf("probMap[eventId]:%+v", probMap[eventId])

			point := EventInfo.Value[1]
			maze.EventRandomPoint[eventId] += int64(point)
			//log.Debugf("eventPoints[eventId]:%+v", eventPoints[eventId])
		}

		for event, point := range maze.EventRandomPoint {
			if point < basePoint {
				continue
			}
			point -= basePoint
			maze.EventRandomPoint[event] = point
			curEventId = event
		}

		if curEventId == 0 {
			curEventId, ok = util.GetRandomValueByMapWeights(probMap, false)
		}
		EventIds = append(EventIds, curEventId)

		eventType := EventType(curEventId)
		switch eventType {
		case msg.MazeEventType_ME_Recruit:
			recruitCount++
			initRecruit(r, maze, levelId, recruitCount)
		case msg.MazeEventType_ME_Normal:
			fallthrough
		case msg.MazeEventType_ME_Elite:
			fallthrough
		case msg.MazeEventType_ME_Boss:
			log.Debugf("len(rank) %d", len(rank))
			if len(rank) != 0 {
				if battleCount >= int32(len(rank)) {
					battleCount = int32(len(rank)) - 1
				}
				playerId = rank[battleCount].Id
			}
			err := &msg.ErrorInfo{}

			initDefenseFormation(maze, levelId, playerId, err)
			if err.Id != 0 {
				log.Debugf("initDefenseFormation error")
				continue
			}
			battleCount++
		}
	}
	maze.EventIds = EventIds
	return
}

func hasEvent(eventIds []int32, eventId int32) bool {
	for i := 0; i < len(eventIds); i++ {
		if eventId != eventIds[i] {
			continue
		}
		return true
	}
	return false
}

func GetEventByLevel(layerId int32, mode int32, eventIds []int32, levelId int32) (eventId int32) {
	base := calcBase(layerId, mode)

	for i := 0; i < len(eventIds); i++ {
		if int32(i)+base != levelId-1 {
			continue
		}
		eventId = eventIds[int32(i)]
	}
	return
}

func StartMazeEvent(r *msg.PlayerInfo, resurrectTroopId int32, levelId int32, RestoreHp map[int32]int32, RestoreEnergy map[int32]int32, err *msg.ErrorInfo) (artifactObjs []*msg.ObjInfo) {
	maze := MustGetPlayerInfoMaze(r)
	if levelId == 0 {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "StartMazeEvent levelId id is error"
		return
	}

	if _, ok := maze.LevelEvent[levelId]; !ok {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "levelId id is not exist"
		return
	}

	eventId := GetEventByLevel(maze.LayerId, maze.Mode, maze.EventIds, levelId)
	if eventId == 0 {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "levelId error"
		return
	}

	if !hasEvent(maze.EventIds, eventId) {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "has no this event"
		return
	}

	eType := EventType(eventId)

	if isBattleEvent(eventId) {

	}

	if eType == msg.MazeEventType_ME_Recruit {

	} else if eType == msg.MazeEventType_ME_SpringWater {
		for k, _ := range RestoreHp {
			deadIndex := GetDeadIndex(maze.DeadTroops, k)
			if deadIndex >= 0 {
				maze.TroopReduceHp[k] = maze.TroopReduceHp[k] - RestoreHp[k]
				if maze.TroopReduceHp[k] < 0 {
					maze.TroopReduceHp[k] = 0
				}
			}
		}
	} else if eType == msg.MazeEventType_ME_Resurrection {
		for k, _ := range RestoreHp {
			if k != resurrectTroopId {
				continue
			}
			maze.TroopReduceHp[k] = 0
		}

		for k, v := range RestoreEnergy {
			if k != resurrectTroopId {
				continue
			}
			maze.TroopEnergy[k] = v
		}

		count := len(maze.DeadTroops)
		var deadTroops []int32
		for k, v := range maze.TroopReduceHp {
			for i := 0; i < count; i++ {
				if maze.DeadTroops[i] == k && v > 0 {
					resurrectTroopId = k
					deadTroops = maze.DeadTroops[0:i]
					deadTroops = append(deadTroops, maze.DeadTroops[int32(i+1):]...)
				}
			}
		}
		maze.DeadTroops = deadTroops
	} else if eType == msg.MazeEventType_ME_TravelSaleler {

	} else if eType == msg.MazeEventType_ME_Artifact {
		err := &msg.ErrorInfo{}
		artifactUnits := ArtifactEvent(levelId, err)
		if err.Id != 0 {
			return
		}

		artifactObjs, _ = AddStaticObjs(r, artifactUnits, err)

		if err.Id != 0 {
			return
		}
	}
	NextLevelId(maze, levelId)
	return
}

func NextLevelId(maze *msg.PlayerInfoMaze, levelId int32) {

	if maze.Points >= maxPoint {
		log.Debugf("NextLevelId point error")
		return
	}

	var hasLevel bool
	for k, _ := range maze.LevelEvent {
		if k == levelId {
			hasLevel = true
		}
	}
	if !hasLevel {
		log.Debugf("NextLevelId hasLevel error")
		return
	}

	for k, _ := range maze.LevelEvent {
		if k != levelId {
			continue
		}
		maze.CurLevelId++
		delete(maze.LevelEvent, k)

		newLevelId := maze.CurLevelId
		newEventId := GetEventByLevel(maze.LayerId, maze.Mode, maze.EventIds, newLevelId)
		if newEventId != 0 {
			maze.LevelEvent[newLevelId] = newEventId
		}
	}
	//本层积分
	maze.Points++
	//log.Debugf("LevelEvent:%+v,eventIds:%+v,curLevelId:%+v", maze.LevelEvent, maze.EventIds, maze.CurLevelId)
}

func ArtifactEvent(levelId int32, err *msg.ErrorInfo) (artifactDropUnits []*msg.StaticObjInfo) {
	refreshConf, ok := conf.GetMazeRefreshConfig(levelId)
	if !ok {
		log.Debugf("StartMazeEvent refreshConf error ")
		return
	}

	artifactDropUnits = GetDropUnits(nil, refreshConf.ArtifactDropId, err)
	if err.Id != 0 {
		log.Debugf("StartMazeEvent artifactDropUnits error ")
		return
	}
	return
}

func getNextEvent(eventIds []int32, point int32) (newEventId int32) {
	for j := 0; j < len(eventIds); j++ {
		if point != int32(j+1) {
			continue
		}
		newEventId = eventIds[j]
		return
	}
	if newEventId == 0 {
		log.Debugf("getNextEvent error no event")
	}
	return
}

func EventType(eventId int32) (eventType msg.MazeEventType) {
	eventConf, ok := conf.GetMazeEventConfig(eventId)
	if !ok {
		log.Errorf("EventType is error%+v", eventId)
		return msg.MazeEventType_ME_None
	}
	return msg.MazeEventType(eventConf.Type)
}

func isBattleEvent(eventId int32) bool {
	eType := EventType(eventId)
	if eType == msg.MazeEventType_ME_None {
		return false
	}
	if msg.MazeEventType_ME_Normal == eType ||
		msg.MazeEventType_ME_Elite == eType ||
		msg.MazeEventType_ME_Boss == eType {
		return true
	}
	return true
}

func initDefenseFormation(maze *msg.PlayerInfoMaze, levelId int32, playerId uint64, err *msg.ErrorInfo) {

	refreshConf, ok := conf.GetMazeRefreshConfig(levelId)
	if !ok {
		log.Errorf("initDefenseFormation GetMazeRefreshConfig error %+v", levelId)
		return
	}

	if _, ok := refreshConf.RobotGet[maze.Times]; ok {
		initRobotBattleFormation(maze, maze.Times, levelId, refreshConf.RobotGet)
	} else {
		if playerId != 0 {
			maze.BattleFormation[levelId] = &msg.PlayerBattleFormation{}
			FillBattleFormation(maze.BattleFormation[levelId], playerId, err)
			if err.Id != 0 {
				log.Debugf("FillBattleFormation error levelId:%+v,target playerId:%+v", levelId, playerId)
				return
			}
		} else {
			log.Debugf("not find player battle robot continue ")

			var times []int
			for key, _ := range refreshConf.RobotGet {
				times = append(times, int(key))
			}
			if len(times) > 1 {
				sort.Ints(times)
			}

			var lastValidTimes int32
			for i := len(times); i > 0; i++ {
				if int32(times[i-1]) > maze.Times {
					continue
				}
				lastValidTimes = int32(times[i-1])
				break
			}
			log.Debugf("lastValidTimes :%+v ", lastValidTimes)
			initRobotBattleFormation(maze, lastValidTimes, levelId, refreshConf.RobotGet)
		}
	}
}

func MakeSureMazeFormation(r *msg.PlayerInfo) {
	if _, ok := r.Formations[int32(msg.FormationType_FT_Maze)]; !ok {
		copyFormation := deepcopy.Copy(r.Formations[int32(msg.FormationType_FT_MainChapter)]).(*msg.Formation)
		r.Formations[int32(msg.FormationType_FT_Maze)] = copyFormation
	}
}

func initRecruit(r *msg.PlayerInfo, maze *msg.PlayerInfoMaze, levelId int32, recruitCount int32) {
	layerConf := ConfByLayerMode(maze.LayerId, maze.Mode)
	//log.Debugf("initRecruit levelId is %d", levelId)
	//log.Debugf("initRecruit layerId:%+v,mode:%+v", maze.LayerId, maze.Mode)
	//log.Debugf("initRecruit layerConf:%+v", layerConf)
	//log.Debugf("initRecruit maze.Times:%+v", maze.Times)
	var recruitGroupId int32
	for k, v := range layerConf.MazeRecruitGet {
		//log.Debugf("initRecruit k:%+v", k)
		if k != maze.Times {
			continue
		}
		if len(v.Value)%2 != 0 {
			continue
		}
		for i := 0; i < len(v.Value); i += 2 {
			if v.Value[i] != recruitCount {
				continue
			}
			recruitGroupId = v.Value[i+1]
			break
		}
	}
	//log.Debugf("initRecruit *** recruitGroupId:%+v", recruitGroupId)

	if recruitGroupId != 0 {
		recruitConf, ok := conf.GetMazeRecruitConfig(recruitGroupId)
		if !ok {
			log.Debugf("initRecruit GetMazeRecruitConfig error")
			return
		}
		if recruitConf.Condition != 0 {
			err := &msg.ErrorInfo{}
			ok := CheckCondition(r, recruitConf.Condition, err)
			if err.Id != 0 || !ok {
				recruitGroupId = 0
				log.Debugf("initRecruit checkCondition error")
			}
		}
	}

	if recruitGroupId == 0 {
		layerConf := ConfByLayerMode(maze.LayerId, maze.Mode)
		if layerConf == nil {
			log.Debugf("initRecruit layerConf error ")
			return
		}

		var validGroupId []int32
		var realRandomList []int32

		for groupId, _ := range layerConf.RecruitId {
			recruitConf, ok := conf.GetMazeRecruitConfig(groupId)
			if !ok {
				log.Debugf("===initRecruit GetMazeRecruitConfig error===")
				return
			}
			if recruitConf.Condition != 0 {
				err := &msg.ErrorInfo{}
				ok := CheckCondition(r, recruitConf.Condition, err)
				if err.Id != 0 || !ok {
					log.Debugf("initRecruit *** err.Id:%+v", err.Id)
					continue
				}
			}
			validGroupId = append(validGroupId, groupId)
		}
		//log.Debugf("initRecruit validGroupId:%+v", validGroupId)

		for groupId, v := range layerConf.RecruitId {
			//log.Debugf("initRecruit  ** groupId:%+v", groupId)
			var isValidGroupId bool
			for i := 0; i < len(validGroupId); i++ {
				//log.Debugf("initRecruit  ** validGroupId[i]:%+v", validGroupId[i])
				if validGroupId[i] != groupId {
					continue
				}
				isValidGroupId = true
				break
			}
			if isValidGroupId == false {
				log.Debugf("==initRecruit GetMazeRecruitConfig groupId is not valid==")
				continue
			}

			for j := 0; j < len(v.Value); j += 2 {
				if (len(v.Value))%2 != 0 {
					log.Debugf("validGroupId data may be error!!!")
					return
				}
				maze.RecruitRandomPoint[groupId] += int64(v.Value[j+1])
				if maze.RecruitRandomPoint[groupId] >= basePoint {
					maze.RecruitRandomPoint[groupId] -= basePoint
					recruitGroupId = groupId
				}
				realRandomList = append(realRandomList, v.Value[j])

				if recruitGroupId != 0 {
					//log.Debugf("recruitGroupId:%+v", recruitGroupId)
					break
				}
			}

			if recruitGroupId != 0 {
				//log.Debugf("**recruitGroupId**:%+v", recruitGroupId)
				break
			}
		}
		//log.Debugf("initRecruit recruitGroupId:%+v", recruitGroupId)
		//log.Debugf("initRecruit realRandomList:%+v", realRandomList)
		//log.Debugf("initRecruit validGroupId:%+v", validGroupId)

		randomIndex := util.GetRandomKeyByWeights(realRandomList, true)
		//log.Debugf("initRecruit random randomIndex:%+v", randomIndex)
		//log.Debugf("initRecruit random validGroupId:%+v", validGroupId)

		if randomIndex < int32(len(validGroupId)) {
			recruitGroupId = validGroupId[randomIndex]
		}
		//log.Debugf("initRecruit random recruitGroupId:%+v", recruitGroupId)
	}
	if recruitGroupId == 0 {
		//panic("initRecruit error no recruitGroupId")
	}
	log.Debugf("initRecruit final recruitGroupId:%+v", recruitGroupId)

	//log.Debugf("initRecruit final recruitGroupId:%+v", recruitGroupId)
	mazeTroopInfo := makeMazeTroop(r, recruitGroupId)
	//log.Debugf("initRecruit mazeTroopInfo is %+v", mazeTroopInfo)
	maze.WillRecruitTroop[levelId] = mazeTroopInfo
	//log.Debugf("initRecruit levelId is %+v", levelId)
	//log.Debugf("initRecruit maze.WillRecruitTroop[levelId] is %+v", maze.WillRecruitTroop[levelId])
	//}
}

func Recruit(r *msg.PlayerInfo, levelId int32, troopObjId int32, err *msg.ErrorInfo) {
	maze := MustGetPlayerInfoMaze(r)
	CheckEventType(maze, levelId, msg.MazeEventType_ME_Recruit, err)
	if err.Id != 0 {
		return
	}

	willTroopInfo := maze.WillRecruitTroop[levelId]

	var troop *msg.ObjInfo
	var troopExt *msg.TroopExtInfo
	equips := make(map[int32]*msg.ObjInfo)

	if _, ok := maze.WillRecruitTroop[levelId]; !ok {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "WillRecruitTroop has no key"
		return
	}

	for _, v := range willTroopInfo.CTroops {
		if v.Id != troopObjId {
			continue
		}

		troop = v

		troopExt = willTroopInfo.CTroopsExt[troopObjId]
		if troopExt != nil {
			for _, v1 := range troopExt.Equips {
				equip := willTroopInfo.CEquips[v1]
				equips[equip.Id] = equip
			}
		}
	}

	if troop == nil {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "WillRecruitTroop has no troop"
		return
	}

	maze.TroopInfo.CTroops[troopObjId] = troop
	maze.TroopInfo.CTroopsExt[troopObjId] = troopExt
	// 装备追加!
	for k, v := range equips {
		maze.TroopInfo.CEquips[k] = v
	}
	NextLevelId(maze, levelId)
}

func ClearLayer(r *msg.PlayerInfo, layerId int32, err *msg.ErrorInfo) (objs []*msg.ObjInfo) {
	maze := MustGetPlayerInfoMaze(r)
	log.Debugf("ClearLayer maze:%+v", maze)
	if maze.LayerId != layerId {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "clear layer error"
		return
	}

	//if maze.LayerId == lastLayerId {
	//	err.Id, err.DebugMsg = msg.Ecode_INVALID, "clear layer lastLayerId error"
	//	return
	//}

	if maze.Points < maxPoint {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "clear layer point error"
		return
	}

	if maze.HasRecvReward == 1 {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "clear layer HasRecvReward "
		return
	}

	layerConf := ConfByLayerMode(maze.LayerId, maze.Mode)

	units, _ := JsonToPb_StaticObjs(layerConf.Reward, layerConf, layerConf.Id, err)
	if err.Id != 0 {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "clear layerConf error"
		return
	}

	objs, _ = AddStaticObjs(r, units, err)
	if err.Id != 0 {
		return
	}

	maze.HasRecvReward = 1

	if lastLayerId > maze.LayerId {
		//以下部分重置..
		maze.Points = 0
		maze.LayerId++
		maze.HasRecvReward = 0
		resetLayerData(maze)
		//其他部分继续保留到本次迷宫结束
		//maze.EventRandomPoint
		//maze.RecruitRandomPoint
		//永久保留
		if lastLayerId == maze.LayerId {
			layerConf := ConfByLayerMode(maze.LayerId, int32(msg.MazeModeType_MM_hard))
			funcConfData, ok := conf.GetFunctionConfig(layerConf.FunctionId)
			if !ok {
				log.Errorf("conf.GetFunctionConfig error %+v", layerConf.FunctionId)
				return
			}
			if CheckConfData(funcConfData, funcConfData.Id, err) {
				if !CheckConditions(r, funcConfData.UnlockCondition, &msg.ErrorInfo{}) {
					//err.Id, err.DebugMsg = msg.Ecode_INVALID, "error condition unlock"
					maze.Mode = int32(msg.MazeModeType_MM_Simple)
				} else {
					maze.Mode = 0
				}
			}
			if maze.Mode == 0 {
				return
			}
		}
		InitNewLayer(r, maze, maze.Mode)
	}
	return
}

func MazeMode(r *msg.PlayerInfo, mode msg.MazeModeType, err *msg.ErrorInfo) {
	maze := MustGetPlayerInfoMaze(r)
	if maze.LayerId != lastLayerId {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "MazeMode layer error"
		return
	}
	InitNewLayer(r, maze, int32(mode))
}

func FillBattleFormation(battleFormation *msg.PlayerBattleFormation, playerId uint64, err *msg.ErrorInfo) {
	var c redisdb.PlayerInfoCache
	otherPlayerInfo := c.Find(playerId)
	if otherPlayerInfo == nil {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "not found user in db"
		return
	}
	formation := SelectFormation(otherPlayerInfo, msg.FormationType_FT_ArenaDef, err)
	CheckFormationIsNull(otherPlayerInfo, formation, err)
	if err.Id != 0 {
		formation = SelectFormation(otherPlayerInfo, msg.FormationType_FT_MainChapter, err)
	}
	//err.Id = 0
	//CheckFormationIsNull(otherPlayerInfo, formation, err)
	if err.Id != 0 {
		log.Debugf("FillBattleFormation FormationType_FT_MainChapter error")
	}
	log.Debugf("FillBattleFormation formation :%+v", formation)

	battleFormation.Formation = formation

	troopInfos := GetTroopInfosForClient(otherPlayerInfo, formation.TroopIds)
	battleFormation.Troops = troopInfos.Troops
	battleFormation.Equips = troopInfos.Equips
	battleFormation.TroopsExt = troopInfos.TroopExts
}

func CurLayerEventTimes(layerId int32, mode int32, events []int32, curLevelId int32) (times int32) {
	//log.Debugf("CurLayerEventTimes events:%+v", events)

	eventId := GetEventByLevel(layerId, mode, events, curLevelId)
	eventType := EventType(eventId)

	base := calcBase(layerId, mode)

	for i := 0; i < len(events); i++ {
		if eventType == EventType(events[i]) {
			times++
		}
		if curLevelId == int32(i+1)+base {
			break
		}
	}
	//log.Debugf("CurLayerEventTimes events:%+v, curLevelId:%+v,times:%+v", events, curLevelId, times)
	return
}

func ConfByLayerMode(layerId int32, mode int32) (layerConf *rawdata.MazeLayerConfig) {
	for i := 0; i < conf.CountMazeEventConfigMap(); i++ {
		layer, ok := conf.GetMazeLayerConfig(int32(i + 1))
		if !ok {
			log.Debugf("GetConfByLayerAndMode layerConf error ")
			continue
		}
		if layer.LayerId == layerId && mode == layer.Type {
			layerConf = layer
			return
		}
	}

	log.Debugf("GetConfByLayerAndMode Get layerConf error ")
	//log.Debugf("GetConfByLayerAndMode  layerConf  %+v", layerConf)
	return
}

func GetBattleEventCount(events []int32) (count int32) {
	for i := 0; i < len(events); i++ {
		if isBattleEvent(events[i]) != true {
			continue
		}
		count++
	}
	return
}

func GetOrderNum() (curOrderNum int32, resetTime int64, lastStartTime int64) {
	refreshConf, ok := conf.GetRefreshConfig(MazeRefreshConfId)
	if !ok {
		log.Errorf("GetOrderNum error ")
		return
	}

	err, startTime := time3.GetTimeFromStr(refreshConf.Start)
	if err != nil {
		log.Errorf("GetTimeFromStr error ")
		return 0, 0, 0
	}
	//log.Debugf("startTime time", startTime)
	now := time3.Now().Unix()
	//log.Debugf("now time", now)
	//refreshConf.DurationSecond = 60 * 1
	var count int32
	for {
		count++
		resetTime = startTime + int64(count*refreshConf.DurationSecond)
		if resetTime >= now {
			break
		}
	}
	curOrderNum = count - 1
	lastStartTime = resetTime - int64(refreshConf.DurationSecond)
	return curOrderNum, resetTime, lastStartTime
}

func CheckEventType(maze *msg.PlayerInfoMaze, levelId int32, eventType msg.MazeEventType, err *msg.ErrorInfo) {
	eventId := GetEventByLevel(maze.LayerId, maze.Mode, maze.EventIds, levelId)
	findEventType := EventType(eventId)
	if eventType != findEventType {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "eventType is not Recruit"
		return
	}
}

func GetDeadIndex(deadTroops []int32, troopId int32) (deadIndex int32) {
	deadIndex = -1
	for i := 0; i < len(deadTroops); i++ {
		if deadTroops[i] != troopId {
			continue
		}
		deadIndex = int32(i)
		break
	}
	return
}

func initRobotBattleFormation(maze *msg.PlayerInfoMaze, times int32, levelId int32, robotGet map[int32]int32) (robotId int32) {
	for k, v := range robotGet {
		if k != times {
			continue
		}
		robotId = v

		maze.BattleFormation[levelId] = &msg.PlayerBattleFormation{
			RobotConfId: int64(robotId),
		}
		break
	}
	return
}

func InitNewLayer(r *msg.PlayerInfo, maze *msg.PlayerInfoMaze, mode int32) {
	log.Debugf("InitNewLayer maze info %+v", maze)
	if maze == nil || r == nil {
		//log.Debugf("InitByNewLayer maze is nil")
		return
	}
	MakeSureMazeFormation(r)

	layerConf := ConfByLayerMode(maze.LayerId, mode)
	if layerConf == nil {
		log.Debugf("ClearLayer layerConf error ")
		return
	}
	log.Debugf("ClearLayer layerConf maze:%+v", maze)

	maze.LevelEvent = make(map[int32]int32)
	maze.BattleFormation = make(map[int32]*msg.PlayerBattleFormation)
	maze.WillRecruitTroop = make(map[int32]*msg.MazeTroopInfo)
	maze.RobotInfos = make(map[int32]*msg.MazeRobotInfo)
	maze.StaticRelicObjs = make(map[int32]*msg.MazeRelicInfo)

	err := &msg.ErrorInfo{}
	initLayerLevels(r, maze, layerConf.LevelId, err)
	if err.Id != 0 {
		log.Errorf("InitNewLayer error %+v", layerConf.LevelId)
		return
	}
	base := calcBase(maze.LayerId, mode)

	for i := 0; i < len(maze.EventIds); i++ {
		maze.LevelEvent[int32(i+1)+base] = maze.EventIds[i]
		if len(maze.LevelEvent) >= curEventCount {
			maze.CurLevelId = base + curEventCount
			break
		}
	}
	maze.Mode = mode
}

func calcBase(layerId, mode int32) (base int32) {
	if layerId < 1 || layerId > lastLayerId {
		return
	}
	base = (layerId - 1) * int32(eventCountPerLayer)
	if layerId == lastLayerId {
		if mode == int32(msg.MazeModeType_MM_hard) {
			base = base + eventCountPerLayer
		}
	}
	return
}

func makeMazeTroop(r *msg.PlayerInfo, recruitGroupId int32) (mazeTroopInfo *msg.MazeTroopInfo) {
	mazeTroopInfo = &msg.MazeTroopInfo{
		CTroops:    make(map[int32]*msg.ObjInfo),
		CEquips:    make(map[int32]*msg.ObjInfo),
		CTroopsExt: make(map[int32]*msg.TroopExtInfo),
	}

	maxPower5Troops := GetTroopsByMaxPower(r, maxPowerNum)
	if len(maxPower5Troops) == 0 {
		panic("maxPower5Troops is nil")
		return
	}
	var averageLevel int32
	for _, v := range maxPower5Troops {
		averageLevel += v.Level
	}
	averageLevel /= int32(len(maxPower5Troops))
	originMaxPowerTroop := maxPower5Troops[0] //最少有一个

	recruitConf, ok := conf.GetMazeRecruitConfig(recruitGroupId)
	if !ok {
		log.Debugf("initRecruit GetMazeRecruitConfig error")
		return
	}

	originExt := GetTroopExt(r, originMaxPowerTroop.Id)
	originEquip2Slot := make(map[int32]int32)
	for k, v := range originExt.Equips {
		originEquip2Slot[v] = k
	}

	for i := 0; i < len(recruitConf.Hero); i++ {
		newTroopId := DispatchObjIdUnsafe(r, msg.ObjType_OT_Troop)
		configId := recruitConf.Hero[i]
		confData, _ := conf.GetTroopConfig(configId)
		tempError := &msg.ErrorInfo{}
		if !CheckConfData(confData, configId, tempError) {
			log.Errorf("recruit err: %+v", tempError)
			continue
		}

		newTroop := &msg.ObjInfo{
			Id:       newTroopId,
			ConfigId: configId,
			Type:     msg.ObjType_OT_Troop,
			Num:      1,
			Level:    averageLevel,
			Star:     originMaxPowerTroop.Star,
		}
		mazeTroopInfo.CTroops[newTroopId] = newTroop
		//log.Debugf("initRecruit mazeTroopInfo.CTroops[troopId] is %+v", mazeTroopInfo.CTroops[troopId])

		//todo 英雄可能不一样不能直接赋值等策划确定
		//todo 这个养成系统后面要改,策划说先不加天赋
		mazeTroopInfo.CTroopsExt[newTroopId] = &msg.TroopExtInfo{
			Equips:      make(map[int32]int32),
			TalentTrees: nil,
		}

		for _, equipId := range originExt.Equips {
			err := &msg.ErrorInfo{}

			// 不能修改原始装备！
			originEquip := GetEquip(r, equipId, err)
			if err.Id != 0 {
				continue
			}

			equip := deepcopy.Copy(originEquip).(*msg.ObjInfo)

			newEquipId := DispatchObjIdUnsafe(r, msg.ObjType_OT_Equipment)
			equip.Id = newEquipId

			mazeTroopInfo.CEquips[newEquipId] = equip

			slotId := originEquip2Slot[originEquip.Id]
			mazeTroopInfo.CTroopsExt[newTroopId].Equips[slotId] = newEquipId
		}

		newPairs := deepcopy.Copy(originMaxPowerTroop.Pairs).([]*msg.IntPair)
		newTroop.Pairs = newPairs
	}
	return
}

func GetMazeTroop(r *msg.PlayerInfo, id int32, err *msg.ErrorInfo) *msg.ObjInfo {
	maze := MustGetPlayerInfoMaze(r)
	if maze.TroopInfo == nil {
		return nil
	}
	for k, troopObj := range maze.TroopInfo.CTroops {
		if k != id {
			continue
		}
		return troopObj
	}
	err.Id, err.DebugMsg = msg.Ecode_INVALID, "no troop"
	return nil
}
