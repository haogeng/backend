package game

import (
	"bitbucket.org/funplus/sandwich/current"
	"context"
	"fmt"
	"server/internal"
	"server/internal/gconst"
	"server/internal/log"
	"server/internal/util"
	"server/pkg/fungin/xmiddleware"
	"server/pkg/gen/conf"
	"server/pkg/gen/msg"
	"server/pkg/gen/rawdata"
	"sort"
)

func GetTroop(r *msg.PlayerInfo, id int32, err *msg.ErrorInfo) *msg.ObjInfo {
	d, ok := ExtTroop(r).Troops[id]
	if !ok {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "no troop"
		debugStr := fmt.Sprintf("%d", id)
		err.Params = append(err.Params, debugStr)
		return nil
	}
	return d
}

func makesureTroopBook(r *msg.PlayerInfo) {
	if r.TroopBook == nil {
		r.TroopBook = make(map[int32]bool)
	}
}

// 有可能返回nil
func GetTroopExt(r *msg.PlayerInfo, troopId int32) *msg.TroopExtInfo {
	d, _ := ExtTroop(r).TroopsExt[troopId]
	return d
}

// 如果没有则添加一个
// troopId 动态id
func MustGetTroopExt(r *msg.PlayerInfo, troopId int32) *msg.TroopExtInfo {
	d, ok := ExtTroop(r).TroopsExt[troopId]
	if !ok {
		ExtTroop(r).TroopsExt[troopId] = &msg.TroopExtInfo{
			TalentTrees: make(map[int32]*msg.TroopTalentTree),
		}
		d = ExtTroop(r).TroopsExt[troopId]
	}
	return d
}

// 得到天赋树
func MustGetTalentTree(r *msg.PlayerInfo, troopId int32, treeId int32) *msg.TroopTalentTree {
	ext := MustGetTroopExt(r, troopId)
	if ext == nil {
		return nil
	}

	if ext.TalentTrees == nil {
		ext.TalentTrees = make(map[int32]*msg.TroopTalentTree)
	}
	if _, ok := ext.TalentTrees[treeId]; !ok {
		ext.TalentTrees[treeId] = &msg.TroopTalentTree{
			Talents: map[int32]int32{},
		}
	}
	return ext.TalentTrees[treeId]
}

func GetTalentLevelConfData(r *msg.PlayerInfo, troopId int32, treeId int32, talentId int32, err *msg.ErrorInfo) (*rawdata.TalentLevelDataConfig, bool) {
	talentConf, _ := conf.GetTalentDataConfig(talentId)
	if !CheckConfData(talentConf, talentId, err) {
		return nil, false
	}

	oldLevel := GetTroopTalentLevel(r, troopId, treeId, talentId)
	levelId, ok := talentConf.TalentLevelCfgs[oldLevel]
	if !ok {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "not find level data"
		return nil, false
	}
	levelConf, _ := conf.GetTalentLevelDataConfig(levelId)
	if !CheckConfData(levelConf, levelId, err) {
		return nil, false
	}

	return levelConf, true
}

func GetTroopTalentLevel(r *msg.PlayerInfo, troopId int32, treeId int32, talentId int32) int32 {
	tree := MustGetTalentTree(r, troopId, treeId)
	if tree == nil || tree.Talents == nil {
		return 0
	}

	if d, ok := tree.Talents[talentId]; ok {
		return d
	}
	return 0
}
func SetTroopTalentLevel(r *msg.PlayerInfo, troopId int32, treeId int32, talentId int32, level int32) {
	tree := MustGetTalentTree(r, troopId, treeId)
	if tree == nil {
		// skip
		log.Error("tree shouldn't be nil")
		return
	}
	if tree.Talents == nil {
		tree.Talents = make(map[int32]int32)
	}

	// set anyway
	tree.Talents[talentId] = level
}

// 用于升级或退换资源
// exp简化：只能整级别的升
// return exp of [startLevel, destLevel)
func GetTroopLevelUpConsume(confId int32, startLevel int32, destLevel int32, err *msg.ErrorInfo) []*msg.StaticObjInfo {
	var needExp, needGold int64
	needExp, needGold = 0, 0
	// maybe: confId decide a scale
	for i := startLevel; i < destLevel; i++ {
		confData, _ := conf.GetTroopLevelUpConfig(i)
		if !CheckConfData(confData, i, err) {
			return nil
		}

		needExp += int64(confData.NeedTroopExp)
		needGold += int64(confData.NeedGold)
	}

	return []*msg.StaticObjInfo{
		&msg.StaticObjInfo{
			Id:    int32(msg.CurrencyId_CI_TroopExp),
			Type:  int32(msg.ObjType_OT_Currency),
			Count: needExp,
		},
		&msg.StaticObjInfo{
			Id:    int32(msg.CurrencyId_CI_Gold),
			Type:  int32(msg.ObjType_OT_Currency),
			Count: needGold,
		},
	}
}

func GetTroopSpecialtyUpConsume(specialtyConf *rawdata.SpecialtyConfig, startLevel int32, destLevel int32, err *msg.ErrorInfo) (costs []*msg.StaticObjInfo) {
	costs = make([]*msg.StaticObjInfo, 0)
	// maybe: confId decide a scale
	for i := startLevel; i < destLevel; i++ {
		levelId, ok := specialtyConf.SpecialtyLevelCfgs[i]
		if !ok {
			err.Id, err.DebugMsg = msg.Ecode_INVALID, "not found level data in SpecialtyLevelCfgs"
			return
		}
		levelData, _ := conf.GetSpecialtyLevelConfig(levelId)
		if !CheckConfData(levelData, levelId, err) {
			return nil
		}

		tempCosts, _ := JsonToPb_StaticObjs(levelData.Cost, levelData, levelId, err)
		if err.Id != 0 {
			return
		}
		// 升级配置为增量值，需要累加一下
		costs = append(costs, tempCosts...)
		costs = MergeStaticObjInfo(costs)
	}

	return
}

// 这个消耗里的英雄是抽象的，需要特殊处理
// 客户端按条件顺序发上来， 服务器约定按序列检查
func GetTroopStarUpConsumeX(confId int32, curStar int32, err *msg.ErrorInfo) (consumeTroopsX []*msg.StaticObjInfo) {
	confData, _ := conf.GetTroopConfig(confId)
	if !CheckConfData(confData, confId, err) {
		return nil
	}

	starConfigId, ok := confData.StarCfgID[curStar]
	if !ok {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "conf data err"
		return
	}
	starConfData, _ := conf.GetTroopStarConfig(starConfigId)
	if !CheckConfData(starConfData, starConfigId, err) {
		return nil
	}

	c, _ := JsonToPb_StaticObjs(starConfData.StarAdvExpand, starConfData, starConfigId, err)
	if err.Id != 0 {
		return
	}

	return c
}

func GetTroopStarUpAwardX(confId int32, curStar int32, err *msg.ErrorInfo) (consumeTroopsX []*msg.StaticObjInfo) {
	confData, _ := conf.GetTroopConfig(confId)
	if !CheckConfData(confData, confId, err) {
		return nil
	}

	starConfigId, ok := confData.StarCfgID[curStar]
	if !ok {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "conf data err"
		return
	}
	starConfData, _ := conf.GetTroopStarConfig(starConfigId)
	if !CheckConfData(starConfData, starConfigId, err) {
		return nil
	}

	c, _ := JsonToPb_StaticObjs(starConfData.Reward, starConfData, starConfigId, err)
	if err.Id != 0 {
		return
	}

	return c
}

// 根据需求匹配英雄
// starExact 星级是否精确；if true use =; else use >=.
func checkMatchTroop(r *msg.PlayerInfo, needTroopsX []*msg.StaticObjInfo, ids []int32, starExact bool, err *msg.ErrorInfo) bool {
	defaultErrId, defaultErrDebug := msg.Ecode_INVALID, "check troop star up consume x err"

	// assume its ok
	err.Id, err.DebugMsg = 0, ""

	if len(needTroopsX) != len(ids) {
		err.Id, err.DebugMsg = defaultErrId, defaultErrDebug
		err.DebugMsg += " troops need nums un match"
		return false
	}

	// use for check repeat ids.
	if util.HasRepeat(ids) {
		err.Id, err.DebugMsg = defaultErrId, defaultErrDebug
		err.DebugMsg += " has repeated"
		return false
	}

	// check consume X
	for i := 0; i < len(ids); i++ {
		need := needTroopsX[i]
		id := ids[i]

		t := GetTroop(r, id, err)
		if err.Id != 0 {
			break
		}

		tc, _ := conf.GetTroopConfig(t.ConfigId)
		if !CheckConfData(tc, t.ConfigId, err) {
			return false
		}

		curStarMatched := false
		if starExact {
			curStarMatched = (t.Star == need.Star)
		} else {
			curStarMatched = (t.Star >= need.Star)
		}

		// notice order...
		//log.Debugf("need %v, real %v", need, t)

		if (need.Id != 0 && need.Id != t.ConfigId) ||
			(need.Star != 0 && !curStarMatched) ||
			(need.Civil != 0 && need.Civil != tc.Civilization) ||
			(t.Num <= 0) {
			err.Id, err.DebugMsg = defaultErrId, defaultErrDebug
			err.DebugMsg += " need id or star or civil err"
			return false
		}
	}

	if err.Id != 0 {
		return false
	}

	return true
}

// !!! 该函数会被bounty重用
// needTroopsX
func CheckTroopConsumeX(r *msg.PlayerInfo, needTroopsX []*msg.StaticObjInfo, ids []int32, err *msg.ErrorInfo) bool {
	return checkMatchTroop(r, needTroopsX, ids, true, err)
}

// 返回已重置的东西
func DelTroop(r *msg.PlayerInfo, id int32, err *msg.ErrorInfo) (ret []*msg.ObjInfo) {
	// do sth
	if ExtTroop(r).TroopsExt != nil {
		ret = ResetTroop(r, id, err)
		if err.Id != 0 {
			return
		}
	}

	// 增加专长返还
	tempAwards := AddSpecialtyResetAward(r, id, err)
	if err.Id != 0 {
		return
	}
	ret = append(ret, tempAwards...)
	// 合并
	ret = MergeObjInfo(ret)

	//log.Debugf("DelTroop id %+v", id)

	// 不需要了，在可能删除阵容的逻辑处，调用ValidFormationAndTrySync
	//UpdateFormationOnTroopRemoved(r, id)

	delete(ExtTroop(r).Troops, id)

	return
}

func calcResetLevel(troopObj *msg.ObjInfo, err *msg.ErrorInfo) (objs []*msg.StaticObjInfo) {
	troopConfigData, ret := conf.GetTroopLevelUpConfig(troopObj.Level)
	if troopConfigData == nil || ret == false {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "troop level config error"
		return
	}

	staticObj := MakeStaticObj(int32(msg.CurrencyId_CI_Gold),
		int32(msg.ObjType_OT_Currency),
		int64(troopConfigData.RestGoldValue))

	objs = append(objs, staticObj)

	staticObj = MakeStaticObj(int32(msg.CurrencyId_CI_TroopExp),
		int32(msg.ObjType_OT_Currency),
		int64(troopConfigData.RestTotalExp))

	if staticObj.Count > 0 {
		objs = append(objs, staticObj)
	}

	return
}

//TODO GR3版本先不做了
func calcResetStar(troopObj *msg.ObjInfo, err *msg.ErrorInfo) (objs []*msg.StaticObjInfo, result bool) {

	troopStarConf, ret := conf.GetTroopStarConfig(troopObj.ConfigId)
	if troopStarConf == nil || ret == false {
		return nil, false
	}

	objs, _ = JsonToPb_StaticObjs(troopStarConf.RestReward, troopStarConf, troopStarConf.Id, err)
	if objs == nil || err.Id != 0 {
		return nil, false
	}
	troopObj.Star = troopStarConf.RestStar
	return objs, true
}

func calcResetTalent(r *msg.PlayerInfo, troopObj *msg.ObjInfo, err *msg.ErrorInfo) (objs []*msg.StaticObjInfo) {
	troopExt := GetTroopExt(r, troopObj.Id)
	if troopExt == nil {
		return nil //可能正常就是没天赋
	}

	for treeId, v := range troopExt.TalentTrees { //???nil 咋办
		for talentId, _ := range v.Talents {
			troopTalentLevelConf, _ := GetTalentLevelConfData(r, troopObj.Id, treeId, talentId, err)
			if err.Id != 0 {
				return
			}
			//troopObj.Extend1 = 1
			units, _ := JsonToPb_StaticObjs(troopTalentLevelConf.RestCost, troopTalentLevelConf, troopTalentLevelConf.Id, err)
			if err.Id != 0 {
				return
			}
			objs = append(objs, units...)
			//talentLevel = 1
		}
	}
	return objs
}

// 返回已重置的东西
func ResetTroop(r *msg.PlayerInfo, id int32, err *msg.ErrorInfo) (objInfos []*msg.ObjInfo) {
	troopObj := GetTroop(r, id, err)
	if err.Id != 0 {
		return
	}

	var addStaticObjs []*msg.StaticObjInfo
	//重置等级返还
	addObjs := calcResetLevel(troopObj, err)
	if err.Id != 0 {
		return
	}
	// addObjs maybe nil
	addStaticObjs = append(addStaticObjs, addObjs...)
	troopObj.Level = 1

	if ExtTroop(r).TroopsExt != nil {
		// sync equip owner
		if ext, ok := ExtTroop(r).TroopsExt[id]; ok {
			if len(ext.Equips) > 0 {
				objInfos = UnEquipOnDelTroop(r, ext)
			}
		}

		// 天赋重置返还
		addObjs = calcResetTalent(r, troopObj, err)
		if err.Id != 0 {
			return
		}
		addStaticObjs = append(addStaticObjs, addObjs...)
	}

	addObjInfos, _ := AddStaticObjs(r, addStaticObjs, err)
	if err.Id != 0 {
		return
	}
	objInfos = append(objInfos, addObjInfos...)
	delete(ExtTroop(r).TroopsExt, id)
	return
}

func LayOff(r *msg.PlayerInfo, troopId int32, troopConfigId int32, err *msg.ErrorInfo) (objInfos []*msg.ObjInfo) {
	//log.Debugf("LayOff troopId %+v", troopId)
	objInfos = ResetTroop(r, troopId, err)
	if err.Id != 0 {
		return
	}

	troopConf, ret := conf.GetTroopConfig(troopConfigId)
	if troopConf.CanDismiss == false || ret == false {
		return
	}

	units, _ := JsonToPb_StaticObjs(troopConf.DismissReward, troopConf, troopConf.Id, err)
	if units == nil || err.Id != 0 {
		return
	}

	objs, _ := AddStaticObjs(r, units, err)
	if objs == nil || err.Id != 0 {
		return
	}

	objInfos = append(objInfos, objs...)

	return
}

func CheckResetTroopCondition(troop *msg.ObjInfo) bool {
	globalConf, ok := conf.GetGlobalConfig(1)
	if !ok {
		return false
	}
	if troop.Level < globalConf.HeroRestNeedLv {
		return false
	}
	return true
}

func GetResetTroopConsume() (unit *msg.StaticObjInfo) {
	globalConf, ok := conf.GetGlobalConfig(1)
	if !ok {
		return
	}
	unit = &msg.StaticObjInfo{
		Id:    int32(msg.CurrencyId_CI_Diamond),
		Type:  int32(msg.ObjType_OT_Currency),
		Count: int64(globalConf.HeroRestNeedCost),
	}
	return
}

func MakeStaticObj(staticObjId int32, staticObjType int32, staticObjCount int64) (staticObj *msg.StaticObjInfo) {
	staticObj = &msg.StaticObjInfo{
		Id:    staticObjId,
		Type:  staticObjType,
		Count: staticObjCount,
	}
	return
}

// 尝试新增到图鉴里
func TryAddIntoTroopBook(r *msg.PlayerInfo, configId int32) {
	makesureTroopBook(r)
	if _, ok := r.TroopBook[configId]; !ok {
		// 初始化为没领奖
		r.TroopBook[configId] = false

		// temp data, just like context
		ctxNewTroopConfigIds, ok := GetCtxValue(r, KeyCtxNewTroopConfigIds, map[int32]bool{}).(map[int32]bool)
		if ok {
			ctxNewTroopConfigIds[configId] = true
			SetCtxValue(r, KeyCtxNewTroopConfigIds, ctxNewTroopConfigIds)
		}
	}
}

func GetTroopPower(r *msg.PlayerInfo, id int32) int32 {
	troop := GetTroop(r, id, &msg.ErrorInfo{})
	if troop != nil {
		return troop.Power
	}
	return 1
}

// 当英雄培养时，刷新英雄战力
func RefreshTroopPowerById(r *msg.PlayerInfo, id int32, refreshFormation bool) {
	t := GetTroop(r, id, &msg.ErrorInfo{})
	if t != nil {
		RefreshTroopPower(r, t, refreshFormation)
	}
}

// 时机：
// 英雄 装备 天赋
// 羁绊等
func RefreshTroopPower(r *msg.PlayerInfo, troop *msg.ObjInfo, refreshFormation bool) {
	if troop == nil || troop.Type != msg.ObjType_OT_Troop {
		return
	}

	// 这一堆计算问题不大，10w qps
	h := NewHeroPropertyCalculator(r, troop)
	h.CalcWithFightPower()
	troop.Power = h.FightPower

	if refreshFormation {
		RefreshFormationPowerByTroopId(r, troop.Id)
	}

	log.Debugf("troop(%d) Power(%d)", troop.Id, troop.Power)
}

func RefreshFormationPowerByTroopId(r *msg.PlayerInfo, troopId int32) {
	for t, f := range r.Formations {
		exist := util.InArray32(f.TroopIds, troopId)
		if exist {
			RefreshFormationPower(r, msg.FormationType(t))
		}
	}
}

func RefreshTotalPower(r *msg.PlayerInfo) {
	c := int32(0)
	troops := ExtTroop(r).Troops
	for _, v := range troops {
		c += v.Power
	}
	old := r.TotalPower
	r.TotalPower = c

	// notify if changed
	if old != r.TotalPower {
		ctx := xmiddleware.GetCtxByUid(r.Id)
		if ctx != nil {
			n := &msg.AllPowerNotify{Power: r.TotalPower}
			current.MustAppendAdditionalOutgoingMessage(ctx, n)
		}
	}
}

func RefreshFormationPowerAll(r *msg.PlayerInfo) {
	RefreshFormationPower(r, msg.FormationType_FT_MainChapter)
	RefreshFormationPower(r, msg.FormationType_FT_ArenaDef)
}

// 刷新主线或竞技场防守阵容的战力总值
func RefreshFormationPower(r *msg.PlayerInfo, t msg.FormationType) {
	if t != msg.FormationType_FT_MainChapter &&
		t != msg.FormationType_FT_ArenaDef {
		return
	}
	f, ok := r.Formations[int32(t)]
	if ok && f != nil {
		s := int32(0)
		heroNums := int32(0)
		err := &msg.ErrorInfo{}
		for i := 0; i < len(f.TroopIds); i++ {
			id := f.TroopIds[i]
			if id == 0 {
				continue
			}
			troop := GetTroop(r, id, err)
			if troop != nil {
				s += troop.Power
				heroNums++
			}
		}

		log.Debugf("formation(%v) Power(%d)", t, s)

		if t == msg.FormationType_FT_MainChapter {
			r.MainPower = s
		} else if t == msg.FormationType_FT_ArenaDef {
			if r.DefPower != s {
				r.DefPower = s

				// 防守战力更改时，尝试刷新排行榜
				TryUpdateTeamPowerRankOfRecentServer(r, heroNums, r.DefPower)
			}
		}
	}
}

func GetTroopSpecialty(troop *msg.ObjInfo, specialtyId int32) *msg.IntPair {
	for _, v := range troop.Pairs {
		if v.Key == specialtyId {
			return v
		}
	}
	return nil
}

func AddSpecialtyResetAward(r *msg.PlayerInfo, id int32, err *msg.ErrorInfo) []*msg.ObjInfo {
	t := GetTroop(r, id, err)
	if err.Id != 0 {
		return nil
	}
	specialtyAwards := GetTroopSpecialtyResetCost(t)
	if len(specialtyAwards) > 0 {
		tempAwards, _ := AddStaticObjs(r, specialtyAwards, err)
		if err.Id != 0 {
			return nil
		}
		return tempAwards
	}
	return nil
}

// 英雄被删除的时候，调用 -- 实际是重置返还，名字有问题
func GetTroopSpecialtyResetCost(troop *msg.ObjInfo) []*msg.StaticObjInfo {
	// 弱错误处理
	ret := make([]*msg.StaticObjInfo, 0)
	for i := 0; i < len(troop.Pairs); i++ {
		p := troop.Pairs[i]
		// key value -> id, level 等级为1其实无返还
		if p.Value > 1 {
			specialtyConf, _ := conf.GetSpecialtyConfig(p.Key)
			if specialtyConf == nil {
				continue
			}
			levelId, ok := specialtyConf.SpecialtyLevelCfgs[p.Value]
			if !ok {
				continue
			}
			specialtyLevelConf, _ := conf.GetSpecialtyLevelConfig(levelId)
			if specialtyLevelConf == nil {
				continue
			}

			restCost, ok := JsonToPb_StaticObjs(specialtyLevelConf.ResetCost, specialtyLevelConf, levelId, &msg.ErrorInfo{})
			if !ok {
				continue
			}
			ret = append(ret, restCost...)
			ret = MergeStaticObjInfo(ret)
		}
	}
	return ret
}

// 简化了错误处理
func initTroopSpecialty(r *msg.PlayerInfo, troop *msg.ObjInfo) {
	if troop.Type != msg.ObjType_OT_Troop {
		return
	}
	// 已有数据
	if len(troop.Pairs) > 0 {
		return
	}
	troopConf, _ := conf.GetTroopConfig(troop.ConfigId)
	var tempError msg.ErrorInfo
	if !CheckConfData(troopConf, troop.ConfigId, &tempError) {
		log.Error(tempError)
		return
	}

	// 默认全部开启，均为1级
	for _, specialId := range troopConf.Specialty {
		s := &msg.IntPair{
			Key:   specialId,
			Value: 1,
		}
		troop.Pairs = append(troop.Pairs, s)
	}
}

func ValidFormationAndTrySync(ctx context.Context, r *msg.PlayerInfo) {
	needSync := false
	for _, f := range r.Formations {
		troopCount := 0
		for i := 0; i < len(f.TroopIds); i++ {
			id := f.TroopIds[i]
			if id == 0 {
				continue
			}
			// 只要找到一个，说明阵容有效
			if _, ok := ExtTroop(r).Troops[id]; ok {
				troopCount++
				// dont break
			} else {
				// 英雄不见了，置为0
				f.TroopIds[i] = 0
				needSync = true
			}
		}

		// 空阵容
		if troopCount == 0 {
			needSync = true
			// fill a random hero
			// 竞技场防守阵容需要补齐，别的不管
			if f.Type == msg.FormationType_FT_ArenaDef {
				for _, v := range ExtTroop(r).Troops {
					// lua start from 1
					f.TroopIds[1] = v.Id
					break
				}
			}
		}
	}

	if needSync {
		n := &msg.FormationSync{
			Formations: r.Formations,
		}
		current.MustAppendAdditionalOutgoingMessage(ctx, n)
	}
}

// 纯刷相关的羁绊数据，
// 触发: 升星，遣散，以及手动配置 (获得英雄不用触发)
// 会伴随推送
func RefreshTroopRelationshipLevelByTroops(r *msg.PlayerInfo, troopConfigIds []int32) {
	relationshipIdMap := make(map[int32]bool, 0)
	for i := 0; i < len(troopConfigIds); i++ {
		troopId := troopConfigIds[i]
		relationshipId := internal.PmConf.GetRelationshipIdByTroopConfigId(troopId)
		if relationshipId != 0 {
			relationshipIdMap[relationshipId] = true
		}
	}

	relationshipIds := make([]int32, 0)
	for k, _ := range relationshipIdMap {
		relationshipIds = append(relationshipIds, k)
	}
	RefreshTroopRelationshipLevel(r, relationshipIds)
}

// 该函数不负责新增
// 外界英雄已经放上去了，，此处判断如果不存在，再卸下来
// 会伴随推送
func RefreshTroopRelationshipLevel(r *msg.PlayerInfo, relationshipIds []int32) {
	if len(relationshipIds) == 0 {
		return
	}

	notify := &msg.TroopRelationshipSyncNotify{
		TroopRelationships: make(map[int32]*msg.TroopRelationship),
	}

	ext := MustGetPlayerInfoExt(r)
	if ext.TroopRelationships == nil {
		ext.TroopRelationships = make(map[int32]*msg.TroopRelationship)
	}

	for i := 0; i < len(relationshipIds); i++ {
		id := relationshipIds[i]
		confData, _ := conf.GetTroopRelationshipConfig(id)
		if confData == nil {
			log.Errorf("GetTroopRelationshipConfig(%d) failed", id)
			continue
		}

		// 计算等级，，战力不在此处计算，暂时忽略战力计算
		if d, ok := ext.TroopRelationships[id]; ok {
			// 获得最小星级
			minStar := int32(100)
			// 尝试卸下已经不存在的英雄 及配表不一致的英雄
			tryClearUnexistTroopRelationship(d, r)
			// 英雄数不够
			if len(d.TroopData) < len(confData.NeedHero) {
				minStar = 0
			} else {
				// 不考虑troopData > NeedHero的情况！
				// 如果哪天配表有类似变化，再调整
				for troopConfigId, troopId := range d.TroopData {
					_ = troopConfigId
					// 主要有一个有空缺就不激活
					if troopId == 0 {
						minStar = 0
						break
					}

					t := GetTroop(r, troopId, &msg.ErrorInfo{})
					if t == nil {
						panic("should never go here")
					}
					if t.Star < minStar {
						minStar = t.Star
					}
				}
			}

			// 根据需求星级计算等级
			d.Level = 0
			// 此处是数组，，索引是等级-1
			for lvIndex, needStar := range confData.NeedStar {
				if minStar < needStar {
					break
				}
				d.Level = int32(lvIndex) + 1
			}

			notify.TroopRelationships[d.Id] = d
		} else {
			// do nth
		}
	}

	// TODO 伴随的战力刷新后续再做，暂时先不刷新
	// 对某一类英雄的属性会有enhance想过

	if len(notify.TroopRelationships) > 0 {
		ctx := xmiddleware.GetCtxByUid(r.Id)
		if ctx != nil {
			current.MustAppendAdditionalOutgoingMessage(ctx, notify)
		}
	}
}

// 检测配表一致性
// 检测过气英雄
func tryClearUnexistTroopRelationship(d *msg.TroopRelationship, r *msg.PlayerInfo) {
	confData, _ := conf.GetTroopRelationshipConfig(d.Id)

	for troopConfigId, troopId := range d.TroopData {
		if troopId == 0 {
			continue
		}

		// 配表有改动
		if !util.InArray32(confData.NeedHero, troopConfigId) {
			delete(d.TroopData, troopConfigId)
			continue
		}

		t := GetTroop(r, troopId, &msg.ErrorInfo{})
		if t == nil {
			// golang map 循环内自赋值安全
			d.TroopData[troopConfigId] = 0
		}
	}
}

// 用于筛选数据，并推送给前端
// 0模式算一种类型，flag按类型处理，不是位标记 0:normal 1:recruit
func GetTroopsByFlag(r *msg.PlayerInfo, flag int32) map[int32]*msg.ObjInfo {
	ret := make(map[int32]*msg.ObjInfo)
	for k, v := range ExtTroop(r).Troops {
		if v.Flag == flag {
			ret[k] = v
		}
	}
	return ret
}

// 用于筛选数据，并推送给前端
func GetTroopsExtByFlag(r *msg.PlayerInfo, flag int32) map[int32]*msg.TroopExtInfo {
	ret := make(map[int32]*msg.TroopExtInfo)
	for k, v := range ExtTroop(r).TroopsExt {
		v2, _ := ExtTroop(r).Troops[k]
		if v2 != nil && v2.Flag == flag {
			ret[k] = v
		}
	}
	return ret
}

// 用于筛选数据，并推送给前端
func GetEquipsByFlag(r *msg.PlayerInfo, flag int32) map[int32]*msg.ObjInfo {
	ret := make(map[int32]*msg.ObjInfo)
	for k, v := range ExtEquip(r).Equips {
		if v.Flag == flag {
			ret[k] = v
		}
	}
	return ret
}

// 根据特定flag对应的清空数据
func ClearDataByFlag(r *msg.PlayerInfo, flag int32) {
	// order!!!
	clearTroopsExtByFlag(r, flag)
	clearEquipsByFlag(r, flag)
	clearTroopsByFlag(r, flag)
}

func clearTroopsByFlag(r *msg.PlayerInfo, flag int32) {
	datas := ExtTroop(r).Troops
	for k, v := range datas {
		if v.Flag == flag {
			delete(datas, k)
		}
	}
}

// 用于筛选数据，并推送给前端
func clearTroopsExtByFlag(r *msg.PlayerInfo, flag int32) {
	datas := ExtTroop(r).TroopsExt
	baseDatas := ExtTroop(r).Troops
	for k, _ := range datas {
		v2, _ := baseDatas[k]
		if v2 != nil && v2.Flag == flag {
			delete(datas, k)
		}
	}
}

// 用于筛选数据，并推送给前端
func clearEquipsByFlag(r *msg.PlayerInfo, flag int32) {
	datas := ExtEquip(r).Equips
	for k, v := range datas {
		if v.Flag == flag {
			delete(datas, k)
		}
	}
}

func GetTroopHp(r *msg.PlayerInfo, troopId int32) int32 {
	return 0
}

type troopByPower struct {
	troop *msg.ObjInfo
}
type Troops []troopByPower

func (p Troops) Len() int           { return len(p) }
func (p Troops) Less(i, j int) bool { return p[i].troop.Power > p[j].troop.Power }
func (p Troops) Swap(i, j int)      { p[i], p[j] = p[j], p[i] }

// 获取战力前N名的英雄信息
func GetTroopsIdByMaxPower(r *msg.PlayerInfo, num int) *TroopInfos {
	var ids []int32
	objs := GetTroopsByMaxPower(r, num)
	for _, v := range objs {
		ids = append(ids, v.Id)
	}
	return GetTroopInfosForClient(r, ids)
}

func GetTroopsByMaxPower(r *msg.PlayerInfo, num int) (objs []*msg.ObjInfo) {
	allTroops := GetTroopsByFlag(r, int32(msg.TroopFlagType_TF_Normal))
	if len(allTroops) == 0 {
		return nil
	}

	var troopSlice Troops
	for _, v := range allTroops {
		t := troopByPower{
			troop: v,
		}
		troopSlice = append(troopSlice, t)
	}
	if len(troopSlice) == 0 {
		return nil
	}

	sort.Sort(troopSlice)

	maxLen := len(troopSlice)
	if maxLen > num {
		maxLen = num
	}

	for i := 0; i < maxLen; i++ {
		objs = append(objs, troopSlice[i].troop)
	}
	return
}

// 得到特定集合的相关英雄数据
// 引用方式，切勿随便修改，如需修改请deepcopy
type TroopInfos struct {
	Troops    map[int32]*msg.ObjInfo
	Equips    map[int32]*msg.ObjInfo
	TroopExts map[int32]*msg.TroopExtInfo
}

// 英雄相关信息
type TroopInfo struct {
	Troop    *msg.ObjInfo
	Equips   map[int32]*msg.ObjInfo
	TroopExt *msg.TroopExtInfo
}

// 得到英雄相关全信息
// 引用返回！！！切勿轻易修改！！！
func GetTroopInfos(r *msg.PlayerInfo, troopIds []int32) []*TroopInfo {
	ret := make([]*TroopInfo, 0)
	for _, id := range troopIds {
		t := GetTroop(r, id, &msg.ErrorInfo{})
		if t == nil {
			continue
		}

		ti := &TroopInfo{}
		ti.Troop = t
		ti.TroopExt = GetTroopExt(r, id)
		ti.Equips = make(map[int32]*msg.ObjInfo)
		if ti.TroopExt != nil && len(ti.TroopExt.Equips) > 0 {
			for _, equipId := range ti.TroopExt.Equips {
				if equipId != 0 {
					ti.Equips[equipId] = GetEquip(r, equipId, &msg.ErrorInfo{})
				}
			}
		}

		ret = append(ret, ti)
	}

	return ret
}

// 得到英雄相关全信息，作为原英雄数据的一个子集
// 引用返回！！！切勿轻易修改！！！
func GetTroopInfosForClient(r *msg.PlayerInfo, troopIds []int32) *TroopInfos {
	ret := &TroopInfos{
		Troops:    make(map[int32]*msg.ObjInfo),
		TroopExts: make(map[int32]*msg.TroopExtInfo),
		Equips:    make(map[int32]*msg.ObjInfo),
	}

	tis := GetTroopInfos(r, troopIds)
	for _, ti := range tis {
		if ti == nil {
			continue
		}

		// merge
		ret.Troops[ti.Troop.Id] = ti.Troop

		if ti.TroopExt != nil {
			ret.TroopExts[ti.Troop.Id] = ti.TroopExt
		}

		for equipId, v := range ti.Equips {
			ret.Equips[equipId] = v
		}
	}
	return ret
}

// 返回不能解散的英雄和已遣散后的其他奖励
func TryAutoDismissTroop(r *msg.PlayerInfo, addedObjs []*msg.ObjInfo) []*msg.ObjInfo {
	var ret []*msg.ObjInfo
	for i := 0; i < len(addedObjs); i++ {
		o := addedObjs[i]
		if o.Type != msg.ObjType_OT_Troop {
			ret = append(ret, o)
			continue
		}

		tc, _ := conf.GetTroopConfig(o.ConfigId)
		if tc == nil {
			// error..
			continue
		}

		if !gconst.TroopCanDismiss(tc.BaseStar) {
			ret = append(ret, o)
			continue
		}

		// layoff
		tempRet := LayOff(r, o.Id, o.ConfigId, &msg.ErrorInfo{})
		ret = append(ret, tempRet...)
	}

	return ret
}

// handler里有可能新增英雄的地方，需要加这个协议
//func TrySyncNewTroops(r *msg.PlayerInfo) {
//	if len(r.CtxNewTroopConfigIds) > 0 {
//		n := &msg.NewTroopsSyncNotify{
//			NewTroops: r.CtxNewTroopConfigIds,
//		}
//		ctx := xmiddleware.GetCtxByUid(r.Id)
//		current.MustAppendAdditionalOutgoingMessage(ctx, n)
//	}
//}

func GetTroopAnyway(r *msg.PlayerInfo, id int32, err *msg.ErrorInfo) *msg.ObjInfo {
	d := GetTroop(r, id, err)
	if err.Id == 0 {
		return d
	}
	err.Id, err.DebugMsg = 0, ""
	d = GetMazeTroop(r, id, err)
	if err.Id == 0 {
		return d
	}
	err.Id, err.DebugMsg = msg.Ecode_INVALID, "maze no troop"
	return nil
}
