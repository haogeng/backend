package game

import (
	"server/internal/log"
	"server/internal/util/time3"
	"server/pkg/gen/conf"
	"server/pkg/gen/msg"
	"server/pkg/gen/rawdata"
)

const (
	TechTypeHangup int32 = 1
	TechTypeAttr   int32 = 2
)

func DealTechForClient(m *msg.TechInfo, nowTime int64) {
	techConf, _ := conf.GetTechnologyConfig(int32(m.Id))
	if !CheckConfData(techConf, int32(m.Id), nil) {
		return
	}

	c, _ := GetTechLevelConf(techConf, m.Level, nil)
	if !CheckConfData(techConf, m.Level, nil) {
		return
	}

	//startTime := time3.Now().Unix()
	if m.State == msg.TechState_TECH_UPGRADING {
		m.PassedTime = int32(nowTime - m.StartTime)
		m.UpgradeTotalTime = c.Time
	}
}

func CheckTechState(tech *msg.TechInfo, needState msg.TechState, err *msg.ErrorInfo) {
	if tech.State != needState {
		log.Debugf("CheckTechState tech state:%+v, needState:%+v", tech.State, needState)
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "state err"
	}
	return
}

func CheckTechCondition(tech *msg.TechInfo, bconf *rawdata.TechnologyConfig, r *msg.PlayerInfo, err *msg.ErrorInfo) bool {
	levelConf, ok := GetTechLevelConf(bconf, tech.Level, err)
	if !ok {
		return false
	}

	return CheckConditions(r, levelConf.Condition, err)
}

func CalcTechConsume(tech *msg.TechInfo, bconf *rawdata.TechnologyConfig, consume string, err *msg.ErrorInfo) (objs []*msg.StaticObjInfo) {
	levelConf, _ := GetTechLevelConf(bconf, tech.Level, err)
	if err.Id != 0 {
		return
	}

	if !CheckConfData(levelConf, tech.Level, err) {
		return
	}

	if err.Id != 0 {
		return
	}

	objs, _ = JsonToPb_StaticObjs(consume, levelConf, levelConf.Id, err)
	if err.Id != 0 {
		return
	}
	return
}

func CheckTechConsume(consumeUnits []*msg.StaticObjInfo, r *msg.PlayerInfo, err *msg.ErrorInfo) {
	checkAndSubTechConsume(consumeUnits, r, false, err)
	if err.Id != 0 {
		return
	}
	return
}
func SubTechConsume(consumeUnits []*msg.StaticObjInfo, r *msg.PlayerInfo, err *msg.ErrorInfo) {
	checkAndSubTechConsume(consumeUnits, r, true, err)
	if err.Id != 0 {
		return
	}
	return
}

// 检查建筑队列
func CheckTechQueue(r *msg.PlayerInfo, err *msg.ErrorInfo) bool {
	if r.TechQueueCount >= r.MaxTechQueueCount {
		err.Id, err.DebugMsg = 1016, "tech queue upto max"
		return false
	}
	return true
}
func checkAndSubTechConsume(consumeUnits []*msg.StaticObjInfo, r *msg.PlayerInfo, doSub bool, err *msg.ErrorInfo) {
	CheckConsumes(r, consumeUnits, err)
	if err.Id != 0 {
		return
	}

	if doSub {
		SubConsumes(r, consumeUnits, err)
		if err.Id != 0 {
			return
		}
	}
	return
}

func GetLevelConfByTech(tech *msg.TechInfo, err *msg.ErrorInfo) (levelConf *rawdata.TechnologyLevelConfig, ok bool) {
	if tech == nil {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "not find tech"
		return
	}

	techConf, _ := conf.GetTechnologyConfig(int32(tech.Id))
	return GetTechLevelConf(techConf, tech.Level, err)
}

func GetTechByEffectId(r *msg.PlayerInfo, effectId int32) (techInfos []*msg.TechInfo, ok bool) {
	effectConf, ret := conf.GetTechnologyEffectConfig(effectId)
	if effectConf == nil || ret == false {
		return nil, false
	}
	for id := 0; id < len(effectConf.TechnologyId); id++ {
		tech := GetTechInfo(r, effectConf.TechnologyId[id])
		if tech == nil {
			continue
		}
		techInfos = append(techInfos, tech)
	}
	return techInfos, true
}

// levelup: 0->1 用0去查
func GetTechLevelConf(bconf *rawdata.TechnologyConfig, level int32, err *msg.ErrorInfo) (levelConf *rawdata.TechnologyLevelConfig, ok bool) {
	levelConfId, ok := bconf.LevelData[level]
	if ok {
		levelConf, ok = conf.GetTechnologyLevelConfig(levelConfId)
	}

	if !ok {
		if err != nil {
			err.Id, err.DebugMsg = msg.Ecode_INVALID, "conf err"
		}
	}
	return
}

func MakeSureTechExist(r *msg.PlayerInfo) bool {
	if r.TechInfos == nil {
		r.TechInfos = make(map[int32]*msg.TechInfo)
		var techInfoTmp *msg.TechInfo = &msg.TechInfo{
			Id:    1,
			Level: 0,
			State: msg.TechState_TECH_LOCK,
		}
		r.TechInfos[1] = techInfoTmp
	}
	return true
}

func AddTech(r *msg.PlayerInfo, techId int32, err *msg.ErrorInfo) (techInfo *msg.TechInfo) {
	if !MakeSureTechExist(r) {
		return
	}

	techInfo = GetTechInfo(r, techId)
	if techInfo != nil {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "AddTech error"
		return
	}
	techInfo = &msg.TechInfo{
		Id:    uint32(techId),
		Level: 0,
	}
	r.TechInfos[int32(techInfo.Id)] = techInfo

	return
}

func GetTechInfo(r *msg.PlayerInfo, techId int32) (ret *msg.TechInfo) {
	if r.TechInfos == nil {
		return
	}

	if v, ok := r.TechInfos[techId]; ok {
		ret = v
		return
	}
	return
}

func GetTechInfos(r *msg.PlayerInfo) map[int32]*msg.TechInfo {
	if nil == r.TechInfos {
		MakeSureTechExist(r)
	}
	return r.TechInfos
}

func GetTechByEffect(r *msg.PlayerInfo, t int32) *msg.TechInfo {
	return nil
}

func CheckTechData(techId int32, techLevel int32, startTime int64, res *msg.ErrorInfo, instant bool) {
	techData, _ := conf.GetTechnologyConfig(techId)

	CheckConfData(techData, int32(techId), res)
	if res.Id != 0 {
		return
	}

	if techLevel >= techData.MaxLevel {
		res.Id, res.DebugMsg = msg.Ecode_INVALID, "tech upto max level"
		return
	}

	levelConf, _ := GetTechLevelConf(techData, techLevel, res)

	if res.Id != 0 {
		return
	}

	// judge time
	if false == instant {
		CheckExpireWithTolerance(startTime, levelConf.Time, KDefaultToleranceSecond, res)
		if res.Id != 0 {
			return
		}
	}

	return
}

func DoLevelUp(r *msg.PlayerInfo, techInfo *msg.TechInfo, err *msg.ErrorInfo) {
	nextLevel := techInfo.Level + 1
	techConf, _ := conf.GetTechnologyConfig(int32(nextLevel))
	if techConf == nil {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "techConf nil "
		return
	}
	if !CheckConfData(techConf, int32(nextLevel), nil) {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "Technology checkConfData err"
		return
	}

	c, _ := GetTechLevelConf(techConf, nextLevel, err)
	if err.Id != 0 {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "Technology levelData err"
		return
	}

	// 只有升级中才会有变化
	if techInfo.State == msg.TechState_TECH_UPGRADING {
		r.TechQueueCount--
		if r.TechQueueCount < 0 {
			r.TechQueueCount = 0
		}
	}

	techInfo.UpgradeTotalTime = c.Time
	techInfo.Level = nextLevel
	techInfo.State = msg.TechState_TECH_NORMAL
	techInfo.StartTime = 0
	techInfo.PassedTime = 0

	// Notice ORDER!!!
	OnTechUpgraded(r, techInfo)

	//game.RefreshTaskNew(r, msg.TaskCondType_TCT_TechLevel, techInfo.Id)
	//game.RefreshTaskCount(r, msg.TaskCondType_TCT_TechLevel, techInfo.Id, 0, int64(building.Level))
}

// 条件： 0->1 1->n
// 会触发一些后续时间
func OnTechUpgraded(r *msg.PlayerInfo, tech *msg.TechInfo) {
	confData, _ := conf.GetTechnologyConfig(int32(tech.Id))
	//
	if confData == nil {
		panic("tech conf is nil")
	}
	t := confData.Type
	switch t {
	case TechTypeHangup:
		// from 0 -> 1
		//RefreshBounty(r, true, false, false)
		//NotifyBountyInfo(r)
		break
	case TechTypeAttr:
		// from 0 ->777777 1
		//techEffectOutput(r)
		break
	}
}

func GetAllTechnologyConfig(key int32, val *rawdata.TechnologyConfig) bool {
	log.Debugf("key %+v", key)
	log.Debugf("val: %+v", val)
	return true
}

func AcademyRefresh(r *msg.PlayerInfo) {
	if r.TechInfos == nil {
		return
	}

	/*res := &msg.TechInfosA{
		Err:       &msg.ErrorInfo{},
		TechInfos: nil,
	}
	techInfos := make(map[int32]*msg.TechInfo)
	res.TechInfos = techInfos
	conf.RangeTechnologyConfigMap(GetAllTechnologyConfig)*/
}

func UpdateTechLevel(r *msg.PlayerInfo, err *msg.ErrorInfo) {
	now := time3.Now().Unix()

	for _, techInfo := range r.TechInfos {
		//log.Debugf("techId:%+v,techInfo:%+v",techId,techInfo)
		if techInfo.State != msg.TechState_TECH_UPGRADING {
			continue
		}

		if now-techInfo.StartTime < int64(techInfo.UpgradeTotalTime) {
			continue
		}

		DoLevelUp(r, techInfo, err)
		if err.Id != 0 {
			return
		}
	}
}
