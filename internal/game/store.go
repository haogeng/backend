package game

import (
	"server/internal/gconst"
	"server/internal/log"
	"server/internal/util"
	"server/internal/util/time3"
	"server/pkg/gen/conf"
	"server/pkg/gen/msg"
	"server/pkg/gen/rawdata"
)

const (
	// 假设最大100级
	maxShelfLvInOneOrder = 100
)

// 逻辑的部分都不管存档到db
// 便于回滚和数据重用

func makeSureStoreExist(r2 *msg.PlayerInfoExt) {
	if r2.StoreInfo == nil {
		r2.StoreInfo = &msg.StoreInfo{}
	}
	if r2.StoreInfo.Stores == nil {
		r2.StoreInfo.Stores = make(map[int32]*msg.StoreItem)
	}
}

func makeSureShelfsExist(s *msg.StoreItem) {
	if s.Shelfs == nil {
		s.Shelfs = make([]*msg.ShelfItem, 0)
	}
}

// on create
func initStore(r *msg.PlayerInfo) {
	// 遍历config，刷新商店开启逻辑
	TryTriggerNewStore(r, false)
}

// 暂时只在主线进度里和大厅等级变化时刷新
// 尝试触发商店开启
// TODO 主线进度
func TryTriggerNewStore(r *msg.PlayerInfo, notify bool) {
	// order
	r2 := MustGetPlayerInfoExt(r)

	log.Debugf("r2:%+v", r2)

	configs := conf.GetStoreConfigConf().StoreConfigs
	for k, v := range configs {
		if _, exist := r2.StoreInfo.Stores[k]; exist {
			continue
		}

		tempMap := make(map[int32][]int32)
		for k, vv := range v.Condition {
			tempMap[k] = vv.Value
		}
		if !checkORConditions(r, tempMap) {
			continue
		}

		// add new store
		addStore(r, v, notify)
	}
}

// 暂时只在主线进度里和大厅等级变化时刷新
// 尝试触发货架开启 客户端主动刷新界面触发
//func TryTriggerNewShelf(r *msg.PlayerInfo, notify bool) {
//
//}

// fill as possible as max level shelf
// dont change existed shelf
func fillStore(r *msg.PlayerInfo, s *msg.StoreItem) {
	tempErr := &msg.ErrorInfo{}
	// 检查静态数据
	confData, _ := conf.GetStoreConfig(s.StoreId)
	if !CheckConfData(confData, s.StoreId, tempErr) {
		return
	}

	// shelfId := storeId * 10000 + shelfOrderId * 100 + lv
	var oldShelfNums int
	if s.StoreId != TravelSellerShop { //迷宫商店特殊处理
		oldShelfNums = len(s.Shelfs)
	} else {
		s.Shelfs = nil
	}

	// 简化处理，每次都从头开始
	// link shelf 是数组，保证有序
	for index, shelfStartId := range confData.LinkShelf {
		// 老的货架不动 -- 货架本身是有序的
		if index < oldShelfNums {
			continue
		}

		// 尝试fill 新的
		// 沿着startId 争取到最大

		// try get cur max
		shelfId := int32(-1)
		for lv := 0; lv < maxShelfLvInOneOrder; lv++ {
			tryId := shelfStartId + int32(lv)
			// 不存在也会返回false
			if !checkShelfCondition(r, tryId) {
				break
			}
			shelfId = tryId
		}

		// 如果startId都失败，则break 前面的货架的基础不开，，则后面的也不会开
		if shelfId == -1 {
			break
		}
		addShelf(r, s, shelfId)
	}
}

func safeRefreshInterval(interval int32) int64 {
	if interval <= 0 {
		interval = 60
		log.Error("trigger safeRefreshTime")
	}
	return int64(interval)
}

func addStore(r *msg.PlayerInfo, confData *rawdata.StoreConfig, notify bool) {
	r2 := MustGetPlayerInfoExt(r)
	s := &msg.StoreItem{
		StoreId:               confData.Id,
		AutoRefreshNextTime:   0,
		ManualRefreshCount:    0,
		ManualRefreshNextTime: 0,
		Shelfs:                make([]*msg.ShelfItem, 0),
	}

	// 刷新时间进行对齐 从今天的0点开始刷起
	curTime := time3.Now()
	startOfDay := time3.StartOfDay(curTime)

	curTimeUnix := curTime.Unix()
	startOfDayUnix := startOfDay.Unix()

	// 更新自动刷新时间 -- 如果开了app-dev里的缩短测试，这里没有做适配
	if confData.IsRefresh {
		for s.AutoRefreshNextTime = startOfDayUnix; s.AutoRefreshNextTime <= curTimeUnix; {
			s.AutoRefreshNextTime += safeRefreshInterval(confData.RefreshTime)
		}
	}

	if confData.IsManualRefresh {
		for s.ManualRefreshNextTime = startOfDayUnix; s.ManualRefreshNextTime <= curTimeUnix; {
			s.ManualRefreshNextTime += safeRefreshInterval(confData.ManualRefreshTime)
		}
	}

	if _, exist := r2.StoreInfo.Stores[s.StoreId]; exist {
		log.Error("add store repeat")
		return
	}
	r2.StoreInfo.Stores[s.StoreId] = s

	// red point -- 根据策划需求再加
	//AddRedPoint(r, msg.SystemType_ST_Store, s.StoreId, notify)
}

func addShelf(r *msg.PlayerInfo, s *msg.StoreItem, shelfId int32) {
	// fill good id
	confData, exist := conf.GetShelfConfig(shelfId)
	if !exist {
		return
	}

	shelfData := &msg.ShelfItem{
		ShelfId:  shelfId,
		GoodId:   0,
		BuyCount: 0,
	}
	goodId, ok := util.GetRandomValueByMapWeights(confData.GoodsId, true)
	if !ok {
		log.Error("GetRandomValueByMapWeights")
		return
	}
	shelfData.GoodId = goodId

	// all is ok
	makeSureShelfsExist(s)
	s.Shelfs = append(s.Shelfs, shelfData)
}

func checkShelfCondition(r *msg.PlayerInfo, shelfId int32) bool {
	// 规避pmconf里的警告
	if _, exist := conf.GetShelfConfigConf().ShelfConfigs[shelfId]; !exist {
		return false
	}

	confData, exist := conf.GetShelfConfig(shelfId)
	if !exist {
		return false
	}

	tempMap := make(map[int32][]int32)
	for k, v := range confData.Condition {
		tempMap[k] = v.Value
	}
	return checkORConditions(r, tempMap)
}

func checkORConditions(r *msg.PlayerInfo, conditions map[int32][]int32) bool {
	// 无条件满足
	if len(conditions) == 0 {
		return true
	}
	// judge cond: 条件组内与，，之间或
	// (..&&..) || (..&&..&&)
	for _, v := range conditions {
		// should ignore err
		tempErr := &msg.ErrorInfo{}
		if CheckConditions(r, v, tempErr) {
			return true
		}
	}

	return false
}

func resetStore(r *msg.PlayerInfo, s *msg.StoreItem) {
	s.Shelfs = make([]*msg.ShelfItem, 0)
	// 不做修改
	//s.AutoRefreshNextTime
	// 不做修改
	//s.ManualRefreshCount
}

func ManualRefreshStore(r *msg.PlayerInfo, s *msg.StoreItem) {
	resetStore(r, s)
	fillStore(r, s)
}

func GetStoreManualRefreshConsume(confData *rawdata.StoreConfig, designStep int32, err *msg.ErrorInfo) *msg.StaticObjInfo {
	return GetStepPriceConsume(confData.StepPriceId, designStep, err)
}

// 尝试重置手动刷新次数
func tryResetStoreManualRefreshCount(r *msg.PlayerInfo, s *msg.StoreItem) {
	confData, _ := conf.GetStoreConfig(s.StoreId)
	tempErr := &msg.ErrorInfo{}
	if !CheckConfData(confData, s.StoreId, tempErr) {
		log.Error(tempErr)
		return
	}
	if !confData.IsManualRefresh {
		return
	}

	curTime := time3.Now().Unix()
	if s.ManualRefreshNextTime < curTime+int64(KDefaultToleranceSecond) {
		//reset
		s.ManualRefreshCount = 0

		for s.ManualRefreshNextTime <= curTime {
			s.ManualRefreshNextTime += safeRefreshInterval(confData.ManualRefreshTime)
		}
	}
}

func TryRefreshStore(r *msg.PlayerInfo, id int32) {
	r2 := MustGetPlayerInfoExt(r)

	confData, _ := conf.GetStoreConfig(id)
	errInfo := &msg.ErrorInfo{}
	if !CheckConfData(confData, id, errInfo) {
		// 如果没有config数据，则清空商店
		delete(r2.StoreInfo.Stores, id)
		log.Error(errInfo)
		return
	}

	s, ok := r2.StoreInfo.Stores[id]
	if !ok {
		log.Errorf("not found store %d", id)
		return
	}

	curTime := time3.Now().Unix()
	// 自动刷新
	if confData.IsRefresh {
		if s.AutoRefreshNextTime <= curTime+gconst.StoreResetToleranceSeconds {
			// reset
			resetStore(r, s)

			// 更新到一个有效时间
			for s.AutoRefreshNextTime <= curTime {
				s.AutoRefreshNextTime = s.AutoRefreshNextTime + safeRefreshInterval(confData.RefreshTime)
			}

			// AutoRefreshNextTime 如果第一次对齐到天，此处不用再考虑对齐
		}
	}

	// 尝试重置手动刷新次数
	tryResetStoreManualRefreshCount(r, s)

	// fill store
	fillStore(r, s)
}

// 在一个触发点上去自动刷新商店,
// 时机：玩家登录，客户端拉取或触发
// 该函数也有可能新增商店！  万无一失...
func TryRefreshStores(r *msg.PlayerInfo) {
	r2 := MustGetPlayerInfoExt(r)

	TryTriggerNewStore(r, true)
	// try reset if need refresh
	for k, _ := range r2.StoreInfo.Stores {
		TryRefreshStore(r, k)
	}
}

func GetStoreItem(r *msg.PlayerInfo, id int32, err *msg.ErrorInfo) *msg.StoreItem {
	r2 := MustGetPlayerInfoExt(r)

	if s, exist := r2.StoreInfo.Stores[id]; exist {
		return s
	}
	err.Id, err.DebugMsg = msg.Ecode_INVALID, "not found store"
	return nil
}

func GetShelfItem(r *msg.PlayerInfo, storeId int32, shelfId int32, err *msg.ErrorInfo) *msg.ShelfItem {
	s := GetStoreItem(r, storeId, err)
	if err.Id != 0 {
		return nil
	}

	// 直接遍历找，其实也可以根据规则找到对应索引号，，无所谓
	index := util.SliceByCond(len(s.Shelfs), func(i int) bool {
		return s.Shelfs[i].ShelfId == shelfId
	})
	if index != -1 {
		return s.Shelfs[index]
	}

	err.Id, err.DebugMsg = msg.Ecode_INVALID, "not find shelf"
	return nil
}

// 根据配表取交集
func GetClientStoreInfoFromOrigin(origin *msg.StoreInfo, storeType msg.StoreType) (clientStoreInfo *msg.StoreInfo) {
	// Dont change origin!!!
	clientStoreInfo = &msg.StoreInfo{
		Stores: make(map[int32]*msg.StoreItem),
	}

	if origin != nil {
		for k, v := range origin.Stores {
			storeConf, exist := conf.GetStoreConfig(k)
			if exist {
				if storeConf.Type != int32(storeType) {
					continue
				}
				clientStoreInfo.Stores[k] = v
			}
			// skip not exist
		}
	}
	return
}
