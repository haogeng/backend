package game

import (
	"bitbucket.org/funplus/golib/deepcopy"
	"fmt"
	jsoniter "github.com/json-iterator/go"
	gocache "github.com/patrickmn/go-cache"
	"reflect"
	"server/internal"
	"server/internal/log"
	"server/internal/util"
	"server/internal/util/time3"
	"server/pkg/gen/conf"
	"server/pkg/gen/msg"
	"sort"
	"strings"
)

const (
	RandomBaseNum  = 10000
	OneHourSeconds = 3600

	OtIdScale = int32(10000000)
)

// 用于统一表达一些配表ID，方便和策划的错误处理整合统一处理
// 仅限于道具和货币
func GetFakeId(ot msg.ObjType, realId int32) (fakeId int32) {
	return int32(ot)*OtIdScale + realId
}
func GetReadId(fakeId int32) (ot msg.ObjType, realId int32) {
	ot = msg.ObjType(fakeId / OtIdScale)
	realId = fakeId % OtIdScale
	return
}

// 上限maxInt
// 如果有频繁生成的，需要提供一个单独的通道
// 线程不安全!
func DispatchObjIdUnsafe(r *msg.PlayerInfo, ot msg.ObjType) int32 {
	// ignore ot
	_ = ot

	r.DynamicIdSeq++

	// 给一个低的阈值，便于后续采取动作
	if r.DynamicIdSeq > 0x40000000 {
		log.Error("DynamicIdSeq will be overflow! Pls change algorithm.")
	}
	return r.DynamicIdSeq
}

func DispatchMailIdUnsafe(r2 *msg.PlayerInfoExt) int32 {
	r2.DynamicMailIdSeq++

	// 给一个低的阈值，便于后续采取动作
	if r2.DynamicMailIdSeq > 0x40000000 {
		log.Error("DynamicMailIdSeq will be overflow! Pls change algorithm.")
	}
	return r2.DynamicMailIdSeq
}

const (
	KDefaultToleranceSecond = int32(10)
)

// 开始于某个时间，是否到期
// if 到期 then return true
func CheckExpireWithTolerance(startTime int64, needTime int32, toleranceSecond int32, err *msg.ErrorInfo) bool {
	return CheckEndExpireWithTolerance(startTime+int64(needTime), toleranceSecond, err)
}

func CheckEndExpireWithTolerance(endTime int64, toleranceSecond int32, err *msg.ErrorInfo) bool {
	now := time3.Now().Unix()
	if endTime > now+int64(toleranceSecond) {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "time err"
		return false
	}
	return true
}

// 形如：
//`{
//"0": {
//	"type": 1,
//	"id": 1,
//	"count": 2000,
//	"param": 0
//},
//"1": {
//	"type": 1,
//	"id": 6,
//	"count": 2000,
//	"param": 0
//}
//}`
// use anyConfData & confId for error debug
// cache_key: class_id_column or j? j is OK. just only cal hash value.
// 使用 gocache 线程安全的缓存优化
// 存在一定可能的线程非安全的冗余加载，，没有问题
// ! return value HAS BEEN ordered by json key!
func JsonToPb_StaticObjs(j string, anyConfData interface{}, confId int32, err *msg.ErrorInfo) (units []*msg.StaticObjInfo, ok bool) {
	tempData, ok := internal.Json2pbAryCache.Get(j)
	if ok {
		if units, ok = tempData.([]*msg.StaticObjInfo); ok {
			log.Debugf("using cacheAry for %s", j)
			return
		} else {
			log.Errorf("Get cacheAry err for %s", j)
		}
	}

	units, ok = jsonToPb_StaticObjs_Impl(j, anyConfData, confId, err)
	if ok {
		// cache
		internal.Json2pbAryCache.Set(j, units, gocache.NoExpiration)
		return
	}

	return
}

// 非优化版本
func jsonToPb_StaticObjs_Impl(j string, anyConfData interface{}, confId int32, err *msg.ErrorInfo) (units []*msg.StaticObjInfo, ok bool) {
	units = make([]*msg.StaticObjInfo, 0)
	ok = true

	var json = jsoniter.ConfigCompatibleWithStandardLibrary
	var data map[string]*msg.StaticObjInfo

	if len(j) == 0 {
		return nil, true
	}

	// 必须先尝试数组
	tempErr := json.Unmarshal(util.StringToBytes(j), &data)
	if tempErr != nil {
		// 数据格式有可能是数组，有可能是单项，此处优先尝试一次单次
		var single *msg.StaticObjInfo = &msg.StaticObjInfo{}
		tempErr = json.Unmarshal(util.StringToBytes(j), single)
		if tempErr == nil {
			data = make(map[string]*msg.StaticObjInfo)
			data["0"] = single
		} else {
			log.Errorf("%T(%d) jsontopb %s err: %v\n", anyConfData, confId, j, tempErr)
			if err != nil {
				err.Id, err.DebugMsg = msg.Ecode_INVALID, "json to pb err"
				eStr := fmt.Sprintf("%T(%d) jsontopb %s err: %v\n", anyConfData, confId, j, tempErr)
				err.Params = append(err.Params, eStr)
				ok = false
				return
			}

			ok = false
			return
		}
	}

	// Must order it!
	keys := make([]string, 0)
	for k, _ := range data {
		keys = append(keys, k)
	}
	sort.Strings(keys)

	for i := 0; i < len(keys); i++ {
		units = append(units, data[keys[i]])
	}
	return
}

func JsonToPb_StaticObjsMap(j string, anyConfData interface{}, confId int32, err *msg.ErrorInfo) (units map[string]*msg.StaticObjInfo, ok bool) {
	tempData, ok := internal.Json2pbMapCache.Get(j)
	if ok {
		if units, ok = tempData.(map[string]*msg.StaticObjInfo); ok {
			log.Debugf("using cacheMap for %s", j)
			return
		} else {
			log.Errorf("Get cacheMap err for %s", j)
		}
	}

	units, ok = jsonToPb_StaticObjsMap_Impl(j, anyConfData, confId, err)
	if ok {
		// cache
		internal.Json2pbMapCache.Set(j, units, gocache.NoExpiration)
		return
	}

	return
}

func jsonToPb_StaticObjsMap_Impl(j string, anyConfData interface{}, confId int32, err *msg.ErrorInfo) (data map[string]*msg.StaticObjInfo, ok bool) {
	ok = true
	data = make(map[string]*msg.StaticObjInfo)

	var json = jsoniter.ConfigCompatibleWithStandardLibrary

	if len(j) == 0 {
		return nil, true
	}

	tempErr := json.Unmarshal(util.StringToBytes(j), &data)

	if tempErr != nil { // 数据格式有可能是数组，有可能是单项，此处优先尝试一次单次
		var single *msg.StaticObjInfo = &msg.StaticObjInfo{}
		tempErr = json.Unmarshal(util.StringToBytes(j), single)
		if tempErr == nil {
			data = make(map[string]*msg.StaticObjInfo)
			data["0"] = single
		} else {
			log.Errorf("%T(%d) jsontopb %s err: %v\n", anyConfData, confId, j, tempErr)
			if err != nil {
				err.Id, err.DebugMsg = msg.Ecode_INVALID, "json to pb err"
				eStr := fmt.Sprintf("%T(%d) jsontopb %s err: %v\n", anyConfData, confId, j, tempErr)
				err.Params = append(err.Params, eStr)
				ok = false
				return
			}
		}
		ok = false
		return
	}

	return
}

func MergeObjKey(keys []*msg.ObjKey) map[int64]*msg.ObjKey {
	m := make(map[int64]*msg.ObjKey)
	for _, o := range keys {
		key := (int64(o.Type) << 32) | int64(o.Id)
		if _, ok := m[key]; ok {
		} else {
			m[key] = &msg.ObjKey{
				Id:   o.Id,
				Type: o.Type,
			}
		}
	}
	return m
}

// 根据id和类型 合并
// key: (type<<32)|id
func MergeObjInfo(objs []*msg.ObjInfo) []*msg.ObjInfo {
	m := make(map[int64]*msg.ObjInfo)
	for _, o := range objs {
		key := (int64(o.Type) << 32) | int64(o.Id)
		if v, ok := m[key]; ok {
			v.Num += o.Num
		} else {
			v = deepcopy.Copy(o).(*msg.ObjInfo)
			m[key] = v
		}
	}

	ret := make([]*msg.ObjInfo, 0)
	for _, v := range m {
		ret = append(ret, v)
	}
	return ret
}

// 请谨慎使用！！！一般用于确定性货币
// 根据id和类型 合并,, 没考虑其他额外参数，，
// 比如不同等级或星级的英雄，会被合并
// key: (type<<32)|id
func MergeStaticObjInfo(objs []*msg.StaticObjInfo) []*msg.StaticObjInfo {
	m := make(map[int64]*msg.StaticObjInfo)
	for _, o := range objs {
		key := (int64(o.Type) << 32) | int64(o.Id)
		if v, ok := m[key]; ok {
			v.Count += o.Count
		} else {
			v = deepcopy.Copy(o).(*msg.StaticObjInfo)
			m[key] = v
		}
	}

	ret := make([]*msg.StaticObjInfo, 0)
	for _, v := range m {
		ret = append(ret, v)
	}
	return ret
}

// 所有获取到的静态数据，可通过此接口统一检查
// i 必须是一个有效的conf data的Ptr，但可以是nil
func CheckConfData(confDataPtr interface{}, id int32, err *msg.ErrorInfo) bool {
	if confDataPtr == nil || (reflect.ValueOf(confDataPtr).Kind() == reflect.Ptr && reflect.ValueOf(confDataPtr).IsNil()) {
		if err != nil {
			err.Id, err.DebugMsg = msg.Ecode_INVALID, "no conf data"
			err.Params = append(err.Params, fmt.Sprintf("not find conf: %T(%d)", confDataPtr, id))
		}

		if internal.Config.Development {
			log.Errorf("not find conf: %T(%d)", confDataPtr, id)
		}
		return false
	}
	return true
}

// 能合理处理map为nil的情况
// *msg.PlayerInfo里的map，在反序列化的时候，会反序列成空...
func AddObjIntoMap(m *map[int32]*msg.ObjInfo, o *msg.ObjInfo) {
	if *m == nil {
		*m = make(map[int32]*msg.ObjInfo)
	}

	(*m)[o.Id] = o
}

func CheckItemUseType(t msg.ItemUseType, err *msg.ErrorInfo) bool {
	if t == msg.ItemUseType_IUT_Reward ||
		t == msg.ItemUseType_IUT_Choose ||
		t == msg.ItemUseType_IUT_Drop ||
		t == msg.ItemUseType_IUT_Merge {
		return true
	}

	if err != nil {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "unknown item use type"
		err.Params = append(err.Params, fmt.Sprintf("%v", t))
		return false
	}

	return false
}

// step 是策划意义上的step，从1开始
func GetStepPriceConsume(id int32, designStep int32, err *msg.ErrorInfo) *msg.StaticObjInfo {
	confData, _ := conf.GetStepPriceConfig(id)
	if !CheckConfData(confData, id, err) {
		return nil
	}

	tempConsumes, _ := JsonToPb_StaticObjs(confData.ConsumeType, confData, id, err)
	if err.Id != 0 {
		return nil
	}
	if len(tempConsumes) != 1 {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "step price just support one consume type"
		return nil
	}

	// 需要修改
	consume := deepcopy.Copy(tempConsumes[0]).(*msg.StaticObjInfo)

	if int32(len(confData.StepPriceList)) < designStep {
		var max int32
		for k, _ := range confData.StepPriceList {
			if k > max {
				max = k
			}
		}
		designStep = max
	}

	price, ok := confData.StepPriceList[designStep]
	if !ok {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "step price err"
		return nil
	}

	consume.Count = int64(price)
	return consume
}

//
func ParseGmAddObjCmd(info string, err *msg.ErrorInfo) (ret []*msg.StaticObjInfo) {
	// debug版本加测试物品
	// 约定： 1,1,200,0,0,0|...|...
	// type,id,count,param,level,star
	strAry := strings.Split(info, "|")

	for i := 0; i < len(strAry); i++ {
		unitStr := strings.Replace(strAry[i], " ", "", -1)
		unitStr = strings.Replace(unitStr, "\t", "", -1)
		unitStr = strings.Replace(unitStr, "\r", "", -1)

		if len(unitStr) == 0 {
			continue
		}

		tempInts, tempErr := util.SplitToInt32Ary(unitStr, ",")

		if tempErr != nil {
			err.Id, err.DebugMsg = msg.Ecode_INVALID, "parse err: "+unitStr
			return
		}

		if len(tempInts) != 6 {
			err.Id, err.DebugMsg = msg.Ecode_INVALID, "len must be 6 "+unitStr
			return
		}
		// to obj
		o := &msg.StaticObjInfo{}
		o.Type = tempInts[0]
		o.Id = tempInts[1]
		o.Count = int64(tempInts[2])
		o.Civil = tempInts[3]
		o.Level = tempInts[4]
		o.Star = tempInts[5]

		ret = append(ret, o)
	}
	return
}
