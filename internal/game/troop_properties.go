package game

// from: TroopProperties.lua
// (\d*):[\s]*"(\w*)", -> Index$2 int32 = $1

// 计算战力
var TroopProperties = map[int32]string{
	1:  "AttP",               //物理攻击力
	2:  "DefP",               //物理防御力
	3:  "Hp",                 //生命值
	4:  "AttRateP",           //百分比物理攻击
	5:  "DefRateP",           //百分比物理防御
	6:  "HpRate",             //百分比生命值
	7:  "AttSpd",             //攻击速度
	8:  "MoveSpd",            //移动速度
	9:  "AttRng",             //攻击距离
	10: "CritLevel",          //暴击等级
	11: "AntiCritLevel",      //抗暴等级
	12: "CritStrength",       //暴击强度
	13: "BlockLevel",         //格挡等级
	14: "AntiBlockLevel",     //破格等级
	15: "BlockStrength",      //格挡强度
	16: "BloodRatio",         //吸血比例
	17: "BounceDamageRatio",  //反伤比例
	18: "EffectHitLevel",     //效果命中
	19: "AntiEffectHitLevel", //效果抵抗
	20: "AddDamageLevel",     //增加伤害等级
	21: "ReliefDamageLevel",  //减免伤害等级
	22: "HitLevel",           //命中等级
	23: "DodgeLevel",         //闪避等级
	24: "AddDamageLevelP",    //物理增伤等级
	25: "ReliefDamageLevelP", //物理免伤等级
	26: "AddDamageLevelM",    //魔法增伤等级
	27: "ReliefDamageLevelM", //魔法免伤等级
	28: "AttM",               //魔法攻击力
	29: "DefM",               //魔法防御力
	30: "AttRateM",           //百分比魔法攻击
	32: "DefRateM",           //百分比魔法防御
	35: "HpAddPerS",          //每秒回复生命值
	36: "HitRateCorrect",     //修正命中率
	37: "CritRateCorrect",    //修正暴击率
	38: "BlockRateCorrect",   //修正格挡率
	39: "HpAddLevel",         //回复等级
	40: "HpNow",              //当前生命值
	41: "HpRateNow",          //百分比当前生命值
	42: "DamagePlusRate",     //伤害修正率
	43: "Att",                //攻击力
	44: "TotalAttRate",       //百分比攻击
	45: "AttRate",            //百分比攻击（英雄装备）
	46: "Deprecated_energy",  //废弃的怒气值(已经不用了)
	47: "EnergyRate",         //怒气增长百分比
	48: "EnergyNow",          //当前怒气值
}

// 计算战力
const (
	IndexAttP               int32 = 1  //物理攻击力
	IndexDefP               int32 = 2  //物理防御力
	IndexHp                 int32 = 3  //生命值
	IndexAttRateP           int32 = 4  //百分比物理攻击
	IndexDefRateP           int32 = 5  //百分比物理防御
	IndexHpRate             int32 = 6  //百分比生命值
	IndexAttSpd             int32 = 7  //攻击速度
	IndexMoveSpd            int32 = 8  //移动速度
	IndexAttRng             int32 = 9  //攻击距离
	IndexCritLevel          int32 = 10 //暴击等级
	IndexAntiCritLevel      int32 = 11 //抗暴等级
	IndexCritStrength       int32 = 12 //暴击强度
	IndexBlockLevel         int32 = 13 //格挡等级
	IndexAntiBlockLevel     int32 = 14 //破格等级
	IndexBlockStrength      int32 = 15 //格挡强度
	IndexBloodRatio         int32 = 16 //吸血比例
	IndexBounceDamageRatio  int32 = 17 //反伤比例
	IndexEffectHitLevel     int32 = 18 //效果命中
	IndexAntiEffectHitLevel int32 = 19 //效果抵抗
	IndexAddDamageLevel     int32 = 20 //增加伤害等级
	IndexReliefDamageLevel  int32 = 21 //减免伤害等级
	IndexHitLevel           int32 = 22 //命中等级
	IndexDodgeLevel         int32 = 23 //闪避等级
	IndexAddDamageLevelP    int32 = 24 //物理增伤等级
	IndexReliefDamageLevelP int32 = 25 //物理免伤等级
	IndexAddDamageLevelM    int32 = 26 //魔法增伤等级
	IndexReliefDamageLevelM int32 = 27 //魔法免伤等级
	IndexAttM               int32 = 28 //魔法攻击力
	IndexDefM               int32 = 29 //魔法防御力
	IndexAttRateM           int32 = 30 //百分比魔法攻击
	IndexDefRateM           int32 = 32 //百分比魔法防御
	IndexHpAddPerS          int32 = 35 //每秒回复生命值
	IndexHitRateCorrect     int32 = 36 //修正命中率
	IndexCritRateCorrect    int32 = 37 //修正暴击率
	IndexBlockRateCorrect   int32 = 38 //修正格挡率
	IndexHpAddLevel         int32 = 39 //回复等级
	IndexHpNow              int32 = 40 //当前生命值
	IndexHpRateNow          int32 = 41 //百分比当前生命值
	IndexDamagePlusRate     int32 = 42 //伤害修正率
	IndexAtt                int32 = 43 //攻击力
	IndexTotalAttRate       int32 = 44 //百分比攻击
	IndexAttRate            int32 = 45 //百分比攻击（英雄装备）
	IndexDeprecated_energy  int32 = 46 //废弃的怒气值(已经不用了)
	IndexEnergyRate         int32 = 47 //怒气增长百分比
	IndexEnergyNow          int32 = 48 //当前怒气值
)

// 不用和前端完全一致，
// golang实现不支持定点数
var Formula1 = []int32{
	35, 7, 9, 8, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 22, 23, 20, 21, 24, 25, 26, 27, 39,
}

var Formula2 = [][]int32{
	{3, 6}, {2, 5}, {29, 32}, {43, 44},
}

var Formula3 = []int32{
	6, 4, 30, 5, 32, 44,
}

//--百分比属性，并且直接取值不用公式计算，需要除以1000
var Formula4 = []int32{
	47,
}

//--直接取值不用公式计算
var Formula5 = []int32{
	48,
}

// regex:
// local ([A-Z0-9_]*) = FixNumber\(([\d.]*)\)
// $1 = float32($2)
const (
	N1    = float32(1)
	N0_05 = float32(0.05)
	N4_5  = float32(4.5)
	N1000 = float32(1000)
	N2    = float32(2)
	N2000 = float32(2000)
	N1_5  = float32(1.5)
	N3    = float32(3)
	N4    = float32(4)
	N8    = float32(8)
	N5000 = float32(5000)
	N0_5  = float32(0.5)
	N100  = float32(100)
	N0_1  = float32(0.1)
)
