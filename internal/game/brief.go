package game

import (
	"bitbucket.org/funplus/golib/encoding2/protobuf"
	"context"
	"server/internal/share"
	"server/pkg/gen_redis/redisdb"
	"time"

	"server/internal"
	"server/pkg/cache"
	"server/pkg/gen/msg"
)

var (
	briefCacheTtl = time.Hour * 24
)

func keyOfBrief(id uint64) string {
	return cache.GetDbKey(internal.Config.DbVer(), "no_db_player_brief", id)
}

func FindPlayerBrief(id uint64) (ret *msg.PlayerBrief) {
	key := keyOfBrief(id)

	b, ok := cache.Redis.Get(key)
	if !ok || len(b) == 0 {
		// load player & update
		var c redisdb.PlayerInfoCache
		r := c.Find(id)
		if r == nil {
			return nil
		}

		ret = MustUpdatePlayerBrief(r)
		return
	}

	ret = unmarshalPlayerBrief(key, b)
	return
}

// 会更新缓存时间戳
func unmarshalPlayerBrief(key string, b []byte) *msg.PlayerBrief {
	ret := &msg.PlayerBrief{}
	err := protobuf.Codec.Unmarshal(b, ret)
	if err == nil {
		// update redis expire
		cache.Redis.Expire(key, briefCacheTtl)
	} else {
		ret = nil
	}
	return ret
}

func FindPlayerBriefs(ids []uint64) (ret []*msg.PlayerBrief) {
	// key name
	keys := make([]string, len(ids))
	for i := 0; i < len(ids); i++ {
		keys[i] = keyOfBrief(ids[i])
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()

	sc := cache.Redis.Cmd.MGet(ctx, keys...)
	results, err := sc.Result()
	if err != nil {
		return nil
	}

	ret = make([]*msg.PlayerBrief, 0, len(ids))
	for i := 0; i < len(ids); i++ {
		var d *msg.PlayerBrief
		b, ok := results[i].([]byte)
		if ok && len(b) > 0 {
			d = unmarshalPlayerBrief(keys[i], b)
		}

		if d == nil {
			d = FindPlayerBrief(ids[i])
		}

		if d != nil {
			ret = append(ret, d)
		}
	}
	return
}

func MustUpdatePlayerBrief(source *msg.PlayerInfo) *msg.PlayerBrief {
	if source == nil {
		return nil
	}

	data := &msg.PlayerBrief{
		Id:             source.Id,
		Name:           source.Name,
		Icon:           source.Icon,
		Level:          int32(GetCount_Currency(source, msg.CurrencyId_CI_PlayerLevel)),
		DefPower:       source.DefPower,
		LastLogoutTime: source.LastLogoutTime,
		ClanId:         0,
		MainPower:      source.MainPower,
		TotalPower:     source.TotalPower,
	}

	rs := share.GetClanId(source.Id)
	if rs != nil {
		data.ClanId = rs.ClanId
	}

	key := keyOfBrief(data.Id)
	b, err := protobuf.Codec.Marshal(data)
	if err != nil {
		panic("PlayerBrief marshal err")
	}

	cache.Redis.Set(key, b, briefCacheTtl)
	return data
}
