package game

import (
	"server/internal/log"
	"server/pkg/gen/conf"
	"server/pkg/gen/msg"
	"server/pkg/gen/rawdata"
	"strconv"
)

// check should be done outside
func GetDrawConsume(confData *rawdata.DrawConfig, countType msg.DrawCountType, consumeType msg.DrawConsumeType, err *msg.ErrorInfo) []*msg.StaticObjInfo {
	// check consume type
	consumeJson := ""
	if consumeType == msg.DrawConsumeType_DCT_Currency {
		consumeJson = confData.ExpendCurrency
	} else if consumeType == msg.DrawConsumeType_DCT_Item {
		consumeJson = confData.ExpendItem
	}

	if len(consumeJson) == 0 {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "need consume data in confData"
		return nil
	}

	consumeMap, _ := JsonToPb_StaticObjsMap(consumeJson, confData, confData.Id, err)
	if err.Id != 0 {
		return nil
	}

	key := strconv.Itoa(int(countType))
	if consume, ok := consumeMap[key]; !ok {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "not find consume"
		return nil
	} else {
		return []*msg.StaticObjInfo{consume}
	}
}

func DoDrawAward(r *msg.PlayerInfo, confData *rawdata.DrawConfig, countType msg.DrawCountType, err *msg.ErrorInfo) []*msg.ObjInfo {
	countAry := []int32{1, 10}
	tempIndex := int(countType)
	if tempIndex < 0 || tempIndex >= len(countAry) {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "count type err"
		return nil
	}

	//RandomAryForFake := []int32 {1, 2}
	fakeSubId := r.RandomVal%2 + 1
	randomFakeConfId := confData.Id*1000 + r.CivilId*10 + fakeSubId
	randomFakeConf, _ := conf.GetFakeRandomConfig(randomFakeConfId)
	log.Debugf("u%d randomFakeConfId: %d", r.Id, randomFakeConfId)
	// maybe randomFakeConf is nil, but its OK!

	count := countAry[tempIndex]
	var addedObjs []*msg.ObjInfo
	// Do Draw.
	// Notice order??
	for i := int32(0); i < count; i++ {
		dropId := confData.DropID
		curDrawCount := GetDrawCount(r, confData.Id) + 1

		// do fake rand; 优先级最高！
		if randomFakeConf != nil {
			tempDropId, ok := randomFakeConf.DropCountCfg[curDrawCount]
			if ok {
				log.Debugf("FakeDrop: count(%d) fakeId(%d) dropId(%d)", curDrawCount, randomFakeConfId, tempDropId)
				dropId = tempDropId
			}
		} else if false { // TODO 特定次数奖励 没多少次给个不一样的奖励
			// dropId is special dropId
		}

		// normal
		tempAdded, _ := Drop(r, dropId, err)
		if err.Id != 0 {
			return nil
		}

		addedObjs = append(addedObjs, tempAdded...)
		AddDrawCount(r, confData.Id)
	}

	return addedObjs
}

func GetDrawCount(r *msg.PlayerInfo, id int32) int32 {
	if d, ok := r.Draws[id]; ok {
		return d
	}
	return 0
}
func AddDrawCount(r *msg.PlayerInfo, id int32) {
	if _, ok := r.Draws[id]; ok {
		r.Draws[id]++
	} else {
		if r.Draws == nil {
			r.Draws = make(map[int32]int32)
		}
		r.Draws[id] = 1
	}
}
