package game

import (
	"server/internal/clan"
	"server/internal/gconst"
	"server/internal/log"
	"server/internal/share"
	"server/pkg/dlock"
	"server/pkg/fungin/xmiddleware"
	"server/pkg/gen/conf"
	"server/pkg/gen/msg"
	"server/pkg/gen/rawdata"
	"server/pkg/gen_redis/redisdb"
	"sort"
)

const (
	KSlotNumInFormation = 9
)

func ValidFormationData(r *msg.PlayerInfo) {
	if r.Formations == nil {
		r.Formations = make(map[int32]*msg.Formation)
	}

	for i := int32(0); i < int32(msg.FormationType_FT_Count); i++ {
		if _, ok := r.Formations[i]; !ok {
			r.Formations[i] = &msg.Formation{Type: msg.FormationType(i)}
		}
		// fill
		for j := len(r.Formations[i].TroopIds); j < KSlotNumInFormation; j++ {
			r.Formations[i].TroopIds = append(r.Formations[i].TroopIds, 0)
		}
	}
}

//func UpdateFormationOnTroopRemoved(r *msg.PlayerInfo, troopId int32) {
//	for _, v := range r.Formations {
//		for i := 0; i < len(v.TroopIds); i++ {
//			if v.TroopIds[i] == troopId {
//				v.TroopIds[i] = 0
//			}
//		}
//	}
//}

type HeroPower struct {
	heroId    int32
	heroPower int32
}
type HeroPowers []HeroPower

func (p HeroPowers) Len() int           { return len(p) }
func (p HeroPowers) Less(i, j int) bool { return p[i].heroPower > p[j].heroPower }
func (p HeroPowers) Swap(i, j int)      { p[i], p[j] = p[j], p[i] }

// 获取指定阵容战力最高的英雄配置ID
func SelectTroopIdByFormation(r *msg.PlayerInfo, selectType msg.FormationType) (troopIdConfigId int32) {
	heroPowers := SelectPowerInfoByFormation(r, selectType)
	if heroPowers == nil {
		return 0
	}
	troopIdConfigId = heroPowers[0].heroId
	return
}

// 获取指定阵容战力的战力总和
func SelectTroopPowerSumByFormation(r *msg.PlayerInfo, selectType msg.FormationType) (troopPowerSum int32) {
	heroPowers := SelectPowerInfoByFormation(r, selectType)
	if heroPowers == nil {
		return 0
	}
	for _, v := range heroPowers {
		troopPowerSum += v.heroPower
	}
	return
}

// 获取指定阵容的战力信息
func SelectPowerInfoByFormation(r *msg.PlayerInfo, selectType msg.FormationType) (HeroPowers HeroPowers) {
	v, ok := r.Formations[selectType.Number()]
	if !ok {
		return
	}
	for _, heroId := range v.TroopIds {
		if heroId <= 0 {
			continue
		}
		heroPower := HeroPower{
			heroId:    heroId,
			heroPower: GetTroopPower(r, heroId),
		}
		HeroPowers = append(HeroPowers, heroPower)
	}
	if len(HeroPowers) == 0 {
		return
	}
	sort.Sort(HeroPowers)
	return HeroPowers
}

func SelectFormation(r *msg.PlayerInfo, selectType msg.FormationType, err *msg.ErrorInfo) *msg.Formation {
	v, ok := r.Formations[selectType.Number()]
	if !ok {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "formation not found by type ="+selectType.String()
		return nil
	}
	return v
}

// 获取阵容的英雄个数
func GetFormationTroopSum(r *msg.PlayerInfo, form *msg.Formation, err *msg.ErrorInfo) (num int32) {
	for _, id := range form.TroopIds {
		if id <= 0 {
			continue
		}
		_ = GetTroop(r, id, err)
		if err.Id == 0 {
			num++
		}
	}
	return
}

// 检查阵容是否为空
func CheckFormationIsNull(r *msg.PlayerInfo, form *msg.Formation, err *msg.ErrorInfo) {
	// check hero exist
	for _, id := range form.TroopIds {
		if id <= 0 {
			continue
		}

		_ = GetTroop(r, id, err)
		if err.Id == 0 {
			// 英雄存在
			return
		}
	}
	err.Id, err.DebugMsg = msg.Ecode_INVALID, "Check Formation Is Null"
	return
}

func ChangeFormation(r *msg.PlayerInfo, form *msg.Formation, err *msg.ErrorInfo) {
	// check formation
	if form == nil || len(form.TroopIds) != KSlotNumInFormation {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "formation len err"
		return
	}

	// 检查保存阵容是否为空
	CheckFormationIsNull(r, form, err)
	if err.Id != 0 {
		return
	}

	// 好友展示阵容特殊处理
	if form.Type == msg.FormationType_FT_Friend {
		// 好友展示阵容必须设置5个
		if GetFormationTroopSum(r, form, err) != 5 {
			err.Id, err.DebugMsg = msg.Ecode_INVALID, "formation not is 5 troop"
			return
		}
	}

	// replace
	r.Formations[int32(form.Type)] = form
	RefreshFormationPower(r, form.Type)
}

func getValidatedTowerConf(r *msg.PlayerInfo, towerConfId int32, err *msg.ErrorInfo) *rawdata.TowerBattleConfig {
	// 没有建筑耦合了
	//b := GetBuildingByType(r, int32(msg.BuildingType_BT_Tower))
	//if b == nil || b.Level < 1 {
	//	err.Id, err.DebugMsg = msg.Ecode_INVALID, "tower locked"
	//	return nil
	//}
	// check passed id
	if towerConfId != r.PassedTowerId+1 {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "tower id err"
		return nil
	}
	// check conf data
	confData, _ := conf.GetTowerBattleConfig(towerConfId)
	if !CheckConfData(confData, towerConfId, err) {
		return nil
	}
	return confData
}

func StartTowerBattle(r *msg.PlayerInfo, req *msg.ReqStartBattleR, res *msg.ReqStartBattleA) {
	confData := getValidatedTowerConf(r, req.ConfigId, res.Err)
	if res.Err.Id != 0 {
		return
	}
	_ = confData

	// update formation
	ChangeFormation(r, req.Form, res.Err)
	if res.Err.Id != 0 {
		return
	}

	res.AdjustScale10000 = 10000
}

func EndTowerBattle(r *msg.PlayerInfo, req *msg.ReqEndBattleR, res *msg.ReqEndBattleA) {
	confData := getValidatedTowerConf(r, req.ConfigId, res.Err)
	if res.Err.Id != 0 {
		return
	}
	_ = confData

	// TODO 战斗数据校验

	if req.Result != msg.BattleResult_BR_Success {
		return
	}

	// do loot
	rewards, _ := JsonToPb_StaticObjs(confData.Reward, confData, confData.Id, res.Err)
	if res.Err.Id != 0 {
		return
	}
	addedObjs, _ := AddStaticObjs(r, rewards, res.Err)
	if res.Err.Id != 0 {
		return
	}

	// 推进进度
	r.PassedTowerId = req.ConfigId

	// TODO 刷新本服务器的排行信息

	// TODO 20200715+ 动态难度调整系统

	// set loots
	res.LootObjs = addedObjs

	// final sync inside
	ctx := xmiddleware.GetCtxByUid(r.Id)
	TryAppendSyncObjsMsgWithConsume(ctx, r, addedObjs, nil)
}

// 战役副本战斗开始
func StartWarCopyBattle(r *msg.PlayerInfo, req *msg.ReqStartBattleR, res *msg.ReqStartBattleA) {
	// 战斗数据校验
	checkResult, _, _, _ := CheckWarCopyBattleStatus(r, req.ConfigId, res.Err)
	if res.Err.Id != 0 {
		return
	}
	if !checkResult {
		res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "据点已通关,无法开启战斗"
		return
	}
	res.Err = &msg.ErrorInfo{}
	// update formation
	ChangeFormation(r, req.Form, res.Err)
	if res.Err.Id != 0 {
		return
	}
	return
}

// 战役副本战斗结算
func EndWarCopyBattle(r *msg.PlayerInfo, req *msg.ReqEndBattleR, res *msg.ReqEndBattleA) {

	// 战斗数据校验
	checkResult, warId, chapterId, strongholdId := CheckWarCopyBattleStatus(r, req.ConfigId, res.Err)
	if res.Err.Id != 0 {
		return
	}
	if !checkResult {
		return
	}

	if req.Result != msg.BattleResult_BR_Success {
		return
	}

	addedObjs, openChapterId, openStrongHoldId := WarCopyInfoBattle(r, warId, chapterId, strongholdId, res.Err)
	if res.Err.Id != 0 {
		return
	}

	// set loots
	res.LootObjs = addedObjs
	// 主线副本战斗返回
	warCopy := &msg.WarCopyEndBattleResult{
		WarId:         warId,
		ChapterId:     chapterId,
		StrongholdId:  strongholdId,
		OpenChapterId: openChapterId,
		OpenHoldId:    openStrongHoldId,
	}
	res.WarCopy = warCopy
	return
}

// 竞技场战斗开始
func StartArenaBattle(r *msg.PlayerInfo, req *msg.ReqStartBattleR, res *msg.ReqStartBattleA) (consumes []*msg.StaticObjInfo) {
	// 战斗数据校验
	if !CheckArenaBattleStatus(r, req.SeasonId, req.BattleId, res.Err) {
		return
	}
	// update formation
	ChangeFormation(r, req.Form, res.Err)

	if res.Err.Id != 0 {
		return
	}
	// 挑战消耗
	consumes = CostArenaBattleTicket(r, req.SeasonId, res.Err)
	return
}

// 竞技场战斗结束
func EndArenaBattle(r *msg.PlayerInfo, battleId uint64, battleR *msg.PlayerInfo, req *msg.ReqEndBattleR, res *msg.ReqEndBattleA) {
	// TODO 这里进行战斗数据校验,会导致最后一场比赛无法正常结算
	//if !CheckArenaBattleStatus(r, req.SeasonId, req.BattleId, res.Err) {
	//	return
	//}
	addedObjs, arenaBattleInfoResult := ArenaBattle(r, battleId, battleR, req.SeasonId, req.Result, res.Err)
	if res.Err.Id != 0 {
		return
	}

	// set loots
	res.LootObjs = addedObjs
	// 竞技场战报返回
	res.Arena = arenaBattleInfoResult

	return
}

// 主线副本战斗开始
func StartMainCopyBattle(r *msg.PlayerInfo, req *msg.ReqStartBattleR, res *msg.ReqStartBattleA) {

	// 战斗数据校验
	checkResult, _, _ := CheckMainCopyBattleStatus(r, req.ConfigId, 1, res.Err)
	if !checkResult {
		return
	}
	// update formation
	ChangeFormation(r, req.Form, res.Err)
	if res.Err.Id != 0 {
		return
	}
}

// 主线副本战斗结算
func EndMainCopyBattle(r *msg.PlayerInfo, req *msg.ReqEndBattleR, res *msg.ReqEndBattleA) {

	// 战斗数据校验
	checkResult, chapterId, strongholdId := CheckMainCopyBattleStatus(r, req.ConfigId, 1, res.Err)
	if !checkResult {
		return
	}

	if req.Result != msg.BattleResult_BR_Success {
		return
	}

	addedObjs, strongholdInfos, openChapterId := MainCopyInfoBattle(r, chapterId, strongholdId, res.Err)
	if res.Err.Id != 0 {
		return
	}

	// set loots
	res.LootObjs = addedObjs
	// 主线副本战斗返回
	mainCopy := &msg.MainCopyBattleInfoResult{
		ChapterId:       chapterId,
		StrongholdId:    strongholdId,
		StrongholdInfos: strongholdInfos,
		OpenChapterId:   openChapterId,
	}
	res.MainCopy = mainCopy
	return
}

func StartClanBossBattle(r *msg.PlayerInfo, req *msg.ReqStartBattleR, res *msg.ReqStartBattleA) {
	rs := share.GetClanId(r.Id)
	if rs == nil {
		res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "StartClanBossBattle rs nil "
		return
	}

	if rs.ClanId == 0 {
		res.Err.Id, res.Err.DebugMsg = 2015, "StartClanBossBattle has no clan"
		return
	}

	key := clan.GetClanLockKey(rs.ClanId)
	lockValue, lockOk := dlock.AcquireLockX(key, gconst.DLockGetTimeout)
	if !lockOk {
		res.Err.Id, res.Err.DebugMsg = -1, "get clan lock failed"
		return
	}
	defer func() {
		if lockOk {
			dlock.ReleaseLockX(key, lockValue)
		}
	}()

	clanInfo := clan.Find(rs.ClanId)
	if clanInfo == nil {
		res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "StartClanBossBattle clanInfo nil "
		return
	}

	playerInfoClan := clan.MustGetPlayerInfoClan(r)

	clan.CheckAttackTimes(req.ClanBossId, playerInfoClan, clanInfo, res.Err)
	if res.Err.Id != 0 {
		return
	}

	unionBossConf, ok := conf.GetUnionBossConfig(req.ClanBossId)
	if !ok {
		res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, " clan boss config is nil"
		return
	}

	for _, bloodId := range unionBossConf.BloodId {
		unionBossBloodConf, ok := conf.GetUnionBossBloodConfig(bloodId)
		if ok {
			if unionBossBloodConf.DropId <= 0 {
				return
			}
			dropStaticObjs := GetDropUnits(r, unionBossBloodConf.DropId, res.Err)
			log.Debugf("dropStaticObjs %+v", dropStaticObjs)

			//var fakeDropObj  msg.BossFakeStaticObj
			//var staticObjs []*msg.BossFakeStaticObj
			bossStaticObj := &msg.BossDropStaticObj{
				BloodNum:       bloodId,
				StaticObjInfos: dropStaticObjs,
			}
			res.Objs = append(res.Objs, bossStaticObj)
			if res.Err.Id != 0 {
				log.Debugf("fake drop error")
			}
		} else {
			res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, " clan boss blood config is nil"
			continue
		}
	}

	clan.FakeBossDropObjs(playerInfoClan, clanInfo, req.ClanBossId, res.Objs, res.Err)

	var playerInfoClanCache redisdb.PlayerInfoClanCache
	playerInfoClanCache.MustUpdate(r.Id, playerInfoClan)
}

// 公会boss战斗结算
func EndClanBossBattle(r *msg.PlayerInfo, req *msg.ReqEndBattleR, res *msg.ReqEndBattleA) {

	rs := share.GetClanId(r.Id)
	if rs == nil {
		res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "EndClanBossBattle rs nil "
		return
	}
	if rs.ClanId == 0 {
		res.Err.Id, res.Err.DebugMsg = 2015, "EndClanBossBattle has no clan"
		return
	}

	key := clan.GetClanLockKey(rs.ClanId)
	lockValue, lockOk := dlock.AcquireLockX(key, gconst.DLockGetTimeout)
	if !lockOk {
		res.Err.Id, res.Err.DebugMsg = -1, "get clan lock failed"
		return
	}
	defer func() {
		if lockOk {
			dlock.ReleaseLockX(key, lockValue)
		}
	}()

	bossBloodConf, ok := conf.GetUnionBossBloodConfig(req.ClanBossBloodId)
	if !ok {
		res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "EndClanBossBattle bossBloodConf nil "
		return
	}

	var passFirstBlood bool
	if req.ClanBossAllDamage < bossBloodConf.Hp { //特殊处理是否通过第一层
		if req.ClanBossBloodId%1000 == 1 {
			passFirstBlood = false
		} else {
			res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "req.ClanBossDamage is too min "
			return
		}
	} else {
		if req.ClanBossBloodId%1000 >= 1 {
			passFirstBlood = true //第一层打过
		}
	}
	if req.ClanBossBloodId/1000 != req.ClanBossId {
		res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "req type is error "
		return
	}

	if passFirstBlood { //第一层打完
		var needBlood int32
		startBloodId := req.ClanBossBloodId
		for i := int(bossBloodConf.Floor); i >= 1; i-- {
			bloodConf, ok := conf.GetUnionBossBloodConfig(startBloodId)
			if !ok {
				res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "EndClanBossBattle bloodConf nil "
				return
			}
			startBloodId--
			needBlood += bloodConf.Hp
		}
		if req.ClanBossAllDamage < needBlood {
			log.Debugf("req.ClanBossAllDamage %+v", req.ClanBossAllDamage)
			log.Debugf("needBlood %+v", needBlood)
			res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, " EndClanBossBattle req.ClanBossAllDamage is error "
			return
		}
	}

	clanInfo := clan.Find(rs.ClanId)
	if clanInfo == nil {
		return
	}
	//不是扫荡
	if req.IsSweep == 0 {
		clan.CheckBattleTime(req.ClanBossId, r, clanInfo, res.Err)
		if res.Err.Id != 0 {
			return
		}
	}

	playerInfoClan := clan.MustGetPlayerInfoClan(r)

	clan.CheckAttackTimes(req.ClanBossId, playerInfoClan, clanInfo, res.Err)
	if res.Err.Id != 0 {
		return
	}
	clanBR := &msg.ClanBossBattleResult{
		BossDropStaticObjs: make([]*msg.BossDropStaticObj, 0),
	}
	clanBRDB := &msg.ClanBossBattleResult{
		BossDropStaticObjs: make([]*msg.BossDropStaticObj, 0),
	}
	for _, v := range playerInfoClan.BossInfos {
		if v.Id != req.ClanBossId {
			continue
		}

		for _, v2 := range v.BossDropStaticObjs {
			conf, ok := conf.GetUnionBossBloodConfig(v2.BloodNum)
			if !ok {
				res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "EndClanBossBattle bossBloodConf nil "
				return
			}

			//if !firstBloodNotOk { //第一层打完
			if bossBloodConf.Floor < conf.Floor {
				break
			}
			//}
			var floorBoss = &msg.BossDropStaticObj{
				BloodNum:       v2.BloodNum,
				StaticObjInfos: make([]*msg.StaticObjInfo, 0),
			}
			var floorBossDB = &msg.BossDropStaticObj{
				BloodNum:       v2.BloodNum,
				StaticObjInfos: make([]*msg.StaticObjInfo, 0),
			}

			//增加dropids
			if passFirstBlood { //第一层打完
				objs, _ := AddStaticObjs(r, v2.StaticObjInfos, res.Err)
				if res.Err.Id != 0 {
					return
				}
				//通知增加objs
				res.LootObjs = append(res.LootObjs, objs...)
				floorBoss.StaticObjInfos = append(floorBoss.StaticObjInfos, v2.StaticObjInfos...)
				floorBossDB.StaticObjInfos = append(floorBossDB.StaticObjInfos, v2.StaticObjInfos...)
			}
			//NormalReward
			//TODO 奖励当前层之前所有层的动态drop的id
			//TODO 但是固定奖励只给最后一层当前层的
			if bossBloodConf.Floor == conf.Floor || !passFirstBlood {
				staticObjs, _ := JsonToPb_StaticObjs(conf.NormalReward, conf, conf.Id, res.Err)
				if res.Err.Id != 0 {
					log.Debugf("fake drop unionBossBloodConf error")
				}
				units, _ := AddStaticObjs(r, staticObjs, res.Err)
				if res.Err.Id != 0 {
					return
				}
				res.LootObjs = append(res.LootObjs, units...)
				floorBoss.StaticObjInfos = append(floorBoss.StaticObjInfos, staticObjs...)
			}
			clanBR.BossDropStaticObjs = append(clanBR.BossDropStaticObjs, floorBoss)
			clanBRDB.BossDropStaticObjs = append(clanBRDB.BossDropStaticObjs, floorBossDB)
		}
	}

	var logRewardIds []int32 //精英boss  log
	if req.ClanBossId == int32(msg.ClanBossType_CLAN_ELITE_BOSS) && passFirstBlood {
		for _, v := range clanBR.BossDropStaticObjs {
			for _, v2 := range v.StaticObjInfos {
				logRewardIds = append(logRewardIds, v2.Id)
			}
		}
	}
	clan.EndBattle(req, playerInfoClan, clanInfo, logRewardIds, clanBRDB.BossDropStaticObjs, req.ClanBossBloodId, res.Err)
	if res.Err.Id != 0 {
		return
	}

	var playerInfoClanCache redisdb.PlayerInfoClanCache
	playerInfoClanCache.MustUpdate(r.Id, playerInfoClan)
	//前端展示用的
	res.ClanBoss = clanBR
}

func StartMazeBattle(r *msg.PlayerInfo, req *msg.ReqStartBattleR, res *msg.ReqStartBattleA) {
	maze := MustGetPlayerInfoMaze(r)
	if _, ok := maze.LevelEvent[req.MazeLevelId]; !ok {
		res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "StartMazeBattle maze.LevelEvent levelId not exist"
		log.Debugf("StartMazeBattle maze.LevelEvent levelId not exist")
		return
	}

	_, ok := conf.GetMazeRefreshConfig(req.MazeLevelId)
	if !ok {
		res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "StartMazeBattle GetMazeRefreshConfig error"
		log.Debugf("StartMazeBattle GetMazeRefreshConfig error ")
		return
	}
}

func EndMazeBattle(r *msg.PlayerInfo, req *msg.ReqEndBattleR, res *msg.ReqEndBattleA) {
	err := &msg.ErrorInfo{}

	maze := MustGetPlayerInfoMaze(r)

	for k, v := range maze.TroopReduceHp {
		_, ok := conf.GetNPCConfig(k)
		if ok {
			log.Debugf("TroopReduceHp npcData.Id %d", k)
			continue
		}
		maze.TroopReduceHp[k] += v
	}
	mazeBattleInfo := req.MazeBattle

	log.Debugf("EndMazeBattle maze.DeadTroops :%+v", maze.DeadTroops)
	levelId := req.MazeBattle.LevelId
	for i := 0; i < len(mazeBattleInfo.DeadTroops); i++ {
		//_, ok := conf.GetNPCConfig(mazeBattleInfo.DeadTroops[i])
		//if ok  {
		//	continue
		//}
		//_ = GetTroop(r, mazeBattleInfo.DeadTroops[i], res.Err)
		if res.Err.Id != 0 {
			mazeBattleInfo.DeadTroops = append(mazeBattleInfo.DeadTroops[:i], mazeBattleInfo.DeadTroops[i+1:]...)
			return
		}
	}
	maze.DeadTroops = append(maze.DeadTroops, mazeBattleInfo.DeadTroops...)
	log.Debugf("after battle EndMazeBattle maze.DeadTroops :%+v", maze.DeadTroops)

	for k, v := range mazeBattleInfo.TroopReduceHp {
		_, ok := conf.GetNPCConfig(k)
		if ok {
			log.Debugf("mazeBattleInfo TroopReduceHp npcData.Id %d", k)
			continue
		}
		_ = GetTroopAnyway(r, k, res.Err)
		if res.Err.Id != 0 {
			return
		}
		maze.TroopReduceHp[k] += v
	}
	for k, v := range mazeBattleInfo.TroopCurEnergy {
		_, ok := conf.GetNPCConfig(k)
		if ok {
			log.Debugf("mazeBattleInfo TroopCurEnergy npcData.Id %d", k)
			continue
		}
		_ = GetTroopAnyway(r, k, res.Err)
		if res.Err.Id != 0 {
			return
		}
		maze.TroopEnergy[k] += v
	}

	if _, ok := maze.RobotInfos[levelId]; !ok {
		log.Debugf("maze.RobotInfos[levelId] not find %d", levelId)
		maze.RobotInfos[levelId] = &msg.MazeRobotInfo{}
	}

	if maze.RobotInfos[levelId].RobotReduceHp == nil {
		maze.RobotInfos[levelId].RobotReduceHp = make(map[int32]int32)
	}

	for k, v := range mazeBattleInfo.TroopReduceHp {
		_, ok := conf.GetNPCConfig(k)
		if ok {
			log.Debugf("TroopReduceHp npcData.Id %d", k)
			maze.RobotInfos[levelId].RobotReduceHp[k] += v
			continue
		}
	}

	if maze.RobotInfos[levelId].RobotCurEnergy == nil {
		maze.RobotInfos[levelId].RobotCurEnergy = make(map[int32]int32)
	}
	for k, v := range mazeBattleInfo.TroopCurEnergy {
		_, ok := conf.GetNPCConfig(k)
		if ok {
			log.Debugf("mazeBattleInfo TroopCurEnergy npcData.Id %d", k)
			maze.RobotInfos[levelId].RobotCurEnergy[k] += v
			continue
		}
	}

	if req.Result != msg.BattleResult_BR_Success {
		return
	}

	if _, ok := maze.LevelEvent[levelId]; !ok {
		res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "EndMazeBattle levelId not exist"
		//log.Debugf("EndMazeBattle levelId not exist ")
		return
	}

	refreshConf, ok := conf.GetMazeRefreshConfig(levelId)
	if !ok {
		res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "EndMazeBattle refreshConf error"
		//log.Debugf("EndMazeBattle refreshConf error ")
		return
	}

	rewardStr := refreshConf.NormalReward
	if int32(msg.MazeModeType_MM_hard) == maze.Mode {
		rewardStr = refreshConf.HardReward
	}
	units, ok := JsonToPb_StaticObjs(rewardStr, refreshConf, req.MazeBattle.LevelId, err)
	if err.Id != 0 {
		//log.Debugf("EndMazeBattle JsonToPb_StaticObjs error ")
		return
	}

	dropUnits := GetDropUnits(r, refreshConf.DropId, err)
	if err.Id != 0 {
		//log.Debugf("EndMazeBattle GetDropUnits error ")
		return
	}

	layerConf := ConfByLayerMode(maze.LayerId, maze.Mode)
	if layerConf == nil {
		res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "EndMazeBattle layerConf error"
		//log.Debugf("EndMazeBattle layerConf error ")
		return
	}

	var relicId int32
	for k, v := range layerConf.RelicGet {
		if k != maze.Times {
			continue
		}

		times := CurLayerEventTimes(maze.LayerId, maze.Mode, maze.EventIds, maze.CurLevelId)
		if len(v.Value)%2 != 0 {
			log.Debugf("data not match 2")
			continue
		}
		for i := 0; i < len(v.Value); i += 2 {
			if v.Value[i] != times {
				continue
			}
			relicId = v.Value[i+1]
			break
		}
	}

	if relicId != 0 {
		relicConf, ok := conf.GetMazeRelicConfig(relicId)
		if !ok {
			res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "EndMazeBattle relicConf error"
			//log.Debugf("EndMazeBattle relicConf error ")
			return
		}
		staticObjInfo := &msg.StaticObjInfo{
			Id:    relicConf.Id,
			Type:  int32(msg.ObjType_OT_MazeRelic),
			Count: 1,
		}
		dropUnits[0] = staticObjInfo
		//units = append(units, staticObjInfo)
	}

	ret, ok := AddStaticObjs(r, units, err)

	if err.Id != 0 {
		return
	}

	//res.StaticRelicObjs = dropUnits
	if maze.StaticRelicObjs == nil {
		maze.StaticRelicObjs = make(map[int32]*msg.MazeRelicInfo)
	}
	maze.StaticRelicObjs[levelId] = &msg.MazeRelicInfo{
		RelicObjInfos: dropUnits,
	}

	res.LootObjs = append(res.LootObjs, ret...)

	//使用全部圣器
	maze.ArtifactObjs = nil
	NextLevelId(maze, levelId)
}

func GetFormationPower(r *msg.PlayerInfo, formation msg.FormationType) (allPower int32) {
	v, ok := r.Formations[formation.Number()]
	if !ok {
		return
	}
	for _, troopId := range v.TroopIds {
		if troopId <= 0 {
			continue
		}
		allPower += GetTroopPower(r, troopId)
	}
	return
}

// 竞技场战斗开始
func StartFriendBattle(r *msg.PlayerInfo, friendId uint64, err *msg.ErrorInfo) {
	if !CheckFriendInfosByUserId(r, friendId) {
		err.Id, err.DebugMsg = msg.Ecode_OK, "102616"
		return
	}
	var c redisdb.PlayerInfoCache
	friendR := c.Find(friendId)
	if friendR == nil {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "切磋好友未找到"
		return
	}
	CheckFormationIsNull(friendR, friendR.Formations[int32(msg.FormationType_FT_Friend)], err)
	if err.Id != 0 {
		err.Id, err.DebugMsg = msg.Ecode_OK, "102623"
		return
	}
	return
}
