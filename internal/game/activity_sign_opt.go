package game

import (
	"server/internal/game/activity"
	"server/internal/util/time3"
	"server/pkg/gen/conf"
	"server/pkg/gen/msg"
	"server/pkg/gen/rawdata"
)

type ActivityDetailSignOpt msg.ActivityData

func (d *ActivityDetailSignOpt) InitDetail(r *msg.PlayerInfo) {
	d.Sign = &msg.ActivityDailySignDetail{
		SignedDay: make(map[int32]int32),
		// 从1开始
		CanSignDay:           1,
		LastLoginTimeForSign: time3.Now().Unix(),
	}
}

func (d *ActivityDetailSignOpt) ShouldGetDetail(r *msg.PlayerInfo) (detail interface{}) {
	u := activity.GetMgr().GetUnit(d.Id)
	if u.Config.Type != int32(msg.ActivityType_AT_DailySign) {
		return nil
	}

	if d.Sign == nil {
		d.Sign = &msg.ActivityDailySignDetail{
			SignedDay:            nil,
			CanSignDay:           1,
			LastLoginTimeForSign: time3.Now().Unix(),
		}
	}
	if d.Sign.SignedDay == nil {
		d.Sign.SignedDay = make(map[int32]int32)
	}

	return d.Sign
}

func (d *ActivityDetailSignOpt) GetConfig(err *msg.ErrorInfo) (confData interface{}) {
	au := GetActivityConfig(d.Id, err)
	if err.Id != 0 {
		return nil
	}
	if au.Config.Type != int32(msg.ActivityType_AT_DailySign) {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "activity type err"
		return nil
	}

	signConf, _ := conf.GetActivitySignConfig(au.Config.Data)
	if !CheckConfData(signConf, au.Config.Data, err) {
		return nil
	}

	return signConf
}

// 根据时间刷新一下状态
func (d *ActivityDetailSignOpt) TryUpdateActivityByTime(r *msg.PlayerInfo) {
	sign := d.ShouldGetDetail(r).(*msg.ActivityDailySignDetail)
	if sign != nil {
		now := time3.Now().Unix()
		if time3.IsCrossDay(sign.LastLoginTimeForSign, now) {
			sign.LastLoginTimeForSign = now
			// 不管过了多久，天数都是加1
			sign.CanSignDay++
		}
	}
}

// 判断根据完成即结束去尝试关闭
func (d *ActivityDetailSignOpt) TryCloseByDirectEnd(r *msg.PlayerInfo) {
	if d.Sign == nil || d.Sign.SignedDay == nil {
		return
	}

	signConf, ok := d.GetConfig(&msg.ErrorInfo{}).(*rawdata.ActivitySignConfig)
	if !ok {
		return
	}

	if signConf == nil {
		return
	}
	if int32(len(d.Sign.SignedDay)) == signConf.DayNums {
		d.Close(r)
		return
	}
}

// 尝试结束的时候，补发未领取的邮件
func (d *ActivityDetailSignOpt) TrySendSupplyRewards(r *msg.PlayerInfo) {
	// do nth.
}

func (d *ActivityDetailSignOpt) Close(r *msg.PlayerInfo) {
	if d.Status == msg.ActivityStatus_AS_Closed {
		return
	}
	d.Status = msg.ActivityStatus_AS_Closed
	// clear data
	// 因为要下推客户端数据，当时不重置sign, 在登录的时候，刷新closed，清空
	//d.Sign = nil
}

func (d *ActivityDetailSignOpt) TryClearWhenClosed(r *msg.PlayerInfo) {
	d.Sign = nil
}
