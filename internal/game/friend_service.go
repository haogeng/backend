package game

import (
	"bitbucket.org/funplus/golib/time2"
	"context"
	"math/rand"
	"server/internal/gconst"
	"server/internal/key"
	"server/pkg/cache"
	"server/pkg/dlock"
	"server/pkg/gen/conf"
	"server/pkg/gen/msg"
	"server/pkg/gen_redis/redisdb"
	"sort"
	"strconv"
	"time"
)

// 推荐好友列表人数限制
const RecommendFriendsMaxLimit = 20

// redis 里的find都是每次unmarshal，相当于每次都是新的，不是纯正的缓存
// 可以分方便的实现事务回滚
// 此处，需要延续性，，将其和playerInfo联系起来，，这样数据的生命期和有效期完全依托于playerInfo
// 有线程安全问题，但实际上没事，，业务逻辑做了seq的串行化
func MustGetPlayerInfoFriend(r *msg.PlayerInfo) *msg.PlayerInfoFriend {
	if r.Friend != nil {
		return r.Friend
	}
	var c redisdb.PlayerInfoFriendCache
	friend := c.Find(r.Id)
	if friend == nil {
		// 初始化用户好友信息
		friend = &msg.PlayerInfoFriend{
			Id:                 r.Id,
			RefreshSendGetTime: time.Now().Unix(),
			DaySendNum:         0,
			DayGetNum:          0,
		}
	}
	r.Friend = friend
	if r.Friend.FriendInfos == nil {
		r.Friend.FriendInfos = make([]*msg.FriendDetailInfo, 0)
	}
	if r.Friend.BlackInfos == nil {
		r.Friend.BlackInfos = make([]*msg.FriendBlackInfo, 0)
	}
	if r.Friend.ApplyInfos == nil {
		r.Friend.ApplyInfos = make([]*msg.FriendApplyInfo, 0)
	}
	return r.Friend
}

func MustUpdatePlayerInfoFriend(id uint64, rFriend *msg.PlayerInfoFriend) {
	var c redisdb.PlayerInfoFriendCache
	c.MustUpdate(id, rFriend)
}

func keyOfPlayerFriendInfos(id uint64) string {
	return cache.GetLockKeyFor(redisdb.LockKeyPostfixOfPlayerInfoFriend(id))
}

// 添加用户到推荐列表
func LoginPlayerToRecommendFriends(ctx context.Context, r *msg.PlayerInfo) {
	// TODO 测试阶段暂时不加限制
	//if r.Level <= 1 {
	//	// 不要一级的LOW逼
	//	return
	//}
	rFriend := MustGetPlayerInfoFriend(r)
	if rFriend.CloseRecommend {
		// 推荐按钮关闭
		return
	}
	if checkFriendInfosMaxLimit(r) {
		// 达到好友上限
		return
	}
	key := key.GetFriendRecommendKey(r.ServerId)
	sum := cache.Redis.Cmd.LLen(ctx, key).Val()
	if sum > RecommendFriendsMaxLimit {
		cache.Redis.Cmd.LTrim(ctx, key, 0, RecommendFriendsMaxLimit)
		// 推荐列表人数达到上限
		cache.Redis.Cmd.RPop(ctx, key)
	}
	// 添加到推荐列表
	cache.Redis.Cmd.LPush(ctx, key, r.Id)
}

// 临时好友返回结构
type TmpFriendResult struct {
	// 好友信息
	FriendInfos []*msg.FriendInfoResult
	// 好友上限
	FriendMaxNum int32
	// 今日赠送
	DaySendNum int32
	// 赠送上限
	SendMaxNum int32
	// 今日领取
	DayGetNum int32
	// 领取上限
	GetMaxNum int32
	// 推荐开关
	FriendStatus bool
}

func GetFriendInfos(rId uint64, err *msg.ErrorInfo) (friendResult *TmpFriendResult) {
	rKeyLock := keyOfPlayerFriendInfos(rId)
	lockValue, ok := dlock.AcquireLockWithTimeoutX(rKeyLock, gconst.DLockTtl, gconst.DLockGetTimeout)
	if !ok {
		return
	}
	defer dlock.ReleaseLockX(rKeyLock, lockValue)

	var c redisdb.PlayerInfoCache
	r := c.Find(rId)
	if r == nil {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, gconst.ErrNoUser.Error()
		return
	}
	rFriend := MustGetPlayerInfoFriend(r)
	globalConfig, _ := conf.GetGlobalConfig(1)
	if !CheckConfData(globalConfig, 1, err) {
		return
	}
	// 检查是否刷新
	if !time2.IsToday(rFriend.RefreshSendGetTime, time.Now()) {
		// 刷新
		rFriend.RefreshSendGetTime = time.Now().Unix()
		rFriend.DayGetNum = 0
		rFriend.DaySendNum = 0
		rFriend.DayApplyNum = 0
		for i, v := range rFriend.FriendInfos {
			if v.GetStatus != msg.FriendRewradReceive_FRR_NOReceive {
				// 未领取爱心不用重置,否则都重置成未发放
				rFriend.FriendInfos[i].GetStatus = msg.FriendRewradReceive_FRR_NOSENDREWARD
			}
			rFriend.FriendInfos[i].SendStatus = msg.RewardStatus_RS_NOSEND
		}
	}
	// 好友返回信息
	var friendInfos []*msg.FriendInfoResult
	for _, v := range rFriend.FriendInfos {
		if v == nil {
			continue
		}
		friendResult := buildFriendInfoResult(v.FriendUserId, v.FriendTime, v.SendStatus, v.GetStatus, msg.FriendApplyType_FAT_NOAPPLY)
		if friendResult == nil {
			continue
		}
		friendInfos = append(friendInfos, friendResult)
	}
	friendResult = &TmpFriendResult{
		FriendInfos:  friendInfos,
		FriendMaxNum: globalConfig.FriendNumLimit,
		SendMaxNum:   globalConfig.FriendGiveHeartDailyLimit,
		GetMaxNum:    globalConfig.FriendReceiveHeartDailyLimit,
		DaySendNum:   rFriend.DaySendNum,
		DayGetNum:    rFriend.DayGetNum,
		FriendStatus: rFriend.CloseRecommend,
	}
	MustUpdatePlayerInfoFriend(rId, rFriend)
	return
}

func GetApplyFriendInfos(r *msg.PlayerInfo) (applyMaxNum, friendCurNum, friendMaxNum int32, applyResult []*msg.FriendInfoResult) {
	rKeyLock := keyOfPlayerFriendInfos(r.Id)
	lockValue, ok := dlock.AcquireLockWithTimeoutX(rKeyLock, gconst.DLockTtl, gconst.DLockGetTimeout)
	if !ok {
		return
	}
	defer dlock.ReleaseLockX(rKeyLock, lockValue)

	rFriend := MustGetPlayerInfoFriend(r)
	globalConfig, _ := conf.GetGlobalConfig(1)
	// 除非服务器炸了,否则不可能没有
	if !CheckConfData(globalConfig, 1, &msg.ErrorInfo{}) {
		return
	}
	applyMaxNum = globalConfig.FriendAcceptApplyLimit
	// 好友返回信息
	for i := 0; i < len(rFriend.ApplyInfos); {
		v := rFriend.ApplyInfos[i]
		// 检查请求是否在有效期
		if !checkApplyInfoByTimeLimit(r, v.ApplyUserId) {
			rFriend.ApplyInfos = append(rFriend.ApplyInfos[:i], rFriend.ApplyInfos[i+1:]...)
			continue
		}
		i++
		friendResult := buildFriendInfoResult(v.ApplyUserId, v.ApplyTime, msg.RewardStatus_RS_NOSEND, msg.FriendRewradReceive_FRR_NOSENDREWARD, msg.FriendApplyType_FAT_INAPPLY)
		if friendResult == nil {
			continue
		}
		applyResult = append(applyResult, friendResult)
	}
	friendCurNum = int32(len(rFriend.FriendInfos))
	friendMaxNum = globalConfig.FriendNumLimit

	MustUpdatePlayerInfoFriend(r.Id, rFriend)
	return
}

func GetBlackFriendInfos(r *msg.PlayerInfo) (blackResult []*msg.FriendInfoResult) {
	rFriend := MustGetPlayerInfoFriend(r)
	// 黑名单返回信息
	for _, v := range rFriend.BlackInfos {
		if v == nil {
			continue
		}
		friendResult := buildFriendInfoResult(v.BlackUserId, v.BlackTime, msg.RewardStatus_RS_NOSEND, msg.FriendRewradReceive_FRR_NOSENDREWARD, msg.FriendApplyType_FAT_NOAPPLY)
		if friendResult == nil {
			continue
		}
		blackResult = append(blackResult, friendResult)
	}
	return
}

func AddBlackFriend(rId, dId uint64, err *msg.ErrorInfo) {
	rKeyLock := keyOfPlayerFriendInfos(rId)
	lockValue, ok := dlock.AcquireLockWithTimeoutX(rKeyLock, gconst.DLockTtl, gconst.DLockGetTimeout)
	if !ok {
		return
	}
	defer dlock.ReleaseLockX(rKeyLock, lockValue)

	var c redisdb.PlayerInfoCache
	r := c.Find(rId)
	rFriend := MustGetPlayerInfoFriend(r)
	if len(rFriend.BlackInfos) >= 999 {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "黑名单达到上限"
		return
	}
	// 添加黑名单列表
	var bIndex = -1
	for i, v := range rFriend.BlackInfos {
		if v.BlackUserId == dId {
			bIndex = i
			break
		}
	}
	if bIndex == -1 {
		blackInfo := &msg.FriendBlackInfo{
			BlackUserId: dId,
			BlackTime:   time.Now().Unix(),
		}
		rFriend.BlackInfos = append(rFriend.BlackInfos, blackInfo)
	}
	MustUpdatePlayerInfoFriend(rId, rFriend)

	RefreshFriendLoveRedPoint(r, true)

	err.Id, err.DebugMsg = msg.Ecode_OK, "102608"
	return
}

func RemoveBlackFriend(rId uint64, removeFriendId uint64, err *msg.ErrorInfo) {
	rKeyLock := keyOfPlayerFriendInfos(rId)
	lockValue, ok := dlock.AcquireLockWithTimeoutX(rKeyLock, gconst.DLockTtl, gconst.DLockGetTimeout)
	if !ok {
		return
	}
	defer dlock.ReleaseLockX(rKeyLock, lockValue)

	var c redisdb.PlayerInfoCache
	r := c.Find(rId)
	if r == nil {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, gconst.ErrNoUser.Error()
		return
	}
	rFriend := MustGetPlayerInfoFriend(r)

	// 移除黑名单列表
	for i, v := range rFriend.BlackInfos {
		if v.BlackUserId == removeFriendId {
			rFriend.BlackInfos = append(rFriend.BlackInfos[:i], rFriend.BlackInfos[i+1:]...)
			break
		}
	}
	MustUpdatePlayerInfoFriend(rId, rFriend)
	return
}

func DeleteFriend(rId, dId uint64, err *msg.ErrorInfo) {
	rKeyLock := keyOfPlayerFriendInfos(rId)
	lockValue, ok := dlock.AcquireLockWithTimeoutX(rKeyLock, gconst.DLockTtl, gconst.DLockGetTimeout)
	if !ok {
		return
	}
	defer dlock.ReleaseLockX(rKeyLock, lockValue)

	var c redisdb.PlayerInfoCache
	r := c.Find(rId)
	if r == nil {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, gconst.ErrNoUser.Error()
		return
	}
	rFriend := MustGetPlayerInfoFriend(r)

	// 删除我方好友
	for i, v := range rFriend.FriendInfos {
		if v.FriendUserId == dId {
			rFriend.FriendInfos = append(rFriend.FriendInfos[:i], rFriend.FriendInfos[i+1:]...)
			break
		}
	}
	MustUpdatePlayerInfoFriend(rId, rFriend)

	RefreshFriendLoveRedPoint(r, true)

	err.Id, err.DebugMsg = msg.Ecode_OK, "102607"
	return
}

func SendApplyFriend(rId, applyFriendId uint64, err *msg.ErrorInfo) {
	rKeyLock := keyOfPlayerFriendInfos(applyFriendId)
	lockValue, ok := dlock.AcquireLockWithTimeoutX(rKeyLock, gconst.DLockTtl, gconst.DLockGetTimeout)
	if !ok {
		return
	}
	defer dlock.ReleaseLockX(rKeyLock, lockValue)

	var c redisdb.PlayerInfoCache
	r := c.Find(rId)
	if r == nil {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, gconst.ErrNoUser.Error()
		return
	}
	applyR := c.Find(applyFriendId)
	if applyR == nil {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, gconst.ErrNoUser.Error()
		return
	}
	//  不能给自己发申请
	if r.Id == applyR.Id {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "不能申请自己为好友"
		return
	}
	if CheckApplyInfosByUserId(applyR, r.Id) == msg.FriendApplyType_FAT_INAPPLY {
		err.Id, err.DebugMsg = 2413, "102626"
		return
	}
	if CheckFriendInfosByUserId(r, applyR.Id) {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "对方已是好友,请勿重复操作"
		return
	}
	// 检查我方好友数量是否达到上限
	if checkFriendInfosMaxLimit(r) {
		err.Id, err.DebugMsg = 2401, "102601"
		return
	}
	// 检查是否在我方黑名单列表
	if CheckBlackInfosByUserId(r, applyFriendId) {
		err.Id, err.DebugMsg = 2402, "102602"
		return
	}
	// 检查是否在对方黑名单列表
	if CheckBlackInfosByUserId(applyR, r.Id) {
		err.Id, err.DebugMsg = 2403, "102603"
		return
	}
	// 检查对方申请列表是否达到上限
	if checkApplyInfosMaxLimit(applyR) {
		err.Id, err.DebugMsg = 2404, "102604"
		return
	}
	rFriend := MustGetPlayerInfoFriend(r)
	applyRFriend := MustGetPlayerInfoFriend(applyR)
	globalConfig, _ := conf.GetGlobalConfig(1)
	if !CheckConfData(globalConfig, 1, err) {
		return
	}
	// 判断己方今日申请好友是否达上限
	if rFriend.DayApplyNum >= globalConfig.FriendDailyApplyLimit {
		err.Id, err.DebugMsg = 2416, "102629"
		return
	}
	// 增加每日申请次数
	rFriend.DayApplyNum += 1
	MustUpdatePlayerInfoFriend(rId, rFriend)

	// 发送申请信息
	var aIndex = -1
	for i, v := range applyRFriend.ApplyInfos {
		if v.ApplyUserId == r.Id {
			aIndex = i
			break
		}
	}
	if aIndex == -1 {
		applyInfo := &msg.FriendApplyInfo{
			ApplyUserId: r.Id,
			ApplyTime:   time.Now().Unix(),
		}
		applyRFriend.ApplyInfos = append(applyRFriend.ApplyInfos, applyInfo)
	}
	// 推送申请好友红点
	RTMNotifyRedPoint(applyR.Id, msg.RedPointType_RPT_TriggerNewFriendApply, 0, 0)

	MustUpdatePlayerInfoFriend(applyFriendId, applyRFriend)
	err.Id, err.DebugMsg = msg.Ecode_OK, "102605"
	return
}

// 批量处理添加好友
func AutoJoinApplyFriend(rId uint64, err *msg.ErrorInfo) {
	var c redisdb.PlayerInfoCache
	r := c.Find(rId)
	if r == nil {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, gconst.ErrNoUser.Error()
		return
	}
	rFriend := MustGetPlayerInfoFriend(r)
	// 按申请时间排序
	sort.Slice(rFriend.ApplyInfos, func(i, j int) bool {
		return rFriend.ApplyInfos[i].ApplyTime < rFriend.ApplyInfos[j].ApplyTime
	})
	for _, v := range rFriend.ApplyInfos {
		if checkFriendInfosMaxLimit(r) {
			// 好友达到上限,结束批量添加好友
			err.Id, err.DebugMsg = msg.Ecode_OK, "102610"
			break
		}
		// 循环添加好友,内部消化异常
		JoinApplyFriend(rId, v.ApplyUserId, &msg.ErrorInfo{})
	}
	return
}

// 处理好友申请
func JoinApplyFriend(rId, joinApplyId uint64, err *msg.ErrorInfo) {
	var c redisdb.PlayerInfoCache
	r := c.Find(rId)
	if r == nil {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, gconst.ErrNoUser.Error()
		return
	}
	joinR := c.Find(joinApplyId)
	if joinR == nil {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, gconst.ErrNoUser.Error()
		return
	}
	globalConfig, _ := conf.GetGlobalConfig(1)
	if !CheckConfData(globalConfig, 1, err) {
		return
	}
	// 不能添加自己
	if r.Id == joinR.Id {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "不能添加自己为好友"
		return
	}
	// 检查请求是否在有效期
	if !checkApplyInfoByTimeLimit(r, joinR.Id) {
		err.Id, err.DebugMsg = 2406, "102609"
		return
	}
	// 检查我方好友是否达到上限
	if checkFriendInfosMaxLimit(r) {
		err.Id, err.DebugMsg = 2407, "102610"
		return
	}
	// 检查是否在我方黑名单列表
	if CheckBlackInfosByUserId(r, joinApplyId) {
		err.Id, err.DebugMsg = 2415, "102628"
		return
	}
	// 检查是否在对方黑名单列表
	if CheckBlackInfosByUserId(joinR, rId) {
		err.Id, err.DebugMsg = 2403, "102603"
		return
	}
	// 检查对方好友是否达到上限
	if checkFriendInfosMaxLimit(joinR) {
		err.Id, err.DebugMsg = 2408, "102611"
		return
	}

	// 添加我方好友
	AddFriend(rId, joinApplyId, true, err)
	// 添加对方好友
	AddFriend(joinApplyId, rId, false, err)

	if err.Id == 0 {
		err.Id, err.DebugMsg = msg.Ecode_OK, "102612"
	} else {
		// 错误码内部传递
	}
	// 刷新红点
	r = c.Find(rId)
	if r == nil {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, gconst.ErrNoUser.Error()
		return
	}
	RefreshFriendApplyRedPoint(r, true)
	RefreshFriendLoveRedPoint(r, true)
	return
}

// 添加好友
func AddFriend(rId, joinApplyId uint64, delApply bool, err *msg.ErrorInfo) {
	rKeyLock := keyOfPlayerFriendInfos(rId)
	lockValue, ok := dlock.AcquireLockWithTimeoutX(rKeyLock, gconst.DLockTtl, gconst.DLockGetTimeout)
	if !ok {
		return
	}
	defer dlock.ReleaseLockX(rKeyLock, lockValue)

	var c redisdb.PlayerInfoCache
	r := c.Find(rId)
	if r == nil {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, gconst.ErrNoUser.Error()
		return
	}
	rFriend := MustGetPlayerInfoFriend(r)
	globalConfig, _ := conf.GetGlobalConfig(1)
	if !CheckConfData(globalConfig, 1, err) {
		return
	}
	if delApply {
		// 移除申请列表
		for i, v := range rFriend.ApplyInfos {
			if v.ApplyUserId == joinApplyId {
				rFriend.ApplyInfos = append(rFriend.ApplyInfos[:i], rFriend.ApplyInfos[i+1:]...)
				break
			}
		}
	}
	// 添加好友
	var rIndex = -1
	for i, v := range rFriend.FriendInfos {
		if v.FriendUserId == joinApplyId {
			rIndex = i
			break
		}
	}
	if rIndex == -1 {
		friendInfo := &msg.FriendDetailInfo{
			FriendUserId: joinApplyId,
			FriendTime:   time.Now().Unix(),
			SendStatus:   msg.RewardStatus_RS_NOSEND,
			GetStatus:    msg.FriendRewradReceive_FRR_NOSENDREWARD,
		}
		rFriend.FriendInfos = append(rFriend.FriendInfos, friendInfo)
	} else {
		// 对方已经是好友
		err.Id, err.DebugMsg = 2414, "102627"
	}
	MustUpdatePlayerInfoFriend(rId, rFriend)
}

// 批量拒绝好友申请
func AutoDeleteApplyFriend(rId uint64, err *msg.ErrorInfo) (deleteResult []uint64) {
	rKeyLock := keyOfPlayerFriendInfos(rId)
	lockValue, ok := dlock.AcquireLockWithTimeoutX(rKeyLock, gconst.DLockTtl, gconst.DLockGetTimeout)
	if !ok {
		return
	}
	defer dlock.ReleaseLockX(rKeyLock, lockValue)

	var c redisdb.PlayerInfoCache
	r := c.Find(rId)
	if r == nil {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, gconst.ErrNoUser.Error()
		return
	}
	rFriend := MustGetPlayerInfoFriend(r)

	for _, v := range rFriend.ApplyInfos {
		deleteResult = append(deleteResult, v.ApplyUserId)
	}
	rFriend.ApplyInfos = make([]*msg.FriendApplyInfo, 0)

	MustUpdatePlayerInfoFriend(rId, rFriend)

	// 刷新红点
	RefreshFriendApplyRedPoint(r, true)
	return
}

// 拒绝好友申请
func DeleteApplyFriend(rId, applyUserId uint64, err *msg.ErrorInfo) {
	rKeyLock := keyOfPlayerFriendInfos(rId)
	lockValue, ok := dlock.AcquireLockWithTimeoutX(rKeyLock, gconst.DLockTtl, gconst.DLockGetTimeout)
	if !ok {
		return
	}
	defer dlock.ReleaseLockX(rKeyLock, lockValue)

	var c redisdb.PlayerInfoCache
	r := c.Find(rId)
	if r == nil {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, gconst.ErrNoUser.Error()
		return
	}
	rFriend := MustGetPlayerInfoFriend(r)

	// 移除申请列表
	for i, v := range rFriend.ApplyInfos {
		if v.ApplyUserId == applyUserId {
			rFriend.ApplyInfos = append(rFriend.ApplyInfos[:i], rFriend.ApplyInfos[i+1:]...)
			break
		}
	}
	MustUpdatePlayerInfoFriend(rId, rFriend)
	// 刷新红点
	RefreshFriendApplyRedPoint(r, true)
	return
}

// 推荐列表
func RecommendFriends(ctx context.Context, r *msg.PlayerInfo, err *msg.ErrorInfo) (recommendInfos []*msg.FriendInfoResult, friendCurNum, friendMaxNum int32) {
	globalConfig, _ := conf.GetGlobalConfig(1)
	if !CheckConfData(globalConfig, 1, err) {
		return
	}
	// 初始化好友人数和上限
	rFriend := MustGetPlayerInfoFriend(r)
	friendCurNum = int32(len(rFriend.FriendInfos))
	friendMaxNum = globalConfig.FriendNumLimit
	// 推荐好友
	var allRecommend []string
	queueKey := key.GetFriendRecommendKey(r.ServerId)
	queueLen := int32(cache.Redis.Cmd.LLen(ctx, queueKey).Val())
	if queueLen < globalConfig.FriendRecommendRandomNum {
		allRecommend = cache.Redis.Cmd.LRange(ctx, queueKey, 0, -1).Val()
	} else {
		randEnd := queueLen - globalConfig.FriendRecommendRandomNum
		randNum := rand.Intn(int(randEnd))
		allRecommend = cache.Redis.Cmd.LRange(ctx, queueKey, int64(randNum), int64(int32(randNum)+globalConfig.FriendRecommendRandomNum)).Val()
	}
	currentNum := int32(len(allRecommend))
	if currentNum < globalConfig.FriendRecommendRandomNum {
		// 本服人数不足,执行全服随机补充
		for i := currentNum; i < globalConfig.FriendRecommendRandomNum; i++ {
			tmpServerId := rand.Intn(int(r.ServerId))
			if tmpServerId <= 0 {
				tmpServerId = 1
			}
			tmpKey := key.GetFriendRecommendKey(int32(tmpServerId))
			tmpQueueLen := int32(cache.Redis.Cmd.LLen(ctx, tmpKey).Val())
			tmpRandIndex := int64(rand.Intn(int(tmpQueueLen)))
			tmpResult := cache.Redis.Cmd.LIndex(ctx, tmpKey, tmpRandIndex).Val()
			allRecommend = append(allRecommend, tmpResult)
		}
	}
	// 过滤
	var filterMap = map[string]string{}
	for i := 0; i < len(allRecommend); {
		v := allRecommend[i]
		randUserId, error := strconv.ParseUint(v, 10, 64)
		if error != nil {
			continue
		}
		if _, ok := filterMap[v]; ok {
			// 不能重复推荐
			allRecommend = append(allRecommend[:i], allRecommend[i+1:]...)
			continue
		}
		if randUserId == r.Id {
			// 不能推荐自己
			allRecommend = append(allRecommend[:i], allRecommend[i+1:]...)
			continue
		}
		if CheckFriendInfosByUserId(r, randUserId) {
			// 不能推荐好友
			allRecommend = append(allRecommend[:i], allRecommend[i+1:]...)
			continue
		}
		if CheckBlackInfosByUserId(r, randUserId) {
			// 不能推荐拉黑
			allRecommend = append(allRecommend[:i], allRecommend[i+1:]...)
			continue
		}
		filterMap[v] = v
		i++
	}
	// 乱序
	for i := 0; i < len(allRecommend); i++ {
		randNum := rand.Intn(len(allRecommend))
		allRecommend[i], allRecommend[randNum] = allRecommend[randNum], allRecommend[i]
	}
	var c redisdb.PlayerInfoCache
	for _, v := range allRecommend {
		tmpUserId, _ := strconv.ParseUint(v, 10, 64)
		tmpR := c.Find(tmpUserId)
		friendInfo := buildFriendInfoResult(tmpUserId, 0, msg.RewardStatus_RS_NOSEND, msg.FriendRewradReceive_FRR_NOSENDREWARD, CheckApplyInfosByUserId(tmpR, r.Id))
		if friendInfo == nil {
			continue
		}
		recommendInfos = append(recommendInfos, friendInfo)
	}
	return
}

// 搜索用户
func SearchPlayerInfo(r *msg.PlayerInfo, searchKey string, err *msg.ErrorInfo) (searchInfos []*msg.FriendInfoResult, friendCurNum, friendMaxNum int32) {
	globalConfig, _ := conf.GetGlobalConfig(1)
	if !CheckConfData(globalConfig, 1, err) {
		return
	}
	// 初始化好友人数和上限
	rFriend := MustGetPlayerInfoFriend(r)
	friendCurNum = int32(len(rFriend.FriendInfos))
	friendMaxNum = globalConfig.FriendNumLimit
	var c redisdb.PlayerInfoCache
	fakeUserId, error := strconv.ParseInt(searchKey, 10, 32)
	if error == nil {
		// 根据玩家ID搜索
		targetUserId := GetPlayerIdByFakeUserId(int32(fakeUserId))
		if targetUserId > 0 && targetUserId != r.Id {
			rByUserId := c.Find(targetUserId)
			searchById := buildFriendInfoResult(targetUserId, 0, msg.RewardStatus_RS_NOSEND, msg.FriendRewradReceive_FRR_NOSENDREWARD, CheckApplyInfosByUserId(rByUserId, r.Id))
			searchInfos = append(searchInfos, searchById)
		}
	}
	// 判断搜索名字是否为初始化名字
	if strconv.FormatInt(int64(globalConfig.InitUserName), 10) != searchKey {
		// 根据玩家名称搜索
		targetUserId := GetPlayerIdByName(searchKey)
		if targetUserId > 0 && targetUserId != r.Id {
			rByName := c.Find(targetUserId)
			searchByName := buildFriendInfoResult(targetUserId, 0, msg.RewardStatus_RS_NOSEND, msg.FriendRewradReceive_FRR_NOSENDREWARD, CheckApplyInfosByUserId(rByName, r.Id))
			searchInfos = append(searchInfos, searchByName)
		}
	}
	return
}

// 快速领取
func QuickGet(rId, getId uint64, reqType msg.FriendReqType, err *msg.ErrorInfo) (getNum int32, addedObjsResult []*msg.ObjInfo) {
	rKeyLock := keyOfPlayerFriendInfos(rId)
	lockValue, ok := dlock.AcquireLockWithTimeoutX(rKeyLock, gconst.DLockTtl, gconst.DLockGetTimeout)
	if !ok {
		return
	}
	defer dlock.ReleaseLockX(rKeyLock, lockValue)

	var c redisdb.PlayerInfoCache
	r := c.Find(rId)
	if r == nil {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, gconst.ErrNoUser.Error()
		return
	}
	rFriend := MustGetPlayerInfoFriend(r)
	globalConfig, _ := conf.GetGlobalConfig(1)
	if !CheckConfData(globalConfig, 1, err) {
		return
	}

	// 一键领取
	for i, v := range rFriend.FriendInfos {
		if v.GetStatus == msg.FriendRewradReceive_FRR_INReceive || v.GetStatus == msg.FriendRewradReceive_FRR_NOSENDREWARD {
			// 已领取或者未赠送
			continue
		}
		if reqType == msg.FriendReqType_FRT_Normal {
			// 模式为一键赠送和领取
			if rFriend.DayGetNum >= globalConfig.FriendReceiveHeartDailyLimit {
				// 一键领取处理逻辑
				break
			}
		} else if reqType == msg.FriendReqType_FRT_GET {
			// 单独领取处理逻辑
			if rFriend.DayGetNum >= globalConfig.FriendReceiveHeartDailyLimit {
				err.Id, err.DebugMsg = 2409, "102617"
				return
			}
		} else {
			break
		}
		if v.FriendUserId == getId || reqType == msg.FriendReqType_FRT_Normal {
			getNum++
			rFriend.DayGetNum = rFriend.DayGetNum + 1
			rFriend.FriendInfos[i].GetStatus = msg.FriendRewradReceive_FRR_INReceive
			//添加爱心
			objs, _ := JsonToPb_StaticObjs(globalConfig.FriendGetHeartGroup, globalConfig, globalConfig.Id, err)
			if err.Id != 0 {
				return
			}
			// 添加挑战劵
			addedObjs, _ := AddStaticObjs(r, objs, err)
			addedObjsResult = append(addedObjsResult, addedObjs...)
		}
	}
	c.MustUpdate(rId, r)

	return
}

// 快速赠送
func QuickSend(rId, sendId uint64, reqType msg.FriendReqType, err *msg.ErrorInfo) (sendNum int32) {
	rKeyLock := keyOfPlayerFriendInfos(rId)
	lockValue, ok := dlock.AcquireLockWithTimeoutX(rKeyLock, gconst.DLockTtl, gconst.DLockGetTimeout)
	if !ok {
		return
	}
	defer dlock.ReleaseLockX(rKeyLock, lockValue)

	var c redisdb.PlayerInfoCache
	r := c.Find(rId)
	if r == nil {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, gconst.ErrNoUser.Error()
		return
	}
	rFriend := MustGetPlayerInfoFriend(r)
	globalConfig, _ := conf.GetGlobalConfig(1)
	if !CheckConfData(globalConfig, 1, err) {
		return
	}
	// 一键赠送
	for i, v := range rFriend.FriendInfos {
		if v.SendStatus == msg.RewardStatus_RS_INSEND {
			// 已赠送
			continue
		}
		if reqType == msg.FriendReqType_FRT_Normal {
			if rFriend.DaySendNum >= globalConfig.FriendGiveHeartDailyLimit {
				// 一键赠送处理逻辑
				break
			}
		} else if reqType == msg.FriendReqType_FRT_SEND {
			// 单独赠送处理逻辑
			if rFriend.DaySendNum >= globalConfig.FriendGiveHeartDailyLimit {
				err.Id, err.DebugMsg = 2411, "102619"
				return
			}
		} else {
			break
		}
		if v.FriendUserId == sendId || reqType == msg.FriendReqType_FRT_Normal {
			// 发送爱心
			SendLove(rId, v.FriendUserId, err)
			if err.Id != 0 {
				continue
			}
			sendNum++
			rFriend.DaySendNum = rFriend.DaySendNum + 1
			rFriend.FriendInfos[i].SendStatus = msg.RewardStatus_RS_INSEND
		}
	}
	MustUpdatePlayerInfoFriend(rId, rFriend)
	return
}

// 赠送爱心
func SendLove(sendId, getId uint64, err *msg.ErrorInfo) {
	rKeyLock := keyOfPlayerFriendInfos(getId)
	lockValue, ok := dlock.AcquireLockWithTimeoutX(rKeyLock, gconst.DLockTtl, gconst.DLockGetTimeout)
	if !ok {
		return
	}
	defer dlock.ReleaseLockX(rKeyLock, lockValue)

	var c redisdb.PlayerInfoCache
	r := c.Find(getId)
	if r == nil {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, gconst.ErrNoUser.Error()
		return
	}
	rFriend := MustGetPlayerInfoFriend(r)

	// 发送爱心
	for i, v := range rFriend.FriendInfos {
		if v.FriendUserId == sendId {
			rFriend.FriendInfos[i].GetStatus = msg.FriendRewradReceive_FRR_NOReceive
			break
		}
	}
	MustUpdatePlayerInfoFriend(getId, rFriend)
	// 推送赠送爱心红点
	RTMNotifyRedPoint(r.Id, msg.RedPointType_RPT_TriggerNewFriendLove, 0, 0)
}

// 赠送和领取爱心
func QuickSendAndGet(rId, userId uint64, reqType msg.FriendReqType, err *msg.ErrorInfo) (getNum, sendNum int32, friendResult *TmpFriendResult, addedObjs []*msg.ObjInfo) {
	// 快速领取
	getNum, addedObjs = QuickGet(rId, userId, reqType, err)
	if err.Id != 0 {
		return
	}
	// 快速赠送
	sendNum = QuickSend(rId, userId, reqType, err)
	if err.Id != 0 {
		return
	}
	if getNum >= 1 && sendNum >= 1 {
		// 领取并赠送成功
		err.Id, err.DebugMsg = msg.Ecode_OK, "102622"
	} else if getNum >= 1 && sendNum == 0 {
		// 领取成功
		err.Id, err.DebugMsg = msg.Ecode_OK, "102618"
	} else if getNum == 0 && sendNum >= 1 {
		// 赠送成功
		err.Id, err.DebugMsg = msg.Ecode_OK, "102620"
	} else {
		// 没有可领取和赠送的
		err.Id, err.DebugMsg = msg.Ecode_OK, "102621"
	}
	friendResult = GetFriendInfos(rId, err)
	return
}

// 刷新好友申请红点
func RefreshFriendApplyRedPoint(r *msg.PlayerInfo, notify bool) {
	rFriend := MustGetPlayerInfoFriend(r)
	// 只有0和1区别！
	curCount := int32(0)

	for _, v := range rFriend.ApplyInfos {
		if checkApplyInfoByTimeLimit(r, v.ApplyUserId) {
			curCount += 1
			break
		}
	}

	_, rp := FindRedPointAnyway(r, &msg.RedPoint{
		Type: msg.RedPointType_RPT_FriendApply,
	})

	if rp.Count != curCount {
		rp.Count = curCount
	}
	if notify {
		NotifyRedPoint(r, rp)
	}
}

// 刷新好友爱心红点
func RefreshFriendLoveRedPoint(r *msg.PlayerInfo, notify bool) {
	rFriend := MustGetPlayerInfoFriend(r)
	// 只有0和1区别！
	curCount := int32(0)
	for _, v := range rFriend.FriendInfos {
		if v.GetStatus == msg.FriendRewradReceive_FRR_NOReceive || v.SendStatus == msg.RewardStatus_RS_NOSEND {
			curCount = 1
			break
		}
	}

	_, rp := FindRedPointAnyway(r, &msg.RedPoint{
		Type: msg.RedPointType_RPT_FriendLove,
	})

	if rp.Count != curCount {
		rp.Count = curCount
	}
	if notify {
		NotifyRedPoint(r, rp)
	}
}

// 检查是否在黑名单列表
func CheckBlackInfosByUserId(r *msg.PlayerInfo, checkId uint64) bool {
	rFriend := MustGetPlayerInfoFriend(r)
	for _, v := range rFriend.BlackInfos {
		if v.BlackUserId == checkId {
			return true
		}
	}
	return false
}

// 检查是否在好友列表
func CheckFriendInfosByUserId(r *msg.PlayerInfo, checkUserId uint64) bool {
	rFriend := MustGetPlayerInfoFriend(r)
	for _, v := range rFriend.FriendInfos {
		if v.FriendUserId == checkUserId {
			return true
		}
	}
	return false
}

// 检查是否在申请列表
func CheckApplyInfosByUserId(r *msg.PlayerInfo, checkUserId uint64) msg.FriendApplyType {
	if r != nil {
		rFriend := MustGetPlayerInfoFriend(r)
		for _, v := range rFriend.ApplyInfos {
			if v.ApplyUserId == checkUserId {
				// 判断申请有效期
				if checkApplyInfoByTimeLimit(r, checkUserId) {
					return msg.FriendApplyType_FAT_INAPPLY
				}
			}
		}
	}
	return msg.FriendApplyType_FAT_NOAPPLY
}

// 检查是否达到好友上限
func checkFriendInfosMaxLimit(r *msg.PlayerInfo) bool {
	rFriend := MustGetPlayerInfoFriend(r)
	globalConfig, _ := conf.GetGlobalConfig(1)
	if !CheckConfData(globalConfig, 1, &msg.ErrorInfo{}) {
		return true
	}
	if int32(len(rFriend.FriendInfos)) >= globalConfig.FriendNumLimit {
		return true
	}
	return false
}

// 检查是否达到申请上限
func checkApplyInfosMaxLimit(r *msg.PlayerInfo) bool {
	rFriend := MustGetPlayerInfoFriend(r)
	globalConfig, _ := conf.GetGlobalConfig(1)
	if !CheckConfData(globalConfig, 1, &msg.ErrorInfo{}) {
		return true
	}
	if int32(len(rFriend.ApplyInfos)) >= globalConfig.FriendAcceptApplyLimit {
		return true
	}
	return false
}

// 检查申请是否在有效期
func checkApplyInfoByTimeLimit(r *msg.PlayerInfo, checkUserId uint64) bool {
	rFriend := MustGetPlayerInfoFriend(r)
	globalConfig, _ := conf.GetGlobalConfig(1)
	if !CheckConfData(globalConfig, 1, &msg.ErrorInfo{}) {
		return false
	}
	var currentTime = time.Now().Unix()
	for _, v := range rFriend.ApplyInfos {
		if v.ApplyUserId != checkUserId {
			continue
		}
		// 有效期
		var friendApplyValid = int64(globalConfig.FriendApplyValid * int32(time.Hour.Seconds()))
		if v.ApplyTime+friendApplyValid > currentTime {
			return true
		}
	}
	return false
}

func buildFriendInfoResult(userId uint64, friendTime int64, sendStatus msg.RewardStatus, rewardStatus msg.FriendRewradReceive, applyStatus msg.FriendApplyType) (result *msg.FriendInfoResult) {
	var c redisdb.PlayerInfoCache
	r := c.Find(userId)
	if r == nil {
		return nil
	}
	result = &msg.FriendInfoResult{
		ServerId:      r.ServerId,
		FriendUserId:  r.Id,
		FriendName:    r.Name,
		FriendHeadUrl: r.Icon,
		FriendLevel:   int32(GetCount_Currency(r, msg.CurrencyId_CI_PlayerLevel)),
		AllPower:      r.TotalPower,
		PassMaxHoldId: r.MaxFiniMainLineAry[0],
		LastLoginTime: r.LastLogoutTime,
		SendStatus:    sendStatus,
		RewardStatus:  rewardStatus,
		FriendTime:    friendTime,
		ApplyStatus:   applyStatus,
		FakeUserId:    r.FakeUserId,
		ModifyNameNum: r.ModifyNameNum,
	}
	return result
}
