package game

import (
	"bitbucket.org/funplus/sandwich/current"
	"server/internal/xrtm"
	"server/pkg/fungin/xmiddleware"
	"server/pkg/gen/msg"
)

// 返回的引用或空
// 不考虑count
func FindRedPoint(r *msg.PlayerInfo, rp *msg.RedPoint) (index int32, val *msg.RedPoint) {
	for k, v := range r.RedPointAry {
		if v.Type == rp.Type && v.Param1 == rp.Param1 && v.Param2 == rp.Param2 {
			return int32(k), v
		}
	}

	return -1, nil
}

func FindRedPointAnyway(r *msg.PlayerInfo, rp *msg.RedPoint) (index int32, val *msg.RedPoint) {
	for k, v := range r.RedPointAry {
		if v.Type == rp.Type && v.Param1 == rp.Param1 && v.Param2 == rp.Param2 {
			return int32(k), v
		}
	}

	r.RedPointAry = append(r.RedPointAry, rp)

	return int32(len(r.RedPointAry) - 1), rp
}

func FindFirstByTypeAndP1(r *msg.PlayerInfo, t msg.RedPointType, param1 int32) (index int32, val *msg.RedPoint) {
	for k, v := range r.RedPointAry {
		if v.Type == t && v.Param1 == param1 {
			return int32(k), v
		}
	}

	return -1, nil
}

func ClearRedPoint(r *msg.PlayerInfo, rp *msg.RedPoint) {
	index, _ := FindRedPoint(r, rp)
	if index != -1 {
		r.RedPointAry = append(r.RedPointAry[:index], r.RedPointAry[index+1:]...)
	}
}

// 针对他人的操作，不需存档，直接在线rtm推送
func RTMNotifyRedPoint(targetUid uint64, rType msg.RedPointType, param1 int32, param2 int32) {
	rp := &msg.RedPoint{
		Type:   rType,
		Param1: param1,
		Param2: param2,
		Count:  1,
	}
	n := &msg.RedPointNotify{
		RedPoints: []*msg.RedPoint{rp},
	}

	xrtm.SendProto(targetUid, n)
}

// 针对自己的操作
// 有则更新，无则插入
// 增加一个红点提示
func AddRedPointForMyself(r *msg.PlayerInfo, rType msg.RedPointType, param1 int32, param2 int32, notify bool) *msg.RedPoint {
	rp := &msg.RedPoint{
		Type:   rType,
		Param1: param1,
		Param2: param2,
		Count:  1,
	}
	//log.Debugf("add1 %+v", rp)
	_, val := FindRedPoint(r, rp)
	if val != nil {
		rp = val
		rp.Count++
	} else {
		r.RedPointAry = append(r.RedPointAry, rp)
	}
	//log.Debugf("add1 %+v", rp)
	if notify {
		NotifyRedPoint(r, rp)
	}
	return rp
}

func NotifyRedPoint(r *msg.PlayerInfo, rp *msg.RedPoint) {
	ctx := xmiddleware.GetCtxByUid(r.Id)
	if ctx != nil {
		n := &msg.RedPointNotify{
			RedPoints: []*msg.RedPoint{rp},
		}
		current.MustAppendAdditionalOutgoingMessage(ctx, n)
	}
}

func resetDailyRedpoint(r *msg.PlayerInfo) {
	for i := int(msg.RedPointType_RPT_DailyStart) + 1; i < int(msg.RedPointType_RPT_DailyEnd); i++ {
		AddRedPointForMyself(r, msg.RedPointType(i), 0, 0, false)
	}
	//未完成的迷宫 未完成有客户端负责
	//每日首抽？
	//任务大厅每日重置后
	//每日公会boss
	//每日公会捐献次数
	//...

	// 删除已关闭活动对应的红点
	tryClearRedPointOfClosedActivity(r)
}

// 删除已关闭活动对应的红点
func tryClearRedPointOfClosedActivity(r *msg.PlayerInfo) {
	act := MustGetPlayerInfoActivity(r)
	for _, d := range act.Data {
		if d.Status == msg.ActivityStatus_AS_Closed {
			// i 没有意义，安全起见，加的i
			for i := 0; i < 100; i++ {
				index, _ := FindFirstByTypeAndP1(r, msg.RedPointType_RPT_Activity, d.Id)
				if index == -1 {
					break
				}
				// remove it.
				r.RedPointAry = append(r.RedPointAry[:index], r.RedPointAry[index+1:]...)
			}
		}
	}
}

func notifyAllRedPoints(r *msg.PlayerInfo) {
	ctx := xmiddleware.GetCtxByUid(r.Id)
	if ctx != nil {
		n := &msg.RedPointNotify{
			RedPoints: r.RedPointAry,
		}
		current.MustAppendAdditionalOutgoingMessage(ctx, n)
	}
}
