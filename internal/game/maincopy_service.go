package game

import (
	"fmt"
	"server/pkg/gen/conf"
	"server/pkg/gen/msg"
	"server/pkg/gen/rawdata"
	"strconv"
)

const (
	// 序章初始章节ID
	MainIntoned int32 = 1000
)

// 初始化章节
func InitMainCopy(r *msg.PlayerInfo, err *msg.ErrorInfo) {
	r2 := MustGetPlayerInfoExt(r)
	if len(r2.MainCopyInfos) == 0 {
		// 解锁初始章节
		m := UnLockMainCopy(MainIntoned, err)
		if err.Id != 0 {
			return
		}
		// 激活章节
		m.ChapterStatus = msg.ChapterStatus_CS_ING
		//初始化副本章节
		r2.MainCopyInfos = map[int32]*msg.MainCopyInfo{}
		r2.MainCopyInfos[m.ChapterId] = m
	}
}

// 检查解锁新章节
func CheckMainCopyUnLock(r *msg.PlayerInfo, v *msg.MainCopyResult, confData *rawdata.MainInstanceConfig, err *msg.ErrorInfo) (openChapterId int32) {
	r2 := MustGetPlayerInfoExt(r)
	// 计算章节完成度
	chapterProcess := float64(v.ChapterScale) / float64(confData.ProgressValue) * 100
	chapterStr := fmt.Sprintf("%.1f", chapterProcess)
	chapterProcess, _ = strconv.ParseFloat(chapterStr, 32)
	// 检查完成度是否达到新章节开启条件
	for k, v := range confData.OpenNextCondition {
		if r2.MainCopyInfos[k] != nil {
			// 章节已解锁
			continue
		}
		if chapterProcess < float64(v) {
			// 完成度无法解锁
			continue
		}
		m := UnLockMainCopy(k, err)
		if err.Id != 0 {
			return
		}

		r2.MainCopyInfos[m.ChapterId] = m
		openChapterId = m.ChapterId
	}
	return
}

// 解锁指定章节
func UnLockMainCopy(confId int32, err *msg.ErrorInfo) *msg.MainCopyInfo {
	confData, _ := conf.GetMainInstanceConfig(confId)
	if !CheckConfData(confData, confId, err) {
		return nil
	}
	// 据点信息
	shArray := map[int32]*msg.StrongholdInfo{}
	for _, v := range confData.StrongholdId {
		holdData, _ := conf.GetMainInstanceStrongholdConfig(v)
		if !CheckConfData(holdData, v, err) {
			return nil
		}
		// 据点状态
		StrongholdStatus := msg.StrongholdStatus_SS_CLOSE
		// 根据条件判断据点开启状态
		for _, v := range holdData.OpenCondition {
			if v == 0 {
				// 开启据点
				StrongholdStatus = msg.StrongholdStatus_SS_ING
			}
		}
		h := &msg.StrongholdInfo{
			StrongholdId:     holdData.Id,
			StrongholdStatus: StrongholdStatus,
		}
		shArray[holdData.Id] = h
	}
	m := &msg.MainCopyInfo{
		ChapterId:       confData.Id,
		FirstJoin:       true,
		ChapterStatus:   msg.ChapterStatus_CS_OPEN,
		StrongholdInfos: shArray,
	}
	return m
}

// 构造章节列表返回
func BuildMainCopyInfoResults(r *msg.PlayerInfo, err *msg.ErrorInfo) []*msg.MainCopyResult {
	r2 := MustGetPlayerInfoExt(r)
	// 返回结果
	result := make([]*msg.MainCopyResult, len(r2.MainCopyInfos))
	index := 0
	for _, v := range r2.MainCopyInfos {
		confData, _ := conf.GetMainInstanceConfig(v.ChapterId)
		if !CheckConfData(confData, v.ChapterId, err) {
			continue
		}
		// 章节返回信息
		m := buildMainCopyInfoResult(v, err)
		// 检查开启新章节
		CheckMainCopyUnLock(r, m, confData, err)
		result[index] = m
		index++
	}
	return result
}

// 内部调用-构建章节返回信息
func buildMainCopyInfoResult(v *msg.MainCopyInfo, err *msg.ErrorInfo) (m *msg.MainCopyResult) {
	// 基础副本数据
	confData, _ := conf.GetMainInstanceConfig(v.ChapterId)
	if !CheckConfData(confData, v.ChapterId, err) {
		return
	}
	// 主线目标
	chapterTarget := map[int32]bool{}
	for pi, pv := range confData.Purpose {
		shinto, ok := v.StrongholdInfos[pv]
		if ok && shinto.StrongholdStatus == msg.StrongholdStatus_SS_OPEN {
			chapterTarget[confData.PurposeDes[pi]] = true
		} else {
			chapterTarget[confData.PurposeDes[pi]] = false
		}
	}
	// 主线完成度
	var chapterScale int32 = 0
	for sid, sv := range v.StrongholdInfos {
		if sv.StrongholdStatus == msg.StrongholdStatus_SS_OPEN {
			// 基础据点数据
			holdData, _ := conf.GetMainInstanceStrongholdConfig(sid)
			if !CheckConfData(holdData, sid, err) {
				return
			}
			chapterScale += holdData.Progress
		}
	}
	m = &msg.MainCopyResult{
		ChapterId:     v.ChapterId,
		FirstJoin:     v.FirstJoin,
		ChapterStatus: v.ChapterStatus,
		ChapterTarget: chapterTarget,
		ChapterScale:  chapterScale,
	}
	return
}

// 构造据点列表返回
func BuildStrongHoldResults(r *msg.PlayerInfo, chapterId int32) []*msg.StrongholdInfo {
	r2 := MustGetPlayerInfoExt(r)
	// 指定章节
	m := r2.MainCopyInfos[chapterId]
	// 返回结果
	var result []*msg.StrongholdInfo
	for _, value := range m.StrongholdInfos {
		result = append(result, value)
	}
	// 进入据点列表,激活状态
	for _, v := range r2.MainCopyInfos {
		if v.ChapterId == chapterId {
			// 进行中
			v.ChapterStatus = msg.ChapterStatus_CS_ING
		} else {
			// 可进入
			v.ChapterStatus = msg.ChapterStatus_CS_OPEN
		}
	}
	return result
}

// 结算主线副本据点
func MainCopyInfoBattle(r *msg.PlayerInfo, chapterId int32, strongholdId int32, err *msg.ErrorInfo) (addedObjs []*msg.ObjInfo, strongholdInfo []*msg.StrongholdInfo, openChapterId int32) {
	confData, _ := conf.GetMainInstanceConfig(chapterId)
	if !CheckConfData(confData, chapterId, err) {
		return
	}
	// 据点基础数据
	holdData, _ := conf.GetMainInstanceStrongholdConfig(strongholdId)
	if !CheckConfData(holdData, strongholdId, err) {
		return
	}
	// 根据战斗类型判断奖励
	if holdData.Type == 1 {
		// 战斗
		configData, _ := conf.GetInstanceConfig(holdData.BattleId)
		if !CheckConfData(configData, strongholdId, err) {
			return
		}
		// 击杀奖励
		rewards, _ := JsonToPb_StaticObjs(configData.KillReward, configData, configData.Id, err)
		if err.Id != 0 {
			return
		}
		addedObjs, _ = AddStaticObjs(r, rewards, err)
		if err.Id != 0 {
			return
		}
	} else {
		// 采集奖励
		rewards, _ := JsonToPb_StaticObjs(holdData.CollectId, holdData, holdData.Id, err)
		if err.Id != 0 {
			return
		}
		addedObjs, _ = AddStaticObjs(r, rewards, err)
		if err.Id != 0 {
			return
		}
	}
	r2 := MustGetPlayerInfoExt(r)
	// 更新据点状态
	r2.MainCopyInfos[chapterId].StrongholdInfos[strongholdId].StrongholdStatus = msg.StrongholdStatus_SS_OPEN
	// 判断挑战据点是否为本章终结据点
	if confData.FinalBattleStronghold == strongholdId {
		// 本章结束,更新任务
		RefreshTaskCount(r,
			msg.TaskCondType_TCT_MainChapter,
			int32(msg.TCT_MainChapterType_TCTM_Chapter),
			0,
			int64(chapterId))
	}
	// 如果是新完成了主线，则更新任务数据
	if holdData.IsMainLine {
		if strongholdId > r.MaxFiniMainLineAry[0] {
			// 刷新最大主线关卡及任务
			r.MaxFiniMainLineAry[0] = strongholdId

			// 关卡进度
			// 使用关卡编号
			RefreshTaskCount(r,
				msg.TaskCondType_TCT_MainChapter,
				int32(msg.TCT_MainChapterType_TCTM_Stronghold),
				0,
				int64(r.MaxFiniMainLineAry[0]))
		}
	}
	// 解锁新据点
	for k, v := range r2.MainCopyInfos[chapterId].StrongholdInfos {
		if v.StrongholdStatus == msg.StrongholdStatus_SS_ING || v.StrongholdStatus == msg.StrongholdStatus_SS_OPEN {
			// 据点已解锁
			continue
		}
		// 基础信息
		confData, _ := conf.GetMainInstanceStrongholdConfig(k)
		if !CheckConfData(confData, k, err) {
			continue
		}
		// 判断条件是否已完成
		orderOver := true
		for _, v := range confData.OpenCondition {
			stronghold, ok := r2.MainCopyInfos[chapterId].StrongholdInfos[v]
			if !ok || stronghold.StrongholdStatus != msg.StrongholdStatus_SS_OPEN {
				// 前置条件未完成
				orderOver = false
				break
			}
		}
		if orderOver {
			// 开启新据点
			v.StrongholdStatus = msg.StrongholdStatus_SS_ING
		}
	}
	// 解锁新章节
	v := buildMainCopyInfoResult(r2.MainCopyInfos[chapterId], err)
	openChapterId = CheckMainCopyUnLock(r, v, confData, err)

	// 进入据点列表,激活状态
	for _, v := range r2.MainCopyInfos {
		if v.ChapterId == chapterId {
			// 进行中
			v.ChapterStatus = msg.ChapterStatus_CS_ING
		} else {
			// 可进入
			v.ChapterStatus = msg.ChapterStatus_CS_OPEN
		}
	}
	// 据点返回信息
	strongholdInfo = BuildStrongHoldResults(r, chapterId)

	return
}

// 主线副本战斗校验(battleType=1：战斗 2：采集)
func CheckMainCopyBattleStatus(r *msg.PlayerInfo, configId int32, battleType int32, err *msg.ErrorInfo) (checkResult bool, chapterId int32, strongholdId int32) {
	r2 := MustGetPlayerInfoExt(r)
	for k, v := range r2.MainCopyInfos {
		if v.ChapterStatus != msg.ChapterStatus_CS_ING {
			continue
		}
		for k1, v1 := range v.StrongholdInfos {
			if v1.StrongholdId != configId {
				continue
			}
			if v1.StrongholdStatus != msg.StrongholdStatus_SS_ING {
				// 据点状态不是进行中
				if err != nil {
					err.Id, err.DebugMsg = msg.Ecode_INVALID, "Second request"
					err.Params = append(err.Params, fmt.Sprintf("Second request: %T(%d)", battleType, configId))
				}
				return
			}
			// 据点基础数据
			holdData, _ := conf.GetMainInstanceStrongholdConfig(k1)
			if !CheckConfData(holdData, k1, err) {
				continue
			}
			if holdData.Id == configId && holdData.Type == battleType {
				// 战斗类型相同,战斗ID相同。验证通过
				checkResult = true
				chapterId = k
				strongholdId = k1
				break
			}
		}
		if checkResult {
			break
		}
	}
	return
}

// 修改用户的主线副本章节和据点
func ModifyMainCopyByPlayerInfo(r *msg.PlayerInfo, chapterId int32, strongholdId int32, err *msg.ErrorInfo) {
	if chapterId < MainIntoned {
		// 章节ID错误
		if err != nil {
			err.Id, err.DebugMsg = msg.Ecode_INVALID, "chapterId less chapterInitId"
			err.Params = append(err.Params, fmt.Sprintf("chapterId=%s less chapterInitId=%s", chapterId, MainIntoned))
			return
		}
	}
	r2 := MustGetPlayerInfoExt(r)
	// 清空用户主线副本进度
	r2.MainCopyInfos = map[int32]*msg.MainCopyInfo{}
	// 根据章节和据点重新设置进度
	for i := MainIntoned; i <= chapterId; i++ {
		// 解锁章节
		m := UnLockMainCopy(i, err)
		if err.Id != 0 {
			continue
		}
		r2.MainCopyInfos[m.ChapterId] = m
		// 结算据点
		for k, _ := range r2.MainCopyInfos[m.ChapterId].StrongholdInfos {
			if k >= strongholdId {
				continue
			}
			MainCopyInfoBattle(r, i, k, err)
		}
	}
	return
}
