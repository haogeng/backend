package game

import (
	"bitbucket.org/funplus/sandwich/current"
	"fmt"
	"server/internal"
	"server/internal/log"
	"server/pkg/fungin/xmiddleware"
	"server/pkg/gen/conf"
	"server/pkg/gen/msg"
	"server/pkg/gen/rawdata"
)

func MakeSureTaskExist(r *msg.PlayerInfo) {
	if r.Tasks == nil {
		r.Tasks = &msg.TaskInfo{
			TaskCounts: make(map[int32]int64),
			TaskLevels: make(map[int32]int32),
		}
	}

	if r.Tasks.TaskCounts == nil {
		r.Tasks.TaskCounts = make(map[int32]int64)
	}

	if r.Tasks.TaskLevels == nil {
		r.Tasks.TaskLevels = make(map[int32]int32)
	}
}

func CheckTaskCondForActive(r *msg.PlayerInfo, taskId int32) bool {
	MakeSureTaskExist(r)

	taskConf, _ := conf.GetAllTaskConfig(taskId)
	if taskConf == nil {
		return false
	}

	// 激活只有可能是最大值类型的条件
	curDestVal := getTaskDestValRealTime(r, msg.TaskCondType(taskConf.ActiveCondType), taskConf.ActiveCondParam)
	if curDestVal >= int64(taskConf.ActiveCondNeed) {
		return true
	}
	return false
}

func CheckTaskCondForComplete(r *msg.PlayerInfo, taskId int32, level int32) bool {
	MakeSureTaskExist(r)

	taskConf, _ := conf.GetAllTaskConfig(taskId)
	if taskConf == nil {
		return false
	}

	if need, ok := taskConf.CompleteCondNeeds[level]; ok {
		// 包括了destValue
		if c, ok2 := r.Tasks.TaskCounts[taskId]; ok2 {
			if c >= int64(need) {
				return true
			}
		}
	}

	return false
}

// 实时获取的
func getTaskDestValRealTime(r *msg.PlayerInfo, condType msg.TaskCondType, condParam int32) int64 {
	if condType >= msg.TaskCondType_TCT_CountTypeStart {
		panic("condType err")
	}

	switch condType {
	case msg.TaskCondType_TCT_MainChapter:
		return int64(r.MaxFiniMainLineAry[0])

	case msg.TaskCondType_TCT_MainChapterNormal:
		return int64(r.MaxFiniMainLineAry[1])

	case msg.TaskCondType_TCT_MainChapterHard:
		return int64(r.MaxFiniMainLineAry[2])

	//case msg.TaskCondType_TCT_HallLevel:
	//	b := GetBuildingByType(r, int32(msg.BuildingType_BT_CityHall))
	//	if b != nil {
	//		return int64(b.Level)
	//	}
	//	break

	case msg.TaskCondType_TCT_Prosperity:
		return GetCount_Currency(r, msg.CurrencyId_CI_Prosperity)

	case msg.TaskCondType_TCT_GotTroopOwnNums:
		return int64(r.GotTroopTotalNums)

	case msg.TaskCondType_TCT_ArenaPoints:
		//TODO
		break

	case msg.TaskCondType_TCT_BuildingLevel:
		b := GetBuildingByType(r, int32(condParam))
		if b != nil {
			return int64(b.Level)
		}
		break

	case msg.TaskCondType_TCT_CurrencyNums:
		return GetCount_Currency(r, msg.CurrencyId(condParam))

	case msg.TaskCondType_TCT_OwnActivity:
		act := MustGetPlayerInfoActivity(r)
		if act != nil {
			ad := GetActivity(r, condParam, &msg.ErrorInfo{})
			if ad != nil {
				if ad.Status == msg.ActivityStatus_AS_Opened {
					return 1
				}
			}
		}
		return 0
	}
	return 0
}

// 任务是否是真正激活
func IsTaskActived(r *msg.PlayerInfo, id int32) bool {
	if r.Tasks != nil && r.Tasks.TaskLevels != nil {
		if d, ok := r.Tasks.TaskLevels[id]; ok {
			return d > 0
		}
	}

	return false
}

//
func RefreshTaskNew(r *msg.PlayerInfo, condType msg.TaskCondType, condParam int32) {
	log.Debugf("u%d RefreshTaskNew condType:%v condParam:%v", r.Id, condType, condParam)
	MakeSureTaskExist(r)

	ids := internal.PmConf.GetTaskIdsByActiveCond(int32(condType), condParam)

	changedIds := []int32{}

	for _, id := range ids {
		// 通过taskLevels判断有无 没有才新增
		if !IsTaskActived(r, id) {
			// 判断是否满足条件
			if !CheckTaskCondForActive(r, id) {
				continue
			}

			taskConf, _ := conf.GetAllTaskConfig(id)
			if taskConf == nil {
				continue
			}

			changedIds = append(changedIds, id)
			addTask(r, taskConf)
		}
	}

	// notify
	c := xmiddleware.GetCtxByUid(r.Id)
	if c != nil && len(changedIds) > 0 {
		n := &msg.TaskNewNotify{
			TaskCounts: make(map[int32]int64),
		}
		for _, id := range changedIds {
			n.TaskCounts[id] = r.Tasks.TaskCounts[id]
		}
		current.MustAppendAdditionalOutgoingMessage(c, n)
	}
}

func addTask(r *msg.PlayerInfo, taskConf *rawdata.AllTaskConfig) {
	id := taskConf.Id
	if IsTaskActived(r, id) {
		panic(fmt.Errorf("task %d added repeated", id))
	}

	r.Tasks.TaskLevels[id] = 1
	if taskConf.CompleteCondType >= int32(msg.TaskCondType_TCT_CountTypeStart) {
		r.Tasks.TaskCounts[id] = 0
	} else {
		r.Tasks.TaskCounts[id] = getTaskDestValRealTime(r, msg.TaskCondType(taskConf.CompleteCondType), taskConf.CompleteCondParam)
	}
}

func delTask(r *msg.PlayerInfo, taskId int32) {
	MakeSureTaskExist(r)
	delete(r.Tasks.TaskLevels, taskId)
	delete(r.Tasks.TaskCounts, taskId)
}

func resetTask(r *msg.PlayerInfo, id int32) {
	// Couldn't delete task here cause this func could be called in iterator of map!!!
	if IsTaskActived(r, id) {
		r.Tasks.TaskLevels[id] = 1
	}

	if _, ok := r.Tasks.TaskCounts[id]; ok {
		// daily reset: so it must be count cond type
		r.Tasks.TaskCounts[id] = 0
	}
}

// 最终值变化或增量值
func RefreshTaskCount(r *msg.PlayerInfo, condType msg.TaskCondType, condParam int32, deltaCount int64, destVal int64) {
	log.Debugf("u%d RefreshTaskCount condType:%v condParam:%v dc:%v destV:%v", r.Id, condType, condParam, deltaCount, destVal)
	MakeSureTaskExist(r)

	ids := internal.PmConf.GetTaskIdsByCompleteCond(int32(condType), condParam)

	changedIds := []int32{}
	// intersect.
	for _, id := range ids {
		// 通过taskLevels判断有无
		if IsTaskActived(r, id) {
			changedIds = append(changedIds, id)

			// make sure count exist
			if _, ok2 := r.Tasks.TaskCounts[id]; !ok2 {
				r.Tasks.TaskCounts[id] = 0
			}

			// change
			if condType >= msg.TaskCondType_TCT_CountTypeStart {
				r.Tasks.TaskCounts[id] += deltaCount
			} else {
				r.Tasks.TaskCounts[id] = destVal
			}
		}
	}

	// notify
	NotifyTaskChange(r, changedIds)
}

func NotifyTaskChange(r *msg.PlayerInfo, changedIds []int32) {
	c := xmiddleware.GetCtxByUid(r.Id)
	if c != nil && len(changedIds) > 0 {
		n := &msg.TaskChangeNotify{
			TaskCounts: make(map[int32]int64),
			TaskLevels: make(map[int32]int32),
		}
		for _, id := range changedIds {
			n.TaskCounts[id], _ = r.Tasks.TaskCounts[id]
			n.TaskLevels[id], _ = r.Tasks.TaskLevels[id]
		}
		current.MustAppendAdditionalOutgoingMessage(c, n)
	}
}

func resetLoopTasks(r *msg.PlayerInfo, taskType msg.TaskType, withSync bool) {
	MakeSureTaskExist(r)

	changedIds := []int32{}
	for k, _ := range r.Tasks.TaskLevels {
		// 没激活的任务不重置
		if !IsTaskActived(r, k) {
			continue
		}
		taskConf, _ := conf.GetAllTaskConfig(k)
		if taskConf != nil && taskConf.TaskType == int32(taskType) {
			resetTask(r, k)
			changedIds = append(changedIds, k)
		}
	}

	var treasureType msg.TreasureType
	// 活跃点重置
	if taskType == msg.TaskType_TT_Daily {
		r.Currency[int32(msg.CurrencyId_CI_DailyTaskPoint)] = 0

		treasureType = msg.TreasureType_TRT_Daily
	} else if taskType == msg.TaskType_TT_Weekly {
		r.Currency[int32(msg.CurrencyId_CI_WeeklyTaskPoint)] = 0

		treasureType = msg.TreasureType_TRT_Weekly
	} else {
		panic("should never go here in resetLoopTasks")
	}

	resetTreasureRecvedData(r, treasureType)

	if withSync {
		// sync to client...
		NotifyTaskChange(r, changedIds)
		NotifyTreasureRecvedData(r, treasureType)

		// sync
		ctx := xmiddleware.GetCtxByUid(r.Id)
		changedKeys := []*msg.ObjKey{
			&msg.ObjKey{
				Id:   int32(msg.CurrencyId_CI_DailyTaskPoint),
				Type: msg.ObjType_OT_Currency,
			},
			&msg.ObjKey{
				Id:   int32(msg.CurrencyId_CI_WeeklyTaskPoint),
				Type: msg.ObjType_OT_Currency,
			},
		}

		TryAppendSyncObjsMsgWithChangedIds(ctx, r, nil, changedKeys)
	}
}

// 取的conf里的数据，上层不要修改！
// return key->[key -> confId...]
func GetTreasureKeyConfMap(treasureType msg.TreasureType, err *msg.ErrorInfo) map[int32]int32 {
	confData, _ := conf.GetGlobalConfig(1)
	if treasureType == msg.TreasureType_TRT_Daily {
		return confData.DailyTaskRewardList
	} else if treasureType == msg.TreasureType_TRT_Weekly {
		return confData.WeekTaskRewardList
	} else if treasureType == msg.TreasureType_TRT_ClanDaily {
		return confData.UnionDailyActiveRewardList
	}

	err.Id, err.DebugMsg = msg.Ecode_INVALID, fmt.Sprintf("Unknown treature type: %v", treasureType)
	return nil
}

func MakeSureTreasureRecvedData(r *msg.PlayerInfo) {
	if r.TreasureRecvedData == nil {
		r.TreasureRecvedData = make(map[int32]*msg.IntList)
	}
}
func resetTreasureRecvedData(r *msg.PlayerInfo, treasureType msg.TreasureType) {
	MakeSureTreasureRecvedData(r)
	r.TreasureRecvedData[int32(treasureType)] = &msg.IntList{
		Data: make([]int32, 0),
	}
}

func NotifyTreasureRecvedData(r *msg.PlayerInfo, treasureType msg.TreasureType) {
	MakeSureTreasureRecvedData(r)
	recvedKeys, ok := r.TreasureRecvedData[int32(treasureType)]
	if !ok {
		recvedKeys = &msg.IntList{
			Data: make([]int32, 0),
		}
	}
	ctx := xmiddleware.GetCtxByUid(r.Id)
	n := &msg.TreasureRecvedDataNotify{
		RecvedKeys: make(map[int32]*msg.IntList),
	}
	n.RecvedKeys[int32(treasureType)] = recvedKeys
	current.MustAppendAdditionalOutgoingMessage(ctx, n)
}

// 日循环和周循环任务，在角色创建时刷新一次，，设置成0级，实际语义是未激活
func RefreshTaskUnactiveButShow(r *msg.PlayerInfo) {
	MakeSureTaskExist(r)

	allTaskConfigs := conf.GetAllTaskConfigConf().AllTaskConfigs
	for _, v := range allTaskConfigs {
		if v.TaskType == int32(msg.TaskType_TT_Daily) ||
			v.TaskType == int32(msg.TaskType_TT_Weekly) {

			// 加新
			if _, ok := r.Tasks.TaskLevels[v.Id]; !ok {
				r.Tasks.TaskLevels[v.Id] = 0
			}
		}
	}
}

// 准备做任务奖励
// 会改变任务进度，，然后返回任务奖励，，但不直接加任务奖励
// 用于兼容邮件和直接领奖
func PrepareDoTaskAward(r *msg.PlayerInfo, taskId int32, err *msg.ErrorInfo) (rewards []*msg.StaticObjInfo) {
	// do logic with r
	MakeSureTaskExist(r)
	// 是否激活此任务
	if !IsTaskActived(r, taskId) {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "you haven't active the task"
		return
	}
	curLevel, ok := r.Tasks.TaskLevels[taskId]

	// 静态数据
	taskConf, _ := conf.GetAllTaskConfig(taskId)
	if !CheckConfData(taskConf, taskId, err) {
		return
	}

	// 等级上限检查
	if curLevel > taskConf.MaxLv {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "task's level has overflow"
		return
	}

	// 完成条件判定
	if !CheckTaskCondForComplete(r, taskId, curLevel) {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "task complete cond unsatisfied"
		return
	}

	//= 发奖励
	objId, ok := taskConf.RewardList[curLevel]
	if !ok {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "data err, task has no awards?"
		return
	}

	objConfData, _ := conf.GetObjList(objId)
	if !CheckConfData(objConfData, objId, err) {
		return
	}

	rewards, _ = JsonToPb_StaticObjs(objConfData.ObjList, objConfData, objConfData.Id, err)

	if err.Id != 0 {
		return
	}

	pointsAward, _ := JsonToPb_StaticObjs(taskConf.PointReward, taskConf, taskConf.Id, err)
	if err.Id != 0 {
		return
	}

	rewards = append(rewards, pointsAward...)

	// 更新任务等级
	r.Tasks.TaskLevels[taskId]++

	return
}

func DoTaskAward(r *msg.PlayerInfo, taskId int32, err *msg.ErrorInfo) (addedAwards []*msg.ObjInfo) {
	rewards := PrepareDoTaskAward(r, taskId, err)
	if err.Id != 0 {
		return
	}

	// 单独的一个活跃点奖励
	addedAwards, _ = AddStaticObjs(r, rewards, err)
	if err.Id != 0 {
		return
	}

	return
}
