package game

import (
	"bitbucket.org/funplus/sandwich/current"
	"fmt"
	"math/rand"
	"server/internal/clan"
	"server/internal/log"
	"server/internal/share"
	"server/internal/util/time3"
	"server/pkg/fungin/xmiddleware"
	"server/pkg/gen/conf"
	"server/pkg/gen/msg"
	"server/pkg/gen_db/dao"
	"sort"
	"time"
)

// 直接查找数据库，谨慎使用
// 0 not exist
func GetPlayerIdByName(name string) (id uint64) {
	mgr := dao.PlayerInfo
	tableName := mgr.TableName()
	sqlStr := fmt.Sprintf("select id from %s where name='%s' limit 10",
		tableName,
		name)
	tmp := &msg.PlayerInfo{}
	err := mgr.GetBySql(tmp, sqlStr, nil)
	if err == nil {
		id = tmp.Id
	}
	return id
}

// 直接查找数据库，谨慎使用
// 0 not exist
func GetPlayerIdByNameReal(nameReal string) (id uint64) {
	mgr := dao.PlayerInfo
	tableName := mgr.TableName()
	sqlStr := fmt.Sprintf("select id from %s where name_real='%s' limit 10",
		tableName,
		nameReal)
	tmp := &msg.PlayerInfo{}
	err := mgr.GetBySql(tmp, sqlStr, nil)
	if err == nil {
		id = tmp.Id
	}
	return id
}

// 直接查找数据库，谨慎使用
// 0 not exist
func GetPlayerIdByFakeUserId(fakeUserId int32) (id uint64) {
	mgr := dao.PlayerInfo
	tableName := mgr.TableName()
	sqlStr := fmt.Sprintf("select id from %s where fake_user_id='%d' limit 10",
		tableName,
		fakeUserId)
	tmp := &msg.PlayerInfo{}
	err := mgr.GetBySql(tmp, sqlStr, nil)
	if err == nil {
		id = tmp.Id
	}
	return id
}

// toleranceSecond 容错时间，当客户端触发的时候，使用容错
func CheckDailyReset(r *msg.PlayerInfo, curTime int64, toleranceSecond int64) bool {
	if toleranceSecond > 30 {
		panic("Tolerance is too much")
	}

	if r.DailyResetTime < curTime+toleranceSecond {
		return true
	}
	return false
}

// 会伴随一些sync信息，，需要区分登录和非登录
func DoDailyReset(r *msg.PlayerInfo, withSync bool) {
	// 当前时间向后错位，避免客户端容错toleranceSecond带来的提前
	t := time3.EndOfDay(time3.Now().Add(time.Second * 30))
	old := r.DailyResetTime

	// 加随机扰动 设计里在日重置和周重置里不能设计大家抢的东西
	r.DailyResetTime = t.Unix() + int64(rand.Int31n(30))

	log.Debugf("u%d DoDailyReset next time: %s [%d->%d]", r.Id, t.String(), old, r.DailyResetTime)

	// reset loop task
	resetLoopTasks(r, msg.TaskType_TT_Daily, withSync)

	// reset bounty
	resetBountyDaily(r, withSync)

	resetOnHookFreeTimes(r, withSync)

	// 公会相关数据
	r.Clan = clan.MustGetPlayerInfoClan(r)
	clan.ResetClanInfo(r.Clan)
	resetClanTreasure(r, msg.TreasureType_TRT_ClanDaily, withSync)
	// 商店的重置，有客户端发起，不走每日逻辑

	RefreshActivityOnLogin(r)

	resetDailyRedpoint(r)

	// 邮件比较特殊，因为别的事可能导致了新邮件
	RefreshMailRedPoint(r, false)

	if withSync {
		notifyAllRedPoints(r)
	}
}

// toleranceSecond 容错时间，当客户端触发的时候，使用容错
func CheckWeeklyReset(r *msg.PlayerInfo, curTime int64, toleranceSecond int64) bool {
	if toleranceSecond > 30 {
		panic("Tolerance is too much")
	}

	if r.WeeklyResetTime < curTime+toleranceSecond {
		return true
	}
	return false
}

// 会伴随一些sync信息，，需要区分登录和非登录
func DoWeeklyReset(r *msg.PlayerInfo, withSync bool) {
	t := time3.EndOfWeek(time3.Now().Add(time.Second * 30))
	old := r.WeeklyResetTime

	// 加随机扰动 设计里在日重置和周重置里不能设计大家抢的东西
	r.WeeklyResetTime = t.Unix() + int64(rand.Int31n(30))

	log.Debugf("u%d DoWeeklyReset next time: %s [%d->%d]", r.Id, t.String(), old, r.WeeklyResetTime)

	resetLoopTasks(r, msg.TaskType_TT_Weekly, withSync)
	//TODO
}

func resetClanTreasure(r *msg.PlayerInfo, treasureType msg.TreasureType, withSync bool) {
	resetTreasureRecvedData(r, treasureType)
	if withSync {
		NotifyTreasureRecvedData(r, treasureType)
	}
}

func getLevelByProsperity(prosperity int32) int32 {
	maxLevel := int32(len(conf.GetProsperityLevelConfigConf().ProsperityLevelConfigs))
	retLevel := int32(1)
	for retLevel <= maxLevel {
		c, _ := conf.GetProsperityLevelConfig(retLevel)
		if c == nil {
			log.Errorf("not found level config%d", retLevel)
			break
		}
		if prosperity < c.ProsperityNextTotal {
			break
		}

		retLevel++
	}

	if retLevel > maxLevel {
		retLevel = maxLevel
	}

	return retLevel
}

func RefreshPlayerLevel(r *msg.PlayerInfo) {
	// 声望记录的是总值
	// 此处根据声望，获得一个最新等级
	// 如果等级发生变化了，推送前端，，并尝试解锁等级相关的新功能
	newLevel := getLevelByProsperity(int32(GetCount_Currency(r, msg.CurrencyId_CI_Prosperity)))
	oldLevel := int32(GetCount_Currency(r, msg.CurrencyId_CI_PlayerLevel))
	if newLevel > oldLevel {
		err := &msg.ErrorInfo{}
		addStaticObj_Currency(r, &msg.StaticObjInfo{
			Id:    int32(msg.CurrencyId_CI_PlayerLevel),
			Type:  int32(msg.ObjType_OT_Currency),
			Count: int64(newLevel - oldLevel),
		}, err)

		// sync level
		ctx := xmiddleware.GetCtxByUid(r.Id)
		if ctx != nil && newLevel > 1 {
			n := &msg.LevelSyncNotify{
				NewLevel: newLevel,
			}
			current.MustAppendAdditionalOutgoingMessage(ctx, n)
		}

		// 等级发生变化的时候，尝试触发和等级相关的内容
		RefreshRegisterActivity(r, true)
	}
}

// 刷新公会红点
func RefreshClanRedPoint(r *msg.PlayerInfo, notify bool, clanInfo *msg.ClanInfo, redPointType msg.RedPointType) {
	// 只有0和1区别！
	curCount := int32(0)
	for {
		if clanInfo == nil {
			rs := share.GetClanId(r.Id)
			if rs == nil {
				break
			}
			clanInfo = clan.Find(rs.ClanId)
		}

		if clanInfo == nil {
			break
		}

		clanInfoExt := clan.MustGetClanInfoExt(clanInfo)
		if clanInfoExt == nil {
			break
		}
		if redPointType == msg.RedPointType_RPT_ClanApply {
			applyCount := len(clanInfoExt.ApplyList)
			if applyCount != 0 {
				curCount = int32(applyCount)
			}
		} else if redPointType == msg.RedPointType_RPT_ClanReward {
			MakeSureTreasureRecvedData(r)
			err := &msg.ErrorInfo{}
			treasureKeyConfMap := GetTreasureKeyConfMap(msg.TreasureType_TRT_ClanDaily, err)
			if err.Id != 0 {
				break
			}

			var sValue []int
			for key, _ := range clanInfo.ActiveValue {
				sValue = append(sValue, int(key))
			}

			var conditionOk []int32
			for _, v := range treasureKeyConfMap {
				err2 := &msg.ErrorInfo{}
				confData, _ := conf.GetTreasureConfig(v)
				if !CheckConfData(confData, v, err2) {
					continue
				}
				fakeConsumes, _ := JsonToPb_StaticObjs(confData.NeedConds, confData, v, err2)
				if err2.Id != 0 {
					continue
				}
				for _, v2 := range fakeConsumes {
					if len(sValue) == 0 {
						continue
					}
					sort.Ints(sValue)
					key := sValue[len(sValue)-1]
					todayActive := clanInfo.ActiveValue[int64(key)]
					if int64(todayActive) >= v2.Count {
						conditionOk = append(conditionOk, v)
					}
				}
			}

			key := int32(msg.TreasureType_TRT_ClanDaily)
			if len(conditionOk) > len(r.TreasureRecvedData[key].Data) {
				curCount = 1
			} else {
				curCount = 0
			}
		}
		break
	}

	if clanInfo != nil {
		if clanInfo.VicePresidentId != r.Id &&
			clanInfo.PresidentId != r.Id &&
			redPointType == msg.RedPointType_RPT_ClanApply {
			return
		}
	}

	_, rp := FindRedPointAnyway(r, &msg.RedPoint{
		Type: redPointType,
	})

	if rp.Count != curCount {
		rp.Count = curCount

		if notify {
			NotifyRedPoint(r, rp)
		}
	}
	return
}
