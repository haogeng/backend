package game

import (
	"fmt"
	"gotest.tools/assert"
	"reflect"
	"testing"
)

func TestJsonToPb_StaticObjs(t *testing.T) {
	j := `{
	"0": {
		"type": 1,
		"id": 1,
		"count": 2000,
		"param": 0
	},
	"1": {
		"type": 1,
		"id": 6,
		"count": 2000,
		"param": 0
	}
	}`
	data, ok := JsonToPb_StaticObjs(j, nil, 0, nil)
	fmt.Printf("%+v\n", data)
	assert.Equal(t, ok, true, "")
}

func TestNilInterface(t *testing.T) {
	var a map[int32]int32
	a = nil

	if a == nil {
		fmt.Printf("a is nil")

		// Equal must be failed cause a will be changed to interface{}
		//assert.Equal(t, a, nil, "a should be nil")
	}

	var i interface{}
	i = a
	// i is always not nil!!!
	if i == nil {
		fmt.Printf("i is nil")
	} else {
		if reflect.ValueOf(i).Kind() == reflect.Ptr && reflect.ValueOf(i).IsNil() {
			fmt.Printf("the pointer stored in interface{} is nil")
		} else {
			fmt.Printf("the pointer stored in interface{} is not nil")
		}
	}
}
