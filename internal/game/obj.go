package game

import (
	"bitbucket.org/funplus/golib/deepcopy"
	"bitbucket.org/funplus/sandwich/current"
	"context"
	"fmt"
	"server/internal/gconst"
	"server/internal/log"
	"server/pkg/gen/conf"
	"server/pkg/gen/msg"
)

//！！！ 约定： 只有在协议处理函数中才会调用r.MustUpdate去存档
// 增加一个静态物体
func addStaticObj_Currency(r *msg.PlayerInfo, d *msg.StaticObjInfo, err *msg.ErrorInfo) (ret []*msg.ObjInfo, result bool) {
	ret, result = nil, false

	currencyId := msg.CurrencyId(d.Id)
	confData, _ := conf.GetCurrencyConfig(d.Id)
	if !CheckConfData(confData, d.Id, err) {
		return
	}

	// 实际加的数量
	deltaNum := int32(d.Count)

	c, _ := r.Currency[d.Id]
	c += d.Count
	r.Currency[d.Id] = c

	// 支持泛型类的货币->任务刷新
	// 累积类
	RefreshTaskNew(r, msg.TaskCondType_TCT_CurrencyNums, int32(currencyId))
	RefreshTaskCount(r, msg.TaskCondType_TCT_CurrencyNums, int32(currencyId), 0, c)

	// 增量类
	if d.Count > 0 {
		RefreshTaskCount(r, msg.TaskCondType_TCT_CurrencyAdded, int32(currencyId), d.Count, 0)
	}

	switch currencyId {
	// 繁荣度 14
	case msg.CurrencyId_CI_Prosperity:
		// 历史遗留问题
		RefreshTaskNew(r, msg.TaskCondType_TCT_Prosperity, 0)
		RefreshTaskCount(r, msg.TaskCondType_TCT_Prosperity, 0, 0, c)

		RefreshPlayerLevel(r)
		break

	case msg.CurrencyId_CI_Gold:
		{
			r.DupGold = c
		}
	case msg.CurrencyId_CI_Diamond:
		{
			r.DupDiamond = c
		}
	case msg.CurrencyId_CI_PlayerLevel:
		{
			r.DupLevel = int32(c)
		}
	}

	if deltaNum > 0 {
		deltaObj := &msg.ObjInfo{
			Id:       d.Id,
			Type:     msg.ObjType(d.Type),
			ConfigId: d.Id,
			Num:      int64(deltaNum),
		}
		ret = append(ret, deltaObj)
		result = true
	}

	return
}

func addStaticObj_Item(r *msg.PlayerInfo, d *msg.StaticObjInfo, err *msg.ErrorInfo) (ret []*msg.ObjInfo, result bool) {
	ret, result = nil, false

	confData, _ := conf.GetItemConfig(d.Id)
	if !CheckConfData(confData, d.Id, err) {
		return
	}

	// 确保存在
	item, ok := r.Items[d.Id]
	if !ok {
		item = &msg.ObjInfo{
			Id:       d.Id,
			Type:     msg.ObjType(d.Type),
			ConfigId: d.Id,
			Num:      0,
		}

		if len(r.Items) >= gconst.HardMaxLimitOfItems {
			err.Id, err.DebugMsg = msg.Ecode_ITEM_FULL, "upto hard limit of items"
			log.Errorf("u%d upto hard limit of items", r.Id)
			return
		}

		AddObjIntoMap(&r.Items, item)
	}
	oldNum := item.Num
	item.Num += int64(d.Count)
	// 堆叠上限处理
	if item.Num > int64(confData.TopLimit) {
		item.Num = int64(confData.TopLimit)
	}
	deltaNum := item.Num - oldNum

	deltaObj := &msg.ObjInfo{
		Id:       d.Id,
		Type:     msg.ObjType(d.Type),
		ConfigId: d.Id,
		Num:      deltaNum,
	}
	ret = append(ret, deltaObj)
	result = true

	return
}

func addStaticObj_Troop(r *msg.PlayerInfo, d *msg.StaticObjInfo, err *msg.ErrorInfo) (ret []*msg.ObjInfo, result bool) {
	ret, result = nil, false
	if d.Count == 0 {
		return
	}

	// 递归加多个
	if d.Count > 1 {
		for i := int32(0); i < int32(d.Count); i++ {
			d_one := deepcopy.Copy(d).(*msg.StaticObjInfo)
			d_one.Count = 1
			tempRet, tempOk := addStaticObj_Troop(r, d_one, err)
			result = tempOk
			if tempOk {
				ret = append(ret, tempRet...)
			} else {
				break
			}
		}
		return
	}

	if d.Count != 1 {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "d.count must be 1"
		return
	}

	// add 1
	dynamicId := DispatchObjIdUnsafe(r, msg.ObjType_OT_Troop)

	// check data
	if d.Level <= 0 ||
		d.Star <= 0 ||
		d.Type != int32(msg.ObjType_OT_Troop) {
		log.Errorf("TroopConfData err: %+v", d)

		err.Id, err.DebugMsg = msg.Ecode_INVALID, "static data err"
		debugInfo := fmt.Sprintf("TroopConf err:%+v", d)
		err.DebugMsg += debugInfo
		return
	}

	realObj := &msg.ObjInfo{
		Id:       dynamicId,
		Type:     msg.ObjType(d.Type),
		ConfigId: d.Id,
		Num:      1,
		Level:    d.Level,
		Star:     d.Star,
	}

	if _, ok := ExtTroop(r).Troops[dynamicId]; ok {
		log.Errorf("u%d Repeated dynamicId\n", r.Id)
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "repeated dynamic id"
		return
	}

	if len(ExtTroop(r).Troops) >= gconst.HardMaxLimitOfTroops {
		err.Id, err.DebugMsg = msg.Ecode_HERO_FULL, "upto hard limit of heroes"
		log.Errorf("u%d upto hard limit of heroes", r.Id)
		return
	}
	initTroopSpecialty(r, realObj)
	AddObjIntoMap(&ExtTroop(r).Troops, realObj)

	// 刷新战力
	RefreshTroopPower(r, realObj, true)
	// 涉及英雄删除的地方，鲁棒的更新所有队伍战力
	RefreshTotalPower(r)

	//WHY ?? // 202007首次测试临时方案：图鉴 临时处理： 只有当前星级>=4的才进图鉴
	//if realObj.Star >= 4 {
	TryAddIntoTroopBook(r, realObj.ConfigId)

	// 统计数++
	r.GotTroopTotalNums++
	RefreshTaskCount(r, msg.TaskCondType_TCT_GotTroopOwnNums, 0, 0, int64(r.GotTroopTotalNums))

	deltaObj := deepcopy.Copy(realObj).(*msg.ObjInfo)
	ret = append(ret, deltaObj)
	result = true

	return
}

func addStaticObj_Equip(r *msg.PlayerInfo, d *msg.StaticObjInfo, err *msg.ErrorInfo) (ret []*msg.ObjInfo, result bool) {
	ret, result = nil, false
	if d.Count == 0 {
		return
	}

	// 递归加多个
	if d.Count > 1 {
		for i := int32(0); i < int32(d.Count); i++ {
			d_one := deepcopy.Copy(d).(*msg.StaticObjInfo)
			d_one.Count = 1
			tempRet, tempOk := addStaticObj_Equip(r, d_one, err)
			result = tempOk
			if tempOk {
				ret = append(ret, tempRet...)
			} else {
				break
			}
		}
		return
	}

	if d.Count != 1 {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "d.count must be 1"
		return
	}

	// add 1
	realObj := genEquip(r, d.Id, err)
	if err.Id != 0 {
		return
	}
	if realObj == nil {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "realObj is nil"
		return
	}

	if _, ok := ExtEquip(r).Equips[realObj.Id]; ok {
		log.Errorf("u%d Repeated dynamicId\n", r.Id)
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "repeated dynamic id"
		return
	}

	if len(ExtEquip(r).Equips) >= gconst.HardMaxLimitOfEquips {
		err.Id, err.DebugMsg = msg.Ecode_EQUIP_FULL, "upto hard limit of equips"
		log.Errorf("u%d upto hard limit of equips", r.Id)
		return
	}
	AddObjIntoMap(&ExtEquip(r).Equips, realObj)

	deltaObj := deepcopy.Copy(realObj).(*msg.ObjInfo)
	ret = append(ret, deltaObj)
	result = true

	return
}
func addStaticObj_BountyTask(r *msg.PlayerInfo, d *msg.StaticObjInfo, err *msg.ErrorInfo) (ret []*msg.ObjInfo, result bool) {
	addBountyTask(r, d.Id)
	return nil, true
}

func addStaticObj_MazeRelic(r *msg.PlayerInfo, d *msg.StaticObjInfo, err *msg.ErrorInfo) (ret []*msg.ObjInfo, result bool) {
	relicObj := &msg.ObjInfo{
		Id:       DispatchObjIdUnsafe(r, msg.ObjType_OT_MazeRelic),
		Type:     msg.ObjType(d.Type),
		ConfigId: d.Id,
		Num:      1,
	}
	maze := MustGetPlayerInfoMaze(r)
	maze.RelicObjs = append(maze.RelicObjs, relicObj)
	ret = append(ret, relicObj)
	result = true
	return
}

func addStaticObj_Artifact(r *msg.PlayerInfo, d *msg.StaticObjInfo, err *msg.ErrorInfo) (ret []*msg.ObjInfo, result bool) {
	artifactObj := &msg.ObjInfo{
		Id:       DispatchObjIdUnsafe(r, msg.ObjType_OT_Artifact),
		Type:     msg.ObjType(d.Type),
		ConfigId: d.Id,
		Num:      1,
	}
	maze := MustGetPlayerInfoMaze(r)
	maze.ArtifactObjs = append(maze.ArtifactObjs, artifactObj)
	ret = append(ret, artifactObj)
	result = true
	return
}

func addStaticObj_Drop(r *msg.PlayerInfo, d *msg.StaticObjInfo, err *msg.ErrorInfo) (ret []*msg.ObjInfo, result bool) {
	if d.Count == 0 {
		return
	}

	ret = make([]*msg.ObjInfo, 0)
	result = true

	if d.Count > 1 {
		for i := 0; i < int(d.Count); i++ {
			d_one := deepcopy.Copy(d).(*msg.StaticObjInfo)
			d_one.Count = 1
			tempRet, tempResult := addStaticObj_Drop(r, d_one, err)
			result = tempResult
			if tempResult {
				ret = append(ret, tempRet...)
			} else {
				break
			}
		}
		return
	}

	if d.Count != 1 {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "d.count must be 1"
		return
	}

	// add 1
	dropAwards, _ := Drop(r, d.Id, err)
	ret = append(ret, dropAwards...)
	result = true
	return
}

// 返回增量值，如果是动态也是增量
func AddStaticObj(r *msg.PlayerInfo, d *msg.StaticObjInfo, err *msg.ErrorInfo) (ret []*msg.ObjInfo, result bool) {
	ret, result = nil, false
	if d.Count <= 0 {
		return
	}

	t := msg.ObjType(d.Type)
	switch t {
	case msg.ObjType_OT_Currency:
		return addStaticObj_Currency(r, d, err)

	case msg.ObjType_OT_Item:
		return addStaticObj_Item(r, d, err)

	case msg.ObjType_OT_Troop:
		return addStaticObj_Troop(r, d, err)

	case msg.ObjType_OT_Equipment:
		return addStaticObj_Equip(r, d, err)

	case msg.ObjType_OT_Drop:
		return addStaticObj_Drop(r, d, err)

	case msg.ObjType_OT_BountyTask:
		return addStaticObj_BountyTask(r, d, err)

	case msg.ObjType_OT_Goods:
		{
			//TODO
		}
		break
	case msg.ObjType_OT_MazeRelic:
		return addStaticObj_MazeRelic(r, d, err)
	case msg.ObjType_OT_Artifact:
		return addStaticObj_Artifact(r, d, err)
	default:
		log.Errorf("not recognize t: %d", t)
		break
	}
	return
}

func AddStaticObjs(r *msg.PlayerInfo, d []*msg.StaticObjInfo, err *msg.ErrorInfo) (ret []*msg.ObjInfo, result bool) {
	for _, v := range d {
		if v.Count == 0 {
			// 因为英雄重置等，，有可能有0的情况
			log.Infof("r%d want to add %+v, but its count is 0", r.Id, v)
			//err.Id, err.DebugMsg = msg.Ecode_INVALID, fmt.Sprintf("data err: %+v", v)
			//break
			continue
		}
		tempRet, tempResult := AddStaticObj(r, v, err)
		result = tempResult
		if tempResult {
			ret = append(ret, tempRet...)
		} else {
			break
		}
	}
	return
}

func AddStaticObjByObjConf(r *msg.PlayerInfo, objConfId int32, err *msg.ErrorInfo) (ret []*msg.ObjInfo, result bool) {
	// do awards
	objConfData, _ := conf.GetObjList(objConfId)
	if !CheckConfData(objConfData, objConfId, err) {
		return
	}

	units, _ := JsonToPb_StaticObjs(objConfData.ObjList, objConfData, objConfData.Id, err)
	if err.Id != 0 {
		return
	}

	return AddStaticObjs(r, units, err)
}

func DelItem(r *msg.PlayerInfo, id int32) {
	delete(r.Items, id)
}

func GetCount_Currency(r *msg.PlayerInfo, id msg.CurrencyId) (count int64) {
	count, _ = r.Currency[int32(id)]
	return count
}

// 类似货币等伪造
// 类似于item hero等，取引用值
// 返回的值，应该在一个作用于尽快使用，否则有风险
// 返回的对象，总是不为nil！！！ 如果没有则返回dummy对象，数量为0
func GetObjInfoAnyway(r *msg.PlayerInfo, t msg.ObjType, id int32) *msg.ObjInfo {
	destRet := &msg.ObjInfo{
		Id:       id,
		Type:     t,
		ConfigId: id, // 有的情况下是不对的，但是允许，比如删除的时候
		Num:      0,
	}

	var tempRet *msg.ObjInfo

	switch t {
	case msg.ObjType_OT_Currency:
		c := GetCount_Currency(r, msg.CurrencyId(id))
		tempRet = &msg.ObjInfo{
			Id:       id,
			Type:     t,
			ConfigId: id,
			Num:      c,
		}
		break

	case msg.ObjType_OT_Item:
		tempRet, _ = r.Items[id]
		break

	case msg.ObjType_OT_Equipment:
		tempRet, _ = ExtEquip(r).Equips[id]
		break

	case msg.ObjType_OT_Troop:
		tempRet, _ = ExtTroop(r).Troops[id]
		break
	case msg.ObjType_OT_MazeRelic:
		maze := MustGetPlayerInfoMaze(r)
		for i := 0; i < len(maze.RelicObjs); i++ {
			if maze.RelicObjs[i].Id == id {
				tempRet = maze.RelicObjs[i]
				break
			}
		}
		break
	case msg.ObjType_OT_Artifact:
		maze := MustGetPlayerInfoMaze(r)
		for i := 0; i < len(maze.ArtifactObjs); i++ {
			if maze.ArtifactObjs[i].Id == id {
				tempRet = maze.ArtifactObjs[i]
				break
			}
		}
		break
	default:
		log.Errorf("Shouldn't go here in GetObjInfoAnyway t:%v", t)
		tempRet = nil
		break
	}

	if tempRet != nil {
		destRet = tempRet
	}
	return destRet
}

func makeFinalObjs(r *msg.PlayerInfo, changeKeys []*msg.ObjKey) []*msg.ObjInfo {
	mergedKeys := MergeObjKey(changeKeys)

	if len(mergedKeys) == 0 {
		return nil
	}

	finalObjs := make([]*msg.ObjInfo, 0)

	for _, v := range mergedKeys {
		finalObj := GetObjInfoAnyway(r, v.Type, v.Id)
		if finalObj != nil {
			finalObjs = append(finalObjs, finalObj)
		}
	}

	return finalObjs
}

func MakeChangeKeysFromStaticObj(m []*msg.StaticObjInfo) []*msg.ObjKey {
	ret := make([]*msg.ObjKey, 0)
	for i := 0; i < len(m); i++ {
		if m[i] == nil {
			continue
		}
		ret = append(ret, &msg.ObjKey{
			Id:   m[i].Id,
			Type: msg.ObjType(m[i].Type)},
		)
	}
	return ret
}

func MakeChangeKeysFromTroopIds(ids []int32) []*msg.ObjKey {
	changedKeys := make([]*msg.ObjKey, 0)
	for i := 0; i < len(ids); i++ {
		changedKeys = append(changedKeys, &msg.ObjKey{
			Id:   ids[i],
			Type: msg.ObjType_OT_Troop,
		})
	}
	return changedKeys
}

func MakeChangeKeysFromEquipIds(ids []int32) []*msg.ObjKey {
	changedKeys := make([]*msg.ObjKey, 0)
	for i := 0; i < len(ids); i++ {
		changedKeys = append(changedKeys, &msg.ObjKey{
			Id:   ids[i],
			Type: msg.ObjType_OT_Equipment,
		})
	}
	return changedKeys
}

func MakeChangeKeysFromObj(m []*msg.ObjInfo) []*msg.ObjKey {
	ret := make([]*msg.ObjKey, 0)
	for i := 0; i < len(m); i++ {
		ret = append(ret, &msg.ObjKey{
			Id:   m[i].Id,
			Type: msg.ObjType(m[i].Type)},
		)
	}
	return ret
}

func TryAppendSyncObjsMsgWithConsume(ctx context.Context, r *msg.PlayerInfo, addedObjs []*msg.ObjInfo, consume *msg.StaticObjInfo) {
	var consumes []*msg.StaticObjInfo
	if consume != nil {
		consumes = []*msg.StaticObjInfo{consume}
	}

	TryAppendSyncObjsMsgWithConsumes(ctx, r, addedObjs, consumes)
}

func TryAppendSyncObjsMsgWithConsumes(ctx context.Context, r *msg.PlayerInfo, addedObjs []*msg.ObjInfo, consumes []*msg.StaticObjInfo) {
	if len(addedObjs) == 0 && len(consumes) == 0 {
		return
	}

	changedIds := MakeChangeKeysFromObj(addedObjs)
	if len(consumes) > 0 {
		changedIds = append(changedIds, MakeChangeKeysFromStaticObj(consumes)...)
	}

	TryAppendSyncObjsMsgWithChangedIds(ctx, r, addedObjs, changedIds)
}

// 会在一个协议中大量使用引用值，谨慎使用
// 一般应该放在一个协议处理的最后
func TryAppendSyncObjsMsgWithChangedIdsAndNext(ctx context.Context, r *msg.PlayerInfo, addedObjs []*msg.ObjInfo, nextAddedObjs []*msg.ObjInfo, changedKeys []*msg.ObjKey) {
	if len(addedObjs) == 0 && len(changedKeys) == 0 {
		return
	}

	finalObjs := makeFinalObjs(r, changedKeys)

	n := &msg.ObjsSyncNotify{
		DeltaObjs:     addedObjs,
		NextDeltaObjs: nextAddedObjs,
		FinalObjs:     finalObjs,
	}

	ctxNewTroopConfigIds, ok := GetCtxValue(r, KeyCtxNewTroopConfigIds, map[int32]bool{}).(map[int32]bool)
	if ok {
		if len(ctxNewTroopConfigIds) > 0 {
			for k, _ := range ctxNewTroopConfigIds {
				n.NewTroops = append(n.NewTroops, k)
			}
		}
	}

	current.MustAppendAdditionalOutgoingMessage(ctx, n)
}

// 会在一个协议中大量使用引用值，谨慎使用
// 一般应该放在一个协议处理的最后
func TryAppendSyncObjsMsgWithChangedIds(ctx context.Context, r *msg.PlayerInfo, addedObjs []*msg.ObjInfo, changedKeys []*msg.ObjKey) {
	if len(addedObjs) == 0 && len(changedKeys) == 0 {
		return
	}

	finalObjs := makeFinalObjs(r, changedKeys)

	n := &msg.ObjsSyncNotify{
		DeltaObjs: addedObjs,
		FinalObjs: finalObjs,
	}

	ctxNewTroopConfigIds, ok := GetCtxValue(r, KeyCtxNewTroopConfigIds, map[int32]bool{}).(map[int32]bool)
	if ok {
		if len(ctxNewTroopConfigIds) > 0 {
			for k, _ := range ctxNewTroopConfigIds {
				n.NewTroops = append(n.NewTroops, k)
			}
		}
	}

	current.MustAppendAdditionalOutgoingMessage(ctx, n)
}

// 增加动态物体 根据堆叠属性简单的增加而已
//func AddDynamicObj(r *msg.PlayerInfo, obj *msg.ObjInfo, err *msg.ErrorInfo) (ret *[]msg.ObjInfo, result bool) {
//	//TODO...
//	return
//}

//func AddDynamicObjs(r *msg.PlayerInfo, obj *[]msg.ObjInfo, err *msg.ErrorInfo) (ret *[]msg.ObjInfo, result bool) {
//	//TODO...
//	return
//}
