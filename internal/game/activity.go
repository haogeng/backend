package game

import (
	"bitbucket.org/funplus/sandwich/current"
	"fmt"
	"server/internal/util/time3"
	"server/pkg/fungin/xmiddleware"
	"time"

	//jsoniter "github.com/json-iterator/go"
	"server/internal/game/activity"
	"server/internal/gconst"
	"server/internal/log"
	"server/pkg/gen/msg"
	"server/pkg/gen_redis/redisdb"
)

func MustGetPlayerInfoActivity(r *msg.PlayerInfo) *msg.PlayerInfoActivity {
	if r.Activity != nil {
		return r.Activity
	}

	var c redisdb.PlayerInfoActivityCache
	r2 := c.Find(r.Id)
	log.Debugf("PlayerInfoActivity %+v", r2)

	if r2 == nil {
		r2 = &msg.PlayerInfoActivity{}
		id := c.MustCreate(r.Id, r2)
		if id != r.Id {
			panic(fmt.Errorf("u%d GetPlayerInfoActivity fail", r2.Id))
		}
	}
	makeSureActivityExist(r2)
	r.Activity = r2

	return r2
}

func makeSureActivityExist(activity *msg.PlayerInfoActivity) {
	if activity.Data == nil {
		activity.Data = make(map[int32]*msg.ActivityData)
	}
}

func RefreshActivityOnLogin(r *msg.PlayerInfo) {
	RefreshRegisterActivity(r, false)
	TryCloseActivities(r)
	TryUpdateActivities(r)
}

// 根据情况重构
// 判断有无调度活动变化，新增或关闭
// 双向扫描 配表->自身
func RefreshRegisterActivity(r *msg.PlayerInfo, withSync bool) {
	act := MustGetPlayerInfoActivity(r)

	addList := make([]int32, 0)
	//changeList := make([]int32, 0)
	// 尝试添加活动
	{
		units := activity.GetMgr().GetUnitsByTimeType(gconst.REGISTER)
		for _, au := range units {
			if tryAddActivity(r, act, au) {
				addList = append(addList, au.Id)
			}
		}
	}

	// 删除强制关闭的活动 isClosed
	delActivityCauseConfClose(r, act)

	if withSync {
		NotifyActivity(r, addList)
	}

	log.Debugf("RefreshRegisterActivity(%d) :%+v", r.Id, act)
}

// 根据配表的关闭标记删除对应活动
func delActivityCauseConfClose(r *msg.PlayerInfo, act *msg.PlayerInfoActivity) {
	for id, _ := range act.Data {
		au := activity.GetMgr().GetUnit(id)
		if au == nil || !au.Config.IsOpen {
			// 彻底删除
			delete(act.Data, id)
			log.Infof("delete activity(%d) cause conf close or not exist", id)
		}
	}
}

func NotifyActivity(r *msg.PlayerInfo, idList []int32) {
	act := MustGetPlayerInfoActivity(r)
	n := &msg.ActivityNotify{
		Datas: make(map[int32]*msg.ActivityData),
	}
	for _, id := range idList {
		if _, exist := act.Data[id]; exist {
			n.Datas[id] = act.Data[id]
		}
	}
	if len(n.Datas) > 0 {
		ctx := xmiddleware.GetCtxByUid(r.Id)
		if ctx != nil {
			current.MustAppendAdditionalOutgoingMessage(ctx, n)
		}
	}
}

func tryAddActivity(r *msg.PlayerInfo, act *msg.PlayerInfoActivity, au *activity.Unit) bool {
	if _, exist := act.Data[au.Id]; !exist {
		// 条件不满足，就不加
		if judgeActivityOpenCond(r, au) {
			doAddActivity(r, act, au)
			return true
		}
	}

	return false
}

func doAddActivity(r *msg.PlayerInfo, act *msg.PlayerInfoActivity, au *activity.Unit) {
	if _, exist := act.Data[au.Id]; exist {
		log.Errorf("repeat doAddActivity %v", au.Id)
		// 允许覆盖.. 循环活动???
	}

	// 暂时不支持预告类的活动，，加入即打开

	d := &msg.ActivityData{
		Id:                 au.Id,
		Status:             msg.ActivityStatus_AS_Opened,
		OpenedTimestamp:    time.Now().Unix(),
		CloseTimestamp:     0,
		NextEventTimestamp: 0,
		Sign:               nil,
		Quest:              nil,
	}
	act.Data[au.Id] = d

	// init time
	initActivityTime(r, au, d)

	// init detail
	initActivityDetail(r, d)

	// 根据新激活的活动，刷新任务
	RefreshTaskNew(r, msg.TaskCondType_TCT_OwnActivity, au.Id)
}

func initActivityTime(r *msg.PlayerInfo, au *activity.Unit, d *msg.ActivityData) {
	switch au.Config.TimeType {
	case gconst.REGISTER:
		{
			if au.TimeInfo.DurationDay > 0 {
				d.CloseTimestamp = time3.Now().Unix() + int64(au.TimeInfo.DurationDay)*gconst.DaySeconds
			}
		}
	}
}

func initActivityDetail(r *msg.PlayerInfo, d *msg.ActivityData) {
	ActivityOpt(d).InitDetail(r)
}

// 判断活动激活条件
func judgeActivityOpenCond(r *msg.PlayerInfo, au *activity.Unit) bool {
	if !au.Config.IsOpen {
		return false
	}

	// 通用条件
	if int32(GetCount_Currency(r, msg.CurrencyId_CI_PlayerLevel)) < au.Config.MinLvCondition {
		return false
	}

	// 时间信息
	now := time3.Now().Unix()
	switch au.Config.TimeType {
	case gconst.REGISTER:
		{
			if r.RegisterTime+int64(au.TimeInfo.DelayDay)*gconst.DaySeconds > now {
				return false
			}
		}
	}

	return true
}

// 活动是否开启中
func GetActivity(r *msg.PlayerInfo, activityId int32, err *msg.ErrorInfo) *msg.ActivityData {
	act := MustGetPlayerInfoActivity(r)
	if d, ok := act.Data[activityId]; ok {
		return d
	}

	err.Id, err.DebugMsg = msg.Ecode_INVALID, "activity not found"
	return nil
}

func IsActivityOpen(r *msg.PlayerInfo, d *msg.ActivityData, err *msg.ErrorInfo) bool {
	//act := MustGetPlayerInfoActivity(r)
	if d.Status != msg.ActivityStatus_AS_Opened {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "activity not open"
		return false
	}

	return true
}

// 可能伴随补发邮件奖励
// 登录时调用
func TryCloseActivities(r *msg.PlayerInfo) {
	act := MustGetPlayerInfoActivity(r)
	for _, v := range act.Data {
		if v.Status == msg.ActivityStatus_AS_Closed {
			ActivityOpt(v).TryClearWhenClosed(r)
			continue
		}
		TryCloseActivity(r, v)
	}
}
func TryUpdateActivities(r *msg.PlayerInfo) {
	act := MustGetPlayerInfoActivity(r)
	for _, v := range act.Data {
		if v.Status == msg.ActivityStatus_AS_Closed {
			continue
		}
		TryUpdateActivityByTime(r, v)
	}
}

// 登录时或时间触发的时候，更新活动
// 比如可登录签到的天数等
func TryUpdateActivityByTime(r *msg.PlayerInfo, d *msg.ActivityData) {
	if d.Status == msg.ActivityStatus_AS_Closed {
		return
	}
	ActivityOpt(d).TryUpdateActivityByTime(r)
}

// 如果配置了完成即结束，则尝试提前结束
func tryCloseActivityByDirectEnd(r *msg.PlayerInfo, d *msg.ActivityData) {
	au := activity.GetMgr().GetUnit(d.Id)
	if au == nil {
		return
	}

	if !au.Config.IsDirectEnd {
		return
	}

	ActivityOpt(d).TryCloseByDirectEnd(r)
}

// 尝试关闭活动，并可能伴随发送奖励
// 关闭条件： 时间 | 已全部完成 等组合条件
func TryCloseActivity(r *msg.PlayerInfo, d *msg.ActivityData) {
	if d.Status == msg.ActivityStatus_AS_Closed {
		return
	}

	tryCloseActivityByDirectEnd(r, d)
	if d.Status == msg.ActivityStatus_AS_Closed {
		return
	}

	// 根据过期时间
	if d.CloseTimestamp > 0 {
		if d.CloseTimestamp < time3.Now().Unix() {
			//!!! ORDER
			trySendEndMail(r, d)
			trySendSupplyRewards(r, d)

			ActivityOpt(d).Close(r)
		}
	}
	//
	//if d.Status == msg.ActivityStatus_AS_Closed {
	//	return
	//}
}

func trySendEndMail(r *msg.PlayerInfo, ad *msg.ActivityData) {
	au := activity.GetMgr().GetUnit(ad.Id)
	if au != nil && au.Config.EndMail > 0 {
		err := &msg.ErrorInfo{}
		m := NewMailFromConf(au.Config.EndMail, "", "", nil, nil, err)
		if err.Id == 0 {
			AddMail(r, m, err)
		}
	}
}

// 尝试补偿邮件
func trySendSupplyRewards(r *msg.PlayerInfo, ad *msg.ActivityData) {
	au := activity.GetMgr().GetUnit(ad.Id)
	if au == nil {
		return
	}
	// 是否补发邮件
	if !au.Config.IsDispatch {
		return
	}

	ActivityOpt(ad).TrySendSupplyRewards(r)
}

func GetActivityConfig(activityId int32, err *msg.ErrorInfo) *activity.Unit {
	au := activity.GetMgr().GetUnit(activityId)
	if au == nil || au.Config == nil {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "not found activity config"
		return nil
	}
	return au
}
