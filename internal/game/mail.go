package game

import (
	"server/internal/gconst"
	"server/internal/log"
	"server/internal/util"
	"server/internal/util/time3"
	"server/pkg/gen/conf"
	"server/pkg/gen/msg"
	"server/pkg/gen/rawdata"
	"time"
)

// 对于线程安全的邮件队列，应该放到线程安全的玩家数据区， Safe区
// 然后在某一个玩家操作过程中去扫描safe区

func AutoClearMailSpace(r *msg.PlayerInfo, err *msg.ErrorInfo) {
	r2 := MustGetPlayerInfoExt(r)
	// 待删除数组
	delCopy := make([]int32, 0)
	for i := 0; i < len(r2.MailQueue); i++ {
		m := r2.MailQueue[i]
		// 判断邮件是否允许自动删除
		if m.SupportAutoDel && (len(r2.MailQueue)-len(delCopy) >= gconst.MaxMailNums) {
			delCopy = append(delCopy, m.Id)
		} else {
			break
		}
	}
	for i := 0; i < len(delCopy); i++ {
		RmMail(r, delCopy[i])
	}
}

// 线程非安全 -- 也就是只能在处理请求逻辑里调用
func AddMail(r *msg.PlayerInfo, m *msg.MailItem, err *msg.ErrorInfo) {
	r2 := MustGetPlayerInfoExt(r)
	if len(r2.MailQueue) >= gconst.MaxMailNums {
		// 自动清理邮件存储空间
		AutoClearMailSpace(r, err)
	}

	// Id分配在此处做
	m.Id = DispatchMailIdUnsafe(r2)
	// 根据邮件是否有奖励物品,设置奖励领取状态
	if len(m.Rewards) == 0 {
		m.IsRecved = true
	}
	r2.MailQueue = append(r2.MailQueue, m)
}

func AddGmMail(r *msg.PlayerInfo, m *msg.MailItem, err *msg.ErrorInfo) {
	r2 := MustGetPlayerInfoExt(r)
	if len(r2.GmMailBuffer) > gconst.MaxGmMailNums {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "gm mail upto max"
		return
	}
	// 不管Id
	r2.GmMailBuffer = append(r2.GmMailBuffer, m)

	RTMNotifyRedPoint(r.Id, msg.RedPointType_RPT_TriggerNewGmMail, 0, 0)
}

// 刷新邮件红点
func RefreshMailRedPoint(r *msg.PlayerInfo, notify bool) {
	r2 := MustGetPlayerInfoExt(r)
	// 只有0和1区别！
	curCount := int32(0)
	for _, v := range r2.MailQueue {
		if !v.IsReaded {
			curCount = 1
		}
		if !v.IsRecved {
			curCount = 1
		}

		if curCount > 0 {
			break
		}
	}

	_, rp := FindRedPointAnyway(r, &msg.RedPoint{
		Type: msg.RedPointType_RPT_Mail,
	})

	if rp.Count != curCount {
		rp.Count = curCount

		if notify {
			NotifyRedPoint(r, rp)
		}
	}
}

func NewSimpleMailFromRewards(r *msg.PlayerInfo, confId int32, rewards []*msg.StaticObjInfo) {
	if len(rewards) == 0 {
		return
	}
	err := &msg.ErrorInfo{}
	mail := NewMailFromConf(confId,
		"", "", nil, nil, err)
	if err.Id != 0 {
		log.Errorf("send mail(%d) err:%v", confId, err)
		return
	}

	mail.Rewards = rewards

	if err.Id == 0 {
		AddMail(r, mail, err)
	}
	if err.Id != 0 {
		log.Errorf("SendMail %v err:%v", mail, err)
	}
}

// 只是创建了
// MailItem 的Id 并没有分配，在最后进入queue的时候再分配
// 邮件参数，默认内部会自动添加。外面也可以加一些特殊的邮件参数
// rewardJson 优先使用的奖励,如果为空才会去加载模板奖励
func NewMailFromConf(confId int32, contentStr string, rewardJson string, paramsEx []string, paramsEx2 []string, err *msg.ErrorInfo) *msg.MailItem {
	curTime := time3.Now()
	return NewMailFromConfByTime(confId, contentStr, rewardJson, paramsEx, paramsEx2, curTime, err)
}

//paramsEx2 []string{"v","k"} => []int{0,1}
func NewMailFromConfByTime(confId int32, contentStr string, rewardJson string, paramsEx []string, paramsEx2 []string, sendTime time.Time, err *msg.ErrorInfo) *msg.MailItem {
	confData, _ := conf.GetMailConfig(confId)
	if !CheckConfData(confData, confId, err) {
		return nil
	}

	if len(rewardJson) == 0 {
		rewardJson = confData.Rewards
	}
	rewards, _ := JsonToPb_StaticObjs(rewardJson, confData, confId, err)
	if err.Id != 0 {
		return nil
	}

	curTime := sendTime.Unix()
	m := &msg.MailItem{
		Id:               0,
		TitleId:          confData.Title,
		TitleStr:         "",
		ContentId:        confData.Content,
		ContentStr:       contentStr,
		Rewards:          rewards,
		CreateTime:       curTime,
		Deadline:         curTime + gconst.MailDeadDuration,
		SupportManualDel: confData.SupportManualDel,
		OneClickReceive:  confData.OneClickReceive,
		OneClickDelete:   confData.OneClickDelete,
		SupportAutoDel:   confData.SupportAutoDel,
		SenderId:         confData.Sender,
	}

	// TODO add params
	// append param ex; replace if exist
	if paramsEx2 == nil {
		for i := 0; i < len(paramsEx); i++ {
			m.Params = append(m.Params, "0")
			m.Params = append(m.Params, paramsEx[i])
		}
	} else {
		for i := 0; i < len(paramsEx2); i++ {
			m.Params = append(m.Params, paramsEx2[i])
			m.Params = append(m.Params, paramsEx[i])
		}
	}

	return m
}

//1. 三种邮件
//1.1 gm邮件：发送到gmmailbuffer，定向个人邮件，玩家重新登录时生效
//1.2 功能(事件)触发邮件：在系统的某个功能触发点触发，比如升级奖励邮件，写死奖励邮件号为8，在升级时触发。(触发时推送redpoint)
//1.3 自动扫描邮件：比如系统维护邮件，为自扫描邮件，配置为autoScan，配合validStartTime和validEndTime. 数据中存档<mailId, lastValidTime>，如果无记录或不在配置期，则重新生效邮件。
//2. 邮件可携带参数(kv pairs)，在合适的时间传入或按规则程序生成

// 线程不安全，，直接扫描邮件，并插入到了邮件队列
func AutoAddMailFromConfScan(r *msg.PlayerInfo) {
	r2 := MustGetPlayerInfoExt(r)
	if r2.ScanMailHistory == nil {
		r2.ScanMailHistory = make(map[int32]int64)
	}

	curTime := time3.Now().Unix()
	confDatas := conf.GetMailConfigByFilter(func(config *rawdata.MailConfig) bool {
		// 客观有效
		if config.AutoScan &&
			int64(config.ValidStartTime) <= curTime &&
			curTime < int64(config.ValidEndTime) {
			// 历史主观无效: 是否重复添加
			if lastValidTime, ok := r2.ScanMailHistory[config.Id]; ok {
				if lastValidTime < int64(config.ValidStartTime) ||
					lastValidTime >= int64(config.ValidEndTime) {
					return true
				}
			} else {
				return true
			}
		}
		return false
	})

	// 直接添加
	err := &msg.ErrorInfo{}

	for _, confData := range confDatas {
		m := NewMailFromConf(confData.Id, "", "", nil, nil, err)
		if err.Id == 0 {
			AddMail(r, m, err)
			if err.Id == 0 {
				// 更新添加记录
				r2.ScanMailHistory[confData.Id] = curTime
			} else {
				log.Errorf("AddMail %v", err)
				continue
			}
		} else {
			log.Errorf("NewMailFromConf %v", err)
			continue
		}
	}
}

func AutoAddMailFromGmBuffer(r *msg.PlayerInfo) {
	r2 := MustGetPlayerInfoExt(r)
	if len(r2.GmMailBuffer) > 0 {
		for i := 0; i < len(r2.GmMailBuffer); i++ {
			tempErr := &msg.ErrorInfo{}
			AddMail(r, r2.GmMailBuffer[i], tempErr)
			// escape err
			if tempErr.Id != 0 {
				log.Error(tempErr)
			}
		}
		r2.GmMailBuffer = nil
	}
}

func AutoDelMailByDeadline(r *msg.PlayerInfo) {
	r2 := MustGetPlayerInfoExt(r)
	curTime := time3.Now().Unix()

	// 避免万一的逻辑死循环，加了循环次数
	for i := 0; i < gconst.MaxMailNums*2; i++ {
		index := util.SliceByCond(len(r2.MailQueue), func(i int) bool {
			// 过期邮件
			return r2.MailQueue[i].Deadline < curTime
		})
		if index != -1 {
			RmMail(r, r2.MailQueue[index].Id)
			continue
		}

		// 没有过期邮件，则跳出循环
		break
	}
}

func GetMail(r *msg.PlayerInfo, id int32, err *msg.ErrorInfo) *msg.MailItem {
	r2 := MustGetPlayerInfoExt(r)
	index := util.SliceByCond(len(r2.MailQueue), func(i int) bool {
		return r2.MailQueue[i].Id == id
	})
	if index == -1 {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "not found mail"
		return nil
	}
	return r2.MailQueue[index]
}

func RmMail(r *msg.PlayerInfo, id int32) {
	r2 := MustGetPlayerInfoExt(r)
	index := util.SliceByCond(len(r2.MailQueue), func(i int) bool {
		return r2.MailQueue[i].Id == id
	})
	if index == -1 {
		return
	}
	r2.MailQueue = append(r2.MailQueue[:index], r2.MailQueue[index+1:]...)
}

func DoRecvMailReward(r *msg.PlayerInfo, ids []int32, checkOneClickRecv bool, err *msg.ErrorInfo) (okIds []int32, addedObjs []*msg.ObjInfo) {
	//r2 := MustGetPlayerInfoExt(r)
	for i := 0; i < len(ids); i++ {
		id := ids[i]
		m := GetMail(r, id, err)
		if err.Id != 0 {
			return
		}

		// 检查一键领取，，但这个邮件不支持一键领取
		if checkOneClickRecv && !m.OneClickReceive {
			err.Id, err.DebugMsg = msg.Ecode_INVALID, "some mail dont support one click receive"
			return
		}

		// 只有有奖励才领奖
		if len(m.Rewards) > 0 {
			// 判断邮件是否已领取
			if m.IsRecved {
				err.Id, err.DebugMsg = msg.Ecode_INVALID, "mail receive is ok"
				return
			}

			tempAdded, _ := AddStaticObjs(r, m.Rewards, err)
			if err.Id != 0 {
				return
			}
			addedObjs = append(addedObjs, tempAdded...)
		}

		// change status
		m.IsRecved = true
		m.IsReaded = true

		okIds = append(okIds, id)
	}

	return
}
