package game

// 内部错误定义
// 有的定义并没有实现，根据需求扩充实现
//ref: 13. 错误处理，通用的错误处理，如条件不足，货币不足，在内部逻辑通过internalErr传出。
// 外部逻辑在defer里根据策划需求做二次加工，传给客户端。
const (
	// 错误号不应该是0，fool!!!
	// p1: buildId, p2: needLevel
	InternalErrCondBuildingLevel = iota + 1

	InternalErrCondOwnProsperity

	InternalErrCondTypeBuildingCount

	InternalErrCondMainChapter

	InternalErrCondTechLevel

	// p1: fakeId
	InternalErrUnEnoughItemOrCurrency
)
