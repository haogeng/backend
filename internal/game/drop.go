package game

import (
	"fmt"
	"server/internal/log"
	"server/internal/util"
	"server/pkg/gen/conf"
	"server/pkg/gen/msg"
)

func GetDropUnits(r *msg.PlayerInfo, dropId int32, err *msg.ErrorInfo) (totalUnits []*msg.StaticObjInfo) {
	confData, _ := conf.GetDropConfig(dropId)
	if !CheckConfData(confData, dropId, err) {
		return
	}

	num, ok := util.GetRandomValueByMapWeights(confData.NumProbability, false)
	if !ok {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "rand weights err"
		return
	}

	if num <= 0 {
		//err.Id, err.DebugMsg = msg.Ecode_INVALID, "num is zero in drop"
		log.Debugf("num is zero in drop %d", dropId)
		return
	}

	// 平均随几次？ 不存在100+这种吧？
	if num > 100 {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "drop num too much, pls check data"
		return
	}

	dropedIds := make(map[int32]bool)

	for i := int32(0); i < num; i++ {
		// 判断重复，最多尝试10次
		for ti := 0; ti < 10; ti++ {
			objId, ok := util.GetRandomValueByMapWeights(confData.DropList, false)
			if !ok {
				err.Id, err.DebugMsg = msg.Ecode_INVALID, "dropList err"
				return
			}

			//log.Debugf("ObjId %d", objId)

			// 排他，独此一份
			if !confData.IsRepeat {
				// 重试
				if _, ok := dropedIds[objId]; ok {
					log.Infof("repeated objId %d", objId)
					continue
				}
			}
			dropedIds[objId] = true

			// drop it
			objConfData, _ := conf.GetObjList(objId)
			if !CheckConfData(objConfData, objId, err) {
				return
			}

			units, _ := JsonToPb_StaticObjs(objConfData.ObjList, objConfData, objConfData.Id, err)
			if err.Id != 0 {
				return
			}
			if len(units) == 0 {
				err.Id, err.DebugMsg = msg.Ecode_INVALID, fmt.Sprintf("obj(%d) is nil", objId)
				return
			}

			totalUnits = append(totalUnits, units...)
			break
		}
	}

	return
}

// showAwards 比如有的东西，需要开两次，礼包里出个礼包，这种暂时不考虑
// dropAwards 考虑堆叠上线，，是否堆叠等，开箱子等界面显示的时候按这个显示，，
// realAwards 客户端实际通过该列表加具体的奖励
func Drop(r *msg.PlayerInfo, dropId int32, err *msg.ErrorInfo) (dropAwards []*msg.ObjInfo, realAwards []*msg.ObjInfo) {
	log.Debugf("u%v StartDrop %v", r.Id, dropId)
	confData, _ := conf.GetDropConfig(dropId)
	if !CheckConfData(confData, dropId, err) {
		return
	}

	units := GetDropUnits(r, dropId, err)

	log.Debugf("u%v StartDrop %v", r.Id, units)

	if err.Id != 0 {
		return
	}
	if len(units) == 0 {
		//err.Id, err.DebugMsg = msg.Ecode_INVALID, fmt.Sprintf("drop %d get nil", dropId)
		log.Debugf("drop %d get nil", dropId)
		return
	}

	// do real drop
	tempRet, result := AddStaticObjs(r, units, err)
	dropAwards = append(dropAwards, tempRet...)
	realAwards = append(realAwards, tempRet...)
	if !result {
		return
	}

	if realAwards != nil {
		realAwards = MergeObjInfo(realAwards)
	}

	// merge dropAward
	if confData.IsStack {
		if dropAwards != nil {
			dropAwards = MergeObjInfo(dropAwards)
		}
	}

	return
}
