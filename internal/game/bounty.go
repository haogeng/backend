package game

import (
	"bitbucket.org/funplus/sandwich/current"
	"server/internal/log"
	"server/internal/util"
	"server/pkg/fungin/xmiddleware"
	"server/pkg/gen/conf"
	"server/pkg/gen/msg"
)

func MakeSureBountyExist(r *msg.PlayerInfo) {
	if r.Bounties == nil {
		r.Bounties = &msg.BountyInfo{
			Level:     1,
			RemainExp: 0,
			Items:     nil,
		}
		// 1级解锁的时候不需要红点提示
		//NotifyBountyLevelRedPoint(r, 1)
	}
	if r.Bounties.Items == nil {
		r.Bounties.Items = make(map[int32]*msg.BountyItem)
	}
}

// by dynamic id
func DeleteBountyTask(r *msg.PlayerInfo, id int32) {
	delete(r.Bounties.Items, id)
}

func addBountyTask(r *msg.PlayerInfo, configId int32) {
	// check count
	MaxCount := GetBountyDailyMaxCount(r.Bounties.Level)
	if int32(len(r.Bounties.Items)) >= MaxCount {
		log.Errorf("Bounties.Items overflow")
		return
	}
	item := &msg.BountyItem{
		Id:       DispatchObjIdUnsafe(r, msg.ObjType_OT_BountyTask),
		ConfigId: configId,
		Status:   msg.BountyStatusType_BST_Free,
		EndTime:  0,
		TroopIds: nil,
	}
	r.Bounties.Items[item.Id] = item
}

func resetBountyDaily(r *msg.PlayerInfo, withSync bool) {
	RefreshBounty(r, true, false, false)

	if withSync {
		NotifyBountyInfo(r)
	}
}

// 派遣任务手动可填充的树木
func GetBountyNumsOfManualFill(r *msg.PlayerInfo) int32 {
	MakeSureBountyExist(r)
	freeNums := int32(0)

	//log.Debug(r.Bounties.Items)

	for _, v := range r.Bounties.Items {
		if v.Status == msg.BountyStatusType_BST_Free {
			freeNums++
		}
	}
	return freeNums
}

// 刷新
// 功能首次解锁的时候 | 手动刷新 | 领取成功 | 升级
// 三个bool互斥
// 需要先删除已领取的
// refillAll 会被重用，，不要过多以来字面含义...
func RefreshBounty(r *msg.PlayerInfo, refillAll bool, manual bool, fillSomeOnGotAward bool) {
	MakeSureBountyExist(r)
	// 补满
	MaxCount := GetBountyDailyMaxCount(r.Bounties.Level)

	// 手动刷新条数
	manualCount := int32(0)
	// 如果是manual，则删除目前空的
	if manual || refillAll {
		delIds := make([]int32, 0, len(r.Bounties.Items))
		for k, v := range r.Bounties.Items {
			if v.Status == msg.BountyStatusType_BST_Free {
				delIds = append(delIds, k)
			}
		}

		if manual {
			manualCount = int32(len(delIds))
		}

		for _, id := range delIds {
			delete(r.Bounties.Items, id)
		}
	}

	confData, _ := conf.GetBountyListConfig(r.Bounties.Level)
	if !CheckConfData(confData, r.Bounties.Level, nil) {
		return
	}

	// 默认填满
	fillCount := MaxCount - int32(len(r.Bounties.Items))

	// 如果是因为领取的，则填一部分
	if fillSomeOnGotAward {
		fillCount = 1
	} else if manual {
		fillCount = manualCount
	}

	log.Debugf("confData: %+v", confData)
	dropId := confData.DropId
	// fill
	for i := int32(0); i < fillCount; i++ {
		// drop will add bounty item.
		Drop(r, dropId, &msg.ErrorInfo{})
	}
}

func GetBountyDailyMaxCount(level int32) int32 {
	confData, _ := conf.GetBountyListConfig(level)
	if !CheckConfData(confData, level, nil) {
		return 0
	}

	return confData.TaskCountLimit
}

func GetBountyManualRefreshCost() *msg.StaticObjInfo {
	globalConf, _ := conf.GetGlobalConfig(1)
	if globalConf == nil {
		panic("globalConf is nil")
	}

	num := globalConf.BountyRefreshCost
	return &msg.StaticObjInfo{
		Id:    int32(msg.CurrencyId_CI_Diamond),
		Type:  int32(msg.ObjType_OT_Currency),
		Count: int64(num),
	}
}

func GetBountyMaxLevel() int32 {
	allConf := conf.GetBountyListConfigConf().BountyListConfigs
	maxLevel := int32(len(allConf))
	return maxLevel
}

// 不用单独sync，，只有领取的时候才加经验，领取后必然sync
// 只是单纯的加exp
func AddBountyExp(r *msg.PlayerInfo, exp int32) {
	if r.Bounties == nil {
		panic("bounties is nil")
	}

	maxLevel := GetBountyMaxLevel()
	if r.Bounties.Level >= maxLevel {
		// skip.
		return
	}

	r.Bounties.RemainExp += exp
	for r.Bounties.Level < maxLevel {
		confData, _ := conf.GetBountyListConfig(r.Bounties.Level)
		if !CheckConfData(confData, r.Bounties.Level, nil) {
			break
		}

		if r.Bounties.RemainExp >= confData.LvUpExp {
			r.Bounties.RemainExp -= confData.LvUpExp
			r.Bounties.Level++

			NotifyBountyLevelRedPoint(r, r.Bounties.Level)
		} else {
			break
		}
	}
}

func NotifyBountyLevelRedPoint(r *msg.PlayerInfo, newLevel int32) {
	_, rp := FindRedPointAnyway(r, &msg.RedPoint{
		Type:   msg.RedPointType_RPT_BountyLevelup,
		Param1: newLevel,
	})
	if rp.Count == 0 {
		rp.Count = 1
		NotifyRedPoint(r, rp)
	}
}

// 推送
func NotifyBountyInfo(r *msg.PlayerInfo) {
	ctx := xmiddleware.GetCtxByUid(r.Id)
	if ctx != nil {
		n := &msg.BountyInfoNotify{
			Info: r.Bounties,
		}
		current.MustAppendAdditionalOutgoingMessage(ctx, n)
	}
}

func GetBounty(r *msg.PlayerInfo, id int32, err *msg.ErrorInfo) *msg.BountyItem {
	item, ok := r.Bounties.Items[id]
	if !ok {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "no bounty"
		return nil
	}

	return item
}

func HasRepeatBountyTroopId(r *msg.PlayerInfo, troopIds []int32) bool {
	for _, v := range r.Bounties.Items {
		for _, id := range v.TroopIds {
			if util.InArray32(troopIds, id) {
				return true
			}
		}
	}

	return false
}

// 效率非常低下，，满足当前业务逻辑
func extendIds(ids []int32) [][]int32 {
	if len(ids) == 0 {
		return nil
	}

	if len(ids) == 1 {
		return [][]int32{[]int32{ids[0]}}
	}

	ret := [][]int32{}
	for i := 0; i < len(ids); i++ {
		pi := ids[i]
		pOther := []int32{}
		pOther = append(pOther, ids[:i]...)
		pOther = append(pOther, ids[i+1:]...)

		tempRet := extendIds(pOther)
		for j := 0; j < len(tempRet); j++ {
			pj := []int32{}
			pj = append(pj, pi)
			pj = append(pj, tempRet[j]...)

			ret = append(ret, pj)
		}
	}

	return ret
}

// 检查是否有重复； if not repeat & no err, return true
func CheckTroopConfigIdsUnique(r *msg.PlayerInfo, ids []int32, err *msg.ErrorInfo) bool {
	configIds := make(map[int32]bool)
	for i := 0; i < len(ids); i++ {
		t := GetTroop(r, ids[i], err)
		// no troop
		if err.Id != 0 {
			break
		}

		if _, ok := configIds[t.ConfigId]; ok {
			err.Id, err.DebugMsg = msg.Ecode_INVALID, "has repeated config id in in troops"
			break
		}

		configIds[t.ConfigId] = true
	}

	return err.Id == 0
}

func CheckBountyNeed(r *msg.PlayerInfo, needTroopsX []*msg.StaticObjInfo, ids []int32, err *msg.ErrorInfo) bool {
	// troop和slot顺序不确定
	// 对ids 进行重新排列，只要一个排列满足条件，则OK
	if len(ids) == 0 {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "no troops"
		return false
	}

	// 英雄的config id 不能重
	if !CheckTroopConfigIdsUnique(r, ids, err) {
		return false
	}

	// 逐个排列试验
	extendIdsAry := extendIds(ids)
	for i := 0; i < len(extendIdsAry); i++ {
		if checkMatchTroop(r, needTroopsX, extendIdsAry[i], false, err) {
			return true
		}
	}

	// must err.
	if err.Id == 0 {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "bounty need unenough"
	}

	return false
}
