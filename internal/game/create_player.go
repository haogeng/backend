package game

import (
	"fmt"
	"math/rand"
	"server/internal/log"
	"server/pkg/gen/conf"
	"server/pkg/gen/msg"
	"server/pkg/gen_redis/redisdb"
	"server/pkg/xsnowflake"
	"time"
)

func CreateNewPlayer(serverId int32, fakeUserId int32, civilId int32, deviceName string) (r *msg.PlayerInfo) {
	// check civil id outside
	uid := xsnowflake.NextIdUint64()
	//log.Infof("Start %d", uid)
	//defer log.Infof("End %d", uid)

	var c redisdb.PlayerInfoCache
	r = c.Find(uid)
	if r != nil {
		log.Errorf("repeated player %d", uid)
		panic(fmt.Errorf("repeated player %d", uid))
		return
	}
	globalConfig, _ := conf.GetGlobalConfig(1)
	if !CheckConfData(globalConfig, 1, &msg.ErrorInfo{}) {
		return
	}
	nameOnly := fmt.Sprintf("U%d", uid)
	nameShow := fmt.Sprintf("%d", globalConfig.InitUserName)
	// TODO 信息配置需要提取出来，策划填
	// Init player
	r = &msg.PlayerInfo{
		Id:       uid,
		ServerId: serverId,
		// 默认先罗马文明
		CivilId:            civilId,
		FakeUserId:         fakeUserId,
		Name:               nameShow,
		NameReal:           nameOnly,
		Icon:               globalConfig.InitUserPortraits,
		DeviceNameDup:      deviceName,
		MaxBuildQueueCount: 1,
		MaxTechQueueCount:  1,
		// 新玩家上来，在第一次登录的时候，会走一次每日重置，需要能兼容
		DailyResetTime:  0,
		WeeklyResetTime: 0,

		RegisterTime: time.Now().Unix(),
	}

	r.Currency = make(map[int32]int64)
	// make sure init
	r.Currency[int32(msg.CurrencyId_CI_Gold)] = 0
	r.Currency[int32(msg.CurrencyId_CI_Diamond)] = 0
	// 初始化为1级
	// 需要触发一些内容
	r.Currency[int32(msg.CurrencyId_CI_PlayerLevel)] = 0
	RefreshPlayerLevel(r)

	r.DupGold = r.Currency[int32(msg.CurrencyId_CI_Gold)]
	r.DupDiamond = r.Currency[int32(msg.CurrencyId_CI_Diamond)]
	r.DupLevel = 1

	// 随机值【0,10) 通过mod在某些情况下做用户标签拆分
	r.RandomVal = rand.Int31n(10)

	//!!! ORDER!!!
	r.Buildings = make(map[int32]*msg.BuildingInfo)
	MakeSureAllBuildingExists(r)

	//!!! 类似map在反序列化的时候会搞空...
	ExtEquip(r).Equips = make(map[int32]*msg.ObjInfo)

	r.Items = make(map[int32]*msg.ObjInfo)

	// init data to avoid map is nil
	r.Draws = make(map[int32]int32)
	r.Draws[1] = 0

	r.Tasks = &msg.TaskInfo{
		TaskCounts: make(map[int32]int64),
		TaskLevels: make(map[int32]int32),
	}

	// 副本的三个难度
	ValidMainLineAry(r)

	initTask(r)
	initBounty(r)
	initLevyHouse(r)
	initStore(r)

	// ORDER!
	// 通过这种AddObjIntoMap去添加
	ExtTroop(r).Troops = make(map[int32]*msg.ObjInfo)
	initTroop(r, civilId)

	initItemOrCurrencyFromConf(r, civilId)

	r.TechInfos = make(map[int32]*msg.TechInfo)
	c.MustCreate(uid, r)

	// 补充存档扩展信息
	if r.Troop != nil {
		var c2 redisdb.PlayerInfoTroopCache
		c2.MustUpdate(uid, r.Troop)
	}
	if r.Equip != nil {
		var c2 redisdb.PlayerInfoEquipCache
		c2.MustUpdate(uid, r.Equip)
	}

	// 强制关联存档 级联数据表
	c.MustUpdate(uid, r)

	return
}

func ValidMainLineAry(r *msg.PlayerInfo) {
	if len(r.MaxFiniMainLineAry) != 3 {
		r.MaxFiniMainLineAry = make([]int32, 3)
	}
}

func initItemOrCurrencyFromConf(r *msg.PlayerInfo, civilId int32) {
	confData, _ := conf.GetCivilizationSelConfig(civilId)
	tempErr := &msg.ErrorInfo{}
	if !CheckConfData(confData, civilId, tempErr) {
		log.Errorf("InitPlayer err:%v", tempErr)
	} else {
		units, _ := JsonToPb_StaticObjs(confData.InitItem, confData, confData.Id, tempErr)
		//log.Debug(units)
		if tempErr.Id == 0 {
			added, _ := AddStaticObjs(r, units, tempErr)
			log.Debugf("Init item: %v", added)
		} else {
			log.Errorf("init item err:%v", tempErr)
		}
	}
}

func CheckDataOnLogin(r *msg.PlayerInfo) {

}

func initTroop(r *msg.PlayerInfo, civilId int32) {
	confData, _ := conf.GetCivilizationSelConfig(civilId)
	tempErr := &msg.ErrorInfo{}
	if !CheckConfData(confData, civilId, tempErr) {
		log.Errorf("InitPlayer err:%v", tempErr)
	} else {
		units, _ := JsonToPb_StaticObjs(confData.InitTroop, confData, confData.Id, tempErr)
		//log.Debug(units)
		if tempErr.Id == 0 {
			added, _ := AddStaticObjs(r, units, tempErr)
			log.Debugf("Init troop: %v", added)
		} else {
			log.Errorf("init troop err:%v", tempErr)
		}
	}
}

func initTask(r *msg.PlayerInfo) {
	// !!!ORDER
	// scan任务列表，，判断是否有合适的任务
	tasks := conf.GetAllTaskConfigConf().AllTaskConfigs
	for _, v := range tasks {
		if CheckTaskCondForActive(r, v.Id) {
			addTask(r, v)
		}
	}
	RefreshTaskUnactiveButShow(r)
}

func initBounty(r *msg.PlayerInfo) {
	MakeSureBountyExist(r)
	// 其实不应该，，测试期，有可能任务大厅上来就开
	b := GetBuildingByType(r, int32(msg.BuildingType_BT_QuestHall))
	if b != nil && b.Level == 1 {
		log.Debugf("refreshBounty when debug init Bounty")
		RefreshBounty(r, true, false, false)
	}
}

func initLevyHouse(r *msg.PlayerInfo) {
	r.LevyHouses = &msg.LevyHouseInfo{
		OnHookIncomeTime:     0,
		QuickOnHookTimes:     0,
		FreeQuickOnHookTimes: 0,
	}
}

func MakeSureAllBuildingExists(r *msg.PlayerInfo) {
	if r.Buildings == nil {
		r.Buildings = make(map[int32]*msg.BuildingInfo)
	}

	configs := conf.GetBuildingConfigConf().BuildingConfigs
	for _, v := range configs {
		if _, ok := r.Buildings[v.Id]; !ok {
			b := &msg.BuildingInfo{
				Id:    v.Id,
				Level: 0,
				State: msg.BuildingState_LOCK,
			}
			if v.InitState == int32(msg.BuildingState_NORMAL) {
				b.Level = 1
				b.State = msg.BuildingState_NORMAL
			} else {
				b.State = msg.BuildingState_LOCK
			}
			r.Buildings[v.Id] = b
		}
	}
}
