package game

import (
	"server/internal/clan"
	"server/internal/log"
	"server/pkg/gen/conf"
	"server/pkg/gen/msg"
)

//！！！ 约定： 只有在协议处理函数中才会调用r.MustUpdate去存档

// 支持ObjType_OT_Currency & Item

// 通过err传出错误信息
// err不能为空！
func CheckConsume(r *msg.PlayerInfo, unit *msg.StaticObjInfo, err *msg.ErrorInfo) bool {
	ret := CheckAndSubStaticObj(r, unit, err, false)
	//log.Debugf("Checkconsume:unit:%v ret: %v, err: %v", unit, ret, err)

	return ret
}
func CheckConsumes(r *msg.PlayerInfo, units []*msg.StaticObjInfo, err *msg.ErrorInfo) bool {
	for _, v := range units {
		if !CheckConsume(r, v, err) {
			return false
		}
	}
	return true
}
func SubConsume(r *msg.PlayerInfo, unit *msg.StaticObjInfo, err *msg.ErrorInfo) bool {
	return CheckAndSubStaticObj(r, unit, err, true)
}
func SubConsumes(r *msg.PlayerInfo, units []*msg.StaticObjInfo, err *msg.ErrorInfo) bool {
	for _, v := range units {
		if !SubConsume(r, v, err) {
			return false
		}
	}
	return true
}

func CheckAndSubStaticObj(r *msg.PlayerInfo, unit *msg.StaticObjInfo, err *msg.ErrorInfo, doSub bool) bool {
	t := msg.ObjType(unit.Type)
	switch t {
	case msg.ObjType_OT_Currency:
		return checkAndSubStaticObj_Currency(r, unit, err, doSub)

	case msg.ObjType_OT_Item:
		return checkAndSubStaticObj_Item(r, unit, err, doSub)
	case msg.ObjType_OT_ClanActiveValue:
		return checkAndSubStaticObj_ClanActive(r, unit, err, doSub)
	default:
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "no support sub this static obj"
		log.Errorf("u%d CheckAndSubStaticObj %v failed\n", r.Id, unit)
		return false
	}
}
func subCurrency64(total *int64, delta int64, doSub bool) bool {
	if *total < int64(delta) {
		return false
	}
	if doSub {
		*total -= int64(delta)
	}
	return true
}
func subCurrency32(total *int32, delta int64, doSub bool) bool {
	if *total < int32(delta) {
		return false
	}
	if doSub {
		*total -= int32(delta)
	}
	return true
}
func checkAndSubStaticObj_Currency(r *msg.PlayerInfo, unit *msg.StaticObjInfo, err *msg.ErrorInfo, doSub bool) (result bool) {
	// 统一追加错误处理
	defer func() {
		if !result {
			err.Id, err.DebugMsg = msg.Ecode_UNENOUGH_ITEM_OR_CURRENCY, "unenough currency"
			if unit != nil {
				fakeId := GetFakeId(msg.ObjType(unit.Type), unit.Id)
				err.IntParams = []int64{int64(fakeId)}
			}
		}
	}()

	//currencyId := msg.CurrencyId(unit.Id)
	confData, _ := conf.GetCurrencyConfig(unit.Id)
	if !CheckConfData(confData, unit.Id, err) {
		return false
	}

	c, exist := r.Currency[unit.Id]
	if exist {
		result = subCurrency64(&c, unit.Count, doSub)
		if result {
			r.Currency[unit.Id] = c

			switch msg.CurrencyId(unit.Id) {
			case msg.CurrencyId_CI_Gold:
				{
					r.DupGold = c
				}
			case msg.CurrencyId_CI_Diamond:
				{
					r.DupDiamond = c
				}
			case msg.CurrencyId_CI_PlayerLevel:
				{
					r.DupLevel = int32(c)
				}
			}
		}
		return result
	}

	return false
}

func checkAndSubStaticObj_Item(r *msg.PlayerInfo, unit *msg.StaticObjInfo, err *msg.ErrorInfo, doSub bool) (result bool) {
	// 统一追加错误处理
	defer func() {
		if !result {
			err.Id, err.DebugMsg = msg.Ecode_UNENOUGH_ITEM_OR_CURRENCY, "unenough res"
			if unit != nil {
				fakeId := GetFakeId(msg.ObjType(unit.Type), unit.Id)
				err.IntParams = []int64{int64(fakeId)}
			}
		}
	}()

	d, ok := r.Items[unit.Id]
	if !ok {
		if unit.Count > 0 {
			return false
		}
	}
	if d.Num < int64(unit.Count) {
		return false
	}

	if doSub {
		d.Num -= int64(unit.Count)
		// remove item.
		if d.Num == 0 {
			DelItem(r, d.Id)
		}
	}

	return true
}

func checkAndSubStaticObj_ClanActive(r *msg.PlayerInfo, unit *msg.StaticObjInfo, err *msg.ErrorInfo, doSub bool) (result bool) {
	// 统一追加错误处理
	defer func() {
		if !result {
			err.Id, err.DebugMsg = msg.Ecode_UNENOUGH_ITEM_OR_CURRENCY, "unenough res"
			if unit != nil {
				fakeId := GetFakeId(msg.ObjType(unit.Type), unit.Id)
				err.IntParams = []int64{int64(fakeId)}
			}
		}
	}()

	if !clan.CheckClanActive(r, unit.Count) {
		return false
	}

	return true
}
