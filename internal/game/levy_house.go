package game

import (
	"bitbucket.org/funplus/golib/deepcopy"
	"math"
	"math/rand"
	"server/internal/log"
	"server/internal/util/time3"
	"server/pkg/gen/conf"
	"server/pkg/gen/rawdata"
	//"server/pkg/gen/conf"
	"server/pkg/gen/msg"
)

const (
	effectTypeFix          = 1
	effectTypeRandomNum    = 2
	effectTypeRandomWeight = 3
)

func CalcOnhookUnitTimes(onHookEffectTime int32, outputUnitTime int32) (unitTimes int32) {
	unitTimes = int32(math.Floor(float64(onHookEffectTime) / float64(outputUnitTime)))
	return
}

/*
挂机时长 = 当前点击收取时间戳 - 上次挂机收取时间戳
各类资源单位时间产量 = 资源基础单位时间产量 + 科技增加单位时间产量
固定产出量 = 各类资源单位时间产量 x （挂机时长 / 固定产出单位时间）
-------------------------------------------------------------------------------------------------------------
注：产出单位时间与资源单位时间产量需要配置，固定产出资源有多种，根据建筑等级升级种类数量会增加，具体会在建筑升级数据中体现。
*/
func fixedOutput(unitTimes int32, output []*msg.StaticObjInfo, techAddResOutput map[int32]int32) (finalOutput []*msg.StaticObjInfo) {
	log.Debugf("fixedOutput:%+v", output)
	log.Debugf(" times :+%v", int64(unitTimes))
	for _, v := range output {
		//log.Debugf("index:%+v", index)
		//log.Debugf("value:+%v", v)
		//log.Debugf("techAddResOutput[v.Id] :+%v", techAddResOutput[v.Id])
		log.Debugf("v.Count without times :+%v", v.Count+int64(techAddResOutput[v.Id]))
		v.Count = (v.Count + int64(techAddResOutput[v.Id])) * int64(unitTimes)
		//log.Debugf("v.Count :+%v", v.Count)
		finalOutput = append(finalOutput, v)
	}
	return
}

/*
按照功能需求不同随机掉落分以下两种：
科技附加掉落：通过科技研究可改变产物的产量和随机概率的掉落功能
科技替换掉落：通过科技研究直接替换掉掉落原有的随机掉落配置。
挂机时长 = 当前点击收取时间戳 - 上次挂机收取时间戳
随机产出单位时间 = 建筑等级对应掉落产单位产出时间
随机产出概率 = 科技附加掉落基础产出概率 + 对应科技等级配置增加产出概率
随机产出道具数量 =科技附加掉落基础产出数量 + 对应科技等级配置增加产出数量
随机次数 = 挂机时长 / 随机产出单位时间
*/
func randomOutput(unitTimes int32, outputObjs []*msg.StaticObjInfo, itemRate map[int32]int32, techAddItemOutput map[int32]int32, techAddItemWeight map[int32]int32) (finalOutput []*msg.StaticObjInfo) {
	log.Debugf("randomOutput unitTimes is %d ", unitTimes)

	rand.Seed(time3.Now().UnixNano())
	changeItemRate := make(map[int32]float64)
	for outputIndex, item := range outputObjs {
		log.Debugf("before outputIndex:%+v,item:%+v", outputIndex, item)
		log.Debugf("techAddItemOutput[item.Id] %+v", techAddItemOutput[item.Id])
		if techAddItemOutput[item.Id] >= 0 {
			item.Count = item.Count + int64(techAddItemOutput[item.Id])
		}

		var itemRateIndex int
		for _, baseWeight := range itemRate {
			itemRateIndex = itemRateIndex + 1
			if itemRateIndex-1 != outputIndex {
				continue
			}
			log.Debugf("find itemRate baseWeight:%+v", baseWeight)
			log.Debugf("Weight:%+v", techAddItemWeight[item.Id])
			if 0 >= techAddItemWeight[item.Id] {
				continue
			}
			changeItemRate[item.Id] = float64(baseWeight+techAddItemWeight[item.Id]) / RandomBaseNum
			//log.Debugf("before item.Id:%+v,itemId:%+v", item.Id, itemId)
		}

		if 0 == changeItemRate[item.Id] {
			log.Debugf("changeItemRate[item.Id] ERROR :%+v,unitTimes:%+v,item.Count:%+v", changeItemRate[item.Id], unitTimes, item.Count)
			continue
		}
		log.Debugf("changeItemRate[item.Id]:%+v,unitTimes:%+v,item.Count:%+v", changeItemRate[item.Id], unitTimes, item.Count)
		finalCount := int64(changeItemRate[item.Id] * float64(unitTimes) * float64(item.Count))
		log.Debugf("finalCount:%+v", finalCount)
		finalOutput = append(finalOutput, item)
	}
	return
}

func techDrop(unitTimes int32, outputObjs []*msg.StaticObjInfo) (finalOutput []*msg.StaticObjInfo) {
	var index int32
	//......time is too more
	log.Debugf("techDrop unitTimes is %d ", unitTimes)
	for ; index < unitTimes; index++ {
		for _, value := range outputObjs {
			log.Debugf("techDrop item value  %+v", value)
			finalOutput = append(finalOutput, value)
		}
	}
	return
}

//必选随机过多,优化。修正次数!!!
func fixTimes(onHookEffectTime int32, outputTime int32) (unitTimes int32) {
	log.Debugf("onHookEffectTime:%+v,outputTime:%+v", onHookEffectTime, outputTime)

	if onHookEffectTime >= OneHourSeconds {
		hours := onHookEffectTime / OneHourSeconds
		unitTimes = int32(math.Floor(float64(onHookEffectTime % OneHourSeconds / outputTime)))
		unitTimes = hours + unitTimes
	} else {
		unitTimes = int32(math.Floor(float64(onHookEffectTime) / float64(outputTime)))
	}
	log.Debugf("final unitTimes:%+v", unitTimes)
	return
}

func techEffect(r *msg.PlayerInfo, techAddOutput map[int32]int32, effectType int32) (finalOutput map[int32]int32) {
	finalOutput = make(map[int32]int32)
	techInfos := GetTechInfos(r)
	if techInfos == nil {
		return
	}

	tempErr := &msg.ErrorInfo{}

	for objId, techEffectId := range techAddOutput {
		effectTechInfos, ret := GetTechByEffectId(r, techEffectId)
		if ret == false {
			continue
		}
		for k, _ := range effectTechInfos {
			techId := int32(effectTechInfos[k].Id)
			if techInfos[techId] == nil {
				continue
			}
			technologyLevelConfig, ret := GetLevelConfByTech(techInfos[techId], tempErr)
			if ret == false || nil == technologyLevelConfig {
				continue
			}
			if effectTypeFix == effectType {
				finalOutput[objId] += technologyLevelConfig.ResourceUp
			} else if effectTypeRandomNum == effectType {
				finalOutput[objId] += technologyLevelConfig.RandomResourceUp
			} else if effectTypeRandomWeight == effectType {
				finalOutput[objId] += technologyLevelConfig.RandomResourceWeightsUp
			}
		}
	}
	log.Debugf("techEffect finalOutput :%+v", finalOutput)
	return
}

func techEffectOutput(r *msg.PlayerInfo, techAddResOutput map[int32]int32) (finalOutput map[int32]int32) {
	finalOutput = techEffect(r, techAddResOutput, effectTypeFix) //1固定产出
	return
}

func techEffectRandomOutput(r *msg.PlayerInfo, techAddItemOutput map[int32]int32) (techRandomOutput map[int32]int32) {
	techRandomOutput = techEffect(r, techAddItemOutput, effectTypeRandomNum) //2
	return
}

func techEffectRandomWeight(r *msg.PlayerInfo, techAddItemWeight map[int32]int32) (techRandomWeight map[int32]int32) {
	techRandomWeight = techEffect(r, techAddItemWeight, effectTypeRandomWeight)
	return
}

func techEffectDropPlace(r *msg.PlayerInfo, techDropId int32, originObjs []*msg.StaticObjInfo) (finalDropObjs []*msg.StaticObjInfo) {
	//finalRandomAddWeight = techEffect(r ,techDropId, 4)
	finalDropObjs = originObjs

	techInfos := GetTechInfos(r)
	if techInfos == nil {
		return
	}

	err := &msg.ErrorInfo{}

	technologyLevelConfig, ret := GetLevelConfByTech(techInfos[techDropId], err)
	if ret == false || nil == technologyLevelConfig {
		return
	}
	finalDropObjs, _ = JsonToPb_StaticObjs(technologyLevelConfig.DropReplace, technologyLevelConfig, technologyLevelConfig.Id, err)
	if err.Id != 0 {
		return
	}
	//log.Debugf("techEffectDropPlace is :%+v",finalDropObjs)
	return
}

func CheckOnHookCondition(r *msg.PlayerInfo, err *msg.ErrorInfo) bool {
	GetBuildingInfo(r, err)
	if err.Id != 0 {
		return false
	}
	return true
}

func GetBuildingInfo(r *msg.PlayerInfo, err *msg.ErrorInfo) (buildingInfo *msg.BuildingInfo, buildingConf *rawdata.BuildingConfig) {
	buildingInfo = GetBuildingByType(r, int32(msg.BuildingType_BT_LevyHouse))
	buildingConf, _ = conf.GetBuildingConfig(buildingInfo.Id)
	if !CheckBuildingExist(buildingInfo, buildingConf, err) {
		return
	}
	if buildingInfo.State == msg.BuildingState_LOCK {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "state err"
		return
	}
	return
}

func GetOnHookTime(r *msg.PlayerInfo, err *msg.ErrorInfo) (onHookEffectTime int64) {
	curTime := time3.Now().Unix()
	if r.LevyHouses.OnHookIncomeTime == 0 {
		buildingInfo := GetBuildingByType(r, int32(msg.BuildingType_BT_LevyHouse))
		if buildingInfo.Level == 1 {
			r.LevyHouses.OnHookIncomeTime = curTime
		}
		//err.Id, err.DebugMsg = msg.Ecode_INVALID, "GetOnHookTime error"
		return
	}
	onHookEffectTime = curTime - r.LevyHouses.OnHookIncomeTime
	if onHookEffectTime < 0 {
		log.Errorf("onHookEffectTime** %d ", onHookEffectTime)
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "onHookEffectTime error"
		return
	}
	globalConf, ok := conf.GetGlobalConfig(1)
	if !ok {
		panic("globalConf is nil")
	}
	if onHookEffectTime >= int64(globalConf.HUEffectiveDuration) {
		onHookEffectTime = int64(globalConf.HUEffectiveDuration)
	}
	log.Debugf("onHookEffectTime && %d ", onHookEffectTime)
	return
}

func CalcOnHookIncome(r *msg.PlayerInfo, onHookEffectTime int32, buildingInfo *msg.BuildingInfo, buildingConf *rawdata.BuildingConfig, err *msg.ErrorInfo) (objs []*msg.StaticObjInfo) {
	log.Debugf("------onHookEffectTime time %+v------", onHookEffectTime)
	levelConf, _ := GetLevelConf(buildingConf, buildingInfo.Level, err)
	if err.Id != 0 {
		return
	}

	//log.Debugf("levelConf.OutputResCfg +%v", levelConf.OutputResCfg)
	fixedUnits, _ := JsonToPb_StaticObjs(levelConf.OutputResCfg, levelConf, levelConf.Id, err)
	fixedObjs := deepcopy.Copy(fixedUnits).([]*msg.StaticObjInfo)
	log.Debugf("===fixedObjs +%v===", fixedObjs)
	if err.Id != 0 {
		return
	}

	unitTimes := int32(math.Floor(float64(onHookEffectTime) / float64(levelConf.OutputTime)))

	log.Debugf("------unitTimes :+%v------", unitTimes)
	if unitTimes < 0 {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "data err"
		return
	}
	if unitTimes == 0 {
		return
	}
	techAddOutput := techEffectOutput(r, levelConf.TechAddResOutput)
	fixedObjs = fixedOutput(unitTimes, fixedObjs, techAddOutput)
	objs = append(objs, fixedObjs...)
	log.Debugf("===final fixedObjs %+v===", fixedObjs)

	randomUnits, _ := JsonToPb_StaticObjs(levelConf.OutputItemCfg, levelConf, levelConf.Id, err)
	randomObjs := deepcopy.Copy(randomUnits).([]*msg.StaticObjInfo)
	if err.Id != 0 {
		return
	}
	log.Debugf("===randomObjs  will %+v===", randomObjs)
	techRandomOutput := techEffectRandomOutput(r, levelConf.TechAddItemOutput)
	techRandomWeight := techEffectRandomWeight(r, levelConf.TechAddItemWeight)
	randomObjs = randomOutput(unitTimes, randomObjs, levelConf.OutputItemRate, techRandomOutput, techRandomWeight)
	objs = append(objs, randomObjs...)
	log.Debugf("===final randomObjs  %+v===", randomObjs)

	techUnits, _ := JsonToPb_StaticObjs(levelConf.Dropid, levelConf, levelConf.Id, err)
	if err.Id != 0 {
		return
	}
	techObjs := deepcopy.Copy(techUnits).([]*msg.StaticObjInfo)
	log.Debugf("===techObjs will %v===", techObjs)
	unitTimes = fixTimes(onHookEffectTime, levelConf.OutputTime)
	techObjs = techEffectDropPlace(r, levelConf.TechDropid, techObjs)
	log.Debugf("techEffectDropPlace is :%+v", techObjs)

	techObjs = techDrop(unitTimes, techObjs)
	objs = append(objs, techObjs...)

	log.Debugf("===final techObjs %v===", techObjs)
	return
}

func CalcQuickOnHook(r *msg.PlayerInfo, buildingInfo *msg.BuildingInfo, buildingConf *rawdata.BuildingConfig, err *msg.ErrorInfo) (objs []*msg.StaticObjInfo) {
	globalConf, ok := conf.GetGlobalConfig(1)
	if !ok {
		panic("globalConf is nil")
	}
	//

	log.Debugf("r.LevyHouses.FreeQuickOnHookTimes: %d", r.LevyHouses.FreeQuickOnHookTimes)
	curOnHookTimes := r.LevyHouses.QuickOnHookTimes
	log.Debugf("curOnHookTimes: %d", curOnHookTimes)
	if curOnHookTimes >= globalConf.QHUDefaultCount {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "quick onhook times max"
		return
	}

	if r.LevyHouses.FreeQuickOnHookTimes < globalConf.QHUFreeCount {
		r.LevyHouses.FreeQuickOnHookTimes = r.LevyHouses.FreeQuickOnHookTimes + 1
	}
	r.LevyHouses.QuickOnHookTimes = curOnHookTimes + 1
	onHookEffectTime := globalConf.QuickHookUpDuration

	objs = CalcOnHookIncome(r, onHookEffectTime, buildingInfo, buildingConf, err)
	if err.Id != 0 {
		return
	}
	return
}

func resetOnHookFreeTimes(r *msg.PlayerInfo, withSync bool) {
	tempErr := &msg.ErrorInfo{}
	ok := CheckOnHookCondition(r, tempErr)
	if !ok {
		return
	}

	if r.LevyHouses == nil {
		return
	}
	r.LevyHouses.QuickOnHookTimes = 0
	r.LevyHouses.FreeQuickOnHookTimes = 0
}

func LevyHouseRefresh(r *msg.PlayerInfo) {
	if r.LevyHouses == nil {
		return
	}
	//for novice guidance
	buildingId := mapBuildingTypeToId[int32(msg.BuildingType_BT_LevyHouse)]

	buildingInfo := GetBuilding(r, buildingId)
	if buildingInfo == nil {
		return
	}

	buildingConf, _ := conf.GetBuildingConfig(buildingId)
	if buildingConf == nil {
		return
	}

	err := &msg.ErrorInfo{}

	levelConf, _ := GetLevelConf(buildingConf, buildingInfo.Level, err)
	if err.Id != 0 {
		return
	}

	r.LevyHouses.OnHookIncomeTime = time3.Now().Unix() - int64(levelConf.OutputTime)
	r.LevyHouses.QuickOnHookTimes = 0
	r.LevyHouses.FreeQuickOnHookTimes = 0
	log.Debugf("LevyHouseRefresh:%v", r.LevyHouses)
}

func MergeObjs(player *msg.PlayerInfo, objs []*msg.StaticObjInfo, errInfo *msg.ErrorInfo) (addedObjs []*msg.ObjInfo) {

	tempObjs, _ := AddStaticObjs(player, objs, errInfo)
	if errInfo.Id != 0 {
		return
	}
	addedObjs = append(addedObjs, tempObjs...)
	addedObjs = MergeObjInfo(addedObjs)
	return
}

func GetQuickOnHookCost(quickOnHookTimes int32, err *msg.ErrorInfo) (staticObj []*msg.StaticObjInfo) {
	globalConf, ok := conf.GetGlobalConfig(1)
	if !ok {
		return
	}

	tempConsume := GetStepPriceConsume(globalConf.TaxhallQHUDPriceCfg, quickOnHookTimes, err)
	log.Debugf("quickhook consume %v", staticObj)

	if err.Id != 0 {
		return
	}

	staticObj = []*msg.StaticObjInfo{tempConsume}
	return
}
