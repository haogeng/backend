package game

import (
	"server/internal/log"
	"server/pkg/gen/conf"
	"server/pkg/gen/msg"
	"server/pkg/gen/rawdata"
	"strconv"
)

// 初始化战役信息
func InitWarCopy(r *msg.PlayerInfo, err *msg.ErrorInfo) {
	r2 := MustGetPlayerInfoExt(r)
	// 战役难度配置信息
	count := conf.CountWarInstanceConfigMap()
	for i := 1; i <= count; i++ {
		confData, _ := conf.GetWarInstanceConfig(int32(i))
		if !CheckConfData(confData, int32(i), &msg.ErrorInfo{}) {
			continue
		}
		if r2.WarDiffInfos != nil {
			_, ok := r2.WarDiffInfos[confData.Id]
			// 检查战役是否初始化
			if ok {
				// 已初始化
				continue
			}
		} else {
			r2.WarDiffInfos = map[int32]*msg.WarDiffInfo{}
		}
		// 判断难度开启条件
		funcData, _ := conf.GetFunctionConfig(confData.FuncId)
		if !CheckConfData(funcData, confData.FuncId, &msg.ErrorInfo{}) {
			continue
		}
		if !CheckConditions(r, funcData.UnlockCondition, &msg.ErrorInfo{}) {
			// 未开启
			continue
		}
		warDiffInfo := &msg.WarDiffInfo{
			WarId: confData.Id,
		}
		// 初始化序章
		warIntoned := confData.Map[0]
		m, _ := UnLockWarCopy(r, warIntoned, &msg.ErrorInfo{})
		if m == nil {
			continue
		}
		// 激活章节
		m.ChapterStatus = msg.ChapterStatus_CS_ING
		if warDiffInfo.WarCopyInfos == nil {
			warDiffInfo.WarCopyInfos = map[int32]*msg.WarCopyInfo{}
		}
		warDiffInfo.WarCopyInfos[m.ChapterId] = m
		// 初始化战役奖励
		warDiffInfo.StarStatus = initWarCopyInfoBoxReward(confData.Id)

		r2.WarDiffInfos[warDiffInfo.WarId] = warDiffInfo
	}
}

// 初始化宝箱奖励信息
func initWarCopyInfoBoxReward(warId int32) (starStatus map[int32]msg.RewardReceive) {
	confData := conf.GetWarInstanceRewardConfigByFilter(func(config *rawdata.WarInstanceRewardConfig) bool {
		if config.Type == warId {
			return true
		} else {
			return false
		}
	})
	starStatus = map[int32]msg.RewardReceive{}
	for _, v := range confData {
		starStatus[v.Id] = msg.RewardReceive_RR_NOReceive
	}
	return
}

// 检查据点是否通过
func checkStrongholdStatusById(r *msg.PlayerInfo, checkStrongholdId int32) bool {
	r2 := MustGetPlayerInfoExt(r)
	for _, wInfo := range r2.WarDiffInfos {
		for _, cInfo := range wInfo.WarCopyInfos {
			for _, sInfo := range cInfo.StrongholdInfos {
				if sInfo.StrongholdId != checkStrongholdId {
					continue
				}
				if sInfo.StrongholdStatus == msg.StrongholdStatus_SS_OPEN {
					return true
				}
			}
		}
	}
	return false
}

// 检查解锁新章节
func CheckWarCopyUnLock(r *msg.PlayerInfo, err *msg.ErrorInfo) (openChapterId int32, openHoldId int32) {
	r2 := MustGetPlayerInfoExt(r)
	// 战役难度配置信息
	count := conf.CountWarInstanceConfigMap()
	for i := 1; i <= count; i++ {
		warData, _ := conf.GetWarInstanceConfig(int32(i))
		if !CheckConfData(warData, int32(i), err) {
			continue
		}
		for _, chapterId := range warData.Map {
			chapterData, _ := conf.GetWarInstanceMapConfig(chapterId)
			if !CheckConfData(chapterData, chapterId, err) {
				continue
			}
			if r2.WarDiffInfos[warData.Id] == nil {
				continue
			}
			if r2.WarDiffInfos[warData.Id].WarCopyInfos == nil {
				continue
			}
			if r2.WarDiffInfos[warData.Id].WarCopyInfos[chapterId] != nil {
				continue
			}
			// 判断章节前置关卡是否通过
			if !checkStrongholdStatusById(r, chapterData.OpenStrongholdId) {
				// 未通过前置关卡
				continue
			}
			openChapterInfo, openHoldInfo := UnLockWarCopy(r, chapterId, err)
			if err.Id != 0 {
				continue
			}
			if openChapterInfo != nil {
				openChapterId = openChapterInfo.ChapterId
				r2.WarDiffInfos[warData.Id].WarCopyInfos[openChapterInfo.ChapterId] = openChapterInfo
			}
			if openHoldInfo != nil {
				openHoldId = openHoldInfo.StrongholdId
			}
		}
	}
	return
}

// 解锁指定章节
func UnLockWarCopy(r *msg.PlayerInfo, confId int32, err *msg.ErrorInfo) (openWarCopyInfo *msg.WarCopyInfo, openHoldInfo *msg.WarStrongHoldInfo) {
	confData, _ := conf.GetWarInstanceMapConfig(confId)
	if !CheckConfData(confData, confId, err) {
		return
	}
	// 据点信息
	shArray := map[int32]*msg.WarStrongHoldInfo{}
	for _, v := range confData.StrongholdId {
		holdData, _ := conf.GetWarInstanceStrongholdConfig(v)
		if !CheckConfData(holdData, v, err) {
			return
		}
		h := &msg.WarStrongHoldInfo{
			StrongholdId: holdData.Id,
		}
		// 据点状态
		strongholdStatus := msg.StrongholdStatus_SS_CLOSE
		// 根据条件判断据点开启状态
		for _, v := range holdData.OpenCondition {
			if v != 0 {
				// 判断前置据点是否通过
				if !checkStrongholdStatusById(r, v) {
					// 未通过前置关卡
					continue
				}
			}
			// 开启据点
			strongholdStatus = msg.StrongholdStatus_SS_ING
			openHoldInfo = h
		}
		h.StrongholdStatus = strongholdStatus
		shArray[holdData.Id] = h
	}
	openWarCopyInfo = &msg.WarCopyInfo{
		ChapterId:       confData.Id,
		FirstJoin:       true,
		ChapterStatus:   msg.ChapterStatus_CS_OPEN,
		StrongholdInfos: shArray,
	}
	return
}

// 构造战役列表返回
func BuildWarCopyInfoResults(r *msg.PlayerInfo, err *msg.ErrorInfo) []*msg.WarDiffResult {
	r2 := MustGetPlayerInfoExt(r)
	// 返回结果
	result := make([]*msg.WarDiffResult, len(r2.WarDiffInfos))
	index := 0
	for _, v := range r2.WarDiffInfos {
		confData, _ := conf.GetWarInstanceConfig(v.WarId)
		if !CheckConfData(confData, v.WarId, err) {
			continue
		}
		// 章节返回信息
		m := BuildWarDiffResult(r, v, err)
		if err.Id != 0 {
			continue
		}
		result[index] = m
		index++
	}
	return result
}

// 构建战役返回信息
func BuildWarDiffResult(r *msg.PlayerInfo, v *msg.WarDiffInfo, err *msg.ErrorInfo) (m *msg.WarDiffResult) {
	m = &msg.WarDiffResult{
		WarId:        v.WarId,
		StarStatus:   v.StarStatus,
		WarCopyInfos: []*msg.WarCopyResult{},
	}
	for _, warCopyInfo := range v.WarCopyInfos {
		war := BuildWarCopyResult(warCopyInfo, err)
		if err.Id != 0 {
			continue
		}
		m.WarCopyInfos = append(m.WarCopyInfos, war)
		// 检查开启新章节
		CheckWarCopyUnLock(r, err)
	}
	return
}

// 构建章节返回信息
func BuildWarCopyResult(warCopyInfo *msg.WarCopyInfo, err *msg.ErrorInfo) (warCopyResult *msg.WarCopyResult) {
	confData, _ := conf.GetWarInstanceMapConfig(warCopyInfo.ChapterId)
	if !CheckConfData(confData, warCopyInfo.ChapterId, err) {
		return
	}
	// 主线目标
	chapterTarget := map[int32]bool{}
	for pi, pv := range confData.Purpose {
		shinto, ok := warCopyInfo.StrongholdInfos[pv]
		if !ok {
			continue
		}
		if shinto.StrongholdStatus == msg.StrongholdStatus_SS_OPEN {
			chapterTarget[confData.PurposeDes[pi]] = true
		} else {
			chapterTarget[confData.PurposeDes[pi]] = false
		}
	}
	// 主线完成度
	var chapterScale int32 = 0
	for sid, sv := range warCopyInfo.StrongholdInfos {
		if sv.StrongholdStatus == msg.StrongholdStatus_SS_OPEN {
			// 基础据点数据
			holdData, _ := conf.GetWarInstanceStrongholdConfig(sid)
			if !CheckConfData(holdData, sid, err) {
				return
			}
			chapterScale += holdData.Progress
		}
	}
	warCopyResult = &msg.WarCopyResult{
		ChapterId:     warCopyInfo.ChapterId,
		FirstJoin:     warCopyInfo.FirstJoin,
		ChapterStatus: warCopyInfo.ChapterStatus,
		ChapterTarget: chapterTarget,
		ChapterScale:  chapterScale,
	}
	// 据点返回信息
	warCopyResult.ChapterStar, warCopyResult.CheckPoints = BuildWarStrongHoldResult(warCopyInfo, err)
	return
}

// 构建战役关卡返回信息
func BuildWarStrongHoldResult(warCopyInfo *msg.WarCopyInfo, err *msg.ErrorInfo) (chapterStar int32, checkPointResult map[int32]*msg.WarStrongHoldResult) {
	checkPointResult = map[int32]*msg.WarStrongHoldResult{}
	for k, v := range warCopyInfo.StrongholdInfos {
		confData, _ := conf.GetWarInstanceStrongholdConfig(k)
		if !CheckConfData(confData, k, err) {
			return
		}
		// 判断据点是否通过
		if v.StrongholdStatus == msg.StrongholdStatus_SS_OPEN {
			chapterStar = chapterStar + 1
		}
		infos, ok := checkPointResult[confData.Type]
		if !ok {
			infos = &msg.WarStrongHoldResult{
				HoldId:          confData.Type,
				StrongholdInfos: map[int32]*msg.WarStrongHoldInfo{},
			}
		}
		infos.StrongholdInfos[k] = v
		checkPointResult[confData.Type] = infos
	}
	return
}

// 结算战役副本据点
func WarCopyInfoBattle(r *msg.PlayerInfo, diffId int32, chapterId int32, strongholdId int32, err *msg.ErrorInfo) (addedObjs []*msg.ObjInfo, openChapterId int32, openHoldIds []int32) {
	warData, _ := conf.GetWarInstanceConfig(diffId)
	if !CheckConfData(warData, diffId, err) {
		return
	}
	chapterData, _ := conf.GetWarInstanceMapConfig(chapterId)
	if !CheckConfData(chapterData, chapterId, err) {
		return
	}
	holdData, _ := conf.GetWarInstanceStrongholdConfig(strongholdId)
	if !CheckConfData(holdData, strongholdId, err) {
		return
	}
	// 战斗奖励
	configData, _ := conf.GetInstanceConfig(holdData.BattleId)
	if !CheckConfData(configData, strongholdId, err) {
		return
	}
	// 击杀奖励
	rewards, _ := JsonToPb_StaticObjs(configData.KillReward, configData, configData.Id, err)
	if err.Id != 0 {
		return
	}
	addedObjs, _ = AddStaticObjs(r, rewards, err)
	if err.Id != 0 {
		return
	}

	r2 := MustGetPlayerInfoExt(r)
	// 更新据点状态
	r2.WarDiffInfos[diffId].WarCopyInfos[chapterId].ChapterStar = r2.WarDiffInfos[diffId].WarCopyInfos[chapterId].ChapterStar + 1
	r2.WarDiffInfos[diffId].WarCopyInfos[chapterId].StrongholdInfos[strongholdId].StrongholdStatus = msg.StrongholdStatus_SS_OPEN

	taskCondTypes := []msg.TaskCondType{msg.TaskCondType_TCT_MainChapter, msg.TaskCondType_TCT_MainChapterNormal, msg.TaskCondType_TCT_MainChapterHard}
	// 只考虑关卡的普通1星
	if holdData.SubType == 1 && strongholdId > r.MaxFiniMainLineAry[(diffId-1)] {
		// 刷新最大主线关卡及任务
		r.MaxFiniMainLineAry[(diffId - 1)] = strongholdId
		log.Debug(r.MaxFiniMainLineAry)
		// 关卡进度
		// 使用关卡编号
		RefreshTaskCount(r,
			taskCondTypes[diffId],
			int32(msg.TCT_MainChapterType_TCTM_Stronghold),
			0,
			int64(r.MaxFiniMainLineAry[(diffId-1)]))
	}

	if holdData.SubType == 1 && chapterData.FinalBattleStronghold == strongholdId {
		// 本章结束,更新任务
		RefreshTaskCount(r,
			taskCondTypes[diffId-1],
			int32(msg.TCT_MainChapterType_TCTM_Chapter),
			0,
			int64(chapterId))
	}

	// 本章最终据点
	if chapterData.FinalBattleStronghold == strongholdId {
		// 关闭章节
		r2.WarDiffInfos[diffId].WarCopyInfos[chapterId].ChapterStatus = msg.ChapterStatus_CS_OPEN
	}

	// 解锁新据点
	for _, wInfo := range r2.WarDiffInfos {
		for _, cInfo := range wInfo.WarCopyInfos {
			for _, sInfo := range cInfo.StrongholdInfos {
				if sInfo.StrongholdStatus == msg.StrongholdStatus_SS_ING || sInfo.StrongholdStatus == msg.StrongholdStatus_SS_OPEN {
					// 据点已解锁
					continue
				}
				// 基础信息
				confData, _ := conf.GetWarInstanceStrongholdConfig(sInfo.StrongholdId)
				if !CheckConfData(confData, sInfo.StrongholdId, err) {
					continue
				}
				// 判断条件是否已完成
				orderOver := true
				for _, v := range confData.OpenCondition {
					if v == 0 {
						continue
					}
					if !checkStrongholdStatusById(r, v) {
						// 前置条件未完成
						orderOver = false
						break
					}
				}
				if orderOver {
					// 开启新据点
					openHoldIds = append(openHoldIds, sInfo.StrongholdId)
					sInfo.StrongholdStatus = msg.StrongholdStatus_SS_ING
				}
			}
		}
	}
	// 解锁新章节
	openChapterId, openHoldId := CheckWarCopyUnLock(r, err)
	if openHoldId != 0 {
		// 添加开启据点
		openHoldIds = append(openHoldIds, openHoldId)
	}
	if openChapterId != 0 {
		// 进入据点列表,激活状态
		for _, dInfo := range r2.WarDiffInfos {
			for _, v := range dInfo.WarCopyInfos {
				if v.ChapterId == openChapterId {
					// 进行中
					v.ChapterStatus = msg.ChapterStatus_CS_ING
				} else {
					// 可进入
					v.ChapterStatus = msg.ChapterStatus_CS_OPEN
				}
			}
		}
	}
	return
}

// 主线副本战斗校验
func CheckWarCopyBattleStatus(r *msg.PlayerInfo, configId int32, err *msg.ErrorInfo) (checkResult bool, diffId int32, chapterId int32, strongholdId int32) {
	r2 := MustGetPlayerInfoExt(r)
Loop:
	for wId, wInfo := range r2.WarDiffInfos {
		for cId, cInfo := range wInfo.WarCopyInfos {
			for sId, sInfo := range cInfo.StrongholdInfos {
				if sInfo.StrongholdStatus != msg.StrongholdStatus_SS_ING {
					// 据点已通过
					continue
				}
				// 据点基础数据
				holdData, _ := conf.GetWarInstanceStrongholdConfig(sId)
				if !CheckConfData(holdData, sId, err) {
					break Loop
				}
				// 战力校验
				formationPowerSum := SelectTroopPowerSumByFormation(r, msg.FormationType_FT_WarChapter)
				if formationPowerSum < holdData.NeedPower {
					errMsg := "上阵队伍战力不足，请提升至" + strconv.FormatInt(int64(holdData.NeedPower), 10) + "再来挑战"
					err.Id, err.DebugMsg = 2305, errMsg
					return
				}
				if holdData.BattleId == configId {
					// 战斗类型相同,战斗ID相同。验证通过
					checkResult = true
					diffId = wId
					chapterId = cId
					strongholdId = sId
					break Loop
				}
			}
		}
	}
	return
}

// 修改用户的战役副本进度
func ModifyWarCopyByPlayerInfo(r *msg.PlayerInfo, warDiffId int32, chapterId int32, strongholdId int32, err *msg.ErrorInfo) {
	confData, _ := conf.GetWarInstanceConfig(warDiffId)
	if !CheckConfData(confData, warDiffId, err) {
		return
	}
	r2 := MustGetPlayerInfoExt(r)
	// 清空用户主线副本进度
	r2.WarDiffInfos[warDiffId].WarCopyInfos = map[int32]*msg.WarCopyInfo{}
	// 根据章节和据点重新设置进度
	for i := confData.Map[0]; i <= chapterId; i++ {
		// 解锁章节
		m, _ := UnLockWarCopy(r, i, err)
		if err.Id != 0 {
			continue
		}
		r2.WarDiffInfos[warDiffId].WarCopyInfos[m.ChapterId] = m
		// 结算据点
		for k, _ := range r2.WarDiffInfos[warDiffId].WarCopyInfos[m.ChapterId].StrongholdInfos {
			if k >= strongholdId {
				continue
			}
			WarCopyInfoBattle(r, warDiffId, i, k, err)
		}
	}
	return
}

// 获取宝箱奖励
func GetWarBoxReward(r *msg.PlayerInfo, warId int32, boxId int32, err *msg.ErrorInfo) (addedObjs []*msg.ObjInfo) {
	rewardData, _ := conf.GetWarInstanceRewardConfig(boxId)
	if !CheckConfData(rewardData, boxId, err) {
		return
	}
	if CheckRewardStatusByBoxId(r, warId, boxId, rewardData, err) {
		// 宝箱奖励
		rewards, _ := JsonToPb_StaticObjs(rewardData.Reward, rewardData, rewardData.Id, err)
		if err.Id != 0 {
			return
		}
		addedObjs, _ = AddStaticObjs(r, rewards, err)
		if err.Id != 0 {
			return
		}
		r2 := MustGetPlayerInfoExt(r)
		r2.WarDiffInfos[warId].StarStatus[boxId] = msg.RewardReceive_RR_INReceive
	}
	return
}

// 检查宝箱可领取状态
func CheckRewardStatusByBoxId(r *msg.PlayerInfo, warId int32, boxId int32, rewardData *rawdata.WarInstanceRewardConfig, err *msg.ErrorInfo) (result bool) {
	r2 := MustGetPlayerInfoExt(r)
	warDiffInfo, ok := r2.WarDiffInfos[warId]
	if !ok {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "战役未开启,无法领取奖励"
		return
	}
	if warDiffInfo.StarStatus[boxId] == msg.RewardReceive_RR_INReceive {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "宝箱已领取"
		return
	}
	warData, _ := conf.GetWarInstanceConfig(warDiffInfo.WarId)
	if !CheckConfData(warData, warDiffInfo.WarId, err) {
		return
	}
	// 战役当前进度
	var warCurStar int32
	for _, v := range warDiffInfo.WarCopyInfos {
		warCurStar = warCurStar + v.ChapterStar
	}
	if warCurStar < rewardData.NeedStar {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "战役进度不足,无法领取奖励"
		return
	}
	result = true
	return
}

// 首次进入章节
func UpdateWarFirstJoin(r *msg.PlayerInfo, warId int32, chapterId int32, err *msg.ErrorInfo) {
	r2 := MustGetPlayerInfoExt(r)
	if r2.WarDiffInfos == nil {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "战役信息未开启"
		return
	}
	warDiffInfo, ok := r2.WarDiffInfos[warId]
	if !ok {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "战役信息未找到"
		return
	}
	warCopyInfo, ok := warDiffInfo.WarCopyInfos[chapterId]
	if !ok {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "战役章节未找到"
		return
	}
	warCopyInfo.FirstJoin = false
	return
}

// 刷新主线副本宝箱可领取红点
func RefreshWarCopyBoxRedPoint(r *msg.PlayerInfo, notify bool) {
	r2 := MustGetPlayerInfoExt(r)
	// 只有0和1区别！
	curCount := int32(0)
	for warId, w := range r2.WarDiffInfos {
		for boxId, v := range w.StarStatus {
			if v == msg.RewardReceive_RR_INReceive {
				continue
			}
			rewardData, _ := conf.GetWarInstanceRewardConfig(boxId)
			if !CheckConfData(rewardData, boxId, &msg.ErrorInfo{}) {
				return
			}
			if CheckRewardStatusByBoxId(r, warId, boxId, rewardData, &msg.ErrorInfo{}) {
				curCount = 1
				break
			}
		}
	}

	_, rp := FindRedPointAnyway(r, &msg.RedPoint{
		Type: msg.RedPointType_RPT_WarCopyBox,
	})

	if rp.Count != curCount {
		rp.Count = curCount
	}
	if notify {
		NotifyRedPoint(r, rp)
	}
}

// 用于筛选数据，并推送给前端
// 0模式算一种类型，flag按类型处理，不是位标记 0:normal 1:recruit
func GetWarInfosByFlag(r *msg.PlayerInfo) map[int32]*msg.WarDiffInfo {
	ret := make(map[int32]*msg.WarDiffInfo)
	r2 := MustGetPlayerInfoExt(r)
	for k, v := range r2.WarDiffInfos {
		ret[k] = v
	}
	return ret
}
