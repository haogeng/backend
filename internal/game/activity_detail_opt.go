package game

import (
	"server/internal/game/activity"
	"server/internal/log"
	"server/pkg/gen/msg"
)

// 具体活动细节
type ActivityDetailOpt interface {
	InitDetail(r *msg.PlayerInfo)

	ShouldGetDetail(r *msg.PlayerInfo) (detail interface{})

	GetConfig(err *msg.ErrorInfo) (confData interface{})

	// 根据时间刷新一下状态
	TryUpdateActivityByTime(r *msg.PlayerInfo)

	// 判断根据完成即结束去尝试关闭
	TryCloseByDirectEnd(r *msg.PlayerInfo)

	// 尝试结束的时候，补发未领取的邮件
	TrySendSupplyRewards(r *msg.PlayerInfo)

	Close(r *msg.PlayerInfo)

	TryClearWhenClosed(r *msg.PlayerInfo)
}

func ActivityOpt(a *msg.ActivityData) ActivityDetailOpt {
	au := activity.GetMgr().GetUnit(a.Id)
	switch au.Config.Type {
	case int32(msg.ActivityType_AT_DailySign):
		{
			return (*ActivityDetailSignOpt)(a)
		}

	case int32(msg.ActivityType_AT_DailyQuest):
		{
			return (*ActivityDetailQuestOpt)(a)
		}

	default:
		log.Errorf("unknown activityData %v", a)
	}

	return (*ActivityDetailDummyOpt)(a)
}
