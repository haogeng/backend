package internal

import (
	"server/internal/log"
	"server/pkg/xwatcher"
)

var Watcher *xwatcher.Watcher

func MustInitWatcher() {
	if Config.EtcdConf {
		Watcher = xwatcher.MustNewEtcdWatcher(Config.EtcdEndpoint, &log.WrapperLogger{})
	} else {
		Watcher = xwatcher.MustNewFileWatcher(&log.WrapperLogger{})
	}
}
