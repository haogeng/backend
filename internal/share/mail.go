package share

import (
	"fmt"
	"server/internal/gconst"
	"server/internal/log"
	"server/pkg/cache"
	"server/pkg/dlock"
	"server/pkg/gen/msg"
	"server/pkg/gen_redis/redisdb"
)

func keyOfPlayerShareMail(id uint64) string {
	return cache.GetLockKeyFor(redisdb.LockKeyPostfixOfPlayerShareMail(id))
}

// 有极小概率会丢，可以接受
func PopAllMailFromShare(id uint64) (ret []*msg.MailItem) {
	if id == 0 {
		return
	}

	// 一些性能优化，延迟获取锁
	rs := mustGetPlayerShareMail(id)
	if len(rs.MailBuffer) == 0 {
		return
	}

	key := keyOfPlayerShareMail(id)
	lockValue, ok := dlock.AcquireLockWithTimeoutX(key, gconst.DLockTtl, gconst.DLockGetTimeout)
	if !ok {
		return
	}
	defer dlock.ReleaseLockX(key, lockValue)
	// 重新获取
	rs = mustGetPlayerShareMail(id)

	ret = rs.MailBuffer
	rs.MailBuffer = nil

	// store
	var c redisdb.PlayerShareMailCache
	c.MustUpdate(rs.Id, rs)

	return ret
}

func MustInsertMailIntoShare(id uint64, m *msg.MailItem) {
	if id == 0 {
		return
	}

	key := keyOfPlayerShareMail(id)
	lockValue, ok := dlock.AcquireLockWithTimeoutX(key, gconst.DLockTtl, gconst.DLockGetTimeout)
	if !ok {
		panic("get lock err")
	}
	defer dlock.ReleaseLockX(key, lockValue)

	rs := mustGetPlayerShareMail(id)
	if len(rs.MailBuffer) >= gconst.ShareMaxMailNums {
		log.Error("shareMail full")
		// skip!
		return
	}

	rs.MailBuffer = append(rs.MailBuffer, m)

	// store
	var c redisdb.PlayerShareMailCache
	c.MustUpdate(rs.Id, rs)
}

func mustGetPlayerShareMail(id uint64) *msg.PlayerShareMail {
	if id == 0 {
		return nil
	}

	// check id is valid?
	var cr redisdb.PlayerInfoCache
	r := cr.Find(id)
	if r == nil {
		log.Errorf("not found player %d", id)
		return nil
	}

	var c redisdb.PlayerShareMailCache
	r2 := c.Find(id)
	//log.Infof("PlayerShareMail %+v", r2)

	if r2 == nil {
		r2 = &msg.PlayerShareMail{}
		id2 := c.MustCreate(id, r2)
		if id2 != r2.Id || id2 != id {
			panic(fmt.Errorf("GetPlayerShareMail fail %d,%d,%d", id, r2.Id, id2))
		}
	}

	return r2
}
