package share

import (
	"fmt"
	"server/internal/gconst"
	"server/internal/log"
	"server/pkg/cache"
	"server/pkg/dlock"
	"server/pkg/gen/msg"
	"server/pkg/gen_redis/redisdb"
)

func keyOfPlayerShareClanId(id uint64) string {
	return cache.GetLockKeyFor(redisdb.LockKeyPostfixOfPlayerShareClanId(id))
}

func GetClanId(playerId uint64) *msg.PlayerShareClanId {
	if playerId == 0 {
		return nil
	}

	return mustGetPlayerShareClanId(playerId)
}

// 不提供回滚，，直接存档！！！
// 需要在其他的Update之前
// 设置公会Id
func SetClanId(playerId uint64, clanId uint64, oldClanId uint64) bool {
	if playerId == 0 {
		return false
	}

	key := keyOfPlayerShareClanId(playerId)
	lockValue, ok := dlock.AcquireLockWithTimeoutX(key, gconst.DLockTtl, gconst.DLockGetTimeout)
	if !ok {
		return false
	}
	defer dlock.ReleaseLockX(key, lockValue)

	rs := GetClanId(playerId)
	// 有隐患，但可以接受！最为严谨的方式是采用最后一次修改时间戳
	if rs.ClanId != oldClanId {
		return false
	}

	rs.ClanId = clanId

	var c redisdb.PlayerShareClanIdCache
	c.MustUpdate(rs.Id, rs)

	return true
}

func mustGetPlayerShareClanId(id uint64) *msg.PlayerShareClanId {
	if id == 0 {
		return nil
	}

	// check id is valid?
	var cr redisdb.PlayerInfoCache
	r := cr.Find(id)
	if r == nil {
		log.Errorf("not found player %d", id)
		return nil
	}

	var c redisdb.PlayerShareClanIdCache
	r2 := c.Find(id)
	//log.Infof("PlayerShareClanId %+v", r2)

	if r2 == nil {
		r2 = &msg.PlayerShareClanId{}
		id2 := c.MustCreate(id, r2)
		if id2 != id {
			panic(fmt.Errorf("GetPlayerShareClanId fail %d,%d,%+v", id, id2, r2))
		}
	}

	return r2
}
