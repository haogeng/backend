package gameex

import (
	"server/internal/clan"
	"server/internal/game"
	"server/pkg/gen/msg"
)

func FindPlayerBriefEx(id uint64) (ret *msg.PlayerBriefWithClan) {
	r1 := game.FindPlayerBrief(id)
	if r1 == nil {
		return nil
	}

	ret = &msg.PlayerBriefWithClan{
		Player: r1,
		Clan:   nil,
	}

	if r1.ClanId != 0 {
		r2 := clan.FindClanBrief(r1.ClanId)
		ret.Clan = r2
	}

	return
}

func FindPlayerBriefExs(ids []uint64) (ret []*msg.PlayerBriefWithClan) {
	playerBriefs := game.FindPlayerBriefs(ids)

	ret = make([]*msg.PlayerBriefWithClan, 0, len(playerBriefs))

	for i := 0; i < len(playerBriefs); i++ {
		if playerBriefs[i] == nil {
			continue
		}

		d := &msg.PlayerBriefWithClan{
			Player: playerBriefs[i],
			Clan:   nil,
		}
		d.Clan = clan.FindClanBrief(playerBriefs[i].ClanId)

		ret = append(ret, d)
	}
	return
}
