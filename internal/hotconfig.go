package internal

import (
	"encoding/json"
	"fmt"
	"server/internal/log"
	"server/pkg/xwatcher"
	"sync"
)

// 使用者应该采用热加载接口

// 热配置 游戏里的热配置都需要放到这里面
type hotConfig struct {
	// Gm的白名单
	WhiteIpsOfGm []string `json:"whiteipsofgm"`

	//# 一个服最大人数
	UserCountPerServer int `json:"usercountperserver"`

	// 线上游戏需要设置为false
	LogDebug bool `json:"logdebug"`

	inited bool

	// 当特定协议的处理有问题的时候，可以通过配置临时屏蔽
	// 路由黑名单
	// 形如：
	//"routerblackorder": {
	//    "msg.PlayerLogoutR": 1
	//  }
	RouterBlackOrder map[string]int `json:"routerblackorder"`
}

// 指针不能变化，有挂接接口
var sHotConfig *hotConfig = &hotConfig{
	WhiteIpsOfGm:       nil,
	UserCountPerServer: 0,
	LogDebug:           false,
	inited:             false,
	RouterBlackOrder:   nil,
}
var m sync.RWMutex

// 热配置
func GetHotConfig() *hotConfig {
	var ret *hotConfig

	m.RLock()
	ret = sHotConfig
	m.RUnlock()

	if ret == nil || !ret.inited {
		panic("Pls init hot config")
	}

	return ret
}

func setHotConfig(hc *hotConfig) {
	m.Lock()
	defer m.Unlock()
	*sHotConfig = *hc
	sHotConfig.inited = true
}

func MustInitHotConfig(path string, w *xwatcher.Watcher) {
	err := w.Register(path, sHotConfig)
	if err != nil {
		panic(err)
	}

	err = w.ForceReload(path)
	if err != nil {
		panic(err)
	}
}

func (p *hotConfig) OnDataLoaded(key string, base string, data []byte) {
	fmt.Printf("\nreload from key:%s, base:%s:\n%s\n", key, base, string(data))

	if len(data) == 0 {
		return
	}

	var temp hotConfig
	err := json.Unmarshal(data, &temp)
	if err != nil {
		log.Errorf("hotconfig unmarshal err: %v", err)
		return
	}

	setHotConfig(&temp)

	// 死逻辑
	log.EnableDebug = temp.LogDebug
}

// 不做锁保护了
func (p *hotConfig) GetWhiteIpsOfGm() []string {
	return p.WhiteIpsOfGm
}

func (p *hotConfig) GetRouterBlackOrder() map[string]int {
	return p.RouterBlackOrder
}
