package res

import (
	"bitbucket.org/funplus/ark"
)

type HandlerHttp struct{}

// 本地测试用，类似于命令行，通过本地url访问
const (
	KGroupName string = "res"
)

func InitHttpRouter(group ark.Router) {
	apiGroup := group.Group(KGroupName)
	// add middleware if needed
	apiGroup.Use()

	{
		apiGroup.GetPost("version", checkVersion)
	}
}
