package res

import (
	"bitbucket.org/funplus/ark"
	"net/http"
	"server/internal/log"
	"server/internal/util/lighthouse"
)

// AppVersion约定：
// 约定客户端版本： x.y.z，x为显示大版本，y为app版本，z为res版本
// Platform: ios android editor
type CheckVersionJsonR struct {
	LightHouseId string `json:"lighthouse_id"`
	AppVersion   string `json:"app_version"`
	FromType     string `json:"from"`
	PlatForm     string `json:"platform"`
}

// 客户端连服务器的第一个协议，走json
// 资源更新，由客户端自己判定
// 是否强更，由服务器判定
//http://127.0.0.1:28085/api/res/version?main=1
func checkVersion(ctx *ark.Context) error {
	req := &CheckVersionJsonR{}
	err := ctx.Bind(req)
	if err != nil {
		return err
	}

	log.Debugf("checkVersion: %+v", req)

	lh := lighthouse.GetLightHouse()

	forceUpdate := false
	errorInfo := ""
	tempErr := lighthouse.CheckAppVersion(req.AppVersion)
	if tempErr != nil {
		errorInfo = tempErr.Error()
		forceUpdate = true
	}

	res := lighthouse.BuildCheckVersionResponse(lh, errorInfo, forceUpdate)
	err = ctx.JSON(http.StatusOK, res)
	if err != nil {
		log.Error(err)
	}

	return err
}
