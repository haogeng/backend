package info

import (
	"bitbucket.org/funplus/ark"
	"bitbucket.org/funplus/golib/ip"
	"encoding/json"
	"fmt"
	"html/template"
	"net/http"
	"reflect"
	"server/internal"
	"server/internal/log"
	"server/internal/util/lighthouse"
	"server/pkg/gen/conf"
	"server/pkg/monitor"
	"server/pkg/version"
	"sort"
	"strconv"
)

const webListTemp = `
<html>
<head>
<title> {{.PageTitle}} </title>
</head>
<body>
<h1> {{.PageTitle}} </h1>
<h4> {{.Note}} </h4>
<ul style="list-style-type:none">
{{range .Datas}}
<li><a href="{{.HRef}}">{{.Name}}</a></li>
{{end}}
</ul>
</html>
`

type liData struct {
	Name string
	HRef string
}
type webListStruct struct {
	PageTitle string
	Note      string
	Datas     []liData
}

type Response struct {
	Alive bool `json:"alive"`
	// round-trip time (milliseconds)
	MysqlRtt        float64 `json:"mysql_rtt"`
	CacheRedisRtt   float64 `json:"cache_redis_rtt"`
	PersistRedisRtt float64 `json:"persist_redis_rtt"`
	HostName        string  `json:"host_name"`
	Ip              string  `json:"ip"`
	UUID            string  `json:"uuid"`
	Tag             string  `json:"tag"`
}

func getList(ctx *ark.Context) (err error) {
	datas := &webListStruct{
		PageTitle: "Info List",
		Datas: []liData{
			{Name: "health", HRef: "health"},
			{Name: "version", HRef: "version"},
			{Name: "lighthouse", HRef: "lighthouse"},
			{Name: "conflist - 配表信息", HRef: "conflist"},
		},
	}
	t := template.New("ul")
	tmpl, err := t.Parse(webListTemp)
	if err != nil {
		return
	}
	err = tmpl.Execute(ctx.Response().Writer, datas)
	if err != nil {
		return
	}

	return err
}

func getHealth(ctx *ark.Context) (err error) {
	res := &Response{
		Alive:           true,
		MysqlRtt:        monitor.GetAvgRtt(monitor.NameMysql),
		CacheRedisRtt:   monitor.GetAvgRtt(monitor.NameCacheRedis),
		PersistRedisRtt: monitor.GetAvgRtt(monitor.NamePersistRedis),
		HostName:        internal.HostName,
		Ip:              ip.GetLocalIP(),
		UUID:            internal.UUID,
		Tag:             internal.Tag,
	}

	resStr, err := json.MarshalIndent(res, "", "  ")
	err = ctx.String(http.StatusOK, string(resStr))
	if err != nil {
		log.Error(err)
	}

	return err
}

func getVersion(ctx *ark.Context) (err error) {
	versions := version.Info()
	versions += "\nTODO: resource version?"
	return ctx.String(http.StatusOK, versions)
}

func getLightHouse(ctx *ark.Context) (err error) {
	l := lighthouse.GetLightHouse()
	s, err := json.MarshalIndent(l, "", "  ")
	if err != nil {
		return err
	}
	return ctx.String(http.StatusOK, string(s))
}

func getConfList(ctx *ark.Context) (err error) {
	var names []string
	for k, _ := range conf.FunctionMap {
		names = append(names, k)
	}
	sort.Strings(names)

	datas := &webListStruct{
		PageTitle: "Conf List",
		Note:      "备注：增删字段或增删表，需要程序配合才能生效",
		Datas:     []liData{},
	}

	for i := 0; i < len(names); i++ {
		datas.Datas = append(datas.Datas, liData{
			Name: strconv.Itoa(i+1) + ". " + names[i],
			HRef: "confdata?t=" + names[i],
		})
	}

	t := template.New("ul")
	tmpl, err := t.Parse(webListTemp)
	if err != nil {
		return
	}

	err = tmpl.Execute(ctx.Response().Writer, datas)
	if err != nil {
		return
	}

	return err
}

func getConfdata(ctx *ark.Context) (err error) {
	confName := ctx.QueryParam("t")
	m, ok := conf.FunctionMap[confName]
	if !ok {
		err = fmt.Errorf("not exist %s", confName)
		return
	}

	c := reflect.ValueOf(m).Call(nil)
	c2 := c[0].Interface()

	s, err := json.MarshalIndent(c2, "", "  ")
	if err != nil {
		return err
	}
	return ctx.String(http.StatusOK, string(s))
}
