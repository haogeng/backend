package info

import (
	"bitbucket.org/funplus/ark"
)

type HandlerHttp struct{}

// 本地测试用，类似于命令行，通过本地url访问
const (
	KGroupName string = "info"
)

func InitHttpRouter(group ark.Router) {
	apiGroup := group.Group(KGroupName)
	// add middleware if needed
	apiGroup.Use()
	{
		apiGroup.GetPost("list", getList)
		apiGroup.GetPost("health", getHealth)
		apiGroup.GetPost("version", getVersion)
		apiGroup.GetPost("lighthouse", getLightHouse)
		apiGroup.GetPost("conflist", getConfList)
		apiGroup.GetPost("confdata", getConfdata)
	}
}
