// 改部分代码的初始部分，应该由模板生成
// 根目录handlers/internal/，，其中没一个子目录对应一个微服务。目录下的为该服务的api集合
package example

import (
	"bitbucket.org/funplus/arkmid/auth"
	"context"
	"github.com/dgrijalva/jwt-go"
	"server/pkg/fungin/xmiddleware"

	"bitbucket.org/funplus/sandwich/current"
	"server/internal/log"
	"server/pkg/gen/msg"
)

func (*HandlerHttp) Example2(ctx context.Context, req *msg.Example2Req) (*msg.Example2Res, error) {
	log.Debugf("req Example2 %v", req)
	log.Debugf("req meta %v", current.MustGetIncomingMetadata(ctx))
	log.Debugf("res meta %v", current.MustGetOutgoingMetadata(ctx))

	//TODO get playerId from device
	playerId := uint64(1)

	tokenClaims := &xmiddleware.TokenClaims{
		StandardClaims: jwt.StandardClaims{},
		UserId:         playerId,
	}
	accessToken, _ := auth.GetToken(jwt.SigningMethodHS256, tokenClaims, xmiddleware.AuthConfigSigningKey)
	log.Debug(accessToken)

	res := &msg.Example2Res{}
	return res, nil
}
