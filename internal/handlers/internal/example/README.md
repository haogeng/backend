# 服务定义样例
1. 该部分代码的初始部分，应该由模板生成
2. 根目录handlers/internal/
3. 其中每一个子目录对应一个微服务。比如example。
4. example/router.go 负责提供http服务注册，同时需要在internal.http中手动调用注册
5. example/example1 为具体api定义。约定没一个api在service/example.proto定义，同时此处的handler对应一个单独的文件。

# 协议数据流备忘
> call exampleDirect
  Req packet:{"rawAny":{"uri":"msg.Example1Req","raw":"CPgGEgNnb2Q=","passThrough":"pass-1"}}
  Req msg:{"id":888,"name":"god"}
  Send header:map[Content-Type:[application/x-protobuf] X-Fun-User-Token:[TODO: token]]
> code:200 
  Res packet:{"rawAny":{"uri":"msg.Example1Res","raw":"COgHEg5oZXJlIGlzIGVycnN0cg==","passThrough":"pass-1"}}
  Res msg: {"err":1000,"errStr":"here is errstr"}
  Recv header:map[Content-Length:[49] Content-Type:[application/x-protobuf] Date:[Thu, 12 Mar 2020 07:41:30 GMT]]

# 典型错误类型
1. url 地址错误
> netutils.ErrorResponse
> 1, router not found
2. proto uri 错误
> netutils.ErrorResponse
> 1, "codec: proto get a unKnown request: "

