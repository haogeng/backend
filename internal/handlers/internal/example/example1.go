package example

import (
	"bitbucket.org/funplus/arkmid/auth"
	"context"
	"github.com/dgrijalva/jwt-go"
	"server/pkg/fungin/xmiddleware"

	"bitbucket.org/funplus/sandwich/current"
	"server/internal/log"
	"server/pkg/gen/msg"
)

// 每个微服务api单独一个文件
func (*HandlerHttp) Example1(ctx context.Context, req *msg.Example1Req) (*msg.Example1Res, error) {
	//TODO get playerId from device
	playerId := uint64(1)

	tokenClaims := &xmiddleware.TokenClaims{
		StandardClaims: jwt.StandardClaims{},
		UserId:         playerId,
	}
	accessToken, _ := auth.GetToken(jwt.SigningMethodHS256, tokenClaims, xmiddleware.AuthConfigSigningKey)

	log.Debug(accessToken)

	res := &msg.Example1Res{
		Err:    1000,
		ErrStr: "no",
		Token:  "token fo res",
	}

	// 测试在一个响应里同步的发送多个协议给客户端
	n1 := &msg.Example1ResSubNotifyA{
		Content1: "c1",
		Content2: "c2",
	}
	current.MustAppendAdditionalOutgoingMessage(ctx, n1)

	n2 := &msg.Example1ResSubNotifyB{
		Content1: 1,
		Content2: 2,
	}
	current.MustAppendAdditionalOutgoingMessage(ctx, n2)

	//NOTE!!! 暂时底层router.PacketWith 有bug，多消息发送有问题
	return res, nil
}
