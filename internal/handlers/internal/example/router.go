package example

import (
	"bitbucket.org/funplus/ark"
	"server/pkg/gen/service"
)

// 需要实现对应的微服务接口
type HandlerHttp struct{}

const (
	// 微服务(集合)名
	KGroupName string = "example"
)

func InitHttpRouter(group ark.Router) {
	apiGroup := group.Group(KGroupName)
	apiGroup.Use(
	////解析request id，字符串存储到context
	//middleware.ParseRequestId(),
	////当前request是否被请求并缓存response
	//middleware.CheckResponseCached(middleware.NewMemoryResponseCache(time.Duration(5)*time.Minute)),
	////检测请求时序是否ok
	//middleware.CheckSequence(middleware.NewMemoryRequestSequencer(time.Duration(5)*time.Minute)),
	)

	{
		service.RegisterExampleServiceHttpHandler(apiGroup, &HandlerHttp{})
	}
}
