package login

import (
	"bitbucket.org/funplus/arkmid/auth"
	"context"
	"database/sql"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"server/internal"
	"server/internal/game"
	"server/internal/global_info"
	"server/internal/log"
	"server/internal/util/tasdk"
	"server/internal/util/time3"
	"server/pkg/cache"
	"server/pkg/dbutil"
	"server/pkg/dlock"
	"server/pkg/fungin/xmiddleware"
	"server/pkg/gen/msg"
	"server/pkg/gen_db/dao"
	"server/pkg/xsnowflake"
	"strings"
	"time"
)

// 得到锁的目标体
func getWhichStr(deviceId string) string {
	if len(deviceId) == 0 {
		panic("len fo device id should not be 0")
	}
	return "device_" + deviceId
}

func checkDeviceId(deviceId string, err *msg.ErrorInfo) bool {
	l := len(deviceId)
	if l == 0 {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "pls upload device id"
		return false
	}
	if l > 256 {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "the length of device of is too long"
		return false
	}
	return true
}

// 登录和创建用户使用device id(或者客户端发上来的token)做分布式锁
// 可以解决同一个设备的连续登录导致的多次创建账号的问题
func (*HandlerHttp) Login(ctx context.Context, req *msg.LoginR) (res *msg.LoginA, err error) {
	err = nil
	res = &msg.LoginA{
		Err:              &msg.ErrorInfo{},
		ServerUtc:        time3.Now().Unix(),
		ServerZoneOffset: time3.ZoneOffset(),
	}

	accountMgr := dao.Account

	// 第三方登录态验证
	if len(req.SessionKey) != 0 {
		checkRs := tasdk.CheckSession(internal.Config.SdkConfig.Dest, internal.Config.SdkConfig.Debug, req.SessionKey)
		if int(checkRs["status"].(float64)) == 1 {
			// Valid Session
			fpid := checkRs["data"].(map[string]interface{})["fpid"].(string)
			if fpid != req.DeviceId {
				// Invalid Fpid
				res.Err.Id, res.Err.DebugMsg = 4001, "您输入的账号无效，请重新输入"
				return
			}
			req.DeviceId = "fpid_" + fpid
		} else {
			// Invalid Session
			res.Err.Id, res.Err.DebugMsg = 4002, "登录验证失败，请稍后再试"
			return
		}
	}
	// check device id
	if !checkDeviceId(req.DeviceId, res.Err) {
		return
	}

	which := getWhichStr(req.DeviceId)
	lockKey := cache.GetLockKeyFor(which)
	if !dlock.AcquireLock(lockKey, time.Second*30) {
		res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "get lock failed"
		return
	}
	defer dlock.ReleaseLock(lockKey)

	// 有则加载，无则创建
	// device -> account -> player
	// TODO 暂时只处理设备登录。后续三方登录，需要做绑定关系
	escapedDeviceName := dbutil.Escape(req.DeviceId)

	var accountRecord *msg.Account

	// 不能走缓存！
	deviceRecord, err := getDeviceByName(escapedDeviceName)
	if err == sql.ErrNoRows {
		// new account for this device
		accountRecord, deviceRecord = createDevice(escapedDeviceName, req)
		// reset err!
		err = nil
	} else if err != nil {
		log.Errorf("LoginErr %v\n", err)
		res.Err.Id, res.Err.DebugMsg = -1, "sql failed"
		return
	} else {
		accountRecord, err = accountMgr.Find(deviceRecord.AccountId)
		if err != nil {
			log.Errorf("LoginErr2 %v\n", err)
			res.Err.Id, res.Err.DebugMsg = -1, "sql failed2"
			return
		}
	}

	// player info...
	// if no last player, then created a new player in current server
	// else use last player
	if accountRecord.LastPlayerId == 0 {
		if !req.NeedTwoStepCreatePlayer {
			// direct create player
			res.Err.Id, res.Err.DebugMsg = createPlayerForAccount(accountRecord, int32(msg.CivleType_CT_Roma), deviceRecord.Name)
			if res.Err.Id != 0 {
				return
			}
		}
	}

	accessToken := ""
	if accountRecord.LastPlayerId != 0 {
		accessToken = updateTokenInfo(accountRecord.LastPlayerId)
	}

	// uid is playerId
	res.Uid = accountRecord.LastPlayerId
	res.Token = accessToken
	res.AccountId = accountRecord.Id
	res.DeviceId = req.DeviceId

	return
}

func (*HandlerHttp) TwoStepCreatePlayer(ctx context.Context, req *msg.TwoStepCreatePlayerR) (res *msg.TwoStepCreatePlayerA, err error) {
	res = &msg.TwoStepCreatePlayerA{
		Err: &msg.ErrorInfo{},
	}

	// 检查文明
	if !(req.CivilId > int32(msg.CivleType_CT_None) && req.CivilId < int32(msg.CivleType_CT_Max)) {
		res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "civil id err"
		return
	}

	if req.AccountId == 0 {
		res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "account id should not be 0"
		return
	}

	if !checkDeviceId(req.DeviceId, res.Err) {
		return
	}

	which := getWhichStr(req.DeviceId)
	lockKey := cache.GetLockKeyFor(which)
	if !dlock.AcquireLock(lockKey, time.Second*30) {
		res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "get lock failed"
		return
	}
	defer dlock.ReleaseLock(lockKey)

	// 检查账号存在否
	accountMgr := dao.Account
	accountRecord, err := accountMgr.Find(req.AccountId)
	if err != nil || accountRecord == nil {
		log.Errorf("LoginErr3 %v\n", err)
		res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "not found account"
		return
	}

	// 已有角色
	if accountRecord.LastPlayerId != 0 {
		res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "the account has player already"
		return
	}

	// 创建新角色
	res.Err.Id, res.Err.DebugMsg = createPlayerForAccount(accountRecord, req.CivilId, req.DeviceId)
	if res.Err.Id != 0 {
		return
	}

	// 更新token
	accessToken := ""
	if accountRecord.LastPlayerId != 0 {
		accessToken = updateTokenInfo(accountRecord.LastPlayerId)
	} else {
		// should never go here
		res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "last player id should not be 0"
		return
	}

	res.Token = accessToken
	res.Uid = accountRecord.LastPlayerId

	return
}

func getDeviceByName(escapedName string) (pb *msg.Device, err error) {
	mgr := dao.Device
	tableName := mgr.TableName()
	sqlStr := fmt.Sprintf("select %s from %s where name='%s' limit 1",
		strings.Join(mgr.Columns(), ","),
		tableName,
		escapedName)
	tmp := &msg.Device{}
	err = mgr.GetBySql(tmp, sqlStr, nil)
	if err == nil {
		pb = tmp
	}

	if pb == nil {
		err = sql.ErrNoRows
	}
	return
}

func createPlayerForAccount(accountRecord *msg.Account, civilId int32, deviceName string) (err msg.Ecode, errStr string) {
	accountMgr := dao.Account

	// check civil id outside

	serverId, curUserCount, _ := global_info.GetGlobalInfo().DispatchServerID()
	fakeUserId := ((1000+serverId)*100000 + curUserCount) ^ (3942658)

	playerRecord := game.CreateNewPlayer(serverId, fakeUserId, civilId, deviceName)
	if playerRecord == nil {
		log.Errorf("MustCreate player failed\n")
		err, errStr = msg.Ecode_INVALID, "create player failed"
		return
	}

	accountRecord.LastPlayerId = playerRecord.Id
	accountRecord.PlayerInfo = append(accountRecord.PlayerInfo,
		&msg.PlayerServerInfo{
			PlayerId: playerRecord.Id,
			ServerId: serverId,
		})
	tempErr := accountMgr.Update(accountRecord.Id, accountRecord)
	if tempErr != nil {
		err, errStr = msg.Ecode_INVALID, "db err"
	}
	return
}

func createDevice(escapedDeviceName string, req *msg.LoginR) (accountRecord *msg.Account, deviceRecord *msg.Device) {
	accountMgr := dao.Account
	deviceMgr := dao.Device

	accountId := xsnowflake.NextIdUint64()
	accountRecord = &msg.Account{
		Id:           accountId,
		PlayerInfo:   nil,
		LastPlayerId: 0,
		Main:         req.Version.Main,
		Minor:        req.Version.Minor,
		Build:        req.Version.Build,
		Platform:     req.Version.Platform,
		Language:     req.Version.Language,
	}
	id, err := accountMgr.Create(accountId, accountRecord)
	log.Info(id, err)

	// bind device & account
	deviceId := xsnowflake.NextIdUint64()
	deviceRecord = &msg.Device{
		Id:        deviceId,
		Name:      escapedDeviceName,
		AccountId: accountId,
	}

	lastInsertId, err := deviceMgr.Create(deviceId, deviceRecord)
	if err == nil {
		if deviceRecord.Id != lastInsertId {
			log.Error("id unequal")
		}
		log.Debugf("new insert id : %d\n", lastInsertId)
	} else {
		panic(fmt.Errorf("device create err:%v", err))
	}

	return accountRecord, deviceRecord
}

func updateTokenInfo(uid uint64) (accessToken string) {
	if uid == 0 {
		log.Error("uid should not be 0")
		return
	}

	// make jwt & store
	//accessToken, _ := ginm.JwtToken(accountRecord.Id, 0, nil)
	// 使用角色Id，理论上可以一个账号在多个设备上玩，只要是不用同一个角色就可以
	// TODO 当玩家在切换角色的时候，需要重新登录游戏
	tokenClaims := &xmiddleware.TokenClaims{
		StandardClaims: jwt.StandardClaims{
			// this is token uuid!
			Id: fmt.Sprintf("%d", time3.Now().Unix()),
		},
		UserId: uid,
	}
	accessToken, err := auth.GetToken(jwt.SigningMethodHS256, tokenClaims, xmiddleware.AuthConfigSigningKey)
	if err != nil {
		return
	}
	xmiddleware.StoreTokenUuid(tokenClaims)

	log.Debugf("tokenClaim:%v accessToken:%v", tokenClaims, accessToken)

	// 清空缓存
	cache.ClearOnRelogin(uid)

	return
}
