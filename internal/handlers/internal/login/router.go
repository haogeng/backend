package login

import (
	"bitbucket.org/funplus/ark"
	internal2 "server/internal"
	"server/pkg/fungin/xmiddleware"
	"server/pkg/fungin/xmiddleware/whiteips"
	"server/pkg/gen/service"
)

type HandlerHttp struct{}

const (
	KGroupName string = "login"
)

func InitHttpRouter(group ark.Router) {
	apiGroup := group.Group(KGroupName)
	apiGroup.Use(
		xmiddleware.CheckVersion(),

		//解析token，struct存储到context
		//ginm.JwtTokenFromHeader(),
		xmiddleware.ArkUnmarshal(),

		whiteips.SkipByProtoName(internal2.GetHotConfig()),

		// 底层透传
		//xmiddleware.EchoSequence(),
	)

	{
		service.RegisterLoginServiceHttpHandler(apiGroup, &HandlerHttp{})
	}
}
