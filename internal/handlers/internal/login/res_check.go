package login

import (
	"context"
	"fmt"

	"server/internal"
	"server/pkg/gen/msg"
)

func (*HandlerHttp) ResCheck(ctx context.Context, req *msg.ResCheckR) (res *msg.ResCheckA, err error) {
	res = &msg.ResCheckA{
		Err: &msg.ErrorInfo{},
	}

	cMain := int32(internal.Config.VersionConfig.ClientMainVer)
	cMinor := int32(internal.Config.VersionConfig.ClientMinorVer)

	if req.Version.Main < cMain ||
		(req.Version.Main == cMain && req.Version.Minor < cMinor) {
		res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "need update version"
		newVerInfoStr := fmt.Sprintf("v%d.%d", cMain, cMinor)
		res.Err.Params = append(res.Err.Params, newVerInfoStr)
		return
	}

	res.ResVersion = "todo by yy"
	res.ResCloudUrl = "http://todo.com"
	res.ResCdnUrl = "http://todo.com"

	return
}
