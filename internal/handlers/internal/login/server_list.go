package login

import (
	"context"
	"server/internal"
	"server/pkg/gen/msg"
	"server/pkg/service_mgr"
	"sort"
	"strings"
)

func (*HandlerHttp) ServerList(ctx context.Context, req *msg.ServerListR) (res *msg.ServerListA, err error) {
	//ignore platform & page
	//req.Platform
	res = &msg.ServerListA{
		Info: nil,
		// ignore
		UserServer: nil,
	}

	retVal, err := service_mgr.GetValues(internal.Tag)
	// use config
	if err != nil {
		err = nil

		id := int32(1)

		serverInfos := internal.Config.ServerList.ServerInfos

		for _, v := range serverInfos {
			msgServer := &msg.ServerInfo{
				ServerId:   id,
				ServerName: v.Name,
				Url:        v.Url,
				Status:     1,
			}
			id++
			res.Info = append(res.Info, msgServer)
		}

		return res, nil
	} else {
		// use etcd
		keys := []string{}
		for k, _ := range retVal {
			keys = append(keys, k)
		}
		sort.Strings(keys)

		id := 0
		for _, k := range keys {
			v := retVal[k]
			serverName := strings.Replace(k, internal.Tag+"/", "", -1)

			id++
			msgServer := &msg.ServerInfo{
				ServerId:   int32(id),
				ServerName: serverName,
				Url:        v,
				Status:     1,
			}
			res.Info = append(res.Info, msgServer)
		}
	}

	return
}
