package local_test

import (
	"bitbucket.org/funplus/ark"
	"fmt"
	"log"
	"net/http"
	"server/internal/global_info"
	"server/pkg/gen/msg"
	"server/pkg/gen_redis/redisdb"
	"server/pkg/xsnowflake"
	"sync"
	"time"
)

func testDispatchServerId(ctx *ark.Context) error {
	fmt.Printf("testLock\n")

	conNums := 8

	var wg sync.WaitGroup
	var c redisdb.PlayerInfoCache
	// serverId -> userCount

	for i := 0; i < conNums; i++ {
		curNode := xsnowflake.MustInit(uint16(i), nil)
		wg.Add(1)
		go func() {
			defer func() {
				wg.Done()
			}()

			for j := 0; j < 500; j++ {
				time.Sleep(time.Millisecond * 5)

				serverId, _, err := global_info.GetGlobalInfo().DispatchServerID()
				if err != nil {
					log.Printf("err %v\n", err)
				}
				//uid := uint64(xsnowflake.GetId())
				uid := uint64(curNode.Generate())
				r := c.Find(uid)
				if r == nil {
					r = &msg.PlayerInfo{
						Id:       uid,
						ServerId: serverId,
					}
					c.MustCreate(uid, r)

				} else {
					panic(fmt.Sprintf("user should unexist"))
				}
			}
		}()
	}

	wg.Wait()

	ctx.String(http.StatusOK, "testDispatchServerId ok, pls see db")
	return nil
}
