package local_test

import (
	"bitbucket.org/funplus/ark"
	internal2 "server/internal"
)

type HandlerHttp struct{}

// 本地测试用，类似于命令行，通过本地url访问
const (
	KGroupName string = "local"
)

func InitHttpRouter(group ark.Router) {
	if !internal2.Config.OpenLocalTest {
		return
	}

	apiGroup := group.Group(KGroupName)
	apiGroup.Use(
	//xmiddleware.CheckLocal(internal2.Config.Development),
	// 底层透传
	//xmiddleware.EchoSequence(),
	)

	{
		apiGroup.Get("dbpool", getDbPool)
		apiGroup.Get("testrespcache", testRespCache)
		apiGroup.Get("testratelimit", testRateLimit)
		apiGroup.Get("testlock", testLock)
		apiGroup.Get("testdispatchserverid", testDispatchServerId)
		apiGroup.Get("createPlayer", testCreatePlayer)
	}
}
