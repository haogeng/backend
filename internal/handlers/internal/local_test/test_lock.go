package local_test

import (
	"bitbucket.org/funplus/ark"
	"context"
	"fmt"
	"log"
	"net/http"
	"server/pkg/cache"
	"time"
)

type lockTest int

func (lt *lockTest) AcquireLock() bool {
	key := "lock:test:8888"

	ctx, _ := context.WithTimeout(context.Background(), time.Second*10)
	ok, err := cache.Redis.Cmd.SetNX(ctx, key, "1", time.Second*10).Result()
	if ok && err == nil {
		log.Printf("get lock ok\n")
		return true
	}

	log.Printf("get lock failed\n")
	return false
}

func (m *lockTest) ReleaseLock() {
	key := "lock:test:8888"
	log.Printf("del lock\n")
	ctx, _ := context.WithTimeout(context.Background(), time.Second*10)
	cache.Redis.Cmd.Del(ctx, key)
}

func testLock(ctx *ark.Context) error {
	fmt.Printf("testLock %s\n", ctx.ClientIP())

	var lt lockTest
	var ok = lt.AcquireLock()
	defer func() {
		if ok {
			go func() {
				// 模拟业务需要一定的时间
				time.Sleep(time.Second * 5)
				lt.ReleaseLock()
			}()
		}
	}()

	if ok {
		ctx.String(http.StatusOK, "testLock get lock ok")
	} else {
		ctx.String(http.StatusOK, "testLock get lock failed")
	}
	return nil
}
