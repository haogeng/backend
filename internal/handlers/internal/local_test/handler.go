package local_test

import (
	"bitbucket.org/funplus/ark"
	"bitbucket.org/funplus/arkmid/auth"
	"github.com/dgrijalva/jwt-go"
	"math/rand"
	"server/internal/game"
	"server/internal/gconst"
	"server/pkg/fungin/xmiddleware/arkmid_helper"
	"server/pkg/gen_redis/redisdb"

	"fmt"
	"net/http"
	"server/internal/log"
	"server/pkg/fungin/xmiddleware"
	"server/pkg/gen/msg"
	"server/pkg/gen_db/dao"
	"time"
)

func getDbPool(ctx *ark.Context) error {
	mgr := dao.TestState

	for i := 0; i < 100; i++ {
		id := uint64(i + 1)
		mgr.Delete(id)
		log.Infof("Delete id%d\n", id)
	}

	for i := 0; i < 100; i++ {
		id := uint64(i + 1)
		log.Debugf("Start id%d\n", id)

		d := &msg.TestState{
			Id:    uint64(i + 1),
			Alias: fmt.Sprintf("a%d", i+1),
		}
		_id, err := mgr.Create(id, d)
		if err != nil {
			log.Infof("id%d _id%d err:%v\n", id, _id, err)
		} else {
			log.Infof("Ok id%d\n", id)
		}
		time.Sleep(time.Second * 1 / 2)
	}

	return nil
}

func testRespCache(ctx *ark.Context) error {
	fmt.Printf("testRespCache")

	var testStr = "testCache"

	testId := uint64(1)
	tokenClaims := &xmiddleware.TokenClaims{
		StandardClaims: jwt.StandardClaims{},
		UserId:         testId,
	}
	tokenStr, _ := auth.GetToken(jwt.SigningMethodHS256, tokenClaims, xmiddleware.AuthConfigSigningKey)
	ctx.Set(xmiddleware.AuthKeyTokenFlag, tokenStr)

	c := arkmid_helper.NewRedisResponseCache(time.Minute * 5)
	c.Set(ctx, 888, []byte(testStr))

	_id, _b, _ok := c.Get(ctx)

	if _ok {
		fmt.Printf("_id: %s _data: %s\n", _id, string(_b))
	} else {
		fmt.Printf("no data\n")
	}
	return nil
}

func testRateLimit(ctx *ark.Context) error {
	fmt.Printf("testRateLimit")

	ctx.String(http.StatusOK, "testRateLimit")
	return nil
}

func testCreatePlayer(ctx *ark.Context) error {
	log.Debug("testCreatePlayer")

	r := game.CreateNewPlayer(888, rand.Int31()/100000, int32(msg.CivleType_CT_Roma), "fromTestCreatePlayer")

	var c redisdb.PlayerInfoCache
	r = c.Find(r.Id)
	if r == nil {
		return gconst.ErrNoUser
	}

	gold, _ := r.Currency[int32(msg.CurrencyId_CI_Gold)]
	r.Currency[int32(msg.CurrencyId_CI_Gold)] = gold + 1

	c.MustUpdate(r.Id, r)

	ctx.String(http.StatusOK, "testCreatePlayer")
	return nil
}
