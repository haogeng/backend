package clan

import (
	"context"
	"fmt"
	"github.com/go-redis/redis/v8"
	"server/internal/clan"
	"server/internal/game"
	"server/internal/gconst"
	"server/internal/log"
	"server/internal/share"
	"server/internal/util/time3"
	"server/pkg/cache"
	"server/pkg/dbutil"
	"server/pkg/dlock"
	"server/pkg/fungin/xmiddleware"
	"server/pkg/gen/conf"
	"server/pkg/gen/msg"
	"server/pkg/gen_redis/redisdb"
	"server/pkg/persistredis"
	"strconv"
)

const (
	//加入公会
	MailTypeJoin = 17
	//职位变更
	MailTypeAppoint = 18
	//拒绝加入
	MailTypeRejectJoin = 19
	//驱逐
	MailTypeExpel = 20
	//会长副会长管理VICE_PRESIDENT
	MailTypeAdmin           = 21
	PresidentLanguageId     = 120020
	VicePresidentLanguageId = 120021
	MemberLanguageId        = 120022
)

func (*HandlerHttp) GetClanList(ctx context.Context, req *msg.GetClanListR) (res *msg.GetClanListA, err error) {
	err = nil
	res = &msg.GetClanListA{
		Err: &msg.ErrorInfo{},
	}

	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}
	var c redisdb.PlayerInfoCache
	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}

	rs := share.GetClanId(r.Id)
	if rs == nil {
		err = gconst.ErrNoUserShare
		return
	}

	clanIds := clan.GetRecommend()
	//log.Debugf("clanIds is %+v", clanIds)
	clans := clan.GetClanInfo(clanIds)

	for _, v := range clans {
		if v.MemberCount == 0 {
			continue
		}
		v.PresidentBrief = game.FindPlayerBrief(v.PresidentId)
		clan.RemoveApplyList(v, r)
		v.Member = nil
		v.Ext = nil
		res.Info = append(res.Info, v)
	}
	res.OwnerClanId = rs.ClanId
	//log.Debugf("res.Info  %+v", res.Info)
	return
}

func (*HandlerHttp) GetClanInfo(ctx context.Context, req *msg.GetClanInfoR) (res *msg.GetClanInfoA, err error) {
	err = nil
	res = &msg.GetClanInfoA{
		Err: &msg.ErrorInfo{},
	}

	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}

	var c redisdb.PlayerInfoCache
	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}

	rs := share.GetClanId(r.Id)
	if rs == nil {
		err = gconst.ErrNoUserShare
		return
	}

	if rs.ClanId == 0 {
		res.Err.Id = msg.Ecode_INVALID
		return
	}

	key := clan.GetClanLockKey(rs.ClanId)
	lockValue, lockOk := dlock.AcquireLockX(key, gconst.DLockGetTimeout)
	if !lockOk {
		res.Err.Id, res.Err.DebugMsg = -1, "get clan lock failed"
		return
	}
	defer func() {
		if lockOk {
			dlock.ReleaseLockX(key, lockValue)
		}
	}()

	// TODO 验证数据一致性 或者在玩家登录中做
	clanInfo := clan.Find(rs.ClanId)
	if clanInfo == nil {
		res.Err.Id = msg.Ecode_INVALID
		return
	}
	now := time3.Now().Unix()
	diffTime := now - clanInfo.LastActiveTime
	clanInfo.LastActiveTime = now

	// save
	var clanInfoCache redisdb.ClanInfoCache
	clanInfoCache.MustUpdate(clanInfo.Id, clanInfo)

	memberStr := fmt.Sprintf("%d", clanInfo.Id)
	persistredis.Redis.ZIncrBy(clan.ClanCache, float64(diffTime), memberStr)

	clanInfo.PresidentBrief = game.FindPlayerBrief(clanInfo.PresidentId)
	res.Info = clanInfo

	return
}

func (*HandlerHttp) ClanMemberList(ctx context.Context, req *msg.ClanMemberListR) (res *msg.ClanMemberListA, err error) {
	err = nil
	res = &msg.ClanMemberListA{
		Err: &msg.ErrorInfo{},
	}

	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}

	var c redisdb.PlayerInfoCache
	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}

	rs := share.GetClanId(r.Id)
	if rs == nil {
		err = gconst.ErrNoUserShare
		return
	}

	clanInfo := clan.Find(req.Id)
	if clanInfo == nil {
		res.Err.Id = msg.Ecode_INVALID
		return
	}
	clanInfo.PresidentBrief = game.FindPlayerBrief(clanInfo.PresidentId)
	clanInfoMember := clan.MustGetClanInfoMember(clanInfo)
	if clanInfoMember == nil {
		res.Err.Id = msg.Ecode_INVALID
		return
	}

	for _, v := range clanInfoMember.MemberList {
		allActive := clan.MemberSevenDayContribution(v)

		playerBrief := game.FindPlayerBrief(v.PlayerId)
		memberInfo := &msg.MemberInfo{
			Brief:       playerBrief,
			Position:    msg.PositionType(v.Position),
			OnlineTime:  playerBrief.LastLogoutTime,
			ActiveValue: allActive,
		}
		res.MemberList = append(res.MemberList, memberInfo)
	}
	return
}

func (*HandlerHttp) ModifyClanInfo(ctx context.Context, req *msg.ModifyClanInfoR) (res *msg.ModifyClanInfoA, err error) {
	err = nil
	res = &msg.ModifyClanInfoA{
		Err: &msg.ErrorInfo{},
	}

	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}

	var c redisdb.PlayerInfoCache
	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}

	rs := share.GetClanId(r.Id)
	if rs == nil {
		err = gconst.ErrNoUserShare
		return
	}

	if rs.ClanId == 0 {
		res.Err.Id, res.Err.DebugMsg = 2015, "has no clan"
		return
	}

	key := clan.GetClanLockKey(rs.ClanId)
	lockValue, lockOk := dlock.AcquireLockX(key, gconst.DLockGetTimeout)
	if !lockOk {
		res.Err.Id, res.Err.DebugMsg = -1, "get clan lock failed"
		return
	}
	defer func() {
		if lockOk {
			dlock.ReleaseLockX(key, lockValue)
		}
	}()

	clanInfo := clan.Find(rs.ClanId)
	if clanInfo == nil {
		res.Err.Id, res.Err.DebugMsg = 2015, "has no clan"
		return
	}

	clanInfoMember := clan.MustGetClanInfoMember(clanInfo)
	if clanInfoMember == nil {
		res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "has no clanInfoMember"
		return
	}

	hasPrivileges := clan.CheckPrivileges(clanInfoMember.MemberList, r.Id)
	if !hasPrivileges {
		res.Err.Id, res.Err.DebugMsg = 2019, "has no privileges"
		return
	}

	if req.Type == msg.ModifyClanType_MODIFY_TYPE_NAME {
		if clanInfo.PresidentId != r.Id {
			res.Err.Id, res.Err.DebugMsg = 2016, "11 has no privileges"
		}
	}

	clan.ModifyInfo(req.Type, req.Content, clanInfo, res.Err)
	if res.Err.Id != 0 {
		return
	}
	// save
	var clanInfoCache redisdb.ClanInfoCache
	clanInfoCache.MustUpdate(clanInfo.Id, clanInfo)

	return
}

func (*HandlerHttp) ApplyFoundClan(ctx context.Context, req *msg.ApplyFoundClanR) (res *msg.ApplyFoundClanA, err error) {
	err = nil
	res = &msg.ApplyFoundClanA{
		Err: &msg.ErrorInfo{},
	}

	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}

	var c redisdb.PlayerInfoCache
	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}

	rs := share.GetClanId(r.Id)
	if rs == nil {
		err = gconst.ErrNoUserShare
		return
	}

	if rs.ClanId != 0 {
		res.Err.Id, res.Err.DebugMsg = 2001, "you has joined a clan"
		return
	}

	globalConf, ok := conf.GetGlobalConfig(1)
	if !ok {
		panic("globalConf is nil")
	}

	escapedName := clan.CheckName(req.Name, res.Err)
	if res.Err.Id != 0 {
		return
	}

	consumeStatic := &msg.StaticObjInfo{
		Id:    int32(msg.CurrencyId_CI_Diamond),
		Type:  int32(msg.ObjType_OT_Currency),
		Count: int64(globalConf.UnionCreateCostDiamond),
	}

	ok = game.CheckConsume(r, consumeStatic, res.Err)
	if !ok || res.Err.Id != 0 {
		return
	}

	lockKey := cache.GetLockKeyFor("gmx:lock:clanName:" + escapedName)
	lockValue, lockOk := dlock.AcquireLockX(lockKey, gconst.DLockGetTimeout)
	if !lockOk {
		res.Err.Id, res.Err.DebugMsg = -1, "get lock failed"
		return
	}
	defer func() {
		if lockOk {
			dlock.ReleaseLockX(lockKey, lockValue)
		}
	}()

	clanInfo := clan.MustCreateClan(r, escapedName, 0, req.Badge)
	if clanInfo == nil {
		res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "ApplyFoundClan clanInfo is nil"
		return
	}
	playerBrief := game.FindPlayerBrief(r.Id)
	clanInfo.PresidentBrief = playerBrief

	clanInfo.Member = clan.MustGetClanInfoMember(clanInfo)
	if clanInfo.Member == nil {
		res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "ApplyFoundClan clanInfo Member is nil"
		return
	}
	clanInfoExt := clan.MustGetClanInfoExt(clanInfo)
	if clanInfoExt == nil {
		res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "ApplyFoundClan clanInfoExt is nil"
		return
	}

	clan.AddMember(r.Id, msg.PositionType_PT_PRESIDENT, clanInfo, clanInfo.Member, clanInfo.Ext, res.Err)
	if res.Err.Id != 0 {
		res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "ApplyFoundClan clanInfo Member add error"
		return
	}

	ok = game.SubConsume(r, consumeStatic, res.Err)
	if !ok {
		return
	}

	ok = share.SetClanId(r.Id, clanInfo.Id, 0)
	if !ok {
		res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "set clan id err"
		return
	}

	c.MustUpdate(r.Id, r)

	clan.MustGetClanInfoExt(clanInfo)

	var clanInfoCache redisdb.ClanInfoCache
	clanInfoCache.MustUpdate(clanInfo.Id, clanInfo)

	z := &redis.Z{
		Score:  float64(clanInfo.LastActiveTime),
		Member: clanInfo.Id,
	}
	persistredis.Redis.ZAdd(clan.ClanCache, z)

	game.TryAppendSyncObjsMsgWithConsume(ctx, r, nil, consumeStatic)

	return
}

func (*HandlerHttp) SearchClan(ctx context.Context, req *msg.SearchClanR) (res *msg.SearchClanA, err error) {
	err = nil
	res = &msg.SearchClanA{
		Err: &msg.ErrorInfo{},
	}

	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}

	var c redisdb.PlayerInfoCache
	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}

	if req.Content == "" {
		res.Err.Id, res.Err.DebugMsg = 2008, "pls input clan name or id"
		return
	}

	globalConf, ok := conf.GetGlobalConfig(1)
	if !ok {
		panic("globalConf is nil")
	}
	if int32(len(req.Content)) > globalConf.UnionNameCharacterLimit[1] {
		res.Err.Id, res.Err.DebugMsg = 2010, "clan name must be 4-16"
		return
	}

	clan.HasBadWord(res.Err, req.Content)
	if res.Err.Id != 0 {
		res.Err.Id, res.Err.DebugMsg = 2011, "clan name is not ok"
		return
	}
	escapedContent := dbutil.Escape(req.Content)

	clanInfos := clan.SearchClanInfo(escapedContent, res.Err)

	if len(clanInfos) == 0 {
		res.Err.Id, res.Err.DebugMsg = 2014, "clan not find 2"
		return
	}
	if res.Err.Id != 0 {
		res.Err.Id, res.Err.DebugMsg = 2014, "clan search is error not found"
		return
	}

	for i := 0; i < len(clanInfos); {
		clanInfos[i].PresidentBrief = game.FindPlayerBrief(clanInfos[i].PresidentId)
		if clanInfos[i].MemberCount == 0 {
			res.Err.Id, res.Err.DebugMsg = 2014, "clan is delete"
			clanInfos = append(clanInfos[:i], clanInfos[i+1:]...)
		} else {
			i++
		}
	}

	res.Infos = clanInfos

	return
}

func (*HandlerHttp) ApplyJoinClan(ctx context.Context, req *msg.ApplyJoinClanR) (res *msg.ApplyJoinClanA, err error) {
	err = nil
	res = &msg.ApplyJoinClanA{
		Err: &msg.ErrorInfo{},
	}

	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}

	var c redisdb.PlayerInfoCache
	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}

	rs := share.GetClanId(r.Id)
	if rs == nil {
		err = gconst.ErrNoUserShare
		return
	}

	if rs.ClanId != 0 {
		clanInfo := clan.Find(rs.ClanId)
		clanInfo.PresidentBrief = game.FindPlayerBrief(clanInfo.PresidentId)
		res.Info = clanInfo
		res.Err.Id, res.Err.DebugMsg = 2001, "you has joined a clan"
		return
	}

	var playerInfoClanCache redisdb.PlayerInfoClanCache
	playerInfoClan := playerInfoClanCache.Find(r.Id)
	if playerInfoClan == nil {
		res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "playerInfoClan is nil"
		return
	}

	now := time3.Now().Unix()
	if playerInfoClan.CdExpiredTime > 0 &&
		playerInfoClan.CdExpiredTime > now {
		res.Err.Id, res.Err.DebugMsg = 2049, "player join clan in cd"
		return
	}

	joinClanId := clan.GetJoinId(req.Id)
	if 0 == joinClanId {
		res.Err.Id, res.Err.DebugMsg = 2009, "ApplyJoinClan handler can not join  clan 0"
		return
	}
	key := clan.GetClanLockKey(joinClanId)
	lockValue, lockOk := dlock.AcquireLockX(key, gconst.DLockGetTimeout)
	if !lockOk {
		res.Err.Id, res.Err.DebugMsg = -1, "get clan lock failed"
		return
	}
	defer func() {
		if lockOk {
			dlock.ReleaseLockX(key, lockValue)
		}
	}()

	clanInfo := clan.Find(joinClanId)
	if clanInfo == nil {
		res.Err.Id, res.Err.DebugMsg = 2002, "ApplyJoinClan handler can not find the clan"
		return
	}

	ok := clan.ApplyJoin(clanInfo, r, res.Err)
	if !ok || res.Err.Id != 0 {
		return
	}

	if 0 == clanInfo.ApplyType {
		game.RefreshClanRedPoint(r, true, clanInfo, msg.RedPointType_RPT_ClanReward)
	}
	// save
	var clanInfoCache redisdb.ClanInfoCache
	clanInfoCache.MustUpdate(clanInfo.Id, clanInfo)

	c.MustUpdate(r.Id, r)

	if clanInfo.ApplyType == 0 ||
		rs.ClanId != 0 {
		clanInfo.Member = nil
		clanInfo.PresidentBrief = game.FindPlayerBrief(clanInfo.PresidentId)
		res.Info = clanInfo
	}

	// 刷新红点
	game.RTMNotifyRedPoint(clanInfo.PresidentId, msg.RedPointType_RPT_TriggerNewClanApply, 0, 0)
	game.RTMNotifyRedPoint(clanInfo.VicePresidentId, msg.RedPointType_RPT_TriggerNewClanApply, 0, 0)

	return
}

func (*HandlerHttp) AgreeJoinClan(ctx context.Context, req *msg.AgreeJoinClanR) (res *msg.AgreeJoinClanA, err error) {
	err = nil
	res = &msg.AgreeJoinClanA{
		Err: &msg.ErrorInfo{},
	}

	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}

	var c redisdb.PlayerInfoCache
	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}

	rs := share.GetClanId(r.Id)
	if rs == nil {
		err = gconst.ErrNoUserShare
		return
	}

	key := clan.GetClanLockKey(rs.ClanId)
	lockValue, lockOk := dlock.AcquireLockX(key, gconst.DLockGetTimeout)
	if !lockOk {
		res.Err.Id, res.Err.DebugMsg = -1, "get clan lock failed"
		return
	}
	defer func() {
		if lockOk {
			dlock.ReleaseLockX(key, lockValue)
		}
	}()

	clanInfo := clan.Find(rs.ClanId)
	if clanInfo == nil {
		res.Err.Id, res.Err.DebugMsg = 2015, "has no clan"
		return
	}

	addPlayerIds := clan.AgreeJoin(r, clanInfo, req.PlayerId, req.IsAgree, res.Err)
	if res.Err.Id != 0 {
		return
	}

	for _, v := range addPlayerIds {
		rs := share.GetClanId(v)
		if rs == nil {
			err = gconst.ErrNoUserShare
			return
		}
		if 1 == req.IsAgree {
			ok := share.SetClanId(v, clanInfo.Id, 0)
			if !ok {
				res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "join failed"
				return
			}
		}

		/* TODO
		var cache2 redisdb.PlayerInfoClanCache
		playerCache := cache2.Find(v)
		delete(playerCache.ApplyList, rs.ClanId)
		*/

		var configId int32
		var paramsEx []string
		var paramsEx2 []string

		paramsEx2 = append(paramsEx2, "0")
		paramsEx = append(paramsEx, clanInfo.Name)

		paramsEx2 = append(paramsEx2, "0")
		paramsEx = append(paramsEx, r.Name)

		if 1 == req.IsAgree {
			configId = int32(MailTypeJoin)
		} else if 0 == req.IsAgree {
			configId = int32(MailTypeRejectJoin)
		}
		var m *msg.MailItem
		m = game.NewMailFromConf(configId, "", "", paramsEx, paramsEx2, res.Err)
		if res.Err.Id != 0 {
			return
		}
		share.MustInsertMailIntoShare(v, m)
	}

	game.RefreshClanRedPoint(r, true, clanInfo, msg.RedPointType_RPT_ClanApply)

	// save
	var clanInfoCache redisdb.ClanInfoCache
	clanInfoCache.MustUpdate(rs.ClanId, clanInfo)

	clanInfo.Member = nil
	clanInfo.Ext = nil
	clanInfo.PresidentBrief = game.FindPlayerBrief(clanInfo.PresidentId)
	res.Info = clanInfo

	// 刷新红点
	game.RTMNotifyRedPoint(req.PlayerId, msg.RedPointType_RPT_TriggerNewClanApply, 0, 0)
	game.RTMNotifyRedPoint(req.PlayerId, msg.RedPointType_RPT_ClanReward, 0, 0)

	return
}

func (*HandlerHttp) AppointPosition(ctx context.Context, req *msg.AppointPositionR) (res *msg.AppointPositionA, err error) {
	err = nil
	res = &msg.AppointPositionA{
		Err: &msg.ErrorInfo{},
	}

	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}

	var c redisdb.PlayerInfoCache
	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}

	// !注意锁的顺序
	rs := share.GetClanId(r.Id)
	if rs == nil {
		err = gconst.ErrNoUserShare
		return
	}

	key := clan.GetClanLockKey(rs.ClanId)
	lockValue, lockOk := dlock.AcquireLockX(key, gconst.DLockGetTimeout)
	if !lockOk {
		res.Err.Id, res.Err.DebugMsg = -1, "get clan lock failed"
		return
	}
	defer func() {
		if lockOk {
			dlock.ReleaseLockX(key, lockValue)
		}
	}()

	clanInfo := clan.Find(rs.ClanId)
	if clanInfo == nil {
		res.Err.Id, res.Err.DebugMsg = 2015, "has no clan"
		return
	}

	if r.Id == req.PlayerId {
		res.Err.Id, res.Err.DebugMsg = 2024, "can not is self" //自己任命自己
		return
	}

	if clanInfo.PresidentId != r.Id {
		res.Err.Id, res.Err.DebugMsg = 2016, "has no privileges"
		return
	}

	members := clan.AppointPosition(r, clanInfo, req.PlayerId, req.Position, res.Err)
	if res.Err.Id != 0 {
		return
	}

	for _, v := range members {
		allActive := clan.MemberSevenDayContribution(v)

		brief := game.FindPlayerBrief(v.PlayerId)
		if brief == nil {
			continue
		}
		member := &msg.MemberInfo{
			Brief:       brief,
			Position:    msg.PositionType(v.Position),
			OnlineTime:  brief.LastLogoutTime,
			ActiveValue: allActive,
		}
		res.MemberList = append(res.MemberList, member)
	}
	//只有被任命人收到
	var m *msg.MailItem

	var paramsEx []string
	var paramsEx2 []string
	paramsEx2 = append(paramsEx2, "0")
	paramsEx = append(paramsEx, clanInfo.Name)

	var posLanguageId int
	if req.Position == msg.PositionType_PT_PRESIDENT {
		posLanguageId = PresidentLanguageId
	} else if req.Position == msg.PositionType_PT_VICE_PRESIDENT {
		posLanguageId = VicePresidentLanguageId
	} else if req.Position == msg.PositionType_PT_MEMBER {
		posLanguageId = MemberLanguageId
	}
	paramsEx2 = append(paramsEx2, "1")
	paramsEx = append(paramsEx, strconv.Itoa(posLanguageId))

	m = game.NewMailFromConf(int32(MailTypeAppoint), "", "", paramsEx, paramsEx2, res.Err)
	if res.Err.Id != 0 {
		return
	}
	share.MustInsertMailIntoShare(req.PlayerId, m)

	var clanInfoCache redisdb.ClanInfoCache
	clanInfoCache.MustUpdate(rs.ClanId, clanInfo)

	return
}

func (*HandlerHttp) ExpelClanMember(ctx context.Context, req *msg.ExpelClanMemberR) (res *msg.ExpelClanMemberA, err error) {
	err = nil
	res = &msg.ExpelClanMemberA{
		Err: &msg.ErrorInfo{},
	}

	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}

	var c redisdb.PlayerInfoCache
	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}

	rs := share.GetClanId(r.Id)
	if rs == nil {
		err = gconst.ErrNoUserShare
		return
	}

	key := clan.GetClanLockKey(rs.ClanId)
	lockValue, lockOk := dlock.AcquireLockX(key, gconst.DLockGetTimeout)
	if !lockOk {
		res.Err.Id, res.Err.DebugMsg = -1, "get clan lock failed"
		return
	}
	defer func() {
		if lockOk {
			dlock.ReleaseLockX(key, lockValue)
		}
	}()

	clanInfo := clan.Find(rs.ClanId)
	if clanInfo == nil {
		res.Err.Id, res.Err.DebugMsg = 2015, "has no clan"
		return
	}

	clan.ExpelClanMember(r, clanInfo, req.PlayerId, res.Err)
	if res.Err.Id != 0 {
		return
	}

	ok := share.SetClanId(req.PlayerId, 0, clanInfo.Id)
	if !ok {
		res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "set clan id err"
		return
	}

	var m *msg.MailItem
	var paramsEx []string
	var paramsEx2 []string

	paramsEx2 = append(paramsEx2, "0")
	paramsEx = append(paramsEx, clanInfo.Name)
	//0:value 1:laguageid
	paramsEx2 = append(paramsEx2, "0")
	paramsEx = append(paramsEx, r.Name)

	m = game.NewMailFromConf(int32(MailTypeExpel), "", "", paramsEx, paramsEx2, res.Err)
	if res.Err.Id != 0 {
		return
	}
	share.MustInsertMailIntoShare(req.PlayerId, m)

	game.RTMNotifyRedPoint(req.PlayerId, msg.RedPointType_RPT_TriggerNewGmMail, 0, 0)
	game.RTMNotifyRedPoint(req.PlayerId, msg.RedPointType_RPT_TriggerNewClanReward, 0, 0)

	// save
	var clanInfoCache redisdb.ClanInfoCache
	clanInfoCache.MustUpdate(rs.ClanId, clanInfo)

	clanInfo.Ext = nil
	clanInfo.Member = nil
	clanInfo.PresidentBrief = game.FindPlayerBrief(clanInfo.PresidentId)
	res.Info = clanInfo
	return
}

func (*HandlerHttp) QuitClan(ctx context.Context, req *msg.QuitClanR) (res *msg.QuitClanA, err error) {
	err = nil
	res = &msg.QuitClanA{
		Err: &msg.ErrorInfo{},
	}

	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}

	var c redisdb.PlayerInfoCache
	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}

	rs := share.GetClanId(r.Id)
	if rs == nil {
		err = gconst.ErrNoUserShare
		return
	}

	if rs.ClanId == 0 {
		res.Err.Id, res.Err.DebugMsg = 2015, "has no clan"
		return
	}

	key := clan.GetClanLockKey(rs.ClanId)
	lockValue, lockOk := dlock.AcquireLockX(key, gconst.DLockGetTimeout)
	if !lockOk {
		res.Err.Id, res.Err.DebugMsg = -1, "get clan lock failed"
		return
	}
	defer func() {
		if lockOk {
			dlock.ReleaseLockX(key, lockValue)
		}
	}()

	clanInfo := clan.Find(rs.ClanId)
	if clanInfo == nil {
		res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "clanInfo is nil"
		return
	}

	newPresidentId := clan.Quit(r, clanInfo, res.Err)

	if res.Err.Id != 0 {
		return
	}

	ok := share.SetClanId(r.Id, 0, clanInfo.Id)
	if !ok {
		res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "set clan id err"
		return
	}

	var playerInfoClanCache redisdb.PlayerInfoClanCache
	playerInfoClan := playerInfoClanCache.Find(r.Id)
	if playerInfoClan == nil {
		res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "playerInfoClan is nil"
		return
	}

	globalConf, ok := conf.GetGlobalConfig(1)
	if !ok {
		panic("globalConf is nil")
	}

	playerInfoClan.QuitTimes++

	if playerInfoClan.QuitTimes > int32(len(globalConf.UnionJoinCD)) {
		playerInfoClan.QuitTimes = int32(len(globalConf.UnionJoinCD))
	}
	now := time3.Now().Unix()
	playerInfoClan.CdExpiredTime = now + int64(globalConf.UnionJoinCD[playerInfoClan.QuitTimes-1])*3600

	if newPresidentId > 0 {
		var m *msg.MailItem

		var paramsEx []string
		var paramsEx2 []string
		paramsEx2 = append(paramsEx2, "0")
		paramsEx = append(paramsEx, clanInfo.Name)

		posLanguageId := PresidentLanguageId
		paramsEx2 = append(paramsEx2, "1")
		paramsEx = append(paramsEx, strconv.Itoa(posLanguageId))

		m = game.NewMailFromConf(MailTypeAppoint, "", "", paramsEx, paramsEx2, res.Err)
		if res.Err.Id != 0 {
			return
		}
		share.MustInsertMailIntoShare(newPresidentId, m)
	}

	game.RefreshClanRedPoint(r, true, clanInfo, msg.RedPointType_RPT_ClanApply)
	game.RefreshClanRedPoint(r, true, clanInfo, msg.RedPointType_RPT_ClanReward)

	// save
	playerInfoClanCache.MustUpdate(playerInfoClan.Id, playerInfoClan)

	var clanInfoCache redisdb.ClanInfoCache
	clanInfoCache.MustUpdate(rs.ClanId, clanInfo)

	if 0 == clanInfo.MemberCount {
		//clanInfo = nil
		clanInfoCache.ExpireRedis(clanInfo.Id)
	}

	res.QuitTimes = playerInfoClan.QuitTimes
	res.CdExpiredTime = playerInfoClan.CdExpiredTime
	return
}

func (*HandlerHttp) GetClanLog(ctx context.Context, req *msg.GetClanLogR) (res *msg.GetClanLogA, err error) {
	err = nil
	res = &msg.GetClanLogA{
		Err: &msg.ErrorInfo{},
	}

	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}

	var c redisdb.PlayerInfoCache
	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}

	rs := share.GetClanId(r.Id)
	if rs == nil {
		err = gconst.ErrNoUserShare
		return
	}

	clanInfo := clan.Find(rs.ClanId)
	if clanInfo == nil {
		res.Err.Id, res.Err.DebugMsg = 2015, "has no clan"
		return
	}

	clanInfoExt := clan.MustGetClanInfoExt(clanInfo)
	if clanInfoExt == nil {
		res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "clanInfoExt is nil"
		return
	}
	for _, v := range clanInfoExt.LogDbInfos {
		playerBrief := game.FindPlayerBrief(v.Params[0])
		var params []string
		params = append(params, playerBrief.Name)
		switch v.OpType {
		case msg.ClanLogType_LOG_TYPE_JOIN:
			//log.Debugf("ClanLogType_LOG_TYPE_JOIN...%d", int32(msg.ClanLogType_LOG_TYPE_JOIN))
		case msg.ClanLogType_LOG_TYPE_QUIT:
			//log.Debugf("ClanLogType_LOG_TYPE_QUIT...%d", int32(msg.ClanLogType_LOG_TYPE_QUIT))
		case msg.ClanLogType_LOG_TYPE_DONATE:
			//档位
			params = append(params, fmt.Sprintf("%d", v.Params[1]))
			//根据档位客户端读了
			//params = append(params, fmt.Sprintf("%d", v.Params[2]))
		case msg.ClanLogType_LOG_TYPE_TRANSFER:
			playerBrief2 := game.FindPlayerBrief(v.Params[1])
			params = append(params, "0")
			params = append(params, playerBrief2.Name)
		case msg.ClanLogType_LOG_TYPE_APPOINT:
			//log.Debugf("ClanLogType_LOG_TYPE_APPOINT...%d", int32(msg.ClanLogType_LOG_TYPE_APPOINT))
		case msg.ClanLogType_LOG_TYPE_ATTACK_BOSS:
			params = append(params, "0")
			params = append(params, fmt.Sprintf("%d", v.Params[1]))
		case msg.ClanLogType_LOG_TYPE_VICE_PRESIDENT_NULL:
			//log.Debugf("ClanLogType_LOG_TYPE_VICE_PRESIDENT_NULL...%d", int32(msg.ClanLogType_LOG_TYPE_VICE_PRESIDENT_NULL))
		default:
			log.Debugf("default none...%d", int32(msg.ClanLogType_LOG_TYPE))
		}
		logInfo := &msg.LogInfo{
			LogTime: v.LogTime,
			OpType:  v.OpType,
			Params:  params,
		}
		res.LogList = append(res.LogList, logInfo)
	}

	return
}

func (*HandlerHttp) GetClanApplyInfo(ctx context.Context, req *msg.GetClanApplyInfoR) (res *msg.GetClanApplyInfoA, err error) {
	err = nil
	res = &msg.GetClanApplyInfoA{
		Err: &msg.ErrorInfo{},
	}

	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}

	var c redisdb.PlayerInfoCache
	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}

	rs := share.GetClanId(r.Id)
	if rs == nil {
		err = gconst.ErrNoUserShare
		return
	}

	clanInfo := clan.Find(rs.ClanId)
	if clanInfo == nil {
		res.Err.Id = msg.Ecode_INVALID
		return
	}
	clanInfo.Ext = clan.MustGetClanInfoExt(clanInfo)
	if clanInfo.Ext == nil {
		res.Err.Id = msg.Ecode_INVALID
		return
	}

	now := time3.Now().Unix()

	for k, v := range clanInfo.Ext.ApplyList {
		if v < now {
			delete(clanInfo.Ext.ApplyList, k)
			//log.Debugf("GetClanApplyInfo ApplyList is expired delete %d", k)
			//res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "playerInfoClan is nil"
			continue
		}
		playerBrief := game.FindPlayerBrief(k)
		applyInfo := &msg.ApplyInfo{
			Brief:     playerBrief,
			ApplyTime: v,
		}
		res.ApplyList = append(res.ApplyList, applyInfo)
	}
	return
}

func (*HandlerHttp) SendClanMail(ctx context.Context, req *msg.SendClanMailR) (res *msg.SendClanMailA, err error) {
	err = nil
	res = &msg.SendClanMailA{
		Err: &msg.ErrorInfo{},
	}

	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}

	var c redisdb.PlayerInfoCache
	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}

	rs := share.GetClanId(r.Id)
	if rs == nil {
		err = gconst.ErrNoUserShare
		return
	}

	key := clan.GetClanLockKey(rs.ClanId)
	lockValue, lockOk := dlock.AcquireLockX(key, gconst.DLockGetTimeout)
	if !lockOk {
		res.Err.Id, res.Err.DebugMsg = -1, "get clan lock failed"
		return
	}
	defer func() {
		if lockOk {
			dlock.ReleaseLockX(key, lockValue)
		}
	}()

	clanInfo := clan.Find(rs.ClanId)
	if clanInfo == nil {
		res.Err.Id, res.Err.DebugMsg = 2015, "has no clan"
		return
	}

	/*
		if clanInfo.PresidentId != req.SendPlayerId &&
			clanInfo.VicePresidentId != req.SendPlayerId {
			res.Err.Id = msg.Ecode_INVALID
			return
		}
	*/
	clan.CheckMailStr(req.Title, req.Content, res.Err)
	if res.Err.Id != 0 {
		return
	}

	escapedTitle := dbutil.Escape(req.Title)

	escapedContent := dbutil.Escape(req.Content)

	clanInfoMember := clan.MustGetClanInfoMember(clanInfo)
	if clanInfoMember == nil {
		res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "clanInfoMember is nil"
		return
	}

	hasPrivileges := clan.CheckPrivileges(clanInfoMember.MemberList, r.Id)
	if !hasPrivileges {
		res.Err.Id, res.Err.DebugMsg = 2019, "has no privileges"
		return
	}

	playerInfoClan := clan.MustGetPlayerInfoClan(r)
	if playerInfoClan == nil {
		res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "playerInfoClan is nil"
		return
	}

	globalConf, ok := conf.GetGlobalConfig(1)
	if !ok {
		panic("globalConf is nil")
	}

	if playerInfoClan.MailSendCount >= globalConf.UnionLeaderSendMailDailyLimit {
		res.Err.Id, res.Err.DebugMsg = 2044, "playerInfoClan.MailSendCount is max"
		return
	}
	// save
	clanInfoExt := clan.MustGetClanInfoExt(clanInfo)
	if clanInfoExt == nil {
		res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "clanInfoExt is max"
		return
	}

	playerBrief := game.FindPlayerBrief(req.SendPlayerId)
	if playerBrief == nil {
		res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "playerBrief is max"
		return
	}

	var m *msg.MailItem
	//m = game.NewMailFromConf(int32(MailTypeAdmin), req.Content, "", nil, res.Err)
	m = &msg.MailItem{
		Id:               0,
		TitleId:          0,
		TitleStr:         escapedTitle,
		ContentId:        0,
		ContentStr:       escapedContent,
		Rewards:          nil,
		CreateTime:       time3.Now().Unix(),
		Deadline:         time3.Now().Unix() + gconst.MailDeadDuration,
		OneClickReceive:  false, // GM default
		OneClickDelete:   false,
		SupportManualDel: true,
		SupportAutoDel:   true,
		SenderId:         0,
		SenderStr:        playerBrief.Name,
	}

	for _, v := range clanInfoMember.MemberList {
		//if v.PlayerId == req.SendPlayerId {
		//	continue
		//}
		share.MustInsertMailIntoShare(v.PlayerId, m)
		game.RTMNotifyRedPoint(v.PlayerId, msg.RedPointType_RPT_TriggerNewGmMail, 0, 0)
	}

	playerInfoClan.MailSendCount = playerInfoClan.MailSendCount + 1

	var clanInfoExtCache redisdb.ClanInfoExtCache
	clanInfoExtCache.MustUpdate(rs.ClanId, clanInfoExt)

	c.MustUpdate(r.Id, r)

	return
}

func (*HandlerHttp) GetClanMailInfo(ctx context.Context, req *msg.GetClanMailInfoR) (res *msg.GetClanMailInfoA, err error) {
	err = nil
	res = &msg.GetClanMailInfoA{
		Err: &msg.ErrorInfo{},
	}

	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}

	var c redisdb.PlayerInfoCache
	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}

	rs := share.GetClanId(r.Id)
	if rs == nil {
		err = gconst.ErrNoUserShare
		return
	}

	return
}

func (*HandlerHttp) ClanDonate(ctx context.Context, req *msg.ClanDonateR) (res *msg.ClanDonateA, err error) {
	err = nil
	res = &msg.ClanDonateA{
		Err: &msg.ErrorInfo{},
	}

	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}

	var c redisdb.PlayerInfoCache
	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}

	rs := share.GetClanId(r.Id)
	if rs == nil {
		err = gconst.ErrNoUserShare
		return
	}

	key := clan.GetClanLockKey(rs.ClanId)
	lockValue, lockOk := dlock.AcquireLockX(key, gconst.DLockGetTimeout)
	if !lockOk {
		res.Err.Id, res.Err.DebugMsg = -1, "get clan lock failed"
		return
	}
	defer func() {
		if lockOk {
			dlock.ReleaseLockX(key, lockValue)
		}
	}()

	clanInfo := clan.Find(rs.ClanId)
	if clanInfo == nil {
		res.Err.Id, res.Err.DebugMsg = 2015, "has no clan"
		return
	}

	playerInfoClan := clan.MustGetPlayerInfoClan(r)
	if playerInfoClan == nil {
		res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "playerInfoClan is nil"
		return
	}

	globalConf, ok := conf.GetGlobalConfig(1)
	if !ok {
		panic("globalConf is nil")
	}

	if playerInfoClan.DonateTimes >= globalConf.UnionDonateDailyTimes {
		res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "playerInfoClan DonateTimes is limit"
		return
	}

	if clanInfo.AllActiveValue >= globalConf.UnionActiveLimit {
		res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "UnionActiveLimit is limit"
		return
	}
	for k, v := range r.Currency {
		if k != int32(msg.CurrencyId_CI_ClanCoin) {
			continue
		}

		if v >= int64(globalConf.UnionTokenLimit) {
			res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "UnionTokenLimit is limit"
			return
		}
	}

	donateConf, ok := conf.GetUnionDonateConfig(req.DonateType)
	if donateConf == nil || !ok {
		res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "donateConf is nil"
		return
	}
	consumeObjs, _ := game.JsonToPb_StaticObjs(donateConf.DonateCost, donateConf, donateConf.Id, res.Err)
	ok = game.CheckConsumes(r, consumeObjs, res.Err)
	if res.Err.Id != 0 {
		return
	}
	staticObjs, _ := game.JsonToPb_StaticObjs(donateConf.DonateReward, donateConf, donateConf.Id, res.Err)
	//log.Debugf("staticObjs:%+v", staticObjs)

	var addObjs []*msg.ObjInfo
	for _, v := range staticObjs {
		//log.Debugf("v.Id :%+v", v.Id)
		//log.Debugf("v.Count :%+v", v.Count)
		if v.Id == int32(msg.CurrencyId_CI_ClanCoin) {
			addObj, _ := game.AddStaticObj(r, v, res.Err)
			addObjs = append(addObjs, addObj...)
			//log.Debugf("addObj:%+v", addObj)
		}
	}
	//log.Debugf("addObjs:%+v", addObjs)

	clan.Donate(r, clanInfo, playerInfoClan, staticObjs, req.DonateType, res.Err)
	if res.Err.Id != 0 {
		return
	}

	ok = game.SubConsumes(r, consumeObjs, res.Err)
	if !ok {
		return
	}

	member := clan.MustGetClanInfoMember(clanInfo)
	for i := 0; i < len(member.MemberList); i++ {

		r2 := c.Find(member.MemberList[i].PlayerId)
		if r2 == nil {
			err = gconst.ErrNoUser
			return
		}

		if rs.ClanId != clanInfo.Id {
			continue
		}

		if member.MemberList[i].PlayerId == r.Id {
			game.RefreshClanRedPoint(r, true, clanInfo, msg.RedPointType_RPT_ClanReward)
			continue
		}
		game.RTMNotifyRedPoint(r2.Id, msg.RedPointType_RPT_TriggerNewClanReward, 0, 0)
	}
	// save
	c.MustUpdate(r.Id, r)

	var clanInfoCache redisdb.ClanInfoCache
	clanInfoCache.MustUpdate(clanInfo.Id, clanInfo)

	game.TryAppendSyncObjsMsgWithConsumes(ctx, r, addObjs, consumeObjs)

	clanInfo.Member = nil
	clanInfo.Ext = nil
	clanInfo.PresidentBrief = game.FindPlayerBrief(clanInfo.PresidentId)
	res.Info = clanInfo

	return
}

func (*HandlerHttp) GetClanBoss(ctx context.Context, req *msg.GetClanBossR) (res *msg.GetClanBossA, err error) {
	err = nil
	res = &msg.GetClanBossA{
		Err: &msg.ErrorInfo{},
	}

	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}

	var c redisdb.PlayerInfoCache
	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}

	rs := share.GetClanId(r.Id)
	if rs == nil {
		err = gconst.ErrNoUserShare
		return
	}

	key := clan.GetClanLockKey(rs.ClanId)
	lockValue, lockOk := dlock.AcquireLockX(key, gconst.DLockGetTimeout)
	if !lockOk {
		res.Err.Id, res.Err.DebugMsg = -1, "get clan lock failed"
		return
	}
	defer func() {
		if lockOk {
			dlock.ReleaseLockX(key, lockValue)
		}
	}()

	clanInfo := clan.Find(rs.ClanId)
	if clanInfo == nil {
		res.Err.Id, res.Err.DebugMsg = 2015, "has no clan"
		return
	}

	clanMember := clan.MustGetClanInfoMember(clanInfo)
	if clanMember == nil {
		res.Err.Id, res.Err.DebugMsg = 2015, "has no clan"
		return
	}

	playerInfoClan := clan.MustGetPlayerInfoClan(r)
	for _, v := range clanMember.MemberList {
		if v.PlayerId != playerInfoClan.Id {
			continue
		}
		if v.EliteBossTimes <= 0 {
			continue
		}
		for _, v2 := range playerInfoClan.BossInfos {
			if v2.Id != int32(msg.ClanBossType_CLAN_ELITE_BOSS) {
				continue
			}
			//log.Debugf("GetClanBoss %+v", v2.Attacktimes)
			v2.Attacktimes = v2.Attacktimes + v.EliteBossTimes
			v.EliteBossTimes = 0
			//log.Debugf("after add  %+v", v2.Attacktimes)
		}
	}

	c.MustUpdate(r.Id, r)

	var clanInfoCache redisdb.ClanInfoCache
	clanInfoCache.MustUpdate(clanInfo.Id, clanInfo)

	res.BossInfos = playerInfoClan.BossInfos

	return
}
