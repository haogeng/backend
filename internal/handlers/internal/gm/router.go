package gm

import (
	"bitbucket.org/funplus/ark"
	internal2 "server/internal"
	"server/pkg/fungin/xmiddleware"
	"server/pkg/fungin/xmiddleware/whiteips"
	"server/pkg/gen/service"
)

type HandlerHttp struct{}

const (
	KGroupName     string = "gm"
	KSKipperHeader string = "x-skipper-whiteip" // 1 or 0
)

// 配合middleware， 带这个头信息的，直接skipper middleware
func HeaderSkipper(ctx *ark.Context) bool {
	x := ctx.GetHeader(KSKipperHeader)
	if x == "1" {
		return true
	}
	return false
}

func InitHttpRouter(group ark.Router) {
	// prepare sth.
	apiGroup := group.Group(KGroupName)
	apiGroup.Use(
		// 白名单 -- 线上版本会统一走白名单，除非在http头信息中有跳过白名单机制
		// http 头信息有特定开关，，代码真正的gm客户端
		whiteips.New(internal2.GetHotConfig(), HeaderSkipper),

		xmiddleware.ArkUnmarshal(),

		whiteips.SkipByProtoName(internal2.GetHotConfig()),
	)

	{
		service.RegisterGmServiceHttpHandler(apiGroup, &HandlerHttp{})
	}
}
