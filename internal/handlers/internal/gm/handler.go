package gm

import (
	"bitbucket.org/funplus/golib/deepcopy"
	"context"
	"errors"
	"server/internal/game"
	"server/internal/gconst"
	"server/internal/key"
	"server/internal/util/time3"
	"server/pkg/fungin/xmiddleware"
	"server/pkg/gen/msg"
	"server/pkg/gen_redis/redisdb"
	"server/pkg/persistredis"
	"strconv"
)

//Gen: [GmLock GmSendMail]

const (
	secretToken = "gmx-secret-uxike78J!#~88oL"
)

func checkSecretToken(s string, err *msg.ErrorInfo) bool {
	if s != secretToken {
		err.Id, err.DebugMsg = msg.Ecode_INVALID, "unknown secret token"
		return false
	}
	return true
}

// 锁玩家
// 1. session 失效， redis失效
func (*HandlerHttp) GmLock(ctx context.Context, req *msg.GmLockR) (res *msg.GmLockA, err error) {
	res = &msg.GmLockA{
		Err: &msg.ErrorInfo{},
	}

	if !checkSecretToken(req.SecretToken, res.Err) {
		return
	}

	var c redisdb.PlayerInfoCache
	r := c.Find(req.TargetId)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}

	// do logic with r
	r.LockDeadline = req.LockDeadline
	r.LockReason = req.LockReason

	//ORDER！！！

	// if needed, update r
	c.MustUpdate(r.Id, r)

	// session 失效， redis失效
	xmiddleware.ExpireToken(req.TargetId)
	c.ExpireRedis(req.TargetId)

	return
}

func (*HandlerHttp) GmSendMail(ctx context.Context, req *msg.GmSendMailR) (res *msg.GmSendMailA, err error) {
	res = &msg.GmSendMailA{
		Err: &msg.ErrorInfo{},
	}

	if !checkSecretToken(req.SecretToken, res.Err) {
		return
	}

	var c redisdb.PlayerInfoCache

	// targetId = fakeUserId
	userId := game.GetPlayerIdByFakeUserId(int32(req.TargetId))
	r := c.Find(userId)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}

	// do logic with r
	// insert mail into mail buffer

	// 如果configId 不为0，则相关信息使用配置模板的数据
	// 如果为0，则configId无用，使用后续内容
	var m *msg.MailItem
	if req.ConfigId != 0 {
		m = game.NewMailFromConf(req.ConfigId, "", "", req.Params, nil, res.Err)
		if res.Err.Id != 0 {
			return
		}
	} else {
		m = &msg.MailItem{
			Id:               0,
			TitleId:          req.TitleId,
			TitleStr:         "",
			ContentId:        req.ContentId,
			ContentStr:       "",
			Rewards:          nil,
			CreateTime:       time3.Now().Unix(),
			Deadline:         time3.Now().Unix() + gconst.MailDeadDuration,
			OneClickReceive:  false, // GM default
			OneClickDelete:   false,
			SupportManualDel: true,
			SupportAutoDel:   true,
		}
		// rewards
		if len(req.Rewards) > 0 {
			m.Rewards = req.Rewards
		} else if len(req.RewardsCmd) > 0 {
			m.Rewards = game.ParseGmAddObjCmd(req.RewardsCmd, res.Err)
			if res.Err.Id != 0 {
				return
			}
		}
		m.Params = req.Params
	}
	game.AddGmMail(r, m, res.Err)
	if res.Err.Id != 0 {
		return
	}

	// if needed, update r
	c.MustUpdate(r.Id, r)

	return
}

func (*HandlerHttp) GmGetMail(ctx context.Context, req *msg.GmGetMailListR) (res *msg.GmGetMailListA, err error) {
	res = &msg.GmGetMailListA{
		Err: &msg.ErrorInfo{},
	}

	if !checkSecretToken(req.SecretToken, res.Err) {
		return
	}

	var c redisdb.PlayerInfoCache

	r := c.Find(req.PlayerId)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}

	// do logic with r
	// insert mail into mail buffer

	// 如果configId 不为0，则相关信息使用配置模板的数据
	// 如果为0，则configId无用，使用后续内容
	game.MustGetPlayerInfoExt(r)

	for i := range r.Ext.GmMailBuffer {
		if r.Ext.GmMailBuffer[i] != nil {
			res.Mails = append(res.Mails, r.Ext.GmMailBuffer[i])
		}
	}

	return
}

func (*HandlerHttp) GmDeleteMail(ctx context.Context, req *msg.GmDeleteMailR) (res *msg.GmDeleteMailA, err error) {
	res = &msg.GmDeleteMailA{
		Err: &msg.ErrorInfo{},
	}

	if !checkSecretToken(req.SecretToken, res.Err) {
		return
	}

	var c redisdb.PlayerInfoCache

	r := c.Find(req.PlayerId)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}
	game.MustGetPlayerInfoExt(r)
	r.Ext.GmMailBuffer = nil
	c.MustUpdate(r.Id, r)
	return
}

func (*HandlerHttp) GmPlayerInfo(ctx context.Context, req *msg.GmPlayerInfoR) (res *msg.GmPlayerInfoA, err error) {
	res = &msg.GmPlayerInfoA{
		Err: &msg.ErrorInfo{},
	}

	if !checkSecretToken(req.SecretToken, res.Err) {
		return
	}

	if req.TargetId == 0 {
		// 用户ID为0,根据名字查询
		req.TargetId = game.GetPlayerIdByName(req.PlayerName)
	}
	var c redisdb.PlayerInfoCache

	r := c.Find(req.TargetId)
	if r == nil {
		err = errors.New("not found user in db =" + strconv.FormatUint(req.TargetId, 10))
		return
	}

	game.MustGetPlayerInfoExt(r)
	res.PlayerInfo = deepcopy.Copy(r).(*msg.PlayerInfo)

	// 部分数据前端数据和后端分离
	game.WithClientDataInFinal(res.PlayerInfo, true, msg.FormationType_FT_Count)

	return
}

func (*HandlerHttp) GmModifyMainCopy(ctx context.Context, req *msg.GmModifyMainCopyR) (res *msg.GmModifyMainCopyA, err error) {
	res = &msg.GmModifyMainCopyA{
		Err: &msg.ErrorInfo{},
	}
	if !checkSecretToken(req.SecretToken, res.Err) {
		return
	}
	if req.TargetId == 0 {
		// 用户ID为0,根据名字查询用户ID
		req.TargetId = game.GetPlayerIdByName(req.PlayerName)
	}
	var c redisdb.PlayerInfoCache
	r := c.Find(req.TargetId)
	if r == nil {
		err = errors.New("not found user in db =" + strconv.FormatUint(req.TargetId, 10))
		return
	}
	// 修改主线副本
	game.ModifyMainCopyByPlayerInfo(r, req.ChapterId, req.StrongholdId, res.Err)
	c.MustUpdate(r.Id, r)
	return
}

func (*HandlerHttp) GmModifyPlayerInfo(ctx context.Context, req *msg.GmModifyPlayerInfoR) (res *msg.GmModifyPlayerInfoA, err error) {
	res = &msg.GmModifyPlayerInfoA{
		Err: &msg.ErrorInfo{},
	}
	if !checkSecretToken(req.SecretToken, res.Err) {
		return
	}
	var c redisdb.PlayerInfoCache
	r := c.Find(req.PlayerInfo.Id)
	if r == nil {
		err = errors.New("not found user in db =" + strconv.FormatUint(req.PlayerInfo.Id, 10))
		return
	}

	// 反向使用客户端数据
	game.ExtTroop(req.PlayerInfo).Troops = req.PlayerInfo.CTroops
	game.ExtTroop(req.PlayerInfo).TroopsExt = req.PlayerInfo.CTroopsExt
	game.ExtEquip(req.PlayerInfo).Equips = req.PlayerInfo.CEquips

	// 更新用户信息
	c.MustUpdate(r.Id, req.PlayerInfo)
	return
}

func (*HandlerHttp) GmModifyArenaUserInfo(ctx context.Context, req *msg.GmModifyArenaUserInfoR) (res *msg.GmModifyArenaUserInfoA, err error) {
	res = &msg.GmModifyArenaUserInfoA{}
	res.Err = &msg.ErrorInfo{}
	if !checkSecretToken(req.SecretToken, res.Err) {
		return
	}
	var c redisdb.PlayerInfoCache
	r := c.Find(req.PlayerId)
	if r == nil {
		err = errors.New("not found user in db =" + strconv.FormatUint(req.PlayerId, 10))
		return
	}
	var rArena = persistredis.Rank32
	arenaRankKey := key.GetArenaDefaultKey(r.ServerId, game.ArenaConfigId)
	rArena.Add(arenaRankKey, r.Id, int32(req.AddScore))
	return
}

func (*HandlerHttp) GmModifyWarCopy(ctx context.Context, req *msg.GmModifyWarCopyR) (res *msg.GmModifyWarCopyA, err error) {
	res = &msg.GmModifyWarCopyA{
		Err: &msg.ErrorInfo{},
	}
	//if !checkSecretToken(req.SecretToken, res.Err) {
	//	return
	//}
	if req.TargetId == 0 {
		// 用户ID为0,根据名字查询用户ID
		req.TargetId = game.GetPlayerIdByName(req.PlayerName)
	}
	var c redisdb.PlayerInfoCache
	r := c.Find(req.TargetId)
	if r == nil {
		err = errors.New("not found user in db =" + strconv.FormatUint(req.TargetId, 10))
		return
	}
	// 修改主线副本
	game.ModifyWarCopyByPlayerInfo(r, req.WarId, req.ChapterId, req.StrongholdId, res.Err)
	c.MustUpdate(r.Id, r)
	return
}
