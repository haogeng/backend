package game

import (
	"context"
	"server/internal/game"
	"server/internal/gconst"
	"server/pkg/fungin/xmiddleware"
	"server/pkg/gen/conf"
	"server/pkg/gen/msg"
	"server/pkg/gen_redis/redisdb"
)

func (*HandlerHttp) MainCopyInfos(ctx context.Context, req *msg.MainCopyInfosR) (res *msg.MainCopyInfosA, err error) {
	res = &msg.MainCopyInfosA{
		Err: &msg.ErrorInfo{},
	}
	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}

	var c redisdb.PlayerInfoCache

	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}
	// 检查初始化章节
	game.InitMainCopy(r, res.Err)
	if res.Err.Id != 0 {
		return
	}
	// 构造章节返回
	res.MainCopyInfos = game.BuildMainCopyInfoResults(r, res.Err)
	if res.Err.Id != 0 {
		return
	}
	c.MustUpdate(r.Id, r)
	return
}

func (*HandlerHttp) StrongholdInfos(ctx context.Context, req *msg.StrongholdInfosR) (res *msg.StrongholdInfosA, err error) {
	res = &msg.StrongholdInfosA{
		Err: &msg.ErrorInfo{},
	}
	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}

	var c redisdb.PlayerInfoCache

	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}
	r2 := game.MustGetPlayerInfoExt(r)
	if len(r2.MainCopyInfos) == 0 {
		err = gconst.ErrNoChapter
		return
	}
	m, ok := r2.MainCopyInfos[req.ChapterId]
	if !ok {
		err = gconst.ErrNoChapter
		return
	}

	res.FirstJoin = m.FirstJoin

	if m.FirstJoin {
		// 更新首次加载
		m.FirstJoin = false
	}
	// 构造据点返回
	res.StrongholdInfos = game.BuildStrongHoldResults(r, req.ChapterId)

	c.MustUpdate(r.Id, r)
	return
}

func (*HandlerHttp) QuickStartMainCopy(ctx context.Context, req *msg.QuickStartMainCopyR) (res *msg.QuickStartMainCopyA, err error) {
	res = &msg.QuickStartMainCopyA{
		Err: &msg.ErrorInfo{},
	}
	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}

	var c redisdb.PlayerInfoCache

	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}
	// 检查初始化章节
	game.InitMainCopy(r, res.Err)
	if res.Err.Id != 0 {
		return
	}
	r2 := game.MustGetPlayerInfoExt(r)
	// 查找正在进行的章节
	var chapterId int32 = 0
	// 是否首次加入
	var firstJoin bool = false
	for i, v := range r2.MainCopyInfos {
		if v.ChapterStatus == msg.ChapterStatus_CS_ING {
			chapterId = i
			firstJoin = v.FirstJoin
			if v.FirstJoin {
				// 更新首次加载
				v.FirstJoin = false
			}
			break
		}
	}
	// 构造据点返回
	res.StrongholdInfos = game.BuildStrongHoldResults(r, chapterId)
	res.FirstJoin = firstJoin

	c.MustUpdate(r.Id, r)
	return
}

func (*HandlerHttp) StrongholdReward(ctx context.Context, req *msg.StrongholdRewardR) (res *msg.StrongholdRewardA, err error) {
	res = &msg.StrongholdRewardA{
		Err: &msg.ErrorInfo{},
	}
	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}

	var c redisdb.PlayerInfoCache

	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}
	// 据点基础数据
	holdData, _ := conf.GetMainInstanceStrongholdConfig(req.StrongholdId)
	if !game.CheckConfData(holdData, req.StrongholdId, res.Err) {
		return
	}
	// 战斗数据校验
	checkResult, chapterId, strongholdId := game.CheckMainCopyBattleStatus(r, holdData.Id, 2, res.Err)
	if !checkResult {
		return
	}
	// 采集结算
	addedObjs, strongholdInfos, openChapterId := game.MainCopyInfoBattle(r, chapterId, strongholdId, res.Err)
	if res.Err.Id != 0 {
		return
	}

	// final sync inside
	game.TryAppendSyncObjsMsgWithConsume(ctx, r, addedObjs, nil)

	res.StrongholdInfos = strongholdInfos
	res.OpenChapterId = openChapterId
	res.ChapterId = req.ChapterId
	res.StrongholdId = req.StrongholdId

	c.MustUpdate(r.Id, r)
	return
}
