package game

import (
	"context"
	"server/internal/game"
	"server/internal/gconst"
	"server/internal/log"
	"server/internal/util/time3"
	"server/pkg/fungin/xmiddleware"
	"server/pkg/gen/conf"
	"server/pkg/gen/msg"
	"server/pkg/gen_redis/redisdb"
)

//
func (*HandlerHttp) StartUpgradeTech(ctx context.Context, req *msg.StartUpgradeTechR) (res *msg.StartUpgradeTechA, err error) {

	err = nil
	res = &msg.StartUpgradeTechA{
		Err:  &msg.ErrorInfo{},
		Info: nil,
	}

	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}

	var c redisdb.PlayerInfoCache
	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}
	log.Debugf("=== uid:===: %+v", uid)
	if !game.MakeSureTechExist(r) {
		res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "MakeSureTechExist error"
		return
	}

	techInfo := game.GetTechInfo(r, req.Id)
	if techInfo == nil {
		techInfo = game.AddTech(r, req.Id, res.Err)
		if res.Err.Id != 0 {
			return
		}
	}

	if nil == techInfo {
		res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "AddTech error techInfo nil"
		return
	}

	techData, _ := conf.GetTechnologyConfig(req.Id)

	game.CheckConfData(techData, int32(req.Id), res.Err)
	if res.Err.Id != 0 {
		return
	}

	if !game.CheckTechQueue(r, res.Err) {
		return
	}

	if 0 == techInfo.Level {
		game.CheckTechState(techInfo, msg.TechState_TECH_LOCK, res.Err)
	} else {
		game.CheckTechState(techInfo, msg.TechState_TECH_NORMAL, res.Err)
	}
	if res.Err.Id != 0 {
		return
	}

	game.CheckTechCondition(techInfo, techData, r, res.Err)
	if res.Err.Id != 0 {
		return
	}

	levelConf, _ := game.GetTechLevelConf(techData, techInfo.Level, res.Err)
	if res.Err.Id != 0 {
		return
	}

	if techInfo.Level+1 > techData.MaxLevel {
		res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "building upto max level"
		return
	}

	consumeObjs := game.CalcTechConsume(techInfo, techData, levelConf.Cost, res.Err)
	if res.Err.Id != 0 {
		return
	}

	game.CheckTechConsume(consumeObjs, r, res.Err)
	if res.Err.Id != 0 {
		return
	}

	game.SubTechConsume(consumeObjs, r, res.Err)
	if res.Err.Id != 0 {
		return
	}

	r.TechQueueCount++
	techInfo.State = msg.TechState_TECH_UPGRADING
	// level 不变
	now := time3.Now().Unix()
	techInfo.StartTime = now

	// save
	c.MustUpdate(r.Id, r)

	game.TryAppendSyncObjsMsgWithConsumes(ctx, r, nil, consumeObjs)
	game.DealTechForClient(techInfo, now)
	res.Info = techInfo
	return
}

func (*HandlerHttp) EndUpgradeTech(ctx context.Context, req *msg.EndUpgradeTechR) (res *msg.EndUpgradeTechA, err error) {
	err = nil
	res = &msg.EndUpgradeTechA{
		Err:  &msg.ErrorInfo{},
		Info: nil,
	}

	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}

	var c redisdb.PlayerInfoCache
	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}

	techInfo := game.GetTechInfo(r, req.Id)
	if techInfo == nil {
		res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "tech info is nil"
		return
	}

	game.CheckTechData(req.Id, techInfo.Level, techInfo.StartTime, res.Err, false)
	if res.Err.Id != 0 {
		return
	}

	game.CheckTechState(techInfo, msg.TechState_TECH_UPGRADING, res.Err)

	if res.Err.Id != 0 {
		return
	}

	game.DoLevelUp(r, techInfo, res.Err)
	if res.Err.Id != 0 {
		return
	}

	// save
	c.MustUpdate(r.Id, r)

	game.DealTechForClient(techInfo, time3.Now().Unix())
	res.Info = techInfo
	return
}

func (*HandlerHttp) InstantUpgradeTech(ctx context.Context, req *msg.InstantUpgradeTechR) (res *msg.InstantUpgradeTechA, err error) {
	err = nil
	res = &msg.InstantUpgradeTechA{
		Err:  &msg.ErrorInfo{},
		Info: nil,
	}

	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}

	var c redisdb.PlayerInfoCache
	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}

	if !game.MakeSureTechExist(r) {
		return
	}

	techData, _ := conf.GetTechnologyConfig(req.Id)

	game.CheckConfData(techData, int32(req.Id), res.Err)
	if res.Err.Id != 0 {
		return
	}

	techInfo := game.GetTechInfo(r, req.Id)
	if techInfo == nil {
		techInfo = game.AddTech(r, req.Id, res.Err)
	}
	//立即完成不需要状态检测直接过
	/*if 0 == techInfo.Level {
		game.CheckTechState(techInfo, msg.TechState_TECH_LOCK, res.Err)
	} else {
		game.CheckTechState(techInfo, msg.TechState_TECH_NORMAL, res.Err)
	}*/
	if res.Err.Id != 0 {
		return
	}

	game.CheckTechCondition(techInfo, techData, r, res.Err)
	if res.Err.Id != 0 {
		return
	}

	levelConf, _ := game.GetTechLevelConf(techData, techInfo.Level, res.Err)
	if res.Err.Id != 0 {
		return
	}

	if techInfo.Level+1 > techData.MaxLevel {
		res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "building upto max level"
		return
	}

	baseConsumeObjs := game.CalcTechConsume(techInfo, techData, levelConf.Cost, res.Err)
	if res.Err.Id != 0 {
		return
	}
	game.CheckTechConsume(baseConsumeObjs, r, res.Err)
	if res.Err.Id != 0 {
		return
	}

	consumeObjs := game.CalcTechConsume(techInfo, techData, levelConf.DirectlyCompleteCost, res.Err)
	if res.Err.Id != 0 {
		return
	}

	game.CheckTechConsume(consumeObjs, r, res.Err)
	if res.Err.Id != 0 {
		return
	}

	game.SubTechConsume(baseConsumeObjs, r, res.Err)
	if res.Err.Id != 0 {
		return
	}

	game.SubTechConsume(consumeObjs, r, res.Err)
	if res.Err.Id != 0 {
		return
	}

	game.DoLevelUp(r, techInfo, res.Err)
	if res.Err.Id != 0 {
		return
	}

	// save
	c.MustUpdate(r.Id, r)

	consumeObjs = append(consumeObjs, baseConsumeObjs...)
	game.TryAppendSyncObjsMsgWithConsumes(ctx, r, nil, consumeObjs)

	game.DealTechForClient(techInfo, time3.Now().Unix())
	res.Info = techInfo
	return
}
