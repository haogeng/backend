package game

import (
	"context"
	"server/internal/game"
	"server/internal/gconst"
	"server/pkg/fungin/xmiddleware"
	"server/pkg/gen/msg"
	"server/pkg/gen_redis/redisdb"
)

func (*HandlerHttp) WarCopyInfos(ctx context.Context, req *msg.WarDiffInfosR) (res *msg.WarDiffInfosA, err error) {
	res = &msg.WarDiffInfosA{
		Err: &msg.ErrorInfo{},
	}
	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}

	var c redisdb.PlayerInfoCache

	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}
	// 检查初始化章节
	game.InitWarCopy(r, res.Err)
	if res.Err.Id != 0 {
		return
	}
	// 构造章节返回
	res.WarDiffInfos = game.BuildWarCopyInfoResults(r, res.Err)
	if res.Err.Id != 0 {
		return
	}
	c.MustUpdate(r.Id, r)
	return
}

func (*HandlerHttp) WarBoxReward(ctx context.Context, req *msg.WarBoxRewardR) (res *msg.WarBoxRewardA, err error) {
	res = &msg.WarBoxRewardA{
		Err: &msg.ErrorInfo{},
	}
	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}

	var c redisdb.PlayerInfoCache

	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}
	addedObjs := game.GetWarBoxReward(r, req.WarId, req.BoxId, res.Err)
	if res.Err.Id != 0 {
		return
	}
	// final sync inside
	game.TryAppendSyncObjsMsgWithConsume(ctx, r, addedObjs, nil)
	c.MustUpdate(r.Id, r)
	res.WarId = req.WarId
	res.BoxId = req.BoxId
	return
}

func (*HandlerHttp) WarFirstJoin(ctx context.Context, req *msg.FirstJoinWarR) (res *msg.FirstJoinWarA, err error) {
	res = &msg.FirstJoinWarA{
		Err: &msg.ErrorInfo{},
	}
	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}

	var c redisdb.PlayerInfoCache

	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}
	game.UpdateWarFirstJoin(r, req.WarId, req.ChapterId, res.Err)
	if res.Err.Id != 0 {
		return
	}
	c.MustUpdate(r.Id, r)
	return
}
