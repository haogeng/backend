package game

import (
	"bitbucket.org/funplus/golib/deepcopy"
	"context"
	"server/internal/game"
	"server/internal/gconst"
	"server/pkg/fungin/xmiddleware"
	"server/pkg/gen/conf"
	"server/pkg/gen/msg"
	"server/pkg/gen_redis/redisdb"
)

func (*HandlerHttp) ManualRefreshStore(ctx context.Context, req *msg.ManualRefreshStoreR) (res *msg.ManualRefreshStoreA, err error) {
	res = &msg.ManualRefreshStoreA{
		Err: &msg.ErrorInfo{},
	}

	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}

	var c redisdb.PlayerInfoCache

	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}

	// do logic with r

	// judge store is valid
	s := game.GetStoreItem(r, req.StoreId, res.Err)
	if s == nil {
		return
	}

	confData, _ := conf.GetStoreConfig(req.StoreId)
	if !game.CheckConfData(confData, req.StoreId, res.Err) {
		return
	}

	if !confData.IsManualRefresh {
		res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "no support manual"
		return
	}

	// 手动刷新次数
	if s.ManualRefreshCount >= confData.ManualRefreshCount {
		res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "upto max count of manual refresh"
		return
	}

	// check consume
	consume := game.GetStoreManualRefreshConsume(confData, s.ManualRefreshCount+1, res.Err)
	if res.Err.Id != 0 {
		return
	}
	if !game.CheckConsume(r, consume, res.Err) {
		return
	}
	if !game.SubConsume(r, consume, res.Err) {
		return
	}
	s.ManualRefreshCount++

	// do refresh
	game.ManualRefreshStore(r, s)

	// if needed, update r
	c.MustUpdate(r.Id, r)

	// sync if ok
	res.Item = s

	// sync consume
	game.TryAppendSyncObjsMsgWithConsume(ctx, r, nil, consume)

	return
}

func (*HandlerHttp) BuyGood(ctx context.Context, req *msg.BuyGoodR) (res *msg.BuyGoodA, err error) {
	res = &msg.BuyGoodA{
		Err: &msg.ErrorInfo{},
	}

	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}

	var c redisdb.PlayerInfoCache

	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}

	// do logic with r
	_ = game.GetStoreItem(r, req.StoreId, res.Err)
	if res.Err.Id != 0 {
		return
	}

	shelf := game.GetShelfItem(r, req.StoreId, req.ShelfId, res.Err)
	if res.Err.Id != 0 {
		return
	}

	// 在此处sync shelf
	// 引用，，shelf信息变化会同步到Item
	res.Item = shelf
	res.StoreId = req.StoreId

	if shelf.GoodId != req.GoodId {
		res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "good id unmatch"
		return
	}

	if req.Count <= 0 {
		res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "count should be positive"
		return
	}

	// 购买上限判定
	goodConfData, _ := conf.GetGoodsConfig(req.GoodId)
	if !game.CheckConfData(goodConfData, req.GoodId, res.Err) {
		return
	}
	// -1无限；0不卖！
	if goodConfData.MaxNum == 0 {
		res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "good max num is 0, conf err"
		return
	}
	if goodConfData.MaxNum > 0 {
		if shelf.BuyCount+req.Count > goodConfData.MaxNum {
			res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "upto max count"
			return
		}
	}

	// consume
	tempConsumes, _ := game.JsonToPb_StaticObjs(goodConfData.Cost, goodConfData, goodConfData.Id, res.Err)
	if res.Err.Id != 0 {
		return
	}
	consumes := deepcopy.Copy(tempConsumes).([]*msg.StaticObjInfo)
	for i := 0; i < len(consumes); i++ {
		consumes[i].Count = consumes[i].Count * int64(req.Count)
	}
	//log.Debug(consumes)

	if !game.CheckConsumes(r, consumes, res.Err) {
		return
	}
	if !game.SubConsumes(r, consumes, res.Err) {
		return
	}

	// add rewards -- 其实就一个，但是此处统一按数组处理
	tempStaticAwards, _ := game.JsonToPb_StaticObjs(goodConfData.StaticObj, goodConfData, goodConfData.Id, res.Err)
	if res.Err.Id != 0 {
		return
	}
	// 修改：x count
	staticAwards := deepcopy.Copy(tempStaticAwards).([]*msg.StaticObjInfo)
	for i := 0; i < len(staticAwards); i++ {
		staticAwards[i].Count = staticAwards[i].Count * int64(req.Count)
	}
	addAwards, _ := game.AddStaticObjs(r, staticAwards, res.Err)
	if res.Err.Id != 0 {
		return
	}
	//log.Debug(addAwards)

	// 增加购买次数
	shelf.BuyCount = shelf.BuyCount + req.Count

	game.RefreshTaskCount(r, msg.TaskCondType_TCT_ShopBuyCount, 0, 1, 0)

	// if needed, update r
	c.MustUpdate(r.Id, r)

	// sync
	game.TryAppendSyncObjsMsgWithConsumes(ctx, r, addAwards, consumes)

	return
}

func (*HandlerHttp) GetStoreItem(ctx context.Context, req *msg.GetStoreItemR) (res *msg.GetStoreItemA, err error) {
	res = &msg.GetStoreItemA{}

	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}

	var c redisdb.PlayerInfoCache

	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}

	// do logic with r
	// 刷新一次
	game.TryRefreshStore(r, req.Id)

	// if needed, update r
	c.MustUpdate(r.Id, r)

	// sync
	res.Item = game.GetStoreItem(r, req.Id, res.Err)

	return
}

func (*HandlerHttp) GetStoreInfo(ctx context.Context, req *msg.GetStoreInfoR) (res *msg.GetStoreInfoA, err error) {
	res = &msg.GetStoreInfoA{
		Err: &msg.ErrorInfo{},
	}

	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}

	var c redisdb.PlayerInfoCache

	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}
	r2 := game.MustGetPlayerInfoExt(r)

	// do logic with r
	game.TryRefreshStores(r)

	// if needed, update r
	c.MustUpdate(r.Id, r)

	// 和配表取交集
	res.Info = game.GetClientStoreInfoFromOrigin(r2.StoreInfo, req.Type)
	//res.Info = r2.StoreInfo

	return
}
