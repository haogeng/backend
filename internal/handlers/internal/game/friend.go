package game

import (
	"context"
	"server/internal/game"
	"server/internal/gconst"
	"server/pkg/fungin/xmiddleware"
	"server/pkg/gen/msg"
	"server/pkg/gen_redis/redisdb"
)

// 好友模块
// 采用了和常规不一样的思路，使用功能函数内即时存档的方式，不是每个消息里最终存档
// 适用于多玩家交互式数据
func (p *HandlerHttp) GetFriendInfos(ctx context.Context, req *msg.FriendInfosR) (res *msg.FriendInfosA, err error) {
	res = &msg.FriendInfosA{
		Err: &msg.ErrorInfo{},
	}
	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}
	tmpFriendResult := game.GetFriendInfos(uid, res.Err)
	if res.Err.Id != 0 {
		return
	}
	res.FriendInfos = tmpFriendResult.FriendInfos
	res.FriendMaxNum = tmpFriendResult.FriendMaxNum
	res.SendMaxNum = tmpFriendResult.SendMaxNum
	res.GetMaxNum = tmpFriendResult.GetMaxNum
	res.DaySendNum = tmpFriendResult.DaySendNum
	res.DayGetNum = tmpFriendResult.DayGetNum
	res.FriendStatus = tmpFriendResult.FriendStatus
	return
}

func (p *HandlerHttp) GetApplyFriendInfos(ctx context.Context, req *msg.ApplyFriendInfosR) (res *msg.ApplyFriendInfosA, err error) {
	res = &msg.ApplyFriendInfosA{
		Err: &msg.ErrorInfo{},
	}
	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}
	var c redisdb.PlayerInfoCache
	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}
	res.ApplyMaxNum, res.FriendCurNum, res.FriendMaxNum, res.ApplyInfos = game.GetApplyFriendInfos(r)
	return
}

func (p *HandlerHttp) GetBlackFriendInfos(ctx context.Context, req *msg.BlackFriendInfosR) (res *msg.BlackFriendInfosA, err error) {
	res = &msg.BlackFriendInfosA{
		Err: &msg.ErrorInfo{},
	}
	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}
	var c redisdb.PlayerInfoCache
	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}
	res.BlackInfos = game.GetBlackFriendInfos(r)
	return
}

func (p *HandlerHttp) AddBlackFriend(ctx context.Context, req *msg.AddBlackFriendR) (res *msg.AddBlackFriendA, err error) {
	res = &msg.AddBlackFriendA{
		Err: &msg.ErrorInfo{},
	}
	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}

	// 删除我方好友
	game.DeleteFriend(uid, req.BlackFriendId, res.Err)
	if res.Err.Id != 0 {
		return
	}
	// 删除对方好友
	game.DeleteFriend(req.BlackFriendId, uid, res.Err)
	if res.Err.Id != 0 {
		return
	}
	// 拉黑好友
	game.AddBlackFriend(uid, req.BlackFriendId, res.Err)
	if res.Err.Id != 0 {
		return
	}
	res.BlackFriendId = req.BlackFriendId
	return
}

func (p *HandlerHttp) RemoveBlackFriend(ctx context.Context, req *msg.RemoveBlackFriendR) (res *msg.RemoveBlackFriendA, err error) {
	res = &msg.RemoveBlackFriendA{
		Err: &msg.ErrorInfo{},
	}
	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}
	game.RemoveBlackFriend(uid, req.RemoveFriendId, res.Err)
	if res.Err.Id != 0 {
		return
	}
	res.BlackFriendId = req.RemoveFriendId
	return
}

func (p *HandlerHttp) DeleteFriend(ctx context.Context, req *msg.DeleteFriendR) (res *msg.DeleteFriendA, err error) {
	res = &msg.DeleteFriendA{
		Err: &msg.ErrorInfo{},
	}
	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}
	// 删除我方好友
	game.DeleteFriend(uid, req.DeleteFriendId, res.Err)
	if res.Err.Id != 0 {
		return
	}
	// 删除对方好友
	game.DeleteFriend(req.DeleteFriendId, uid, res.Err)
	if res.Err.Id != 0 {
		return
	}
	res.DeleteFriendId = req.DeleteFriendId
	return
}

func (p *HandlerHttp) SendApplyFriend(ctx context.Context, req *msg.SendApplyFriendR) (res *msg.SendApplyFriendA, err error) {
	res = &msg.SendApplyFriendA{
		Err: &msg.ErrorInfo{},
	}
	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}
	game.SendApplyFriend(uid, req.ApplyFriendId, res.Err)
	if res.Err.Id != 0 {
		res.ApplyFriendId = req.ApplyFriendId
		return
	}
	res.ApplyFriendId = req.ApplyFriendId
	return
}

func (p *HandlerHttp) JoinApplyFriend(ctx context.Context, req *msg.JoinApplyFriendR) (res *msg.JoinApplyFriendA, err error) {
	res = &msg.JoinApplyFriendA{
		Err: &msg.ErrorInfo{},
	}
	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}
	if req.JoinApplyUserId <= 0 {
		// 批量接受申请
		game.AutoJoinApplyFriend(uid, res.Err)
	} else {
		// 单独接受申请
		game.JoinApplyFriend(uid, req.JoinApplyUserId, res.Err)
	}
	var c redisdb.PlayerInfoCache
	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}
	res.JoinApplyUserId = req.JoinApplyUserId
	res.ApplyMaxNum, res.FriendCurNum, res.FriendMaxNum, res.ApplyInfos = game.GetApplyFriendInfos(r)
	return
}

func (p *HandlerHttp) DeleteApplyFriend(ctx context.Context, req *msg.DeleteApplyFriendR) (res *msg.DeleteApplyFriendA, err error) {
	res = &msg.DeleteApplyFriendA{
		Err: &msg.ErrorInfo{},
	}

	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}
	if req.DeleteApplyUserId <= 0 {
		// 批量删除申请
		deleteResult := game.AutoDeleteApplyFriend(uid, res.Err)
		if res.Err.Id != 0 {
			return
		}
		res.DeleteApplyUserId = deleteResult
	} else {
		// 单独删除申请
		game.DeleteApplyFriend(uid, req.DeleteApplyUserId, res.Err)
		res.DeleteApplyUserId = append(res.DeleteApplyUserId, req.DeleteApplyUserId)
		if res.Err.Id != 0 {
			return
		}
	}
	return
}

func (p *HandlerHttp) RecommendFriends(ctx context.Context, req *msg.RecommendFriendsR) (res *msg.RecommendFriendsA, err error) {
	res = &msg.RecommendFriendsA{
		Err: &msg.ErrorInfo{},
	}
	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}
	var c redisdb.PlayerInfoCache
	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}
	friendInfos, friendCurNum, friendMaxNum := game.RecommendFriends(ctx, r, res.Err)
	if res.Err.Id != 0 {
		return
	}
	res.RecommendInfos = friendInfos
	res.FriendCurNum = friendCurNum
	res.FriendMaxNum = friendMaxNum
	return
}

func (p *HandlerHttp) SearchPlayerInfo(ctx context.Context, req *msg.SearchPlayerInfoR) (res *msg.SearchPlayerInfoA, err error) {
	res = &msg.SearchPlayerInfoA{
		Err: &msg.ErrorInfo{},
	}
	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}
	var c redisdb.PlayerInfoCache
	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}
	searchInfos, friendCurNum, friendMaxNum := game.SearchPlayerInfo(r, req.SearchKey, res.Err)
	if res.Err.Id != 0 {
		return
	}
	res.SearchInfos = searchInfos
	res.FriendCurNum = friendCurNum
	res.FriendMaxNum = friendMaxNum
	return
}

func (p *HandlerHttp) QuickSendAndGet(ctx context.Context, req *msg.QuickSendAndGetR) (res *msg.QuickSendAndGetA, err error) {
	res = &msg.QuickSendAndGetA{
		Err: &msg.ErrorInfo{},
	}
	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}
	getNum, sendNum, tmpFriendResult, addedObjs := game.QuickSendAndGet(uid, req.UserId, req.Type, res.Err)
	if res.Err.Id != 0 {
		return
	}
	res.FriendInfos = tmpFriendResult.FriendInfos
	res.FriendMaxNum = tmpFriendResult.FriendMaxNum
	res.SendMaxNum = tmpFriendResult.SendMaxNum
	res.GetMaxNum = tmpFriendResult.GetMaxNum
	res.DaySendNum = tmpFriendResult.DaySendNum
	res.DayGetNum = tmpFriendResult.DayGetNum
	res.CurGetNum = getNum
	res.CurSendNum = sendNum

	var c redisdb.PlayerInfoCache
	r := c.Find(uid)
	if r != nil {
		// 刷新爱心红点
		game.RefreshFriendLoveRedPoint(r, true)
		game.TryAppendSyncObjsMsgWithConsume(ctx, r, addedObjs, nil)
	}
	return
}
