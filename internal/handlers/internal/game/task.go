package game

import (
	"context"
	"server/internal/game"
	"server/internal/gconst"
	"server/internal/util"
	"server/pkg/fungin/xmiddleware"
	"server/pkg/gen/conf"
	"server/pkg/gen/msg"
	"server/pkg/gen_redis/redisdb"
)

func (*HandlerHttp) GetTaskReward(ctx context.Context, req *msg.GetTaskRewardR) (res *msg.GetTaskRewardA, err error) {
	res = &msg.GetTaskRewardA{
		Err: &msg.ErrorInfo{},
	}

	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}

	var c redisdb.PlayerInfoCache
	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}

	addedAwards := game.DoTaskAward(r, req.Id, res.Err)
	if res.Err.Id != 0 {
		return
	}

	if addedAwards != nil {
		game.TryAppendSyncObjsMsgWithConsumes(ctx, r, addedAwards, nil)
	}

	// if needed, update r
	c.MustUpdate(r.Id, r)

	res.Id = req.Id
	res.Level = r.Tasks.TaskLevels[req.Id]

	return
}

func (*HandlerHttp) GetTreasureReward(ctx context.Context, req *msg.GetTreasureRewardR) (res *msg.GetTreasureRewardA, err error) {
	res = &msg.GetTreasureRewardA{
		Err: &msg.ErrorInfo{},
	}

	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}

	var c redisdb.PlayerInfoCache
	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}

	// do logic with r
	game.MakeSureTreasureRecvedData(r)

	// type(日/周) --> [1:confId1, 2:confId2...] key -> confId
	treasureKeyConfMap := game.GetTreasureKeyConfMap(req.Type, res.Err)
	if res.Err.Id != 0 {
		return
	}

	// 是否已领取判定
	iType := int32(req.Type)
	recvedList, ok := r.TreasureRecvedData[iType]
	if !ok {
		r.TreasureRecvedData[iType] = &msg.IntList{
			Data: make([]int32, 0),
		}
		recvedList = r.TreasureRecvedData[iType]
	}
	if util.InArray32(recvedList.Data, req.Key) {
		res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "has claimed the rewards"
		return
	}

	// 条件消耗判定
	confId, ok := treasureKeyConfMap[req.Key]
	if !ok {
		res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "not found key in global conf"
		return
	}
	confData, _ := conf.GetTreasureConfig(confId)
	if !game.CheckConfData(confData, confId, res.Err) {
		return
	}
	fakeConsumes, _ := game.JsonToPb_StaticObjs(confData.NeedConds, confData, confId, res.Err)
	if res.Err.Id != 0 {
		return
	}
	if !game.CheckConsumes(r, fakeConsumes, res.Err) {
		return
	}

	// 此处不能扣除，只需要判定
	//if !game.SubConsumes(r, consumes, res.Err) {
	//	return
	//}

	// 奖励
	addedObjs, _ := game.AddStaticObjByObjConf(r, confData.RewardId, res.Err)
	if res.Err.Id != 0 {
		return
	}

	// 加到已领取列表
	r.TreasureRecvedData[iType].Data = append(r.TreasureRecvedData[iType].Data, req.Key)

	game.RefreshClanRedPoint(r, true, nil, msg.RedPointType_RPT_ClanReward)

	// if needed, update r
	c.MustUpdate(r.Id, r)

	// final sync
	game.TryAppendSyncObjsMsgWithConsumes(ctx, r, addedObjs, fakeConsumes)

	// final sync
	game.NotifyTreasureRecvedData(r, req.Type)

	return
}
