package game

import (
	"context"
	"server/internal/game"
	"server/internal/gconst"
	"server/pkg/fungin/xmiddleware"
	"server/pkg/gen/msg"
	"server/pkg/gen_redis/redisdb"
)

func (*HandlerHttp) ClearRedPoint(ctx context.Context, req *msg.ClearRedPointR) (res *msg.ClearRedPointA, err error) {
	res = &msg.ClearRedPointA{
		Err: &msg.ErrorInfo{},
	}

	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}

	var c redisdb.PlayerInfoCache
	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}

	// do logic with r
	game.ClearRedPoint(r, req.RedPoint)

	// if needed, update r
	c.MustUpdate(r.Id, r)

	return
}
