package game

import (
	"context"
	"fmt"
	"server/internal/clan"
	"server/internal/game"
	"server/internal/gconst"
	"server/internal/share"
	"server/internal/util/xsensitive"
	"server/pkg/dbutil"
	"server/pkg/dlock"
	"server/pkg/fungin/xmiddleware"
	"server/pkg/gen/conf"
	"server/pkg/gen/msg"
	"server/pkg/gen_redis/redisdb"
	"strconv"
	"strings"
	"time"
	"unicode/utf8"
)

func (p *HandlerHttp) PlayerLogout(ctx context.Context, req *msg.PlayerLogoutR) (res *msg.PlayerLogoutA, err error) {
	res = &msg.PlayerLogoutA{
		Err: &msg.ErrorInfo{},
	}
	return
}

func (p *HandlerHttp) GameSetupInfo(ctx context.Context, req *msg.GameSetupInfoR) (res *msg.GameSetupInfoA, err error) {
	res = &msg.GameSetupInfoA{
		Err: &msg.ErrorInfo{},
	}
	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}
	var c redisdb.PlayerInfoCache
	r := c.Find(uid)
	if r == nil {
		res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, gconst.ErrNoUser.Error()
		return
	}
	// 修改名字价格
	res.ModifyNamePrice = game.ComputerModifyPriceByName(r, res.Err)
	if res.Err.Id != 0 {
		return
	}
	// 头像列表
	res.IconInfos = game.SelectPlayerIconInfos(r)
	// 总战力
	res.PowerSum = r.TotalPower
	// 工会信息
	rs := share.GetClanId(r.Id)
	if rs != nil {
		res.CInfo = clan.FindClanBrief(rs.ClanId)
	}
	res.UInfo = r
	game.WithClientDataInFinal(res.UInfo, true, msg.FormationType_FT_Friend)
	return
}

func (p *HandlerHttp) ModifyPlayerName(ctx context.Context, req *msg.ModifyPlayerNameR) (res *msg.ModifyPlayerNameA, err error) {
	res = &msg.ModifyPlayerNameA{
		Err: &msg.ErrorInfo{},
	}
	playerRename := &msg.PlayerRenameR{
		NewName: req.Name,
	}
	result, err := p.PlayerRename(ctx, playerRename)
	res.Err = result.Err
	if res.Err.Id != 0 {
		return
	}
	res.Name = result.NewName
	res.ModifyNamePrice = result.ModifyNamePrice
	return
}

func (p *HandlerHttp) ModifyPlayerIcon(ctx context.Context, req *msg.ModifyPlayerIconR) (res *msg.ModifyPlayerIconA, err error) {
	res = &msg.ModifyPlayerIconA{
		Err: &msg.ErrorInfo{},
	}
	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}
	var c redisdb.PlayerInfoCache
	r := c.Find(uid)
	if r == nil {
		res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, gconst.ErrNoUser.Error()
		return
	}
	game.ModifyPlayerIcon(r, req.Icon, res.Err)
	if res.Err.Id != 0 {
		return
	}
	c.MustUpdate(r.Id, r)
	res.Icon = req.Icon
	return
}

func (p *HandlerHttp) ModifyFriendRecommend(ctx context.Context, req *msg.ModifyFriendRecommendR) (res *msg.ModifyFriendRecommendA, err error) {
	res = &msg.ModifyFriendRecommendA{
		Err: &msg.ErrorInfo{},
	}
	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}
	res.Close = req.Close
	game.ModifyFriendRecommend(uid, req.Close, res.Err)
	return
}

// 作为一个模板存在
// replace TestGameReq to xxR
// replace TestGameRes to xxA
func (*HandlerHttp) PlayerRename(ctx context.Context, req *msg.PlayerRenameR) (res *msg.PlayerRenameA, err error) {
	res = &msg.PlayerRenameA{
		Err: &msg.ErrorInfo{},
	}

	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}

	var c redisdb.PlayerInfoCache
	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}
	// 去除首尾空格
	req.NewName = strings.TrimSpace(req.NewName)
	// check len
	{
		length := int32(utf8.RuneCountInString(req.NewName))
		if length < 4 {
			res.Err.Id, res.Err.DebugMsg = 1001, "字数触发最小限制"
			return
		}

		if length > 16 {
			res.Err.Id, res.Err.DebugMsg = 1002, "字数触发最大限制"
			return
		}
	}

	// trim space ..
	{

	}

	// check bad word
	{
		badWord, hasBad := xsensitive.ContainsBadWord(req.NewName)
		if hasBad {
			res.Err.Id, res.Err.DebugMsg = 2011, "您输入的内容不合法，请重新输入"
			res.Err.Params = append(res.Err.Params, badWord)
			return
		}
	}

	// check rename count
	//{
	//	if r.RenameCount > 0 {
	//		res.Err.Id, res.Err.DebugMsg = msg.Ecode_RENAME_COUNT_OVERFLOW, "rename count overflow"
	//		return
	//	}
	//}

	// 检查新名字是否和老名字相同
	if req.NewName == r.Name {
		res.Err.Id, res.Err.DebugMsg = 2321, "新名字不能和老名字相同"
		return
	}

	globalConfig, _ := conf.GetGlobalConfig(1)
	if !game.CheckConfData(globalConfig, 1, &msg.ErrorInfo{}) {
		return
	}
	// 检查新名字不能和初始名字相同
	if strconv.FormatInt(int64(globalConfig.InitUserName), 10) == req.NewName {
		res.Err.Id, res.Err.DebugMsg = 2011, "您输入的内容不合法，请重新输入"
		return
	}

	// check unique in global
	escapedName := dbutil.Escape(req.NewName)
	// 唯一昵称
	nameReal := fmt.Sprintf("S%d-%s", r.ServerId, escapedName)
	{
		// 以该名字为key加分布式锁
		lockKey := "gmx:lock:playername:" + nameReal
		lockValue, lockOk := dlock.AcquireLockX(lockKey, time.Second*10)
		if !lockOk {
			res.Err.Id, res.Err.DebugMsg = -1, "get lock failed"
			return
		}
		defer func() {
			if lockOk {
				dlock.ReleaseLockX(lockKey, lockValue)
			}
		}()
	}
	// 检查唯一昵称是否存在
	{
		existedId := game.GetPlayerIdByNameReal(nameReal)
		if existedId != 0 {
			res.Err.Id, res.Err.DebugMsg = msg.Ecode_HAS_EXIST_NAME, "has exist name"
			return
		}
	}

	// 改名扣钱
	consumes := game.ComputerModifyPriceByName(r, res.Err)
	if res.Err.Id != 0 {
		return
	}
	result := game.CheckConsume(r, consumes, res.Err)
	if !result {
		return
	}
	consumesResult := game.SubConsume(r, consumes, res.Err)
	if !consumesResult {
		return
	}
	r.ModifyNameNum++
	r.Name = escapedName
	r.NameReal = nameReal

	res.RenameCount = r.ModifyNameNum
	res.NewName = r.Name
	// 修改名字价格
	res.ModifyNamePrice = game.ComputerModifyPriceByName(r, res.Err)

	// if needed, update r
	c.MustUpdate(r.Id, r)

	game.TryAppendSyncObjsMsgWithConsume(ctx, r, nil, consumes)

	return
}

func (*HandlerHttp) ModifyPlayerAutoLayoff(ctx context.Context, req *msg.ModifyPlayerAutoLayoffR) (res *msg.ModifyPlayerAutoLayoffA, err error) {
	res = &msg.ModifyPlayerAutoLayoffA{
		Err: &msg.ErrorInfo{},
	}

	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}

	var c redisdb.PlayerInfoCache
	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}

	// do logic with r
	r.AutoLayoff = req.AutoLayoff

	// if needed, update r
	c.MustUpdate(r.Id, r)

	res.AutoLayoff = req.AutoLayoff

	return
}
