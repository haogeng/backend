package game

import (
	"context"
	"server/internal/gconst"
	"server/pkg/fungin/xmiddleware"
	"server/pkg/gen/msg"
	"server/pkg/gen_redis/redisdb"
)

func (*HandlerHttp) SetCurGuideIndex(ctx context.Context, req *msg.SetCurGuideIndexR) (res *msg.SetCurGuideIndexA, err error) {
	res = &msg.SetCurGuideIndexA{
		Err: &msg.ErrorInfo{},
	}

	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}

	var c redisdb.PlayerInfoCache
	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}

	// do logic with r
	r.CurGuideIndex = req.Index

	// if needed, update r
	c.MustUpdate(r.Id, r)

	return
}

func (*HandlerHttp) SetGuideState(ctx context.Context, req *msg.SetGuideStateR) (res *msg.SetGuideStateA, err error) {
	res = &msg.SetGuideStateA{
		Err: &msg.ErrorInfo{},
	}

	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}

	var c redisdb.PlayerInfoCache
	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}

	// do logic with r
	if r.GuideState == nil {
		r.GuideState = make(map[int32]int32)
	}
	r.GuideState[req.Index] = req.State

	// if needed, update r
	c.MustUpdate(r.Id, r)

	return
}
