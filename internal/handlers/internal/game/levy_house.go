package game

import (
	"context"
	"server/internal/game"
	"server/internal/gconst"
	"server/internal/log"
	"server/internal/util/time3"
	"server/pkg/fungin/xmiddleware"
	"server/pkg/gen/conf"
	"server/pkg/gen/msg"
	"server/pkg/gen_redis/redisdb"
)

func (*HandlerHttp) OnHookIncome(ctx context.Context, req *msg.OnHookIncomeR) (res *msg.OnHookIncomeA, err error) {
	err = nil
	res = &msg.OnHookIncomeA{
		Err:              &msg.ErrorInfo{},
		OnHookIncomeTime: 0,
	}

	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}

	var c redisdb.PlayerInfoCache
	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}
	buildingInfo, buildingConf := game.GetBuildingInfo(r, res.Err)
	if res.Err.Id != 0 {
		return
	}
	if res.Err.Id != 0 {
		log.Debugf("CheckOnHookCondition GetBuildingInfo err %d", res.Err.Id)
		return
	}
	onHookEffectTime := game.GetOnHookTime(r, res.Err)
	if res.Err.Id != 0 {
		log.Debugf("CheckOnHookTime err %d", res.Err.Id)
		return
	}

	objs := game.CalcOnHookIncome(r, int32(onHookEffectTime), buildingInfo, buildingConf, res.Err)

	if res.Err.Id != 0 {
		log.Debugf("OnHookIncome err %d", res.Err.Id)
		return
	}
	if len(objs) == 0 {
		return
	}
	r.LevyHouses.OnHookIncomeTime = time3.Now().Unix()
	log.Debugf("----OnHookIncome time %d----", r.LevyHouses.OnHookIncomeTime)

	addedObjs := game.MergeObjs(r, objs, res.Err)
	game.TryAppendSyncObjsMsgWithConsumes(ctx, r, addedObjs, nil)

	// save
	c.MustUpdate(r.Id, r)
	res.OnHookIncomeTime = r.LevyHouses.OnHookIncomeTime
	return
}

func (*HandlerHttp) QuickOnHook(ctx context.Context, req *msg.QuickOnHookR) (res *msg.QuickOnHookA, err error) {
	err = nil
	res = &msg.QuickOnHookA{
		Err:                  &msg.ErrorInfo{},
		QuickOnHookTimes:     0,
		FreeQuickOnHookTimes: 0,
	}

	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}

	var c redisdb.PlayerInfoCache
	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}

	buildingInfo, buildingConf := game.GetBuildingInfo(r, res.Err)
	if res.Err.Id != 0 {
		return
	}

	curOnHookTimes := r.LevyHouses.QuickOnHookTimes
	// 策划次数从1开始，程序从0开始
	consume := game.GetQuickOnHookCost(curOnHookTimes+1, res.Err)
	if !game.CheckConsumes(r, consume, res.Err) {
		return
	}

	objs := game.CalcQuickOnHook(r, buildingInfo, buildingConf, res.Err)
	if res.Err.Id != 0 {
		log.Debugf("QuickOnhook err %d", res.Err.Id)
		//internal.Logger.Debug()
		return
	}

	globalConf, ok := conf.GetGlobalConfig(1)
	if !ok {
		panic("globalConf is nil")
	}
	if r.LevyHouses.QuickOnHookTimes > globalConf.QHUFreeCount {
		game.SubConsumes(r, consume, res.Err)
	}

	addedObjs := game.MergeObjs(r, objs, res.Err)
	game.TryAppendSyncObjsMsgWithConsumes(ctx, r, addedObjs, consume)

	game.RefreshTaskCount(r, msg.TaskCondType_TCT_HookupCount, 0, 1, 0)

	// save
	c.MustUpdate(r.Id, r)

	res.QuickOnHookTimes = r.LevyHouses.QuickOnHookTimes
	res.FreeQuickOnHookTimes = r.LevyHouses.FreeQuickOnHookTimes
	return
}
