package game

import (
	"context"
	"server/internal/game"
	"server/internal/gconst"
	"server/internal/share"
	"server/pkg/fungin/xmiddleware"
	"server/pkg/gen/msg"
	"server/pkg/gen_redis/redisdb"
)

func (*HandlerHttp) MailInfo(ctx context.Context, req *msg.MailInfoR) (res *msg.MailInfoA, err error) {
	res = &msg.MailInfoA{
		Err: &msg.ErrorInfo{},
	}

	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}

	var c redisdb.PlayerInfoCache

	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}
	r2 := game.MustGetPlayerInfoExt(r)

	// do logic with r
	// 触发自动添加邮件逻辑
	game.AutoAddMailFromConfScan(r)
	game.AutoAddMailFromGmBuffer(r)
	game.AutoDelMailByDeadline(r)

	// 从共享里获取邮件
	mailsFromShare := share.PopAllMailFromShare(r.Id)
	for i := 0; i < len(mailsFromShare); i++ {
		game.AddMail(r, mailsFromShare[i], &msg.ErrorInfo{})
	}
	// dont use rs anymore

	// todo 需要根据最后一次修改时间戳，决定要不要全部推送客户端新邮件...
	// 或者根据是否重复加的标记等确定要不要新增邮件
	// 系统过程中有新邮件，通过redpoint告知客户端，客户端在此处拉取真正的邮件列表

	game.RefreshMailRedPoint(r, true)

	// if needed, update r
	c.MustUpdate(r.Id, r)

	// sync
	res.MailList = r2.MailQueue

	return
}

func (*HandlerHttp) RecvMailReward(ctx context.Context, req *msg.RecvMailRewardR) (res *msg.RecvMailRewardA, err error) {
	res = &msg.RecvMailRewardA{
		Err: &msg.ErrorInfo{},
	}

	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}

	var c redisdb.PlayerInfoCache

	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}

	// do logic with r
	_, addedObjs := game.DoRecvMailReward(r, []int32{req.Id}, false, res.Err)
	if res.Err.Id != 0 {
		return
	}

	game.RefreshMailRedPoint(r, true)

	// if needed, update r
	c.MustUpdate(r.Id, r)

	// final sync
	game.TryAppendSyncObjsMsgWithConsume(ctx, r, addedObjs, nil)

	return
}

// 该功能不纯粹，既是一键领取也是读取
// 一键领取，如果没有奖励，而且可以一键读取，也状态也更为读取
func (*HandlerHttp) RecvMailRewardAll(ctx context.Context, req *msg.RecvMailRewardAllR) (res *msg.RecvMailRewardAllA, err error) {
	res = &msg.RecvMailRewardAllA{
		Err: &msg.ErrorInfo{},
	}

	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}

	var c redisdb.PlayerInfoCache

	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}
	r2 := game.MustGetPlayerInfoExt(r)

	// 生成可以一键领取的列表
	reqIds := []int32{}
	for i := 0; i < len(r2.MailQueue); i++ {
		m := r2.MailQueue[i]
		// 有附件，已领取
		// 只有有奖励才判断领取状态
		if len(m.Rewards) > 0 && m.IsRecved {
			continue
		}
		// 没附件，已读取
		if len(m.Rewards) == 0 && m.IsReaded {
			continue
		}

		if !m.OneClickReceive {
			continue
		}
		reqIds = append(reqIds, m.Id)
	}

	// do logic with r
	// 支持一键领取与否，由客户端判定，，服务器按说无所谓，不用判定
	okIds, addedObjs := game.DoRecvMailReward(r, reqIds, true, res.Err)
	if res.Err.Id != 0 {
		return
	}

	game.RefreshMailRedPoint(r, true)

	// if needed, update r
	c.MustUpdate(r.Id, r)

	// 事务，，全成功或全失败
	res.Ids = okIds

	// final sync
	game.TryAppendSyncObjsMsgWithConsume(ctx, r, addedObjs, nil)

	return
}

func (*HandlerHttp) ReadMail(ctx context.Context, req *msg.ReadMailR) (res *msg.ReadMailA, err error) {
	res = &msg.ReadMailA{
		Err: &msg.ErrorInfo{},
	}

	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}

	var c redisdb.PlayerInfoCache

	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}

	// do logic with r
	m := game.GetMail(r, req.Id, res.Err)
	if res.Err.Id != 0 {
		return
	}

	// 更改，，无论如何都能已读取
	//if !m.IsRecved {
	//	// 未领取奖励的邮件无法修改已读状态
	//	res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "mail has not been received"
	//	return
	//}
	m.IsReaded = true

	game.RefreshMailRedPoint(r, true)

	// if needed, update r
	c.MustUpdate(r.Id, r)

	return
}

func (*HandlerHttp) DelMail(ctx context.Context, req *msg.DelMailR) (res *msg.DelMailA, err error) {
	res = &msg.DelMailA{
		Err: &msg.ErrorInfo{},
	}

	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}

	var c redisdb.PlayerInfoCache

	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}

	m := game.GetMail(r, req.Id, res.Err)
	if res.Err.Id != 0 {
		return
	}

	if !m.IsRecved {
		res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "mail has not been received"
		return
	}

	if !m.SupportManualDel {
		res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "dont support manual del"
		return
	}

	// do logic with r
	game.RmMail(r, req.Id)

	game.RefreshMailRedPoint(r, true)

	// if needed, update r
	c.MustUpdate(r.Id, r)

	return
}

func (*HandlerHttp) DelMailAll(ctx context.Context, req *msg.DelMailAllR) (res *msg.DelMailAllA, err error) {
	res = &msg.DelMailAllA{
		Err: &msg.ErrorInfo{},
	}

	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}

	var c redisdb.PlayerInfoCache

	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}
	r2 := game.MustGetPlayerInfoExt(r)

	// deepcopy just for safe
	idsCopy := make([]int32, 0, len(r2.MailQueue))
	for i := 0; i < len(r2.MailQueue); i++ {
		idsCopy = append(idsCopy, r2.MailQueue[i].Id)
	}

	// do logic with r
	for i := 0; i < len(idsCopy); i++ {
		// ignore un exist
		m := game.GetMail(r, idsCopy[i], &msg.ErrorInfo{})
		if m == nil {
			continue
		}

		// 不管读不读，只要领取了都一键删除
		if !m.IsRecved {
			continue
		}

		if !m.OneClickDelete {
			continue
		}
		if !m.SupportManualDel {
			continue
		}

		game.RmMail(r, idsCopy[i])
		res.Ids = append(res.Ids, idsCopy[i])
	}

	game.RefreshMailRedPoint(r, true)

	// if needed, update r
	c.MustUpdate(r.Id, r)

	return
}
