package game

import (
	"context"
	"server/internal/game"
	"server/internal/gconst"
	"server/internal/log"
	"server/pkg/fungin/xmiddleware"
	"server/pkg/gen/conf"
	"server/pkg/gen/msg"
	"server/pkg/gen_redis/redisdb"
)

func (*HandlerHttp) GetMazeInfo(ctx context.Context, req *msg.GetMazeInfoR) (res *msg.GetMazeInfoA, err error) {
	err = nil
	res = &msg.GetMazeInfoA{
		Err: &msg.ErrorInfo{},
	}

	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}

	var c redisdb.PlayerInfoCache
	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}

	game.ResetMaze(r, res.Err)
	if res.Err.Id != 0 {
		return
	}
	maze := game.MustGetPlayerInfoMaze(r)
	res.MazeInfo = maze
	c.MustUpdate(r.Id, r)
	return
}

func (*HandlerHttp) StartMazeEvent(ctx context.Context, req *msg.StartMazeEventR) (res *msg.StartMazeEventA, err error) {
	err = nil
	res = &msg.StartMazeEventA{
		Err: &msg.ErrorInfo{},
	}

	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}

	var c redisdb.PlayerInfoCache
	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}

	artifactObjs := game.StartMazeEvent(r, req.ResurrectTroopId, req.LevelId, req.RestoreHp, req.RestoreEnergy, res.Err)

	if res.Err.Id != 0 {
		return
	}

	c.MustUpdate(r.Id, r)

	if len(artifactObjs) > 0 {
		res.ArtifactObjs = artifactObjs
		//game.TryAppendSyncObjsMsgWithConsume(ctx, r, artifactObjs, nil)
	}
	return
}

func (*HandlerHttp) GetMazeRelic(ctx context.Context, req *msg.GetMazeRelicR) (res *msg.GetMazeRelicA, err error) {
	err = nil
	res = &msg.GetMazeRelicA{
		Err: &msg.ErrorInfo{},
	}

	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}

	var c redisdb.PlayerInfoCache
	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}

	maze := game.MustGetPlayerInfoMaze(r)

	for _, v := range maze.RelicObjs {
		res.RelicIds = append(res.RelicIds, v.Id)
	}
	return
}

func (*HandlerHttp) RecvMazeRelic(ctx context.Context, req *msg.RecvMazeRelicR) (res *msg.RecvMazeRelicA, err error) {
	err = nil
	res = &msg.RecvMazeRelicA{
		Err: &msg.ErrorInfo{},
	}

	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}

	var c redisdb.PlayerInfoCache
	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}

	maze := game.MustGetPlayerInfoMaze(r)

	if _, ok := maze.StaticRelicObjs[req.LevelId]; !ok {
		res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "staticRelicObjs level error"
		return
	}

	willGetStaticObjs := maze.StaticRelicObjs[req.LevelId].RelicObjInfos

	var staticRelicObjs []*msg.StaticObjInfo
	for i := 0; i < len(willGetStaticObjs); i++ {
		if req.RelicId != willGetStaticObjs[i].Id {
			continue
		}

		staticRelicObjs = append(staticRelicObjs, willGetStaticObjs[i])
		break
	}
	if len(staticRelicObjs) == 0 {
		log.Debugf("staticRelicObjs is may be error")
		return
	}
	addedObjs, ok := game.AddStaticObjs(r, staticRelicObjs, res.Err)
	if !ok {
		res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "staticRelicObjs AddStaticObjs error"
		return
	}

	maze.RelicObjs = append(maze.RelicObjs, addedObjs...)

	maze.StaticRelicObjs[req.LevelId].RelicObjInfos = nil
	delete(maze.StaticRelicObjs, req.LevelId)

	game.TryAppendSyncObjsMsgWithConsume(ctx, r, addedObjs, nil)

	res.RelicObjs = maze.RelicObjs

	c.MustUpdate(r.Id, r)
	return
}

func (*HandlerHttp) MazeRecruit(ctx context.Context, req *msg.MazeRecruitR) (res *msg.MazeRecruitA, err error) {
	err = nil
	res = &msg.MazeRecruitA{
		Err: &msg.ErrorInfo{},
	}

	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}

	var c redisdb.PlayerInfoCache
	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}

	levelId := req.LevelId
	troopObjId := req.TroopId

	game.Recruit(r, levelId, troopObjId, res.Err)
	if res.Err.Id != 0 {
		return
	}
	maze := game.MustGetPlayerInfoMaze(r)

	res.TroopInfo = maze.TroopInfo

	c.MustUpdate(r.Id, r)
	return
}

func (*HandlerHttp) TravelSellerReset(ctx context.Context, req *msg.TravelSellerResetR) (res *msg.TravelSellerResetA, err error) {
	err = nil
	res = &msg.TravelSellerResetA{
		Err: &msg.ErrorInfo{},
	}

	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}

	var c redisdb.PlayerInfoCache
	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}

	maze := game.MustGetPlayerInfoMaze(r)
	game.CheckEventType(maze, req.LevelId, msg.MazeEventType_ME_TravelSaleler, res.Err)
	if res.Err.Id != 0 {
		return
	}
	game.NextLevelId(maze, req.LevelId)

	game.TryRefreshStore(r, game.TravelSellerShop)

	c.MustUpdate(r.Id, r)

	return
}

func (*HandlerHttp) ClearLayer(ctx context.Context, req *msg.ClearLayerR) (res *msg.ClearLayerA, err error) {
	err = nil
	res = &msg.ClearLayerA{
		Err: &msg.ErrorInfo{},
	}

	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}

	var c redisdb.PlayerInfoCache
	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}

	objs := game.ClearLayer(r, req.LayerId, res.Err)

	if res.Err.Id != 0 {
		return
	}
	c.MustUpdate(r.Id, r)

	if len(objs) > 0 {
		game.TryAppendSyncObjsMsgWithConsume(ctx, r, objs, nil)
	}
	return
}

func (*HandlerHttp) MazeMode(ctx context.Context, req *msg.MazeModeR) (res *msg.MazeModeA, err error) {
	err = nil
	res = &msg.MazeModeA{
		Err: &msg.ErrorInfo{},
	}

	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}

	var c redisdb.PlayerInfoCache
	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}

	game.MazeMode(r, msg.MazeModeType(req.ModeType), res.Err)

	if res.Err.Id != 0 {
		return
	}
	c.MustUpdate(r.Id, r)
	return
}

func (*HandlerHttp) EventRefresh(ctx context.Context, req *msg.EventRefreshR) (res *msg.EventRefreshA, err error) {
	err = nil
	res = &msg.EventRefreshA{
		Err: &msg.ErrorInfo{},
	}

	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}

	var c redisdb.PlayerInfoCache
	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}

	maze := game.MustGetPlayerInfoMaze(r)
	res.LevelEvent = maze.LevelEvent
	res.NewLevelId = maze.CurLevelId

	//c.MustUpdate(r.Id, r)
	return
}

//复活药剂,所以等级大于1的hp和energy 100%
func (*HandlerHttp) ResurrectPotion(ctx context.Context, req *msg.ResurrectPotionR) (res *msg.ResurrectPotionA, err error) {
	err = nil
	res = &msg.ResurrectPotionA{
		Err: &msg.ErrorInfo{},
	}

	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}

	var c redisdb.PlayerInfoCache
	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}

	maze := game.MustGetPlayerInfoMaze(r)
	//req.LevelId
	/*eventType := game.GetEventByLevel(maze.EventIds, req.LevelId)
	if eventType != int32(msg.MazeEventType_ME_Resurrection) {
		res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "event type error"
		return
	}*/
	//1002 是复活药剂策划说不会变,变了 todo
	consumeStatic := &msg.StaticObjInfo{
		Id:   int32(game.ResurrectPotionId),
		Type: int32(msg.ObjType_OT_Item),
	}

	if len(maze.DeadTroops) == 0 {
		res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "has  no dead troops"
		return
	}
	//newDeadTroops := make([]int32, 1)
	for k, _ := range req.RestoreHp {
		_, ok := conf.GetNPCConfig(k)
		if ok {
			log.Debugf("ResurrectPotion npcData.Id %d", k)
			continue
		}
		troop := game.GetTroopAnyway(r, k, res.Err)
		if res.Err.Id != 0 {
			continue
		}
		if troop.Level <= 1 {
			continue // 注释 for test
		}

		deadIndex := game.GetDeadIndex(maze.DeadTroops, k)
		if deadIndex < 0 {
			continue
		}

		consumeStatic.Count++
		maze.TroopReduceHp[k] = 0
		maze.DeadTroops = append(maze.DeadTroops[:deadIndex], maze.DeadTroops[deadIndex+1:]...)
	}

	//maze.DeadTroops = newDeadTroops
	game.CheckConsume(r, consumeStatic, res.Err)
	if res.Err.Id != 0 {
		return
	}

	for k, v := range req.RestoreEnergy {
		_, ok := conf.GetNPCConfig(k)
		if ok {
			log.Debugf("ResurrectPotion RestoreEnergy npcData.Id %d", k)
			continue
		}
		_ = game.GetTroopAnyway(r, k, res.Err)
		if res.Err.Id != 0 {
			continue
		}
		maze.TroopEnergy[k] = v
	}

	game.SubConsume(r, consumeStatic, res.Err)
	if res.Err.Id != 0 {
		return
	}

	c.MustUpdate(r.Id, r)

	game.TryAppendSyncObjsMsgWithConsume(ctx, r, nil, consumeStatic)

	return
}
