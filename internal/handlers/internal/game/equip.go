package game

import (
	"context"
	"server/internal/game"
	"server/internal/gconst"
	"server/internal/util"
	"server/pkg/fungin/xmiddleware"
	"server/pkg/gen/conf"
	"server/pkg/gen/msg"
	"server/pkg/gen_redis/redisdb"
)

func (*HandlerHttp) LevelUpEquip(ctx context.Context, req *msg.LevelUpEquipR) (res *msg.LevelUpEquipA, err error) {
	res = &msg.LevelUpEquipA{
		Err: &msg.ErrorInfo{},
	}

	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}

	var c redisdb.PlayerInfoCache
	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}

	// do logic with r
	// 目标装备
	e := game.GetEquip(r, req.Id, res.Err)
	if res.Err.Id != 0 {
		return
	}

	eConf, _ := conf.GetEquipConfig(e.ConfigId)
	if !game.CheckConfData(eConf, e.ConfigId, res.Err) {
		return
	}

	// 材料中不能包含目标
	if util.InArray32(req.ConsumedEquipIds, req.Id) {
		res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "consume include dest target"
		return
	}

	oldLevel := e.Level
	// 满级判定
	if e.Level >= eConf.MaxLv {
		res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "upto max lv"
		return
	}

	if len(req.ConsumedEquipIds) == 0 {
		res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "consumed is nil"
		return
	}

	// 消耗品提供的经验
	var expSupply int32
	// 计算经验，同时检测 材料必须存在，且只能在背包中
	for i := 0; i < len(req.ConsumedEquipIds); i++ {
		tempId := req.ConsumedEquipIds[i]
		tempE := game.GetEquip(r, tempId, res.Err)
		// judge tempE exist by errId
		if res.Err.Id != 0 {
			return
		}

		// 身上的不能吃
		if tempE.OwnerId != 0 {
			res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "couldn't consume equipped equip"
			return
		}

		// 状态判定
		if tempE.Status != 0 {
			res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "material equip locked"
			return
		}

		tempConfData, _ := conf.GetEquipConfig(tempE.ConfigId)
		if !game.CheckConfData(tempConfData, tempE.ConfigId, res.Err) {
			return
		}

		tempLevelConfData := game.GetEquipLevelConfData(tempConfData.Id, tempE.Level, res.Err)
		if res.Err.Id != 0 {
			return
		}

		levelExp := tempLevelConfData.TotalExp - tempLevelConfData.Exp
		if levelExp < 0 {
			res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "conf exp data err"
			return
		}

		// 基础经验+等级经验+剩余经验
		tempExp := tempConfData.Exp + levelExp + tempE.Exp
		expSupply = expSupply + tempExp
	}

	// 计算金币消耗
	globalConf, _ := conf.GetGlobalConfig(1)
	coefficient, ok := globalConf.EnhanceGoldCoefficient[e.Star]
	if !ok {
		res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "not find coefficient in global conf"
		return
	}

	// 升到满级后多余的经验不消耗金币，，也不会累积到装备身上
	e.Exp += expSupply
	for ; e.Level < eConf.MaxLv; e.Level++ {
		eLevelConfData := game.GetEquipLevelConfData(eConf.Id, e.Level, res.Err)
		if res.Err.Id != 0 {
			return
		}

		if e.Exp < eLevelConfData.Exp {
			break
		}

		// level up sub consume exp
		e.Exp -= eLevelConfData.Exp
	}
	// 最大等级了 多余经验清0
	if e.Level == eConf.MaxLv {
		expSupply -= e.Exp
		e.Exp = 0
	}

	gold := expSupply * coefficient

	// judge gold consume
	consume := &msg.StaticObjInfo{
		Id:    int32(msg.CurrencyId_CI_Gold),
		Type:  int32(msg.ObjType_OT_Currency),
		Count: int64(gold),
	}
	if !game.CheckConsume(r, consume, res.Err) {
		return
	}
	if !game.SubConsume(r, consume, res.Err) {
		return
	}

	// del equip
	for i := 0; i < len(req.ConsumedEquipIds); i++ {
		game.DelEquip(r, req.ConsumedEquipIds[i], res.Err)
		if res.Err.Id != 0 {
			return
		}
	}

	if e.Level > oldLevel {
		game.RefreshTaskCount(r, msg.TaskCondType_TCT_EquipStrengthCount, 0, 1, 0)
	}

	// 刷新战力
	if e.OwnerId != 0 {
		game.RefreshTroopPowerById(r, e.OwnerId, true)
	}

	// if needed, update r
	c.MustUpdate(r.Id, r)

	// sync
	changedKeys := game.MakeChangeKeysFromEquipIds(req.ConsumedEquipIds)
	ck2 := game.MakeChangeKeysFromStaticObj([]*msg.StaticObjInfo{consume})
	changedKeys = append(changedKeys, ck2...)
	game.TryAppendSyncObjsMsgWithChangedIds(ctx, r, nil, changedKeys)

	res.Equip = e

	return
}
func (*HandlerHttp) LockEquip(ctx context.Context, req *msg.LockEquipR) (res *msg.LockEquipA, err error) {
	res = &msg.LockEquipA{
		Err: &msg.ErrorInfo{},
	}

	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}

	var c redisdb.PlayerInfoCache
	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}

	// do logic with r
	// check status
	if req.LockStatus != 0 && req.LockStatus != 1 {
		res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "status err"
		return
	}

	e := game.GetEquip(r, req.Id, res.Err)
	if res.Err.Id != 0 {
		return
	}

	e.Status = req.LockStatus

	// if needed, update r
	c.MustUpdate(r.Id, r)

	return
}

func (*HandlerHttp) ChangeEquip(ctx context.Context, req *msg.ChangeEquipR) (res *msg.ChangeEquipA, err error) {
	res = &msg.ChangeEquipA{
		Err: &msg.ErrorInfo{},
	}

	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}

	var c redisdb.PlayerInfoCache
	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}

	// do logic with r
	// check troop
	_ = game.GetTroop(r, req.TroopId, res.Err)
	if res.Err.Id != 0 {
		return
	}

	// 牵涉到的装备，用于同步信息
	changedEquipIdMap := make(map[int32]bool)

	// change equip
	for slotType, newEquipId := range req.ChangeInfo {
		// check slot
		if !game.CheckEquipSlotType(slotType, res.Err) {
			return
		}

		// Check new equip exist
		if newEquipId != 0 {
			_ = game.GetEquip(r, newEquipId, res.Err)
			if res.Err.Id != 0 {
				return
			}
		}
		// 可以正确处理0以及无等情况
		// 会对应调整装备的ownerId
		oldEquipId := game.SetEquipIdBySlotType(r, req.TroopId, slotType, newEquipId)
		// 先不管是否是0
		changedEquipIdMap[oldEquipId] = true
		changedEquipIdMap[newEquipId] = true
	}

	// 刷新战力
	game.RefreshTroopPowerById(r, req.TroopId, true)

	// if needed, update r
	c.MustUpdate(r.Id, r)

	// sync 本英雄的装备信息
	ext := game.GetTroopExt(r, req.TroopId)
	if ext != nil {
		res.TroopEquips = ext.Equips
	}

	// 方便客户端操作 同步owner 信息
	game.NotifyEquipOwnerChangeWithMap(r, changedEquipIdMap)

	return
}
