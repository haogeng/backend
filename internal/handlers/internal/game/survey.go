package game

import (
	"context"
	"errors"
	"server/internal/gconst"
	"server/pkg/fungin/xmiddleware"
	"server/pkg/gen/msg"
	"server/pkg/gen_redis/redisdb"
	"time"
)

func (*HandlerHttp) AddSurveyId(ctx context.Context, req *msg.AddSurveyIdR) (res *msg.AddSurveyIdA, err error) {
	res = &msg.AddSurveyIdA{
		Err: &msg.ErrorInfo{},
	}

	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}

	var c redisdb.PlayerInfoCache
	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}

	if len(req.SurveyId) == 0 ||
		len(req.SurveyId) > 512 ||
		len(r.SurveyHistory) > 512 {
		err = errors.New("Survey id err")
		return
	}

	// do logic with r
	if r.SurveyHistory == nil {
		r.SurveyHistory = make(map[string]int64)
	}
	r.SurveyHistory[req.SurveyId] = time.Now().Unix()

	// if needed, update r
	c.MustUpdate(r.Id, r)

	return
}
