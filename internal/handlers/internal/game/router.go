package game

import (
	"bitbucket.org/funplus/ark"
	"bitbucket.org/funplus/arkmid"
	"bitbucket.org/funplus/arkmid/auth"
	"bitbucket.org/funplus/sandwich/current"
	"github.com/dgrijalva/jwt-go"
	internal2 "server/internal"
	"server/pkg/fungin/xmiddleware"
	"server/pkg/fungin/xmiddleware/arkmid_helper"
	"server/pkg/fungin/xmiddleware/whiteips"
	"server/pkg/gen/service"

	"server/pkg/fungin/xmiddleware/compressor"
	//"server/pkg/fungin/xmiddleware/respcache"
	//"server/pkg/fungin/xmiddleware/seqcheck"
	"bitbucket.org/funplus/arkmid/respcache"
	"bitbucket.org/funplus/arkmid/seqcheck"
)

type HandlerHttp struct{}

const (
	KGroupName string = "game"
)

func getCompressorSkipper() arkmid.Skipper {
	var msgUriForGzip = map[string]bool{
		"msg.PlayerLoginR": true,
	}
	return func(ctx *ark.Context) bool {
		msgs := current.MustGetIncomingMessages(ctx)
		// 类似的大消息，可以加入到一个绿色通道的map中
		if len(msgs) > 0 {
			if msgUriForGzip[msgs[0].Uri] {
				return false
			}
		}
		return true
	}
}

func InitHttpRouter(group ark.Router) {
	// prepare sth.
	redisSequencer := arkmid_helper.NewRedisRequestSequencer(internal2.Config.SequenceIdTtl, internal2.Config.SequenceDealWithLock)
	lastSeqIdProvider := redisSequencer.(seqcheck.LastSeqIdProvider)
	seqLockProvider := redisSequencer.(seqcheck.LockProvider)

	apiGroup := group.Group(KGroupName)
	apiGroup.Use(
		xmiddleware.CheckVersion(),

		//解析token，struct存储到context
		auth.NewParser(auth.WithSigningKeys(xmiddleware.AuthConfigSigningKey),
			auth.WithSigningMethod(jwt.SigningMethodHS256),
			auth.WithKeyTokenInContext(xmiddleware.AuthKeyTokenFlag),
			auth.WithKeyClaimInContext(xmiddleware.AuthKeyClaimFlag),
			auth.WithKeyTokenInHeader(xmiddleware.AuthKeyTokenInHeader),
			auth.WithAuthScheme(""),
			auth.WithClaims(&xmiddleware.TokenClaims{})),

		xmiddleware.CheckToken(internal2.Config.TokenTtl),

		xmiddleware.ArkUnmarshal(),

		whiteips.SkipByProtoName(internal2.GetHotConfig()),

		// 逻辑层面，用户绑定context
		xmiddleware.BindCtxToUser(),

		//当前request是否被请求并缓存response
		respcache.New(
			respcache.WithCacheProvider(arkmid_helper.NewRedisResponseCache(internal2.Config.ResponseTtl)),
			respcache.WithSeqIdProvider(&arkmid_helper.ContextSeqIdProvider{})),

		//检测请求时序是否ok
		seqcheck.New(
			seqcheck.WithLockProvider(seqLockProvider),
			seqcheck.WithLastSeqIdProvider(lastSeqIdProvider),
			seqcheck.WithSeqIdProvider(&arkmid_helper.ContextSeqIdProvider{})),

		// No need echo cause check sequence has added echo
		//middleware.EchoSequence(),

		compressor.New(compressor.WithSkipper(getCompressorSkipper())),
	)

	{
		service.RegisterGameServiceHttpHandler(apiGroup, &HandlerHttp{})
	}
}
