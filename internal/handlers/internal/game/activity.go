package game

import (
	"context"
	"server/internal/game"
	"server/internal/gconst"
	"server/internal/util/time3"
	"server/pkg/fungin/xmiddleware"
	"server/pkg/gen/msg"
	"server/pkg/gen/rawdata"
	"server/pkg/gen_redis/redisdb"
)

// !!! DayIndex 都是从1开始的！！！
func (*HandlerHttp) GetActivitySignAward(ctx context.Context, req *msg.GetActivitySignAwardR) (res *msg.GetActivitySignAwardA, err error) {
	res = &msg.GetActivitySignAwardA{
		Err: &msg.ErrorInfo{},
	}

	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}

	var c redisdb.PlayerInfoCache
	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}

	// 登录触发下一次签到
	// do logic with r
	// 判断有无活动
	ad := game.GetActivity(r, req.ActivityId, res.Err)
	if res.Err.Id != 0 {
		return
	}

	// 是否开启
	if !game.IsActivityOpen(r, ad, res.Err) {
		return
	}

	// detail数据是否存在
	sign := game.ActivityOpt(ad).ShouldGetDetail(r).(*msg.ActivityDailySignDetail)
	if sign == nil {
		res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "no sign data"
		return
	}

	// 是否已签到
	if _, ok := sign.SignedDay[req.DayIndex]; ok {
		res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "has signed"
		return
	}

	// 先刷新一下最新的can sign day
	game.ActivityOpt(ad).TryUpdateActivityByTime(r)

	// 能否签到,并发放奖励
	if req.DayIndex <= sign.CanSignDay {
		// Do check in award
		signConf := game.ActivityOpt(ad).GetConfig(res.Err).(*rawdata.ActivitySignConfig)
		if res.Err.Id != 0 {
			return
		}
		dayIndexFrom0 := req.DayIndex - 1
		if !(dayIndexFrom0 >= 0 && dayIndexFrom0 < int32(len(signConf.Reward))) {
			res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "day index err"
			return
		}

		addedAwards, _ := game.AddStaticObjByObjConf(r, signConf.Reward[dayIndexFrom0], res.Err)
		if res.Err.Id != 0 {
			return
		}
		game.TryAppendSyncObjsMsgWithConsumes(ctx, r, addedAwards, nil)

		sign.SignedDay[req.DayIndex] = 1
	}

	game.TryCloseActivity(r, ad)

	game.RefreshMailRedPoint(r, true)
	// if needed, update r
	c.MustUpdate(r.Id, r)

	// sync activity
	game.NotifyActivity(r, []int32{req.ActivityId})

	return
}

func (*HandlerHttp) GetActivityQuestAward(ctx context.Context, req *msg.GetActivityQuestAwardR) (res *msg.GetActivityQuestAwardA, err error) {
	res = &msg.GetActivityQuestAwardA{
		Err: &msg.ErrorInfo{},
	}

	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}

	var c redisdb.PlayerInfoCache
	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}

	// do logic with r
	// 判断有无活动
	ad := game.GetActivity(r, req.ActivityId, res.Err)
	if res.Err.Id != 0 {
		return
	}

	// 先刷新一下最新的can sign day
	game.ActivityOpt(ad).TryUpdateActivityByTime(r)

	// 是否开启
	if !game.IsActivityOpen(r, ad, res.Err) {
		return
	}

	// check day no
	dayNo := game.GetActivityDayInQuestDetail(ad, req.TaskId, res.Err)
	if res.Err.Id != 0 {
		return
	}
	// 该天活动是否开了
	passedDay := time3.GetCrossDays(ad.OpenedTimestamp, time3.Now().Unix())
	if passedDay+1 < dayNo {
		res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "passed day didnt upto day no"
		return
	}

	// 走通用任务逻辑
	addedAwards := game.DoTaskAward(r, req.TaskId, res.Err)
	if res.Err.Id != 0 {
		return
	}

	detail := game.ActivityOpt(ad).ShouldGetDetail(r).(*msg.ActivityDailyQuestDetail)
	if detail == nil {
		res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "no detail"
		return
	}
	// 记录一个已完成任务
	detail.FinishedTaskIds[req.TaskId] = 1

	// !!ORDER 先notify task 然后try close activity
	// sync data
	game.NotifyTaskChange(r, []int32{req.TaskId})

	// close里有可能会清空task计数，，虽然状态结束了，但客户端当时还是不关闭界面，任务不应该清
	game.TryCloseActivity(r, ad)

	game.RefreshMailRedPoint(r, true)
	// if needed, update r
	c.MustUpdate(r.Id, r)

	//res.TaskId = req.TaskId
	//res.TaskLevel = r.Tasks.TaskLevels[req.TaskId]

	if len(addedAwards) > 0 {
		game.TryAppendSyncObjsMsgWithConsumes(ctx, r, addedAwards, nil)
	}

	game.NotifyActivity(r, []int32{req.ActivityId})

	return
}
