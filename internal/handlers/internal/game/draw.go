package game

import (
	"bitbucket.org/funplus/golib/deepcopy"
	"context"
	"server/internal/game"
	"server/internal/gconst"
	"server/pkg/fungin/xmiddleware"
	"server/pkg/gen/conf"
	"server/pkg/gen/msg"
	"server/pkg/gen_redis/redisdb"
)

func (*HandlerHttp) Draw(ctx context.Context, req *msg.DrawR) (res *msg.DrawA, err error) {
	res = &msg.DrawA{
		Err: &msg.ErrorInfo{},
	}

	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}

	var c redisdb.PlayerInfoCache
	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}

	// do logic with r
	// check conf data
	confData, _ := conf.GetDrawConfig(req.Id)
	if !game.CheckConfData(confData, req.Id, res.Err) {
		return
	}

	// check count type
	if req.CountType != msg.DrawCountType_DCT_Single &&
		req.CountType != msg.DrawCountType_DCT_Ten {
		res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "unknown countType"
		return
	}

	// check consume type
	if req.ConsumeType != msg.DrawConsumeType_DCT_Currency &&
		req.ConsumeType != msg.DrawConsumeType_DCT_Item {
		res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "unknown consumeType"
		return
	}

	oldDrawCount := game.GetDrawCount(r, req.Id)

	// get consumes
	consumes := game.GetDrawConsume(confData, req.CountType, req.ConsumeType, res.Err)
	if res.Err.Id != 0 {
		return
	}

	// check consume
	if !game.CheckConsumes(r, consumes, res.Err) {
		return
	}

	// sub consume
	if !game.SubConsumes(r, consumes, res.Err) {
		return
	}

	// Do draw Award!
	addedObjs := game.DoDrawAward(r, confData, req.CountType, res.Err)
	if res.Err.Id != 0 {
		return
	}

	res.FinalDrawCount = game.GetDrawCount(r, req.Id)

	// 备份delta,用于前端展示用
	showDeltaObjs := deepcopy.Copy(addedObjs).([]*msg.ObjInfo)

	// 二部显示，，自动分解的碎片
	nextDeltaObjs := []*msg.ObjInfo{}
	// 自动分解
	if r.AutoLayoff {
		addedObjs = game.TryAutoDismissTroop(r, addedObjs)

		// changed id
		// 抽卡的特殊显示，分解后的进一步显示
		for i := 0; i < len(addedObjs); i++ {
			if addedObjs[i].Type != msg.ObjType_OT_Troop {
				tempObj := deepcopy.Copy(addedObjs[i]).(*msg.ObjInfo)
				nextDeltaObjs = append(nextDeltaObjs, tempObj)
			}
		}
		if len(nextDeltaObjs) > 0 {
			nextDeltaObjs = game.MergeObjInfo(nextDeltaObjs)
		}
	}

	deltaDrawCount := int64(res.FinalDrawCount - oldDrawCount)
	game.RefreshTaskCount(r, msg.TaskCondType_TCT_OpenChestCount, 0, deltaDrawCount, 0)

	// sync
	changeIds := game.MakeChangeKeysFromObj(addedObjs)
	changeIds = append(changeIds, game.MakeChangeKeysFromStaticObj(consumes)...)
	game.TryAppendSyncObjsMsgWithChangedIdsAndNext(ctx, r, showDeltaObjs, nextDeltaObjs, changeIds)
	//game.TryAppendSyncObjsMsgWithConsumes(ctx, r, addedObjs, consumes)
	//
	//game.TrySyncNewTroops(r)

	// if needed, update r
	c.MustUpdate(r.Id, r)

	return
}

// ref
//郭飞
//17:49
//抽卡功能相关配置修改：
//
//1，DrawConfig：
//1.1原expend字段改名为expendItem，用于招募消耗道具配置（单抽十连）
//1.2添加expendCurrency字段，用于招募消耗货币配置（单抽十连）
//1.3添加dropID字段，用于调用具体的drop掉落包（int型）
//以上配置除dropid，都为四元组数组
//
//2，WeightConfig
//废置原J06.抽卡权重表
//
//3，FakeRandomConfig
//3.1废置抽取权重（weight）字段，功能有服务器在玩家选择文明时，做随机处理
//3.2该表主键id规则改为组合式：
//id=卡包id×1000+文明id×10+编号（1、2）
//——编号1、2取决于具体一个文明有2条伪随机链
//3.3model字段中数据，已替换为最新六元组形式
//
//4，DropConfig和ObjList已添加最新抽卡掉落配置
