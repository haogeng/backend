package game

import (
	"context"
	"server/internal/game"
	"server/internal/gconst"
	"server/pkg/fungin/xmiddleware"
	"server/pkg/gen/msg"
	"server/pkg/gen_redis/redisdb"
	"time"
)

func (*HandlerHttp) GetArenaInfos(ctx context.Context, req *msg.ArenaInfosR) (res *msg.ArenaInfosA, err error) {
	res = &msg.ArenaInfosA{
		Err: &msg.ErrorInfo{},
	}
	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}
	var c redisdb.PlayerInfoCache
	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}
	arenaInfos := game.GetArenaInfos(r, res.Err)

	if res.Err.Id != 0 {
		return
	}
	c.MustUpdate(r.Id, r)

	res.ArenaInfos = arenaInfos
	return
}

func (*HandlerHttp) GetArenaMainInfo(ctx context.Context, req *msg.ArenaInfoR) (res *msg.ArenaInfoA, err error) {
	res = &msg.ArenaInfoA{
		Err: &msg.ErrorInfo{},
	}
	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}
	var c redisdb.PlayerInfoCache
	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}
	addedObjs, res := game.GetArenaMainInfo(r, req.SeasonId)
	if res.Err.Id != 0 {
		return
	}
	game.TryAppendSyncObjsMsgWithConsume(ctx, r, addedObjs, nil)

	c.MustUpdate(r.Id, r)

	// 刷新积分竞技场红点
	game.ClearArenaSeasonRedPoint(r, true)
	return
}

func (*HandlerHttp) MatchArenaBattle(ctx context.Context, req *msg.ArenaMatchInfoR) (res *msg.ArenaMatchInfoA, err error) {
	res = &msg.ArenaMatchInfoA{
		Err: &msg.ErrorInfo{},
	}
	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}
	var c redisdb.PlayerInfoCache
	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}
	value := game.GetMatchInfos(r, req.SeasonId, res.Err)
	if res.Err.Id != 0 {
		return
	}
	res.MatchInfos = value
	res.FreeNum, res.BuyNum = game.GetArenaBattleNum(r, req.SeasonId, res.Err)
	res.RefreshTime = time.Now().Add(3 * time.Second).Unix()
	return
}

func (*HandlerHttp) GetArenaBattleLogs(ctx context.Context, req *msg.ArenaBattleLogInfosR) (res *msg.ArenaBattleLogInfosA, err error) {
	res = &msg.ArenaBattleLogInfosA{
		Err: &msg.ErrorInfo{},
	}
	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}
	var c redisdb.PlayerInfoCache
	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}
	battleLogs := game.GetArenaBattleRecord(r, res.Err)
	if res.Err.Id != 0 {
		return
	}
	res.ArenaBattleLogs = battleLogs

	c.MustUpdate(r.Id, r)

	// 刷新竞技场战报红点
	game.RefreshArenaLogRedPoint(r, true)
	return
}

func (*HandlerHttp) BuyCostArenaBattleNum(ctx context.Context, req *msg.BuyCostArenaBattleR) (res *msg.BuyCostArenaBattleA, err error) {
	res = &msg.BuyCostArenaBattleA{
		Err: &msg.ErrorInfo{},
	}
	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}
	var c redisdb.PlayerInfoCache
	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}
	addedObjs := game.BuyArenaBattleTicket(r, req.SeasonId, req.CostNum, res.Err)
	if res.Err.Id != 0 {
		return
	}
	game.TryAppendSyncObjsMsgWithConsume(ctx, r, addedObjs, nil)

	c.MustUpdate(r.Id, r)

	_, res.BuyNum = game.GetArenaBattleNum(r, req.SeasonId, res.Err)
	return
}
