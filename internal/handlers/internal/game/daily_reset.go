package game

import (
	"context"
	"server/internal/game"
	"server/internal/gconst"
	"server/internal/util/time3"
	"server/pkg/fungin/xmiddleware"
	"server/pkg/gen/msg"
	"server/pkg/gen_redis/redisdb"
)

func (*HandlerHttp) DailyReset(ctx context.Context, req *msg.DailyResetR) (res *msg.DailyResetA, err error) {
	res = &msg.DailyResetA{
		Err: &msg.ErrorInfo{},
	}

	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}

	var c redisdb.PlayerInfoCache
	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}

	// do logic with r
	if !game.CheckDailyReset(r, time3.Now().Unix(), gconst.DailyResetToleranceSeconds) {
		res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "daily reset time err"
		// 报错的时候，不sync
		return
	}

	game.DoDailyReset(r, true)

	// if needed, update r
	c.MustUpdate(r.Id, r)

	res.DailyResetTime = r.DailyResetTime

	return
}

func (*HandlerHttp) WeeklyReset(ctx context.Context, req *msg.WeeklyResetR) (res *msg.WeeklyResetA, err error) {
	res = &msg.WeeklyResetA{
		Err: &msg.ErrorInfo{},
	}

	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}

	var c redisdb.PlayerInfoCache
	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}

	// do logic with r
	if game.CheckWeeklyReset(r, time3.Now().Unix(), 10) {
		game.DoWeeklyReset(r, true)
		// 报错的时候，不sync
		return
	}

	// if needed, update r
	c.MustUpdate(r.Id, r)

	res.WeeklyResetTime = r.WeeklyResetTime

	return
}
