package game

import (
	"context"
	"server/internal/gconst"
	"server/pkg/fungin/xmiddleware"
	"server/pkg/gen/msg"
	"server/pkg/gen_redis/redisdb"
)

// 作为一个模板存在
// replace TestGameReq to xxR
// replace TestGameRes to xxA
func (*HandlerHttp) XxxTpl(ctx context.Context, req *msg.XxxTplR) (res *msg.XxxTplA, err error) {
	res = &msg.XxxTplA{
		Err: &msg.ErrorInfo{},
	}

	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}

	var c redisdb.PlayerInfoCache
	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}

	// do logic with r

	// if needed, update r
	c.MustUpdate(r.Id, r)

	return
}
