package game

import (
	"context"
	"fmt"
	"server/internal/clan"
	"server/internal/game"
	"server/internal/gconst"
	"server/internal/log"
	"server/internal/share"
	"server/internal/util/time3"
	"server/internal/xrtm"
	"server/pkg/fungin/xmiddleware"
	"server/pkg/gen/msg"
	"server/pkg/gen_redis/redisdb"
)

func (*HandlerHttp) PlayerLogin(ctx context.Context, req *msg.PlayerLoginR) (res *msg.PlayerLoginA, err error) {
	res = &msg.PlayerLoginA{
		Err: &msg.ErrorInfo{},
	}

	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}

	var c redisdb.PlayerInfoCache
	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}

	// do logic with r
	// 账号锁定检查
	now := time3.Now().Unix()
	if r.LockDeadline > now {
		lockInfo := fmt.Sprintf("data is lock(%d), pls wait %d seconds", r.LockReason, r.LockDeadline-now)
		res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, lockInfo
		return
	}

	// TODO: 比如安全检查，vip过期处理，离线奖励等等

	// daily reset
	if game.CheckDailyReset(r, now, 0) {
		game.DoDailyReset(r, false)
	}
	if game.CheckWeeklyReset(r, now, 0) {
		game.DoWeeklyReset(r, false)
	}

	// 问题多多，需要考虑redis 数据有效性，这个最好是从db加载到redis的时候做
	//r.CheckDataOnLogin()
	game.ValidMainLineAry(r)
	game.ValidFormationData(r)
	clan.ValidPlayerClan(r)

	// 开发期加的，(老账号没商店)，以后也无害
	game.TryTriggerNewStore(r, false)
	// 刷新需要显示但又不是激活的任务
	game.RefreshTaskUnactiveButShow(r)

	// 尝试初始化副本信息
	tempErr := &msg.ErrorInfo{}
	game.InitMainCopy(r, tempErr)
	if tempErr.Id != 0 {
		log.Errorf("u%d login err for init_main_copy: %v", r.Id, tempErr)
		// reset err
		tempErr.Id = 0
	}
	// 刷新普通竞技场
	game.ResetArenaInfos(r, tempErr)

	if tempErr.Id != 0 {
		log.Errorf("u%d login err for send_arena_reward: %v", r.Id, tempErr)
		// reset err
		tempErr.Id = 0
	}

	// 活动
	game.RefreshActivityOnLogin(r)

	// 邮件红点
	game.RefreshMailRedPoint(r, false)

	// 好友申请红点
	game.RefreshFriendApplyRedPoint(r, false)

	// 好友爱心红点
	game.RefreshFriendLoveRedPoint(r, false)

	// 竞技场战报红点
	game.RefreshArenaLogRedPoint(r, false)

	// 主线副本红点
	game.RefreshWarCopyBoxRedPoint(r, false)

	// 公会申请红点
	game.RefreshClanRedPoint(r, false, nil, msg.RedPointType_RPT_ClanApply)
	game.RefreshClanRedPoint(r, false, nil, msg.RedPointType_RPT_ClanReward)

	//!!!ORDER 很重要
	// if needed, update r
	c.MustUpdate(r.Id, r)

	//////////////////////////////////////////
	// 只能存档一次，不能再存档了
	// 为客户端数据做后续处理，并发送给客户端，不存档

	// 尝试追加副本信息 (序章阶段)
	mainCopyInfos := game.BuildMainCopyInfoResults(r, tempErr)
	// 还在序章中，(==第二章没解锁)
	if tempErr.Id == 0 && len(mainCopyInfos) == 1 {
		strongHoldInfos := game.BuildStrongHoldResults(r, game.MainIntoned)
		res.MainCopyInfoInLoginA = &msg.MainCopyInfoInLoginA{
			MainCopyInfos:   mainCopyInfos,
			StrongholdInfos: strongHoldInfos,
			FirstJoin:       mainCopyInfos[0].FirstJoin,
		}
	}

	res.ClientTime = req.ClientTime
	res.CurServerTime = now

	// 玩家信息做deepcopy，
	// 个别信息不发送给客户端
	// 其实不用deepCopy，因为r不会再被存档
	//res.PlayerInfo = deepcopy.Copy(r).(*msg.PlayerInfo)
	res.PlayerInfo = r

	rs := share.GetClanId(r.Id)
	if rs != nil {
		res.PlayerInfo.ClanId = rs.ClanId
	}

	game.DealBuildingsForClient(res.PlayerInfo.Buildings)
	// 屏蔽某一些客户端暂时不需要的数据，需要客户端需要时候拉取
	{
		res.PlayerInfo.CWarDiffInfos = game.GetWarInfosByFlag(res.PlayerInfo)
		res.PlayerInfo.Ext = nil
		// 如果不屏蔽，则需要针对客户端处理storeItem...
	}
	if res.PlayerInfo.TechInfos == nil {
		res.PlayerInfo.TechInfos = make(map[int32]*msg.TechInfo)
	}
	if res.PlayerInfo.Clan == nil {
		res.PlayerInfo.Clan = clan.MustGetPlayerInfoClan(r)
	}

	res.RtmInfo = xrtm.GetRTMInfo(r.Id)

	//if res.PlayerInfo.Maze == nil {
	//	res.PlayerInfo.Maze = game.MustGetPlayerInfoMaze(r)
	//}
	// 部分数据前端数据和后端分离
	game.WithClientDataInFinal(res.PlayerInfo, true, msg.FormationType_FT_Count)

	// 用户添加到推荐列表
	game.LoginPlayerToRecommendFriends(ctx, r)
	return
}
