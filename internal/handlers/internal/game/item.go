package game

import (
	"context"
	"fmt"
	"server/internal/game"
	"server/internal/gconst"
	"server/pkg/fungin/xmiddleware"
	"server/pkg/gen/conf"
	"server/pkg/gen/msg"
	"server/pkg/gen_redis/redisdb"
)

func (*HandlerHttp) SellItem(ctx context.Context, req *msg.SellItemR) (res *msg.SellItemA, err error) {
	res = &msg.SellItemA{
		Err: &msg.ErrorInfo{},
	}

	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}

	var c redisdb.PlayerInfoCache
	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}

	// do logic with r
	confData, _ := conf.GetItemConfig(req.Id)
	if !game.CheckConfData(confData, req.Id, res.Err) {
		return
	}

	// can sell
	if !confData.Sell {
		res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "cant sell"
		return
	}

	// CheckConsume
	tempConsume := &msg.StaticObjInfo{
		Id:    req.Id,
		Type:  int32(msg.ObjType_OT_Item),
		Count: int64(req.Count),
	}
	if !game.CheckConsume(r, tempConsume, res.Err) {
		return
	}

	// Do consume
	if !game.SubConsume(r, tempConsume, res.Err) {
		return
	}

	// rewards.
	units, _ := game.JsonToPb_StaticObjs(confData.SellItemList, confData, confData.Id, res.Err)
	if res.Err.Id != 0 {
		return
	}

	var addedObjs []*msg.ObjInfo
	for i := 0; i < int(req.Count); i++ {
		tempObjs, _ := game.AddStaticObjs(r, units, res.Err)
		if res.Err.Id != 0 {
			return
		}

		addedObjs = append(addedObjs, tempObjs...)
	}

	addedObjs = game.MergeObjInfo(addedObjs)
	game.TryAppendSyncObjsMsgWithConsume(ctx, r, addedObjs, tempConsume)

	// if needed, update r
	c.MustUpdate(r.Id, r)

	return
}

func (*HandlerHttp) UseItem(ctx context.Context, req *msg.UseItemR) (res *msg.UseItemA, err error) {
	res = &msg.UseItemA{
		Err: &msg.ErrorInfo{},
	}

	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}

	var c redisdb.PlayerInfoCache
	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}

	// do logic with r
	confData, _ := conf.GetItemConfig(req.Id)
	if !game.CheckConfData(confData, req.Id, res.Err) {
		return
	}

	// can use
	useType := msg.ItemUseType(confData.UseType)
	if !game.CheckItemUseType(useType, res.Err) {
		return
	}

	// 消耗和奖励
	rewardCount := req.Count
	consumeCount := req.Count

	// 针对合成类型做特殊处理
	if useType == msg.ItemUseType_IUT_Merge {
		if confData.CountCondition == 0 {
			res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "count condition should not be 0"
			return
		}

		if req.Count%confData.CountCondition != 0 {
			res.Err.Id = msg.Ecode_INVALID
			res.Err.DebugMsg = fmt.Sprintf("req count err %d/%d", req.Count, confData.CountCondition)
			return
		}
		rewardCount = req.Count / confData.CountCondition
		if rewardCount == 0 {
			res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "count unenough"
			return
		}
	}

	// CheckConsume
	tempConsume := &msg.StaticObjInfo{
		Id:    req.Id,
		Type:  int32(msg.ObjType_OT_Item),
		Count: int64(consumeCount),
	}
	if !game.CheckConsume(r, tempConsume, res.Err) {
		return
	}

	// Do consume
	if !game.SubConsume(r, tempConsume, res.Err) {
		return
	}

	//log.Debugf("Try use item")
	// rewards.
	units, _ := game.JsonToPb_StaticObjs(confData.ItemList, confData, confData.Id, res.Err)
	if res.Err.Id != 0 {
		return
	}

	//log.Debugf("units1: %v\n", units)

	// filter units by user choose.
	if useType == msg.ItemUseType_IUT_Choose {
		units = fillterUnits(req, units, res.Err)
	}
	//log.Debugf("units2: %v\n", units)

	var addedObjs []*msg.ObjInfo
	for i := 0; i < int(rewardCount); i++ {
		tempObjs, _ := game.AddStaticObjs(r, units, res.Err)
		if res.Err.Id != 0 {
			return
		}

		addedObjs = append(addedObjs, tempObjs...)
	}
	//log.Debugf("addedObjs: %v\n", addedObjs)

	addedObjs = game.MergeObjInfo(addedObjs)
	game.TryAppendSyncObjsMsgWithConsume(ctx, r, addedObjs, tempConsume)

	// if needed, update r
	c.MustUpdate(r.Id, r)

	return
}

func fillterUnits(req *msg.UseItemR, units []*msg.StaticObjInfo, err *msg.ErrorInfo) []*msg.StaticObjInfo {
	tempUnits := make([]*msg.StaticObjInfo, 0)

	for i := 0; i < len(req.Chooses); i++ {
		for _, v := range units {
			if v.Type == int32(req.Chooses[i].Type) && v.Id == req.Chooses[i].Id {
				tempUnits = append(tempUnits, v)
			}
		}
	}

	return tempUnits
}
