package game

import (
	"context"
	"math/rand"
	"server/internal"
	"server/internal/clan"
	"server/internal/game"
	"server/internal/gconst"
	"server/internal/log"
	"server/internal/share"
	"server/pkg/fungin/xmiddleware"
	"server/pkg/gen/conf"
	"server/pkg/gen/msg"
	"server/pkg/gen_redis/redisdb"
)

func (*HandlerHttp) SelectFormation(ctx context.Context, req *msg.SelectFormationR) (res *msg.SelectFormationA, err error) {
	res = &msg.SelectFormationA{
		Err: &msg.ErrorInfo{},
	}
	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}

	var c redisdb.PlayerInfoCache
	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}
	res.Formation = game.SelectFormation(r, req.Type, res.Err)
	return
}

func (*HandlerHttp) ChangeFormation(ctx context.Context, req *msg.ChangeFormationR) (res *msg.ChangeFormationA, err error) {
	res = &msg.ChangeFormationA{
		Err: &msg.ErrorInfo{},
	}

	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}

	var c redisdb.PlayerInfoCache
	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}

	// do logic with r
	game.ChangeFormation(r, req.Form, res.Err)
	if res.Err.Id != 0 {
		return
	}

	// if needed, update r
	c.MustUpdate(r.Id, r)

	return
}

func (*HandlerHttp) SelectPlayerInfo(ctx context.Context, req *msg.SelectPlayerInfoR) (res *msg.SelectPlayerInfoA, err error) {
	res = &msg.SelectPlayerInfoA{
		Err: &msg.ErrorInfo{},
	}
	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}

	var c redisdb.PlayerInfoCache
	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}
	if game.CheckPlayerIdIsRobot(req.PlayerId) {
		confData, _ := conf.GetRobotConfig(int32(req.PlayerId))
		if !game.CheckConfData(confData, int32(req.PlayerId), res.Err) {
			return
		}
		res.RInfo = &msg.RobotInfo{
			Id:       req.PlayerId,
			DefPower: confData.FormationForce,
			NpcInfos: confData.ArmyEmbattle,
		}
	} else {
		res.DInfo = c.Find(req.PlayerId)
		res.FriendStatus = game.CheckFriendInfosByUserId(r, req.PlayerId)
		res.ApplyStatus = game.CheckApplyInfosByUserId(res.DInfo, r.Id) == msg.FriendApplyType_FAT_INAPPLY
		res.BlackStatus = game.CheckBlackInfosByUserId(r, req.PlayerId)
		rs := share.GetClanId(req.PlayerId)
		if rs != nil {
			res.CInfo = clan.FindClanBrief(rs.ClanId)
		}
		// 部分数据前端数据和后端分离
		game.WithClientDataInFinal(res.DInfo, uid == req.PlayerId, req.Type)
	}
	return
}

func (*HandlerHttp) ReqStartBattle(ctx context.Context, req *msg.ReqStartBattleR) (res *msg.ReqStartBattleA, err error) {
	res = &msg.ReqStartBattleA{
		Err: &msg.ErrorInfo{},
	}

	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}

	var c redisdb.PlayerInfoCache
	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}
	var consumes []*msg.StaticObjInfo
	// do logic with r
	if req.Type == msg.BattleType_EBT_Tower {
		// check 系统是否开启
		game.StartTowerBattle(r, req, res)
		if res.Err.Id != 0 {
			return
		}

		game.RefreshTaskCount(r, msg.TaskCondType_TCT_TowerCount, 0, 1, 0)

	} else if req.Type == msg.BattleType_EBT_MainChapter {

		game.StartMainCopyBattle(r, req, res)

		if res.Err.Id != 0 {
			return
		}

	} else if req.Type == msg.BattleType_EBT_Maze {
		// 异界迷宫开始
		game.StartMazeBattle(r, req, res)
		if res.Err.Id != 0 {
			return
		}
	} else if req.Type == msg.BattleType_EBT_Arena {
		consumes = game.StartArenaBattle(r, req, res)
		if res.Err.Id != 0 {
			return
		}
	} else if req.Type == msg.BattleType_EBT_ClanBoss {
		game.StartClanBossBattle(r, req, res)
		if res.Err.Id != 0 {
			return
		}
	} else if req.Type == msg.BattleType_EBT_WarChapter {
		game.StartWarCopyBattle(r, req, res)
		if res.Err.Id != 0 {
			return
		}
	} else if req.Type == msg.BattleType_EBT_Friend {
		game.StartFriendBattle(r, req.BattleId, res.Err)
	} else {
		res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "not support battle type"
		return
	}

	// common
	// gen seed
	res.Seed = rand.Int31()
	if res.AdjustScale10000 == 0 {
		res.AdjustScale10000 = 10000
	}

	// final sync inside
	game.TryAppendSyncObjsMsgWithConsumes(ctx, r, nil, consumes)

	// if needed, update r
	c.MustUpdate(r.Id, r)

	return
}
func (*HandlerHttp) ReqEndBattle(ctx context.Context, req *msg.ReqEndBattleR) (res *msg.ReqEndBattleA, err error) {
	res = &msg.ReqEndBattleA{
		Err: &msg.ErrorInfo{},
	}

	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}

	var c redisdb.PlayerInfoCache
	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}

	// 透传数据
	res.ConfigId = req.ConfigId
	res.Type = req.Type

	// do logic with r
	if req.Type == msg.BattleType_EBT_Tower {
		// check 系统是否开启
		game.EndTowerBattle(r, req, res)
		if res.Err.Id != 0 {
			return
		}
	} else if req.Type == msg.BattleType_EBT_MainChapter {
		// 主线副本结算
		game.EndMainCopyBattle(r, req, res)
		if res.Err.Id != 0 {
			return
		}
	} else if req.Type == msg.BattleType_EBT_Maze {
		// 异界迷宫结算
		game.EndMazeBattle(r, req, res)
		if res.Err.Id != 0 {
			return
		}
	} else if req.Type == msg.BattleType_EBT_Arena {
		// PVP
		battleR := c.Find(req.BattleId)
		// 竞技场结算
		game.EndArenaBattle(r, req.BattleId, battleR, req, res)
		if res.Err.Id != 0 {
			return
		}
		// TODO PVP更新战斗用户,考虑会出现被多人互殴的情况怎么办
		if battleR != nil {
			c.MustUpdate(battleR.Id, battleR)
		}
	} else if req.Type == msg.BattleType_EBT_ClanBoss {
		game.EndClanBossBattle(r, req, res)
		if res.Err.Id != 0 {
			return
		}
	} else if req.Type == msg.BattleType_EBT_WarChapter {
		game.EndWarCopyBattle(r, req, res)
		if res.Err.Id != 0 {
			return
		}
	} else if req.Type == msg.BattleType_EBT_Friend {
		// 好友切磋啥都不干
	} else {
		res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "not support battle type"
		return
	}

	// final sync inside
	game.TryAppendSyncObjsMsgWithConsume(ctx, r, res.LootObjs, nil)

	// if needed, update r
	c.MustUpdate(r.Id, r)

	return
}
func (*HandlerHttp) DebugSetBattleProgress(ctx context.Context, req *msg.DebugSetBattleProgressR) (res *msg.DebugSetBattleProgressA, err error) {
	res = &msg.DebugSetBattleProgressA{
		Err: &msg.ErrorInfo{},
	}
	if !internal.Config.Development {
		res.Err.Id, res.Err.DebugMsg = 0, "dont support debug set battle progress"
		return
	}

	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}

	var c redisdb.PlayerInfoCache
	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}

	// do logic with r
	log.Infof("Debug set process Type:%v Id:%v", req.Type, req.ConfigId)
	if req.Type == msg.BattleType_EBT_Tower {
		r.PassedTowerId = req.ConfigId
	} else {
		res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "not support battle type"
		return
	}

	// if needed, update r
	c.MustUpdate(r.Id, r)

	return
}
