package game

import (
	"context"
	"server/internal/game"
	"server/internal/gconst"
	"server/internal/util/time3"
	"server/pkg/fungin/xmiddleware"
	"server/pkg/gen/conf"
	"server/pkg/gen/msg"
	"server/pkg/gen_redis/redisdb"
)

func (*HandlerHttp) doStartBounty(r *msg.PlayerInfo, req *msg.StartBountyR) (res *msg.StartBountyA, err error) {
	res = &msg.StartBountyA{
		Err: &msg.ErrorInfo{},
	}

	// do logic with r
	b := game.GetBounty(r, req.Id, res.Err)
	if res.Err.Id != 0 {
		return
	}

	// check status
	if b.Status != msg.BountyStatusType_BST_Free {
		res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "bounty status err"
		return
	}

	// check id no repeat
	if game.HasRepeatBountyTroopId(r, req.TroopIds) {
		res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "has repeat id"
		return
	}

	// check id satisfy need
	confData, _ := conf.GetBountyTaskConfig(b.ConfigId)
	if !game.CheckConfData(confData, b.ConfigId, res.Err) {
		return
	}
	needs, _ := game.JsonToPb_StaticObjs(confData.DispatchCondNeeds, confData, b.ConfigId, res.Err)
	if res.Err.Id != 0 {
		return
	}
	if !game.CheckBountyNeed(r, needs, req.TroopIds, res.Err) {
		return
	}

	// do start.
	b.TroopIds = req.TroopIds
	b.Status = msg.BountyStatusType_BST_Doing
	b.EndTime = time3.Now().Unix() + int64(confData.CompleteTime)

	res.Item = b

	return
}

func (p *HandlerHttp) StartBounty(ctx context.Context, req *msg.StartBountyR) (res *msg.StartBountyA, err error) {
	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}

	var c redisdb.PlayerInfoCache
	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}
	res, err = p.doStartBounty(r, req)
	// if needed, update r
	if err == nil {
		c.MustUpdate(r.Id, r)
	}
	return
}

func (p *HandlerHttp) StartBounties(ctx context.Context, req *msg.StartBountiesR) (res *msg.StartBountiesA, err error) {
	res = &msg.StartBountiesA{
		Err: &msg.ErrorInfo{},
	}
	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}

	var c redisdb.PlayerInfoCache
	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}

	// 直接修改了内存中的临时数据，，即做到了本次操作的一致性，当错误发生时，也能直接rollback
	for i := 0; i < len(req.Reqs); i++ {
		tempRes, tempErr := p.doStartBounty(r, req.Reqs[i])

		// 不管对错，如果错了最后会清空
		res.As = append(res.As, tempRes)

		if tempErr != nil || (tempRes != nil && tempRes.Err != nil && tempRes.Err.Id != 0) {
			// try use err info
			if tempRes != nil {
				res.Err = tempRes.Err
			}
			// make sure error occur.
			if res.Err == nil || res.Err.Id == 0 {
				res.Err = &msg.ErrorInfo{
					Id:       msg.Ecode_INVALID,
					Params:   nil,
					DebugMsg: "unknown err",
				}
			}
			// reset
			res.As = nil
			err = tempErr
			break
		}
	}

	// if needed, update r
	if err == nil {
		c.MustUpdate(r.Id, r)
	}

	return
}

func (*HandlerHttp) GetBountyReward(ctx context.Context, req *msg.GetBountyRewardR) (res *msg.GetBountyRewardA, err error) {
	res = &msg.GetBountyRewardA{
		Err: &msg.ErrorInfo{},
	}

	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}

	var c redisdb.PlayerInfoCache
	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}

	var totalAddedObjs []*msg.ObjInfo
	for i := 0; i < len(req.Ids); i++ {
		reqId := req.Ids[i]

		// do logic with r
		b := game.GetBounty(r, reqId, res.Err)
		if res.Err.Id != 0 {
			return
		}

		// check status
		if b.Status != msg.BountyStatusType_BST_Doing {
			res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "bounty status err"
			return
		}

		// Judge time
		if !game.CheckEndExpireWithTolerance(b.EndTime, game.KDefaultToleranceSecond, res.Err) {
			return
		}

		// check config
		confData, _ := conf.GetBountyTaskConfig(b.ConfigId)
		if !game.CheckConfData(confData, b.ConfigId, res.Err) {
			return
		}

		// rewards
		tempAddedObjs, _ := game.AddStaticObjByObjConf(r, confData.RewardId, res.Err)
		if res.Err.Id != 0 {
			return
		}

		totalAddedObjs = append(totalAddedObjs, tempAddedObjs...)

		// delete b
		game.DeleteBountyTask(r, b.Id)

		// Notice ORDER!!!
		// 实际按次数来 1:1
		game.AddBountyExp(r, 1)

		// Refresh fill bounty...
		// 如果开始时间是上一个重置日，则填充
		startTime := b.EndTime - int64(confData.CompleteTime)
		lastDailyResetTime := r.DailyResetTime - time3.GetSecondsOfOneDay()
		if startTime <= lastDailyResetTime {
			game.RefreshBounty(r, false, false, true)
		}
	}

	if res.Err.Id != 0 {
		return
	}

	game.NotifyBountyInfo(r)

	game.RefreshTaskCount(r, msg.TaskCondType_TCT_SingleTaskCount, 0, 1, 0)

	// final sync
	game.TryAppendSyncObjsMsgWithConsume(ctx, r, totalAddedObjs, nil)

	// if needed, update r
	c.MustUpdate(r.Id, r)

	// sync
	res.Level = r.Bounties.Level
	res.RemainExp = r.Bounties.RemainExp

	return
}

func (*HandlerHttp) ManualRefreshBounty(ctx context.Context, req *msg.ManualRefreshBountyR) (res *msg.ManualRefreshBountyA, err error) {
	res = &msg.ManualRefreshBountyA{
		Err: &msg.ErrorInfo{},
	}

	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}

	var c redisdb.PlayerInfoCache
	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}

	if game.GetBountyNumsOfManualFill(r) <= 0 {
		res.Err.Id, res.Err.DebugMsg = msg.Ecode_NO_BOUNTY_TOBE_REFILL, "no free bounty to be refreshed"
		return
	}

	// do logic with r
	// consume
	consume := game.GetBountyManualRefreshCost()

	if !game.CheckConsume(r, consume, res.Err) {
		return
	}
	game.SubConsume(r, consume, res.Err)

	// with sync.
	//game.RefreshBounty(r, false, true, false)
	game.MakeSureBountyExist(r)
	game.RefreshBounty(r, false, true, false)

	game.NotifyBountyInfo(r)

	// final sync consume
	game.TryAppendSyncObjsMsgWithConsume(ctx, r, nil, consume)

	// if needed, update r
	c.MustUpdate(r.Id, r)

	return
}
