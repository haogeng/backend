package game

import (
	"context"
	"server/internal/game"
	"server/internal/gconst"
	"server/internal/util/time3"
	"server/pkg/fungin/xmiddleware"
	"server/pkg/gen/conf"
	"server/pkg/gen/msg"
	"server/pkg/gen_redis/redisdb"
)

func (*HandlerHttp) BuildingInfo(ctx context.Context, req *msg.BuildingInfoR) (res *msg.BuildingInfoA, err error) {
	err = nil
	res = &msg.BuildingInfoA{
		Err:  &msg.ErrorInfo{},
		Info: nil,
	}
	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}

	var c redisdb.PlayerInfoCache
	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}

	building := game.GetBuilding(r, req.Id)

	now := time3.Now().Unix()
	game.DealBuildingForClient(building, now)

	res.Info = building

	// no need update.
	//c.MustUpdate(r.Id, r)

	return
}

func (*HandlerHttp) StartUnlockBuilding(ctx context.Context, req *msg.StartUnlockBuildingR) (res *msg.StartUnlockBuildingA, err error) {
	err = nil
	res = &msg.StartUnlockBuildingA{
		Err:  &msg.ErrorInfo{},
		Info: nil,
	}

	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}

	var c redisdb.PlayerInfoCache
	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}

	building := game.GetBuilding(r, req.Id)
	buildingConf, _ := conf.GetBuildingConfig(req.Id)

	if !game.CheckBuildingExist(building, buildingConf, res.Err) {
		return
	}

	if !game.CheckBuildingState(building, msg.BuildingState_LOCK, res.Err) {
		return
	}

	// 建造不走队列

	if !game.CheckBuildCondition(building, buildingConf, r, res.Err) {
		return
	}

	consumeObjs := game.CalcBuildConsume(building, buildingConf, res.Err)
	if res.Err.Id != 0 {
		return
	}
	if !game.CheckBuildConsume(consumeObjs, r, res.Err) {
		return
	}

	if !game.SubBuildConsume(consumeObjs, r, res.Err) {
		return
	}
	// 建造不走队列

	building.State = msg.BuildingState_UPGRADING
	// level 不变
	now := time3.Now().Unix()
	building.StartTime = now

	// save
	c.MustUpdate(r.Id, r)

	game.TryAppendSyncObjsMsgWithConsumes(ctx, r, nil, consumeObjs)

	game.DealBuildingForClient(building, now)
	res.Info = building
	return
}

func (*HandlerHttp) EndUnlockBuilding(ctx context.Context, req *msg.EndUnlockBuildingR) (res *msg.EndUnlockBuildingA, err error) {
	err = nil
	res = &msg.EndUnlockBuildingA{
		Err:  &msg.ErrorInfo{},
		Info: nil,
	}

	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}

	var c redisdb.PlayerInfoCache
	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}

	building := game.GetBuilding(r, req.Id)
	buildingConf, _ := conf.GetBuildingConfig(req.Id)

	if !game.CheckBuildingExist(building, buildingConf, res.Err) {
		return
	}

	if !game.CheckBuildingState(building, msg.BuildingState_UPGRADING, res.Err) {
		return
	}

	levelConf, ok := game.GetLevelConf(buildingConf, building.Level, res.Err)
	if !ok {
		return
	}

	// judge time
	if !game.CheckExpireWithTolerance(building.StartTime, levelConf.Time, game.KDefaultToleranceSecond, res.Err) {
		return
	}

	building.State = msg.BuildingState_NORMAL

	levelConf, ok = game.GetLevelConf(buildingConf, building.Level, res.Err)
	if !ok {
		return
	}

	staticObjs := &msg.StaticObjInfo{
		Id:    int32(msg.CurrencyId_CI_Prosperity),
		Type:  int32(msg.ObjType_OT_Currency),
		Count: int64(levelConf.Prosperity),
	}

	addObjs, _ := game.AddStaticObj(r, staticObjs, res.Err)
	if res.Err.Id != 0 {
		return
	}

	building.Level++

	// Notice ORDER!!!
	game.OnBuildingUpgraded(r, building)

	// 建造不走队列

	game.RefreshTaskNew(r, msg.TaskCondType_TCT_BuildingLevel, building.Id)
	game.RefreshTaskCount(r, msg.TaskCondType_TCT_BuildingLevel, building.Id, 0, int64(building.Level))

	// save
	c.MustUpdate(r.Id, r)

	game.TryAppendSyncObjsMsgWithConsumes(ctx, r, addObjs, nil)

	game.DealBuildingForClient(building, time3.Now().Unix())
	res.Info = building
	return
}

//
func (*HandlerHttp) StartUpgradeBuilding(ctx context.Context, req *msg.StartUpgradeBuildingR) (res *msg.StartUpgradeBuildingA, err error) {
	err = nil
	res = &msg.StartUpgradeBuildingA{
		Err:  &msg.ErrorInfo{},
		Info: nil,
	}

	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}

	var c redisdb.PlayerInfoCache
	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}

	building := game.GetBuilding(r, req.Id)
	buildingConf, _ := conf.GetBuildingConfig(req.Id)

	if !game.CheckBuildingExist(building, buildingConf, res.Err) {
		return
	}

	if !game.CheckBuildingState(building, msg.BuildingState_NORMAL, res.Err) {
		return
	}

	if building.Level >= buildingConf.MaxLevel {
		res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "building upto max level"
		return
	}

	if !game.CheckBuildingQueue(building, r, res.Err) {
		return
	}

	if !game.CheckBuildCondition(building, buildingConf, r, res.Err) {
		return
	}

	consumeObjs := game.CalcBuildConsume(building, buildingConf, res.Err)
	if res.Err.Id != 0 {
		return
	}
	if !game.CheckBuildConsume(consumeObjs, r, res.Err) {
		return
	}

	if !game.SubBuildConsume(consumeObjs, r, res.Err) {
		return
	}

	r.BuildQueueCount++
	building.State = msg.BuildingState_UPGRADING
	// level 不变
	now := time3.Now().Unix()
	building.StartTime = now

	// save
	c.MustUpdate(r.Id, r)

	game.TryAppendSyncObjsMsgWithConsumes(ctx, r, nil, consumeObjs)

	game.DealBuildingForClient(building, now)
	res.Info = building
	return
}

func (*HandlerHttp) EndUpgradeBuilding(ctx context.Context, req *msg.EndUpgradeBuildingR) (res *msg.EndUpgradeBuildingA, err error) {
	err = nil
	res = &msg.EndUpgradeBuildingA{
		Err:  &msg.ErrorInfo{},
		Info: nil,
	}

	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}

	var c redisdb.PlayerInfoCache
	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}

	building := game.GetBuilding(r, req.Id)
	buildingConf, _ := conf.GetBuildingConfig(req.Id)

	if !game.CheckBuildingExist(building, buildingConf, res.Err) {
		return
	}

	if !game.CheckBuildingState(building, msg.BuildingState_UPGRADING, res.Err) {
		return
	}

	if building.Level >= buildingConf.MaxLevel {
		res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "building upto max level"
		return
	}

	levelConf, ok := game.GetLevelConf(buildingConf, building.Level, res.Err)
	if !ok {
		return
	}

	// judge time
	if !game.CheckExpireWithTolerance(building.StartTime, levelConf.Time, game.KDefaultToleranceSecond, res.Err) {
		return
	}

	building.State = msg.BuildingState_NORMAL
	r.BuildQueueCount--
	if r.BuildQueueCount < 0 {
		r.BuildQueueCount = 0
	}

	// Notice ORDER!!!
	game.OnBuildingUpgraded(r, building)

	game.RefreshTaskNew(r, msg.TaskCondType_TCT_BuildingLevel, building.Id)
	game.RefreshTaskCount(r, msg.TaskCondType_TCT_BuildingLevel, building.Id, 0, int64(building.Level))

	staticObjs := &msg.StaticObjInfo{
		Id:    int32(msg.CurrencyId_CI_Prosperity),
		Type:  int32(msg.ObjType_OT_Currency),
		Count: int64(levelConf.Prosperity),
	}

	addObjs, _ := game.AddStaticObj(r, staticObjs, res.Err)
	if res.Err.Id != 0 {
		return
	}

	building.Level++

	// save
	c.MustUpdate(r.Id, r)

	game.TryAppendSyncObjsMsgWithConsumes(ctx, r, addObjs, nil)

	game.DealBuildingForClient(building, time3.Now().Unix())
	res.Info = building

	return
}
