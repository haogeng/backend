package game

import (
	"context"
	"server/internal/game"
	"server/internal/gconst"
	"server/internal/log"
	"server/internal/util"
	"server/pkg/fungin/xmiddleware"
	"server/pkg/gen/conf"
	"server/pkg/gen/msg"
	"server/pkg/gen_redis/redisdb"
)

func (*HandlerHttp) LevelUpTroop(ctx context.Context, req *msg.LevelUpTroopR) (res *msg.LevelUpTroopA, err error) {
	res = &msg.LevelUpTroopA{
		Err: &msg.ErrorInfo{},
	}

	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}

	var c redisdb.PlayerInfoCache
	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}

	// do logic with r
	// judge troop exist.
	t := game.GetTroop(r, req.Id, res.Err)
	if res.Err.Id != 0 {
		return
	}

	// 预设置，报错的时候同步到初始等级
	res.DestLevel = t.Level
	oldLevel := t.Level

	// max level -- from some building.
	// 读取训练大厅建筑等级
	building := game.GetBuildingByType(r, int32(msg.BuildingType_BT_TrainingCourt))
	levelConf, _ := game.GetLevelConfByBuilding(building, res.Err)
	if res.Err.Id != 0 {
		return
	}
	MaxLevel := levelConf.HeroLvLimit

	// correct destLevel
	finalDestLevel := req.DestLevel
	if finalDestLevel > MaxLevel {
		log.Debugf("Correct level from %d to %d", req.DestLevel, finalDestLevel)
		finalDestLevel = MaxLevel
	}

	// get consume
	consumes := game.GetTroopLevelUpConsume(t.ConfigId, t.Level, finalDestLevel, res.Err)
	if res.Err.Id != 0 {
		return
	}

	//log.Debugf("consumesA:%v", consumes)

	if !game.CheckConsumes(r, consumes, res.Err) {
		return
	}

	if !game.SubConsumes(r, consumes, res.Err) {
		return
	}

	// do levelup
	t.Level = finalDestLevel
	res.DestLevel = t.Level

	// 刷新战力
	game.RefreshTroopPower(r, t, true)

	// 只关心次数
	if t.Level > oldLevel {
		game.RefreshTaskCount(r, msg.TaskCondType_TCT_TroopLevelUpCount, 0, 1, 0)
	}

	// sync changed items.
	//log.Debugf("consumesB:%v", consumes)
	game.TryAppendSyncObjsMsgWithConsumes(ctx, r, nil, consumes)

	// 刷新总战力
	game.RefreshTotalPower(r)

	// if needed, update r
	c.MustUpdate(r.Id, r)

	//// TEST
	//if internal.Config.Development {
	//	xrtm.SendProto(r.Id, &msg.RedPoint{
	//		Type:                 msg.RedPointType_RPT_Test,
	//		Param1:               1,
	//		Param2:               2,
	//		Count:                t.Level,
	//		XXX_NoUnkeyedLiteral: struct{}{},
	//		XXX_unrecognized:     nil,
	//		XXX_sizecache:        0,
	//	})
	//}

	return
}

func (*HandlerHttp) StarUpTroop(ctx context.Context, req *msg.StarUpTroopR) (res *msg.StarUpTroopA, err error) {
	res = &msg.StarUpTroopA{
		Err: &msg.ErrorInfo{},
	}

	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}

	var c redisdb.PlayerInfoCache
	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}

	// do logic with r
	// judge troop exist.
	t := game.GetTroop(r, req.Id, res.Err)
	if res.Err.Id != 0 {
		return
	}

	// 受影响的英雄配置id 	// 不怕重复
	troopConfigIds := make([]int32, 0)
	troopConfigIds = append(troopConfigIds, t.ConfigId)
	for i := 0; i < len(req.ConsumedTroopIds); i++ {
		consumeTroop := game.GetTroop(r, req.ConsumedTroopIds[i], res.Err)
		if res.Err.Id != 0 {
			return
		}
		troopConfigIds = append(troopConfigIds, consumeTroop.ConfigId)
	}

	// 消耗里不能包含自己
	if util.InArray32(req.ConsumedTroopIds, req.Id) {
		res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "cant eat self"
		return
	}

	confData, _ := conf.GetTroopConfig(t.ConfigId)
	if !game.CheckConfData(confData, t.ConfigId, res.Err) {
		return
	}

	// max star check
	if t.Star >= confData.MaxStar {
		res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "upto max star"
		return
	}

	// consume
	consumeX := game.GetTroopStarUpConsumeX(t.ConfigId, t.Star, res.Err)
	if res.Err.Id != 0 {
		return
	}

	// check consume special
	if !game.CheckTroopConsumeX(r, consumeX, req.ConsumedTroopIds, res.Err) {
		return
	}

	var awardsAddedOfReset []*msg.ObjInfo
	// eat mat troops
	for i := 0; i < len(req.ConsumedTroopIds); i++ {
		temp := game.DelTroop(r, req.ConsumedTroopIds[i], res.Err)
		if res.Err.Id != 0 {
			return
		}
		awardsAddedOfReset = append(awardsAddedOfReset, temp...)
	}

	// add rewards
	var addedAwards []*msg.ObjInfo
	awardsToAdd := game.GetTroopStarUpAwardX(t.ConfigId, t.Star, res.Err)
	if len(awardsToAdd) > 0 {
		addedAwards, _ = game.AddStaticObjs(r, awardsToAdd, res.Err)
	}

	// change my star
	t.Star++

	// 只关心次数
	game.RefreshTaskCount(r, msg.TaskCondType_TCT_TroopStarUpCount, 0, 1, 0)

	res.Id = t.Id
	res.Star = t.Star

	// sync sub troops!
	// no sync myself.
	// 增加重置奖励
	addedAwards = append(addedAwards, awardsAddedOfReset...)

	// 合并
	addedAwards = game.MergeObjInfo(addedAwards)

	changedKeys := game.MakeChangeKeysFromTroopIds(req.ConsumedTroopIds)
	changedKeys = append(changedKeys, game.MakeChangeKeysFromObj(addedAwards)...)
	game.TryAppendSyncObjsMsgWithChangedIds(ctx, r, addedAwards, changedKeys)

	// 遍历阵容，如果有空的阵容，随便填一个英雄
	game.ValidFormationAndTrySync(ctx, r)

	// 影响羁绊
	game.RefreshTroopRelationshipLevelByTroops(r, troopConfigIds)

	// 刷新战力
	game.RefreshTroopPower(r, t, false)

	// 涉及英雄删除的地方，鲁棒的更新所有队伍战力
	game.RefreshFormationPowerAll(r)
	// 刷新总战力
	game.RefreshTotalPower(r)
	// if needed, update r
	c.MustUpdate(r.Id, r)

	return
}

func (*HandlerHttp) LevelUpTalent(ctx context.Context, req *msg.LevelUpTalentR) (res *msg.LevelUpTalentA, err error) {
	res = &msg.LevelUpTalentA{
		Err: &msg.ErrorInfo{},
	}

	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}

	var c redisdb.PlayerInfoCache
	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}

	// 补充加工错误
	defer func() {
		if res.Err.InternalErrId == game.InternalErrCondBuildingLevel {
			//当前天赋名称	所需建筑等级
			res.Err.Id = msg.Ecode_TALENT_BUILDING_LEVEL
			res.Err.IntParams = []int64{int64(req.TalentId), res.Err.InternalParam2}
			return
		} else if res.Err.InternalErrId == game.InternalErrUnEnoughItemOrCurrency {
			res.Err.Id = msg.Ecode_TALENT_CURRENCY
			res.Err.IntParams = append([]int64{int64(req.TalentId)}, res.Err.IntParams...)
			return
		}
	}()
	// do logic with r
	// check troop
	troop := game.GetTroop(r, req.TroopId, res.Err)
	if res.Err.Id != 0 {
		return
	}

	// check talent is matched to troop
	troopConf, _ := conf.GetTroopConfig(troop.ConfigId)
	if !game.CheckConfData(troopConf, troop.ConfigId, res.Err) {
		return
	}
	//log.Debug(troopConf)

	if !util.InArray32(troopConf.TalentGroupId, req.GroupId) {
		res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "talent group id err"
		return
	}

	talentGroupConf, _ := conf.GetTalentGroupConfig(req.GroupId)
	if !game.CheckConfData(talentGroupConf, req.GroupId, res.Err) {
		return
	}
	if !util.InArray32(talentGroupConf.TalentList, req.TalentId) {
		res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "talent is not in troop's group"
		return
	}

	//tExt := game.MustGetTroopExt(r, req.TroopId)
	oldLevel := game.GetTroopTalentLevel(r, req.TroopId, req.GroupId, req.TalentId)

	// check max level
	talentConf, _ := conf.GetTalentDataConfig(req.TalentId)
	if !game.CheckConfData(talentConf, req.TalentId, res.Err) {
		return
	}

	if oldLevel >= talentConf.MaxLv {
		res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "upto max level"
		return
	}

	// check talent info
	levelConf, _ := game.GetTalentLevelConfData(r, req.TroopId, req.GroupId, req.TalentId, res.Err)
	if res.Err.Id != 0 {
		return
	}

	// check talent condition (pre talents...)
	for needTalentId, needLv := range levelConf.TalentLevelCondNeeds {
		if game.GetTroopTalentLevel(r, req.TroopId, req.GroupId, needTalentId) < needLv {
			res.Err.Id, res.Err.DebugMsg = msg.Ecode_UNENOUGH_TALENT_LEVEL, "unenough talent level"
			res.Err.IntParams = []int64{int64(req.TalentId), int64(needTalentId), int64(needLv)}
			return
		}
	}

	// check normal condition (judge cond id...)
	if !game.CheckCondition(r, levelConf.BuildingLevelCondNeed, res.Err) {
		return
	}

	// check consumes
	costs, ok := game.JsonToPb_StaticObjs(levelConf.Cost, levelConf, levelConf.Id, res.Err)
	if !ok {
		return
	}
	if !game.CheckConsumes(r, costs, res.Err) {
		return
	}

	// sub consumes
	if !game.SubConsumes(r, costs, res.Err) {
		return
	}

	// do level up
	newLevel := oldLevel + 1
	game.SetTroopTalentLevel(r, req.TroopId, req.GroupId, req.TalentId, newLevel)

	// 刷新战力
	game.RefreshTroopPower(r, troop, true)

	// sync
	game.TryAppendSyncObjsMsgWithConsumes(ctx, r, nil, costs)
	// 刷新总战力
	game.RefreshTotalPower(r)
	// if needed, update r
	c.MustUpdate(r.Id, r)

	res.GroupId = req.GroupId
	res.TroopId = req.TroopId
	res.TalentId = req.TalentId
	res.DestLevel = newLevel

	return
}

func (*HandlerHttp) ResetTroop(ctx context.Context, req *msg.ResetTroopR) (res *msg.ResetTroopA, err error) {
	res = &msg.ResetTroopA{
		Err: &msg.ErrorInfo{},
	}

	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}

	var c redisdb.PlayerInfoCache
	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}

	// do logic with r
	troop := game.GetTroop(r, req.TroopId, res.Err)
	if res.Err.Id != 0 {
		return
	}

	ret := game.CheckResetTroopCondition(troop)
	if false == ret {
		res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, " troop level is too low "
		return
	}

	consume := game.GetResetTroopConsume()
	if !game.CheckConsume(r, consume, res.Err) {
		return
	}

	// sub consume
	if !game.SubConsume(r, consume, res.Err) {
		return
	}

	// if needed, update r
	addedObjs := game.ResetTroop(r, troop.Id, res.Err)
	if res.Err.Id != 0 {
		return
	}

	// 刷新战力
	game.RefreshTroopPower(r, troop, true)
	// 刷新总战力
	game.RefreshTotalPower(r)
	c.MustUpdate(r.Id, r)

	// sync.
	changedKeys := game.MakeChangeKeysFromTroopIds([]int32{req.TroopId})
	consumeKeys := game.MakeChangeKeysFromStaticObj([]*msg.StaticObjInfo{consume})
	changedKeys = append(changedKeys, consumeKeys...)
	changedKeys = append(changedKeys, game.MakeChangeKeysFromObj(addedObjs)...)

	game.TryAppendSyncObjsMsgWithChangedIds(ctx, r, addedObjs, changedKeys)

	return
}

func (*HandlerHttp) LayOffTroop(ctx context.Context, req *msg.LayOffTroopR) (res *msg.LayOffTroopA, err error) {
	res = &msg.LayOffTroopA{
		Err: &msg.ErrorInfo{},
	}

	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}

	var c redisdb.PlayerInfoCache
	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}

	// do logic with r
	var addedObjs []*msg.ObjInfo

	// 不怕重复
	troopConfigIds := make([]int32, 0)

	for _, v := range req.TroopId {
		res.Err = &msg.ErrorInfo{}
		troop := game.GetTroop(r, v, res.Err)
		if res.Err.Id != 0 {
			return
		}

		troopConf, _ := conf.GetTroopConfig(troop.ConfigId)
		if !game.CheckConfData(troopConf, troopConf.Id, res.Err) {
			return
		}

		troopConfigIds = append(troopConfigIds, troop.ConfigId)

		// 按规则走
		//if troopConf.CanDismiss == false {
		if !gconst.TroopCanDismiss(troopConf.BaseStar) {
			res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "troop can not dismiss"
			return
		}

		addedObj := game.LayOff(r, troop.Id, troop.ConfigId, res.Err)
		if res.Err.Id != 0 {
			return
		}
		addedObjs = append(addedObjs, addedObj...)

		// fix 专长返还
		a2 := game.DelTroop(r, troop.Id, res.Err)
		addedObjs = append(addedObjs, a2...)

		if res.Err.Id != 0 {
			return
		}
	}

	// 遍历阵容，如果有空的阵容，随便填一个英雄
	game.ValidFormationAndTrySync(ctx, r)

	// 影响羁绊
	game.RefreshTroopRelationshipLevelByTroops(r, troopConfigIds)

	// 涉及英雄删除的地方，鲁棒的更新所有队伍战力
	game.RefreshFormationPowerAll(r)
	// 刷新总战力
	game.RefreshTotalPower(r)
	c.MustUpdate(r.Id, r)

	changedKeys := game.MakeChangeKeysFromTroopIds(req.TroopId)
	changedKeys = append(changedKeys, game.MakeChangeKeysFromObj(addedObjs)...)

	game.TryAppendSyncObjsMsgWithChangedIds(ctx, r, addedObjs, changedKeys)

	// if needed, update r

	return
}

// 升级专长
func (*HandlerHttp) LevelUpSpecialty(ctx context.Context, req *msg.LevelUpSpecialtyR) (res *msg.LevelUpSpecialtyA, err error) {
	res = &msg.LevelUpSpecialtyA{
		Err: &msg.ErrorInfo{},
	}

	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}

	var c redisdb.PlayerInfoCache
	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}

	if req.DestLevel <= 1 {
		res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "dest level too min"
		return
	}

	// do logic with r
	// judge troop exist.
	t := game.GetTroop(r, req.TroopId, res.Err)
	if res.Err.Id != 0 {
		return
	}

	pairRef := game.GetTroopSpecialty(t, req.SpecialtyId)
	if pairRef == nil {
		res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "not found specialty"
		return
	}

	specialtyConf, _ := conf.GetSpecialtyConfig(req.SpecialtyId)
	if !game.CheckConfData(specialtyConf, req.SpecialtyId, res.Err) {
		return
	}

	// 预设置，报错的时候还原到初始等级
	res.DestLevel = pairRef.Value

	// max level -- from some building.
	building := game.GetBuildingByType(r, int32(msg.BuildingType_BT_Specialty))
	if building == nil {
		res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "not found building"
		return
	}

	buildingLevelConf, _ := game.GetLevelConfByBuilding(building, res.Err)
	if res.Err.Id != 0 {
		return
	}
	MaxLevel := buildingLevelConf.SpecialtyLvLimit
	// 取二者中的最小值
	if MaxLevel > specialtyConf.MaxLv {
		MaxLevel = specialtyConf.MaxLv
	}

	// correct destLevel
	finalDestLevel := req.DestLevel
	if finalDestLevel > MaxLevel {
		log.Debugf("Correct level from %d to %d", req.DestLevel, finalDestLevel)
		finalDestLevel = MaxLevel
		// 直接返回...
		res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "overflow max level"
		return
	}

	// check cond
	// 升级的数据总是用上一条数据，不如升到2，就用1的数据
	finalLevelId, ok := specialtyConf.SpecialtyLevelCfgs[finalDestLevel-1]
	if !ok {
		res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "not foudn special level cfg"
		return
	}
	destLevelData, _ := conf.GetSpecialtyLevelConfig(finalLevelId)
	if !game.CheckConfData(destLevelData, finalLevelId, res.Err) {
		return
	}

	// check last cond
	if !game.CheckCondition(r, destLevelData.BuildingCondition, res.Err) {
		return
	}

	// get consume
	consumes := game.GetTroopSpecialtyUpConsume(specialtyConf, pairRef.Value, finalDestLevel, res.Err)
	if res.Err.Id != 0 {
		return
	}

	//log.Debugf("consumesA:%v", consumes)

	if !game.CheckConsumes(r, consumes, res.Err) {
		return
	}

	if !game.SubConsumes(r, consumes, res.Err) {
		return
	}

	// do levelup
	pairRef.Value = finalDestLevel
	res.DestLevel = pairRef.Value

	// 刷新战力
	game.RefreshTroopPower(r, t, true)

	// sync changed items.
	//log.Debugf("consumesB:%v", consumes)
	game.TryAppendSyncObjsMsgWithConsumes(ctx, r, nil, consumes)

	// 刷新总战力
	game.RefreshTotalPower(r)

	// if needed, update r
	c.MustUpdate(r.Id, r)

	return
}

func (*HandlerHttp) AddTroopRelationship(ctx context.Context, req *msg.AddTroopRelationshipR) (res *msg.AddTroopRelationshipA, err error) {
	res = &msg.AddTroopRelationshipA{
		Err: &msg.ErrorInfo{},
	}

	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}

	var c redisdb.PlayerInfoCache
	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}

	// do logic with r
	confData, _ := conf.GetTroopRelationshipConfig(req.RelationId)
	if !game.CheckConfData(confData, req.RelationId, res.Err) {
		return
	}

	ext := game.MustGetPlayerInfoExt(r)
	if ext.TroopRelationships == nil {
		ext.TroopRelationships = make(map[int32]*msg.TroopRelationship)
	}
	if _, ok := ext.TroopRelationships[req.RelationId]; !ok {
		ext.TroopRelationships[req.RelationId] = &msg.TroopRelationship{
			Id:        req.RelationId,
			Level:     0,
			TroopData: make(map[int32]int32),
		}
	}
	if ext.TroopRelationships[req.RelationId].TroopData == nil {
		ext.TroopRelationships[req.RelationId].TroopData = make(map[int32]int32)
	}

	// 同步客户端发上来的数据
	for troopConfigId, troopId := range req.TroopData {
		// check kv
		if !util.InArray32(confData.NeedHero, troopConfigId) {
			res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "un match need hero"
			res.Err.Id = 2201
			return
		}
		if troopId != 0 {
			_ = game.GetTroop(r, troopId, res.Err)
			if res.Err.Id != 0 {
				return
			}
		}
		ext.TroopRelationships[req.RelationId].TroopData[troopConfigId] = troopId
	}

	game.RefreshTroopRelationshipLevel(r, []int32{req.RelationId})
	// 刷新总战力
	game.RefreshTotalPower(r)
	// if needed, update r
	c.MustUpdate(r.Id, r)

	return
}
