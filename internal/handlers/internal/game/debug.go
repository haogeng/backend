package game

import (
	"bitbucket.org/funplus/golib/deepcopy"
	"context"
	"server/internal"
	"server/internal/game"
	"server/internal/gconst"
	"server/internal/util/time3"
	"server/pkg/fungin/xmiddleware"
	"server/pkg/gen/msg"
	"server/pkg/gen_redis/redisdb"
)

func (*HandlerHttp) DebugAddObj(ctx context.Context, req *msg.DebugAddObjR) (res *msg.DebugAddObjA, err error) {
	res = &msg.DebugAddObjA{
		Err: &msg.ErrorInfo{},
	}
	if !internal.Config.Development {
		res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "dont support DebugAddObj on product"
		return
	}

	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}

	var c redisdb.PlayerInfoCache

	r := c.Find(uid)
	if r == nil {
		err = gconst.ErrNoUser
		return
	}

	// do logic with r
	statics := game.ParseGmAddObjCmd(req.Info, res.Err)
	if res.Err.Id != 0 {
		return
	}

	if len(statics) == 0 {
		return
	}

	var totalAddedObjs []*msg.ObjInfo
	var consumes []*msg.StaticObjInfo
	for i := 0; i < len(statics); i++ {
		o := statics[i]
		// 加邮件
		if o.Type == int32(msg.ObjType_OT_Mail) {
			for c := 0; c < int(o.Count); c++ {
				m := game.NewMailFromConf(o.Id, "", "", nil, nil, res.Err)
				if res.Err.Id != 0 {
					return
				}

				game.AddMail(r, m, res.Err)
				if res.Err.Id != 0 {
					return
				}
			}
			continue
		}

		if o.Count > 0 {
			addedObjs, _ := game.AddStaticObj(r, o, res.Err)
			totalAddedObjs = append(totalAddedObjs, addedObjs...)
		} else {
			tempConsume := deepcopy.Copy(o).(*msg.StaticObjInfo)
			tempConsume.Count = -tempConsume.Count
			ok := game.SubConsume(r, tempConsume, res.Err)
			if ok {
				consumes = append(consumes, tempConsume)
			}
		}
	}

	game.TryAppendSyncObjsMsgWithConsumes(ctx, r, totalAddedObjs, consumes)
	// if needed, update r
	c.MustUpdate(r.Id, r)

	return
}

func (*HandlerHttp) DebugSetServerTempTime(ctx context.Context, req *msg.DebugSetServerTempTimeR) (res *msg.DebugSetServerTempTimeA, err error) {
	res = &msg.DebugSetServerTempTimeA{
		Err: &msg.ErrorInfo{},
	}
	if internal.Config.Development {
		res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, "dont support DebugSetServerTempTime on product"
		return
	}

	// like gm
	tempErr := time3.SetTimeNowForTestFromStr(req.Info)
	if tempErr != nil {
		res.Err.Id, res.Err.DebugMsg = msg.Ecode_INVALID, tempErr.Error()
		return
	}

	return
}
