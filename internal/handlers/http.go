package handlers

import (
	ark_middleware "bitbucket.org/funplus/arkmid"
	"bitbucket.org/funplus/sandwich/current"
	"bitbucket.org/funplus/sandwich/protocol/netutils"
	"errors"
	"server/internal"
	"server/internal/handlers/internal/clan"
	"server/internal/handlers/internal/gm"
	"server/internal/handlers/internal/info"
	"server/internal/handlers/internal/res"
	"server/internal/log"
	"server/pkg/cache"
	"server/pkg/fungin/xmiddleware/reqcountlimit"
	"server/pkg/gen/msg"

	"server/internal/handlers/internal/example"
	"server/internal/handlers/internal/game"
	"server/internal/handlers/internal/local_test"
	"server/internal/handlers/internal/login"
	"server/pkg/fungin/xmiddleware"

	"bitbucket.org/funplus/ark"
	"bitbucket.org/funplus/arkmid/bodylimit"
	midrecover "bitbucket.org/funplus/arkmid/recover"
	"bitbucket.org/funplus/sandwich"
)

func panicCatcher(ctx *ark.Context, err error) error {
	defer func() {
		if reason := recover(); reason != nil {
			log.Error("panicCatcher recover:%v", reason)
		}
	}()

	log.Info("panicCatcher")
	// ark 底层会打印日志
	log.Errorf("panicCatcher: server internal panic %v", err)
	//errToC := fmt.Errorf("server internal panic")
	errToC := &netutils.ErrorResponse{
		Code:    int32(msg.Ecode_SERVER_PANIC),
		Message: "server internal panic",
	}
	return sandwich.SendWithArkContext(ctx, errToC)
}

func arkErrorCatcher() ark.MiddlewareFunc {
	//log.Info("arkErrorCatcher")

	return func(ctx *ark.Context, next ark.HandlerFunc) error {
		if err := next(ctx); err != nil {
			log.Infof("ErrorCatcher: %v", err)
			return sandwich.SendWithArkContext(ctx, err)
		}
		return nil
	}
}

//func arkPermissionCheck(tokenKey string) ark.MiddlewareFunc {
//	return func(next ark.HandlerFunc) ark.HandlerFunc {
//		return func(ctx context.Context) error {
//			ts := ctx.GetHeader(tokenKey)
//			if len(ts) == 0 {
//				return errors.New("token empty")
//			}
//			token := &internal.GameAccessToken{}
//			b, err := base64.StdEncoding.DecodeString(ts)
//			if err != nil {
//				return err
//			}
//			err = json.Unmarshal(b, token)
//			if err != nil {
//				return err
//			}
//			ctx.Set("token", token)
//			return next(ctx)
//		}
//	}
//}

func InitHttpRouter(engine *ark.Ark) {
	c := internal.Config
	engine.Use(
		internal.ArkAccessLog(ark_middleware.DefaultSkipper),

		midrecover.New(midrecover.WithPanicCatcher(panicCatcher)),

		bodylimit.New(bodylimit.WithBodyLimit(4*1024*1024)),

		current.InstallCurrent(),

		arkErrorCatcher(),

		// 限流器
		//xmiddleware.ReqCountRateLimit(c.RateLimitConfig.MaxCount, c.RateLimitConfig.FillAllDuration, true, true),
		reqcountlimit.New(reqcountlimit.WithMaxCountPerFill(c.RateLimitConfig.MaxCount),
			reqcountlimit.WithTokenFillDuration(c.RateLimitConfig.FillAllDuration),
			reqcountlimit.WithExpireDuration(c.RateLimitConfig.FillAllDuration*2),
			reqcountlimit.WithCacheProvider(cache.GetGoCache()),
			reqcountlimit.WithCacheKeyProvider(&reqcountlimit.IpCacheKeyProvider{})),

		// 检查内部redis，db等连接状态
		xmiddleware.CheckInternalCon(),
	)

	engine.SetNoRouter(func(ctx *ark.Context) error {
		err := errors.New("router not found")
		log.Info(err.Error())
		return sandwich.SendWithArkContext(ctx, err)
	})
	engine.SetMethodNotAllowed(func(ctx *ark.Context) error {
		err := errors.New("method not allowed")
		log.Info(err.Error())
		return sandwich.SendWithArkContext(ctx, err)
	})

	//engine.GetPost("/favicon.ico", func(ctx context.Context) error {
	//	_ = ctx.JSON(http.StatusOK, `"{"ret":"ok"}`)
	//	return nil
	//})

	// Register micro server into api group
	apiGroup := engine.Group("api")
	{
		local_test.InitHttpRouter(apiGroup)
		res.InitHttpRouter(apiGroup)
		example.InitHttpRouter(apiGroup)
		login.InitHttpRouter(apiGroup)
		game.InitHttpRouter(apiGroup)
		gm.InitHttpRouter(apiGroup)
		info.InitHttpRouter(apiGroup)
		clan.InitHttpRouter(apiGroup)
		//game.InitHttpRouter(apiGroup)
		// ...
	}
}
