package internal

import (
	"fmt"
	"github.com/zero-yy/viper"
	"log"
	"server/internal/util"
	"server/pkg/cache"
	"server/pkg/gen_db"
	"server/pkg/persistredis"
	"strconv"
	"strings"
	"time"
)

type SdkConfig struct {
	Debug  bool   `default:"true"`
	Dest   string `default:"global"`
	Secret string
}

type CommonConfig struct {
	// No use now, just for goconf..
	//conf.AutoOptions
	ConfigEnv       string
	PmConfPath      string `default:"pkg/gen/conf/rawdata"`
	LightHousePath  string `default:"configs/lighthouse.json"`
	HotConfigPath   string `default:"configs/hotconfig.json"`
	LogPath         string
	TcpPort         int     `default:"8087"`
	TcpSendChanSize int     `default:"1024"`
	HttpPort        int     `default:"8088"`
	HttpRateLimit   float64 `default:"3000"`
	Development     bool    `default:"true"`
	ReadTimeout     int     `default:"30"`
	EtcdConf        bool    `default:"false"`
	EtcdEndpoint    []string

	//// Gm的白名单
	//WhiteIpsOfGm []string

	LogWithServerInfo bool

	TokenTtl      time.Duration
	ResponseTtl   time.Duration
	SequenceIdTtl time.Duration
	DbCacheTtl    time.Duration

	// 是否启用单个用户sequence处理的分布式锁
	SequenceDealWithLock bool

	////# 一个服最大人数
	//UserCountPerServer int

	//# 用于服务发现(开发阶段的各种服务器状态)
	EtcdEndPointsForServerList []string

	//# 使用多长时间(2m <= cur <= 1h)来测试一天 比如 ["1h", "30m", "10m", "2m"]
	//# 其他值关闭测试. 只在develop模式下生效
	DurationUsingForTestDaily time.Duration

	// 本地测试是否打开
	OpenLocalTest bool
}

type VersionConfig struct {
	ServerMainVer  int
	ServerMinorVer int
	ClientMainVer  int
	ClientMinorVer int
}

type RateLimitConfig struct {
	MaxCount        int
	FillAllDuration time.Duration
}

type ServerInfo struct {
	Name string
	Url  string
}
type ServerList struct {
	ServerInfos []ServerInfo
}

func (m *VersionConfig) DbVer() string {
	return fmt.Sprintf("%d.%d", m.ServerMainVer, m.ServerMinorVer)
}

type config struct {
	CommonConfig
	VersionConfig
	RateLimitConfig RateLimitConfig

	DbConfig           gen_db.Conf
	RedisConfig        cache.RedisConfig
	PersistRedisConfig persistredis.PersistRedisConfig

	ServerList ServerList
	SdkConfig  SdkConfig
}

func (c config) String() string {
	//cj, _ := json.MarshalIndent(c.CommonConfig, "", "\t")
	return fmt.Sprintf("\n\nCommon: %+v\n\nDbConfig: %v\n\nRedisConfig: %+v\n\nServerList: %+v\n\nSdkConfig: %+v\n\n",
		c.CommonConfig,
		c.DbConfig,
		c.RedisConfig,
		c.ServerList,
		c.SdkConfig)
}

//
var Config = &config{}
var EnvTag string

func setEnvTag() {
	viper.SetEnvPrefix("GMX")
	err := viper.BindEnv("CONFIG_POSTFIX")
	if err != nil {
		panic(fmt.Errorf("Bindenv CONFIG_POSTFIX err %v\n", err))
	}

	env := viper.Get("CONFIG_POSTFIX")
	if env == nil {
		panic(fmt.Errorf("Please set env variable GMX_CONFIG_POSTFIX first\n"))
	}

	EnvTag = env.(string)
}

func getEnvString(envName string) string {
	viper.SetEnvPrefix("GMX")
	err := viper.BindEnv(envName)
	if err != nil {
		panic(err)
	}

	env := viper.Get(envName)
	if env == nil {
		fmt.Printf("Not find env GMX_%s\n", envName)
		return ""
	}

	return env.(string)
}
func getEnvInt(envName string) int {
	viper.SetEnvPrefix("GMX")
	err := viper.BindEnv(envName)
	if err != nil {
		panic(err)
	}

	env := viper.Get(envName)
	if env == nil {
		fmt.Printf("Not find env GMX_%s\n", envName)
		return 0
	}

	s := env.(string)
	ret, err := strconv.Atoi(s)
	if err != nil {
		panic(fmt.Errorf("getEnvInt GMX_%s err %v", envName, err))
	}
	return ret
}

func MustLoadConfig() {
	setEnvTag()

	baseConfigName := "app"
	configName := baseConfigName
	// app or app_$(env)
	if len(EnvTag) > 0 {
		configName = fmt.Sprintf("%s_%s", configName, EnvTag)
		configName = strings.ToLower(configName)
	}

	//baseConfigName += ".toml"
	//configName += ".toml"

	log.Printf("\nRead config: %s (%s)\n", baseConfigName, configName)

	viper.SetConfigType("toml")
	viper.AddConfigPath("./configs/")

	// base
	{
		viper.SetConfigName(baseConfigName)
		err := viper.ReadInConfig()
		if err != nil {
			panic(fmt.Errorf("Read config %v\n", err))
		}
	}

	// custom
	if configName != baseConfigName {
		viper.SetConfigName(configName)
		err := viper.MergeInConfig()
		if err != nil {
			panic(fmt.Errorf("Merge config %v \n", err))
		}
	}

	err := viper.Unmarshal(Config)
	if err != nil {
		panic(fmt.Errorf("Unmarshal %v\n", err))
	}

	//Config.RedisConfig.Db = getEnvInt("ENV_REDIS_DB")
	//Config.DbConfig.MysqlConf.MysqlDbName = Config.DbConfig.MysqlConf.MysqlDbName + getEnvString("ENV_MYSQL_POSTFIX")
	//Config.HttpPort = getEnvInt("ENV_HTTP_PORT")

	log.Printf("Config: %v\n", Config)
}

// 得到改服务器信息
// like "gmx_server/xx": "http://10.0.84.152:28084/api/"
func GetServiceInfo() (key string, addr string) {
	//subName := getEnvString("ENV_MYSQL_POSTFIX")
	subName := EnvTag
	if len(subName) == 0 {
		subName = fmt.Sprintf("S%d", time.Now().Unix())
	}
	key = Tag + "/" + subName

	ip := util.GetLocalIP()
	addr = fmt.Sprintf("http://%s:%d/api/", ip, Config.HttpPort)
	return
}
