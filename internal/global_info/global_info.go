package global_info

import (
	"fmt"
	"server/internal"
	"server/internal/log"
	"server/pkg/cache"
	"server/pkg/dlock"
	"server/pkg/gen/msg"
	"server/pkg/gen_redis/redisdb"
	"sync"
	"time"
)

type globalInfo struct {
}

var _globalInfo *globalInfo

var once sync.Once

func GetGlobalInfo() *globalInfo {
	once.Do(func() {
		if _globalInfo == nil {
			_globalInfo = &globalInfo{}
			_globalInfo.mustInit()
		}
	})
	return _globalInfo
}

func (g *globalInfo) mustInit() {
	var c redisdb.GlobalInfoCache
	// 确保有一条记录,
	// 严格意义上需要加分布式锁，但create重复也没事，此处不加锁也可以
	// docker stack启动的时候，mustCretae会打架，，故加redis 锁
	dLockKeyPost := "globalInfo:mustInit"
	dLockKey := cache.GetLockKeyFor(dLockKeyPost)
	lockValue, locked := dlock.AcquireLockWithTimeoutX(dLockKey, time.Second*10, time.Second*30)

	if !locked {
		panic("get globalInfo lock failed")
	}

	defer dlock.ReleaseLockX(dLockKey, lockValue)

	r := c.Find(1)
	if r == nil {
		data := &msg.GlobalInfo{
			Id:                 1,
			CurServerId:        1,
			CurServerUserCount: 1,
		}
		c.MustCreate(1, data)

		r = c.Find(1)
		if r == nil {
			panic(fmt.Errorf("globalInfo must have one record with id = 1"))
		}
	}
	log.Infof("global info: %+v\n", r)
}

var (
	globalInfoLockKey = "gmx:lock:globalinfo"
	globalInfoLockTtl = time.Second * 10
	// 允许一定的错误率
	globalInfoLockGetTimeout = time.Second * 10
)

func (g *globalInfo) UnsafeServerId() (serverId int32) {
	var c redisdb.GlobalInfoCache
	r := c.Find(1)
	if r == nil {
		panic(fmt.Errorf("unsafe server id err cause not found global info"))
	}
	return r.CurServerId
}

// 每次调用都会增加当前服务器人数，，人数满后新开服
func (g *globalInfo) DispatchServerID() (serverId int32, curUserCount int32, err error) {
	lockValue, ok := dlock.AcquireLockWithTimeoutX(globalInfoLockKey, globalInfoLockTtl, globalInfoLockGetTimeout)
	var c redisdb.GlobalInfoCache
	if ok {
		defer func() {
			dlock.ReleaseLockX(globalInfoLockKey, lockValue)
		}()

		r := c.Find(1)
		if r == nil {
			panic(fmt.Errorf("dispatch server id err cause not found global info"))
		}

		r.CurServerUserCount++

		// 安全检查
		userCount := internal.GetHotConfig().UserCountPerServer
		if !internal.Config.Development {
			// 正式的一个服最起码1000人
			if userCount < 1000 {
				log.Debug("UserCountPerServer should more than 1000\n")
				userCount = 1000
			}
		}

		if r.CurServerUserCount > int32(userCount) {
			// new server
			r.CurServerId++
			r.CurServerUserCount = 1
			log.Debugf("New server %d maxUserCount%d\n", r.CurServerId, userCount)

		}
		c.MustUpdate(1, r)

		return r.CurServerId, r.CurServerUserCount, nil
	} else {
		// 不算致命bug，返回当前服务器ID
		r := c.Find(1)
		if r == nil {
			panic(fmt.Errorf("dispatch server id err cause not found global info"))
		}

		// 忽略错误，外界不用关系错误
		//return r.CurServerId, fmt.Errorf("DispatchServerID get lock failed")
		return r.CurServerId, r.CurServerUserCount, nil
	}
}

func (g *globalInfo) IncrementClanCount() (curClanCount int32, err error) {
	lockValue, ok := dlock.AcquireLockWithTimeoutX(globalInfoLockKey, globalInfoLockTtl, globalInfoLockGetTimeout)
	var c redisdb.GlobalInfoCache
	if ok {
		defer func() {
			dlock.ReleaseLockX(globalInfoLockKey, lockValue)
		}()

		r := c.Find(1)
		if r == nil {
			panic(fmt.Errorf("dispatch server id err cause not found global info"))
		}

		r.ClanIncrId++

		c.MustUpdate(1, r)

		return r.ClanIncrId, nil
	} else {
		panic(fmt.Errorf("get global lock error \n"))
	}
}
