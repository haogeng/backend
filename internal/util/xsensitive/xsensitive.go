// 脏字过滤
package xsensitive

import (
	"fmt"
	"github.com/zero-yy/sensitive"
)

// 对于只读，携程安全，，
// 读写不安全, 切勿运行时修改脏字
// 谨慎使用，，，没有做线程安全处理

var filter *sensitive.Filter

// 线程不安全
func MustLoad(resFileName string) {
	filter = sensitive.New()
	err := filter.LoadWordDict(resFileName)
	if err != nil {
		panic(fmt.Errorf("xsensitive load failed: %v", err))
	}
}

func ContainsBadWord(str string) (badWord string, exist bool) {
	if filter == nil {
		panic(fmt.Errorf("You should call xsensitive MustLoad"))
		return
	}

	exist, badWord = filter.FindIn(str)
	return
}
