package util

import (
	"bytes"
	"encoding/gob"
	"fmt"
	"math/rand"
	"net"
	"reflect"
	"server/internal/util/time3"
	"strconv"
	"strings"
	"time"
	"unsafe"
)

// GetLocalIP returns the non loopback local IP of the host
func GetLocalIP() string {
	addrs, err := net.InterfaceAddrs()
	if err != nil {
		return ""
	}
	for _, address := range addrs {
		// check the address type and if it is not a loopback the display it
		if ipnet, ok := address.(*net.IPNet); ok && !ipnet.IP.IsLoopback() {
			if ipnet.IP.To4() != nil {
				return ipnet.IP.String()
			}
		}
	}
	return ""
}

// Time2String 时间转换成字符串
func Time2String(time time.Time) string {
	time = time.UTC()
	return time.Format("20060102150405")
}

// String2Time 字符串转换成时间
func String2Time(str string) (time.Time, error) {
	return time.Parse("20060102150405", str)
}

// StringSortMerge 字符串排列拼接，相同的串返回结果相同
func StringSortMerge(str1 string, str2 string) string {
	if str1 < str2 {
		return str1 + ":" + str2
	}
	return str2 + ":" + str1
}

// MaxInt 最大数
func MaxInt(args ...int) int {
	max := args[0]

	for _, i := range args {
		if max < i {
			max = i
		}
	}
	return max
}

// MinInt 最小数
func MinInt(args ...int) int {
	min := args[0]
	for _, i := range args {
		if min > i {
			min = i
		}
	}
	return min
}

func MinInt32(args ...int32) int32 {
	min := args[0]
	for _, i := range args {
		if min > i {
			min = i
		}
	}
	return min
}

var t2017 = time.Date(2017, 1, 1, 0, 0, 0, 0, time.UTC)

// TimeSince2017 从2017年到现在的 duration
func TimeSince2017(t time.Time) time.Duration {
	return t.Sub(t2017)
}

func InArray(container []int64, element int64) bool {
	if container == nil {
		return false
	}

	if len(container) == 0 {
		return false
	}

	for _, x := range container {
		if x == element {
			return true
		}
	}

	return false
}

func InArray32(container []int32, element int32) bool {
	if container == nil {
		return false
	}

	if len(container) == 0 {
		return false
	}

	for _, x := range container {
		if x == element {
			return true
		}
	}

	return false
}

func Clamp(v int32, min int32, max int32) int32 {
	if v < min {
		v = min
	} else if v > max {
		v = max
	}
	return v
}

// 得到权重总和
func GetTotalWeights(m []int32) int32 {
	s := int32(0)
	for _, v := range m {
		s = s + v
	}

	return s
}

// like:
//1,2,3,4-10,11-50,50+
//ary1 := []int32{1,2,3,10,50,100}
//ary2 := []int32{5000, 3000, 2000, 1000, 500, 200}
func GetRangeAryValue(indexAry []int32, valueAry []int32, no int32) int32 {
	if len(indexAry) == 0 {
		return 0
	}
	index := len(indexAry) - 1
	for i := 0; i < len(indexAry); i++ {
		if no <= indexAry[i] {
			index = i
			break
		}
	}

	return GetSafeValue(valueAry, int32(index))
}

func GetSafeIndex(ary []int32, index int32) int32 {
	if index < 0 {
		return 0
	}

	if len(ary) == 0 {
		return 0
	}

	index = MinInt32(index, int32(len(ary)-1))
	return index
}

func GetSafeValue(ary []int32, index int32) int32 {
	if index < 0 {
		return 0
	}

	if len(ary) == 0 {
		return 0
	}

	index = MinInt32(index, int32(len(ary)-1))
	return ary[index]
}

func GetSafeValue64(ary []int64, index int32) int64 {
	if index < 0 {
		return 0
	}

	if len(ary) == 0 {
		return 0
	}

	index = MinInt32(index, int32(len(ary)-1))
	return ary[index]
}

func GetSafeValueFloat32(ary []float32, index int32) float32 {
	if index < 0 {
		return 0
	}

	if len(ary) == 0 {
		return 0
	}

	index = MinInt32(index, int32(len(ary)-1))
	return ary[index]
}

func MapToSlice(m map[int32]int32) (keys []int32, values []int32) {
	keys, values = nil, nil
	for k, v := range m {
		keys = append(keys, k)
		values = append(values, v)
	}
	return
}

// input: value weight
// output: value, ok
func GetRandomValueByMapWeights(m map[int32]int32, randIfZero bool) (int32, bool) {
	keys, values := MapToSlice(m)
	index := GetRandomKeyByWeights(values, randIfZero)
	if index < 0 || index >= int32(len(keys)) {
		return -1, false
	}
	return keys[index], true
}

// 根据权重得到随机值
func GetRandomKeyByWeights(m []int32, randIfZero bool) int32 {
	if len(m) == 0 {
		return -1
	}

	s := GetTotalWeights(m)

	if s == 0 {
		// 平均随机
		if randIfZero {
			i := rand.Intn(len(m))
			return int32(i)
		} else {
			return -1
		}
	} else {
		randVal := rand.Intn(int(s))
		partSum := 0

		// log.Debug("randVal %v m %v", randVal, m)
		for i := 0; i < len(m); i++ {
			partSum = partSum + int(m[i])
			if partSum > 0 && partSum > randVal {
				return int32(i)
			}
		}
	}

	fmt.Printf("Shouldnt go here\n")
	return 0
}

// 根据时间，对齐到下一个双周周日的0点
// t is UTC time
func GetNextSunday2(t time.Time) time.Time {
	// 随便找一个礼拜日的0点为参考点
	refTime := time.Date(1970, 1, 4, 0, 0, 0, 0, time.UTC)

	refUnix := refTime.Unix()
	tUnix := t.Unix()

	// 对齐到2周一个间隔
	offsetUnix := tUnix - refUnix
	oldOffsetUnix := offsetUnix

	TwoWeeksSeconds := int64(3600 * 24 * 7 * 2)
	offsetUnix = (offsetUnix + TwoWeeksSeconds - 1) / TwoWeeksSeconds * TwoWeeksSeconds

	// 当前时间增量值
	offsetUnix = offsetUnix - oldOffsetUnix

	return t.Add(time.Duration(offsetUnix) * time.Second)
}

// 通过闭包间接的实现了模板函数
// 得到索引 only one
func SliceByCond(limit int, predicate func(i int) bool) int {
	for i := 0; i < limit; i++ {
		if predicate(i) {
			return i
		}
	}
	return -1
}

// return new slice
// use as newX = RemoveFromSlice(oldX, id)
func RemoveFromSlice(slice []int64, id int64, all bool) []int64 {
	if all {
		newSlice := make([]int64, 0)
		for i := range slice {
			if slice[i] != id {
				newSlice = append(newSlice, slice[i])
			}
		}

		return newSlice
	} else {
		for i := range slice {
			if slice[i] == id {
				slice = append(slice[:i], slice[i+1:]...)
				break
			}
		}
	}

	return slice
}

// 需要复制到新数组中，对原来数组不会修改！！！
func RemoveFromSlice32(slice []int32, id int32, all bool) []int32 {
	if all {
		newSlice := make([]int32, 0)
		for i := range slice {
			if slice[i] != id {
				newSlice = append(newSlice, slice[i])
			}
		}

		return newSlice
	} else {
		for i := range slice {
			if slice[i] == id {
				slice = append(slice[:i], slice[i+1:]...)
				break
			}
		}
	}

	return slice
}

// from x.x.x.x:8888 to x.x.x.x
func GetIpFromAddr(addr string) string {
	t := strings.Split(addr, ":")
	return t[0]
}

// 一天的开始时间
func DayStart(t time.Time) time.Time {
	tm1 := time.Date(t.Year(), t.Month(), t.Day(), 0, 0, 0, 0, t.Location())
	return tm1
}

// 一周的开始时间
func ISOWeekStart(t time.Time) time.Time {
	t = DayStart(t)
	wd := t.Weekday()
	if wd == time.Monday {
		return t
	}
	offset := int(time.Monday - wd)
	if offset > 0 {
		offset -= 7
	}
	return t.AddDate(0, 0, offset)
}

// // 一周的开始时间
// func ISOMonthStart(t time.Time) time.Time {
// 	t = DayStart(t)
// 	wd := t.Weekday()
// 	if wd == time.Monday {
// 		return t
// 	}
// 	offset := int(time.Monday - wd)
// 	if offset > 0 {
// 		offset -= 7
// 	}
// 	return t.AddDate(0, 0, offset)
// }

// 当前这个周期的开始时间
func ISOPeriodStart(days int32) int64 {
	curTime := time3.Now().Unix()

	periodSeconds := int64(days * 24 * 3600)

	t := curTime / periodSeconds * periodSeconds
	return t
}

// 当前这个周期的结束时间
func ISOPeriodEnd(days int32) int64 {
	periodSeconds := int64(days * 24 * 3600)

	return ISOPeriodStart(days) + periodSeconds
}

type WeekActivity struct {
	WeekId    int64
	StartTime int64
	EndTime   int64
}

// 测试，手动改时间
// //TEST
// var testWeekOffsetDuration = -3600 * 24 * 7 * time.Second

var testWeekOffsetDuration = 0 * time.Second

func CurWeekActivity() *WeekActivity {
	wa := &WeekActivity{}
	t := time3.Now().Add(testWeekOffsetDuration)
	weekStartT := ISOWeekStart(t)

	wa.StartTime = weekStartT.Unix()
	wa.WeekId = wa.StartTime
	wa.EndTime = wa.StartTime + 3600*24*7
	return wa

	// //TEST 缩短活动间隔
	// wa := &WeekActivity{}
	// t := time3.Now().Add(testWeekOffsetDuration)
	// unixT := t.Unix()
	// interval := int64(30 * 60)
	// unixT = unixT / interval * interval

	// wa.StartTime = unixT
	// wa.WeekId = unixT
	// wa.EndTime = unixT + interval
	// return wa
}

func LastWeekActivity() *WeekActivity {
	wa := &WeekActivity{}
	t := time3.Now().Add(testWeekOffsetDuration)
	weekStartT := ISOWeekStart(t)

	wa.StartTime = weekStartT.Unix() - 3600*24*7
	wa.WeekId = wa.StartTime
	wa.EndTime = wa.StartTime + 3600*24*7

	return wa
}

func EscapeString(s string) string {
	// escape
	// s = strings.Replace(s, " ", "", -1)
	s = strings.Replace(s, "\t", "", -1)
	s = strings.Replace(s, "\\", "", -1)
	s = strings.Replace(s, "\n", "", -1)
	s = strings.Replace(s, ";", "", -1)
	return s
}

func DeepCopy(dst, src interface{}) error {
	var buf bytes.Buffer
	if err := gob.NewEncoder(&buf).Encode(src); err != nil {
		return err
	}
	return gob.NewDecoder(bytes.NewBuffer(buf.Bytes())).Decode(dst)
}

// 根据概率决定数量，0.8概率一次衰减
// 改函数返回[0, n-1]
func GetRandomDeltaNobalance(n int32, randVal float32) int32 {
	if n == 0 {
		return n
	}

	ret := int32(0)
	for i := int32(0); i < n; i++ {
		if rand.Float32() < randVal {
			ret = i
			break
		}
	}

	return ret
}

func SplitToInt32Ary(s string, sep string) ([]int32, error) {
	s = strings.TrimSpace(s)

	tempStrs := strings.Split(s, sep)
	tempInts := make([]int32, 0)
	for i := 0; i < len(tempStrs); i++ {
		tempI, tempErr := strconv.Atoi(tempStrs[i])
		if tempErr != nil {
			return nil, tempErr
		}
		tempInts = append(tempInts, int32(tempI))
	}
	return tempInts, nil
}

// 是否包含重复
func HasRepeat(ids []int32) bool {
	idMap := make(map[int32]bool)
	for i := 0; i < len(ids); i++ {
		id := ids[i]
		// check repeat id
		if _, ok := idMap[id]; ok {
			return true
		} else {
			idMap[id] = true
		}
	}
	return false
}

func Marshal64(l int32, h int32) int64 {
	a := int64(h) << 32
	b := int64(l) & 0xffffffff
	return a | b
}
func Unmarshal64(x int64) (l int32, h int32) {
	l = int32(x & 0xffffffff)
	h = int32(x >> 32)
	return
}

// 慎用！！！
// StringToBytes converts string to byte slice without a memory allocation.
func StringToBytes(s string) (b []byte) {
	sh := *(*reflect.StringHeader)(unsafe.Pointer(&s))
	bh := (*reflect.SliceHeader)(unsafe.Pointer(&b))
	bh.Data, bh.Len, bh.Cap = sh.Data, sh.Len, sh.Len
	return b
}
