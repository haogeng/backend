package lighthouse

import (
	"encoding/json"
	"errors"
	"github.com/Masterminds/semver"
	"server/internal/log"
	"server/pkg/xwatcher"
	"sync"
)

type Detail struct {
	ResEditorVersion  string `json:"res_editor_version"`
	ResAndroidVersion string `json:"res_android_version"`
	ResIosVersion     string `json:"res_ios_version"`
}

type UpdateInfo struct {
	Version       string `json:"version"`
	UrlPackage    string `json:"url_package"`
	UrlStore      string `json:"url_store"`
	UrlDesc       string `json:"url_desc"`
	DisplayPerDay int    `json:"display_per_day"`
}

type Server struct {
	Name       string   `json:"name"`
	Url        string   `json:"url"`
	VersionMin string   `json:"version_min"`
	VersionMax string   `json:"version_max"`
	Fallback   []string `json:"fallback"`
}

type LightHouse struct {
	ResourceDetail  Detail     `json:"res_detail"`
	UpdateDetail    UpdateInfo `json:"update_info"`
	UrlPattern      string     `json:"url_pattern"`
	LighthouseId    string     `json:"lighthouse_id"`
	ForceUpdate     bool       `json:"force_update"`
	MaintenanceOpen bool       `json:"maintenance_open"`
	ErrorInfo       string     `json:"error_info"`
	ServerInfo      []Server   `json:"servers"`

	inited bool
}

var lightHouse *LightHouse = &LightHouse{
	inited: false,
}
var m sync.RWMutex

func MustInit(path string, w *xwatcher.Watcher) {
	err := w.Register(path, lightHouse)
	if err != nil {
		panic(err)
	}

	err = w.ForceReload(path)
	if err != nil {
		panic(err)
	}
}

func (lh *LightHouse) OnDataLoaded(watchKey string, baseName string, data []byte) {
	log.Infof("LightHouse OnDataLoaded %s %s", watchKey, baseName)
	if data == nil {
		return
	}

	var temp LightHouse
	err := json.Unmarshal(data, &temp)
	if err != nil {
		log.Errorf("lightHouse err: %v", err)
	} else {
		// replace New
		setLightHouse(&temp)
	}
	log.Info("NewLightHouse: ", lightHouse)
}

func GetLightHouse() *LightHouse {
	m.RLock()
	ret := lightHouse
	m.RUnlock()

	if ret == nil || !ret.inited {
		log.Error("lightHouse must init first")
		return nil
	}
	return ret
}

func setLightHouse(lh *LightHouse) {
	m.Lock()
	defer m.Unlock()
	*lightHouse = *lh
	lightHouse.inited = true
}

func getResVersion(platform string) string {
	lh := GetLightHouse()
	if platform == "ios" {
		return lh.ResourceDetail.ResIosVersion
	} else if platform == "android" {
		return lh.ResourceDetail.ResAndroidVersion
	} else if platform == "editor" {
		return lh.ResourceDetail.ResEditorVersion
	} else {
		return ""
	}
}

func CheckResVersion(appVersion string, platform string) bool {
	// 要么不传，要么就传对了
	resVersion := getResVersion(platform)
	if len(resVersion) == 0 {
		return false
	}

	return resVersion == appVersion
}

func CheckAppVersion(appVersion string) error {
	lh := GetLightHouse()
	minVersion := lh.ServerInfo[0].VersionMin
	maxVersion := lh.ServerInfo[0].VersionMax

	return doCheckAppVersion(appVersion, minVersion, maxVersion)
}

func doCheckAppVersion(appVersion, minVersion, maxVersion string) error {
	constraintLow, err := semver.NewConstraint("< " + minVersion)
	if err != nil {
		// Handle constraint not being parseable.
		return err
	}

	constraintHigh, err := semver.NewConstraint("> " + maxVersion)
	if err != nil {
		// Handle constraint not being parseable.
		return err
	}

	constraintEqual, err := semver.NewConstraint(">=" + minVersion + ", < " + maxVersion) //左闭右开[)
	if err != nil {
		// Handle constraint not being parseable.
		return err
	}

	appVer, err := semver.NewVersion(appVersion)
	if err != nil {
		// Handle version not being parsable.
		return err
	}

	value := constraintLow.Check(appVer)
	if value {
		return errors.New("low")
	}

	value = constraintHigh.Check(appVer)
	if value {
		return errors.New("high")
	}

	value, _ = constraintEqual.Validate(appVer)
	if value {
		return nil
	}
	return err
	// Validate a version against a constraint.
	/*value, msgs := constraint.Validate(version)
	log.Debugf("value:%+v",value)
	// a is false
	for _, m := range msgs {
		log.Debugf("m:%+v",m)
		// Loops over the errors which would read
		// "1.3 is greater than 1.2.3"
		// "1.3 is less than 1.4"
	}
	return value*/
}

// 沿用了部分lighthouse的字段，，直接按lighthouse发送
func BuildCheckVersionResponse(lightHouse *LightHouse, errorInfo string, forceUpdate bool) (newLightHouse *LightHouse) {
	newLightHouse = &LightHouse{
		ResourceDetail:  lightHouse.ResourceDetail,
		UpdateDetail:    lightHouse.UpdateDetail,
		UrlPattern:      lightHouse.UrlPattern,
		LighthouseId:    lightHouse.LighthouseId,
		MaintenanceOpen: lightHouse.MaintenanceOpen,
		ErrorInfo:       errorInfo,
		ServerInfo:      lightHouse.ServerInfo,
	}

	// 决定是否强更
	newLightHouse.ForceUpdate = forceUpdate

	return
}
