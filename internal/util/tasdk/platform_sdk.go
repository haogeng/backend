package tasdk

import (
	"crypto/md5"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"sort"
	"strings"
)

// dest: cn or global
func CheckSession(dest string, debug bool, sessionKey string) map[string]interface{} {
	envs := map[string]map[string]string{
		"cn": {
			"sandbox": "https://passport-dev-cn.ifunplus.cn:8443/server_api.php",
			"prod":    "https://passport-diandian-cn.ifunplus.cn:8443/server_api.php",
		},
		"global": {
			"sandbox": "https://passport-dev.funplusgame.com/server_api.php",
			"prod":    "https://passport.funplusgame.com/server_api.php",
		},
	}
	var url string
	if debug {
		url = envs[dest]["sandbox"]
	} else {
		url = envs[dest]["prod"]
	}
	resp, _ := http.Post(url, "application/x-www-form-urlencoded",
		strings.NewReader("method=check_session&session_key="+sessionKey))

	defer resp.Body.Close()
	body, _ := ioutil.ReadAll(resp.Body)
	data := make(map[string]interface{})
	json.Unmarshal(body, &data)
	return data
}

func IsValidSig(secret string, postData map[string]interface{}) bool {
	if _, ok := postData["token"]; ok {
		delete(postData, "token")
	}
	tokenAll, ok := postData["token_all"]
	if ok {
		delete(postData, "token_all")
	}
	var keys []string
	for k := range postData {
		keys = append(keys, k)
	}
	sort.Strings(keys)
	var sortStr string
	for _, k := range keys {
		sortStr += postData[k].(string)
	}
	sortStr += secret
	hasher := md5.New()
	hasher.Write([]byte(sortStr))
	sigAll := hex.EncodeToString(hasher.Sum(nil))
	fmt.Print(sigAll)
	if sigAll == tokenAll {
		return true
	}
	return false
}

/*
func checkIsDuplicated(tid string) bool {
	// 检查是否为重复订单逻辑
	return false
}

// Callback 实现的示例代码
func callback(secret string, postData map[string]interface{}) map[string]string {
	if IsValidSig(secret, postData) { //验证是否是合法的 token_all
		// update user data and write the payment log
		uid := postData["uid"]
		tid := postData["tid"] //transaction id if uid and amount:
		body := postData["app_data"]

		data := make(map[string]interface{})
		json.Unmarshal([]byte(body.(string)), &data)
		productID := data["product_id"]

		if uid != nil && tid != nil && productID != nil {
			if !checkIsDuplicated(tid.(string)) {
				//为用户增加product_id为product_id的物品, 并写入记录, 用tid做唯一校验, 如重复则不处理
				return map[string]string{"status": "OK", "reason": "no reason"}
			} else {
				return map[string]string{"status": "OK", "reason": "repeat"}
			}
		} else {
			return map[string]string{"status": "ERROR", "reason": "uid or tid or product_id is none"}
		}
	} else {
		return map[string]string{"status": "ERROR", "reason": "We got a wrong signature"}
	}
}
*/
