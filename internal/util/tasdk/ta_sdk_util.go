package tasdk

import (
	"github.com/ThinkingDataAnalytics/go-sdk/thinkingdata"
)

type EventTypeName string

const (
	APP_ID    string        = "1fec17656acb4cf49f2aa078c044d571"
	TestEvent EventTypeName = "testEvent"
)

var ta thinkingdata.TDAnalytics

func init() {
	//默认是true，代表入库
	// consumer, _ := thinkingdata.NewDebugConsumerWithWriter("SERVER_URL", "APP_ID", false)
	// consumer, _ := thinkingdata.NewDebugConsumer("SERVER_URL", "APP_ID")

	// 创建按天切分的 LogConsumer, 不设置单个日志上限
	consumer, _ := thinkingdata.NewLogConsumer("/path/to/data", thinkingdata.ROTATE_DAILY)
	// 创建按小时切分的 LogConsumer, 不设置单个日志上限
	// consumer, err := thinkingdata.NewLogConsumer("/path/to/data", thinkingdata.ROTATE_HOURLY)
	// 创建按天切分的 LogConsumer，并设置单个日志文件上限为 10 G
	// consumer, err := thinkingdata.NewLogConsumerWithFileSize("/path/to/data", thinkingdata.ROTATE_DAILY, 10*1024)

	ta = thinkingdata.New(consumer)

	// 设置公共事件属性
	ta.SetSuperProperties(map[string]interface{}{})
}

func TrackTestEvent(accountId, distinctId string) {
	properties := map[string]interface{}{}
	ta.Track(accountId, distinctId, string(TestEvent), properties)
}
