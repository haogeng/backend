package util

import (
	"github.com/stretchr/testify/assert"
	"math/rand"
	"testing"
)

func TestMarshal64(t *testing.T) {
	for i := 0; i < 10000; i++ {
		l := rand.Int31()
		h := rand.Int31()

		m := Marshal64(l, h)
		l2, h2 := Unmarshal64(m)

		assert.Equal(t, l, l2)
		assert.Equal(t, h, h2)
	}
}
