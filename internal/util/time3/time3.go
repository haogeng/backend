package time3

import (
	"bitbucket.org/funplus/golib/time2"
	"errors"
	"server/internal/log"
	"strconv"
	"strings"
	"time"
)

var durationForTest time.Duration = 0

// 配置加载完后修改
// 有效范围： [2m, 1h]
func SetDurationForTest(duration time.Duration) {
	durationForTest = duration
}

var timeNowOffsetForTest time.Duration = 0

// like B: 2020-06-04-16-56-50
func SetTimeNowForTestFromStr(s string) error {
	s = strings.TrimSpace(s)

	strAry := strings.Split(s, "-")
	Len := 6
	if len(strAry) != Len {
		return errors.New("time format err")
	}

	var err error
	intAry := make([]int, Len, Len)
	for i := 0; i < Len; i++ {
		intAry[i], err = strconv.Atoi(strAry[i])
		if err != nil {
			return err
		}
	}
	t := time.Date(intAry[0], time.Month(intAry[1]), intAry[2], intAry[3], intAry[4], intAry[5], 0, time.Local)
	SetTimeNowForTest(t)
	return nil
}

func SetTimeNowForTest(t time.Time) {
	timeNowOffsetForTest = t.Sub(time.Now())
	log.Infof("Debug setTimeNow from %v to %v, diffSeconds: %v", time.Now(), t, timeNowOffsetForTest/time.Second)
	log.Infof("Nowtime is %v", Now())
}

// 支持时间测试
func GetSecondsOfOneDay() int64 {
	if isUsingTestDuration() {
		return int64(durationForTest / time.Second)
	} else {
		return 24 * 3600
	}
}

func startOfDurationForTest(tt time.Time) time.Time {
	if durationForTest == time.Hour {
		return time2.EndOfHour(tt).Add(-time.Hour)
	} else {
		return tt.Truncate(durationForTest)
	}
}

func endOfDurationForTest(tt time.Time) time.Time {
	if durationForTest == time.Hour {
		return time2.EndOfHour(tt)
	} else {
		return tt.Truncate(durationForTest).Add(durationForTest)
	}
}

func isUsingTestDuration() bool {
	return durationForTest >= time.Minute*2 &&
		durationForTest <= time.Hour
}

// EndOfDay end of day
func EndOfDay(tt time.Time) time.Time {
	if isUsingTestDuration() {
		return endOfDurationForTest(tt)
	} else {
		return time2.EndOfDay(tt)
	}
}

func StartOfDay(tt time.Time) time.Time {
	if isUsingTestDuration() {
		return startOfDurationForTest(tt)
	} else {
		return time2.BeginningOfDay(tt)
	}
}

// EndOfWeek end of week
func EndOfWeek(tt time.Time) time.Time {
	if isUsingTestDuration() {
		return endOfDurationForTest(tt)
	} else {
		return time2.EndOfWeek(tt)
	}
}

// 业务逻辑需要调用time3.Now
// 方便策划测试
func Now() time.Time {
	if timeNowOffsetForTest != 0 {
		return time.Now().Add(timeNowOffsetForTest)
	} else {
		return time.Now()
	}
}

// 获取指定时间N小时之后的时间
func TimeOfHoursLater(tt time.Time, hourSpec int) time.Time {
	y, m, d := tt.Date()
	return time.Date(y, m, d, tt.Hour()+hourSpec, tt.Minute(), tt.Second(), tt.Nanosecond(), tt.Location())
}

//获取第多少天
func GetDayNum() (day int64) {
	curTime := time.Now().Unix()
	//从1开始没有0
	day = curTime/(24*3600) + 1
	return
}

// 格式化年月日
func TimeFormatYMD(tt time.Time) string {
	return tt.Format("20200817")
}

// 格式化年月日时分秒
func TimeFormatYMDHMS(tt time.Time) string {
	return tt.Format("20200817110115")
}

// 是否跨天了
// 按服务器所在的local自然日
func IsCrossDay(start, end int64) bool {
	if end < start {
		return false
	}

	tt1 := time.Unix(start, 0)
	tt2 := time.Unix(end, 0)

	tt1_0 := time2.EndOfDay(tt1)
	tt2_0 := time2.EndOfDay(tt2)

	if tt1_0.Before(tt2_0) {
		return true
	}
	return false
}

// 得到跨的天数
// 按服务器所在的local自然日
// -1 start > end; 0 当天； 1 跨了1天了
func GetCrossDays(start, end int64) int32 {
	if end < start {
		return -1
	}

	tt1 := time.Unix(start, 0)
	tt2 := time.Unix(end, 0)

	tt1_0 := time2.EndOfDay(tt1)
	tt2_0 := time2.EndOfDay(tt2)

	return int32((tt2_0.Unix() - tt1_0.Unix()) / (24 * 3600))
}

// by seconds
func ZoneOffset() int32 {
	_, offset := time.Now().Local().Zone()
	return int32(offset)
}

// like B: 2020-06-04-16-56-50
func GetTimeFromStr(s string) (err error, startTime int64) {
	layout := "2006-01-02 15:04:05"
	sTime, err := time.ParseInLocation(layout, s, time.Local)
	if err != nil {
		return err, 0
	}
	startTime = sTime.Unix()
	return err, startTime
}
