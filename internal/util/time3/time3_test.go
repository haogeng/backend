package time3

import (
	"fmt"
	"testing"
	"time"
)
import (
	"github.com/stretchr/testify/assert"
)

func TestCrossDay(t *testing.T) {
	t1 := time.Date(2020, 6, 4, 16, 56, 50, 0, time.Local).Unix()
	t2 := time.Date(2020, 6, 5, 1, 56, 50, 0, time.Local).Unix()
	assert.Equal(t, true, IsCrossDay(t1, t2))
	assert.Equal(t, int32(1), GetCrossDays(t1, t2))

	t1 = time.Date(2020, 6, 1, 0, 56, 50, 0, time.Local).Unix()
	t2 = time.Date(2020, 6, 4, 23, 56, 50, 0, time.Local).Unix()
	assert.Equal(t, true, IsCrossDay(t1, t2))
	assert.Equal(t, int32(3), GetCrossDays(t1, t2))

	t1 = time.Date(2020, 6, 4, 0, 56, 50, 0, time.Local).Unix()
	t2 = time.Date(2020, 6, 4, 23, 56, 50, 0, time.Local).Unix()
	assert.Equal(t, false, IsCrossDay(t1, t2))
	assert.Equal(t, int32(0), GetCrossDays(t1, t2))

	t1 = time.Date(2020, 6, 7, 0, 56, 50, 0, time.Local).Unix()
	t2 = time.Date(2020, 6, 4, 23, 56, 50, 0, time.Local).Unix()
	assert.Equal(t, false, IsCrossDay(t1, t2))
	assert.Equal(t, int32(-1), GetCrossDays(t1, t2))

	//assert.Equal(t, int32(8*3600), ZoneOffset())
}

func TestTime3(t *testing.T) {
	// Both is ok
	//str := "2020-06-04-16-56-50"
	str := "2020-06-4-16-56-50"
	err := SetTimeNowForTestFromStr(str)
	assert.Equal(t, nil, err, "")

	t3Now := Now()
	expectedT3Now := time.Date(2020, 6, 4, 16, 56, 50, 0, time.Local)
	diff := t3Now.Sub(expectedT3Now)
	if diff < 0 {
		diff = -diff
	}
	diff /= time.Second
	assert.Equal(t, time.Duration(0), diff, "")

	fmt.Printf("t3Now %v\n", t3Now)

	tNow := time.Now()
	assert.NotEqual(t, t3Now, tNow, "")

	str2 := "2020-08-05-00-03-05"
	err = SetTimeNowForTestFromStr(str2)
	assert.Equal(t, nil, err, "")

	//fmt.Println("newTime: ", Now().Format(time.Stamp))
	assert.Contains(t, Now().Format(time.Stamp), "Aug  5 00:03:05", "")
}
