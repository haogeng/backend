package main

import (
	"bitbucket.org/funplus/golib/xdb"
	"context"
	"database/sql"
	"fmt"
	"github.com/jmoiron/sqlx"
	"go.uber.org/atomic"
	"math/rand"
	"server/internal/game"
	"server/internal/log"
	"server/pkg/gen/msg"
	"server/pkg/gen_db/dao"
	"server/pkg/gen_redis/redisdb"
	"server/pkg/xsnowflake"
	"strings"
	"sync"
	"time"
)

func testMySQLAndRedisBenchmark() {
	go func() {
		//TestMaxConn(db)
		//TestMaxConn2(db)
		// 测试创建用户!!!
		//TestMaxConn3(db)
	}()
}

func TestMaxConn(db2 xdb.DB) {
	db := db2.(*sqlx.DB)
	ctx := context.Background()

	var wg sync.WaitGroup
	Num := 100
	wg.Add(Num)
	var failNum atomic.Int32
	for i := 0; i < Num; i++ {
		go func(n int) {
			defer wg.Done()
			_, err := db.Exec("insert into users (name, age) values ('xxx', 18)")
			if err != nil {
				failNum.Add(1)
				return
			}

			age := 27
			rows, err := db.QueryContext(ctx, "SELECT id, name FROM users WHERE age=?", age)
			if err != nil {
				failNum.Add(1)
				return
			} else {
				log.Infof("---------OK %d", n)
			}

			for rows.Next() {
			}
			_ = rows.Close()
		}(i)
	}
	wg.Wait()

	log.Infof("---------test:%d fail:%d", Num, failNum)
}

func TestMaxConn2(db2 xdb.DB) {
	//db := db2.(*sqlx.DB)
	//ctx := context.Background()

	start := time.Now()
	var wg sync.WaitGroup
	Num := 20000
	wg.Add(Num)
	var failNum atomic.Int32
	for i := 0; i < Num; i++ {
		go func(n int) {
			defer wg.Done()

			id := xsnowflake.NextIdUint64()

			_, err := dao.PlayerInfo.Find(id)
			if err != nil && !strings.Contains(err.Error(), sql.ErrNoRows.Error()) {
				log.Info(err)
			}

			d := &msg.PlayerInfo{Id: id}
			d.Name = fmt.Sprintf("%d", id)
			_id, _err := dao.PlayerInfo.Create(id, d)
			if _id != id || _err != nil {
				failNum.Add(1)
				log.Info(_id, id, _err)
				return
			}

			ret, err := dao.PlayerInfo.Find(id)
			if err != nil {
				failNum.Add(1)
				log.Info(err)
				return
			}

			err = dao.PlayerInfo.Update(id, ret)
			if err != nil {
				failNum.Add(1)
				log.Info(err)
				return
			}
		}(i)
	}
	wg.Wait()

	diff := time.Now().Sub(start)

	log.Infof("---------test2:%d fail:%d cost:%d ms", Num, failNum, diff.Milliseconds())
}

//Benchmark: 每秒可创建千级的用户！ 5K qps mysql IO.
func TestMaxConn3(db2 xdb.DB) {
	//db := db2.(*sqlx.DB)
	//ctx := context.Background()

	start := time.Now()
	var wg sync.WaitGroup
	Num := 10000
	wg.Add(Num)
	var failNum atomic.Int32
	for i := 0; i < Num; i++ {
		go func(n int) {
			defer wg.Done()

			for x := 0; x < 10; x++ {
				r := game.CreateNewPlayer(188, rand.Int31()%100000, 2, "xxx")
				if r == nil {
					failNum.Add(1)
					return
				}
			}
		}(i)
	}
	wg.Wait()

	diff := time.Now().Sub(start)

	log.Infof("---------test3:%d fail:%d cost:%d ms", Num, failNum, diff.Milliseconds())
}

func TestMaxConn4(db2 xdb.DB) {
	//db := db2.(*sqlx.DB)
	//ctx := context.Background()
	defer func() {
		if r := recover(); r != nil {
			log.Info("TestMaxConn4 on panic")
		}
	}()

	start := time.Now()
	var wg sync.WaitGroup
	Num := 1000
	wg.Add(Num)
	var failNum atomic.Int32
	for i := 0; i < Num; i++ {
		go func(n int) {
			defer func() {
				if r := recover(); r != nil {
					failNum.Add(1)
				}
				wg.Done()
			}()

			uid := xsnowflake.NextIdUint64()
			name := fmt.Sprintf("U%d", uid)
			_ = name
			//log.Infof("Start %d", uid)
			//defer log.Infof("End %d", uid)

			var c redisdb.PlayerInfoCache
			_ = c
			var r *msg.PlayerInfo

			//r, err := dao.PlayerInfo.Find(uid)
			//if err != nil && !strings.Contains(err.Error(), sql.ErrNoRows.Error()) {
			//	log.Info(err)
			//}

			r = c.Find(uid)

			//if r != nil {
			//	log.Errorf("repeated player %d", uid)
			//	panic(fmt.Errorf("repeated player %d", uid))
			//	return
			//}

			// TODO 信息配置需要提取出来，策划填
			// Init player
			r = &msg.PlayerInfo{
				Id:       uid,
				ServerId: 888,
				// 默认先罗马文明
				CivilId: 111,
				Name:    name,
				//DeviceNameDup:      name,
				//Level:              1,
				//RenameCount:        0,
				//MaxBuildQueueCount: 1,
				//// 新玩家上来，在第一次登录的时候，会走一次每日重置，需要能兼容
				//DailyResetTime:  0,
				//WeeklyResetTime: 0,
			}
			_ = r

			//r.Currency = make(map[int32]int64)
			//// make sure init
			//r.Currency[int32(msg.CurrencyId_CI_Gold)] = 0
			//
			//// 随机值【0,10) 通过mod在某些情况下做用户标签拆分
			//r.RandomVal = rand.Int31n(10)
			//
			////!!! ORDER!!!
			//r.Buildings = make(map[int32]*msg.BuildingInfo)
			//MakeSureAllBuildingExists(r)
			//
			////!!! 类似map在反序列化的时候会搞空...
			//ExtEquip(r).Equips = make(map[int32]*msg.ObjInfo)

			//r.Items = make(map[int32]*msg.ObjInfo)
			//
			//// init data to avoid map is nil
			//r.Draws = make(map[int32]int32)
			//r.Draws[1] = 0
			//
			//r.Tasks = &msg.TaskInfo{
			//	TaskCounts: make(map[int32]int64),
			//	TaskLevels: make(map[int32]int32),
			//}
			//initTask(r)
			//initBounty(r)
			//initLevyHouse(r)
			//initStore(r)

			// ORDER!
			// 通过这种AddObjIntoMap去添加
			//ExtTroop(r).Troops = make(map[int32]*msg.ObjInfo)
			//initTroop(r, civilId)
			//
			//initItemOrCurrencyFromConf(r, civilId)

			//r.TechInfos = make(map[int32]*msg.TechInfo)
			//c.MustCreate(uid, r)

			//// 补充存档扩展信息
			//if r.Troop != nil {
			//	var c2 redisdb.PlayerInfoTroopCache
			//	c2.MustUpdate(uid, r.Troop)
			//}
			//if r.Equip != nil {
			//	var c2 redisdb.PlayerInfoEquipCache
			//	c2.MustUpdate(uid, r.Equip)
			//}
		}(i)
	}
	wg.Wait()

	diff := time.Now().Sub(start)

	log.Infof("---------test4:%d fail:%d cost:%d ms", Num, failNum, diff.Milliseconds())
}
