package main

import (
	"bitbucket.org/funplus/golib/module"
	"database/sql"
	"fmt"
	"github.com/jmoiron/sqlx"
	"net"
	"runtime"
	"server/internal"
	"server/internal/global_info"
	"server/internal/gmodule"
	log "server/internal/log"
	"server/internal/util/lighthouse"
	"server/internal/util/time3"
	"server/internal/util/xsensitive"
	"server/internal/xrtm"
	"server/pkg/cache"
	"server/pkg/dlock"
	"server/pkg/gen/msg"
	"server/pkg/gen_db"
	"server/pkg/gen_db/dao"
	"server/pkg/gen_redis/redisdb"
	"server/pkg/monitor"
	"server/pkg/persistredis"
	"server/pkg/service_mgr"
	"server/pkg/version"
	"server/pkg/xsnowflake"
)

func main() {
	// 用于性能检测
	//defer profile.Start().Stop()
	//defer profile.Start(profile.GoroutineProfile).Stop()

	defer func() {
		if reason := recover(); reason != nil {
			stack := make([]byte, 2048)
			length := runtime.Stack(stack, true)
			log.Errorf("ERROR: main catch: %+v stack:%s", reason, string(stack[:length]))
		}
		log.Sync()
	}()

	// 注意顺序！！！
	//log.Info("Start read config")
	internal.MustLoadConfig()

	// 文件&etcd监控 -- 依赖于配置
	// hot config  - json 配置
	internal.MustInitWatcher()

	internal.MustInitHotConfig(internal.Config.HotConfigPath, internal.Watcher)

	// 设置测试时间
	if internal.Config.Development {
		time3.SetDurationForTest(internal.Config.DurationUsingForTestDaily)
	}

	xsensitive.MustLoad("./configs/sensitive_dict.txt")

	internal.MustInstallLogger(internal.Config.Development, internal.Config.LogWithServerInfo)

	log.Info(version.Info())

	internal.MustInitPmConf()

	db := gen_db.MustConnectWithConf(internal.Config.DbConfig)
	log.Info("Db connected ok")
	sdb, ok := db.(*sqlx.DB)
	if ok {
		log.Infof("Dbstats: %+v", sdb.Stats())
	}

	lighthouse.MustInit(internal.Config.LightHousePath, internal.Watcher)

	log.Info(" lighthouse init ok")

	cache.MustInit(&internal.Config.RedisConfig)
	log.Info("cache Redis connected ok")

	dlock.Init(cache.Redis)

	persistredis.MustInit(&internal.Config.PersistRedisConfig)
	persistredis.DebugFunc = log.Debugf
	log.Info("persist Redis connected ok")

	redisdb.MustInit(internal.Config.DbCacheTtl, internal.Config.DbVer())

	xrtm.MustInit(xrtm.GmxGlobalTestConf)

	//xrtm.MustInit(xrtm.GmxGlobalTestConf)
	monitor.MustInit(db)

	//! nodeId 0 试用default ip lower14
	newNode := xsnowflake.MustInit(0, CheckNodeConflict)

	log.Infof("XSnowFlake Get Node ok %+v\n", newNode)

	// 全局单件数据表
	_ = global_info.GetGlobalInfo()

	//log.Info("Start load module")
	// service register
	if internal.Config.Development && len(internal.Config.EtcdEndPointsForServerList) > 0 {
		_, err := service_mgr.Init(internal.Config.EtcdEndPointsForServerList)
		if err != nil {
			log.Errorf("service_mgr init err %v\n", err)
		}
		defer service_mgr.Close()

		k, v := internal.GetServiceInfo()
		err = service_mgr.Login(k, v)
		if err != nil {
			log.Error(err)
		}
		log.Infof("registed %s:%s", k, v)
		defer service_mgr.Logout(k)
	}
	// 加载竞技场
	// game.StartServerCheckArenaInfos()

	if internal.Config.Development {
		//testMySQLAndRedisBenchmark()
	}

	log.Debug("Start run gmodule..")
	//module.Run(&gmodule.Tcp{}, &gmodule.Http{})
	module.Run(gmodule.GetSingletonSvc(), &gmodule.Http{})
	// hangup here!

	// 等待退出，确保所有的db都存档完成 (基于：handler里db是同步存档的)
	//ctx, cancel := context.WithTimeout(context.Background(), time.Second*120)
	//defer cancel()
	//WaitHandler(ctx)
}

func CheckNodeConflict(nodeId uint16, ipId uint32, ip net.IP) error {
	nodeMgr := dao.NodeInfo
	nodeInfo, err := nodeMgr.Find((uint64(ipId)))
	if err != sql.ErrNoRows && err != nil {
		panic(fmt.Errorf("snowflake ip conflict: ipId %v,%v\n", ipId, err))
	}

	// reset err
	err = nil

	if nodeInfo == nil {
		nodeInfo := &msg.NodeInfo{
			Id:      uint64(ipId),
			LocalIp: ip.String(),
			NodeId:  uint32(nodeId),
		}
		_, err = nodeMgr.Create(nodeInfo.Id, nodeInfo)
		if err != nil {
			panic(fmt.Errorf("nodeMgr.MustCreate %v\n", err))
		}
	}
	return err
}
