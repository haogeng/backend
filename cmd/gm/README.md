# local_gm_x
> 程序化的gm工具
> 用于方便的查看远程玩家的信息，并存储到json文件

## command list
    
    playerinfo
    
    playerlogin
    
    repeatlogin
    
    sendmail
    
    maincopy

## example
 
`./local_gm_darwin -c playerinfo -s dev3 -i 61198507911190582
`

## TODO
-n 的支持

## post
`subl gmPlayerInfo.json` 

## 帮助
./local_gm_darwin -h