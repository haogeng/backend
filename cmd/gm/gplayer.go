package main

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"server/pkg/gen/msg"
	"server/pkg/gpclient"
)

func gmPlayerInfo() {
	seqNum := 1
	req := &msg.GmPlayerInfoR{
		SecretToken: "gmx-secret-uxike78J!#~88oL",
		TargetId:    InputParams.playerId,
		PlayerName:  InputParams.playerName,
	}
	res := gpclient.ReqProtoWithHeader(GetGmUrl(InputServerName), req, seqNum, map[string]string{"x-skipper-whiteip": "1"})

	if len(res) > 0 {
		if v, ok := res[0].(*msg.GmPlayerInfoA); ok {
			jsonValue, _ := json.MarshalIndent(v, "", "\t")
			ioutil.WriteFile("gmPlayerInfo.json", []byte(jsonValue), os.FileMode(0644))
		}
	}
}
