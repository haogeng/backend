package main

import (
	"server/pkg/gen/msg"
	"server/pkg/gpclient"
)

func gmSendMail() {
	seqNum := 1

	req := &msg.GmSendMailR{
		SecretToken: "gmx-secret-uxike78J!#~88oL",
		TargetId:    InputParams.playerId,
		ConfigId:    int32(InputParams.ConfigId),
	}
	gpclient.ReqProtoWithHeader(GetGmUrl(InputServerName), req, seqNum, map[string]string{"x-skipper-whiteip": "1"})
}
