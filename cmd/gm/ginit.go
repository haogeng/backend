package main

func gmInitTable() map[string]func() {
	return map[string]func(){
		"playerinfo":    gmPlayerInfo,
		"sendmail":      gmSendMail,
		"maincopy":      gmModifyMainCopy,
		"arenauserinfo": gmModifyArenaUserInfo,
		"warcopy":       gmModifyWarCopy,
	}
}

func gmInitFlag() map[string]func() {
	return map[string]func(){
		"playerinfo": gmPlayerInfoFlag,
		"sendmail":   gmSendMailFlag,
		"maincopy":   gmModifyMainCopyFlag,
	}
}
