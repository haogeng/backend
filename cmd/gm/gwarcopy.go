package main

import (
	"server/pkg/gen/msg"
	"server/pkg/gpclient"
)

func gmModifyWarCopy() {
	seqNum := 1
	req := &msg.GmModifyWarCopyR{
		SecretToken:  "gmx-secret-uxike78J!#~88oL",
		TargetId:     InputParams.playerId,
		PlayerName:   InputParams.playerName,
		WarId:        InputParams.warId,
		ChapterId:    InputParams.chapterId,
		StrongholdId: InputParams.strongholdId,
	}
	gpclient.ReqProtoWithHeader(GetGmUrl(InputServerName), req, seqNum, map[string]string{"x-skipper-whiteip": "1"})
	seqNum++
}
