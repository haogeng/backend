package main

import (
	"flag"
	"fmt"
	"os"
	"reflect"
)

// 专门设计一个流程，用于无障碍测试协议流通！！！
type GmClientParams struct {
	ConfigId     int32  // 邮件ID
	warId        int32  // 难度ID
	chapterId    int32  // 章节ID
	strongholdId int32  // 据点ID
	playerId     uint64 // 用户ID
	playerName   string // 用户昵称
	deviceName   string // 设备名字
}

var (
	// addr/api/microServerName
	BaseUrls = map[string]string{
		"qa2":     "http://10.1.8.113:28082/api/",
		"dev3":    "http://10.0.84.38:28083/api/",
		"yy4":     "http://10.0.84.38:28084/api/",
		"root9":   "http://10.0.84.38:28089/api/",
		"yylocal": "http://127.0.0.1:28084/api/",
		"geng5":   "http://10.1.8.188:28085/api/",
		"lnj6":    "http://10.1.9.34:28086/api/",
		"gmxtest": "http://54.254.192.201/api/",
		"pro":     "http://makerx-test.centurygame.com/api/",
	}
	InputServerName = "lnj6"
	InputParams     GmClientParams
)

var (
	h  bool   // 启动帮助
	c  string // 指令名称
	s  string // 服务器版本
	d  string // DeviceName
	n  string // PlayerName
	i  int64  // PlayerId
	mi int    // 邮件模版ID
	wi int    // 难度ID
	ci int    // 章节ID
	si int    // 据点ID
)

func GetLoginUrl(serverName string) string {
	return BaseUrls[serverName] + "login"
}

func GetGameUrl(serverName string) string {
	return BaseUrls[serverName] + "game"
}

func GetGmUrl(serverName string) string {
	return BaseUrls[serverName] + "gm"
}

func main() {

	// 命令函数
	t := gmInitTable()
	// 帮助函数
	f := gmInitFlag()

	flag.Parse()
	// 触发一级帮助
	if h && c == "" {
		flag.Usage()
		return
	} else {
		params := os.Args[1:]
		if len(params) == 0 {
			// 触发一级帮助
			flag.Usage()
			return
		}
	}
	// 触发二级帮助
	if h && c != "" {
		f[c]()
		return
	}

	funcName := c
	InputServerName = s

	InputParams = GmClientParams{
		playerId:     uint64(i),
		playerName:   n,
		deviceName:   d,
		chapterId:    int32(ci),
		strongholdId: int32(si),
		ConfigId:     int32(mi),
	}

	url, ok := BaseUrls[InputServerName]
	if !ok {
		panic(fmt.Sprintf("url err: %v", url))
	}
	fmt.Printf("InputServerName: %s\nUrl: %s\n\n", InputServerName, url)

	if f, ok := t[funcName]; ok {
		f()
	} else {
		fmt.Printf("Pls input right func name. Ref: \n%+v\n", t)
	}
}

func init() {
	flag.BoolVar(&h, "h", false, "一级命令帮助描述文档")
	cmdArr := reflect.ValueOf(gmInitTable()).MapKeys()
	flag.StringVar(&c, "c", "", "Command必选项包括"+fmt.Sprint(cmdArr)+",帮助命令:Command --help")
	infoArr := reflect.ValueOf(BaseUrls).MapKeys()
	flag.StringVar(&s, "s", infoArr[0].String(), "Server服务器版本选项"+fmt.Sprint(infoArr))
	flag.StringVar(&d, "d", "", "DeviceName可选参数,根据设备名称查询信息")
	flag.StringVar(&n, "n", "", "PLayerName可选参数,根据用户昵称查询信息")
	flag.Int64Var(&i, "i", 0, "PlayerId可选参数,根据用户ID查询信息")
	flag.IntVar(&mi, "mi", 0, "ConfigId邮件的模版配置ID")
	flag.IntVar(&wi, "wi", 0, "warId战役难度ID")
	flag.IntVar(&ci, "ci", 0, "chapterId副本章节ID")
	flag.IntVar(&si, "si", 0, "strongholdId章节据点ID")
	flag.Usage = usage
}

func usage() {
	fmt.Fprintf(os.Stderr, "version: %s/0.0.2 \nUsage: %s [-c XXX] [-s XXX] [-n XXX] \nOptions:\n"+
		"", os.Args[0], os.Args[0])
	flag.PrintDefaults()
}

// 显示查询用户信息命令的帮助信息
func gmPlayerInfoFlag() {
	fmt.Printf("Usage: %s -c playerinfo COMMAND\n", os.Args[0])
	fmt.Println("命令执行必须选择以下命令中的一个")
	fmt.Println("Commands:")
	fmt.Println("  [-n XXX]   PLayerName可选参数,根据用户昵称查询信息")
	fmt.Println("  [-i XXX]   PlayerId可选参数,根据用户ID查询信息")
}

// 显示发送邮件命令的帮助信息
func gmSendMailFlag() {
	fmt.Printf("Usage: %s -c sendmail COMMAND\n", os.Args[0])
	fmt.Println("命令执行必须包含以下所有命令")
	fmt.Println("Commands:")
	fmt.Println("  [-n XXX]   PLayerName可选参数,根据用户昵称查询信息")
	fmt.Println("  [-i XXX]   PlayerId可选参数,根据用户ID查询信息")
	fmt.Println("  [-mi XXX]  ConfigId邮件的模版配置ID")
}

// 显示修改主线副本命令的帮助信息
func gmModifyMainCopyFlag() {
	fmt.Printf("Usage: %s -c maincopy COMMAND\n", os.Args[0])
	fmt.Println("命令执行必须包含以下所有命令")
	fmt.Println("Commands:")
	fmt.Println("  [-n XXX]   PLayerName可选参数,根据用户昵称查询信息")
	fmt.Println("  [-i XXX]   PlayerId可选参数,根据用户ID查询信息")
	fmt.Println("  [-ci XXX]   chapterId副本章节ID")
	fmt.Println("  [-si XXX]  strongholdId章节据点ID")
}
