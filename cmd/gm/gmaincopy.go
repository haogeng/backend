package main

import (
	"server/pkg/gen/msg"
	"server/pkg/gpclient"
)

func gmModifyMainCopy() {
	seqNum := 1
	req := &msg.GmModifyMainCopyR{
		SecretToken:  "gmx-secret-uxike78J!#~88oL",
		TargetId:     InputParams.playerId,
		PlayerName:   InputParams.playerName,
		ChapterId:    InputParams.chapterId,
		StrongholdId: InputParams.strongholdId,
	}
	gpclient.ReqProtoWithHeader(GetGmUrl(InputServerName), req, seqNum, map[string]string{"x-skipper-whiteip": "1"})
	seqNum++
}
