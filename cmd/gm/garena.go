package main

import (
	"server/pkg/gen/msg"
	"server/pkg/gpclient"
)

func gmModifyArenaUserInfo() {
	seqNum := 1
	req := &msg.GmModifyArenaUserInfoR{
		SecretToken: "gmx-secret-uxike78J!#~88oL",
		PlayerId:    InputParams.playerId,
		AddScore:    4000,
	}
	gpclient.ReqProtoWithHeader(GetGmUrl(InputServerName), req, seqNum, map[string]string{"x-skipper-whiteip": "1"})
	seqNum++
}
