package main

import (
	"fmt"
	"os"
	"reflect"
)

// 专门设计一个流程，用于无障碍测试协议流通！！！

var (
	// addr/api/microServerName
	BaseUrls = map[string]string{
		"dev3":     "http://10.0.84.38:28083/api/",
		"yy4":      "http://10.0.84.38:28084/api/",
		"root9":    "http://10.0.84.38:28089/api/",
		"yylocal":  "http://127.0.0.1:28084/api/",
		"gh5local": "http://10.1.8.188:28085/api/",
		"lnj6":     "http://10.1.9.34:28086/api/",
		"gmxtest":  "http://54.254.192.201/api/",
	}

	InputServerName = "lnj6local"
	InputDeviceId   = "dnf"
)

func GetLoginUrl(serverName string) string {
	return BaseUrls[serverName] + "login"
}

func GetGameUrl(serverName string) string {
	return BaseUrls[serverName] + "game"
}

func GetGmUrl(serverName string) string {
	return BaseUrls[serverName] + "gm"
}

func GetClanUrl(serverName string) string {
	return BaseUrls[serverName] + "clan"
}

func main() {
	// 如果不传，服务器则忽略
	//gpclient.AppVersion = "1.1.14399827"
	//gpclient.Platform = "android"

	fmt.Printf("\n=========================================")
	t := initTable()

	fmt.Printf("\nUsage: exec funcName serverName deviceId\n")
	fmt.Printf("default server is %s\n", InputServerName)
	fmt.Printf("default deviceId is %s\n\n", InputDeviceId)

	fmt.Printf("=========================================\n\n")

	argsN := os.Args[1:]

	if len(argsN) < 1 {
		fmt.Printf("=-----------Help------------: \nfuncName: +%v\nserverName: +%v\n\n",
			reflect.ValueOf(t).MapKeys(),
			reflect.ValueOf(BaseUrls).MapKeys())
		return
	}

	funcName := argsN[0]
	if len(argsN) > 1 {
		InputServerName = argsN[1]
	}

	if len(argsN) > 2 {
		InputDeviceId = argsN[2]
	}

	url, ok := BaseUrls[InputServerName]
	if !ok {
		panic(fmt.Sprintf("url err: %v", url))
	}
	fmt.Printf("InputServerName: %s\nUrl: %s\n\n", InputServerName, url)

	if f, ok := t[funcName]; ok {
		f()
	} else {
		fmt.Printf("Pls input right func name. Ref: \n%+v\n", t)
	}
}
