package main

func initTable() map[string]func() {
	return map[string]func(){
		"testPlayerLogin":  func() { testPlayerLogin() },
		"testPlayerLogin2": func() { testPlayerLogin2() },

		"testRepeatLogin": testRepeatLogin,

		"testServerList":      testServerList,
		"testBuildingUnlock":  testBuildingUnlock,
		"testBuildingUpgrade": testBuildingUpgrade,

		"testDebugAddObj": testDebugAddObj,

		"testSellItem": testSellItem,
		"testUseItem":  testUseItem,

		"testLevelUpTroop":      testLevelUpTroop,
		"testStarUpTroop":       testStarUpTroop,
		"testLevelUpTalent":     testLevelUpTalent,
		"testLevelUpSpecial":    testLevelUpSpecial,
		"testTroopRelationship": testTroopRelationship,

		"testDraw": testDraw,

		"testTask":         testTask,
		"testOnHookIncome": testOnHookIncome,
		"testQuickOnHook":  testQuickOnHook,

		"testBounty":              testBounty,
		"testManualRefreshBounty": testManualRefreshBounty,

		"testRespCache": testRespCache,
		"testSeqCheck":  testSeqCheck,

		"testUpgradeTech":        testUpgradeTech,
		"testInstantUpgradeTech": testInstantUpgradeTech,
		"testReqCount":           testReqCount,

		"testStore": testStore,

		"testResetTroop":  testResetTroop,
		"testLayOffTroop": testLayOffTroop,

		"testLevelUpEquip": testLevelUpEquip,

		"testGmSendMail":        testGmSendMail,
		"testMailInfo":          testMailInfo,
		"testRecvMailReward":    testRecvMailReward,
		"testReadMail":          testReadMail,
		"testDelMail":           testDelMail,
		"testDelMailAll":        testDelMailAll,
		"testRecvMailRewardAll": testRecvMailRewardAll,

		"testDebugSetServerTempTime": testDebugSetServerTempTime,

		"testGmPlayerInfo": testGmPlayerInfo,

		"testMainCopyInfos":      testMainCopyInfos,
		"testStrongholdInfos":    testStrongholdInfos,
		"testQuickStartMainCopy": testQuickStartMainCopy,
		"testStrongholdReward":   testStrongholdReward,

		"testArenaInfos":       testArenaInfos,
		"testArenaMainInfo":    testArenaMainInfo,
		"testArenaMatchInfo":   testArenaMatchInfo,
		"testArenaStartBattle": testArenaStartBattle,
		"testArenaEndBattle":   testArenaEndBattle,
		"testArenaBattleLogs":  testArenaBattleLogs,

		"testGetClanList":         testGetClanList,
		"testGetClanInfo":         testGetClanInfo,
		"testClanMemberList":      testClanMemberList,
		"testApplyJoinClan":       testApplyJoinClan,
		"testAgreeJoinClan":       testAgreeJoinClan,
		"testModifyClanInfo":      testModifyClanInfo,
		"testApplyFoundClan":      testApplyFoundClan,
		"testAppointPosition":     testAppointPosition,
		"testExpelClanMember":     testExpelClanMember,
		"testSearchClanInfo":      testSearchClanInfo,
		"testSearchClanInfo2":     testSearchClanInfo2,
		"testQuitClan":            testQuitClan,
		"testGetClanApplyInfo":    testGetClanApplyInfo,
		"testGetClanLog":          testGetClanLog,
		"testClanDonate":          testClanDonate,
		"testSendClanMail":        testSendClanMail,
		"testGetClanBoss":         testGetClanBoss,
		"testClanBossStartBattle": testClanBossStartBattle,
		"testClanBossEndBattle":   testClanBossEndBattle,

		"testWarDiffInfos":    testWarDiffInfos,
		"testWarBoxReward":    testWarBoxReward,
		"testWarBeginBattle":  testWarBeginBattle,
		"testWarEndBattle":    testWarEndBattle,
		"testGmModifyWarCopy": testGmModifyWarCopy,

		"testGetFriendInfos":      testGetFriendInfos,
		"testGetApplyFriendInfos": testGetApplyFriendInfos,
		"testGetBlackFriendInfos": testGetBlackFriendInfos,
		"testRecommendFriends":    testRecommendFriends,
		"testSearchPlayerInfo":    testSearchPlayerInfo,
		"testSendApplyFriend":     testSendApplyFriend,
		"testJoinApplyFriend":     testJoinApplyFriend,
		"testDeleteApplyFriend":   testDeleteApplyFriend,
		"testAddBlackFriend":      testAddBlackFriend,
		"testRemoveBlackFriend":   testRemoveBlackFriend,
		"testDeleteFriend":        testDeleteFriend,
		"testQuickSendAndGet":     testQuickSendAndGet,

		"testGetMazeInfo":       testGetMazeInfo,
		"testStartMazeEvent":    testStartMazeEvent,
		"testUseMazeRelic":      testUseMazeRelic,
		"testMazeRecruit":       testMazeRecruit,
		"testTravelSellerReset": testTravelSellerReset,
		"testGetMazeRelic":      testGetMazeRelic,
		"testClearLayer":        testClearLayer,
		"testMazeMode":          testMazeMode,
		"testEventRefresh":      testEventRefresh,
		"testMazeStartBattle":   testMazeStartBattle,
		"testMazeEndBattle":     testMazeEndBattle,
		"testResurrectPotion":   testResurrectPotion,

		"testPlayerLogout": testPlayerLogout,
	}
}
