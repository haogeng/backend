package main

import (
	"server/pkg/gen/msg"
	"server/pkg/gpclient"
)

func testGetMazeInfo() {
	token, _ := testPlayerLogin()

	seqNum := 1
	//lock 2
	{
		req := &msg.GetMazeInfoR{}
		gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
		seqNum++
	}
}

func testStartMazeEvent() {
	token, _ := testPlayerLogin()

	seqNum := 1
	//lock 2
	{
		req := &msg.StartMazeEventR{
			LevelId: 1,
		}
		gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
		seqNum++
	}
}

func testUseMazeRelic() {
	token, _ := testPlayerLogin()

	seqNum := 1
	//lock 2
	{
		req := &msg.RecvMazeRelicR{
			LevelId: 1,
			RelicId: 1,
		}
		gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
		seqNum++
	}
}

func testMazeRecruit() {
	token, _ := testPlayerLogin()

	seqNum := 1
	//lock 2
	{
		req := &msg.MazeRecruitR{
			LevelId: 1,
			TroopId: 1,
		}
		gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
		seqNum++
	}
}

func testTravelSellerReset() {
	token, _ := testPlayerLogin()

	seqNum := 1
	//lock 2
	{
		req := &msg.TravelSellerResetR{
			LevelId: 1,
		}
		gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
		seqNum++
	}
}

func testGetMazeRelic() {
	token, _ := testPlayerLogin()

	seqNum := 1
	//lock 2
	{
		req := &msg.GetMazeRelicR{}
		gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
		seqNum++
	}
}

func testClearLayer() {
	token, _ := testPlayerLogin()

	seqNum := 1
	//lock 2
	{
		req := &msg.ClearLayerR{
			LayerId: 1,
		}
		gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
		seqNum++
	}
}

func testMazeMode() {
	token, _ := testPlayerLogin()

	seqNum := 1
	//lock 2
	{
		req := &msg.MazeModeR{
			ModeType: 1,
		}
		gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
		seqNum++
	}
}

func testEventRefresh() {
	token, _ := testPlayerLogin()

	seqNum := 1
	//lock 2
	{
		req := &msg.EventRefreshR{}
		gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
		seqNum++
	}
}

func testMazeStartBattle() {
	token, _ := testPlayerLogin()

	seqNum := 1
	req := &msg.ReqStartBattleR{
		Type:        msg.BattleType_EBT_Maze,
		MazeLevelId: 5,
	}

	gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
}

func testMazeEndBattle() {
	token, _ := testPlayerLogin()

	seqNum := 1
	req := &msg.ReqEndBattleR{
		Type: msg.BattleType_EBT_Maze,
		MazeBattle: &msg.MazeBattle{
			LevelId:       3,
			TroopReduceHp: make(map[int32]int32),
		},
	}

	gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
}

func testResurrectPotion() {
	token, _ := testPlayerLogin()

	seqNum := 1
	req := &msg.ResurrectPotionR{
		RestoreHp:     make(map[int32]int32),
		RestoreEnergy: make(map[int32]int32),
	}
	req.RestoreHp[1] = 1000
	req.RestoreEnergy[1] = 1000

	gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
}
