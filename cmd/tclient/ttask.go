package main

import (
	"server/pkg/gen/msg"
	"server/pkg/gpclient"
)

func testTask() {
	token, _ := testPlayerLogin()

	// 约定： 0,1,200,0,0,0
	// type,id,count,param,level,star

	seqNum := 1
	//lock 2
	// add 声望
	{
		req := &msg.DebugAddObjR{
			Info: "0,14,20000,0,0,0",
		}
		gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
		seqNum++
	}

	// hero troop
	{
		req := &msg.DebugAddObjR{
			Info: "2,601,3,0,2,4",
		}
		gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
		seqNum++
	}

	// drop
	{
		req := &msg.DebugAddObjR{
			Info: "4,1,3,0,0,0",
		}
		gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
		seqNum++
	}

	// 领取任务奖励...
	taskId := int32(1)
	{
		req := &msg.GetTaskRewardR{
			Id: taskId,
		}
		gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
		seqNum++
	}
}
