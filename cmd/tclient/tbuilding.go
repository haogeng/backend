package main

import (
	"server/pkg/gen/msg"
	"server/pkg/gpclient"
)

func testBuildingUnlock() {
	token, _ := testPlayerLogin()

	buildingId := int32(1)
	seqNum := 1
	//lock 2
	{
		req := &msg.StartUnlockBuildingR{
			Id: buildingId,
		}
		gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
		seqNum++
	}

	{
		req := &msg.EndUnlockBuildingR{
			Id: buildingId,
		}
		gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
		seqNum++
	}
}

func testBuildingUpgrade() {
	token, _ := testPlayerLogin()

	buildingId := int32(1)
	seqNum := 1
	//lock 2
	{
		req := &msg.StartUpgradeBuildingR{
			Id: buildingId,
		}
		gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
		seqNum++
	}

	{
		req := &msg.EndUpgradeBuildingR{
			Id: buildingId,
		}
		gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
		seqNum++
	}
}
