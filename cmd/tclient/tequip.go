package main

import (
	_ "fmt"
	"server/pkg/gen/msg"
	"server/pkg/gpclient"
)

func testLevelUpEquip() {
	token, playerLoginA := testPlayerLogin()

	_ = playerLoginA

	seqNum := 1
	// Add equip.
	{
		req := &msg.DebugAddObjR{
			// type,id,count,param,level,star
			Info: "3,10112,1,0,0,0|3,10122,1,0,0,0|3,10132,1,0,0,0|3,10142,1,0,0,0|3,10152,1,0,0,0|3,10162,1,0,0,0|",
		}
		gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
		seqNum++
	}

	// Add money
	{
		req := &msg.DebugAddObjR{
			// type,id,count,param,level,star
			Info: "0,1,100000,0,0,0",
		}
		gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
		seqNum++
	}

	//{
	//	req := &msg.LevelUpEquipR{
	//		Id:               19,
	//		ConsumedEquipIds: []int32{22},
	//	}
	//
	//	log.Debug("Target: ", playerLoginA.PlayerInfo.Equips[req.Id])
	//	log.Debug("Consume: ")
	//	for i := 0; i < len(req.ConsumedEquipIds); i++ {
	//		log.Debug("consume: ", playerLoginA.PlayerInfo.Equips[req.ConsumedEquipIds[i]])
	//	}
	//
	//	gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
	//	seqNum++
	//}

	{
		req := &msg.ChangeEquipR{
			TroopId:    1,
			ChangeInfo: map[int32]int32{1: 19, 2: 34, 3: 35, 4: 36, 5: 37, 6: 38},
		}
		gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
		seqNum++
	}

	{
		req := &msg.ChangeEquipR{
			TroopId:    1,
			ChangeInfo: map[int32]int32{1: 33},
		}
		gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
		seqNum++
	}

	// Lock & unlock
	{
		req := &msg.LockEquipR{
			Id:         19,
			LockStatus: 1,
		}
		gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
		seqNum++
	}
	{
		req := &msg.LockEquipR{
			Id:         19,
			LockStatus: 0,
		}
		gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
		seqNum++
	}
}
