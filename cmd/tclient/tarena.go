package main

import (
	"server/pkg/gen/msg"
	"server/pkg/gpclient"
)

func testArenaInfos() {
	token, _ := testPlayerLogin()

	seqNum := 1

	req := &msg.ArenaInfosR{}

	gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
}

func testArenaMainInfo() {
	token, _ := testPlayerLogin()

	seqNum := 1

	req := &msg.ArenaInfoR{
		SeasonId: 1,
	}

	gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
}

func testArenaMatchInfo() {
	token, _ := testPlayerLogin()

	seqNum := 1

	req := &msg.ArenaMatchInfoR{
		SeasonId: 1,
	}

	gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
}

func testArenaStartBattle() {
	token, _ := testPlayerLogin()

	seqNum := 1

	req := &msg.ReqStartBattleR{
		Type: msg.BattleType_EBT_Arena,
		Form: &msg.Formation{
			Type:     msg.FormationType_FT_ArenaDef,
			TroopIds: []int32{0, 0, 0, 0, 0, 0, 0, 0, 0},
		},
		BattleId: 29,
		SeasonId: 1,
	}

	gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
}

func testArenaEndBattle() {
	token, _ := testPlayerLogin()

	seqNum := 1

	req := &msg.ReqEndBattleR{
		Type:     msg.BattleType_EBT_Arena,
		BattleId: 29,
		SeasonId: 1,
		Result:   msg.BattleResult_BR_Success,
	}

	gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
}

func testArenaBattleLogs() {
	token, _ := testPlayerLogin()

	seqNum := 1

	req := &msg.ArenaBattleLogInfosR{
		SeasonId: 1,
	}

	gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
}
