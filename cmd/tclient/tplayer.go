package main

import (
	"fmt"
	"server/pkg/gen/msg"
	"server/pkg/gpclient"
	"sync"
)

func testPlayerLogin() (string, *msg.PlayerLoginA) {
	token := testLogin()
	req := &msg.PlayerLoginR{}

	res := gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, 0, map[string]string{"x-fun-user-token": token})
	if len(res) > 0 {
		if pr, ok := res[0].(*msg.PlayerLoginA); ok {
			fmt.Printf("len.CTroops):%d\n", len(pr.PlayerInfo.CTroops))
		}
	}
	return token, res[0].(*msg.PlayerLoginA)
}

func testPlayerLogin2() (token string, a *msg.PlayerLoginA) {
	// 适当的看看并发问题，，登录有并发风险
	var wg sync.WaitGroup
	ConcurrencyCount := 1
	wg.Add(ConcurrencyCount)
	for i := 0; i < ConcurrencyCount; i++ {
		go func(ti int) {
			defer wg.Done()
			tempToken, tempA := doTestPlayerLogin2()
			if i == 0 {
				token, a = tempToken, tempA
			}
		}(i)
	}
	wg.Wait()
	return
}

func doTestPlayerLogin2() (string, *msg.PlayerLoginA) {
	// two step create player
	loginA := testLogin2()
	if loginA == nil {
		fmt.Println("err")
		return "", nil
	}

	accessToken := loginA.Token

	// two step create player
	if loginA.Uid == 0 {
		req := &msg.TwoStepCreatePlayerR{
			AccountId:            loginA.AccountId,
			CivilId:              1,
			DeviceId:             loginA.DeviceId,
			XXX_NoUnkeyedLiteral: struct{}{},
			XXX_unrecognized:     nil,
			XXX_sizecache:        0,
		}

		res := gpclient.ReqProtoWithHeader(GetLoginUrl(InputServerName), req, 0, map[string]string{"x-fun-user-token": ""})
		ret := res[0].(*msg.TwoStepCreatePlayerA)
		if ret.Err.Id != 0 {
			return "", nil
		}

		accessToken = ret.Token
	}

	req := &msg.PlayerLoginR{}

	res := gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, 0, map[string]string{"x-fun-user-token": accessToken})
	if len(res) > 0 {
		if pr, ok := res[0].(*msg.PlayerLoginA); ok {
			fmt.Printf("len.CTroops):%d\n", len(pr.PlayerInfo.CTroops))
		}
	}
	return accessToken, res[0].(*msg.PlayerLoginA)
}
