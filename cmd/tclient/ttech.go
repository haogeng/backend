package main

import (
	"server/pkg/gen/msg"
	"server/pkg/gpclient"
)

func testUpgradeTech() {
	token, _ := testPlayerLogin()

	techId := int32(1)
	seqNum := 1
	//lock 2
	{
		req := &msg.StartUpgradeTechR{
			Id: techId,
		}
		gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
		seqNum++
	}

	{
		req := &msg.EndUpgradeTechR{
			Id: techId,
		}
		gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
		seqNum++
	}
}

func testInstantUpgradeTech() {
	token, _ := testPlayerLogin()

	techId := int32(1)
	seqNum := 1
	//lock 2
	{
		req := &msg.InstantUpgradeTechR{
			Id: techId,
		}
		gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
		seqNum++
	}

	{
		req := &msg.InstantUpgradeTechR{
			Id: techId,
		}
		gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
		seqNum++
	}
}
