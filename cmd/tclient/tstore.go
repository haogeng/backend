package main

import (
	"log"
	"server/pkg/gen/msg"
	"server/pkg/gpclient"
)

func testStore() {
	token, _ := testPlayerLogin()

	seqNum := 1

	var s *msg.StoreItem
	{
		req := &msg.GetStoreItemR{
			Id: 1,
		}
		a := gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
		a2 := a[0].(*msg.GetStoreItemA)
		s = a2.Item
		log.Print(s)
		seqNum++
	}

	{
		req := &msg.GetStoreInfoR{}
		gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
		seqNum++
	}

	{
		req := &msg.BuyGoodR{
			StoreId: 1,
			ShelfId: 10101,
			GoodId:  s.Shelfs[10101].GoodId,
			Count:   1,
		}
		gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
		seqNum++
	}

	{
		req := &msg.ManualRefreshStoreR{
			StoreId: 1,
		}
		gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
		seqNum++
	}

}
