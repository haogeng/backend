package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"server/pkg/gen/msg"
	"server/pkg/gpclient"
	"strconv"
)

func testGmSendMail() {
	token, _ := testPlayerLogin()

	seqNum := 1

	req := &msg.GmSendMailR{
		SecretToken: "gmx-secret-uxike78J!#~88oL",
		TargetId:    92273164408632160,
		ConfigId:    0,
		TitleId:     1,
		ContentId:   1,
	}
	req.Rewards = append(req.Rewards, &msg.StaticObjInfo{
		Id:    801,
		Type:  2,
		Count: 1,
		Level: 1,
		Star:  5,
		Civil: 0,
		Param: 0,
	})
	req.Rewards = append(req.Rewards, &msg.StaticObjInfo{
		Id:    601,
		Type:  2,
		Count: 1,
		Level: 1,
		Star:  5,
		Civil: 0,
		Param: 0,
	})
	req.Rewards = append(req.Rewards, &msg.StaticObjInfo{
		Id:    901,
		Type:  2,
		Count: 1,
		Level: 1,
		Star:  5,
		Civil: 0,
		Param: 0,
	})
	req.Rewards = append(req.Rewards, &msg.StaticObjInfo{
		Id:    1901,
		Type:  2,
		Count: 1,
		Level: 1,
		Star:  5,
		Civil: 0,
		Param: 0,
	})
	req.Rewards = append(req.Rewards, &msg.StaticObjInfo{
		Id:    1601,
		Type:  2,
		Count: 1,
		Level: 1,
		Star:  5,
		Civil: 0,
		Param: 0,
	})
	req.Rewards = append(req.Rewards, &msg.StaticObjInfo{
		Id:    2301,
		Type:  2,
		Count: 1,
		Level: 1,
		Star:  5,
		Civil: 0,
		Param: 0,
	})
	gpclient.ReqProtoWithHeader(GetGmUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
}

func testGmPlayerInfo() {
	if InputDeviceId == "" {
		fmt.Println("请输入查询的用户ID")
		os.Exit(0)
	}
	seqNum := 1
	target, _ := strconv.ParseUint(InputDeviceId, 10, 64)
	req := &msg.GmPlayerInfoR{
		TargetId: target,
	}
	res := gpclient.ReqProtoWithHeader(GetGmUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": "token"})

	if len(res) > 0 {
		if v, ok := res[0].(*msg.GmPlayerInfoA); ok {
			jsonValue, _ := json.MarshalIndent(v, "", "\t")
			ioutil.WriteFile("gmPlayerInfo.json", []byte(jsonValue), os.FileMode(0644))
		}
	}
}
