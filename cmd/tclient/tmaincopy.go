package main

import (
	"server/pkg/gen/msg"
	"server/pkg/gpclient"
)

func testMainCopyInfos() {
	token, _ := testPlayerLogin()

	seqNum := 1

	req := &msg.MainCopyInfosR{}

	gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
}

func testStrongholdInfos() {
	token, _ := testPlayerLogin()

	seqNum := 1

	req := &msg.StrongholdInfosR{
		ChapterId: 1000,
	}

	gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
}

func testQuickStartMainCopy() {
	token, _ := testPlayerLogin()

	seqNum := 1

	req := &msg.QuickStartMainCopyR{}

	gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
}

func testStrongholdReward() {
	token, _ := testPlayerLogin()

	seqNum := 1

	req := &msg.StrongholdRewardR{
		ChapterId:    1000,
		StrongholdId: 1000001,
	}

	gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
}
