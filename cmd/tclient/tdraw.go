package main

import (
	"fmt"
	"server/pkg/gen/msg"
	"server/pkg/gpclient"
)

func testDraw() {
	token, _ := testPlayerLogin()

	// 约定： 0,1,200,0,0,0
	// type,id,count,param,level,star
	seqNum := 1
	drawId := int32(1)
	// add diamond 4000
	if true {
		//if false {
		req := &msg.DebugAddObjR{
			Info: fmt.Sprintf("0,%d,%d,0,0,0", int32(msg.CurrencyId_CI_Diamond), 4000),
		}
		gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
		seqNum++
	}

	// add item 1005
	if true {
		//if false {
		req := &msg.DebugAddObjR{
			Info: fmt.Sprintf("1,%d,%d,0,0,0", 1005, 4000),
		}
		gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
		seqNum++
	}

	// single diamond
	{
		req := &msg.DrawR{
			Id:          drawId,
			CountType:   msg.DrawCountType_DCT_Single,
			ConsumeType: msg.DrawConsumeType_DCT_Currency,
		}
		gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
		seqNum++
	}

	// ten diamond
	{
		req := &msg.DrawR{
			Id:          drawId,
			CountType:   msg.DrawCountType_DCT_Ten,
			ConsumeType: msg.DrawConsumeType_DCT_Currency,
		}
		gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
		seqNum++
	}

	// single diamond
	{
		req := &msg.DrawR{
			Id:          drawId,
			CountType:   msg.DrawCountType_DCT_Single,
			ConsumeType: msg.DrawConsumeType_DCT_Item,
		}
		gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
		seqNum++
	}

	// ten diamond
	{
		req := &msg.DrawR{
			Id:          drawId,
			CountType:   msg.DrawCountType_DCT_Ten,
			ConsumeType: msg.DrawConsumeType_DCT_Item,
		}
		gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
		seqNum++
	}
}
