package main

import (
	"server/pkg/gen/msg"
	"server/pkg/gpclient"
)

func testSeqCheck() {
	token, _ := testPlayerLogin()

	// 约定： 0,1,200,0,0,0
	// type,id,count,param,level,star

	seqNum := -1
	//lock 2
	// currency
	{
		req := &msg.DebugAddObjR{
			Info: "0,1,200,0,0,0",
		}
		gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
		// dont add seqNum to trigger resp cache
		//seqNum++
	}

}
