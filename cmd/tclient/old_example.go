package main

import (
	"bitbucket.org/funplus/golib/encoding2/protobuf"
	"bitbucket.org/funplus/sandwich"
	"bitbucket.org/funplus/sandwich/gen/netutils"
	"bitbucket.org/funplus/sandwich/message"
	metadata2 "bitbucket.org/funplus/sandwich/metadata"
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"reflect"
	"server/pkg/gpclient"

	"server/pkg/gen/msg"
)

func setReqHeader(req *http.Request) {
	req.Header.Set("Content-Type", "application/x-protobuf")

	token := "TODO: token"
	req.Header.Set("x-fun-user-token", token)

	req.Header.Set("charset", "utf-8")

	fmt.Printf("Send header:%v\n", req.Header)
}

// Example1: using router.PacketWith
func exampleUsingPacketWith() {
	fmt.Println("call exampleUsingPacketWith")

	req := &msg.Example1Req{
		Id:   999,
		Name: "devil",
	}
	res := gpclient.ReqProto(GetGameUrl(InputServerName), req, 0)
	fmt.Printf("res: %+v\n", res)
}

// Example12: using router.PacketWith
func example12UsingPacketWith() {
	fmt.Println("call exampleUsingPacketWith")

	client := &http.Client{}

	var reqBody []byte
	// Set Body
	{
		req := &msg.Example1Req{
			Id:   999,
			Name: "devil",
		}
		req2 := &msg.Example2Req{
			Id:   888,
			Name: "heaven",
		}
		// 用于透传信息，，比如seq-id, 等信息，，可灵活扩展，类似于map
		// 可以传空
		md := metadata2.Pairs("k1", "v1", "k2", "v2")

		//md := metadata2.MD{}

		var reqAry []*message.Message
		reqAry = append(reqAry, message.SliceWith(req)...)
		reqAry = append(reqAry, message.SliceWith(req2)...)
		rawPacket, err := sandwich.PacketWith(protobuf.Codec, reqAry, md, 0)
		if err != nil {
			log.Fatal(err)
		}

		reqBody, err = protobuf.Codec.Marshal(rawPacket)
		if err != nil {
			log.Fatal(err)
		}

		fmt.Printf("reqBody: %s\n", reqBody)
	}

	req, err := http.NewRequest("GET", GetGameUrl(InputServerName), bytes.NewReader(reqBody))
	if err != nil {
		log.Fatal(err)
	}

	// Set Header
	setReqHeader(req)

	resp, err := client.Do(req)
	if err != nil {
		log.Fatal(err)
	}

	defer resp.Body.Close()

	respBody, err := ioutil.ReadAll(resp.Body)
	rawPacket := &netutils.RawPacket{}
	if err := protobuf.Codec.Unmarshal(respBody, rawPacket); err != nil {
		log.Fatal(err)
	}

	fmt.Printf("\n\n")
	fmt.Printf("res meta: %v\n", rawPacket.Metadata)
	fmt.Printf("code:%v\n", resp.StatusCode)
	fmt.Printf("header:%v\n", resp.Header)
	fmt.Printf("body:%x\n sBody:%s\n err:%v\n", respBody, string(respBody), err)
}

// Example2: like native code. 客户端可以依次才封装协议
func exampleDirect() {
	fmt.Println("call exampleDirect")

	client := &http.Client{}

	//// Using proxy
	//proxy := func(_ *http.Request) (*url2.URL, error) {
	//	return url2.Parse("http://127.0.0.1:8888")
	//}
	//
	//transport := &http.Transport{Proxy: proxy}
	//
	//client := &http.client{Transport: transport}

	var reqBody []byte
	// Set Body
	{
		req := &msg.Example1Req{
			Id:   888,
			Name: "god",
		}

		// 用于透传信息，，比如seq-id, 等信息，，可灵活扩展，类似于map
		rawPacket := &netutils.RawPacket{}

		// 必须是偶数kv对 可以不传
		//rawPacket.Metadata = []string{"k1", "v1", "k2", "v2"}

		// 可以是多个msg，类似于pipeline发送，此处只示范1个msg的情况
		raw, _ := protobuf.Codec.Marshal(req)

		// 通过反射获得 "msg.Example1Req"
		uri := reflect.TypeOf(*req).String()
		fmt.Printf("Req uri:%s\n", uri)

		rawAny := &netutils.RawAny{
			Uri:         uri,      // 协议全名，
			Raw:         raw,      // 真实的协议pb bytes
			PassThrough: "pass-1", // seq-id 系列
		}
		//rawPacket.RawAny = rawAny
		rawPacket.RawAny = append(rawPacket.RawAny, rawAny)

		reqBody, _ = protobuf.Codec.Marshal(rawPacket)

		jsonPacket, _ := json.Marshal(rawPacket)
		jsonReq, _ := json.Marshal(req)
		fmt.Printf("Req packet:%s\nReq msg:%s\n", string(jsonPacket), string(jsonReq))
	}

	req, err := http.NewRequest("GET", GetGameUrl(InputServerName), bytes.NewReader(reqBody))
	if err != nil {
		log.Fatal(err)
	}

	// Set Header
	setReqHeader(req)

	resp, err := client.Do(req)
	if err != nil {
		log.Fatal(err)
	}

	defer resp.Body.Close()

	respBody, err := ioutil.ReadAll(resp.Body)
	rawPacket := &netutils.RawPacket{}
	if err := protobuf.Codec.Unmarshal(respBody, rawPacket); err != nil {
		log.Fatal(err)
	}

	// 已知uri，如"msg.Example1Res"， 此处根据名字创建对应对象
	// golang中无全局对象注册，，需要自己额外加机制支持
	// 此处，简单写死
	// 支持反射创建的语言，可以通过名字反射创建！
	res := &msg.Example1Res{}

	// 此处可能是个数组
	protobuf.Codec.Unmarshal(rawPacket.RawAny[0].Raw, res)

	jsonPacket, _ := json.Marshal(rawPacket)
	jsonRes, _ := json.Marshal(res)
	fmt.Printf("\n\n")
	fmt.Printf("code:%v \nRes packet:%s\n", resp.StatusCode, string(jsonPacket))
	fmt.Printf("Res msg: %s\n", string(jsonRes))
	fmt.Printf("Recv header:%v\n", resp.Header)
}
