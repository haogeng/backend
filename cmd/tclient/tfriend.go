package main

import (
	"server/pkg/gen/msg"
	"server/pkg/gpclient"
)

func testGetFriendInfos() {
	token, _ := testPlayerLogin()

	seqNum := 1

	req := &msg.FriendInfosR{}

	gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
}

func testGetApplyFriendInfos() {
	token, _ := testPlayerLogin()

	seqNum := 1

	req := &msg.ApplyFriendInfosR{}

	gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
}

func testGetBlackFriendInfos() {
	token, _ := testPlayerLogin()

	seqNum := 1

	req := &msg.BlackFriendInfosR{}

	gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
}

func testRecommendFriends() {
	token, _ := testPlayerLogin()

	seqNum := 1

	req := &msg.RecommendFriendsR{}

	gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
}

func testSearchPlayerInfo() {
	token, _ := testPlayerLogin()

	seqNum := 1

	req := &msg.SearchPlayerInfoR{
		SearchKey: "u90860939624653332",
	}

	gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
}

func testSendApplyFriend() {
	token, _ := testPlayerLogin()

	seqNum := 1

	req := &msg.SendApplyFriendR{
		ApplyFriendId: 90860939624653332,
	}

	gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
}

func testJoinApplyFriend() {
	token, _ := testPlayerLogin()

	seqNum := 1

	req := &msg.JoinApplyFriendR{
		JoinApplyUserId: 0,
	}

	gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
}

func testDeleteApplyFriend() {
	token, _ := testPlayerLogin()

	seqNum := 1

	req := &msg.DeleteApplyFriendR{
		DeleteApplyUserId: 90855282620047907,
	}

	gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
}

func testAddBlackFriend() {
	token, _ := testPlayerLogin()

	seqNum := 1

	req := &msg.AddBlackFriendR{
		BlackFriendId: 90855282620047907,
	}

	gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
}

func testRemoveBlackFriend() {
	token, _ := testPlayerLogin()

	seqNum := 1

	req := &msg.RemoveBlackFriendR{
		RemoveFriendId: 90855282620047907,
	}

	gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
}

func testDeleteFriend() {
	token, _ := testPlayerLogin()

	seqNum := 1

	req := &msg.DeleteFriendR{
		DeleteFriendId: 90860939624653332,
	}

	gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
}

func testQuickSendAndGet() {
	token, _ := testPlayerLogin()

	seqNum := 1

	req := &msg.QuickSendAndGetR{
		Type: 0,
	}

	gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
}
