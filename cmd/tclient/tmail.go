package main

import (
	"server/pkg/gen/msg"
	"server/pkg/gpclient"
)

func testMailInfo() {
	token, _ := testPlayerLogin()

	seqNum := 1

	req := &msg.MailInfoR{}

	gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})

}

func testRecvMailReward() {
	token, _ := testPlayerLogin()

	seqNum := 1

	req := &msg.RecvMailRewardR{
		Id: 1,
	}

	gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
}

func testRecvMailRewardAll() {
	token, _ := testPlayerLogin()

	seqNum := 1

	req := &msg.RecvMailRewardAllR{}

	gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
}

func testReadMail() {
	token, _ := testPlayerLogin()

	seqNum := 1

	req := &msg.ReadMailR{
		Id: 1,
	}

	gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
}

func testDelMail() {
	token, _ := testPlayerLogin()

	seqNum := 1

	req := &msg.DelMailR{
		Id: 1,
	}

	gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
}

func testDelMailAll() {
	token, _ := testPlayerLogin()

	seqNum := 1

	req := &msg.DelMailAllR{}

	gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
}
