package main

import (
	"server/pkg/gen/msg"
	"server/pkg/gpclient"
)

func testManualRefreshBounty() {
	token, _ := testPlayerLogin()
	seqNum := 1
	{
		req := &msg.ManualRefreshBountyR{}
		gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
		seqNum++
	}
}

func testBounty() {
	token, _ := testPlayerLogin()

	//hallCityId := 1

	seqNum := 1

	// added res
	// type,id,count,param,level,star
	//{
	//	req := &msg.DebugAddObjR{
	//		Info: "0,1,10000,0,0,0",
	//	}
	//	gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
	//	seqNum++
	//}
	//
	//{
	//	req := &msg.DebugAddObjR{
	//		Info: "0,6,10000,0,0,0",
	//	}
	//	gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
	//	seqNum++
	//}

	{
		req := &msg.DebugAddObjR{
			// type,id,count,param,level,star
			Info: "2,701,1,0,1,6",
		}
		gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
		seqNum++
	}
	{
		req := &msg.DebugAddObjR{
			// type,id,count,param,level,star
			Info: "2,301,1,0,1,6",
		}
		gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
		seqNum++
	}
	{
		req := &msg.DebugAddObjR{
			// type,id,count,param,level,star
			Info: "2,401,1,0,1,6",
		}
		gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
		seqNum++
	}

	{
		req := &msg.DebugAddObjR{
			// type,id,count,param,level,star
			Info: "0,2,10,0,0,0",
		}
		gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
		seqNum++
	}

	id1, id2 := int32(401), int32(402)
	troopIds := []int32{103, 102, 104}
	// 派遣
	{
		req := &msg.StartBountyR{
			// type,id,count,param,level,star
			Id:       id1,
			TroopIds: troopIds,
		}
		gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
		seqNum++
	}
	{
		req := &msg.StartBountyR{
			// type,id,count,param,level,star
			Id:       id2,
			TroopIds: troopIds,
		}
		gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
		seqNum++
	}
	{
		req := &msg.GetBountyRewardR{
			// type,id,count,param,level,star
			Ids: []int32{id1},
		}
		gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
		seqNum++
	}
	{
		req := &msg.GetBountyRewardR{
			// type,id,count,param,level,star
			Ids: []int32{id2},
		}
		gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
		seqNum++
	}
	//questHallId := int32(13)
	//lock 2
	//{
	//	req := &msg.StartUnlockBuildingR{
	//		Id: questHallId,
	//	}
	//	gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
	//	seqNum++
	//}
	//
	//{
	//	req := &msg.EndUnlockBuildingR{
	//		Id: questHallId,
	//	}
	//	gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
	//	seqNum++
	//}
	//
	//{
	//	req := &msg.StartUpgradeBuildingR{
	//		Id: questHallId,
	//	}
	//	gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
	//	seqNum++
	//}
	//
	//{
	//	req := &msg.EndUpgradeBuildingR{
	//		Id: questHallId,
	//	}
	//	gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
	//	seqNum++
	//}
}
