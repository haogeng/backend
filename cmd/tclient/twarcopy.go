package main

import (
	"server/pkg/gen/msg"
	"server/pkg/gpclient"
)

func testWarDiffInfos() {
	token, _ := testPlayerLogin()

	seqNum := 1

	req := &msg.WarDiffInfosR{}

	gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
}

func testWarBeginBattle() {
	token, _ := testPlayerLogin()

	seqNum := 1

	req := &msg.ReqStartBattleR{
		ConfigId: 1000001,
		Type:     msg.BattleType_EBT_WarChapter,
		Form: &msg.Formation{
			Type:     msg.FormationType_FT_WarChapter,
			TroopIds: []int32{10, 9, 28, 18, 0, 0, 0, 0, 0},
		},
	}
	gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
}

func testWarEndBattle() {
	token, _ := testPlayerLogin()

	seqNum := 1

	req := &msg.ReqEndBattleR{
		Type:     msg.BattleType_EBT_WarChapter,
		ConfigId: 1000003,
		Result:   msg.BattleResult_BR_Success,
	}

	gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
}

func testWarBoxReward() {
	token, _ := testPlayerLogin()

	seqNum := 1

	req := &msg.WarBoxRewardR{
		WarId: 1,
		BoxId: 1001,
	}

	gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
}

func testGmModifyWarCopy() {
	token, _ := testPlayerLogin()

	seqNum := 1

	req := &msg.GmModifyWarCopyR{
		TargetId:     92300819240133171,
		WarId:        1,
		ChapterId:    1010,
		StrongholdId: 1010209,
	}

	gpclient.ReqProtoWithHeader(GetGmUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
}
