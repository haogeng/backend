package main

import (
	"server/pkg/gen/msg"
	"server/pkg/gpclient"
	"strconv"
)

func testGetClanList() {
	token, _ := testPlayerLogin()

	seqNum := 1

	req := &msg.GetClanListR{}

	gpclient.ReqProtoWithHeader(GetClanUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
}

func testGetClanInfo() {
	token, _ := testPlayerLogin()

	seqNum := 1

	req := &msg.GetClanInfoR{
		//Id: 77012091374317922, //77012088937427233,77012091906994450
	}

	gpclient.ReqProtoWithHeader(GetClanUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
}

func testClanMemberList() {
	token, _ := testPlayerLogin()

	seqNum := 1

	req := &msg.ClanMemberListR{
		Id: 78173742261910618,
	}

	gpclient.ReqProtoWithHeader(GetClanUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
}

func testModifyClanInfo() {
	token, _ := testPlayerLogin()

	seqNum := 1

	for i := 1; i < 7; i++ {
		seqNum = seqNum + i

		req := &msg.ModifyClanInfoR{
			Type:    msg.ModifyClanType(i),
			Content: "testTest",
		}

		gpclient.ReqProtoWithHeader(GetClanUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
	}
}

func testApplyFoundClan() {
	token, _ := testPlayerLogin()

	seqNum := 1

	for i := 0; i < 50; i++ {
		seqNum = seqNum + i

		req := &msg.ApplyFoundClanR{
			Name:  "大佬集结" + strconv.Itoa(i),
			Badge: 0,
		}

		gpclient.ReqProtoWithHeader(GetClanUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
	}
}

func testSearchClanInfo() {
	token, _ := testPlayerLogin()

	seqNum := 1

	req := &msg.SearchClanR{
		Content: "100001", //77012088937427233,77012091906994450
	}

	gpclient.ReqProtoWithHeader(GetClanUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
}

func testSearchClanInfo2() {
	token, _ := testPlayerLogin()

	seqNum := 1

	req := &msg.SearchClanR{
		Content: "傻傻的沙和尚",
	}

	gpclient.ReqProtoWithHeader(GetClanUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
}

func testApplyJoinClan() {
	token, _ := testPlayerLogin()

	seqNum := 1

	req := &msg.ApplyJoinClanR{
		Id: 78173742261910618,
	}

	gpclient.ReqProtoWithHeader(GetClanUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
}

func testAgreeJoinClan() {
	token, _ := testPlayerLogin()

	seqNum := 1

	req := &msg.AgreeJoinClanR{
		PlayerId: 78174293255044223,
	}

	gpclient.ReqProtoWithHeader(GetClanUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
}

func testAppointPosition() {
	token, _ := testPlayerLogin()

	seqNum := 1

	req := &msg.AppointPositionR{
		PlayerId: 76740316757406023,
		Position: 2,
	}

	gpclient.ReqProtoWithHeader(GetClanUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
}

func testExpelClanMember() {
	token, _ := testPlayerLogin()

	seqNum := 1

	req := &msg.ExpelClanMemberR{
		PlayerId: 76740316757406023,
	}

	gpclient.ReqProtoWithHeader(GetClanUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
}

func testQuitClan() {
	token, _ := testPlayerLogin()

	seqNum := 1

	req := &msg.QuitClanR{}

	gpclient.ReqProtoWithHeader(GetClanUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
}

func testGetClanApplyInfo() {
	token, _ := testPlayerLogin()

	seqNum := 1

	req := &msg.GetClanApplyInfoR{
		Id: 77012088937427233,
	}

	gpclient.ReqProtoWithHeader(GetClanUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
}

func testGetClanLog() {
	token, _ := testPlayerLogin()

	seqNum := 1

	req := &msg.GetClanLogR{
		Id: 78174293255044223,
	}

	gpclient.ReqProtoWithHeader(GetClanUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
}

func testClanDonate() {
	token, _ := testPlayerLogin()

	seqNum := 1

	req := &msg.ClanDonateR{
		DonateType: 1,
	}

	gpclient.ReqProtoWithHeader(GetClanUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
}

func testSendClanMail() {
	token, _ := testPlayerLogin()

	seqNum := 1

	req := &msg.SendClanMailR{
		SendPlayerId: 78174293255044223,
		Title:        "test clan mail",
		Content:      "test clan mail content",
	}

	gpclient.ReqProtoWithHeader(GetClanUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
}

func testGetClanBoss() {
	token, _ := testPlayerLogin()

	seqNum := 1
	req := &msg.GetClanBossR{}

	gpclient.ReqProtoWithHeader(GetClanUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
}

func testClanBossStartBattle() {
	token, _ := testPlayerLogin()

	seqNum := 1
	req := &msg.ReqStartBattleR{
		Type:       msg.BattleType_EBT_ClanBoss,
		ClanBossId: 1,
	}

	gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
}

func testClanBossEndBattle() {
	token, _ := testPlayerLogin()

	seqNum := 1
	req := &msg.ReqEndBattleR{
		Type:              msg.BattleType_EBT_ClanBoss,
		ClanBossId:        1,
		ClanBossBloodId:   1010,
		ClanBossAllDamage: 275000,
	}

	gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
}
