package main

import (
	uuid2 "github.com/hashicorp/go-uuid"
	"go.uber.org/atomic"
	"log"
	"server/pkg/gen/msg"
	"server/pkg/gpclient"
	"sync"
	"time"
)

func testServerList() {
	req := &msg.ServerListR{}
	gpclient.ReqProto(GetLoginUrl(InputServerName), req, 0)
}

func testLogin() string {
	//deviceId, _ := uuid2.GenerateUUID()
	deviceId := InputDeviceId

	req := &msg.LoginR{
		DeviceId:     deviceId,
		Version:      nil,
		ChannelType:  "",
		ChannelToken: "",
	}
	req.Version = &msg.VersionInfo{
		Main:     0,
		Minor:    0,
		Build:    1,
		Platform: 0,
		Language: ";delete table account;",
	}
	ret := gpclient.ReqProtoWithHeader(GetLoginUrl(InputServerName), req, 0, map[string]string{
		"xignore_rate_limit": "true",
	})
	if ret != nil {
		//fmt.Printf("login ret:%v\n", ret)
	}
	a := ret[0].(*msg.LoginA)
	return a.Token
}

// 需要两步登录
func testLogin2() *msg.LoginA {
	//deviceId, _ := uuid2.GenerateUUID()
	deviceId := InputDeviceId

	req := &msg.LoginR{
		DeviceId:                deviceId,
		Version:                 nil,
		ChannelType:             "",
		ChannelToken:            "",
		NeedTwoStepCreatePlayer: true,
	}
	req.Version = &msg.VersionInfo{
		Main:     0,
		Minor:    0,
		Build:    1,
		Platform: 0,
		Language: ";delete table account;",
	}
	ret := gpclient.ReqProtoWithHeader(GetLoginUrl(InputServerName), req, 0, map[string]string{
		"xignore_rate_limit": "true",
	})
	if ret != nil {
		//fmt.Printf("login ret:%v\n", ret)
	}
	a, ok := ret[0].(*msg.LoginA)
	if ok {
		return a
	}
	return nil
}

func testRepeatLogin() {
	deviceId, _ := uuid2.GenerateUUID()
	for i := 0; i < 2; i++ {
		req := &msg.LoginR{
			DeviceId:     deviceId,
			Version:      nil,
			ChannelType:  "",
			ChannelToken: "",
		}
		req.Version = &msg.VersionInfo{
			Main:     0,
			Minor:    0,
			Build:    1,
			Platform: 0,
			Language: ";delete table account;",
		}
		gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, 0, map[string]string{
			"xignore_rate_limit": "true",
		})
	}
}

func testBenchLogin() {
	var wg sync.WaitGroup
	var count atomic.Int32
	var okNums atomic.Int32

	for i := 0; i < 4; i++ {
		wg.Add(1)
		go func() {
			defer func() {
				wg.Done()
			}()

			for j := 0; j < 100; j++ {
				count.Inc()
				deviceId, _ := uuid2.GenerateUUID()
				req := &msg.LoginR{
					DeviceId:     deviceId,
					Version:      nil,
					ChannelType:  "",
					ChannelToken: "",
				}
				req.Version = &msg.VersionInfo{
					Main:     0,
					Minor:    0,
					Build:    1,
					Platform: 0,
					Language: ";delete table account;",
				}
				ret := gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, 0, map[string]string{
					"xignore_rate_limit": "true",
				})
				if ret != nil {
					okNums.Inc()
				}
				time.Sleep(time.Millisecond * 100)
			}
		}()
	}

	wg.Wait()
	log.Printf("Over %v/%v\n", okNums, count)
}
