package main

import (
	"server/pkg/gen/msg"
	"server/pkg/gpclient"
)

func testOnHookIncome() {
	token, _ := testPlayerLogin()

	seqNum := 1
	//lock 2
	{
		req := &msg.OnHookIncomeR{}
		gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
		seqNum++
	}
}

func testQuickOnHook() {
	token, _ := testPlayerLogin()

	seqNum := 1
	//lock 2
	{
		req := &msg.QuickOnHookR{}
		gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
		seqNum++
	}
}
