package main

import (
	"fmt"
	"server/pkg/gen/msg"
	"server/pkg/gpclient"
)

func testLevelUpTroop() {
	token, playerLoginA := testPlayerLogin()

	var pInfo = playerLoginA.PlayerInfo
	// get first troop
	var t *msg.ObjInfo
	for _, v := range pInfo.CTroops {
		t = v
		break
	}
	if t == nil {
		fmt.Printf("no troop data\n")
		return
	}

	seqNum := 1
	{
		req := &msg.DebugAddObjR{
			// type,id,count,param,level,star
			Info: "0,5,3000,0,0,0",
		}
		gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
		seqNum++
	}

	{
		req := &msg.LevelUpTroopR{
			Id:        t.Id,
			DestLevel: 5,
		}
		gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
		seqNum++
	}
}

func testStarUpTroop() {
	token, playerLoginA := testPlayerLogin()

	var pInfo = playerLoginA.PlayerInfo
	// get first troop
	var t *msg.ObjInfo
	for _, v := range pInfo.CTroops {
		t = v
		break
	}
	if t == nil {
		fmt.Printf("no troop data\n")
		return
	}

	seqNum := 1
	{
		req := &msg.DebugAddObjR{
			// type,id,count,param,level,star
			Info: "0,5,3000,0,0,0",
		}
		gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
		seqNum++
	}

	{
		req := &msg.StarUpTroopR{
			Id: t.Id,
		}
		gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
		seqNum++
	}
}

func testLevelUpTalent() {
	token, playerLoginA := testPlayerLogin()

	var pInfo = playerLoginA.PlayerInfo

	var t *msg.ObjInfo

	// get min troop
	//troopIds := []int32{}
	//for k, _ := range pInfo.CTroops {
	//	troopIds = append(troopIds, k)
	//}
	//minId := util.MinInt32(troopIds...)
	minId := int32(64)
	t = pInfo.CTroops[minId]

	if t == nil {
		fmt.Printf("no troop data\n")
		return
	}

	seqNum := 1
	{
		req := &msg.DebugAddObjR{
			// type,id,count,param,level,star
			Info: "0,5,3000,0,0,0",
		}
		gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
		seqNum++
	}

	{
		req := &msg.LevelUpTalentR{
			TroopId:  t.Id,
			TalentId: 1,
		}
		gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
		seqNum++
	}
}

func testLevelUpSpecial() {
	token, playerLoginA := testPlayerLogin()

	var pInfo = playerLoginA.PlayerInfo

	var t *msg.ObjInfo

	// get min troop
	//troopIds := []int32{}
	//for k, _ := range pInfo.CTroops {
	//	troopIds = append(troopIds, k)
	//}
	//minId := util.MinInt32(troopIds...)
	minId := int32(1)
	t = pInfo.CTroops[minId]

	if t == nil {
		fmt.Printf("no troop data\n")
		return
	}

	seqNum := 1
	{
		req := &msg.DebugAddObjR{
			// type,id,count,param,level,star
			Info: "1,1001,100,0,0,0",
		}
		gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
		seqNum++
	}

	{
		req := &msg.LevelUpSpecialtyR{
			TroopId:     t.Id,
			SpecialtyId: t.Pairs[0].Key,
			DestLevel:   t.Pairs[0].Value + 1,
		}
		gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
		seqNum++
	}
}

func testResetTroop() {
	token, playerLoginA := testPlayerLogin()

	var pInfo = playerLoginA.PlayerInfo

	var t *msg.ObjInfo

	minId := int32(1)
	t = pInfo.CTroops[minId]

	if t == nil {
		fmt.Printf("no troop data\n")
		return
	}

	seqNum := 1
	{
		req := &msg.DebugAddObjR{
			// type,id,count,param,level,star
			Info: "0,5,3000,0,0,0",
		}
		gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
		seqNum++
	}

	{
		req := &msg.ResetTroopR{
			TroopId: t.Id,
		}
		gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
		seqNum++
	}
}

func testLayOffTroop() {
	token, playerLoginA := testPlayerLogin()

	var pInfo = playerLoginA.PlayerInfo

	var t *msg.ObjInfo

	minId := int32(1)
	t = pInfo.CTroops[minId]

	if t == nil {
		fmt.Printf("no troop data\n")
		return
	}

	seqNum := 1
	{
		req := &msg.DebugAddObjR{
			// type,id,count,param,level,star
			Info: "0,5,3000,0,0,0",
		}
		gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
		seqNum++
	}

	{
		troopIds := []int32{0}
		troopIds = append(troopIds, t.Id)
		req := &msg.LayOffTroopR{
			TroopId: troopIds,
		}
		gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
		seqNum++
	}
}

func testTroopRelationship() {
	token, playerLoginA := testPlayerLogin()

	var pInfo = playerLoginA.PlayerInfo
	_ = pInfo

	seqNum := 1
	{
		req := &msg.DebugAddObjR{
			// type,id,count,param,level,star
			Info: "2,1901,1,0,1,5",
		}
		gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
		seqNum++
	}
	{
		req := &msg.DebugAddObjR{
			// type,id,count,param,level,star
			Info: "2,2201,1,0,1,5",
		}
		gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
		seqNum++
	}

	{
		req := &msg.LevelUpTroopR{
			Id:        13,
			DestLevel: 20,
		}
		gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
		seqNum++
	}
	{
		req := &msg.LevelUpTroopR{
			Id:        14,
			DestLevel: 20,
		}
		gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
		seqNum++
	}

	{
		req := &msg.LayOffTroopR{
			TroopId: []int32{13},
		}
		gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
		seqNum++
	}

	{
		req := &msg.AddTroopRelationshipR{
			// type,id,count,param,level,star
			RelationId: 2,
			TroopData:  map[int32]int32{1901: 13, 2201: 14},
		}
		gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
		seqNum++
	}
}
