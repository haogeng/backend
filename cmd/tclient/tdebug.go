package main

import (
	"server/pkg/gen/msg"
	"server/pkg/gpclient"
)

func testDebugSetServerTempTime() {
	token, _ := testPlayerLogin()

	seqNum := 1
	{
		req := &msg.DebugSetServerTempTimeR{
			Info: "2020-06-4-16-56-50",
		}
		gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
		seqNum++
	}
	{
		req := &msg.DebugSetServerTempTimeR{
			Info: "2020-08-04-16-56-50",
		}
		gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
		seqNum++
	}
}

func testDebugAddObj() {
	token, _ := testPlayerLogin()

	// 约定： 0,1,200,0,0,0
	// type,id,count,param,level,star

	seqNum := 1
	////test sub
	//{
	//	req := &msg.DebugAddObjR{
	//		Info: "0,1,-200,0,0,0",
	//	}
	//	gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
	//	seqNum++
	//}
	//return

	//{
	//	req := &msg.DebugAddObjR{
	//		Info: "7,1,3,0,0,0",
	//	}
	//	gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
	//	seqNum++
	//	return
	//}

	//lock 2
	// currency
	{
		req := &msg.DebugAddObjR{
			Info: "0,1,200,0,0,0|1,1001,200,0,0, 0|",
		}
		gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
		seqNum++
	}

	//for {
	//	time.Sleep(time.Second * 10)
	//	{
	//		req := &msg.DebugAddObjR{
	//			Info: "0,1,200,0,0,0|1,1001,200,0,0, 0|",
	//		}
	//		gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
	//		seqNum++
	//	}
	//}

	// item
	{
		req := &msg.DebugAddObjR{
			Info: "1,1001,200,0,0,0",
		}
		gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
		seqNum++
	}

	// hero troop
	{
		req := &msg.DebugAddObjR{
			Info: "2,601,3,0,2,4",
		}
		gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
		seqNum++
	}

	// equip
	{
		req := &msg.DebugAddObjR{
			Info: "3,1,3,1,0,0",
		}
		gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
		seqNum++
	}

	// drop
	{
		req := &msg.DebugAddObjR{
			Info: "4,1,3,0,0,0",
		}
		gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
		seqNum++
	}
}
