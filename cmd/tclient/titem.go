package main

import (
	"fmt"
	"server/pkg/gen/msg"
	"server/pkg/gpclient"
)

func testSellItem() {
	token, _ := testPlayerLogin()

	// 约定： 0,1,200,0,0,0
	// type,id,count,param,level,star

	seqNum := 1
	itemId := 1001
	itemCount := 5
	// add item
	{
		req := &msg.DebugAddObjR{
			Info: fmt.Sprintf("1,%d,%d,0,0,0", itemId, itemCount),
		}
		gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
		seqNum++
	}

	// sell item
	{
		req := &msg.SellItemR{
			Id:    int32(itemId),
			Count: int32(itemCount),
		}
		gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
		seqNum++
	}
}

func testUseItem() {
	token, _ := testPlayerLogin()

	// 约定： 0,1,200,0,0,0
	// type,id,count,param,level,star

	seqNum := 1
	itemId := 10008
	itemCount := 1
	// add item
	{
		req := &msg.DebugAddObjR{
			Info: fmt.Sprintf("1,%d,%d,0,0,0", itemId, itemCount),
		}
		gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
		seqNum++
	}

	// use item
	{
		req := &msg.UseItemR{
			Id:    int32(itemId),
			Count: int32(itemCount),
		}
		gpclient.ReqProtoWithHeader(GetGameUrl(InputServerName), req, seqNum, map[string]string{"x-fun-user-token": token})
		seqNum++
	}
}
