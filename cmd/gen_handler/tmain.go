package main

import (
	"fmt"
	"os"
	"regexp"

	"strings"
)

var supportStr = ` 
support:
 1
    rpc MailInfo (msg.MailInfoR) returns (msg.MailInfoA) {}
    rpc RecvMailReward (msg.RecvMailRewardR) returns (msg.RecvMailRewardA) {}
 2
	MailInfo RecvMailReward

example:
./local_gen_handler_darwin "    rpc MailInfo (msg.MailInfoR) returns (msg.MailInfoA) {}
    rpc RecvMailReward (msg.RecvMailRewardR) returns (msg.RecvMailRewardA) {}
    rpc RecvMailRewardAll (msg.RecvMailRewardAllR) returns (msg.RecvMailRewardAllA) {}
    rpc ReadMail (msg.ReadMailR) returns (msg.ReadMailA) {}
    rpc ReadMailAll (msg.ReadMailAllR) returns (msg.ReadMailAllA) {}
    rpc DelMail (msg.DelMailR) returns (msg.DelMailA) {}
    rpc DelMailAll (msg.DelMailAllR) returns (msg.DelMailAllA) {}" | pbcopy

./local_gen_handler_darwin AA BB CC 
`

func GenHandler(input string) (output string, err error) {
	lines := strings.Split(input, "\n")

	re := regexp.MustCompile(`rpc\s*(\w*)\s*\(`)

	names := []string{}

	for i := 0; i < len(lines); i++ {
		if strings.Contains(lines[i], "rpc") {
			// rpc MailInfo (msg.MailInfoR) returns (msg.MailInfoA) {}
			bs := re.FindSubmatch([]byte(lines[i]))
			if len(bs) > 0 {
				names = append(names, string(bs[1]))
			}
		} else {
			// MailInfo RecvMailReward
			ns := strings.Split(lines[i], " ")
			for j := 0; j < len(ns); j++ {
				name := strings.TrimSpace(ns[j])
				if len(name) > 0 {
					names = append(names, name)
				}
			}
		}
	}

	fmt.Printf("// Gen: %s\n", names)
	return genHandler(names)
}

func genHandler(names []string) (output string, err error) {
	var tpl = `
func (*HandlerHttp) XxxTpl(ctx context.Context, req *msg.XxxTplR) (res *msg.XxxTplA, err error) {
	res = &msg.XxxTplA{
		Err: &msg.ErrorInfo{},
	}

	uid, err := xmiddleware.GetUid(ctx)
	if err != nil {
		return
	}

	var c redisdb.PlayerInfoCache
	r := c.Find(uid)
	if r == nil {
		err = internal.ErrNoUser
		return
	}

	// do logic with r

	// if needed, update r
	c.MustUpdate(r.Id, r)

	return
}
`

	for i := 0; i < len(names); i++ {
		a := strings.Replace(tpl, "XxxTpl", names[i], -1)
		output += a
		output += "\n"
	}

	return
}

func main() {
	if len(os.Args) < 2 {
		fmt.Printf("pls input like:\n %s\n ", supportStr)
		return
	}
	argsN := os.Args[1:]
	output, err := GenHandler(strings.Join(argsN, "\n"))
	if err != nil {
		panic(err)
	}
	fmt.Println(output)
}
