# 游戏服打包部署流程

## 一、编译数据
1. cd /data/gmx_dev/trunk/
2. svn update
3. cd toolkits_bin
4. ./dbstate_final.sh

## 二、编译dev版本
1. cd /data/gmx_dev/trunk/
2. ./run.sh

## 三、编译pro版本
1. pro版本需要用到aws
2. `pip3 install awscli`
3. `aws configure` 配置信息查看gmail邮件
4. pro版本的sql文件需要手动更新

## 四、编译docker版本
1. cd /data/gmx_dev/trunk/backend
2. 编译命令在根目录Makefile文件，包含make dev3deploy; make qa7deploy; make root9deploy等。对应独立的用户环境dev，qa，root
3. 编译成功之后会将工作目录/tmp/deploy/xxx下的所有内容压缩打包成xxx.zip并通过scp上传到指定服务器的/data/usr/workspace目录下。

## 五、线上部署
1. 配置用户环境变量。具体配置参考如下：
 >`export GMX_CONFIG_POSTFIX="dev3"`
 > 
 >`export GMX_ENV_HTTP_PORT=28083`'`
2. 将docker-start.sh和docker-compose.yml上传到用户工作目录/data/usr/workspace
3. 确认已安装docker并正常启动
4. 确认已安装mysql并支持远程访问且正常启动
5. 确认已安装redis并支持远程访问且正常启动
6. 执行docker-start.sh，成功之后游戏服内容在/data/usr/xxx/tmp/deploy/
7. Docker ps -a 查看容器运行状态
8. Docker logs xxx  查看容器日志

## 六、约束事项
1. 禁止切换到root账号去执行run.sh、docker-start.sh等脚本
2. 禁止访问其他用户工作空间