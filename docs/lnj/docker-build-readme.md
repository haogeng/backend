列举镜像
docker images

新建一个镜像
docker build -t gmx_dev .

单独运行，映射到外网端口4001
docker run --name gmx_dev -it -d -p 80:28086 -v /Users/centurygame/gmxlogs/gmx-server.log:/gmx-server.log gmx_dev

swarm集群初始化
docker swarm init

通过compose启动集群
docker stack deploy -c docker-compose.yml gmx_dev

查看集群状态
docker stack ps gmx_dev


其他：
docker service ls
docker service rm hellolab_web