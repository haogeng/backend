# gmx_res
# 资源生成流程
```mermaid
sequenceDiagram
根据tag 客户端资源打包工具->>生成新的包和文件:jenkins 0.1.xxx
生成新的包和文件->>nginx服务器: scp 目录(版本号)/资源+version.txt(默认生效,tf默认不生效,pmt开启)[etcd通知??]

```
0.1
├── android
│   ├── dev
│   │   └── 0.1.2007
│   │       ├── files
│   │       └── version.txt
│   └── tf
│       └── 0.1.2007
│           ├── files
│           └── version.txt
├── data
│   └── rawdata
│       └── xlsm
 |-----ios



```mermaid
sequenceDiagram
根据tag  服务器data(conf/xlsm)->>gen/xlsm策划原始文件:protokit->conf/gen/golang/server/pkg/gen
```

# 请求更新流程
```mermaid
sequenceDiagram
客户端->>cdn/oss:http 方式get lighthouse.json
cdn/oss->>客户端:lighthouse.json里有server url
客户端->>游戏服:http方式:getVersion:lighthouse_id,from,app_version,platform
游戏服->>客户端:getVersionResp.json 
```

lighthouse.json

{
  "lighthouse_id": "c2f7814b7c158c17eb1b2c948ca11f2b", 
    "servers": [
    {
      "name": "stable", //服务器标识
      "url": "http://10.0.84.152:28083/api/", //服务器地址
      "version_min": "0.1.2230", //大于等于
      "version_max": "1.1.2230", //小于 左闭右开，客户端版本号匹配上[min,max)这个规则才连这个服务器
      "fallback": [ //如果服务器连不上则连备选地址，可能有多个，逐个尝试
        "http://10.0.84.152:28089/api/",
        "http://10.0.84.152:28085/api/",
        "http://10.0.84.152:28084/api/"
      ],
      "maintenance_open": false, //true标识维护中
      "url_pattern": "http://10.0.84.152/maintenance.html" //维护公告，可能是字符串，也可能是html页面，业务方自行选择
    }
  ]
}

getVersionResp.json 

{
 "lighthouse_id":"c2f7814b7c158c17eb1b2c948ca11f2b",//同上
 "res_detail": { //热更资源版本信息
   "res_editor_version": "1.2.2", //pc数据资源res_version:1.2.2
   "res_android_version": "1.2.2", //android前端资源
   "res_ios_version": "1.2.2" //ios前端资源
 },

 "force_update": false,//是否需要强更

  "update_info": { //版本更新信息
    "version": "0.1.2230", //必填,新版本版本号appversion
      "url_package": "http://10.1.8.138:8081/s/2BVJ", //安装包的下载地址(如果没有商店的话，从这里下载)
      "url_store": "http://itunes.apple.com/id539920547?mt=8",  //商店地址，对应各个渠道的商店
      "url_desc": "http://10.0.84.152/desc_zh.html", //版本更新描述
      "title_desc": "版本更新", //可选，标题描述
      "display_per_day": 5 //必填，建议更新一天显示几次面板，如果>0，当前版本号小于新版本版本号，则提示建议更新
  },
  "maintenance_open": false, //true标识维护中,
 "url_pattern": "http://10.0.84.152/maintenance_zh.html" //维护公告，可能是字符串，也可能是html页面，业务方自行选择
}

# 配置维护工具/gmx_pmt

```mermaid
graph TD
A[自动生成小版本号]-->B[callapime]
B[配置是否启用手动etcd通知??]-->C[x]
C[选择版本号配置是否启用]-->D[etcd通知??]
D-->E[服务器是否重启手动]
E-->C
```
# gmx-gmt

```mermaid
sequenceDiagram
    GMT->>call jenkins:button触发,打包完成后sshpass:scp资源+version.txt
    GMT->>res2s3:button编辑lighthouse负责上传/conf
    GMT->>export data: button调用excel2pb/protokit
    GMT->>commit svn pb: 提交到svn
    GMT->>dat2s3: excel/data copy2s3
    GMT->>游戏服: update 编辑lighthouse负责上传资源版本号/conf 控制版本是否生效
    GMT->>sync server: 控制版本是否生效,现在通过lighthouse来做
    GMT->>游戏服: button控制是否重启游戏服务器
```

## backendgh pkg/gen_db/提交到bitbucket

## sandwichgmt的版本

## frontend的packages

## .env.development 修改ip

# backend 

### 修改base.toml 

### 修改连接数据库.for dbvisitor

 

###  







