# 84.38测试服deploy

## qa为例
> 适用于xbase,qa
```bash
# 切换用户很重要
su qa
cd /data/usr/dev/gmx_dev_trunk
./docker-run.sh
``` 

## 服务器分工
1. pro
    * 线上版本
1. base9 
    * 周稳定版本 
    * 伴随每周客户端的jenkins包
2. daily1
    * 日稳定版本
    * 测试人员(轩腾)配合服务器，每日18:30稳定build版本，由测试人员测试验收
3. qa7
    * 专门给测试人员用的专有服
4. dev3
    * 多人开发版，可能不稳定
5. ????local
    * 后端程序各自本地开发版