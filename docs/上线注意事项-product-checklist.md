# MakerX 测试
> 稳中求胜
> 20200720左右东南亚第一次测试
> 

## 远程db&redis操作
```
mysql password = eUeKRASN7$7*RVh
mycli  -hmakerx-test-mysql.cfi8w7rl2ab3.ap-southeast-1.rds.amazonaws.com -umakerx
iredis -hmakerx-test-redis.mh01pt.ng.0001.apse1.cache.amazonaws.com -p 6379
```

## 端口开放
线上http开80，https开443

## 上线备忘
1. 双redis模式，A单纯缓存，B持久化。B模式主要用于竞技场以及排行榜信息。 
2. 注意查看ulimit，避免文件句柄数不够。 
3. 线上版本gm工具需要检查secretToken是否生效。gm/handler文件
4. IsDevelopment设置为false

## 维护备忘
1. sql-dev,通过查看alembic_version里的数据版本信息，并查找sql-dev，找到需要手动执行的sql代码段

## 工具备忘
1. history | grep mycli (iredis)
2. 