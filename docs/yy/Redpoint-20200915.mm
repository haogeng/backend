<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1600136381318" ID="ID_1669760473" MODIFIED="1600136393653" TEXT="redpoint 20200915">
<node CREATED="1600136416932" ID="ID_557986163" MODIFIED="1600136422871" POSITION="right" TEXT="&#x534f;&#x8bae;">
<node CREATED="1600136425388" ID="ID_107573330" MODIFIED="1600136433358" TEXT="s-&gt;c notify"/>
<node CREATED="1600136434400" ID="ID_125598203" MODIFIED="1600136436660" TEXT="c-&gt;s clear"/>
<node CREATED="1600136442955" ID="ID_740557581" MODIFIED="1600136452752" TEXT="s-&gt;c info on login"/>
</node>
<node CREATED="1600139159727" ID="ID_1750027434" MODIFIED="1600139758582" POSITION="left" TEXT="&#x670d;&#x52a1;&#x5668;&#x76f8;&#x5173;&#x7ea2;&#x70b9;">
<node CREATED="1600139330850" ID="ID_1437617131" MODIFIED="1600139332713" TEXT="&#x8bbe;&#x8ba1;&#x601d;&#x8def;">
<node CREATED="1600139333025" ID="ID_960771922" MODIFIED="1600139335159" TEXT="&#x670d;&#x52a1;&#x5668;&#x8d1f;&#x8d23;&#x52a0;"/>
<node CREATED="1600139335637" ID="ID_656627055" MODIFIED="1600139339314" TEXT="&#x5ba2;&#x6237;&#x7aef;&#x8d1f;&#x8d23;&#x6e05;"/>
</node>
<node CREATED="1600139766293" ID="ID_1073038160" MODIFIED="1600139768236" TEXT="&#x5206;&#x7c7b;">
<node CREATED="1600138765859" ID="ID_1970556814" MODIFIED="1600139794202" TEXT="&#x6bcf;&#x65e5;">
<node CREATED="1600138771007" ID="ID_1169360610" MODIFIED="1600260920975" TEXT="&#x672a;&#x5b8c;&#x6210;&#x7684;&#x8ff7;&#x5bab;">
<icon BUILTIN="button_ok"/>
<node CREATED="1600157215647" ID="ID_454924295" MODIFIED="1600157216238" TEXT="yy "/>
</node>
<node CREATED="1600139377486" ID="ID_598889328" MODIFIED="1600260920978" TEXT="&#x6bcf;&#x65e5;&#x9996;&#x62bd;&#xff1f;">
<icon BUILTIN="help"/>
<icon BUILTIN="button_ok"/>
<node CREATED="1600157213789" ID="ID_50428281" MODIFIED="1600157214421" TEXT="yy "/>
</node>
<node CREATED="1600139408027" ID="ID_196559143" MODIFIED="1600260920978" TEXT="&#x4efb;&#x52a1;&#x5927;&#x5385;&#x6bcf;&#x65e5;&#x91cd;&#x7f6e;&#x540e;">
<icon BUILTIN="button_ok"/>
<node CREATED="1600157212090" ID="ID_1639092120" MODIFIED="1600157212951" TEXT="yy "/>
</node>
<node CREATED="1600139438493" ID="ID_706254352" MODIFIED="1600260920977" TEXT="&#x6bcf;&#x65e5;&#x516c;&#x4f1a;boss">
<icon BUILTIN="button_ok"/>
<node CREATED="1600157199180" ID="ID_798051970" MODIFIED="1600157200053" TEXT="yy "/>
</node>
<node CREATED="1600139473218" ID="ID_1925166575" MODIFIED="1600260920977" TEXT="&#x6bcf;&#x65e5;&#x516c;&#x4f1a;&#x6350;&#x732e;&#x6b21;&#x6570;">
<icon BUILTIN="button_ok"/>
<node CREATED="1600157196745" ID="ID_719527810" MODIFIED="1600157197637" TEXT="yy "/>
</node>
<node CREATED="1600139687020" ID="ID_1483839957" MODIFIED="1600260920976" TEXT="&#x6bcf;&#x89e3;&#x9501;&#x65b0;&#x4e00;&#x5929;&#x7684;7&#x65e5;&#x4efb;&#x52a1;&#x65f6;&#xff1f;">
<icon BUILTIN="help"/>
<icon BUILTIN="button_ok"/>
<node CREATED="1600157189880" ID="ID_1347683714" MODIFIED="1600157193947" TEXT="yy"/>
</node>
</node>
<node CREATED="1600139296452" ID="ID_1312553764" MODIFIED="1600139794205" TEXT="&#x5176;&#x4ed6;&#x4e8b;&#x4ef6;&#x89e6;&#x53d1;&#x7c7b;">
<node CREATED="1600139177230" ID="ID_1667949815" MODIFIED="1600139181422" TEXT="&#x7ade;&#x6280;&#x573a;&#x8d5b;&#x5b63;&#x5f00;&#x59cb;">
<node CREATED="1600157161979" ID="ID_506573816" MODIFIED="1600157176800" TEXT="lnj"/>
</node>
<node CREATED="1600139310075" ID="ID_825220379" MODIFIED="1600311392907" TEXT="&#x6311;&#x6218;&#x6218;&#x62a5;">
<icon BUILTIN="help"/>
<icon BUILTIN="kaddressbook"/>
<node CREATED="1600157166690" ID="ID_1136982422" MODIFIED="1600157172811" TEXT="lnj"/>
</node>
<node CREATED="1600139519341" ID="ID_15922678" MODIFIED="1600311392908" TEXT="&#x65b0;&#x90ae;&#x4ef6;&#x7ea2;&#x70b9;&#x63a8;&#x9001;">
<icon BUILTIN="help"/>
<icon BUILTIN="kaddressbook"/>
<node CREATED="1600310804721" ID="ID_789237647" MODIFIED="1600310809286" TEXT="&#x610f;&#x4e49;">
<node CREATED="1600310809642" ID="ID_123510860" MODIFIED="1600310827331" TEXT="&#x6307;&#x793a;&#x670d;&#x52a1;&#x5668;&#x4e0a;&#x6709;&#x65b0;&#x90ae;&#x4ef6;&#x4e86;&#xff0c;&#x9700;&#x8981;&#x62c9;&#x53d6;&#x4e0b;"/>
</node>
<node CREATED="1600139524892" ID="ID_817849603" MODIFIED="1600310676657" TEXT="&#x63a8;&#x9001;&#x65f6;&#x673a;">
<node CREATED="1600310709280" ID="ID_437861256" MODIFIED="1600310714833" TEXT="&#x7ade;&#x6280;&#x573a;&#x6bcf;&#x65e5;&#x53d1;&#x5956;"/>
<node CREATED="1600310715444" ID="ID_1134164305" MODIFIED="1600310723831" TEXT="&#x6d3b;&#x52a8;&#x7ed3;&#x675f;&#x540e;&#x8865;&#x53d1;&#x90ae;&#x4ef6;&#x63a8;&#x9001;"/>
<node CREATED="1600310724743" ID="ID_346872209" MODIFIED="1600310733131" TEXT="gm&#x90ae;&#x4ef6;&#x63a8;&#x9001;"/>
<node CREATED="1600310775429" ID="ID_1957572402" MODIFIED="1600310777772" TEXT="&#x5176;&#x4ed6;..."/>
</node>
<node CREATED="1600310676893" ID="ID_1455824348" MODIFIED="1600310680317" TEXT="&#x6e05;&#x9664;&#x65f6;&#x673a;">
<node CREATED="1600310735031" ID="ID_28895767" MODIFIED="1600310753876" TEXT="&#x6253;&#x5f00;&#x9875;&#x9762;&#x62c9;&#x53d6;&#x7684;&#x65f6;&#x5019;&#xff0c;&#x524d;&#x7aef;&#x540e;&#x7aef;&#x5404;&#x81ea;&#x6e05;&#x7a7a;&#x90ae;&#x4ef6;&#x7ea2;&#x70b9;"/>
</node>
<node CREATED="1600310681095" ID="ID_1333094486" MODIFIED="1600310758061" TEXT="&#x7c7b;&#x578b;">
<node CREATED="1600310683462" ID="ID_126948355" MODIFIED="1600310702050" TEXT="RPT_TriggerNewMail"/>
</node>
<node CREATED="1600157178687" ID="ID_138853116" MODIFIED="1600157179712" TEXT="yy"/>
</node>
<node CREATED="1600139719089" ID="ID_1681884273" MODIFIED="1600311392909" TEXT="&#x65b0;&#x597d;&#x53cb;&#x7533;&#x8bf7;&#xff1f;">
<icon BUILTIN="help"/>
<icon BUILTIN="kaddressbook"/>
<node CREATED="1600157183204" ID="ID_887049838" MODIFIED="1600157184332" TEXT="lnj"/>
</node>
<node CREATED="1600314661525" ID="ID_496939993" MODIFIED="1600314667424" TEXT="&#x516c;&#x4f1a;&#x7533;&#x8bf7;&#xff1f;">
<node CREATED="1600314837918" ID="ID_345991846" MODIFIED="1600314845628" TEXT="gh">
<icon BUILTIN="kaddressbook"/>
</node>
</node>
<node CREATED="1600311396504" ID="ID_1176277636" MODIFIED="1600311403930" TEXT="RTM&#x63a8;&#x9001;">
<icon BUILTIN="kaddressbook"/>
</node>
</node>
</node>
</node>
<node CREATED="1600139155766" ID="ID_927503286" MODIFIED="1600845776605" POSITION="right" TEXT="add">
<node CREATED="1600845777050" ID="ID_945328378" MODIFIED="1600845785169" TEXT="&#x6bcf;&#x65e5;&#x5927;&#x5385;&#x4efb;&#x52a1;"/>
<node CREATED="1600845792202" ID="ID_853529263" MODIFIED="1600845863992" TEXT="&#x5927;&#x5385;&#x5347;&#x7ea7;&#x4e8b;&#x4ef6;"/>
</node>
</node>
</map>
