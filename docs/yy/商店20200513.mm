<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1589361997682" ID="ID_120086828" MODIFIED="1589362022923" TEXT="&#x5546;&#x5e97;20200513">
<node CREATED="1589362501923" ID="ID_1905477892" MODIFIED="1589362505704" POSITION="right" TEXT="&#x5206;&#x5c42;">
<node CREATED="1589362506084" ID="ID_1906621361" MODIFIED="1589364736925" TEXT="store">
<node CREATED="1589362641540" ID="ID_1253529681" MODIFIED="1589363442816" TEXT="open conditions">
<node CREATED="1589363443428" ID="ID_166736515" MODIFIED="1589363456010" TEXT="&#x8868;&#x91cc;&#x914d;&#x7684;cond id &#x4e4b;&#x95f4;&#x90fd;&#x662f;&#x6216;"/>
<node CREATED="1589363456525" ID="ID_118682665" MODIFIED="1589363466958" TEXT="&#x7136;&#x540e;&#x4e00;&#x4e2a;cond id&#x91cc;&#x7684;&#x662f;&#x4e0e;&#x5173;&#x7cfb;"/>
<node CREATED="1589363482930" ID="ID_1093523454" MODIFIED="1589363807952" TEXT="&#x901a;&#x7528;&#x6761;&#x4ef6;"/>
</node>
<node CREATED="1589362796671" ID="ID_728887516" MODIFIED="1589363179968" TEXT="refresh">
<node CREATED="1589363180411" ID="ID_793936274" MODIFIED="1589363182007" TEXT="auto">
<node CREATED="1589363182533" ID="ID_750325039" MODIFIED="1589363184384" TEXT="interval"/>
</node>
<node CREATED="1589363204302" ID="ID_370345353" MODIFIED="1589363205724" TEXT="manual">
<node CREATED="1589363253134" ID="ID_1027177601" MODIFIED="1589363776476" TEXT="&#x6700;&#x5927;&#x6b21;&#x6570;"/>
<node CREATED="1589363776701" ID="ID_610605175" MODIFIED="1589363782545" TEXT="&#x5237;&#x65b0;CD"/>
<node CREATED="1589363835484" ID="ID_394975541" MODIFIED="1589363839856" TEXT="&#x52a0;&#x4e2a;&#x67e5;&#x8868;"/>
</node>
</node>
<node CREATED="1589362770762" ID="ID_975649556" MODIFIED="1589362772903" TEXT="client">
<node CREATED="1589362717822" ID="ID_123975105" MODIFIED="1589362720064" TEXT="sort id"/>
<node CREATED="1589362739504" ID="ID_284943321" MODIFIED="1589362742856" TEXT="show currency"/>
</node>
</node>
<node CREATED="1589362609454" ID="ID_1058769684" MODIFIED="1589362610796" TEXT="slot">
<node CREATED="1589363950666" ID="ID_1789260679" MODIFIED="1589363963359" TEXT="goods list with weights"/>
</node>
<node CREATED="1589362611065" ID="ID_726863600" MODIFIED="1589362612114" TEXT="good">
<node CREATED="1589364176181" ID="ID_21794899" MODIFIED="1589364321325" TEXT="item">
<node CREATED="1589364190296" ID="ID_140223072" MODIFIED="1589364295777" TEXT="staticObjInfo"/>
</node>
<node CREATED="1589364190296" ID="ID_1641628950" MODIFIED="1589364260373" TEXT="consume">
<node CREATED="1589364190296" ID="ID_826706647" MODIFIED="1589364295777" TEXT="staticObjInfo"/>
</node>
<node CREATED="1589364349227" ID="ID_1840087516" MODIFIED="1589364371493" TEXT="maxNum"/>
<node CREATED="1589364449046" ID="ID_1388128818" MODIFIED="1589364464487" TEXT="discount">
<node CREATED="1589364502498" ID="ID_1980940366" MODIFIED="1589364502498" TEXT=""/>
</node>
</node>
</node>
<node CREATED="1589362623969" ID="ID_1899112984" MODIFIED="1589362626361" POSITION="left" TEXT="player">
<node CREATED="1589362626362" ID="ID_923762878" MODIFIED="1589364743661" TEXT="1:n store"/>
</node>
<node CREATED="1589364118742" ID="ID_1825803254" MODIFIED="1589364122833" POSITION="left" TEXT="&#x95ee;&#x9898;">
<node CREATED="1589364123904" ID="ID_1650667247" MODIFIED="1589364144773" TEXT="&#x5237;&#x65b0;&#x65f6;&#x673a;&#xff1f;">
<node CREATED="1589374702801" ID="ID_650904540" MODIFIED="1589376239224" TEXT="&#x5546;&#x5e97;&#x548c;&#x4e00;&#x7ea7;&#x69fd;&#x4f4d;&#x7684;&#x5237;&#x65b0;&#x8d70;&#x5173;&#x5361;&#x8fdb;&#x5ea6;"/>
<node CREATED="1589374723197" ID="ID_1895778420" MODIFIED="1589374756965" TEXT="&#x69fd;&#x4f4d;&#x5347;&#x7ea7;&#x7684;&#x5237;&#x65b0;&#xff0c;&#x53ea;&#x5728;&#x6bcf;&#x6b21;&#x81ea;&#x52a8;&#x6216;&#x624b;&#x52a8;&#x5237;&#x65b0;&#x7684;&#x65f6;&#x5019;&#x626b;&#x4e00;&#x6b21;"/>
</node>
<node CREATED="1589364626556" ID="ID_1941120428" MODIFIED="1589364652896" TEXT="slot &#x7684;&#x5f00;&#x542f;&#x6761;&#x4ef6;">
<node CREATED="1589364630680" ID="ID_1988916010" MODIFIED="1589364648094" TEXT="&#x53ef;&#x4ee5;&#x662f;&#x89c4;&#x5219;&#x5417;&#xff1f;"/>
</node>
<node CREATED="1589364520365" ID="ID_220151460" MODIFIED="1589364527228" TEXT="&#x6253;&#x6298;&#x4fe1;&#x606f;&#x5728;good&#x8fd8;&#x662f;slot&#x4e0a;&#xff1f;"/>
<node CREATED="1589364931938" ID="ID_1603069143" MODIFIED="1589364980128" TEXT="store &#x548c; slot&#x4e4b;&#x95f4;&#x6709;&#x4e00;&#x4e2a;&#x7ea6;&#x5b9a;&#x5173;&#x7cfb;"/>
<node CREATED="1589365182485" ID="ID_971400703" MODIFIED="1589365191338" TEXT="&#x4e0d;&#x540c;&#x9636;&#x6bb5;&#x5237;&#x4e0d;&#x4e0d;&#x4e00;&#x6837;&#x7684;&#x7269;&#x54c1;&#xff1f;"/>
</node>
<node CREATED="1589364590064" ID="ID_385089118" MODIFIED="1589364591886" POSITION="left" TEXT="opt">
<node CREATED="1589364592367" ID="ID_1913810349" MODIFIED="1589364594237" TEXT="buy">
<node CREATED="1589364692767" ID="ID_1053692527" MODIFIED="1589364695186" TEXT="num"/>
</node>
</node>
<node CREATED="1589422205223" ID="ID_715665703" MODIFIED="1589422210930" POSITION="left" TEXT="&#x534f;&#x8bae;">
<node CREATED="1589422677497" ID="ID_56341261" MODIFIED="1589422719410" TEXT="ManualRefreshStore"/>
<node CREATED="1589422942354" ID="ID_129994268" MODIFIED="1589422988684" TEXT="&#x81ea;&#x52a8;&#x5237;&#x65b0;">
<node CREATED="1589422989114" ID="ID_491519791" MODIFIED="1589423004998" TEXT="&#x5728;&#x767b;&#x5f55;&#x65f6;&#x548c;&#x6bcf;&#x65e5;&#x91cd;&#x7f6e;&#x7684;&#x65f6;&#x5019;&#x5224;&#x5b9a;"/>
</node>
<node CREATED="1589422730440" ID="ID_945315045" MODIFIED="1589423078803" TEXT="StoreInfoNotify">
<node CREATED="1589422772344" ID="ID_1307362768" MODIFIED="1589422799282" TEXT="&#x6574;&#x4e2a;&#x5546;&#x5e97;&#x53d8;&#x5316;&#x65f6;&#x540c;&#x6b65;"/>
</node>
<node CREATED="1589422838883" ID="ID_201781226" MODIFIED="1589422844821" TEXT="BuyGood"/>
<node CREATED="1589422900954" ID="ID_1743507323" MODIFIED="1589423061177" TEXT="ShelfNewNotify">
<node CREATED="1589423082189" ID="ID_1876561490" MODIFIED="1589423088390" TEXT="&#x5f00;&#x542f;&#x65b0;&#x8d27;&#x67b6;"/>
<node CREATED="1589423118883" ID="ID_1861776960" MODIFIED="1589423137220" TEXT="&#x4e3b;&#x7ebf;&#x6218;&#x5f79;&#x8fdb;&#x5ea6;&#x89e6;&#x53d1;"/>
</node>
<node CREATED="1589423092372" ID="ID_221208357" MODIFIED="1589423095391" TEXT="StoreNewNotify">
<node CREATED="1589423096105" ID="ID_514512546" MODIFIED="1589423104929" TEXT="&#x5f00;&#x542f;&#x65b0;&#x5546;&#x5e97;"/>
<node CREATED="1589423128822" ID="ID_1941065903" MODIFIED="1589423133921" TEXT="&#x4e3b;&#x7ebf;&#x6218;&#x5f79;&#x8fdb;&#x5ea6;&#x89e6;&#x53d1;"/>
</node>
</node>
<node CREATED="1589423285831" ID="ID_1966420377" MODIFIED="1589423288796" POSITION="left" TEXT="&#x52a8;&#x6001;&#x6570;&#x636e;">
<node CREATED="1589423289559" ID="ID_1162992528" MODIFIED="1589424221904" TEXT="player store shelf buy"/>
</node>
</node>
</map>
