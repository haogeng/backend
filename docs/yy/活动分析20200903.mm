<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1599099148492" ID="ID_795637846" MODIFIED="1599099175948" TEXT="&#x6d3b;&#x52a8;&#x5b9e;&#x65bd;20200903">
<node CREATED="1599100743578" ID="ID_400383460" MODIFIED="1599100746821" POSITION="left" TEXT="&#x7591;&#x95ee;">
<node CREATED="1599100747224" ID="ID_1453625179" MODIFIED="1599101403509" TEXT="&#x53ef;&#x9884;&#x89c8;&#x7684;&#x8bbe;&#x8ba1;&#x6682;&#x65f6;&#x53ef;&#x4ee5;&#x4e0d;&#x8981;"/>
<node CREATED="1599101405167" ID="ID_964233805" MODIFIED="1599101409881" TEXT="&#x6709;&#x7684;&#x529f;&#x80fd;&#x4e0d;&#x7528;&#x8003;&#x8651;&#x53ef;&#x9884;&#x89c8;"/>
<node CREATED="1599100759671" ID="ID_989927819" MODIFIED="1599100787442" TEXT="&#x6709;&#x4e00;&#x4e9b;&#x662f;&#x901a;&#x7528;&#x903b;&#x8f91;&#xff0c;&#x6709;&#x4e00;&#x4e9b;&#x662f;&#x4e13;&#x5c5e;&#x903b;&#x8f91;&#xff0c;&#xff0c;&#x5982;&#x679c;&#x6bcf;&#x4e2a;&#x5730;&#x65b9;&#x90fd;&#x8003;&#x8651;&#x901a;&#x7528;&#x903b;&#x8f91;&#xff0c;&#x505a;&#x4e07;&#x80fd;&#x94a5;&#x5319;&#xff0c;&#x95ee;&#x9898;&#x5c31;&#x4f1a;&#x590d;&#x6742;&#x5f88;&#x591a;&#xff01;"/>
<node CREATED="1599102810768" ID="ID_1799343546" MODIFIED="1599102811957" TEXT="&#x6d3b;&#x52a8;&#x8fc7;&#x671f;&#x540e;&#x6216;&#x6d3b;&#x52a8;&#x5f7b;&#x5e95;&#x5b8c;&#x6210;&#x540e; &#x662f;&#x5426;&#x5220;&#x9664; "/>
<node CREATED="1599102972575" ID="ID_1760509190" MODIFIED="1599102998920" TEXT="&#x65f6;&#x95f4;&#x6709;&#x65e0;&#x5c0f;&#x4e8e;&#x4e00;&#x5929;&#x7684;&#x9700;&#x6c42;&#xff1f;"/>
<node CREATED="1599103038872" ID="ID_1124234645" MODIFIED="1599103048330" TEXT="&#x5956;&#x52b1;&#x90ae;&#x4ef6;&#x53d1;&#x653e;&#xff1f;&#xff1f;&#xff1f; &#x5177;&#x4f53;&#x600e;&#x4e48;&#x8bb2;&#xff1f;"/>
</node>
<node CREATED="1599102882215" ID="ID_584241421" MODIFIED="1599102884107" POSITION="left" TEXT="&#x7ea2;&#x70b9;">
<node CREATED="1599102884361" ID="ID_354348727" MODIFIED="1599102905049" TEXT="&#x6709;&#x8bf8;&#x591a;&#x7ea2;&#x70b9;&#x903b;&#x8f91;&#xff0c;&#xff0c;&#x767b;&#x5f55;&#x65f6;&#xff0c;&#x6d3b;&#x52a8;&#x6570;&#x636e;&#x5c31;&#x9700;&#x8981;&#x53d1;&#x9001;&#x7ed9;&#x73a9;&#x5bb6;"/>
</node>
<node CREATED="1599102970838" ID="ID_496303574" MODIFIED="1599102972340" POSITION="left" TEXT="&#x7ea6;&#x675f;">
<node CREATED="1599103102314" ID="ID_1679534208" MODIFIED="1599103116472" TEXT="&#x5956;&#x52b1;&#x7c7b;&#x7684;&#xff0c;&#xff0c;&#x6709;&#x4e00;&#x4e2a;objList&#xff0c;&#xff0c;&#x53ef;&#x4ee5;&#x76f4;&#x63a5;&#x7528;&#x90a3;&#x4e2a;id"/>
</node>
<node CREATED="1599103150056" ID="ID_1310960847" MODIFIED="1599103153315" POSITION="right" TEXT="&#x5177;&#x4f53;&#x6d3b;&#x52a8;">
<node CREATED="1599099177310" ID="ID_1075958014" MODIFIED="1599099183425" TEXT="&#x4e03;&#x65e5;&#x4efb;&#x52a1;">
<node CREATED="1599101052094" ID="ID_79650046" MODIFIED="1599101058602" TEXT="&#x53ef;&#x4ee5;&#x6269;&#x5c55;&#x4e3a; x&#x65e5;&#x4efb;&#x52a1;"/>
<node CREATED="1599101059201" ID="ID_154564228" MODIFIED="1599101076094" TEXT="&#x53ef;&#x4ee5;&#x8003;&#x8651;&#x6302;&#x4e00;&#x4e2a;&#x7edf;&#x4e00;&#x8fdb;&#x5ea6;&#x5b9d;&#x7bb1;"/>
<node CREATED="1599101092681" ID="ID_499589895" MODIFIED="1599101105090" TEXT="&#x6240;&#x6709;&#x4efb;&#x52a1;&#x91cc;&#xff0c;&#x7b56;&#x5212;&#x53ef;&#x914d;&#x7f6e;&#x5956;&#x52b1;&#x4efb;&#x52a1;&#x70b9;"/>
<node CREATED="1599101109482" ID="ID_1576163344" MODIFIED="1599101337908" TEXT="&#x914d;&#x7f6e;&#x91cc;&#x9700;&#x8981;&#x77e5;&#x9053;&#x6d3b;&#x8dc3;&#x70b9;&#x9053;&#x5177;&#xff0c;&#xff0c;&#x5e76;&#x53ef;&#x914d;&#x7f6e;&#x6d3b;&#x52a8;&#x5230;&#x671f;&#x540e;&#x5220;&#x9664;&#x4e0e;&#x5426;"/>
<node CREATED="1599101382905" ID="ID_1925313979" MODIFIED="1599101389255" TEXT="&#x4efb;&#x52a1;&#x603b;&#x76ee;&#x6807;&#x7684;&#x8bbe;&#x8ba1;&#xff1f;&#xff1f;&#xff1f;"/>
</node>
<node CREATED="1599099183911" ID="ID_725711144" MODIFIED="1599099187650" TEXT="&#x4e03;&#x65e5;&#x767b;&#x5f55;">
<node CREATED="1599102747887" ID="ID_1995350007" MODIFIED="1599102758226" TEXT="&#x53ef;&#x6269;&#x5c55;&#x4e3a;x&#x65e5;&#x767b;&#x5f55;"/>
</node>
<node CREATED="1599099199439" ID="ID_1826568345" MODIFIED="1599099205005" TEXT="&#x6d3b;&#x8dc3;&#x793c;"/>
</node>
<node CREATED="1599103162161" ID="ID_330743689" MODIFIED="1599103164264" POSITION="right" TEXT="&#x6846;&#x67b6;"/>
<node CREATED="1599103169258" ID="ID_1797709886" MODIFIED="1599103169258" POSITION="right" TEXT=""/>
<node CREATED="1599103178434" ID="ID_1450016754" MODIFIED="1599103189479" POSITION="left" TEXT="&#x6846;&#x67b6;">
<node CREATED="1599099234165" ID="ID_1738189526" MODIFIED="1599099476455" TEXT="&#x5927;&#x6d3b;&#x52a8;">
<node CREATED="1599099242386" ID="ID_476535257" MODIFIED="1599099246621" TEXT="&#x542f;&#x505c;&#x7b49;&#x8bbe;&#x7f6e;"/>
<node CREATED="1599101451445" ID="ID_1028888199" MODIFIED="1599101453366" TEXT="&#x72b6;&#x6001;">
<node CREATED="1599101453949" ID="ID_1471632687" MODIFIED="1599101497557" TEXT="&#x4e09;&#x4e2a;&#x7ef4;&#x5ea6;">
<node CREATED="1599101497882" ID="ID_1620711702" MODIFIED="1599101501338" TEXT="&#x7b56;&#x5212;&#x7ef4;&#x5ea6; &#x5f00;&#x5173;"/>
<node CREATED="1599101501561" ID="ID_1721681406" MODIFIED="1599101505329" TEXT="&#x6574;&#x4f53;&#x65f6;&#x95f4; &#x5f00;&#x5173;"/>
<node CREATED="1599101505562" ID="ID_1375626177" MODIFIED="1599101520028" TEXT="&#x6e38;&#x620f;&#x5185;&#x73a9;&#x5bb6;&#x6570;&#x636e;&#x53ca;&#x4e8b;&#x4ef6;&#x89e6;&#x53d1;  &#x5f00;&#x5173;"/>
<node CREATED="1599101579771" ID="ID_164327324" MODIFIED="1599101590485" TEXT="&#x4ece;&#x4e0a;&#x5230;&#x4e0b; &#x5305;&#x542b;&#x5173;&#x7cfb;"/>
</node>
</node>
<node CREATED="1599099247832" ID="ID_182092624" MODIFIED="1599099260614" TEXT="&#x662f;&#x5426;&#x5faa;&#x73af;&#x7b49;&#x8bbe;&#x7f6e;"/>
<node CREATED="1599099762195" ID="ID_576493983" MODIFIED="1599099778268" TEXT="&#x6d3b;&#x52a8;&#x533a;&#x5206;&#x4e2a;&#x4eba;&#x6d3b;&#x52a8;&#x4ee5;&#x53ca;&#x96c6;&#x4f53;&#x6d3b;&#x52a8;&#xff0c;&#xff0c;&#x6b64;&#x5904;&#x8ba8;&#x8bba;&#x7684;&#x90fd;&#x662f;&#x4e2a;&#x4eba;&#x6d3b;&#x52a8;"/>
<node CREATED="1599099631107" ID="ID_1677294264" MODIFIED="1599100018688" TEXT="&#x6bcf;&#x4e00;&#x4e2a;&#x6d3b;&#x52a8;&#xff0c;&#x53ef;&#x80fd;&#x4f1a;&#x6709;&#x4e0b;&#x4e00;&#x4e2a;&#x4e8b;&#x4ef6;&#x7684;&#x89e6;&#x53d1;&#x65f6;&#x673a;&#xff0c;&#xff0c;&#x5ba2;&#x6237;&#x7aef;&#x9700;&#x8981;&#x52a0;&#x8c03;&#x5ea6;&#xff0c;&#x5230;&#x65f6;&#x50cf;&#x670d;&#x52a1;&#x5668;&#x53d1;&#x9001;trigger&#x4e8b;&#x4ef6;"/>
<node CREATED="1599100169993" ID="ID_1558717674" MODIFIED="1599100188035" TEXT="&#x7c7b;&#x4f3c;&#x4e8e;&#x4e00;&#x4e9b;&#x5468;&#x671f;&#x6d3b;&#x52a8;&#xff0c;&#x5168;&#x670d;&#x8d70;&#x7edf;&#x4e00;&#x65f6;&#x95f4;&#xff0c;&#x7c7b;&#x4f3c;&#x4e8e;&#x7ade;&#x6280;&#x573a;">
<node CREATED="1599100274651" ID="ID_1873712440" MODIFIED="1599100274651" TEXT=""/>
</node>
</node>
<node CREATED="1599099261963" ID="ID_1048086223" MODIFIED="1599099479171" TEXT="&#x5b50;&#x6d3b;&#x52a8;">
<node CREATED="1599099263868" ID="ID_1048571179" MODIFIED="1599099485603" TEXT="&#x542f;&#x505c;&#x4f9d;&#x9644;&#x4e8e;&#x5927;&#x6d3b;&#x52a8;"/>
<node CREATED="1599099521361" ID="ID_1021246370" MODIFIED="1599099529976" TEXT="&#x53ef;&#x4ee5;&#x662f;&#x7279;&#x6b8a;&#x903b;&#x8f91;"/>
<node CREATED="1599099281853" ID="ID_7747922" MODIFIED="1599099288834" TEXT="&#x6309;&#x4e00;&#x5b9a;&#x903b;&#x8f91;&#x9010;&#x4e2a;&#x5f00;&#x542f;">
<node CREATED="1599099386091" ID="ID_1946996301" MODIFIED="1599099392288" TEXT="&#x4e8b;&#x4ef6;&#x89e6;&#x53d1;&#x578b;">
<node CREATED="1599099512122" ID="ID_120993041" MODIFIED="1599101992935" TEXT="&#x524d;&#x9a71;&#x5b8c;&#x6210;&#x5f00;&#x59cb;&#x4e0b;&#x4e00;&#x4e2a;"/>
<node CREATED="1599102126915" ID="ID_9725784" MODIFIED="1599102138921" TEXT="&#x6682;&#x65f6;&#x652f;&#x6301;&#x767b;&#x5f55;&#x9886;&#x5956;&#x7c7b;"/>
</node>
<node CREATED="1599099392508" ID="ID_1442498677" MODIFIED="1599099402367" TEXT="&#x65f6;&#x95f4;&#x89e6;&#x53d1;&#x578b;">
<node CREATED="1599099502347" ID="ID_62772283" MODIFIED="1599099510266" TEXT="&#x5ef6;&#x8fdf;&#x591a;&#x4e45;"/>
</node>
</node>
</node>
</node>
</node>
</map>
