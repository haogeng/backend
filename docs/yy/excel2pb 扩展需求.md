## excel2pb 复杂类型扩展需求

* ## 支持复杂map/slice
> 以下以map为例

单元格内容如下(策划输出的json内容)：

```
{
    "0": {
        "type": 1,
        "id": 1001,
        "count": 1,
        "param": 0
    },
    "1": {
        "type": 1,
        "id": 1002,
        "count": 1,
        "param": 0
    }
}

```
单元格类型可指定为 map[string]*msg.StaticObjInfo
msg.StaticObjInfo 相关proto定义为： 

```
message StaticObjInfo {
    // configId
    int32 id = 1;
    // ObjType
    int32 type = 2;
    // 数量需要是64位的，以防万一
    int64 count = 3;
    int32 level = 4;
    int32 star = 5;
    // 文明
    int32 civil = 6;
    int32 param = 7;
}
```
