# STATELESS服务架构备忘V3

***
### 设计约束
> * 可线性扩展，分布式
> * 无状态
> * 单个玩家的处理通过排他访问和请求排队进行串行化处理
> * 多个玩家的共享数据，通过微服务路由串行化或者分布式锁处理

***
### 关键概念
* **seqId:** sequence id。每个req协议里和res协议里携带(配对)，用于消息的有序化，以及日志追踪。形如xxx-%id%。xxx可以是更有意义的，比如pid-tokenMd5，%id%是顺序号。客户端只有真正收到了上一个处理，才会递增seqId。如果超时没收到，则重传req。(因此服务器需要缓存seqId对应的处理结果)
目前方案通过metadata传送seq_num, 如果无，则为0. 服务器在处理成功后会回传客户端发上来的seq_num.
**底层passthrough代替了seqid**
* **token:** jwt生成token,同时生成md5，缓存redis。后续通过md5比对。需要在token check时更新下md5，避免过期。(假设md5真过期了，是否token失效，根据业务定)。每次请求时，http头携带token信息。
* **redis缓存:** 
    * pid -> {seqId, tokenMd5} (10min)
    * seqId -> {response} (5s)
    * pid -> {playerMySQLData} (**TODO**)
* **cors**：手游简化，不存在该问题，客户端不用关心http.origin。
* **msg header**: 每个协议加协议头header，包括{seqId, sign(可选)}.

***
### 角色定义：
* a: a用户 
    * a1 设备1上的a
    * a2 设备2上的a
* b: b用户
* clan: 公会
* PS: player_server 玩家业务服，多点线性扩展，配合lbs
* CS: clan_server 公会服，暂时单点，可扩展为多点，配合etcd等

***
* #### logic of one player in single-device
```mermaid
sequenceDiagram

b->>PS: auth
PS-->>b: token

loop req&res
    b->>PS: reqX(token, sequence_id)
    PS->>PS: check token
    PS->>PS: check sequence_id (req MUST be sequence!)
    PS->>PS: if cached(sequence_id) then use cache data
    PS->>PS: if nocached then do logic (MUST stateless) and cache(5s)
    PS->>b: resX(err)
    b->>b: add sequence_id (or use server passthrough data)
end
```

***
* #### logic of one player in multi-device
```mermaid
sequenceDiagram

a1->>PS: auth
PS->>a1: token1

a2->>PS: auth
PS->>PS: make token2(reset seq_id) && invalid token1
a1-->>a1: token1 invalid (lazy)
PS->>a2: token2

a1->>PS: reqX(token1)
PS->>a1: failed cause token1 invalid

```

***
* #### logic of clan
> * token验证同上，此处简化模型暂时忽略
> * clan 逻辑的可选方式： 不开微服务，使用redis分布式锁
```mermaid
sequenceDiagram

b->>PS: reqAddClanVal(clanID=3)

opt discovery
    PS->>etcd: getClanServerByID(3)
    etcd->>PS: clanServerInfo
end

PS->>CS: rpcCallAddVal(3)
CS->>CS: do logic(one goroutine per clan)
CS->>PS: rpsRes

PS->>b: res()

```

***
* #### redis缓存机制
> **设计约束：**
>
> * 此处讨论的是单个玩家的数据。多玩家共享数据需额外加一层分布式锁。
* ##### 读玩家数据
```mermaid 
graph LR 
A(访问者) -->|getById| R(GetFromRedis) 
R --> G{Gotted} 
G --> |Yes| UR(UpateRedisExpireTime) 
G --> |No| M(GetFromMySQL) 
M --> S(StoreToRedis) 
S --> RD(ReturnData) 
UR --> RD 
```
* ##### 写玩家数据 
```mermaid 
graph LR 
A(访问者) -->|writeById| WM(WriteMySQL) 
WM --> WR(StoreToRedis) 

```

***
* #### dbproxy
1. 沿用之前公司内部dbproxy技术 (参考[https://github.com/highras/dbproxy](https://github.com/highras/dbproxy))
2. 分库分表使用hintid默认按主键，如果有需求可以自定义特定表的hintid
3. 通过配置启用dbproxy，以及路由策略，暂时首选hash方式，需要提前预备预计的多库(后期不能再新增库)
4. TODO migrate需要支持dbproxy

***
* #### Rate Limit
1. 虽然业务分布式，但限流可以做成根据uid的单点限流，粗略的限流即可 (token bucket | leak bucket)
2. 更进一步的方案，采用redis 分布式限流，使用滑动窗口，
    >参考
    >[https://github.com/imtoori/gin-redis-ip-limiter](https://github.com/imtoori/gin-redis-ip-limiter)
    >[https://github.com/didip/tollbooth](https://github.com/didip/tollbooth)

***
* #### 单个玩家业务的并行判定
理论上不允许单个userId的玩家的业务并行提交。客户端通过串行提交请求做初步处理，同时服务器也做相应的容错处理，对于单个玩家的同一个请求号，在redis缓存中做了分布式锁。处理完成后或超时到了释放锁。在check sequence中判定分布式锁，决定是否Next或Abort。