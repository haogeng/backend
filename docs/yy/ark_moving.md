#### ark迁移备忘

* change gin.Context to context.Context
直接大招，查找替换

* golib/router/ -> sandwich/
* 调整handlers/http.go
```golang
package handlers

import (
	ark_middleware "bitbucket.org/funplus/arkmid"
	"errors"
	"fmt"
	"server/internal"
	"server/internal/log"

	"server/internal/handlers/internal/example"
	"server/internal/handlers/internal/game"
	"server/internal/handlers/internal/local_test"
	"server/internal/handlers/internal/login"
	"server/pkg/fungin/xmiddleware"

	"bitbucket.org/funplus/ark"
	"bitbucket.org/funplus/arkmid/bodylimit"
	"bitbucket.org/funplus/arkmid/recover"
	"bitbucket.org/funplus/sandwich"
	"bitbucket.org/funplus/sandwich/current"
)

func panicCatcher(ctx context.Context, err error) error {
	log.Errorf("panicCatcher: %v", err)
	errToC := fmt.Errorf("server internal panic")
	_ = sandwich.SendWithArkContext(ctx, errToC)
	return err
}

func arkErrorCatcher() ark.MiddlewareFunc {
	return func(next ark.HandlerFunc) ark.HandlerFunc {
		return func(ctx context.Context) error {
			if err := next(ctx); err != nil {
				log.Errorf("ErrorCatcher: %v", err)
				return sandwich.SendWithArkContext(ctx, err)
			}
			return nil
		}
	}
}

//func arkPermissionCheck(tokenKey string) ark.MiddlewareFunc {
//	return func(next ark.HandlerFunc) ark.HandlerFunc {
//		return func(ctx context.Context) error {
//			ts := ctx.GetHeader(tokenKey)
//			if len(ts) == 0 {
//				return errors.New("token empty")
//			}
//			token := &internal.GameAccessToken{}
//			b, err := base64.StdEncoding.DecodeString(ts)
//			if err != nil {
//				return err
//			}
//			err = json.Unmarshal(b, token)
//			if err != nil {
//				return err
//			}
//			ctx.Set("token", token)
//			return next(ctx)
//		}
//	}
//}

func InitHttpRouter(engine *ark.Ark) {
	c := internal.Config
	engine.Use(
		internal.ArkAccessLog(ark_middleware.DefaultSkipper),

		recover.New(recover.WithPanicCatcher(panicCatcher)),

		bodylimit.New(bodylimit.WithBodyLimit(4*1024*1024)),

		arkErrorCatcher(),

		current.InstallCurrent(),

		// 限流器
		xmiddleware.ReqCountRateLimit(c.RateLimitConfig.MaxCount, c.RateLimitConfig.FillAllDuration, true, true),

		// 检查内部redis，db等连接状态
		xmiddleware.CheckInternalCon(),
	)

	engine.SetNoRouter(func(ctx context.Context) error {
		err := errors.New("router not found")
		_ = sandwich.SendWithArkContext(ctx, err)
		return err
	})
	engine.SetMethodNotAllowed(func(ctx context.Context) error {
		err := errors.New("method not allowed")
		_ = sandwich.SendWithArkContext(ctx, err)
		return err
	})

	// Register micro server into api group
	apiGroup := engine.Group("api")
	{
		local_test.InitHttpRouter(apiGroup)

		example.InitHttpRouter(apiGroup)
		login.InitHttpRouter(apiGroup)
		game.InitHttpRouter(apiGroup)

		//game.InitHttpRouter(apiGroup)
		// ...
	}
}

```

* 调整game/router.go 
```golang
package game

import (
	"bitbucket.org/funplus/ark"
	"bitbucket.org/funplus/arkmid/auth"
	"github.com/dgrijalva/jwt-go"
	internal2 "server/internal"
	"server/pkg/fungin/xmiddleware"
	"server/pkg/gen/service"
)

type HandlerHttp struct{}

const (
	KGroupName string = "game"
)

func InitHttpRouter(group ark.Router) {
	apiGroup := group.Group(KGroupName)
	apiGroup.Use(
		//解析token，struct存储到context
		auth.NewParser(auth.WithSigningKeys(xmiddleware.AuthConfigSigningKey),
			auth.WithSigningMethod(jwt.SigningMethodHS256),
			auth.WithKeyTokenInContext(xmiddleware.AuthKeyTokenFlag),
			auth.WithKeyClaimInContext(xmiddleware.AuthKeyClaimFlag),
			auth.WithKeyTokenInHeader(xmiddleware.AuthKeyTokenInHeader),
			auth.WithAuthScheme(""),
			auth.WithClaims(&xmiddleware.TokenClaims{})),

		xmiddleware.CheckToken(internal2.Config.TokenTtl),

		xmiddleware.ArkUnmarshal(),

		// 逻辑层面，用户绑定context
		xmiddleware.BindCtxToUser(),

		//解析request id，字符串存储到context
		xmiddleware.ParseRequestId(),
		//当前request是否被请求并缓存response
		xmiddleware.CheckResponseCached(xmiddleware.NewRedisResponseCache(internal2.Config.ResponseTtl)),
		//检测请求时序是否ok
		xmiddleware.CheckSequence(xmiddleware.NewRedisRequestSequencer(internal2.Config.SequenceIdTtl, internal2.Config.SequenceDealWithLock)),
		// No need echo cause check sequence has added echo
		//middleware.EchoSequence(),
	)

	{
		service.RegisterGameServiceHttpHandler(apiGroup, &HandlerHttp{})
	}
}

```
* kill gogo proto which is not compatible with protobuf 1.4.0

* excel2pb check out to feature-sandwich branch
* go.mod ref 主要是golib 需要时feature-sandwich版本
```
bitbucket.org/funplus/ark v0.0.0-20200428081930-d38119dc4ee5
	bitbucket.org/funplus/arkmid v0.0.0-20200429033955-102e5d84e172
	bitbucket.org/funplus/golib v0.1.8 // using feature-sandwich
	bitbucket.org/funplus/sandwich v0.0.0-20200428030526-4d88db474bb9
```

* jwt auth 参考
    * 生成token
```golang
tokenClaims := &xmiddleware.TokenClaims{
		StandardClaims: jwt.StandardClaims{
			// this is token uuid!
			Id: fmt.Sprintf("%d", time.Now().Unix()),
		},
		UserId: accountRecord.LastPlayerId,
	}
	accessToken, err := auth.GetToken(jwt.SigningMethodHS256, tokenClaims, xmiddleware.AuthConfigSigningKey)
	if err != nil {
		return
	}
	xmiddleware.StoreTokenUuid(tokenClaims)
```