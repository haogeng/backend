# cgo cross build

## 需求
1. 目标环境: alpine docker
2. 在mac或centos上编译
3. 支持cgo

## 方案
1. 指定编译器为musl gcc 
    ```
    CC=x86_64-linux-musl-gcc GOOS="${OS}" GOARCH="${ARCH}" CGO_ENABLED=1 go build ...
    ```
    1. mac 
    ```
    CC=x86_64-linux-musl-gcc
   ```
    2. linux
    ```
    ln -s /usr/local/musl/bin/musl-gcc /usr/local/bin/x86_64-linux-musl-gcc
    CC=/usr/local/musl/bin/musl-gcc
    ```
2. mac
    ```bash
    brew install FiloSottile/musl-cross/musl-cross
    ```
3. centos
    ```bash
    curl -LSs https://www.musl-libc.org/releases/musl-1.1.21.tar.gz -o musl.tar.gz
    tar -xvf musl.tar.gz 
    cd musl-1.1.21/
    ./configure 
    make
    sudo make install
    cd ..
    rm -rf musl-1.1.21/
    ls /usr/local/musl/bin/musl-gcc
    ```