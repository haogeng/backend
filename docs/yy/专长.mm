<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1597201503038" ID="ID_1480278688" MODIFIED="1597201509544" TEXT="&#x4e13;&#x957f;">
<node CREATED="1597201510007" ID="ID_605389852" MODIFIED="1597201520912" POSITION="right" TEXT="&#x6570;&#x636e;">
<node CREATED="1597201521474" ID="ID_1938589237" MODIFIED="1597213957408" TEXT="pairs">
<node CREATED="1597201834294" ID="ID_115895514" MODIFIED="1597220228385" TEXT="id-&gt;level">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1597202928899" ID="ID_1742257104" MODIFIED="1597213947838" TEXT="&#x83b7;&#x5f97;&#x82f1;&#x96c4;&#x65f6; &#x521d;&#x59cb;&#x5316;">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1597212945745" ID="ID_363538526" MODIFIED="1597220228385" TEXT="&#x8001;&#x6570;&#x636e;&#x91cc; &#x5df2;&#x6709;&#x82f1;&#x96c4;&#x4e0d;&#x505a;&#x5904;&#x7406;">
<icon BUILTIN="button_ok"/>
</node>
</node>
</node>
<node CREATED="1597202062623" ID="ID_78349327" MODIFIED="1597202065326" POSITION="right" TEXT="&#x64cd;&#x4f5c;">
<node CREATED="1597202065683" ID="ID_841954192" MODIFIED="1597217078383" TEXT="&#x5347;&#x7ea7;&#x4e13;&#x957f;">
<icon BUILTIN="button_ok"/>
<node CREATED="1597202085407" ID="ID_347580491" MODIFIED="1597217078383" TEXT="&#x6218;&#x529b;&#x5237;&#x65b0;">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1597214785503" ID="ID_1308471056" MODIFIED="1597217078382" TEXT="&#x6700;&#x5927;&#x7b49;&#x7ea7;&#x68c0;&#x6d4b;">
<icon BUILTIN="button_ok"/>
<node CREATED="1597214788444" ID="ID_1726871115" MODIFIED="1597217078381" TEXT="&#x5efa;&#x7b51;&#x51b3;&#x5b9a;">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1597215022650" ID="ID_1142763720" MODIFIED="1597217078381" TEXT="special &#x8bb0;&#x5f55;&#x4e5f;&#x51b3;&#x5b9a;">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1597215030934" ID="ID_1563717395" MODIFIED="1597217078379" TEXT="&#x4e8c;&#x8005;&#x5c0f;&#x503c;">
<icon BUILTIN="button_ok"/>
</node>
</node>
<node CREATED="1597202141582" ID="ID_1902669700" MODIFIED="1597217078382" TEXT="&#x6761;&#x4ef6;">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1597202143578" ID="ID_393744083" MODIFIED="1597217078382" TEXT="&#x6750;&#x6599;&#x6d88;&#x8017;">
<icon BUILTIN="button_ok"/>
</node>
</node>
<node CREATED="1597202094735" ID="ID_955210009" MODIFIED="1597220228383" TEXT="&#x9500;&#x6bc1;&#x82f1;&#x96c4;&#x65f6;&#x7684;&#x4e13;&#x957f;&#x91cd;&#x7f6e;">
<icon BUILTIN="button_ok"/>
<node CREATED="1597217753174" ID="ID_1518962890" MODIFIED="1597220228384" TEXT="&#x8865;&#x507f;cost">
<icon BUILTIN="button_ok"/>
</node>
</node>
</node>
</node>
</map>
