<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1599026917507" ID="ID_1150042758" MODIFIED="1599026928385" TEXT="&#x6d3b;&#x52a8;20200902">
<node CREATED="1599026950362" ID="ID_41143277" MODIFIED="1599027024805" POSITION="right" TEXT="&#x57fa;&#x7840;&#x529f;&#x80fd;-&#x6846;&#x67b6;">
<node CREATED="1599027487054" ID="ID_715539944" MODIFIED="1599027490660" TEXT="&#x5f00;"/>
<node CREATED="1599027490899" ID="ID_1061548514" MODIFIED="1599027492187" TEXT="&#x5173;">
<node CREATED="1599028067507" ID="ID_1633942495" MODIFIED="1599028069513" TEXT="&#x7ed3;&#x675f;&#x65f6;&#x95f4;"/>
</node>
<node CREATED="1599027492452" ID="ID_254855518" MODIFIED="1599027498757" TEXT="&#x53d1;&#x5956;&#x52b1;"/>
<node CREATED="1599027503164" ID="ID_1819688915" MODIFIED="1599027512782" TEXT="&#x662f;&#x5426;&#x5faa;&#x73af;"/>
</node>
<node CREATED="1599026957076" ID="ID_94672395" MODIFIED="1599027043134" POSITION="right" TEXT="&#x7279;&#x5b9a;&#x6d3b;&#x52a8;">
<node CREATED="1599026961606" ID="ID_1139978075" MODIFIED="1599027564833" TEXT="&#x4e03;&#x65e5;&#x76ee;&#x6807;">
<node CREATED="1599027565008" ID="ID_1583269549" MODIFIED="1599027567066" TEXT="&#x65b0;&#x624b;&#x4efb;&#x52a1;">
<node CREATED="1599027678717" ID="ID_1907273436" MODIFIED="1599027683991" TEXT="&#x4efb;&#x52a1;&#x540c;&#x5df2;&#x6709;AllTask"/>
</node>
<node CREATED="1599027727249" ID="ID_760152538" MODIFIED="1599029842022" TEXT="&#x6bcf;&#x65e5;&#x89e3;&#x9501;&#xff1f;">
<icon BUILTIN="help"/>
<node CREATED="1599027757234" ID="ID_1283315866" MODIFIED="1599027769775" TEXT="&#x662f;&#x5fc5;&#x987b;&#x4e00;&#x5929;&#x5929;&#x89e3;&#x9501;&#xff1f;&#x6bd4;&#x5982;2&#x5929;&#x6ca1;&#x73a9;"/>
<node CREATED="1599027790593" ID="ID_1498550928" MODIFIED="1599027798097" TEXT="&#x6bcf;&#x65e5;&#x91cd;&#x7f6e;&#x7684;&#x65f6;&#x5019;&#x5237;&#x65b0;&#x4efb;&#x52a1;"/>
</node>
<node CREATED="1599027907456" ID="ID_467652791" MODIFIED="1599027909650" TEXT="&#xf06e;&#x540c;&#x4e00;&#x6761;&#x4ef6;&#x4efb;&#x52a1;&#x662f;&#x5426;&#x591a;&#x8f6e;&#xff0c;&#x5728;&#x4efb;&#x52a1;&#x8868;&#x4e2d;&#x914d;&#x7f6e;&#xff1f;"/>
<node CREATED="1599027966139" ID="ID_1290767734" MODIFIED="1599027966695" TEXT="&#xf06e;&#x662f;&#x5426;&#x6709;&#x6d3b;&#x52a8;&#x603b;&#x76ee;&#x6807;&#x53ca;&#x5956;&#x52b1;&#xff0c;&#x53ef;&#x914d;&#x7f6e;&#xff1f;"/>
</node>
<node CREATED="1599027568567" ID="ID_786213780" MODIFIED="1599027579037" TEXT="&#x4e03;&#x65e5;&#x767b;&#x5f55;&#x7b7e;&#x5230;"/>
<node CREATED="1599030860732" ID="ID_301671041" MODIFIED="1599030873020" TEXT="&#x5f00;&#x670d;&#x6d3b;&#x8dc3;&#x793c;&#xff1f; &#x6709;&#x4e2a;&#x503e;&#x5411;&#x6027;"/>
<node CREATED="1599027581281" ID="ID_971089727" MODIFIED="1599027585711" TEXT="&#x6d3b;&#x8dc3;&#x9886;&#x5956;"/>
</node>
<node CREATED="1599029753997" ID="ID_1955288118" MODIFIED="1599029758350" POSITION="right" TEXT="&#x7591;&#x95ee;&#xff1f;">
<node CREATED="1599029758350" ID="ID_1189131873" MODIFIED="1599029842021" TEXT="&#x5bf9;&#x4e8e;7&#x65e5;&#x4efb;&#x52a1;&#xff0c;&#x6709;&#x5fc5;&#x8981;&#x9488;&#x5bf9;&#x6bcf;&#x65e5;&#x518d;&#x8bbe;&#x7f6e;&#x8d85;&#x65f6;&#x5417;&#xff1f;">
<icon BUILTIN="help"/>
</node>
<node CREATED="1599030547587" ID="ID_376510816" MODIFIED="1599030632432" TEXT="7&#x65e5;&#x767b;&#x5f55; &#x5929;&#x6570;&#x662f;&#x7b7e;&#x4e00;&#x6b21;&#x540e;&#x8ba1;&#x65f6;&#xff1f;&#x8fd8;&#x662f;&#x4e00;&#x76f4;&#x8ba1;&#x65f6;&#xff1f;&#x8fd8;&#x662f;&#x8bf4;&#x767b;&#x5f55;&#x7684;&#x65f6;&#x5019;&#x5237;&#x4e00;&#x4e0b;&#x8ba1;&#x65f6;&#xff1f;"/>
</node>
<node CREATED="1599027481968" ID="ID_1652953917" MODIFIED="1599027529821" POSITION="left" TEXT="&#x533a;&#x5206;&#x4e2a;&#x4eba;&#x548c;&#x5168;&#x670d;&#xff1f;"/>
<node CREATED="1599029433467" ID="ID_161061007" MODIFIED="1599029441161" POSITION="left" TEXT="&#x6d3b;&#x52a8;&#x9884;&#x89c8;&#x7684;&#x9700;&#x6c42;&#xff1f;">
<node CREATED="1599029449079" ID="ID_304833044" MODIFIED="1599029456401" TEXT="&#x7c7b;&#x4f3c;&#x4e8e;&#x65e5;&#x5e38;/&#x5468;&#x5e38;&#x7684;0&#x7ea7;&#x6d3b;&#x52a8;"/>
</node>
<node CREATED="1599029457389" ID="ID_1928472006" MODIFIED="1599029460412" POSITION="left" TEXT="&#x4efb;&#x52a1;&#x5907;&#x5fd8;">
<node CREATED="1599029460735" ID="ID_255740036" MODIFIED="1599029474541" TEXT="1 &#x53ea;&#x7ef4;&#x62a4;&#x4efb;&#x52a1;&#x8fdb;&#x5ea6;&#x6570;+&#x7b49;&#x7ea7;"/>
<node CREATED="1599029475494" ID="ID_1821610523" MODIFIED="1599029619797" TEXT="2 &#x662f;&#x5426;&#x6fc0;&#x6d3b;&#xff1a; &#x67e5;&#x770b;&#x914d;&#x8868;&#x6709;&#x65e0;&#x5f53;&#x524d;&#x7b49;&#x7ea7;&#x7684;&#x6570;&#x636e; level &gt; 0 &amp;&amp; level&lt;confMaxLevel"/>
<node CREATED="1599029488033" ID="ID_740322738" MODIFIED="1599029526767" TEXT="3 &#x4efb;&#x52a1;&#x5956;&#x52b1;&#x9886;&#x53d6;&#x540e;&#x7b49;&#x7ea7;&#x81ea;&#x52a8;+1"/>
<node CREATED="1599029498969" ID="ID_1959769176" MODIFIED="1599029549325" TEXT="4 &#x5982;&#x679c;&#x7b49;&#x7ea7;&#x4e3a;0&#xff0c;&#x5219;&#x4e00;&#x822c;&#x4e3a;&#x9884;&#x89c8;&#x4efb;&#x52a1;"/>
<node CREATED="1599029554900" ID="ID_56452543" MODIFIED="1599029564701" TEXT="5 &#x6fc0;&#x6d3b;&#x7684;&#x6709;&#x6548;&#x7684;&#x4efb;&#x52a1;&#xff0c;&#x4e0a;&#x6765;&#x7b49;&#x7ea7;&#x5c31;&#x662f;1"/>
<node CREATED="1599029565312" ID="ID_1846411202" MODIFIED="1599029583623" TEXT="6 &#x4efb;&#x52a1;&#x5f53;&#x524d;&#x7b49;&#x7ea7;&gt;&#x914d;&#x8868;&#x7b49;&#x7ea7;&#xff0c;&#x5219;&#x4e3a;&#x5df2;&#x5220;&#x9664;&#x7684;&#x4efb;&#x52a1;"/>
</node>
<node CREATED="1599098633649" ID="ID_313139903" MODIFIED="1599098795044" POSITION="left" TEXT="&#x5468;&#x5faa;&#x73af;&#x7684;&#x6d3b;&#x52a8;&#xff0c;&#x8fdb;&#x884c;&#x53cd;&#x5411;&#x67e5;&#xff0c;&#xff0c;&#xff0c;&#x6d3b;&#x52a8;&#x4e0d;&#x77e5;&#x9053;&#x529f;&#x80fd;&#xff0c;&#x529f;&#x80fd;&#x53bb;&#x67e5;&#x6d3b;&#x52a8;&#x7684;&#x65f6;&#x95f4;&#x662f;&#x5426;&#x5f00;&#x542f;&#xff1f;"/>
</node>
</map>
