#### GMX资源更新机制V2
---
* 版本约定
    版本号为main.minor.patch.buildinfo;（buildinfo可以是时间戳svn版本等等）
    区分强制更新和非强制更新。
    服务器配置中加一个向上兼容的客户端main.minor版本号信息。小于此版本的需要强制更新。
    二进制版本binVersion对应修改main.minor；
    资源版本resVersion对应修改patch.timestamp。
   
* 平台约定
    资源需要区分android/ios两个平台。需要单独目录
    
* 环境约定
    * dev
    * tf(testflight)
    * ol(online)
    online设定为不直接操作，都是通过tf测试后，直接sync过去。
    类似的环境可以很容易扩展，比如新增一个tf3。
    
    pmt工具在操作的时候需要指定依赖环境以及大版本。
    输入目录约定如下：
```
├── 0.1
│   ├── dev
│   │   └── conf
│   └── tf
│       └── conf
└── 0.2
    ├── dev
    │   └── conf
    └── tf
        └── conf
```

输入目录为conf，是对应的策划excel以及程序用的proto定义等，就是当前目录里的conf（额外需要加excel文件）。conf的输出为通过protokit先生成对应的rawdata，直接同步到后端。这个rawdata只有后端使用。
在pmt中通过单独的按钮针对特定的环境生成，并自动svn up & svn ci。此部分不需要同步到客户端。

考虑大部分资源都会伴随assetbundle或图片的变化，客户端的相关资源，，每次都需要jenkins build。pmt中提供一个pull按钮主动拉去jenkins build的信息，然后将version.txt推送到rawdata目录，将对应其他资源推送到cdn目录。

如果不走全量更新，那么pmt需要提供一个通过jenkins build生成增量更新的工具。

* 资源实时更新
    客户端如果需要及时知道版本更新信息，发的协议htt-header头中需要携带版本信息。服务器在每一个消息中检查版本信息。
    
* 资源分类
    * client
    * conf -> rawdata

* PMT build及上传资源流程 (适用于dev & tf)
```mermaid
sequenceDiagram
admin->>admin: prepare conf
admin->>pmt: select binVersion & env
admin->>pmt: build (call protokit to make conf rawdata & make reslist.json) and svn ci 
admin->>admin: prepare client jenkins build
admin->>admin: prepare client res path (include res.json & version.txt)
admin->>pmt: call push 2 cdn
pmt->>cdn: sync client res path & res.json
pmt->>game server: sync version.txt
admin->>pmt: test get from cdn
admin->>pmt: hot reload server
pmt->>game server: hot reload
```

* PMT online更新流程:
```mermaid
sequenceDiagram
admin->>admin: broadcast and so on..
admin->>pmt: sync from tf to ol
admin->>admin: hot reload server sometime...
```

* 客户端资源更新时序
```mermaid
sequenceDiagram

client->>game: req res ver num
game->>client: res version num
client->>client: check version info & diff resfile & cal update list
client-->client: select available cdn
client->>cdn: update res...
client->>s3: update res if cdn failed...
client->>client: store newversion code in version.txt if all is uploaded
```
