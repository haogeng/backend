package determinproto

import (
	"github.com/golang/protobuf/proto"
	"hash/crc32"
	_ "hash/crc32"
	"server/pkg/gen/msg"
	"strings"
	"testing"
)
import "github.com/stretchr/testify/assert"

type obj struct {
	D map[string]string
}

func TestDeterminant(t *testing.T) {
	o := &msg.PlayerInfo{
		Currency: map[int32]int64{1: 1, 2: 2}, //nil,//map[int32]int64{1: 1, 2: 2, 3: 3},
	}

	b0, err := Marshal(o)
	assert.Nil(t, err)

	c1 := crc32.ChecksumIEEE(b0)

	for i := 0; i < 100; i++ {
		b, err := Marshal(o)
		assert.Nil(t, err)
		assert.EqualValues(t, b, b0)
		//
		c2 := crc32.ChecksumIEEE(b)
		assert.Equal(t, c2, c1)
	}
}

func TestMarshal(t *testing.T) {
	o := &msg.PlayerInfo{
		Currency: map[int32]int64{1: 1, 2: 2}, //nil,//map[int32]int64{1: 1, 2: 2, 3: 3},
	}
	b, err := Marshal(o)
	assert.Nil(t, err)

	var o2 = &msg.PlayerInfo{}
	err = Unmarshal(b, o2)
	assert.Nil(t, err)

	b1, err := Marshal(o)
	b2, err := Marshal(o2)

	assert.EqualValues(t, b1, b2)
}

// proto 性能近似于msgpack，比msgpack稍低一点
// gob性能 1:10 msgpack
func BenchmarkProto(b *testing.B) {
	o := &msg.PlayerInfo{
		Name:     strings.Repeat("a", 1024*1),
		Currency: map[int32]int64{1: 1, 2: 2}, //nil,//map[int32]int64{1: 1, 2: 2, 3: 3},
	}

	for i := 0; i < b.N; i++ {
		data, err := proto.Marshal(o)
		assert.Nil(b, err)

		var o2 = &msg.PlayerInfo{}
		err = proto.Unmarshal(data, o2)
		assert.Nil(b, err)
	}
}

func BenchmarkMarshal(b *testing.B) {
	o := &msg.PlayerInfo{
		Name:     strings.Repeat("a", 1024*1),
		Currency: map[int32]int64{1: 1, 2: 2}, //nil,//map[int32]int64{1: 1, 2: 2, 3: 3},
	}
	for i := 0; i < b.N; i++ {
		data, err := Marshal(o)
		assert.Nil(b, err)

		var o2 = &msg.PlayerInfo{}
		err = Unmarshal(data, o2)
		assert.Nil(b, err)
	}
}
