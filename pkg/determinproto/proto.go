package determinproto

import (
	"github.com/golang/protobuf/proto"
)

// msgpack 只支持以下map的序列化，改为proto
//  - map[string]string
//  - map[string]interface{}

// 潜在问题：对Proto3生成的protobuf XXX_sizecache可能处理有问题，，可能无所谓
// map 稳定序列序列化
func Marshal(m proto.Message) ([]byte, error) {
	buf := proto.NewBuffer(nil)
	buf.SetDeterministic(true)
	err := buf.Marshal(m)
	if err != nil {
		return nil, err
	}
	return buf.Bytes(), err
}

func Unmarshal(data []byte, m proto.Message) error {
	return proto.Unmarshal(data, m)
}
