// gpclient 封装用于基于gin的protobuf协议，，兼容funplus netpacket框架
package gpclient

import (
	"bitbucket.org/funplus/golib/encoding2/protobuf"
	"bitbucket.org/funplus/sandwich"
	"bitbucket.org/funplus/sandwich/message"
	metadata2 "bitbucket.org/funplus/sandwich/metadata"
	"bitbucket.org/funplus/sandwich/protocol/netutils"
	"bytes"
	"encoding/json"
	"fmt"
	proto "github.com/gogo/protobuf/proto"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
)

var (
	AppVersion = ""
	Platform   = ""
)

func setReqHeader(req *http.Request) {
	req.Header.Set("Content-Type", "application/x-protobuf")

	token := "TODO: token"
	req.Header.Set("x-fun-user-token", token)
	req.Header.Set("charset", "utf-8")

	if len(AppVersion) > 0 {
		req.Header.Set("x-fun-version", AppVersion)
	}

	if len(Platform) > 0 {
		req.Header.Set("x-fun-platform", Platform)
	}
}

func ReqProto(url string, req proto.Message, seqNum int) (ret []proto.Message) {
	return ReqProtoWithHeader(url, req, seqNum, nil)
}

// 只能做简单测试，，或者内部加序列号和token这种忽略判定的标记
func ReqProtoWithHeader(url string, req proto.Message, seqNum int, headers map[string]string) (ret []proto.Message) {
	ret = nil

	fmt.Printf("\n----------------------------------------")
	fmt.Printf("\nReqUrl:\t%s\n", url)
	//reqJsonByte, _ := json.MarshalIndent(req, "", "\t")
	reqJsonByte, _ := json.Marshal(req)
	fmt.Printf("\n----ReqProto[%T]:\t%+v\n\n", req, string(reqJsonByte))

	var reqBody = PbToReqBody(seqNum, req)

	client := &http.Client{}
	httpReq, err := http.NewRequest("GET", url, bytes.NewReader(reqBody))
	if err != nil {
		log.Fatal(err)
	}
	// Set Header
	setReqHeader(httpReq)
	if headers != nil {
		for k, v := range headers {
			httpReq.Header.Set(k, v)
		}
	}
	//fmt.Printf("Send header:%v\n\n", httpReq.Header)

	resp, err := client.Do(httpReq)
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()

	respBody, err := ioutil.ReadAll(resp.Body)
	//log.Printf("respBody:%+s\n", respBody)
	log.Printf("resp:%v\n", resp)
	log.Printf("Content-Length:%d\tRecvLen:%d\n", resp.ContentLength, len(respBody))

	rawPacket := &netutils.RawPacket{}
	if err := protobuf.Codec.Unmarshal(respBody, rawPacket); err != nil {
		log.Fatal(err)
	} else {
		ms, err := sandwich.ToMessages(rawPacket.RawAny, protobuf.Codec)
		if err != nil {
			log.Fatal(err)
		}

		//log.Printf("Metadata: %+v\n", rawPacket.Metadata)
		for _, v := range ms {
			if temp, ok := v.Message.(proto.Message); ok {
				ret = append(ret, temp)
			} else {
				log.Fatalf("ms %v is not proto.Message\n", v)
			}
		}
	}

	// log it
	for i := 0; i < len(ret); i++ {
		m := ret[i]
		//jsonBytes, _ := json.MarshalIndent(m, "", "\t")
		jsonBytes, _ := json.Marshal(m)
		fmt.Printf("----ResProto[%T]:\t%+v\n\n", m, string(jsonBytes))
	}
	return
}

// 从原始的pb 蹈 netpack对应的raw
func PbToReqBody(seqNum int, req proto.Message) []byte {
	var reqBody []byte
	// 用于透传信息，，比如seq-id, 等信息，，可灵活扩展，类似于map
	md := metadata2.Pairs("seq_num", strconv.Itoa(seqNum))

	rawPacket, err := sandwich.PacketWith(protobuf.Codec, message.SliceWith(req), md, int32(seqNum))
	if err != nil {
		log.Fatal(err)
	}

	reqBody, err = protobuf.Codec.Marshal(rawPacket)
	if err != nil {
		log.Fatal(err)
	}
	return reqBody
}
