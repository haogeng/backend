package xmiddleware

import (
	"bitbucket.org/funplus/ark"
	"bitbucket.org/funplus/sandwich"
	"bitbucket.org/funplus/sandwich/protocol/netutils"
	"github.com/dgrijalva/jwt-go"
	"gopkg.in/errgo.v2/errors"
	"server/internal/log"
	"server/pkg/cache"
	"server/pkg/gen/msg"
	"time"
)

var (
	AuthConfigSigningKey = map[string][]byte{"key": []byte("spL^YOr^sl87Yenci^y7QAIayJ4P%GCj")}
	AuthKeyTokenFlag     = "gmx-key-token"
	AuthKeyClaimFlag     = "gmx-key-claim"
	// 客户端硬编码，不要轻易改动
	AuthKeyTokenInHeader = "x-fun-user-token"
)

//var AuthConfigOptions = [...]jwt.ConfigOption{
//	auth.WithErrorHandler(nil),
//	auth.WithSuccessHandler(nil),
//	auth.WithKeyClaimInContext(AuthKeyClaimFlag),
//	auth.WithKeyTokenInContext(AuthKeyTokenFlag),
//	auth.WithKeyTokenInHeader(AuthKeyTokenInHeader),
//	auth.WithKeyTokenInCookie(""),
//	auth.WithClaims(jwt.MapClaims{}),
//	auth.WithSigningKey(nil),
//	auth.WithSigningKeys(nil),
//	auth.WithSigningMethod(jwt.SigningMethodHS256),
//	auth.WithAuthScheme(""),
//	auth.WithShouldSetTokenToResponseHeader(true),
//	auth.WithShouldSetTokenToCookie(false),
//}

var (
	ErrTokenCheckFailed  = errors.New("token check failed")
	ErrTokenCheckCompare = errors.New("token check compare error")
)

type TokenClaims struct {
	jwt.StandardClaims
	UserId uint64
	Seq    int64 `json:"seq"`
	Raw    []byte
}

var TokenTtl time.Duration

// 更新时间戳 每次check的时候自动更新
func updateTokenUuidTtl(tokenClaims *TokenClaims) {
	//StoreTokenUuid(tokenClaims)
	key := cache.GetTokenUuidKey(tokenClaims.UserId)
	//tokenClaims.ExpiresAt(time.Now().Unix() + TokenTtl / time.Second))
	cache.Redis.Expire(key, TokenTtl)
}

func loadTokenUuid(uid uint64) (tokenUuid string, ok bool) {
	key := cache.GetTokenUuidKey(uid)
	if b, ok := cache.Redis.Get(key); ok {
		tokenUuid = string(b)
		return tokenUuid, ok
	}

	return "", false
}

func StoreTokenUuid(tokenClaims *TokenClaims) {
	if tokenClaims == nil {
		return
	}

	key := cache.GetTokenUuidKey(tokenClaims.UserId)
	cache.Redis.Set(key, []byte(tokenClaims.Id), TokenTtl)
}

// 失效token
func ExpireToken(uid uint64) {
	key := cache.GetTokenUuidKey(uid)
	cache.Redis.Del(key)
}

func CheckToken(ttl time.Duration) ark.MiddlewareFunc {
	TokenTtl = ttl

	// 此处需要命名返回，defer捕获返回值
	return func(c *ark.Context, next ark.HandlerFunc) (err error) {
		//log.Debug("Start check token")

		tokenClaims, err := GetTokenClaim(AuthKeyClaimFlag, c)
		//log.Debugf("claims:%+v", tokenClaims)

		hasGoneNext := false

		// 按特定code发错误
		defer func() {
			// 只覆盖token类错误
			if err != nil && !hasGoneNext {
				errToC := &netutils.ErrorResponse{
					Code:    int32(msg.Ecode_TOKEN_FAILED),
					Message: err.Error(),
				}

				// 覆盖err,,没走责任连机制，手动重复啊的链式关系，此处覆盖错误值
				log.Debug(errToC)
				err = sandwich.SendWithArkContext(c, errToC)
			}
		}()

		if err != nil {
			return ErrTokenCheckFailed
		}

		if tokenClaims == nil {
			return ErrTokenCheckFailed
		}

		if len(tokenClaims.Id) == 0 {
			return ErrTokenCheckFailed
		}

		uuid, ok := loadTokenUuid(tokenClaims.UserId)
		//log.Debugf("uuid:%v", uuid)
		if !ok || uuid != tokenClaims.Id {
			return ErrTokenCheckCompare
		}

		hasGoneNext = true
		err = next(c)
		if err == nil {
			updateTokenUuidTtl(tokenClaims)
		}

		return err
	}
}
