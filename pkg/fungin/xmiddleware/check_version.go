package xmiddleware

import (
	"bitbucket.org/funplus/ark"
	"bitbucket.org/funplus/sandwich"
	"server/internal/log"
	"server/internal/util/lighthouse"
	"server/pkg/gen/msg"
)

const (
	VersionInHeader  = "X-Fun-Version"
	PlatformInHeader = "X-Fun-Platform"
)

const (
	noVersionOrPlatformInHeader = "not found version or platform in header"
	versionTooOld               = "need relogin to update res"
)

func CheckVersion() ark.MiddlewareFunc {
	return func(c *ark.Context, next ark.HandlerFunc) error {
		appVersion := c.GetHeader(VersionInHeader)
		platform := c.GetHeader(PlatformInHeader)

		// skip if not exist
		if len(appVersion) == 0 || len(platform) == 0 {
			return next(c)
		}

		err := lighthouse.CheckAppVersion(appVersion)
		versionOk := true
		// 重写错误值
		if err != nil {
			versionOk = false
		} else {
			versionOk = lighthouse.CheckResVersion(appVersion, platform)
		}

		if !versionOk {
			log.Debug("version err")
			return sandwich.SendWithArkContext(c, &msg.ErrorInfo{
				Id:       1021,
				DebugMsg: versionTooOld,
			})
		}

		return next(c)
	}
}
