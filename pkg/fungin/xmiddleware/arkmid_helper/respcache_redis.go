package arkmid_helper

import (
	"bitbucket.org/funplus/ark"
	"bitbucket.org/funplus/arkmid/respcache"
	"fmt"
	"github.com/vmihailenco/msgpack/v4"
	"server/pkg/cache"
	"server/pkg/fungin/xmiddleware"
	"time"
)

// !!! 一坑
// msgpack 及其他需要注意
// 1 传入是一层指针  2 关注字段大小写
type rrInfo struct {
	Id  int32
	Raw []byte
}

type RedisResponseCache struct {
	Ttl time.Duration
}

func NewRedisResponseCache(ttl time.Duration) respcache.CacheProvider {
	return &RedisResponseCache{Ttl: ttl}
}

func (*RedisResponseCache) getKey(ctx *ark.Context) string {
	if uid, err := xmiddleware.GetUid(ctx); err == nil {
		return cache.GetResponseKey(uid)
	}
	return ""
}

// @ret seq_id, Raw, resultFlag
func (m *RedisResponseCache) Get(ctx *ark.Context) (id int32, raw []byte, result bool) {
	id, raw, result = -1, nil, false

	key := m.getKey(ctx)
	if key == "" {
		return
	}

	//cache.
	b, ok := cache.Redis.Get(key)
	if !ok {
		return
	}

	var rr rrInfo
	err := msgpack.Unmarshal(b, &rr)
	if err != nil {
		panic(fmt.Errorf("%v\n", err))
		return
	}

	id, raw, result = rr.Id, rr.Raw, true

	return
}

func (m *RedisResponseCache) Set(ctx *ark.Context, id int32, raw []byte) {
	key := m.getKey(ctx)
	if key == "" {
		return
	}

	b, err := msgpack.Marshal(&rrInfo{Id: id, Raw: raw})
	if err != nil {
		panic(fmt.Errorf("%v\n", err))
		return
	}

	cache.Redis.Set(key, b, m.Ttl)
}
