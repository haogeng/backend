package arkmid_helper

import (
	"bitbucket.org/funplus/ark"
	"bitbucket.org/funplus/sandwich/current"
)

type ContextSeqIdProvider struct {
}

func (p *ContextSeqIdProvider) Get(ctx *ark.Context) (seqId int32, ok bool) {
	return current.MustGetIncomingSequenceID(ctx), true
}
