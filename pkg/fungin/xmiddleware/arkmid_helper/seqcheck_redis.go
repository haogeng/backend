package arkmid_helper

import (
	"bitbucket.org/funplus/ark"
	"bitbucket.org/funplus/arkmid/seqcheck"
	"server/pkg/cache"
	"server/pkg/dlock"
	"server/pkg/fungin/xmiddleware"
	"strconv"
	"time"
)

type RedisRequestSequencer struct {
	ttl                  time.Duration
	SequenceDealWithLock bool
}

func NewRedisRequestSequencer(ttl time.Duration, sequenceDealWithLock bool) seqcheck.LastSeqIdProvider {
	return &RedisRequestSequencer{ttl: ttl, SequenceDealWithLock: sequenceDealWithLock}
}

func (*RedisRequestSequencer) getKey(ctx *ark.Context) string {
	if uid, err := xmiddleware.GetUid(ctx); err == nil {
		return cache.GetSeqIdKey(uid)
	}
	return ""
}

func (*RedisRequestSequencer) getLockKey(ctx *ark.Context) string {
	if uid, err := xmiddleware.GetUid(ctx); err == nil {
		return cache.GetLockKeyFor(cache.GetSeqIdKey(uid))
	}
	return ""
}

func (m *RedisRequestSequencer) Get(ctx *ark.Context) (int32, bool) {
	if key := m.getKey(ctx); key != "" {
		if byteIdLast, ok := cache.Redis.Get(key); ok {
			strIdLast := string(byteIdLast)
			idLast, _ := strconv.Atoi(strIdLast)
			return int32(idLast), true
		}
	}
	return -1, true
}

func (m *RedisRequestSequencer) Set(ctx *ark.Context, id int32) {
	if key := m.getKey(ctx); key != "" {
		cache.Redis.Set(key, []byte(strconv.Itoa(int(id))), m.ttl)
	}
}

//
//func (m *RedisRequestSequencer) Check(last int, now int) (bool, error) {
//	return last < now, nil
//}

func (m *RedisRequestSequencer) Acquire(ctx *ark.Context) bool {
	if !m.SequenceDealWithLock {
		return true
	}

	key := m.getLockKey(ctx)
	return dlock.AcquireLock(key, time.Second*10)
}

func (m *RedisRequestSequencer) Release(ctx *ark.Context) {
	if !m.SequenceDealWithLock {
		return
	}

	key := m.getLockKey(ctx)
	if len(key) == 0 {
		return
	}

	dlock.ReleaseLock(key)
}
