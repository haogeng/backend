package xmiddleware

import (
	"bitbucket.org/funplus/ark"
	gocache "github.com/patrickmn/go-cache"
	"strconv"
	"time"
)

// 被用于近似计算当前正在处理的请求数
var uid2CtxCache = gocache.New(time.Minute*10, time.Minute*5)

// 正在处理的请求数
func CountOfHandling() int {
	return uid2CtxCache.ItemCount()
}

func GetCtxByUid(uid uint64) *ark.Context {
	sUid := strconv.FormatUint(uid, 10)
	if v, ok := uid2CtxCache.Get(sUid); ok {
		return v.(*ark.Context)
	}
	return nil
}

// 有时候需要根据玩家信息获得ctx，来追加信息等操作
// 使用本地内存缓存 (go cache)
func BindCtxToUser() ark.MiddlewareFunc {
	return func(c *ark.Context, next ark.HandlerFunc) error {
		uid, err := GetUid(c)
		if err != nil {
			return err
		}

		// 实现了一个类似于上下文的东东...
		sUid := strconv.FormatUint(uid, 10)
		uid2CtxCache.Set(sUid, c, time.Minute*10)
		defer func() {
			uid2CtxCache.Delete(sUid)
		}()

		return next(c)
	}
}
