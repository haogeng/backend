package xmiddleware

import (
	"bitbucket.org/funplus/ark"
	"gopkg.in/errgo.v2/errors"
	"log"
	"strings"
)

var (
	ErrNeedLocal = errors.New("you should visit in local")
	ErrNeedDev   = errors.New("you should visit in development")
)
var isDevelop bool

func CheckLocal(isDev bool) ark.MiddlewareFunc {
	isDevelop = isDev
	return func(c *ark.Context, next ark.HandlerFunc) error {
		if !isDevelop {
			return ErrNeedDev
		}

		//referer := c.Request.Header.Get("Referer") //请求头部
		ip := c.ClientIP()
		log.Printf("CheckLocal %v", ip)

		isLocal := false
		if strings.Contains(ip, "127.0.0.1") || // ipv4
			strings.Contains(ip, "localhost") ||
			strings.Contains(ip, "::1") { // ipv6
			isLocal = true
		}

		if !isLocal {
			return ErrNeedLocal
		}

		return next(c)
	}
}
