package xmiddleware

import (
	"bitbucket.org/funplus/ark"
	"server/pkg/monitor"
)

// 判断redis和mysql连接是否ok，，如果不ok，则直接通知运营人员，，同时暂停一切业务逻辑
func CheckInternalCon() ark.MiddlewareFunc {
	return func(c *ark.Context, next ark.HandlerFunc) error {
		if err := monitor.Check(); err != nil {
			return err
		}
		return next(c)
	}
}
