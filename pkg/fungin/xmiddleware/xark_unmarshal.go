package xmiddleware

import (
	"bitbucket.org/funplus/ark"
	"bitbucket.org/funplus/golib/encoding2/protobuf"
	"bitbucket.org/funplus/sandwich"
)

// ark 底层unmarshal后，这里就不需要了

func ArkUnmarshal() ark.MiddlewareFunc {
	return func(c *ark.Context, next ark.HandlerFunc) error {
		// 里面有判定，多调用一次也无所谓
		err := sandwich.ArkUnmarshalToCurrent(c, protobuf.Codec)
		if err == nil {
			err = next(c)
		}
		return err
	}
}
