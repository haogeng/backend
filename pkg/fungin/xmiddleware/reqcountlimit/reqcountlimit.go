package reqcountlimit

import (
	"errors"
	"fmt"
	"time"

	"bitbucket.org/funplus/ark"
)

var (
	ErrTokenExhausted = errors.New("req count token exhausted")
)

// 资源
type token struct {
	Count        float64
	LastFillTime time.Duration
}

//!!! TokenBucket 只读信息，线程安全
//!!! token 单元，虽然线程不安全，，但由于用户业务的串行化，不存在多线程访问情况
// rate = count / duration
type TokenBucket struct {
	*Config
}

func (m *TokenBucket) Find(key string) *token {
	c := m.Config.cacheProvider
	vt, ok := c.Get(key)
	var t *token
	if ok {
		t = vt.(*token)
	} else {
		// ttl 过期后，按full值返回
		t = &token{
			Count:        float64(m.Config.maxCountPerFill),
			LastFillTime: time.Duration(time.Now().UnixNano()),
		}
	}
	return t
}

func (m *TokenBucket) Update(key string, t *token) {
	m.Config.cacheProvider.Set(key, t, m.Config.expireDuration)
}

// 根据逝去的时间填充token
func (m *TokenBucket) FillByTime(key string) {
	t := m.Find(key)

	rate := float64(m.Config.maxCountPerFill) / float64(m.Config.tokenFillDuration)

	cur := time.Duration(time.Now().UnixNano())
	diff := cur - t.LastFillTime
	if diff <= 0 {
		return
	}

	t.Count = t.Count + rate*float64(diff)
	if t.Count > float64(m.Config.maxCountPerFill) {
		t.Count = float64(m.Config.maxCountPerFill)
	}
	t.LastFillTime = cur

	m.Update(key, t)
}

func (m *TokenBucket) debugCount(key string) {
	t := m.Find(key)
	fmt.Printf("key:%s token:%+v\n", key, t)
}

// 消耗成功返回
func (m *TokenBucket) Consume(key string) error {
	t := m.Find(key)
	if t != nil && t.Count >= 1 {
		t.Count--
		m.Update(key, t)
		return nil
	}

	return ErrTokenExhausted
}

// 针对ip或uid的请求次数限流器
// 可以使用gocache，没用滑动窗口，使用tokenbucket思路
// 可配合根据ip的字节流速限流
func New(ops ...ConfigOption) ark.MiddlewareFunc {
	conf := NewConfig(ops...)
	tokenBucket := &TokenBucket{
		Config: conf,
	}

	return func(c *ark.Context, next ark.HandlerFunc) error {
		if conf.skipper != nil && conf.skipper(c) {
			return next(c)
		}

		if len(conf.ignoreKeyInHeader) > 0 {
			if c.GetHeader(conf.ignoreKeyInHeader) == "true" {
				return next(c)
			}
		}

		rcKey := conf.cacheKeyProvider.CacheKey(c)
		tokenBucket.FillByTime(rcKey)
		err := tokenBucket.Consume(rcKey)

		//tokenBucket.debugCount(rcKey)

		if err != nil {
			return err
		}
		return next(c)
	}
}
