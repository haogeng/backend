package reqcountlimit

import (
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"sync"
	"testing"
	"time"

	"bitbucket.org/funplus/ark"
	. "github.com/smartystreets/goconvey/convey"
)

type mockCacheProvider struct {
	m sync.Map
}

func (p *mockCacheProvider) Get(k string) (interface{}, bool) {
	return p.m.Load(k)
}
func (p *mockCacheProvider) Set(k string, x interface{}, d time.Duration) {
	p.m.Store(k, x)
	// no use d
}

type dummyKeyProvider struct {
}

func (p *dummyKeyProvider) CacheKey(ctx *ark.Context) string {
	return "dummyKeyProvider_key"
}

func TestTokenBucket(t *testing.T) {
	t.Log("TestTokenBucket will take some seconds, pls wait patiently.")
	//maxCount := int64(60)
	Convey("test req count limit", t, func() {
		// 加速测试
		TestScale := 100

		conf := newDefaultConfig()
		conf.cacheProvider = &mockCacheProvider{}
		conf.cacheKeyProvider = &dummyKeyProvider{}
		conf.maxCountPerFill = 60
		conf.tokenFillDuration = time.Minute / time.Duration(TestScale)
		conf.expireDuration = time.Minute * 2 / time.Duration(TestScale)

		tb := &TokenBucket{
			Config: conf,
		}

		idKey := "mockKey"
		// consume all
		for i := 0; i < int(conf.maxCountPerFill); i++ {
			err := tb.Consume(idKey)
			So(err, ShouldBeNil)
		}

		err := tb.Consume(idKey)
		So(err, ShouldNotBeNil)
		So(err.Error(), ShouldContainSubstring, ErrTokenExhausted.Error())

		// fill half
		for i := 0; i < int(conf.maxCountPerFill/2); i++ {
			time.Sleep(time.Duration(tb.tokenFillDuration) / time.Duration(conf.maxCountPerFill))

			tb.FillByTime(idKey)
			// go test -v 才显示
			tb.debugCount(idKey)
		}
		for i := 0; i < int(conf.maxCountPerFill/2); i++ {
			err := tb.Consume(idKey)
			So(err, ShouldBeNil)
		}

		time.Sleep(tb.tokenFillDuration)
		tb.FillByTime(idKey)
		tb.debugCount(idKey)

		time.Sleep(tb.tokenFillDuration)
		tb.FillByTime(idKey)
		tb.debugCount(idKey)

		So(tb.Find(idKey).Count >= float64(tb.maxCountPerFill), ShouldBeTrue)
		So(tb.Find(idKey).Count < float64(tb.maxCountPerFill+1), ShouldBeTrue)
	})
}

func TestReqCount(t *testing.T) {
	maxCount := 10
	fillDuration := time.Second

	partFillCount := 2

	countAry := []int{maxCount, 1, partFillCount}
	retSubStrAry := []string{"ok", ErrTokenExhausted.Error(), "ok"}
	sleepAry := []time.Duration{0, fillDuration / time.Duration(maxCount) * time.Duration(partFillCount), 0}

	//maxCount := int64(60)
	Convey("test req count limit", t, func() {
		e := ark.New()
		e.Use(New(WithCacheProvider(&mockCacheProvider{}),
			WithCacheKeyProvider(&IpCacheKeyProvider{}),
			WithMaxCountPerFill(maxCount),
			WithTokenFillDuration(fillDuration),
			WithExpireDuration(fillDuration*2)))

		e.Get("/", func(ctx *ark.Context) error {
			return ctx.String(http.StatusOK, "ok")
		})

		for ti := 0; ti < len(countAry); ti++ {
			for i := 0; i < countAry[ti]; i++ {
				req := httptest.NewRequest(http.MethodGet, "/", nil)
				resp, err := e.Test(req)
				So(err, ShouldBeNil)

				defer resp.Body.Close()
				body, err := ioutil.ReadAll(resp.Body)

				So(string(body), ShouldContainSubstring, retSubStrAry[ti])
			}
			// wait a while
			time.Sleep(sleepAry[ti])
		}
	})
}
