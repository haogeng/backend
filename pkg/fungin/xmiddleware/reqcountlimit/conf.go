package reqcountlimit

import (
	"time"

	"bitbucket.org/funplus/ark"
	"bitbucket.org/funplus/arkmid"
)

const (
	DefaultIgnoreKeyInHeader = "xignore_rate_limit"
)

type CacheProvider interface {
	Get(k string) (interface{}, bool)
	Set(k string, x interface{}, d time.Duration)
}
type CacheKeyProvider interface {
	// Example:
	// Uid key like "reqcount:uid:{1}"
	// Ip key like "reqcount:uid:{0.0.0.0}
	CacheKey(ctx *ark.Context) string
}

//go:generate optionGen --v=true
func _ConfigOptionDeclareWithDefault() interface{} {
	return map[string]interface{}{
		// should skip the middleware
		"skipper": arkmid.Skipper(arkmid.DefaultSkipper),

		"cacheProvider": CacheProvider(nil),

		"cacheKeyProvider": CacheKeyProvider(&IpCacheKeyProvider{}),

		"ignoreKeyInHeader": string(DefaultIgnoreKeyInHeader),

		"maxCountPerFill":   int(90),
		"tokenFillDuration": time.Duration(time.Minute),
		// should be more than double tokenFillDuration
		"expireDuration": time.Duration(time.Minute * 2),
	}
}

func init() {
	InstallConfigWatchDog(func(cc *Config) {
		if cc.cacheProvider == nil {
			panic("reqcount cache shouldn't be nil")
		}

		if cc.cacheKeyProvider == nil {
			panic("reqcount key provider shouldn't be nil")
		}

		if cc.expireDuration < cc.tokenFillDuration*2 {
			panic("reqcount expireDuration should be more than 2 minutes")
		}
	})
}
