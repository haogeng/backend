package reqcountlimit

import (
	"bitbucket.org/funplus/ark"
	"fmt"
)

type IpCacheKeyProvider struct {
}

func (p *IpCacheKeyProvider) CacheKey(ctx *ark.Context) string {
	ipStr := ctx.ClientIP()
	return fmt.Sprintf("rclimit:ip:{%s}", ipStr)
}
