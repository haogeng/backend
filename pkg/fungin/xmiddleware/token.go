package xmiddleware

import (
	"bitbucket.org/funplus/sandwich/current"
	"context"
	"gopkg.in/errgo.v2/errors"
)

// uid: user id or player id
func GetUid(cCtx context.Context) (uid uint64, e error) {
	ctx := current.MustGetArkContext(cCtx)

	sessionTokenIf := ctx.Value(AuthKeyClaimFlag)
	if sessionTokenIf == nil {
		return 0, errors.New("get token from context error")
	}
	sessionToken := sessionTokenIf.(*TokenClaims)
	return sessionToken.UserId, nil
}

// token uuid: token id
func GetTokenUuid(cCtx context.Context) (tokenUuid string, e error) {
	ctx := current.MustGetArkContext(cCtx)

	sessionTokenIf := ctx.Value(AuthKeyClaimFlag)
	if sessionTokenIf == nil {
		return "", errors.New("get token from context error")
	}
	sessionToken := sessionTokenIf.(*TokenClaims)
	return sessionToken.Id, nil
}

func GetTokenClaim(tokenKey string, cCtx context.Context) (tokenClaim *TokenClaims, e error) {
	ctx := current.MustGetArkContext(cCtx)

	sessionTokenIf := ctx.Value(tokenKey)
	if sessionTokenIf == nil {
		return nil, errors.New("get token from context error")
	}
	//log.Debugf("session:%+v %T Kind:%v", sessionTokenIf, sessionTokenIf, reflect.ValueOf(sessionTokenIf).Kind())

	sessionToken := sessionTokenIf.(*TokenClaims)
	return sessionToken, nil
}
