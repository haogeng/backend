package compressor

import (
	"fmt"
	"net/http"
	"strings"

	"bitbucket.org/funplus/ark"
	"bitbucket.org/funplus/golib/encoding2"
	"bitbucket.org/funplus/golib/encoding2/compressor"
	// Trigger gzip init
	_ "bitbucket.org/funplus/golib/encoding2/compressor/gzip"
)

const (
	vary            = "Vary"
	acceptEncoding  = "Accept-Encoding"
	contentEncoding = "Content-Encoding"
	contentLength   = "Content-Length"
)

type CompressResponseWriter struct {
	http.ResponseWriter
	zWriter            compressor.WriteFlushCloser
	oldContentEncoding string
	oldVary            string
}

// ctx.Response().bufferRecorder will escape from gzip.
func (g *CompressResponseWriter) StartCompress(ctx *ark.Context, name string) error {
	// Use pool internal in encoding2
	cp := encoding2.GetCompressor(name)
	if cp == nil {
		return fmt.Errorf("not found %s", name)
	}

	w := ctx.Response().Writer
	c, _ := cp.Compress(w)
	g.ResponseWriter = w
	g.zWriter = c

	g.oldContentEncoding = ctx.GetHeader(contentEncoding)
	g.oldVary = ctx.GetHeader(vary)

	// 必须放在start里设置
	ctx.HeaderSet(contentEncoding, name)
	ctx.HeaderSet(vary, acceptEncoding)
	ctx.Response().Header().Del(contentLength)

	ctx.Response().Writer = g
	return nil
}

func (g *CompressResponseWriter) RestoreHeaderOnPanic(ctx *ark.Context) {
	ctx.HeaderSet(contentEncoding, g.oldContentEncoding)
	ctx.HeaderSet(vary, g.oldVary)
	ctx.Response().Header().Add(contentLength, "")
}

func (g *CompressResponseWriter) Write(b []byte) (int, error) {
	n, err := g.zWriter.Write(b)
	//log.Printf("write %d\n", n)
	return n, err
}

func (g *CompressResponseWriter) EndCompress() error {
	// set header ... and so on.
	return g.zWriter.Close()
}

func New(ops ...ConfigOption) ark.MiddlewareFunc {
	conf := NewConfig(ops...)
	return func(ctx *ark.Context, next ark.HandlerFunc) error {
		if conf.Skipper != nil && conf.Skipper(ctx) {
			return next(ctx)
		}

		// Judge if accept encoding
		if !strings.Contains(ctx.GetHeader(acceptEncoding), conf.AlgorithmName) {
			return next(ctx)
		}

		// TODO judge by min size, but maybe it will take unnecessary pollution

		// Compress
		old := ctx.Response().Writer

		gzipWriter := &CompressResponseWriter{}
		err := gzipWriter.StartCompress(ctx, conf.AlgorithmName)
		if err != nil {
			return err
		}

		endedCompress := false
		defer func() {
			if r := recover(); r != nil {
				//log.Error("panicCatcher recover:%v", r)
				gzipWriter.RestoreHeaderOnPanic(ctx)
				ctx.Response().Writer = old
				panic("rePanic in compressor")
			} else {
				if !endedCompress {
					_ = gzipWriter.EndCompress()
					endedCompress = true
					ctx.Response().Writer = old
				}
			}
		}()

		err = next(ctx)

		endedCompress = true
		err = gzipWriter.EndCompress()

		// Restore
		ctx.Response().Writer = old

		return err
	}
}
