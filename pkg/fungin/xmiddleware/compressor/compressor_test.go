package compressor

import (
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	. "github.com/smartystreets/goconvey/convey"

	"bitbucket.org/funplus/ark"
	"bitbucket.org/funplus/golib/encoding2/compressor"
)

func TestCompressor(t *testing.T) {
	testStr := "hello world"

	Convey("test accept gzip", t, func() {
		e := ark.New()
		e.Use(New(WithAlgorithmName("gzip")))

		e.Post("/test", func(c context.Context) error {
			body, err := ioutil.ReadAll(c.Request().Body)
			if err != nil {
				return err
			}
			retStr := string(body)
			return c.String(http.StatusOK, retStr)
		})

		req := httptest.NewRequest(http.MethodPost, "/test", strings.NewReader(testStr))
		req.Header.Set(acceptEncoding, "gzip")

		resp, err := e.Test(req)
		defer resp.Body.Close()
		So(err, ShouldBeNil)

		So(resp.Header.Get(contentEncoding), ShouldContainSubstring, "gzip")

		c := compressor.GetCompressor("gzip")
		de, err := c.Decompress(resp.Body)
		So(err, ShouldBeNil)
		r, err := ioutil.ReadAll(de)
		So(err, ShouldBeNil)
		So(string(r), ShouldEqual, testStr)
		err = de.Close()
		So(err, ShouldBeNil)
	})

	Convey("test un-accept gzip", t, func() {
		e := ark.New()
		e.Use(New(WithAlgorithmName("gzip")))

		e.Post("/test", func(c context.Context) error {
			body, err := ioutil.ReadAll(c.Request().Body)
			if err != nil {
				return err
			}
			retStr := string(body)
			return c.String(http.StatusOK, retStr)
		})

		req := httptest.NewRequest(http.MethodPost, "/test", strings.NewReader(testStr))
		req.Header.Set(acceptEncoding, "")

		resp, err := e.Test(req)
		defer resp.Body.Close()

		So(resp.Header.Get(contentEncoding), ShouldNotContainSubstring, "gzip")

		So(err, ShouldBeNil)
		r, err := ioutil.ReadAll(resp.Body)
		So(err, ShouldBeNil)
		So(string(r), ShouldEqual, testStr)
	})
}
