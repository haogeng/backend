package compressor

import (
	"bitbucket.org/funplus/arkmid"
	"fmt"
)

//go:generate optionGen --v=true
func _ConfigOptionDeclareWithDefault() interface{} {
	return map[string]interface{}{
		// should skip the middleware
		"Skipper": arkmid.Skipper(arkmid.DefaultSkipper),

		"AlgorithmName": string("gzip"),

		// TODO
		"MinSize": int(1400),
	}
}

func init() {
	InstallConfigWatchDog(func(cc *Config) {
		if cc.AlgorithmName != "gzip" {
			panic(fmt.Errorf("Unsupport algorithm %s", cc.AlgorithmName))
		}
	})
}
