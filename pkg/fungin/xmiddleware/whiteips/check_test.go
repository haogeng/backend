package whiteips

import (
	"github.com/stretchr/testify/assert"
	"net"
	"strings"
	"testing"
)

func TestHasIp(t *testing.T) {
	//assert.Equal(t, hasIp("192.168.0.1/24", "192.168.0.1"), true, "")
	ips := [][]string{
		{"192.168.0.1/24"},
		{"192.168.0.3/32"},
		{"192.168.0.4/32"},
		{"192.168.0.1/24", "192.168.1.3/24"},
		{"192.168.0.1/24", "192.168.1.3/32"},
	}
	ip := []string{
		"192.168.0.77",
		"192.168.0.3",
		"192.168.0.3",
		"192.168.1.2",
		"192.168.1.4",
	}

	result := []bool{true, true, false, true, false}
	for i := 0; i < len(ips); i++ {
		var ipNets []*net.IPNet
		for j := 0; j < len(ips[i]); j++ {
			_, ipNet, err := net.ParseCIDR(ips[i][j])
			if err != nil {
				panic(err)
			}
			ipNets = append(ipNets, ipNet)
		}

		assert.Equal(t, result[i], hasIp(ipNets, ip[i]), strings.Join(ips[i], "|")+" contains "+ip[i])
	}
}
