package whiteips

import (
	"bitbucket.org/funplus/ark"
	"bitbucket.org/funplus/arkmid"
	"bitbucket.org/funplus/sandwich/current"
	"errors"
	"fmt"
	"net"
	"strings"
)

func hasIp(ipNets []*net.IPNet, curIpStr string) bool {
	curIp := net.ParseIP(curIpStr)
	for i := 0; i < len(ipNets); i++ {
		if ipNets[i].Contains(curIp) {
			return true
		}
	}
	return false
}

func isLocal(ip string) bool {
	if strings.Contains(ip, "127.0.0.1") || // ipv4
		strings.Contains(ip, "localhost") ||
		strings.Contains(ip, "::1") { // ipv6
		return true
	}
	return false
}

type WhiteIpsOfGmProvider interface {
	GetWhiteIpsOfGm() []string
	GetRouterBlackOrder() map[string]int
}

// like "192.0.2.0/24" or "2001:db8::/32", as defined in
// !dont support: "127.0.0.1", "localhost", "192.168.0.1"
// RFC 4632 and RFC 4291.
// 24, 32是指子网掩码位数，24匹配前24位，32指匹配32位，精确匹配使用32
// ips is nil meanings anything can not pass-through
// local always can pass
func New(provider WhiteIpsOfGmProvider, skipper arkmid.Skipper) ark.MiddlewareFunc {
	return func(c *ark.Context, next ark.HandlerFunc) error {
		if skipper != nil && skipper(c) {
			return next(c)
		}

		ips := provider.GetWhiteIpsOfGm()
		if len(ips) == 0 {
			return next(c)
		}

		var ipNets []*net.IPNet
		for i := 0; i < len(ips); i++ {
			_, ipNet, err := net.ParseCIDR(ips[i])
			if err != nil {
				panic(err)
			}
			ipNets = append(ipNets, ipNet)
		}

		ip := c.ClientIP()

		if isLocal(ip) || hasIp(ipNets, ip) {
			return next(c)
		} else {
			return errors.New("The client ip is not in white ips =" + ip)
		}
	}
}

func SkipByProtoName(provider WhiteIpsOfGmProvider) ark.MiddlewareFunc {
	return func(ctx *ark.Context, next ark.HandlerFunc) error {
		message := current.MustGetIncomingMessages(ctx)
		for _, m := range message {
			if _, ok := provider.GetRouterBlackOrder()[m.Uri]; ok {
				return fmt.Errorf("The service is temporarily inaccessible")
			}
		}
		return next(ctx)
	}
}
