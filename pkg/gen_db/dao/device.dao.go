/** Code generated by pb2db. DO NOT EDIT.**/
package dao

import (
	"database/sql"
	"errors"

	"bitbucket.org/funplus/golib/xdb"
	"bitbucket.org/haogeng/backend/pkg/gen/msg"
	"bitbucket.org/haogeng/backend/pkg/gen_db/internal/models"
	"bitbucket.org/haogeng/backend/pkg/gen_db/internal/repository"
	"bitbucket.org/haogeng/backend/pkg/gen_db/internal/utils"
)

var Device = new(DeviceMgr)

type DeviceMgr struct {
	_rep *repository.DeviceRepositoryDbProxy
}

func (*DeviceMgr) pb2db(_in *msg.Device) (*models.Device, error) {
	_out := new(models.Device)
	if _in == nil {
		return _out, nil
	}
	_out.Id = _in.Id
	_out.Name = _in.Name
	_out.ChannelType = _in.ChannelType
	_out.ChannelToken = _in.ChannelToken
	_out.AccountId = _in.AccountId
	_out.Crc32 = _in.Crc32
	return _out, nil
}

func (*DeviceMgr) db2pb(_in *models.Device) (*msg.Device, error) {
	_out := new(msg.Device)
	if _in == nil {
		return _out, nil
	}
	_out.Id = _in.Id
	_out.Name = _in.Name
	_out.ChannelType = _in.ChannelType
	_out.ChannelToken = _in.ChannelToken
	_out.AccountId = _in.AccountId
	_out.Crc32 = _in.Crc32
	return _out, nil
}

func (_mgr *DeviceMgr) hintId(_hintId *int64, _in *models.Device) (bool, error) {
	_, _hid, _err1 := _mgr._rep.ResolveHints(_in)
	if _err1 != nil {
		return false, _err1
	}
	if *_hintId < 0 {
		*_hintId = _hid
	}
	return _hid == *_hintId, nil
}

func (_mgr *DeviceMgr) bulk(_in []*models.Device, fn func([]*models.Device) error) error {
	var _err error
	var _maxBulkCount = utils.GetMaxBulkCount()
	for {
		if _c := len(_in); _c > _maxBulkCount {
			_err = fn(_in[:_maxBulkCount])
			if _err != nil {
				break
			}
			_in = _in[_maxBulkCount:]
		} else {
			if _c > 0 {
				_err = fn(_in)
			}
			break
		}
	}
	return _err
}

func (_mgr *DeviceMgr) i2db(_in interface{}) (interface{}, error) {
	if _in == nil {
		return _in, nil
	}
	if _ins, _ok := _in.([]interface{}); _ok {
		var _err error
		for _k, _v := range _ins {
			_ins[_k], _err = _mgr.i2db(_v)
			if _err != nil {
				return nil, _err
			}
		}
		return _ins, nil
	}
	if _ins, _ok := _in.(*[]*msg.Device); _ok {
		_m_ins := make([]*models.Device, len(*_ins))
		for _k, _v := range *_ins {
			_p_v, _err := _mgr.i2db(_v)
			if _err != nil {
				return nil, _err
			}
			_m_ins[_k] = _p_v.(*models.Device)
		}
		return &_m_ins, nil
	}
	_m, _ok := _in.(*msg.Device)
	if !_ok {
		return _in, nil
	}
	return _mgr.pb2db(_m)
}

func (_mgr *DeviceMgr) is2dbs(_in []interface{}) ([]interface{}, error) {
	for _k, _v := range _in {
		_out, _err := _mgr.i2db(_v)
		if _err != nil {
			return nil, _err
		}
		_in[_k] = _out
	}
	return _in, nil
}

func (_mgr *DeviceMgr) i2pb(_in interface{}) (interface{}, error) {
	if _in == nil {
		return _in, nil
	}
	if _ins, _ok := _in.([]interface{}); _ok {
		var _err error
		for _k, _v := range _ins {
			_ins[_k], _err = _mgr.i2pb(_v)
			if _err != nil {
				return nil, _err
			}
		}
		return _ins, nil
	}
	if _ins, _ok := _in.(*[]*models.Device); _ok {
		_p_ins := make([]*msg.Device, len(*_ins))
		for _k, _v := range *_ins {
			_p_v, _err := _mgr.i2pb(_v)
			if _err != nil {
				return nil, _err
			}
			_p_ins[_k] = _p_v.(*msg.Device)
		}
		return &_p_ins, nil
	}
	_m, _ok := _in.(*models.Device)
	if !_ok {
		return _in, nil
	}
	return _mgr.db2pb(_m)
}

func (_mgr *DeviceMgr) is2pbs(_in []interface{}) ([]interface{}, error) {
	for _k, _v := range _in {
		_out, _err := _mgr.i2pb(_v)
		if _err != nil {
			return nil, _err
		}
		_in[_k] = _out
	}
	return _in, nil
}

func (_mgr *DeviceMgr) direct(_query string) xdb.XTabler {
	if _, ok := models.DeviceQueries[_query]; !ok {
		return _mgr._rep.XTabler.UnTagged()
	}
	return _mgr._rep.XTabler.Direct()
}

func (_mgr *DeviceMgr) MountTable(xTable xdb.XTabler) {
	_mgr._rep = &repository.DeviceRepositoryDbProxy{XTabler: xTable}
}

func (*DeviceMgr) TableName() string { return models.TableInfoDevice.TableName() }

func (*DeviceMgr) Columns() []string { return models.TableInfoDevice.Columns() }

func (*DeviceMgr) Pks() []string { return models.TableInfoDevice.Pks() }

func (*DeviceMgr) Pb() interface{} { return new(msg.Device) }

func (_mgr *DeviceMgr) RegisterHintIdGenFunc(_fn func(string, *msg.Device) (string, int64, error)) {
	xdb.RegisterGlobalHintGenFunction(func(_tn string, _hints xdb.Hints) (string, int64, error) {
		_data, _err := _mgr.db2pb(_hints.(*models.Device))
		if _err != nil {
			return "", 0, _err
		}
		return _fn(_tn, _data)
	}, _mgr._rep.TableName())
}

func (_mgr *DeviceMgr) Create(id uint64, _in *msg.Device) (_id uint64, _err error) {
	var _m *models.Device
	_m, _err = _mgr.pb2db(_in)
	if _err != nil {
		return
	}
	_m.Id = id
	_, _err = _mgr._rep.Insert(_m)
	if _err != nil {
		return
	}
	return id, nil
}

func (_mgr *DeviceMgr) BatchCreate(idList []uint64, _in []*msg.Device) error {
	_dataCount := len(_in)
	if _dataCount == 0 {
		return nil
	}
	if idList != nil && len(idList) != _dataCount {
		return errors.New("idList count != _in list count")
	}
	var (
		_hintId int64 = -1
		_ms     []*models.Device
	)
	for _i, _pbData := range _in {
		_m, _err := _mgr.pb2db(_pbData)
		if _err != nil {
			return _err
		}
		if idList != nil {
			_m.Id = idList[_i]
		}
		_ok, _err := _mgr.hintId(&_hintId, _m)
		if _err != nil {
			return _err
		}
		if _ok {
			_ms = append(_ms, _m)
			continue
		}
		_, _err = _mgr.Create(_m.Id, _pbData)
		if _err != nil {
			return _err
		}
	}
	return _mgr.bulk(_ms, func(__ms []*models.Device) error {
		_, _err := _mgr._rep.InsertBulk(__ms)
		return _err
	})
}

func (_mgr *DeviceMgr) Delete(id uint64) error {
	_m, _err1 := _mgr.pb2db(nil)
	if _err1 != nil {
		return _err1
	}
	_m.Id = id
	_, _err2 := _mgr._rep.Delete(_m)
	return _err2
}

func (_mgr *DeviceMgr) BatchDelete(_ids []uint64) error {
	if len(_ids) == 0 {
		return nil
	}
	var (
		_hintId int64 = -1
		_ms     []*models.Device
	)
	for _, id := range _ids {
		_m, _err := _mgr.pb2db(nil)
		if _err != nil {
			return _err
		}
		_m.Id = id
		_ok, _err := _mgr.hintId(&_hintId, _m)
		if _err != nil {
			return _err
		}
		if _ok {
			_ms = append(_ms, _m)
			continue
		}
		_err = _mgr.Delete(_m.Id)
		if _err != nil {
			return _err
		}
	}
	return _mgr.bulk(_ms, func(__ms []*models.Device) error {
		_, _err := _mgr._rep.DeleteBulk(__ms)
		return _err
	})
}

func (_mgr *DeviceMgr) Update(id uint64, _in *msg.Device) error {
	_m, _err1 := _mgr.pb2db(_in)
	if _err1 != nil {
		return _err1
	}
	_m.Id = id
	_, _err2 := _mgr._rep.Save(_m)
	return _err2
}

func (_mgr *DeviceMgr) BatchUpdate(_in map[uint64]*msg.Device) error {
	if len(_in) == 0 {
		return nil
	}
	var (
		_hintId int64 = -1
		_ms     []*models.Device
	)
	for id, _pbData := range _in {
		_m, _err := _mgr.pb2db(_pbData)
		if _err != nil {
			return _err
		}
		_m.Id = id
		_ok, _err := _mgr.hintId(&_hintId, _m)
		if _err != nil {
			return _err
		}
		if _ok {
			_ms = append(_ms, _m)
			continue
		}
		_err = _mgr.Update(_m.Id, _pbData)
		if _err != nil {
			return _err
		}
	}
	return _mgr.bulk(_ms, func(__ms []*models.Device) error {
		_, _err := _mgr._rep.SaveBulk(__ms)
		return _err
	})
}

func (_mgr *DeviceMgr) Find(id uint64) (*msg.Device, error) {
	_m := &models.Device{}
	_m.Id = id
	_err := _mgr._rep.Get(_m)
	if _err != nil {
		return nil, _err
	}
	return _mgr.db2pb(_m)
}

func (_mgr *DeviceMgr) FindAllAsSlice() ([]*msg.Device, error) {
	_ms, _err1 := _mgr._rep.All()
	if _err1 != nil {
		return nil, _err1
	}
	var _out []*msg.Device
	for _, _m := range _ms {
		_data, _err2 := _mgr.db2pb(_m)
		if _err2 != nil {
			return nil, _err2
		}
		_out = append(_out, _data)
	}
	return _out, nil
}

func (_mgr *DeviceMgr) FindAllAsMap() (map[uint64]*msg.Device, error) {
	_ms, _err1 := _mgr._rep.All()
	if _err1 != nil {
		return nil, _err1
	}
	_out := make(map[uint64]*msg.Device)
	for _, _m := range _ms {
		_data, _err2 := _mgr.db2pb(_m)
		if _err2 != nil {
			return nil, _err2
		}
		_out[_m.Id] = _data
	}
	return _out, nil
}

func (_mgr *DeviceMgr) resolve(_dest interface{}, _hints interface{}, _args []interface{}) (interface{}, interface{}, []interface{}, error) {
	var _err error
	if _dest, _err = _mgr.i2db(_dest); _err != nil {
		return nil, nil, nil, _err
	}
	if _hints, _err = _mgr.i2db(_hints); _err != nil {
		return nil, nil, nil, _err
	}
	if _args, _err = _mgr.is2dbs(_args); _err != nil {
		return nil, nil, nil, _err
	}
	return _dest, _hints, _args, nil
}

func (_mgr *DeviceMgr) ExecBySql(_query string, _hints interface{}, _args ...interface{}) (result sql.Result, err error) {
	var _err error
	_, _hints, _args, _err = _mgr.resolve(nil, _hints, _args)
	if _err != nil {
		return nil, _err
	}
	if utils.WantNamed(_args) {
		return _mgr.direct(_query).NamedExec(_query, _hints, _args[0])
	}
	return _mgr.direct(_query).Exec(_query, _hints, _args...)
}

func (_mgr *DeviceMgr) SelectBySql(_dest interface{}, _query string, _hints interface{}, _args ...interface{}) error {
	_value, _err := utils.GetInterfaceValue(_dest)
	if _err != nil {
		return _err
	}
	_dest, _hints, _args, _err = _mgr.resolve(_dest, _hints, _args)
	if _err != nil {
		return _err
	}
	if utils.WantNamed(_args) {
		_err = _mgr.direct(_query).NamedSelect(_dest, _query, _hints, _args[0])
	} else {
		_err = _mgr.direct(_query).Select(_dest, _query, _hints, _args...)
	}
	if _err != nil {
		return _err
	}
	if _dest, _err = _mgr.i2pb(_dest); _err == nil {
		utils.SetInterfaceValue(_value, _dest)
	}
	return _err
}

func (_mgr *DeviceMgr) GetBySql(_dest interface{}, _query string, _hints interface{}, _args ...interface{}) error {
	_value, _err := utils.GetInterfaceValue(_dest)
	if _err != nil {
		return _err
	}
	_dest, _hints, _args, _err = _mgr.resolve(_dest, _hints, _args)
	if _err != nil {
		return _err
	}
	if utils.WantNamed(_args) {
		_err = _mgr.direct(_query).NamedGet(_dest, _query, _hints, _args[0])
	} else {
		_err = _mgr.direct(_query).Get(_dest, _query, _hints, _args...)
	}
	if _err != nil {
		return _err
	}
	if _dest, _err = _mgr.i2pb(_dest); _err == nil {
		utils.SetInterfaceValue(_value, _dest)
	}
	return _err
}

func (_mgr *DeviceMgr) QueryBySql(_query string, _hints interface{}, _args ...interface{}) (*sql.Rows, error) {
	var _err error
	_, _hints, _args, _err = _mgr.resolve(nil, _hints, _args)
	if _err != nil {
		return nil, _err
	}
	if utils.WantNamed(_args) {
		return _mgr.direct(_query).NamedQuery(_query, _hints, _args[0])
	}
	return _mgr.direct(_query).Query(_query, _hints, _args...)
}
