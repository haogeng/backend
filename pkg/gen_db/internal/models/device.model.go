/** Code generated by pb2db. DO NOT EDIT.**/
package models

import (
	"bitbucket.org/haogeng/backend/pkg/gen_db/tag"
)

type Device struct {
	Id           uint64 `json:"id" db:"id"`
	Name         string `json:"name" db:"name"`
	ChannelType  string `json:"channel_type" db:"channel_type"`
	ChannelToken string `json:"channel_token" db:"channel_token"`
	AccountId    uint64 `json:"account_id" db:"account_id"`
	Crc32        int32  `json:"crc32" db:"crc32"`
}

var TableInfoDevice = &Device{}

func (_m *Device) Hints() (string, map[string]interface{}) {
	return "device", map[string]interface{}{"id": _m.Id}
}

func (*Device) TableName() string { return "device" }

func (_m *Device) Keys() []interface{} { return []interface{}{_m.Id} }

func (_m *Device) Mapping() map[string]interface{} {
	return map[string]interface{}{"id": _m.Id, "name": _m.Name, "channel_type": _m.ChannelType, "channel_token": _m.ChannelToken, "account_id": _m.AccountId, "crc32": _m.Crc32}
}

func (*Device) Pks() []string { return []string{"id"} }

func (*Device) Columns() []string {
	return []string{"id", "name", "channel_type", "channel_token", "account_id", "crc32"}
}

func (*Device) Version() string { return "" }

func (*Device) Queries() map[string]string { return DeviceQueries }

var DeviceQueries = map[string]string{
	tag.SqlInsert:              `INSERT INTO __TABLE_NAME__ (id,name,channel_type,channel_token,account_id,crc32) VALUES (:id,:name,:channel_type,:channel_token,:account_id,:crc32)`,
	tag.SqlGetAll:              `SELECT id,name,channel_type,channel_token,account_id,crc32 FROM __TABLE_NAME__`,
	tag.SqlGetByPks:            `SELECT id,name,channel_type,channel_token,account_id,crc32 FROM __TABLE_NAME__ WHERE id=:id limit 1`,
	tag.SqlUpdateByPks:         `UPDATE __TABLE_NAME__ SET id=:id,name=:name,channel_type=:channel_type,channel_token=:channel_token,account_id=:account_id,crc32=:crc32 WHERE id=:id`,
	tag.SqlSaveByPks:           `INSERT INTO __TABLE_NAME__ (id,name,channel_type,channel_token,account_id,crc32) VALUES (:id,:name,:channel_type,:channel_token,:account_id,:crc32) ON DUPLICATE KEY UPDATE id=:id,name=:name,channel_type=:channel_type,channel_token=:channel_token,account_id=:account_id,crc32=:crc32`,
	tag.SqlHardDeleteByPks:     `DELETE FROM __TABLE_NAME__ WHERE id=:id`,
	tag.SqlSoftDeleteByPks:     `DELETE FROM __TABLE_NAME__ WHERE id=:id`,
	tag.SqlInsertBulk:          `INSERT INTO __TABLE_NAME__ (id,name,channel_type,channel_token,account_id,crc32) VALUES (:id,:name,:channel_type,:channel_token,:account_id,:crc32)`,
	tag.SqlSaveBulkByPks:       `INSERT INTO __TABLE_NAME__ (id,name,channel_type,channel_token,account_id,crc32) VALUES (:id,:name,:channel_type,:channel_token,:account_id,:crc32) ON DUPLICATE KEY UPDATE id=VALUES(id),name=VALUES(name),channel_type=VALUES(channel_type),channel_token=VALUES(channel_token),account_id=VALUES(account_id),crc32=VALUES(crc32)`,
	tag.SqlHardDeleteBulkByPks: `DELETE FROM __TABLE_NAME__ WHERE :data_or_list(id=:id)`,
	tag.SqlSoftDeleteBulkByPks: `DELETE FROM __TABLE_NAME__ WHERE :data_or_list(id=:id)`,

	tag.SqlTableCreate: `
				CREATE TABLE IF NOT EXISTS __TABLE_NAME__ (
					id BIGINT UNSIGNED NOT NULL,
					name VARCHAR(256) NOT NULL DEFAULT '',
					channel_type VARCHAR(128) NOT NULL DEFAULT '',
					channel_token VARCHAR(128) NOT NULL DEFAULT '',
					account_id BIGINT UNSIGNED NOT NULL DEFAULT '0',
					crc32 INT NOT NULL DEFAULT '0',
					gmt_modified TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
					gmt_create TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
					INDEX (name),
					UNIQUE (name),
					PRIMARY KEY (id)
				) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;`,
	tag.SqlTableDrop: `DROP TABLE IF EXISTS __TABLE_NAME__;`,
}
