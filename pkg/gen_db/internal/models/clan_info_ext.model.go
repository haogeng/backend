/** Code generated by pb2db. DO NOT EDIT.**/
package models

import (
	"database/sql"

	"bitbucket.org/haogeng/backend/pkg/gen_db/tag"
)

type ClanInfoExt struct {
	Id         uint64         `json:"id" db:"id"`
	LogDbInfos sql.NullString `json:"log_db_infos" db:"log_db_infos"`
	ApplyList  sql.NullString `json:"apply_list" db:"apply_list"`
	Crc32      int32          `json:"crc32" db:"crc32"`
}

var TableInfoClanInfoExt = &ClanInfoExt{}

func (_m *ClanInfoExt) Hints() (string, map[string]interface{}) {
	return "clan_info_ext", map[string]interface{}{"id": _m.Id}
}

func (*ClanInfoExt) TableName() string { return "clan_info_ext" }

func (_m *ClanInfoExt) Keys() []interface{} { return []interface{}{_m.Id} }

func (_m *ClanInfoExt) Mapping() map[string]interface{} {
	return map[string]interface{}{"id": _m.Id, "log_db_infos": _m.LogDbInfos, "apply_list": _m.ApplyList, "crc32": _m.Crc32}
}

func (*ClanInfoExt) Pks() []string { return []string{"id"} }

func (*ClanInfoExt) Columns() []string { return []string{"id", "log_db_infos", "apply_list", "crc32"} }

func (*ClanInfoExt) Version() string { return "" }

func (*ClanInfoExt) Queries() map[string]string { return ClanInfoExtQueries }

var ClanInfoExtQueries = map[string]string{
	tag.SqlInsert:              `INSERT INTO __TABLE_NAME__ (id,log_db_infos,apply_list,crc32) VALUES (:id,:log_db_infos,:apply_list,:crc32)`,
	tag.SqlGetAll:              `SELECT id,log_db_infos,apply_list,crc32 FROM __TABLE_NAME__`,
	tag.SqlGetByPks:            `SELECT id,log_db_infos,apply_list,crc32 FROM __TABLE_NAME__ WHERE id=:id limit 1`,
	tag.SqlUpdateByPks:         `UPDATE __TABLE_NAME__ SET id=:id,log_db_infos=:log_db_infos,apply_list=:apply_list,crc32=:crc32 WHERE id=:id`,
	tag.SqlSaveByPks:           `INSERT INTO __TABLE_NAME__ (id,log_db_infos,apply_list,crc32) VALUES (:id,:log_db_infos,:apply_list,:crc32) ON DUPLICATE KEY UPDATE id=:id,log_db_infos=:log_db_infos,apply_list=:apply_list,crc32=:crc32`,
	tag.SqlHardDeleteByPks:     `DELETE FROM __TABLE_NAME__ WHERE id=:id`,
	tag.SqlSoftDeleteByPks:     `DELETE FROM __TABLE_NAME__ WHERE id=:id`,
	tag.SqlInsertBulk:          `INSERT INTO __TABLE_NAME__ (id,log_db_infos,apply_list,crc32) VALUES (:id,:log_db_infos,:apply_list,:crc32)`,
	tag.SqlSaveBulkByPks:       `INSERT INTO __TABLE_NAME__ (id,log_db_infos,apply_list,crc32) VALUES (:id,:log_db_infos,:apply_list,:crc32) ON DUPLICATE KEY UPDATE id=VALUES(id),log_db_infos=VALUES(log_db_infos),apply_list=VALUES(apply_list),crc32=VALUES(crc32)`,
	tag.SqlHardDeleteBulkByPks: `DELETE FROM __TABLE_NAME__ WHERE :data_or_list(id=:id)`,
	tag.SqlSoftDeleteBulkByPks: `DELETE FROM __TABLE_NAME__ WHERE :data_or_list(id=:id)`,

	tag.SqlTableCreate: `
				CREATE TABLE IF NOT EXISTS __TABLE_NAME__ (
					id BIGINT UNSIGNED NOT NULL,
					log_db_infos BLOB,
					apply_list BLOB,
					crc32 INT NOT NULL DEFAULT '0',
					gmt_modified TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
					gmt_create TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
					PRIMARY KEY (id)
				) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;`,
	tag.SqlTableDrop: `DROP TABLE IF EXISTS __TABLE_NAME__;`,
}
