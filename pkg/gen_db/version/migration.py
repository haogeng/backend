from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from sqlalchemy.dialects.mysql.types import TINYINT, DOUBLE, INTEGER, BIGINT, FLOAT
from sqlalchemy import Column, String, ForeignKey, LargeBinary, text, TIMESTAMP
import pymysql
pymysql.install_as_MySQLdb()

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://root:3cu8rt@127.0.0.1:3306/gmx_server_geng5'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
app.config['SQLALCHEMY_ECHO'] = True

db = SQLAlchemy(app)
migrate = Migrate(app, db)

class Account(db.Model):
    __talbe__ = 'account'
    id = Column(BIGINT(unsigned=True), nullable=False, server_default=text('0'), primary_key=True, autoincrement=False)
    player_info = Column(LargeBinary)
    last_player_id = Column(BIGINT(unsigned=True), nullable=False, server_default=text('0'))
    main = Column(INTEGER, nullable=False, server_default=text('0'))
    minor = Column(INTEGER, nullable=False, server_default=text('0'))
    build = Column(INTEGER, nullable=False, server_default=text('0'))
    platform = Column(INTEGER, nullable=False, server_default=text('0'))
    language = Column(String(128), nullable=False, server_default=text('""'))
    crc32 = Column(INTEGER, nullable=False, server_default=text('0'))

    gmt_modified = Column(TIMESTAMP(True), nullable=False, server_default=text('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))
    gmt_create = Column(TIMESTAMP(True), nullable=False, server_default=text('CURRENT_TIMESTAMP'))
class ArenaSeasonInfo(db.Model):
    __talbe__ = 'arena_season_info'
    id = Column(BIGINT(unsigned=True), nullable=False, server_default=text('0'), primary_key=True, autoincrement=False)
    arena_type = Column(INTEGER, nullable=False, server_default=text('0'))
    season_begin_time = Column(BIGINT, nullable=False, server_default=text('0'))
    season_end_time = Column(BIGINT, nullable=False, server_default=text('0'))
    day_rewards = Column(LargeBinary)
    season_number = Column(INTEGER, nullable=False, server_default=text('0'))
    crc32 = Column(INTEGER, nullable=False, server_default=text('0'))

    gmt_modified = Column(TIMESTAMP(True), nullable=False, server_default=text('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))
    gmt_create = Column(TIMESTAMP(True), nullable=False, server_default=text('CURRENT_TIMESTAMP'))
class ClanInfo(db.Model):
    __talbe__ = 'clan_info'
    id = Column(BIGINT(unsigned=True), nullable=False, server_default=text('0'), primary_key=True, autoincrement=False)
    president_id = Column(BIGINT(unsigned=True), nullable=False, server_default=text('0'))
    vice_president_id = Column(BIGINT(unsigned=True), nullable=False, server_default=text('0'))
    clan_increment_id = Column(INTEGER, nullable=False, server_default=text('0'), index=True, unique=True)
    name = Column(String(64), nullable=False, server_default=text('""'), index=True, unique=True)
    apply_type = Column(INTEGER, nullable=False, server_default=text('0'), index=True)
    badge = Column(INTEGER, nullable=False, server_default=text('0'))
    announcement = Column(String(128), nullable=False, server_default=text('""'))
    motto = Column(String(128), nullable=False, server_default=text('""'))
    language_id = Column(INTEGER, nullable=False, server_default=text('0'))
    active_value = Column(LargeBinary)
    average_active_value = Column(LargeBinary)
    all_active_value = Column(INTEGER, nullable=False, server_default=text('0'))
    combat_power = Column(INTEGER, nullable=False, server_default=text('0'))
    member_count = Column(INTEGER, nullable=False, server_default=text('0'), index=True)
    last_active_time = Column(BIGINT, nullable=False, server_default=text('0'))
    crc32 = Column(INTEGER, nullable=False, server_default=text('0'))

    gmt_modified = Column(TIMESTAMP(True), nullable=False, server_default=text('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))
    gmt_create = Column(TIMESTAMP(True), nullable=False, server_default=text('CURRENT_TIMESTAMP'))
class ClanInfoExt(db.Model):
    __talbe__ = 'clan_info_ext'
    id = Column(BIGINT(unsigned=True), nullable=False, server_default=text('0'), primary_key=True, autoincrement=False)
    log_db_infos = Column(LargeBinary)
    apply_list = Column(LargeBinary)
    crc32 = Column(INTEGER, nullable=False, server_default=text('0'))

    gmt_modified = Column(TIMESTAMP(True), nullable=False, server_default=text('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))
    gmt_create = Column(TIMESTAMP(True), nullable=False, server_default=text('CURRENT_TIMESTAMP'))
class ClanInfoMember(db.Model):
    __talbe__ = 'clan_info_member'
    id = Column(BIGINT(unsigned=True), nullable=False, server_default=text('0'), primary_key=True, autoincrement=False)
    member_list = Column(LargeBinary)
    crc32 = Column(INTEGER, nullable=False, server_default=text('0'))

    gmt_modified = Column(TIMESTAMP(True), nullable=False, server_default=text('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))
    gmt_create = Column(TIMESTAMP(True), nullable=False, server_default=text('CURRENT_TIMESTAMP'))
class Device(db.Model):
    __talbe__ = 'device'
    id = Column(BIGINT(unsigned=True), nullable=False, server_default=text('0'), primary_key=True, autoincrement=False)
    name = Column(String(256), nullable=False, server_default=text('""'), index=True, unique=True)
    channel_type = Column(String(128), nullable=False, server_default=text('""'))
    channel_token = Column(String(128), nullable=False, server_default=text('""'))
    account_id = Column(BIGINT(unsigned=True), nullable=False, server_default=text('0'))
    crc32 = Column(INTEGER, nullable=False, server_default=text('0'))

    gmt_modified = Column(TIMESTAMP(True), nullable=False, server_default=text('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))
    gmt_create = Column(TIMESTAMP(True), nullable=False, server_default=text('CURRENT_TIMESTAMP'))
class GlobalInfo(db.Model):
    __talbe__ = 'global_info'
    id = Column(BIGINT(unsigned=True), nullable=False, server_default=text('0'), primary_key=True, autoincrement=False)
    cur_server_id = Column(INTEGER, nullable=False, server_default=text('1'))
    cur_server_user_count = Column(INTEGER, nullable=False, server_default=text('0'))
    clan_incr_id = Column(INTEGER, nullable=False, server_default=text('0'))
    crc32 = Column(INTEGER, nullable=False, server_default=text('0'))

    gmt_modified = Column(TIMESTAMP(True), nullable=False, server_default=text('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))
    gmt_create = Column(TIMESTAMP(True), nullable=False, server_default=text('CURRENT_TIMESTAMP'))
class GlobalVersion(db.Model):
    __talbe__ = 'global_version'
    id = Column(BIGINT(unsigned=True), nullable=False, server_default=text('0'), primary_key=True, autoincrement=False)
    last_version = Column(String(128), nullable=False, server_default=text('""'))
    crc32 = Column(INTEGER, nullable=False, server_default=text('0'))

    gmt_modified = Column(TIMESTAMP(True), nullable=False, server_default=text('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))
    gmt_create = Column(TIMESTAMP(True), nullable=False, server_default=text('CURRENT_TIMESTAMP'))
class NodeInfo(db.Model):
    __talbe__ = 'node_info'
    id = Column(BIGINT(unsigned=True), nullable=False, server_default=text('0'), primary_key=True, autoincrement=False)
    local_ip = Column(String(128), nullable=False, server_default=text('""'), index=True, unique=True)
    node_id = Column(INTEGER(unsigned=True), nullable=False, server_default=text('0'), index=True, unique=True)
    crc32 = Column(INTEGER, nullable=False, server_default=text('0'))

    gmt_modified = Column(TIMESTAMP(True), nullable=False, server_default=text('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))
    gmt_create = Column(TIMESTAMP(True), nullable=False, server_default=text('CURRENT_TIMESTAMP'))
class PlayerInfo(db.Model):
    __talbe__ = 'player_info'
    id = Column(BIGINT(unsigned=True), nullable=False, server_default=text('0'), primary_key=True, autoincrement=False)
    server_id = Column(INTEGER, nullable=False, server_default=text('0'), index=True)
    civil_id = Column(INTEGER, nullable=False, server_default=text('0'))
    name = Column(String(128), nullable=False, server_default=text('""'), index=True)
    name_real = Column(String(64), nullable=False, server_default=text('""'), index=True, unique=True)
    dup_level = Column(INTEGER, nullable=False, server_default=text('0'))
    dup_gold = Column(BIGINT, nullable=False, server_default=text('0'))
    dup_diamond = Column(BIGINT, nullable=False, server_default=text('0'))
    build_queue_count = Column(INTEGER, nullable=False, server_default=text('0'))
    max_build_queue_count = Column(INTEGER, nullable=False, server_default=text('1'))
    dynamic_id_seq = Column(INTEGER, nullable=False, server_default=text('0'))
    random_val = Column(INTEGER, nullable=False, server_default=text('0'))
    daily_reset_time = Column(BIGINT, nullable=False, server_default=text('0'))
    weekly_reset_time = Column(BIGINT, nullable=False, server_default=text('0'))
    got_troop_total_nums = Column(INTEGER, nullable=False, server_default=text('0'))
    passed_tower_id = Column(INTEGER, nullable=False, server_default=text('0'))
    lock_deadline = Column(BIGINT, nullable=False, server_default=text('0'))
    lock_reason = Column(INTEGER, nullable=False, server_default=text('0'))
    daily_task_point = Column(INTEGER, nullable=False, server_default=text('0'))
    weekly_task_point = Column(INTEGER, nullable=False, server_default=text('0'))
    troop_soul = Column(BIGINT, nullable=False, server_default=text('0'))
    last_logout_time = Column(BIGINT, nullable=False, server_default=text('0'))
    def_power = Column(INTEGER, nullable=False, server_default=text('0'))
    icon = Column(INTEGER, nullable=False, server_default=text('0'))
    main_power = Column(INTEGER, nullable=False, server_default=text('0'))
    cur_guide_index = Column(INTEGER, nullable=False, server_default=text('0'))
    guide_state = Column(LargeBinary)
    buildings = Column(LargeBinary)
    items = Column(LargeBinary)
    draws = Column(LargeBinary)
    tasks = Column(LargeBinary)
    bounties = Column(LargeBinary)
    levy_houses = Column(LargeBinary)
    tech_infos = Column(LargeBinary)
    formations = Column(LargeBinary)
    treasure_recved_data = Column(LargeBinary)
    troop_book = Column(LargeBinary)
    device_name_dup = Column(String(128), nullable=False, server_default=text('""'), index=True)
    currency = Column(LargeBinary)
    register_time = Column(BIGINT, nullable=False, server_default=text('0'))
    survey_history = Column(LargeBinary)
    max_fini_main_line_ary = Column(LargeBinary)
    modify_name_num = Column(INTEGER, nullable=False, server_default=text('0'))
    fake_user_id = Column(INTEGER, nullable=False, server_default=text('0'), index=True)
    red_point_ary = Column(LargeBinary)
    total_power = Column(INTEGER, nullable=False, server_default=text('0'))
    auto_layoff = Column(TINYINT, nullable=False, server_default=text('0'))
    tech_queue_count = Column(INTEGER, nullable=False, server_default=text('0'))
    max_tech_queue_count = Column(INTEGER, nullable=False, server_default=text('1'))
    crc32 = Column(INTEGER, nullable=False, server_default=text('0'))

    gmt_modified = Column(TIMESTAMP(True), nullable=False, server_default=text('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))
    gmt_create = Column(TIMESTAMP(True), nullable=False, server_default=text('CURRENT_TIMESTAMP'))
class PlayerInfoActivity(db.Model):
    __talbe__ = 'player_info_activity'
    id = Column(BIGINT(unsigned=True), nullable=False, server_default=text('0'), primary_key=True, autoincrement=False)
    data = Column(LargeBinary)
    crc32 = Column(INTEGER, nullable=False, server_default=text('0'))

    gmt_modified = Column(TIMESTAMP(True), nullable=False, server_default=text('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))
    gmt_create = Column(TIMESTAMP(True), nullable=False, server_default=text('CURRENT_TIMESTAMP'))
class PlayerInfoArena(db.Model):
    __talbe__ = 'player_info_arena'
    id = Column(BIGINT(unsigned=True), nullable=False, server_default=text('0'), primary_key=True, autoincrement=False)
    season_id = Column(BIGINT(unsigned=True), nullable=False, server_default=text('0'), index=True)
    keep_suc = Column(INTEGER, nullable=False, server_default=text('0'))
    arena_battle_num = Column(INTEGER, nullable=False, server_default=text('0'))
    arena_buy_num = Column(INTEGER, nullable=False, server_default=text('0'))
    day_reward = Column(LargeBinary)
    season_reward = Column(LargeBinary)
    battle_log = Column(LargeBinary)
    season_number = Column(INTEGER, nullable=False, server_default=text('0'))
    last_reset_time = Column(BIGINT, nullable=False, server_default=text('0'))
    last_battle_time = Column(BIGINT, nullable=False, server_default=text('0'))
    crc32 = Column(INTEGER, nullable=False, server_default=text('0'))

    gmt_modified = Column(TIMESTAMP(True), nullable=False, server_default=text('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))
    gmt_create = Column(TIMESTAMP(True), nullable=False, server_default=text('CURRENT_TIMESTAMP'))
class PlayerInfoClan(db.Model):
    __talbe__ = 'player_info_clan'
    id = Column(BIGINT(unsigned=True), nullable=False, server_default=text('0'), primary_key=True, autoincrement=False)
    mail_send_count = Column(INTEGER, nullable=False, server_default=text('0'))
    donate_times = Column(INTEGER, nullable=False, server_default=text('0'))
    quit_times = Column(INTEGER, nullable=False, server_default=text('0'))
    cd_expired_time = Column(BIGINT, nullable=False, server_default=text('0'))
    apply_list = Column(LargeBinary)
    boss_infos = Column(LargeBinary)
    crc32 = Column(INTEGER, nullable=False, server_default=text('0'))

    gmt_modified = Column(TIMESTAMP(True), nullable=False, server_default=text('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))
    gmt_create = Column(TIMESTAMP(True), nullable=False, server_default=text('CURRENT_TIMESTAMP'))
class PlayerInfoEquip(db.Model):
    __talbe__ = 'player_info_equip'
    id = Column(BIGINT(unsigned=True), nullable=False, server_default=text('0'), primary_key=True, autoincrement=False)
    equips = Column(LargeBinary)
    crc32 = Column(INTEGER, nullable=False, server_default=text('0'))

    gmt_modified = Column(TIMESTAMP(True), nullable=False, server_default=text('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))
    gmt_create = Column(TIMESTAMP(True), nullable=False, server_default=text('CURRENT_TIMESTAMP'))
class PlayerInfoExt(db.Model):
    __talbe__ = 'player_info_ext'
    id = Column(BIGINT(unsigned=True), nullable=False, server_default=text('0'), primary_key=True, autoincrement=False)
    store_info = Column(LargeBinary)
    mail_queue = Column(LargeBinary)
    gm_mail_buffer = Column(LargeBinary)
    dynamic_mail_id_seq = Column(INTEGER, nullable=False, server_default=text('0'))
    scan_mail_history = Column(LargeBinary)
    main_copy_infos = Column(LargeBinary)
    troop_relationships = Column(LargeBinary)
    war_diff_infos = Column(LargeBinary)
    crc32 = Column(INTEGER, nullable=False, server_default=text('0'))

    gmt_modified = Column(TIMESTAMP(True), nullable=False, server_default=text('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))
    gmt_create = Column(TIMESTAMP(True), nullable=False, server_default=text('CURRENT_TIMESTAMP'))
class PlayerInfoFriend(db.Model):
    __talbe__ = 'player_info_friend'
    id = Column(BIGINT(unsigned=True), nullable=False, server_default=text('0'), primary_key=True, autoincrement=False)
    friend_infos = Column(LargeBinary)
    black_infos = Column(LargeBinary)
    apply_infos = Column(LargeBinary)
    refresh_send_get_time = Column(BIGINT, nullable=False, server_default=text('0'))
    day_send_num = Column(INTEGER, nullable=False, server_default=text('0'))
    day_get_num = Column(INTEGER, nullable=False, server_default=text('0'))
    close_recommend = Column(TINYINT, nullable=False, server_default=text('0'))
    day_apply_num = Column(INTEGER, nullable=False, server_default=text('0'))
    crc32 = Column(INTEGER, nullable=False, server_default=text('0'))

    gmt_modified = Column(TIMESTAMP(True), nullable=False, server_default=text('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))
    gmt_create = Column(TIMESTAMP(True), nullable=False, server_default=text('CURRENT_TIMESTAMP'))
class PlayerInfoMaze(db.Model):
    __talbe__ = 'player_info_maze'
    id = Column(BIGINT(unsigned=True), nullable=False, server_default=text('0'), primary_key=True, autoincrement=False)
    layer_id = Column(INTEGER, nullable=False, server_default=text('0'))
    mode = Column(INTEGER, nullable=False, server_default=text('0'))
    times = Column(INTEGER, nullable=False, server_default=text('0'))
    order_num = Column(INTEGER, nullable=False, server_default=text('0'))
    troop_reduce_hp = Column(LargeBinary)
    relic_objs = Column(LargeBinary)
    artifact_objs = Column(LargeBinary)
    event_ids = Column(LargeBinary)
    event_random_point = Column(LargeBinary)
    points = Column(INTEGER, nullable=False, server_default=text('0'))
    level_event = Column(LargeBinary)
    battle_formation = Column(LargeBinary)
    will_recruit_troop = Column(LargeBinary)
    recruit_random_point = Column(LargeBinary)
    has_recv_reward = Column(INTEGER, nullable=False, server_default=text('0'))
    cur_level_id = Column(INTEGER, nullable=False, server_default=text('0'))
    dead_troops = Column(LargeBinary)
    troop_energy = Column(LargeBinary)
    robot_infos = Column(LargeBinary)
    static_relic_objs = Column(LargeBinary)
    crc32 = Column(INTEGER, nullable=False, server_default=text('0'))

    gmt_modified = Column(TIMESTAMP(True), nullable=False, server_default=text('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))
    gmt_create = Column(TIMESTAMP(True), nullable=False, server_default=text('CURRENT_TIMESTAMP'))
class PlayerInfoTroop(db.Model):
    __talbe__ = 'player_info_troop'
    id = Column(BIGINT(unsigned=True), nullable=False, server_default=text('0'), primary_key=True, autoincrement=False)
    troops = Column(LargeBinary)
    troops_ext = Column(LargeBinary)
    crc32 = Column(INTEGER, nullable=False, server_default=text('0'))

    gmt_modified = Column(TIMESTAMP(True), nullable=False, server_default=text('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))
    gmt_create = Column(TIMESTAMP(True), nullable=False, server_default=text('CURRENT_TIMESTAMP'))
class PlayerShareClanId(db.Model):
    __talbe__ = 'player_share_clan_id'
    id = Column(BIGINT(unsigned=True), nullable=False, server_default=text('0'), primary_key=True, autoincrement=False)
    clan_id = Column(BIGINT(unsigned=True), nullable=False, server_default=text('0'))
    crc32 = Column(INTEGER, nullable=False, server_default=text('0'))

    gmt_modified = Column(TIMESTAMP(True), nullable=False, server_default=text('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))
    gmt_create = Column(TIMESTAMP(True), nullable=False, server_default=text('CURRENT_TIMESTAMP'))
class PlayerShareMail(db.Model):
    __talbe__ = 'player_share_mail'
    id = Column(BIGINT(unsigned=True), nullable=False, server_default=text('0'), primary_key=True, autoincrement=False)
    mail_buffer = Column(LargeBinary)
    last_update_timestamp = Column(BIGINT, nullable=False, server_default=text('0'))
    crc32 = Column(INTEGER, nullable=False, server_default=text('0'))

    gmt_modified = Column(TIMESTAMP(True), nullable=False, server_default=text('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))
    gmt_create = Column(TIMESTAMP(True), nullable=False, server_default=text('CURRENT_TIMESTAMP'))
class ScheduleInfo(db.Model):
    __talbe__ = 'schedule_info'
    id = Column(BIGINT(unsigned=True), nullable=False, server_default=text('0'), primary_key=True, autoincrement=False)
    start_time = Column(BIGINT, nullable=False, server_default=text('0'))
    end_time = Column(BIGINT, nullable=False, server_default=text('0'))
    order_num = Column(INTEGER, nullable=False, server_default=text('0'))
    crc32 = Column(INTEGER, nullable=False, server_default=text('0'))

    gmt_modified = Column(TIMESTAMP(True), nullable=False, server_default=text('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))
    gmt_create = Column(TIMESTAMP(True), nullable=False, server_default=text('CURRENT_TIMESTAMP'))
class TestState(db.Model):
    __talbe__ = 'test_state'
    id = Column(BIGINT(unsigned=True), nullable=False, server_default=text('0'), primary_key=True, autoincrement=False)
    alias = Column(String(128), nullable=False, server_default=text('""'), index=True, unique=True)
    profile = Column(LargeBinary)
    gold = Column(INTEGER, nullable=False, server_default=text('0'))
    base_wid = Column(BIGINT(unsigned=True), nullable=False, server_default=text('0'))
    own_worlds = Column(LargeBinary)
    own_items = Column(LargeBinary)
    test_migration = Column(String(128), nullable=False, server_default=text('""'))
    test_migration2 = Column(INTEGER, nullable=False, server_default=text('0'))
    test_migration4 = Column(INTEGER, nullable=False, server_default=text('0'))
    test_migration5 = Column(INTEGER, nullable=False, server_default=text('0'))
    test_migration64 = Column(BIGINT, nullable=False, server_default=text('0'))
    account_id = Column(BIGINT(unsigned=True), nullable=False, server_default=text('0'))
    account_id2 = Column(BIGINT(unsigned=True), nullable=False, server_default=text('0'))
    crc32 = Column(INTEGER, nullable=False, server_default=text('0'))

    gmt_modified = Column(TIMESTAMP(True), nullable=False, server_default=text('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))
    gmt_create = Column(TIMESTAMP(True), nullable=False, server_default=text('CURRENT_TIMESTAMP'))
