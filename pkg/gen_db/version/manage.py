#encoding: utf-8
# 备注：yy
# 1 migration.py 有pb2db自动生成
# 2 manage.py 可通用
# 3 其他看pb2db md文件

from flask import Flask
from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand
import migration

# 初始化管理器
manager = Manager(migration.app)
# 添加 db 命令，并与 MigrateCommand 绑定
manager.add_command('db', MigrateCommand)

if __name__ == '__main__':
    manager.run()

