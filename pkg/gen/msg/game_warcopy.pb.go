// Code generated by protoc-gen-go. DO NOT EDIT.
// source: msg/game_warcopy.proto

package msg

import (
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

// 战役信息
type WarDiffInfosR struct {
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *WarDiffInfosR) Reset()         { *m = WarDiffInfosR{} }
func (m *WarDiffInfosR) String() string { return proto.CompactTextString(m) }
func (*WarDiffInfosR) ProtoMessage()    {}
func (*WarDiffInfosR) Descriptor() ([]byte, []int) {
	return fileDescriptor_1870c3b6b3e339ee, []int{0}
}

func (m *WarDiffInfosR) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_WarDiffInfosR.Unmarshal(m, b)
}
func (m *WarDiffInfosR) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_WarDiffInfosR.Marshal(b, m, deterministic)
}
func (m *WarDiffInfosR) XXX_Merge(src proto.Message) {
	xxx_messageInfo_WarDiffInfosR.Merge(m, src)
}
func (m *WarDiffInfosR) XXX_Size() int {
	return xxx_messageInfo_WarDiffInfosR.Size(m)
}
func (m *WarDiffInfosR) XXX_DiscardUnknown() {
	xxx_messageInfo_WarDiffInfosR.DiscardUnknown(m)
}

var xxx_messageInfo_WarDiffInfosR proto.InternalMessageInfo

type WarDiffInfosA struct {
	Err                  *ErrorInfo       `protobuf:"bytes,1,opt,name=err,proto3" json:"err,omitempty"`
	WarDiffInfos         []*WarDiffResult `protobuf:"bytes,2,rep,name=warDiffInfos,proto3" json:"warDiffInfos,omitempty"`
	XXX_NoUnkeyedLiteral struct{}         `json:"-"`
	XXX_unrecognized     []byte           `json:"-"`
	XXX_sizecache        int32            `json:"-"`
}

func (m *WarDiffInfosA) Reset()         { *m = WarDiffInfosA{} }
func (m *WarDiffInfosA) String() string { return proto.CompactTextString(m) }
func (*WarDiffInfosA) ProtoMessage()    {}
func (*WarDiffInfosA) Descriptor() ([]byte, []int) {
	return fileDescriptor_1870c3b6b3e339ee, []int{1}
}

func (m *WarDiffInfosA) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_WarDiffInfosA.Unmarshal(m, b)
}
func (m *WarDiffInfosA) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_WarDiffInfosA.Marshal(b, m, deterministic)
}
func (m *WarDiffInfosA) XXX_Merge(src proto.Message) {
	xxx_messageInfo_WarDiffInfosA.Merge(m, src)
}
func (m *WarDiffInfosA) XXX_Size() int {
	return xxx_messageInfo_WarDiffInfosA.Size(m)
}
func (m *WarDiffInfosA) XXX_DiscardUnknown() {
	xxx_messageInfo_WarDiffInfosA.DiscardUnknown(m)
}

var xxx_messageInfo_WarDiffInfosA proto.InternalMessageInfo

func (m *WarDiffInfosA) GetErr() *ErrorInfo {
	if m != nil {
		return m.Err
	}
	return nil
}

func (m *WarDiffInfosA) GetWarDiffInfos() []*WarDiffResult {
	if m != nil {
		return m.WarDiffInfos
	}
	return nil
}

// 宝箱奖励
type WarBoxRewardR struct {
	// 战役ID
	WarId int32 `protobuf:"varint,1,opt,name=warId,proto3" json:"warId,omitempty"`
	// 宝箱ID
	BoxId                int32    `protobuf:"varint,2,opt,name=boxId,proto3" json:"boxId,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *WarBoxRewardR) Reset()         { *m = WarBoxRewardR{} }
func (m *WarBoxRewardR) String() string { return proto.CompactTextString(m) }
func (*WarBoxRewardR) ProtoMessage()    {}
func (*WarBoxRewardR) Descriptor() ([]byte, []int) {
	return fileDescriptor_1870c3b6b3e339ee, []int{2}
}

func (m *WarBoxRewardR) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_WarBoxRewardR.Unmarshal(m, b)
}
func (m *WarBoxRewardR) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_WarBoxRewardR.Marshal(b, m, deterministic)
}
func (m *WarBoxRewardR) XXX_Merge(src proto.Message) {
	xxx_messageInfo_WarBoxRewardR.Merge(m, src)
}
func (m *WarBoxRewardR) XXX_Size() int {
	return xxx_messageInfo_WarBoxRewardR.Size(m)
}
func (m *WarBoxRewardR) XXX_DiscardUnknown() {
	xxx_messageInfo_WarBoxRewardR.DiscardUnknown(m)
}

var xxx_messageInfo_WarBoxRewardR proto.InternalMessageInfo

func (m *WarBoxRewardR) GetWarId() int32 {
	if m != nil {
		return m.WarId
	}
	return 0
}

func (m *WarBoxRewardR) GetBoxId() int32 {
	if m != nil {
		return m.BoxId
	}
	return 0
}

type WarBoxRewardA struct {
	Err *ErrorInfo `protobuf:"bytes,1,opt,name=err,proto3" json:"err,omitempty"`
	// 战役ID
	WarId int32 `protobuf:"varint,2,opt,name=warId,proto3" json:"warId,omitempty"`
	// 宝箱ID
	BoxId                int32    `protobuf:"varint,3,opt,name=boxId,proto3" json:"boxId,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *WarBoxRewardA) Reset()         { *m = WarBoxRewardA{} }
func (m *WarBoxRewardA) String() string { return proto.CompactTextString(m) }
func (*WarBoxRewardA) ProtoMessage()    {}
func (*WarBoxRewardA) Descriptor() ([]byte, []int) {
	return fileDescriptor_1870c3b6b3e339ee, []int{3}
}

func (m *WarBoxRewardA) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_WarBoxRewardA.Unmarshal(m, b)
}
func (m *WarBoxRewardA) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_WarBoxRewardA.Marshal(b, m, deterministic)
}
func (m *WarBoxRewardA) XXX_Merge(src proto.Message) {
	xxx_messageInfo_WarBoxRewardA.Merge(m, src)
}
func (m *WarBoxRewardA) XXX_Size() int {
	return xxx_messageInfo_WarBoxRewardA.Size(m)
}
func (m *WarBoxRewardA) XXX_DiscardUnknown() {
	xxx_messageInfo_WarBoxRewardA.DiscardUnknown(m)
}

var xxx_messageInfo_WarBoxRewardA proto.InternalMessageInfo

func (m *WarBoxRewardA) GetErr() *ErrorInfo {
	if m != nil {
		return m.Err
	}
	return nil
}

func (m *WarBoxRewardA) GetWarId() int32 {
	if m != nil {
		return m.WarId
	}
	return 0
}

func (m *WarBoxRewardA) GetBoxId() int32 {
	if m != nil {
		return m.BoxId
	}
	return 0
}

// 首次进入
type FirstJoinWarR struct {
	// 战役ID
	WarId int32 `protobuf:"varint,1,opt,name=warId,proto3" json:"warId,omitempty"`
	// 章节ID
	ChapterId            int32    `protobuf:"varint,2,opt,name=chapterId,proto3" json:"chapterId,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *FirstJoinWarR) Reset()         { *m = FirstJoinWarR{} }
func (m *FirstJoinWarR) String() string { return proto.CompactTextString(m) }
func (*FirstJoinWarR) ProtoMessage()    {}
func (*FirstJoinWarR) Descriptor() ([]byte, []int) {
	return fileDescriptor_1870c3b6b3e339ee, []int{4}
}

func (m *FirstJoinWarR) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_FirstJoinWarR.Unmarshal(m, b)
}
func (m *FirstJoinWarR) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_FirstJoinWarR.Marshal(b, m, deterministic)
}
func (m *FirstJoinWarR) XXX_Merge(src proto.Message) {
	xxx_messageInfo_FirstJoinWarR.Merge(m, src)
}
func (m *FirstJoinWarR) XXX_Size() int {
	return xxx_messageInfo_FirstJoinWarR.Size(m)
}
func (m *FirstJoinWarR) XXX_DiscardUnknown() {
	xxx_messageInfo_FirstJoinWarR.DiscardUnknown(m)
}

var xxx_messageInfo_FirstJoinWarR proto.InternalMessageInfo

func (m *FirstJoinWarR) GetWarId() int32 {
	if m != nil {
		return m.WarId
	}
	return 0
}

func (m *FirstJoinWarR) GetChapterId() int32 {
	if m != nil {
		return m.ChapterId
	}
	return 0
}

type FirstJoinWarA struct {
	Err                  *ErrorInfo `protobuf:"bytes,1,opt,name=err,proto3" json:"err,omitempty"`
	XXX_NoUnkeyedLiteral struct{}   `json:"-"`
	XXX_unrecognized     []byte     `json:"-"`
	XXX_sizecache        int32      `json:"-"`
}

func (m *FirstJoinWarA) Reset()         { *m = FirstJoinWarA{} }
func (m *FirstJoinWarA) String() string { return proto.CompactTextString(m) }
func (*FirstJoinWarA) ProtoMessage()    {}
func (*FirstJoinWarA) Descriptor() ([]byte, []int) {
	return fileDescriptor_1870c3b6b3e339ee, []int{5}
}

func (m *FirstJoinWarA) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_FirstJoinWarA.Unmarshal(m, b)
}
func (m *FirstJoinWarA) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_FirstJoinWarA.Marshal(b, m, deterministic)
}
func (m *FirstJoinWarA) XXX_Merge(src proto.Message) {
	xxx_messageInfo_FirstJoinWarA.Merge(m, src)
}
func (m *FirstJoinWarA) XXX_Size() int {
	return xxx_messageInfo_FirstJoinWarA.Size(m)
}
func (m *FirstJoinWarA) XXX_DiscardUnknown() {
	xxx_messageInfo_FirstJoinWarA.DiscardUnknown(m)
}

var xxx_messageInfo_FirstJoinWarA proto.InternalMessageInfo

func (m *FirstJoinWarA) GetErr() *ErrorInfo {
	if m != nil {
		return m.Err
	}
	return nil
}

// 战役单项返回
type WarDiffResult struct {
	// 战役Id
	WarId int32 `protobuf:"varint,1,opt,name=warId,proto3" json:"warId,omitempty"`
	// 星级奖励信息
	StarStatus map[int32]RewardReceive `protobuf:"bytes,2,rep,name=starStatus,proto3" json:"starStatus,omitempty" protobuf_key:"varint,1,opt,name=key,proto3" protobuf_val:"varint,2,opt,name=value,proto3,enum=msg.RewardReceive"`
	// 战役章节信息
	WarCopyInfos         []*WarCopyResult `protobuf:"bytes,3,rep,name=warCopyInfos,proto3" json:"warCopyInfos,omitempty"`
	XXX_NoUnkeyedLiteral struct{}         `json:"-"`
	XXX_unrecognized     []byte           `json:"-"`
	XXX_sizecache        int32            `json:"-"`
}

func (m *WarDiffResult) Reset()         { *m = WarDiffResult{} }
func (m *WarDiffResult) String() string { return proto.CompactTextString(m) }
func (*WarDiffResult) ProtoMessage()    {}
func (*WarDiffResult) Descriptor() ([]byte, []int) {
	return fileDescriptor_1870c3b6b3e339ee, []int{6}
}

func (m *WarDiffResult) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_WarDiffResult.Unmarshal(m, b)
}
func (m *WarDiffResult) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_WarDiffResult.Marshal(b, m, deterministic)
}
func (m *WarDiffResult) XXX_Merge(src proto.Message) {
	xxx_messageInfo_WarDiffResult.Merge(m, src)
}
func (m *WarDiffResult) XXX_Size() int {
	return xxx_messageInfo_WarDiffResult.Size(m)
}
func (m *WarDiffResult) XXX_DiscardUnknown() {
	xxx_messageInfo_WarDiffResult.DiscardUnknown(m)
}

var xxx_messageInfo_WarDiffResult proto.InternalMessageInfo

func (m *WarDiffResult) GetWarId() int32 {
	if m != nil {
		return m.WarId
	}
	return 0
}

func (m *WarDiffResult) GetStarStatus() map[int32]RewardReceive {
	if m != nil {
		return m.StarStatus
	}
	return nil
}

func (m *WarDiffResult) GetWarCopyInfos() []*WarCopyResult {
	if m != nil {
		return m.WarCopyInfos
	}
	return nil
}

// 章节单项返回
type WarCopyResult struct {
	// 章节ID
	ChapterId int32 `protobuf:"varint,1,opt,name=chapterId,proto3" json:"chapterId,omitempty"`
	// 章节首次进入
	FirstJoin bool `protobuf:"varint,2,opt,name=firstJoin,proto3" json:"firstJoin,omitempty"`
	// 章节状态(0=不可进入,1=进行中,2=可进入)
	ChapterStatus ChapterStatus `protobuf:"varint,3,opt,name=chapterStatus,proto3,enum=msg.ChapterStatus" json:"chapterStatus,omitempty"`
	// 章节进度
	ChapterScale int32 `protobuf:"varint,4,opt,name=chapterScale,proto3" json:"chapterScale,omitempty"`
	// 章节目标
	ChapterTarget map[int32]bool `protobuf:"bytes,5,rep,name=chapterTarget,proto3" json:"chapterTarget,omitempty" protobuf_key:"varint,1,opt,name=key,proto3" protobuf_val:"varint,2,opt,name=value,proto3"`
	// 章节星级
	ChapterStar int32 `protobuf:"varint,6,opt,name=chapterStar,proto3" json:"chapterStar,omitempty"`
	// 战役关卡列表
	CheckPoints          map[int32]*WarStrongHoldResult `protobuf:"bytes,7,rep,name=checkPoints,proto3" json:"checkPoints,omitempty" protobuf_key:"varint,1,opt,name=key,proto3" protobuf_val:"bytes,2,opt,name=value,proto3"`
	XXX_NoUnkeyedLiteral struct{}                       `json:"-"`
	XXX_unrecognized     []byte                         `json:"-"`
	XXX_sizecache        int32                          `json:"-"`
}

func (m *WarCopyResult) Reset()         { *m = WarCopyResult{} }
func (m *WarCopyResult) String() string { return proto.CompactTextString(m) }
func (*WarCopyResult) ProtoMessage()    {}
func (*WarCopyResult) Descriptor() ([]byte, []int) {
	return fileDescriptor_1870c3b6b3e339ee, []int{7}
}

func (m *WarCopyResult) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_WarCopyResult.Unmarshal(m, b)
}
func (m *WarCopyResult) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_WarCopyResult.Marshal(b, m, deterministic)
}
func (m *WarCopyResult) XXX_Merge(src proto.Message) {
	xxx_messageInfo_WarCopyResult.Merge(m, src)
}
func (m *WarCopyResult) XXX_Size() int {
	return xxx_messageInfo_WarCopyResult.Size(m)
}
func (m *WarCopyResult) XXX_DiscardUnknown() {
	xxx_messageInfo_WarCopyResult.DiscardUnknown(m)
}

var xxx_messageInfo_WarCopyResult proto.InternalMessageInfo

func (m *WarCopyResult) GetChapterId() int32 {
	if m != nil {
		return m.ChapterId
	}
	return 0
}

func (m *WarCopyResult) GetFirstJoin() bool {
	if m != nil {
		return m.FirstJoin
	}
	return false
}

func (m *WarCopyResult) GetChapterStatus() ChapterStatus {
	if m != nil {
		return m.ChapterStatus
	}
	return ChapterStatus_CS_CLOSE
}

func (m *WarCopyResult) GetChapterScale() int32 {
	if m != nil {
		return m.ChapterScale
	}
	return 0
}

func (m *WarCopyResult) GetChapterTarget() map[int32]bool {
	if m != nil {
		return m.ChapterTarget
	}
	return nil
}

func (m *WarCopyResult) GetChapterStar() int32 {
	if m != nil {
		return m.ChapterStar
	}
	return 0
}

func (m *WarCopyResult) GetCheckPoints() map[int32]*WarStrongHoldResult {
	if m != nil {
		return m.CheckPoints
	}
	return nil
}

// 关卡单项返回
type WarStrongHoldResult struct {
	// 关卡号
	HoldId int32 `protobuf:"varint,1,opt,name=holdId,proto3" json:"holdId,omitempty"`
	//  据点列表
	StrongholdInfos      map[int32]*WarStrongHoldInfo `protobuf:"bytes,2,rep,name=strongholdInfos,proto3" json:"strongholdInfos,omitempty" protobuf_key:"varint,1,opt,name=key,proto3" protobuf_val:"bytes,2,opt,name=value,proto3"`
	XXX_NoUnkeyedLiteral struct{}                     `json:"-"`
	XXX_unrecognized     []byte                       `json:"-"`
	XXX_sizecache        int32                        `json:"-"`
}

func (m *WarStrongHoldResult) Reset()         { *m = WarStrongHoldResult{} }
func (m *WarStrongHoldResult) String() string { return proto.CompactTextString(m) }
func (*WarStrongHoldResult) ProtoMessage()    {}
func (*WarStrongHoldResult) Descriptor() ([]byte, []int) {
	return fileDescriptor_1870c3b6b3e339ee, []int{8}
}

func (m *WarStrongHoldResult) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_WarStrongHoldResult.Unmarshal(m, b)
}
func (m *WarStrongHoldResult) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_WarStrongHoldResult.Marshal(b, m, deterministic)
}
func (m *WarStrongHoldResult) XXX_Merge(src proto.Message) {
	xxx_messageInfo_WarStrongHoldResult.Merge(m, src)
}
func (m *WarStrongHoldResult) XXX_Size() int {
	return xxx_messageInfo_WarStrongHoldResult.Size(m)
}
func (m *WarStrongHoldResult) XXX_DiscardUnknown() {
	xxx_messageInfo_WarStrongHoldResult.DiscardUnknown(m)
}

var xxx_messageInfo_WarStrongHoldResult proto.InternalMessageInfo

func (m *WarStrongHoldResult) GetHoldId() int32 {
	if m != nil {
		return m.HoldId
	}
	return 0
}

func (m *WarStrongHoldResult) GetStrongholdInfos() map[int32]*WarStrongHoldInfo {
	if m != nil {
		return m.StrongholdInfos
	}
	return nil
}

// 战役副本战斗返回信息
type WarCopyEndBattleResult struct {
	// 难度ID
	WarId int32 `protobuf:"varint,1,opt,name=warId,proto3" json:"warId,omitempty"`
	// 章节ID
	ChapterId int32 `protobuf:"varint,2,opt,name=chapterId,proto3" json:"chapterId,omitempty"`
	// 据点ID
	StrongholdId int32 `protobuf:"varint,3,opt,name=strongholdId,proto3" json:"strongholdId,omitempty"`
	// 激活新章节
	OpenChapterId int32 `protobuf:"varint,4,opt,name=openChapterId,proto3" json:"openChapterId,omitempty"`
	// 激活新据点
	OpenHoldId           []int32  `protobuf:"varint,5,rep,packed,name=openHoldId,proto3" json:"openHoldId,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *WarCopyEndBattleResult) Reset()         { *m = WarCopyEndBattleResult{} }
func (m *WarCopyEndBattleResult) String() string { return proto.CompactTextString(m) }
func (*WarCopyEndBattleResult) ProtoMessage()    {}
func (*WarCopyEndBattleResult) Descriptor() ([]byte, []int) {
	return fileDescriptor_1870c3b6b3e339ee, []int{9}
}

func (m *WarCopyEndBattleResult) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_WarCopyEndBattleResult.Unmarshal(m, b)
}
func (m *WarCopyEndBattleResult) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_WarCopyEndBattleResult.Marshal(b, m, deterministic)
}
func (m *WarCopyEndBattleResult) XXX_Merge(src proto.Message) {
	xxx_messageInfo_WarCopyEndBattleResult.Merge(m, src)
}
func (m *WarCopyEndBattleResult) XXX_Size() int {
	return xxx_messageInfo_WarCopyEndBattleResult.Size(m)
}
func (m *WarCopyEndBattleResult) XXX_DiscardUnknown() {
	xxx_messageInfo_WarCopyEndBattleResult.DiscardUnknown(m)
}

var xxx_messageInfo_WarCopyEndBattleResult proto.InternalMessageInfo

func (m *WarCopyEndBattleResult) GetWarId() int32 {
	if m != nil {
		return m.WarId
	}
	return 0
}

func (m *WarCopyEndBattleResult) GetChapterId() int32 {
	if m != nil {
		return m.ChapterId
	}
	return 0
}

func (m *WarCopyEndBattleResult) GetStrongholdId() int32 {
	if m != nil {
		return m.StrongholdId
	}
	return 0
}

func (m *WarCopyEndBattleResult) GetOpenChapterId() int32 {
	if m != nil {
		return m.OpenChapterId
	}
	return 0
}

func (m *WarCopyEndBattleResult) GetOpenHoldId() []int32 {
	if m != nil {
		return m.OpenHoldId
	}
	return nil
}

func init() {
	proto.RegisterType((*WarDiffInfosR)(nil), "msg.WarDiffInfosR")
	proto.RegisterType((*WarDiffInfosA)(nil), "msg.WarDiffInfosA")
	proto.RegisterType((*WarBoxRewardR)(nil), "msg.WarBoxRewardR")
	proto.RegisterType((*WarBoxRewardA)(nil), "msg.WarBoxRewardA")
	proto.RegisterType((*FirstJoinWarR)(nil), "msg.FirstJoinWarR")
	proto.RegisterType((*FirstJoinWarA)(nil), "msg.FirstJoinWarA")
	proto.RegisterType((*WarDiffResult)(nil), "msg.WarDiffResult")
	proto.RegisterMapType((map[int32]RewardReceive)(nil), "msg.WarDiffResult.StarStatusEntry")
	proto.RegisterType((*WarCopyResult)(nil), "msg.WarCopyResult")
	proto.RegisterMapType((map[int32]bool)(nil), "msg.WarCopyResult.ChapterTargetEntry")
	proto.RegisterMapType((map[int32]*WarStrongHoldResult)(nil), "msg.WarCopyResult.CheckPointsEntry")
	proto.RegisterType((*WarStrongHoldResult)(nil), "msg.WarStrongHoldResult")
	proto.RegisterMapType((map[int32]*WarStrongHoldInfo)(nil), "msg.WarStrongHoldResult.StrongholdInfosEntry")
	proto.RegisterType((*WarCopyEndBattleResult)(nil), "msg.WarCopyEndBattleResult")
}

func init() { proto.RegisterFile("msg/game_warcopy.proto", fileDescriptor_1870c3b6b3e339ee) }

var fileDescriptor_1870c3b6b3e339ee = []byte{
	// 651 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0x8c, 0x55, 0x5d, 0x6f, 0x12, 0x41,
	0x14, 0xcd, 0xb2, 0x52, 0xeb, 0xa5, 0xb4, 0xcd, 0xd8, 0xe0, 0x86, 0x34, 0x86, 0xac, 0x9a, 0xf4,
	0x41, 0x21, 0xd6, 0xc4, 0x34, 0xfa, 0xa2, 0x20, 0xa6, 0xd5, 0x97, 0xba, 0x98, 0xd4, 0x34, 0x31,
	0xcd, 0xc0, 0x0e, 0xdb, 0x4d, 0x61, 0x87, 0xcc, 0x0c, 0xa5, 0xfc, 0x25, 0x7f, 0x80, 0x3f, 0xc8,
	0xdf, 0xe0, 0x9b, 0x2f, 0x66, 0x3e, 0x58, 0x66, 0x60, 0xdb, 0xf4, 0x8d, 0x39, 0xf7, 0xdc, 0x73,
	0xee, 0x9e, 0x3b, 0xec, 0x42, 0x6d, 0xcc, 0x93, 0x56, 0x82, 0xc7, 0xe4, 0x62, 0x86, 0xd9, 0x80,
	0x4e, 0xe6, 0xcd, 0x09, 0xa3, 0x82, 0x22, 0x7f, 0xcc, 0x93, 0xfa, 0xb6, 0x2c, 0x92, 0x6c, 0x3a,
	0xd6, 0x60, 0x7d, 0x57, 0x9e, 0x63, 0x32, 0x4c, 0x33, 0x62, 0x90, 0x27, 0x0a, 0xe9, 0x5f, 0x70,
	0x81, 0x05, 0xb9, 0xe8, 0x63, 0x6e, 0x0a, 0xe1, 0x0e, 0x54, 0xcf, 0x30, 0xfb, 0x94, 0x0e, 0x87,
	0x27, 0xd9, 0x90, 0xf2, 0x28, 0x4c, 0x5d, 0xe0, 0x23, 0x6a, 0x80, 0x4f, 0x18, 0x0b, 0xbc, 0x86,
	0x77, 0x50, 0x39, 0xdc, 0x6e, 0x8e, 0x79, 0xd2, 0xec, 0x32, 0x46, 0x99, 0x2c, 0x47, 0xb2, 0x84,
	0xde, 0xc2, 0xd6, 0xcc, 0x6a, 0x09, 0x4a, 0x0d, 0xff, 0xa0, 0x72, 0x88, 0x14, 0xd5, 0x68, 0x45,
	0x84, 0x4f, 0x47, 0x22, 0x72, 0x78, 0xe1, 0x7b, 0x65, 0xd5, 0xa6, 0x37, 0x11, 0x99, 0x61, 0x16,
	0x47, 0x68, 0x0f, 0xca, 0x33, 0xcc, 0x4e, 0x62, 0x65, 0x56, 0x8e, 0xf4, 0x41, 0xa2, 0x7d, 0x7a,
	0x73, 0x12, 0x07, 0x25, 0x8d, 0xaa, 0x43, 0xf8, 0xd3, 0x6d, 0xbe, 0xcf, 0x9c, 0xb9, 0x7c, 0xa9,
	0x50, 0xde, 0xb7, 0xe5, 0x3b, 0x50, 0xfd, 0x9c, 0x32, 0x2e, 0xbe, 0xd0, 0x34, 0x3b, 0xc3, 0xec,
	0xb6, 0xd9, 0xf6, 0xe1, 0xd1, 0xe0, 0x12, 0x4f, 0x04, 0x59, 0xca, 0x2e, 0x81, 0xf0, 0xb5, 0x2b,
	0x72, 0x8f, 0x19, 0xc3, 0xbf, 0x5e, 0x9e, 0xbf, 0xce, 0xec, 0x16, 0xe3, 0x36, 0x00, 0x17, 0x98,
	0xf5, 0x04, 0x16, 0xd3, 0x45, 0xe2, 0xe1, 0x7a, 0xe2, 0xcd, 0x5e, 0x4e, 0xea, 0x66, 0x82, 0xcd,
	0x23, 0xab, 0xcb, 0xec, 0xad, 0x43, 0x27, 0x73, 0xbd, 0x37, 0xdf, 0xdd, 0x9b, 0x2c, 0x58, 0x7b,
	0xcb, 0x79, 0xf5, 0x6f, 0xb0, 0xb3, 0x22, 0x8b, 0x76, 0xc1, 0xbf, 0x22, 0x73, 0x33, 0xa2, 0xfc,
	0x89, 0x0e, 0xa0, 0x7c, 0x8d, 0x47, 0x53, 0xa2, 0x52, 0xd9, 0x36, 0xaa, 0x66, 0xd1, 0x64, 0x40,
	0xd2, 0x6b, 0x12, 0x69, 0xc2, 0xbb, 0xd2, 0x91, 0x17, 0xfe, 0xf3, 0xd5, 0x63, 0x2f, 0x2d, 0xdd,
	0x64, 0xbd, 0x95, 0x64, 0x65, 0x75, 0xb8, 0x48, 0x56, 0x39, 0x6c, 0x46, 0x4b, 0x00, 0x1d, 0x41,
	0xd5, 0x50, 0x4d, 0x3e, 0xbe, 0x35, 0x43, 0xc7, 0xae, 0x44, 0x2e, 0x11, 0x85, 0xb0, 0xb5, 0x00,
	0x06, 0x78, 0x44, 0x82, 0x07, 0xca, 0xd8, 0xc1, 0xd0, 0xd7, 0x5c, 0xfd, 0x3b, 0x66, 0x09, 0x11,
	0x41, 0x59, 0xe5, 0xf6, 0x62, 0x3d, 0xb7, 0x85, 0x97, 0xe6, 0xe9, 0x05, 0xb8, 0xbd, 0xa8, 0x01,
	0x95, 0xe5, 0x04, 0x2c, 0xd8, 0x50, 0x7e, 0x36, 0x84, 0xba, 0x92, 0x41, 0x06, 0x57, 0xa7, 0x34,
	0xcd, 0x04, 0x0f, 0x1e, 0x2a, 0xb3, 0x67, 0x85, 0x66, 0x39, 0x4b, 0x5b, 0xd9, 0x7d, 0xf5, 0x0f,
	0x80, 0xd6, 0xa7, 0x29, 0xd8, 0xdb, 0x9e, 0xbd, 0xb7, 0x4d, 0x6b, 0x47, 0xf5, 0x1f, 0xb0, 0xbb,
	0x6a, 0x51, 0xd0, 0xdf, 0xb4, 0xfb, 0x2b, 0x87, 0xc1, 0x62, 0xd0, 0x9e, 0x60, 0x34, 0x4b, 0x8e,
	0xe9, 0x28, 0x36, 0x77, 0xca, 0xda, 0xfe, 0x1f, 0x0f, 0x1e, 0x17, 0x50, 0x50, 0x0d, 0x36, 0x2e,
	0xe9, 0x28, 0xce, 0x2f, 0x80, 0x39, 0xa1, 0x33, 0xd8, 0xe1, 0x8a, 0xab, 0xce, 0xd6, 0x3b, 0xe7,
	0xd5, 0x6d, 0x6e, 0xcd, 0x9e, 0xcb, 0xd7, 0x01, 0xad, 0xaa, 0xd4, 0xcf, 0x61, 0xaf, 0x88, 0x58,
	0xf0, 0x98, 0x2f, 0xdd, 0xc7, 0xac, 0xad, 0x1b, 0xab, 0xff, 0xb4, 0xf5, 0x90, 0xbf, 0x3d, 0xa8,
	0x99, 0x85, 0x75, 0xb3, 0xb8, 0x8d, 0x85, 0x18, 0x91, 0x3b, 0xff, 0xe2, 0x77, 0xbe, 0x5b, 0xe4,
	0x4d, 0xb5, 0xa6, 0x5f, 0xbc, 0xbd, 0x1c, 0x0c, 0x3d, 0x87, 0x2a, 0x9d, 0x90, 0xac, 0x93, 0xab,
	0xe8, 0xeb, 0xec, 0x82, 0xe8, 0x29, 0x80, 0x04, 0x8e, 0xb5, 0x8e, 0xbc, 0xcc, 0xe5, 0xc8, 0x42,
	0xda, 0xfb, 0xe7, 0x88, 0x13, 0x76, 0x4d, 0x58, 0x6b, 0x72, 0x95, 0xb4, 0x12, 0x92, 0xb5, 0xc6,
	0x3c, 0xf9, 0x55, 0x2a, 0x9f, 0xca, 0x0f, 0x48, 0x7f, 0x43, 0x7d, 0x47, 0xde, 0xfc, 0x0f, 0x00,
	0x00, 0xff, 0xff, 0x74, 0x9d, 0xd1, 0x27, 0xa1, 0x06, 0x00, 0x00,
}
