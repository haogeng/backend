// Code generated by protoc-gen-go. DO NOT EDIT.
// source: rawdata/GoodsConfig.proto

package rawdata

import (
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

type GoodsConfig struct {
	XTags_ []string `protobuf:"bytes,1,rep,name=_tags_,json=tags,proto3" json:"_tags_,omitempty"`
	// 是否弹出二次确认
	ConfirmTwice bool `protobuf:"varint,2,opt,name=confirmTwice,proto3" json:"confirmTwice,omitempty"`
	// 商品价值
	Cost string `protobuf:"bytes,3,opt,name=cost,proto3" json:"cost,omitempty"`
	// 左上折扣力度图标显示，纯显示用，不填不显示图标
	Discount int32 `protobuf:"varint,11,opt,name=discount,proto3" json:"discount,omitempty"`
	// 商品ID
	Id int32 `protobuf:"varint,4,opt,name=id,proto3" json:"id,omitempty"`
	// 商品限购次数(-1，无限购买、0，会当成售罄)
	MaxNum int32 `protobuf:"varint,5,opt,name=maxNum,proto3" json:"maxNum,omitempty"`
	// 商品名称
	Name int32 `protobuf:"varint,6,opt,name=name,proto3" json:"name,omitempty"`
	// 商品索引道具
	StaticObj string `protobuf:"bytes,7,opt,name=staticObj,proto3" json:"staticObj,omitempty"`
	// 商品Tips
	Tips int32 `protobuf:"varint,8,opt,name=tips,proto3" json:"tips,omitempty"`
	// 商品栏右上特殊UI
	UpperRightUI         string   `protobuf:"bytes,10,opt,name=upperRightUI,proto3" json:"upperRightUI,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *GoodsConfig) Reset()         { *m = GoodsConfig{} }
func (m *GoodsConfig) String() string { return proto.CompactTextString(m) }
func (*GoodsConfig) ProtoMessage()    {}
func (*GoodsConfig) Descriptor() ([]byte, []int) {
	return fileDescriptor_7ab6aab73f967366, []int{0}
}

func (m *GoodsConfig) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_GoodsConfig.Unmarshal(m, b)
}
func (m *GoodsConfig) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_GoodsConfig.Marshal(b, m, deterministic)
}
func (m *GoodsConfig) XXX_Merge(src proto.Message) {
	xxx_messageInfo_GoodsConfig.Merge(m, src)
}
func (m *GoodsConfig) XXX_Size() int {
	return xxx_messageInfo_GoodsConfig.Size(m)
}
func (m *GoodsConfig) XXX_DiscardUnknown() {
	xxx_messageInfo_GoodsConfig.DiscardUnknown(m)
}

var xxx_messageInfo_GoodsConfig proto.InternalMessageInfo

func (m *GoodsConfig) GetXTags_() []string {
	if m != nil {
		return m.XTags_
	}
	return nil
}

func (m *GoodsConfig) GetConfirmTwice() bool {
	if m != nil {
		return m.ConfirmTwice
	}
	return false
}

func (m *GoodsConfig) GetCost() string {
	if m != nil {
		return m.Cost
	}
	return ""
}

func (m *GoodsConfig) GetDiscount() int32 {
	if m != nil {
		return m.Discount
	}
	return 0
}

func (m *GoodsConfig) GetId() int32 {
	if m != nil {
		return m.Id
	}
	return 0
}

func (m *GoodsConfig) GetMaxNum() int32 {
	if m != nil {
		return m.MaxNum
	}
	return 0
}

func (m *GoodsConfig) GetName() int32 {
	if m != nil {
		return m.Name
	}
	return 0
}

func (m *GoodsConfig) GetStaticObj() string {
	if m != nil {
		return m.StaticObj
	}
	return ""
}

func (m *GoodsConfig) GetTips() int32 {
	if m != nil {
		return m.Tips
	}
	return 0
}

func (m *GoodsConfig) GetUpperRightUI() string {
	if m != nil {
		return m.UpperRightUI
	}
	return ""
}

func init() {
	proto.RegisterType((*GoodsConfig)(nil), "rawdata.GoodsConfig")
}

func init() { proto.RegisterFile("rawdata/GoodsConfig.proto", fileDescriptor_7ab6aab73f967366) }

var fileDescriptor_7ab6aab73f967366 = []byte{
	// 255 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0x54, 0x90, 0xb1, 0x4e, 0xc3, 0x30,
	0x18, 0x84, 0x15, 0x37, 0x4d, 0xd3, 0xbf, 0x88, 0xc1, 0x42, 0xd5, 0x0f, 0x62, 0x88, 0x32, 0x65,
	0x22, 0x03, 0x6f, 0x00, 0x03, 0x62, 0x01, 0x29, 0x82, 0x85, 0xa5, 0x72, 0x6d, 0x63, 0x0c, 0x4a,
	0x6c, 0xd9, 0x0e, 0xe5, 0x15, 0x78, 0x15, 0x9e, 0x12, 0xd9, 0x04, 0x68, 0xb7, 0xbb, 0xef, 0x74,
	0xb6, 0xfe, 0x83, 0x53, 0xc7, 0x76, 0x82, 0x05, 0xd6, 0xde, 0x18, 0x23, 0xfc, 0xb5, 0x19, 0x9e,
	0xb5, 0xba, 0xb0, 0xce, 0x04, 0x43, 0x17, 0x53, 0x54, 0x7f, 0x12, 0x58, 0xed, 0xc5, 0xf4, 0x04,
	0x8a, 0x4d, 0x60, 0xca, 0x6f, 0x30, 0xab, 0x66, 0xcd, 0xb2, 0xcb, 0xa3, 0xa1, 0x35, 0x1c, 0xf1,
	0x98, 0xbb, 0xfe, 0x61, 0xa7, 0xb9, 0x44, 0x52, 0x65, 0x4d, 0xd9, 0x1d, 0x30, 0x4a, 0x21, 0xe7,
	0xc6, 0x07, 0x9c, 0x55, 0x59, 0xec, 0x45, 0x4d, 0xcf, 0xa0, 0x14, 0xda, 0x73, 0x33, 0x0e, 0x01,
	0x57, 0x55, 0xd6, 0xcc, 0xbb, 0x3f, 0x4f, 0x8f, 0x81, 0x68, 0x81, 0x79, 0xa2, 0x44, 0x0b, 0xba,
	0x86, 0xa2, 0x67, 0x1f, 0x77, 0x63, 0x8f, 0xf3, 0xc4, 0x26, 0x17, 0xdf, 0x1d, 0x58, 0x2f, 0xb1,
	0x48, 0x34, 0x69, 0x7a, 0x0e, 0x4b, 0x1f, 0x58, 0xd0, 0xfc, 0x7e, 0xfb, 0x8a, 0x8b, 0xf4, 0xe1,
	0x3f, 0x88, 0x8d, 0xa0, 0xad, 0xc7, 0xf2, 0xa7, 0x11, 0x75, 0xbc, 0x60, 0xb4, 0x56, 0xba, 0x4e,
	0xab, 0x97, 0xf0, 0x78, 0x8b, 0x90, 0x4a, 0x07, 0xec, 0xaa, 0x7e, 0x5a, 0x7b, 0xe9, 0xde, 0xa5,
	0x6b, 0xed, 0x9b, 0x6a, 0x95, 0x1c, 0xda, 0x69, 0xa5, 0x2f, 0xf2, 0xbb, 0xd7, 0xb6, 0x48, 0xfb,
	0x5d, 0x7e, 0x07, 0x00, 0x00, 0xff, 0xff, 0x31, 0x6a, 0x61, 0xc2, 0x5c, 0x01, 0x00, 0x00,
}
