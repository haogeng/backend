// Code generated by protoc-gen-go. DO NOT EDIT.
// source: rawdata/ObjList_conf.proto

package rawdata

import (
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

type ObjListConf struct {
	ObjLists             map[int32]*ObjList `protobuf:"bytes,1,rep,name=ObjLists,json=objLists,proto3" json:"ObjLists,omitempty" protobuf_key:"varint,1,opt,name=key,proto3" protobuf_val:"bytes,2,opt,name=value,proto3"`
	XXX_NoUnkeyedLiteral struct{}           `json:"-"`
	XXX_unrecognized     []byte             `json:"-"`
	XXX_sizecache        int32              `json:"-"`
}

func (m *ObjListConf) Reset()         { *m = ObjListConf{} }
func (m *ObjListConf) String() string { return proto.CompactTextString(m) }
func (*ObjListConf) ProtoMessage()    {}
func (*ObjListConf) Descriptor() ([]byte, []int) {
	return fileDescriptor_f8e5dd26e5a0e7d0, []int{0}
}

func (m *ObjListConf) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_ObjListConf.Unmarshal(m, b)
}
func (m *ObjListConf) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_ObjListConf.Marshal(b, m, deterministic)
}
func (m *ObjListConf) XXX_Merge(src proto.Message) {
	xxx_messageInfo_ObjListConf.Merge(m, src)
}
func (m *ObjListConf) XXX_Size() int {
	return xxx_messageInfo_ObjListConf.Size(m)
}
func (m *ObjListConf) XXX_DiscardUnknown() {
	xxx_messageInfo_ObjListConf.DiscardUnknown(m)
}

var xxx_messageInfo_ObjListConf proto.InternalMessageInfo

func (m *ObjListConf) GetObjLists() map[int32]*ObjList {
	if m != nil {
		return m.ObjLists
	}
	return nil
}

func init() {
	proto.RegisterType((*ObjListConf)(nil), "rawdata.ObjList_conf")
	proto.RegisterMapType((map[int32]*ObjList)(nil), "rawdata.ObjList_conf.ObjListsEntry")
}

func init() { proto.RegisterFile("rawdata/ObjList_conf.proto", fileDescriptor_f8e5dd26e5a0e7d0) }

var fileDescriptor_f8e5dd26e5a0e7d0 = []byte{
	// 180 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xe2, 0x92, 0x2a, 0x4a, 0x2c, 0x4f,
	0x49, 0x2c, 0x49, 0xd4, 0xf7, 0x4f, 0xca, 0xf2, 0xc9, 0x2c, 0x2e, 0x89, 0x4f, 0xce, 0xcf, 0x4b,
	0xd3, 0x2b, 0x28, 0xca, 0x2f, 0xc9, 0x17, 0x62, 0x87, 0xca, 0x49, 0x89, 0xa2, 0x29, 0x82, 0xc8,
	0x2b, 0xcd, 0x63, 0xe4, 0xe2, 0x41, 0xd6, 0x26, 0x64, 0xcf, 0xc5, 0x01, 0xe5, 0x17, 0x4b, 0x30,
	0x2a, 0x30, 0x6b, 0x70, 0x1b, 0x29, 0xeb, 0x41, 0xb5, 0xea, 0xa1, 0x98, 0x0f, 0x53, 0xe5, 0x9a,
	0x57, 0x52, 0x54, 0x19, 0xc4, 0x91, 0x0f, 0xe5, 0x4a, 0xf9, 0x72, 0xf1, 0xa2, 0x48, 0x09, 0x09,
	0x70, 0x31, 0x67, 0xa7, 0x56, 0x4a, 0x30, 0x2a, 0x30, 0x6a, 0xb0, 0x06, 0x81, 0x98, 0x42, 0x6a,
	0x5c, 0xac, 0x65, 0x89, 0x39, 0xa5, 0xa9, 0x12, 0x4c, 0x0a, 0x8c, 0x1a, 0xdc, 0x46, 0x02, 0xe8,
	0x16, 0x04, 0x41, 0xa4, 0xad, 0x98, 0x2c, 0x18, 0x9d, 0x94, 0xa2, 0xc4, 0x8a, 0x53, 0x8b, 0xca,
	0x52, 0x8b, 0xf4, 0x0b, 0xb2, 0xd3, 0xf5, 0xd3, 0x53, 0xf3, 0xf4, 0xa1, 0x8a, 0x57, 0x31, 0xc1,
	0xfc, 0x96, 0xc4, 0x06, 0xf6, 0x8b, 0x31, 0x20, 0x00, 0x00, 0xff, 0xff, 0x90, 0xb5, 0x9c, 0x00,
	0x09, 0x01, 0x00, 0x00,
}
