// Code generated by protoc-gen-go. DO NOT EDIT.
// source: rawdata/GoodsConfig_conf.proto

package rawdata

import (
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

type GoodsConfigConf struct {
	GoodsConfigs         map[int32]*GoodsConfig `protobuf:"bytes,1,rep,name=GoodsConfigs,json=goodsConfigs,proto3" json:"GoodsConfigs,omitempty" protobuf_key:"varint,1,opt,name=key,proto3" protobuf_val:"bytes,2,opt,name=value,proto3"`
	XXX_NoUnkeyedLiteral struct{}               `json:"-"`
	XXX_unrecognized     []byte                 `json:"-"`
	XXX_sizecache        int32                  `json:"-"`
}

func (m *GoodsConfigConf) Reset()         { *m = GoodsConfigConf{} }
func (m *GoodsConfigConf) String() string { return proto.CompactTextString(m) }
func (*GoodsConfigConf) ProtoMessage()    {}
func (*GoodsConfigConf) Descriptor() ([]byte, []int) {
	return fileDescriptor_75bdaed9cebc582f, []int{0}
}

func (m *GoodsConfigConf) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_GoodsConfigConf.Unmarshal(m, b)
}
func (m *GoodsConfigConf) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_GoodsConfigConf.Marshal(b, m, deterministic)
}
func (m *GoodsConfigConf) XXX_Merge(src proto.Message) {
	xxx_messageInfo_GoodsConfigConf.Merge(m, src)
}
func (m *GoodsConfigConf) XXX_Size() int {
	return xxx_messageInfo_GoodsConfigConf.Size(m)
}
func (m *GoodsConfigConf) XXX_DiscardUnknown() {
	xxx_messageInfo_GoodsConfigConf.DiscardUnknown(m)
}

var xxx_messageInfo_GoodsConfigConf proto.InternalMessageInfo

func (m *GoodsConfigConf) GetGoodsConfigs() map[int32]*GoodsConfig {
	if m != nil {
		return m.GoodsConfigs
	}
	return nil
}

func init() {
	proto.RegisterType((*GoodsConfigConf)(nil), "rawdata.GoodsConfig_conf")
	proto.RegisterMapType((map[int32]*GoodsConfig)(nil), "rawdata.GoodsConfig_conf.GoodsConfigsEntry")
}

func init() { proto.RegisterFile("rawdata/GoodsConfig_conf.proto", fileDescriptor_75bdaed9cebc582f) }

var fileDescriptor_75bdaed9cebc582f = []byte{
	// 186 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xe2, 0x92, 0x2b, 0x4a, 0x2c, 0x4f,
	0x49, 0x2c, 0x49, 0xd4, 0x77, 0xcf, 0xcf, 0x4f, 0x29, 0x76, 0xce, 0xcf, 0x4b, 0xcb, 0x4c, 0x8f,
	0x4f, 0xce, 0xcf, 0x4b, 0xd3, 0x2b, 0x28, 0xca, 0x2f, 0xc9, 0x17, 0x62, 0x87, 0xca, 0x4b, 0x49,
	0x62, 0x51, 0x08, 0x51, 0xa3, 0xb4, 0x8b, 0x91, 0x4b, 0x00, 0x5d, 0xbb, 0x90, 0x3f, 0x17, 0x0f,
	0x92, 0x58, 0xb1, 0x04, 0xa3, 0x02, 0xb3, 0x06, 0xb7, 0x91, 0xb6, 0x1e, 0xd4, 0x18, 0x3d, 0x0c,
	0xfb, 0x90, 0x55, 0xbb, 0xe6, 0x95, 0x14, 0x55, 0x06, 0xf1, 0xa4, 0x23, 0x09, 0x49, 0x85, 0x72,
	0x09, 0x62, 0x28, 0x11, 0x12, 0xe0, 0x62, 0xce, 0x4e, 0xad, 0x94, 0x60, 0x54, 0x60, 0xd4, 0x60,
	0x0d, 0x02, 0x31, 0x85, 0xb4, 0xb8, 0x58, 0xcb, 0x12, 0x73, 0x4a, 0x53, 0x25, 0x98, 0x14, 0x18,
	0x35, 0xb8, 0x8d, 0x44, 0xb0, 0x59, 0x18, 0x04, 0x51, 0x62, 0xc5, 0x64, 0xc1, 0xe8, 0xa4, 0x14,
	0x25, 0x56, 0x9c, 0x5a, 0x54, 0x96, 0x5a, 0xa4, 0x5f, 0x90, 0x9d, 0xae, 0x9f, 0x9e, 0x9a, 0xa7,
	0x0f, 0xd5, 0xb0, 0x8a, 0x09, 0xe6, 0xf7, 0x24, 0x36, 0xb0, 0x3f, 0x8d, 0x01, 0x01, 0x00, 0x00,
	0xff, 0xff, 0xff, 0xe8, 0x0f, 0x4c, 0x2d, 0x01, 0x00, 0x00,
}
