// Code generated by protoc-gen-go. DO NOT EDIT.
// source: rawdata/TavernLevelConfig.proto

package rawdata

import (
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

type TavernLevelConfig struct {
	XTags_ []string `protobuf:"bytes,1,rep,name=_tags_,json=tags,proto3" json:"_tags_,omitempty"`
	// 升级条件
	Condition []int32 `protobuf:"varint,2,rep,packed,name=condition,proto3" json:"condition,omitempty"`
	// 等级ID
	Id int32 `protobuf:"varint,3,opt,name=id,proto3" json:"id,omitempty"`
	// 可刷新任务星级
	QuestStar []int32 `protobuf:"varint,4,rep,packed,name=questStar,proto3" json:"questStar,omitempty"`
	// 每日个人悬赏
	SoloQuest int32 `protobuf:"varint,5,opt,name=soloQuest,proto3" json:"soloQuest,omitempty"`
	// 每日团队悬赏
	TeamQuest            int32    `protobuf:"varint,6,opt,name=teamQuest,proto3" json:"teamQuest,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *TavernLevelConfig) Reset()         { *m = TavernLevelConfig{} }
func (m *TavernLevelConfig) String() string { return proto.CompactTextString(m) }
func (*TavernLevelConfig) ProtoMessage()    {}
func (*TavernLevelConfig) Descriptor() ([]byte, []int) {
	return fileDescriptor_2c18a8184d458cc0, []int{0}
}

func (m *TavernLevelConfig) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_TavernLevelConfig.Unmarshal(m, b)
}
func (m *TavernLevelConfig) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_TavernLevelConfig.Marshal(b, m, deterministic)
}
func (m *TavernLevelConfig) XXX_Merge(src proto.Message) {
	xxx_messageInfo_TavernLevelConfig.Merge(m, src)
}
func (m *TavernLevelConfig) XXX_Size() int {
	return xxx_messageInfo_TavernLevelConfig.Size(m)
}
func (m *TavernLevelConfig) XXX_DiscardUnknown() {
	xxx_messageInfo_TavernLevelConfig.DiscardUnknown(m)
}

var xxx_messageInfo_TavernLevelConfig proto.InternalMessageInfo

func (m *TavernLevelConfig) GetXTags_() []string {
	if m != nil {
		return m.XTags_
	}
	return nil
}

func (m *TavernLevelConfig) GetCondition() []int32 {
	if m != nil {
		return m.Condition
	}
	return nil
}

func (m *TavernLevelConfig) GetId() int32 {
	if m != nil {
		return m.Id
	}
	return 0
}

func (m *TavernLevelConfig) GetQuestStar() []int32 {
	if m != nil {
		return m.QuestStar
	}
	return nil
}

func (m *TavernLevelConfig) GetSoloQuest() int32 {
	if m != nil {
		return m.SoloQuest
	}
	return 0
}

func (m *TavernLevelConfig) GetTeamQuest() int32 {
	if m != nil {
		return m.TeamQuest
	}
	return 0
}

func init() {
	proto.RegisterType((*TavernLevelConfig)(nil), "rawdata.TavernLevelConfig")
}

func init() { proto.RegisterFile("rawdata/TavernLevelConfig.proto", fileDescriptor_2c18a8184d458cc0) }

var fileDescriptor_2c18a8184d458cc0 = []byte{
	// 207 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xe2, 0x92, 0x2f, 0x4a, 0x2c, 0x4f,
	0x49, 0x2c, 0x49, 0xd4, 0x0f, 0x49, 0x2c, 0x4b, 0x2d, 0xca, 0xf3, 0x49, 0x2d, 0x4b, 0xcd, 0x71,
	0xce, 0xcf, 0x4b, 0xcb, 0x4c, 0xd7, 0x2b, 0x28, 0xca, 0x2f, 0xc9, 0x17, 0x62, 0x87, 0x2a, 0x50,
	0xda, 0xc9, 0xc8, 0x25, 0x88, 0xa1, 0x48, 0x48, 0x84, 0x8b, 0x2d, 0xbe, 0x24, 0x31, 0xbd, 0x38,
	0x5e, 0x82, 0x51, 0x81, 0x59, 0x83, 0x33, 0x88, 0x05, 0xc4, 0x11, 0x52, 0xe0, 0xe2, 0x4c, 0xce,
	0xcf, 0x4b, 0xc9, 0x2c, 0xc9, 0xcc, 0xcf, 0x93, 0x60, 0x52, 0x60, 0xd6, 0x60, 0x75, 0x62, 0x12,
	0x60, 0x0c, 0x42, 0x08, 0x0a, 0xf1, 0x71, 0x31, 0x65, 0xa6, 0x48, 0x30, 0x2b, 0x30, 0x6a, 0xb0,
	0x06, 0x31, 0x65, 0xa6, 0x80, 0x74, 0x14, 0x96, 0xa6, 0x16, 0x97, 0x04, 0x97, 0x24, 0x16, 0x49,
	0xb0, 0x20, 0x74, 0xc0, 0x05, 0x85, 0x64, 0xb8, 0x38, 0x8b, 0xf3, 0x73, 0xf2, 0x03, 0x41, 0x02,
	0x12, 0xac, 0x60, 0x8d, 0x08, 0x01, 0x90, 0x6c, 0x49, 0x6a, 0x62, 0x2e, 0x44, 0x96, 0x0d, 0x22,
	0x0b, 0x17, 0x70, 0x52, 0x8a, 0x12, 0x2b, 0x4e, 0x2d, 0x2a, 0x4b, 0x2d, 0xd2, 0x2f, 0xc8, 0x4e,
	0xd7, 0x4f, 0x4f, 0xcd, 0xd3, 0x87, 0xfa, 0x6a, 0x15, 0x13, 0xcc, 0x7f, 0x49, 0x6c, 0x60, 0xff,
	0x1a, 0x03, 0x02, 0x00, 0x00, 0xff, 0xff, 0x2f, 0x55, 0xa0, 0x6d, 0x12, 0x01, 0x00, 0x00,
}
