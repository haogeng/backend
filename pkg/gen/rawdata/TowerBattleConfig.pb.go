// Code generated by protoc-gen-go. DO NOT EDIT.
// source: rawdata/TowerBattleConfig.proto

package rawdata

import (
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

type TowerBattleConfig struct {
	XTags_ []string `protobuf:"bytes,1,rep,name=_tags_,json=tags,proto3" json:"_tags_,omitempty"`
	// 据点Id
	BattleId int32 `protobuf:"varint,2,opt,name=battleId,proto3" json:"battleId,omitempty"`
	// 关卡ID，同时也是层数，必须连续且从1开始
	Id int32 `protobuf:"varint,4,opt,name=id,proto3" json:"id,omitempty"`
	// 关卡形象
	Model string `protobuf:"bytes,8,opt,name=model,proto3" json:"model,omitempty"`
	// 关卡奖励
	Reward string `protobuf:"bytes,6,opt,name=reward,proto3" json:"reward,omitempty"`
	// 关卡奖励预览
	RewardShow string `protobuf:"bytes,7,opt,name=rewardShow,proto3" json:"rewardShow,omitempty"`
	// 关卡类型，1-普通战斗关卡；2-BOSS战斗关卡，目前只用于前端区分显示效果
	Type                 int32    `protobuf:"varint,9,opt,name=type,proto3" json:"type,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *TowerBattleConfig) Reset()         { *m = TowerBattleConfig{} }
func (m *TowerBattleConfig) String() string { return proto.CompactTextString(m) }
func (*TowerBattleConfig) ProtoMessage()    {}
func (*TowerBattleConfig) Descriptor() ([]byte, []int) {
	return fileDescriptor_14708616512637ad, []int{0}
}

func (m *TowerBattleConfig) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_TowerBattleConfig.Unmarshal(m, b)
}
func (m *TowerBattleConfig) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_TowerBattleConfig.Marshal(b, m, deterministic)
}
func (m *TowerBattleConfig) XXX_Merge(src proto.Message) {
	xxx_messageInfo_TowerBattleConfig.Merge(m, src)
}
func (m *TowerBattleConfig) XXX_Size() int {
	return xxx_messageInfo_TowerBattleConfig.Size(m)
}
func (m *TowerBattleConfig) XXX_DiscardUnknown() {
	xxx_messageInfo_TowerBattleConfig.DiscardUnknown(m)
}

var xxx_messageInfo_TowerBattleConfig proto.InternalMessageInfo

func (m *TowerBattleConfig) GetXTags_() []string {
	if m != nil {
		return m.XTags_
	}
	return nil
}

func (m *TowerBattleConfig) GetBattleId() int32 {
	if m != nil {
		return m.BattleId
	}
	return 0
}

func (m *TowerBattleConfig) GetId() int32 {
	if m != nil {
		return m.Id
	}
	return 0
}

func (m *TowerBattleConfig) GetModel() string {
	if m != nil {
		return m.Model
	}
	return ""
}

func (m *TowerBattleConfig) GetReward() string {
	if m != nil {
		return m.Reward
	}
	return ""
}

func (m *TowerBattleConfig) GetRewardShow() string {
	if m != nil {
		return m.RewardShow
	}
	return ""
}

func (m *TowerBattleConfig) GetType() int32 {
	if m != nil {
		return m.Type
	}
	return 0
}

func init() {
	proto.RegisterType((*TowerBattleConfig)(nil), "rawdata.TowerBattleConfig")
}

func init() { proto.RegisterFile("rawdata/TowerBattleConfig.proto", fileDescriptor_14708616512637ad) }

var fileDescriptor_14708616512637ad = []byte{
	// 212 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xe2, 0x92, 0x2f, 0x4a, 0x2c, 0x4f,
	0x49, 0x2c, 0x49, 0xd4, 0x0f, 0xc9, 0x2f, 0x4f, 0x2d, 0x72, 0x4a, 0x2c, 0x29, 0xc9, 0x49, 0x75,
	0xce, 0xcf, 0x4b, 0xcb, 0x4c, 0xd7, 0x2b, 0x28, 0xca, 0x2f, 0xc9, 0x17, 0x62, 0x87, 0x2a, 0x50,
	0xda, 0xce, 0xc8, 0x25, 0x88, 0xa1, 0x48, 0x48, 0x84, 0x8b, 0x2d, 0xbe, 0x24, 0x31, 0xbd, 0x38,
	0x5e, 0x82, 0x51, 0x81, 0x59, 0x83, 0x33, 0x88, 0x05, 0xc4, 0x11, 0x92, 0xe2, 0xe2, 0x48, 0x02,
	0xab, 0xf2, 0x4c, 0x91, 0x60, 0x52, 0x60, 0xd4, 0x60, 0x0d, 0x82, 0xf3, 0x85, 0xf8, 0xb8, 0x98,
	0x32, 0x53, 0x24, 0x58, 0xc0, 0xa2, 0x4c, 0x99, 0x29, 0x42, 0x22, 0x5c, 0xac, 0xb9, 0xf9, 0x29,
	0xa9, 0x39, 0x12, 0x1c, 0x0a, 0x8c, 0x1a, 0x9c, 0x41, 0x10, 0x8e, 0x90, 0x18, 0x17, 0x5b, 0x51,
	0x6a, 0x79, 0x62, 0x51, 0x8a, 0x04, 0x1b, 0x58, 0x18, 0xca, 0x13, 0x92, 0xe3, 0xe2, 0x82, 0xb0,
	0x82, 0x33, 0xf2, 0xcb, 0x25, 0xd8, 0xc1, 0x72, 0x48, 0x22, 0x42, 0x42, 0x5c, 0x2c, 0x25, 0x95,
	0x05, 0xa9, 0x12, 0x9c, 0x60, 0xf3, 0xc1, 0x6c, 0x27, 0xa5, 0x28, 0xb1, 0xe2, 0xd4, 0xa2, 0xb2,
	0xd4, 0x22, 0xfd, 0x82, 0xec, 0x74, 0xfd, 0xf4, 0xd4, 0x3c, 0x7d, 0xa8, 0x9f, 0x56, 0x31, 0xc1,
	0x7c, 0x97, 0xc4, 0x06, 0xf6, 0xad, 0x31, 0x20, 0x00, 0x00, 0xff, 0xff, 0x9b, 0x48, 0xfd, 0x69,
	0x10, 0x01, 0x00, 0x00,
}
