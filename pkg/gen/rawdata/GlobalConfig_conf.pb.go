// Code generated by protoc-gen-go. DO NOT EDIT.
// source: rawdata/GlobalConfig_conf.proto

package rawdata

import (
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

type GlobalConfigConf struct {
	GlobalConfigs        map[int32]*GlobalConfig `protobuf:"bytes,1,rep,name=GlobalConfigs,json=globalConfigs,proto3" json:"GlobalConfigs,omitempty" protobuf_key:"varint,1,opt,name=key,proto3" protobuf_val:"bytes,2,opt,name=value,proto3"`
	XXX_NoUnkeyedLiteral struct{}                `json:"-"`
	XXX_unrecognized     []byte                  `json:"-"`
	XXX_sizecache        int32                   `json:"-"`
}

func (m *GlobalConfigConf) Reset()         { *m = GlobalConfigConf{} }
func (m *GlobalConfigConf) String() string { return proto.CompactTextString(m) }
func (*GlobalConfigConf) ProtoMessage()    {}
func (*GlobalConfigConf) Descriptor() ([]byte, []int) {
	return fileDescriptor_78a59d074a4af82e, []int{0}
}

func (m *GlobalConfigConf) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_GlobalConfigConf.Unmarshal(m, b)
}
func (m *GlobalConfigConf) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_GlobalConfigConf.Marshal(b, m, deterministic)
}
func (m *GlobalConfigConf) XXX_Merge(src proto.Message) {
	xxx_messageInfo_GlobalConfigConf.Merge(m, src)
}
func (m *GlobalConfigConf) XXX_Size() int {
	return xxx_messageInfo_GlobalConfigConf.Size(m)
}
func (m *GlobalConfigConf) XXX_DiscardUnknown() {
	xxx_messageInfo_GlobalConfigConf.DiscardUnknown(m)
}

var xxx_messageInfo_GlobalConfigConf proto.InternalMessageInfo

func (m *GlobalConfigConf) GetGlobalConfigs() map[int32]*GlobalConfig {
	if m != nil {
		return m.GlobalConfigs
	}
	return nil
}

func init() {
	proto.RegisterType((*GlobalConfigConf)(nil), "rawdata.GlobalConfig_conf")
	proto.RegisterMapType((map[int32]*GlobalConfig)(nil), "rawdata.GlobalConfig_conf.GlobalConfigsEntry")
}

func init() { proto.RegisterFile("rawdata/GlobalConfig_conf.proto", fileDescriptor_78a59d074a4af82e) }

var fileDescriptor_78a59d074a4af82e = []byte{
	// 187 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xe2, 0x92, 0x2f, 0x4a, 0x2c, 0x4f,
	0x49, 0x2c, 0x49, 0xd4, 0x77, 0xcf, 0xc9, 0x4f, 0x4a, 0xcc, 0x71, 0xce, 0xcf, 0x4b, 0xcb, 0x4c,
	0x8f, 0x4f, 0xce, 0xcf, 0x4b, 0xd3, 0x2b, 0x28, 0xca, 0x2f, 0xc9, 0x17, 0x62, 0x87, 0x2a, 0x90,
	0x92, 0xc2, 0xa6, 0x12, 0xa2, 0x48, 0xe9, 0x20, 0x23, 0x97, 0x20, 0x86, 0x01, 0x42, 0xc1, 0x5c,
	0xbc, 0xc8, 0x82, 0xc5, 0x12, 0x8c, 0x0a, 0xcc, 0x1a, 0xdc, 0x46, 0xba, 0x7a, 0x50, 0x93, 0xf4,
	0x30, 0xed, 0x44, 0x51, 0xef, 0x9a, 0x57, 0x52, 0x54, 0x19, 0xc4, 0x9b, 0x8e, 0x2c, 0x26, 0x15,
	0xce, 0x25, 0x84, 0xa9, 0x48, 0x48, 0x80, 0x8b, 0x39, 0x3b, 0xb5, 0x52, 0x82, 0x51, 0x81, 0x51,
	0x83, 0x35, 0x08, 0xc4, 0x14, 0xd2, 0xe6, 0x62, 0x2d, 0x4b, 0xcc, 0x29, 0x4d, 0x95, 0x60, 0x52,
	0x60, 0xd4, 0xe0, 0x36, 0x12, 0xc5, 0x6a, 0x69, 0x10, 0x44, 0x8d, 0x15, 0x93, 0x05, 0xa3, 0x93,
	0x52, 0x94, 0x58, 0x71, 0x6a, 0x51, 0x59, 0x6a, 0x91, 0x7e, 0x41, 0x76, 0xba, 0x7e, 0x7a, 0x6a,
	0x9e, 0x3e, 0x54, 0xc7, 0x2a, 0x26, 0x58, 0x18, 0x24, 0xb1, 0x81, 0xbd, 0x6b, 0x0c, 0x08, 0x00,
	0x00, 0xff, 0xff, 0x2b, 0xbe, 0x3a, 0x64, 0x36, 0x01, 0x00, 0x00,
}
