// Code generated by protoc-gen-go. DO NOT EDIT.
// source: rawdata/ArenaRankStageConfig_conf.proto

package rawdata

import (
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

type ArenaRankStageConfigConf struct {
	ArenaRankStageConfigs map[int32]*ArenaRankStageConfig `protobuf:"bytes,1,rep,name=ArenaRankStageConfigs,json=arenaRankStageConfigs,proto3" json:"ArenaRankStageConfigs,omitempty" protobuf_key:"varint,1,opt,name=key,proto3" protobuf_val:"bytes,2,opt,name=value,proto3"`
	XXX_NoUnkeyedLiteral  struct{}                        `json:"-"`
	XXX_unrecognized      []byte                          `json:"-"`
	XXX_sizecache         int32                           `json:"-"`
}

func (m *ArenaRankStageConfigConf) Reset()         { *m = ArenaRankStageConfigConf{} }
func (m *ArenaRankStageConfigConf) String() string { return proto.CompactTextString(m) }
func (*ArenaRankStageConfigConf) ProtoMessage()    {}
func (*ArenaRankStageConfigConf) Descriptor() ([]byte, []int) {
	return fileDescriptor_cd7b8aa2710ec439, []int{0}
}

func (m *ArenaRankStageConfigConf) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_ArenaRankStageConfigConf.Unmarshal(m, b)
}
func (m *ArenaRankStageConfigConf) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_ArenaRankStageConfigConf.Marshal(b, m, deterministic)
}
func (m *ArenaRankStageConfigConf) XXX_Merge(src proto.Message) {
	xxx_messageInfo_ArenaRankStageConfigConf.Merge(m, src)
}
func (m *ArenaRankStageConfigConf) XXX_Size() int {
	return xxx_messageInfo_ArenaRankStageConfigConf.Size(m)
}
func (m *ArenaRankStageConfigConf) XXX_DiscardUnknown() {
	xxx_messageInfo_ArenaRankStageConfigConf.DiscardUnknown(m)
}

var xxx_messageInfo_ArenaRankStageConfigConf proto.InternalMessageInfo

func (m *ArenaRankStageConfigConf) GetArenaRankStageConfigs() map[int32]*ArenaRankStageConfig {
	if m != nil {
		return m.ArenaRankStageConfigs
	}
	return nil
}

func init() {
	proto.RegisterType((*ArenaRankStageConfigConf)(nil), "rawdata.ArenaRankStageConfig_conf")
	proto.RegisterMapType((map[int32]*ArenaRankStageConfig)(nil), "rawdata.ArenaRankStageConfig_conf.ArenaRankStageConfigsEntry")
}

func init() {
	proto.RegisterFile("rawdata/ArenaRankStageConfig_conf.proto", fileDescriptor_cd7b8aa2710ec439)
}

var fileDescriptor_cd7b8aa2710ec439 = []byte{
	// 196 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xe2, 0x52, 0x2f, 0x4a, 0x2c, 0x4f,
	0x49, 0x2c, 0x49, 0xd4, 0x77, 0x2c, 0x4a, 0xcd, 0x4b, 0x0c, 0x4a, 0xcc, 0xcb, 0x0e, 0x2e, 0x49,
	0x4c, 0x4f, 0x75, 0xce, 0xcf, 0x4b, 0xcb, 0x4c, 0x8f, 0x4f, 0xce, 0xcf, 0x4b, 0xd3, 0x2b, 0x28,
	0xca, 0x2f, 0xc9, 0x17, 0x62, 0x87, 0x2a, 0x94, 0x52, 0xc2, 0xa7, 0x03, 0xa2, 0x58, 0xe9, 0x27,
	0x23, 0x97, 0x24, 0x4e, 0x03, 0x85, 0x8a, 0xb9, 0x44, 0xb1, 0x49, 0x16, 0x4b, 0x30, 0x2a, 0x30,
	0x6b, 0x70, 0x1b, 0xd9, 0xea, 0x41, 0x6d, 0xd0, 0xc3, 0xed, 0x26, 0xac, 0xfa, 0x5d, 0xf3, 0x4a,
	0x8a, 0x2a, 0x83, 0x44, 0x13, 0xb1, 0xc9, 0x49, 0xa5, 0x73, 0x49, 0xe1, 0xd6, 0x24, 0x24, 0xc0,
	0xc5, 0x9c, 0x9d, 0x5a, 0x29, 0xc1, 0xa8, 0xc0, 0xa8, 0xc1, 0x1a, 0x04, 0x62, 0x0a, 0x19, 0x73,
	0xb1, 0x96, 0x25, 0xe6, 0x94, 0xa6, 0x4a, 0x30, 0x29, 0x30, 0x6a, 0x70, 0x1b, 0xc9, 0xe2, 0x75,
	0x54, 0x10, 0x44, 0xad, 0x15, 0x93, 0x05, 0xa3, 0x93, 0x52, 0x94, 0x58, 0x71, 0x6a, 0x51, 0x59,
	0x6a, 0x91, 0x7e, 0x41, 0x76, 0xba, 0x7e, 0x7a, 0x6a, 0x9e, 0x3e, 0x54, 0xe7, 0x2a, 0x26, 0x58,
	0x18, 0x26, 0xb1, 0x81, 0x83, 0xc9, 0x18, 0x10, 0x00, 0x00, 0xff, 0xff, 0x27, 0xa2, 0xd5, 0x7e,
	0x7e, 0x01, 0x00, 0x00,
}
