// Code generated by protoc-gen-go. DO NOT EDIT.
// source: rawdata/BuildingShowConfig_conf.proto

package rawdata

import (
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

type BuildingShowConfigConf struct {
	BuildingShowConfigs  map[int32]*BuildingShowConfig `protobuf:"bytes,1,rep,name=BuildingShowConfigs,json=buildingShowConfigs,proto3" json:"BuildingShowConfigs,omitempty" protobuf_key:"varint,1,opt,name=key,proto3" protobuf_val:"bytes,2,opt,name=value,proto3"`
	XXX_NoUnkeyedLiteral struct{}                      `json:"-"`
	XXX_unrecognized     []byte                        `json:"-"`
	XXX_sizecache        int32                         `json:"-"`
}

func (m *BuildingShowConfigConf) Reset()         { *m = BuildingShowConfigConf{} }
func (m *BuildingShowConfigConf) String() string { return proto.CompactTextString(m) }
func (*BuildingShowConfigConf) ProtoMessage()    {}
func (*BuildingShowConfigConf) Descriptor() ([]byte, []int) {
	return fileDescriptor_9ee1435a79e514d1, []int{0}
}

func (m *BuildingShowConfigConf) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_BuildingShowConfigConf.Unmarshal(m, b)
}
func (m *BuildingShowConfigConf) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_BuildingShowConfigConf.Marshal(b, m, deterministic)
}
func (m *BuildingShowConfigConf) XXX_Merge(src proto.Message) {
	xxx_messageInfo_BuildingShowConfigConf.Merge(m, src)
}
func (m *BuildingShowConfigConf) XXX_Size() int {
	return xxx_messageInfo_BuildingShowConfigConf.Size(m)
}
func (m *BuildingShowConfigConf) XXX_DiscardUnknown() {
	xxx_messageInfo_BuildingShowConfigConf.DiscardUnknown(m)
}

var xxx_messageInfo_BuildingShowConfigConf proto.InternalMessageInfo

func (m *BuildingShowConfigConf) GetBuildingShowConfigs() map[int32]*BuildingShowConfig {
	if m != nil {
		return m.BuildingShowConfigs
	}
	return nil
}

func init() {
	proto.RegisterType((*BuildingShowConfigConf)(nil), "rawdata.BuildingShowConfig_conf")
	proto.RegisterMapType((map[int32]*BuildingShowConfig)(nil), "rawdata.BuildingShowConfig_conf.BuildingShowConfigsEntry")
}

func init() {
	proto.RegisterFile("rawdata/BuildingShowConfig_conf.proto", fileDescriptor_9ee1435a79e514d1)
}

var fileDescriptor_9ee1435a79e514d1 = []byte{
	// 194 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xe2, 0x52, 0x2d, 0x4a, 0x2c, 0x4f,
	0x49, 0x2c, 0x49, 0xd4, 0x77, 0x2a, 0xcd, 0xcc, 0x49, 0xc9, 0xcc, 0x4b, 0x0f, 0xce, 0xc8, 0x2f,
	0x77, 0xce, 0xcf, 0x4b, 0xcb, 0x4c, 0x8f, 0x4f, 0xce, 0xcf, 0x4b, 0xd3, 0x2b, 0x28, 0xca, 0x2f,
	0xc9, 0x17, 0x62, 0x87, 0x2a, 0x93, 0x52, 0xc0, 0xad, 0x1e, 0xa2, 0x54, 0xe9, 0x35, 0x23, 0x97,
	0x38, 0x0e, 0xc3, 0x84, 0xb2, 0xb9, 0x84, 0x31, 0xa5, 0x8a, 0x25, 0x18, 0x15, 0x98, 0x35, 0xb8,
	0x8d, 0x2c, 0xf5, 0xa0, 0x66, 0xeb, 0xe1, 0x72, 0x0b, 0x16, 0xbd, 0xae, 0x79, 0x25, 0x45, 0x95,
	0x41, 0xc2, 0x49, 0x98, 0x32, 0x52, 0xc9, 0x5c, 0x12, 0xb8, 0x34, 0x08, 0x09, 0x70, 0x31, 0x67,
	0xa7, 0x56, 0x4a, 0x30, 0x2a, 0x30, 0x6a, 0xb0, 0x06, 0x81, 0x98, 0x42, 0x86, 0x5c, 0xac, 0x65,
	0x89, 0x39, 0xa5, 0xa9, 0x12, 0x4c, 0x0a, 0x8c, 0x1a, 0xdc, 0x46, 0xd2, 0x78, 0x1c, 0x13, 0x04,
	0x51, 0x69, 0xc5, 0x64, 0xc1, 0xe8, 0xa4, 0x14, 0x25, 0x56, 0x9c, 0x5a, 0x54, 0x96, 0x5a, 0xa4,
	0x5f, 0x90, 0x9d, 0xae, 0x9f, 0x9e, 0x9a, 0xa7, 0x0f, 0xd5, 0xb7, 0x8a, 0x09, 0x16, 0x66, 0x49,
	0x6c, 0xe0, 0x80, 0x31, 0x06, 0x04, 0x00, 0x00, 0xff, 0xff, 0x57, 0x0c, 0x2a, 0xfa, 0x6c, 0x01,
	0x00, 0x00,
}
