// Code generated by protoc-gen-go. DO NOT EDIT.
// source: rawdata/NPCPropertyConfig_conf.proto

package rawdata

import (
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

type NPCPropertyConfigConf struct {
	NPCPropertyConfigs   map[int32]*NPCPropertyConfig `protobuf:"bytes,1,rep,name=NPCPropertyConfigs,json=nPCPropertyConfigs,proto3" json:"NPCPropertyConfigs,omitempty" protobuf_key:"varint,1,opt,name=key,proto3" protobuf_val:"bytes,2,opt,name=value,proto3"`
	XXX_NoUnkeyedLiteral struct{}                     `json:"-"`
	XXX_unrecognized     []byte                       `json:"-"`
	XXX_sizecache        int32                        `json:"-"`
}

func (m *NPCPropertyConfigConf) Reset()         { *m = NPCPropertyConfigConf{} }
func (m *NPCPropertyConfigConf) String() string { return proto.CompactTextString(m) }
func (*NPCPropertyConfigConf) ProtoMessage()    {}
func (*NPCPropertyConfigConf) Descriptor() ([]byte, []int) {
	return fileDescriptor_9a6b4b2b10bac436, []int{0}
}

func (m *NPCPropertyConfigConf) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_NPCPropertyConfigConf.Unmarshal(m, b)
}
func (m *NPCPropertyConfigConf) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_NPCPropertyConfigConf.Marshal(b, m, deterministic)
}
func (m *NPCPropertyConfigConf) XXX_Merge(src proto.Message) {
	xxx_messageInfo_NPCPropertyConfigConf.Merge(m, src)
}
func (m *NPCPropertyConfigConf) XXX_Size() int {
	return xxx_messageInfo_NPCPropertyConfigConf.Size(m)
}
func (m *NPCPropertyConfigConf) XXX_DiscardUnknown() {
	xxx_messageInfo_NPCPropertyConfigConf.DiscardUnknown(m)
}

var xxx_messageInfo_NPCPropertyConfigConf proto.InternalMessageInfo

func (m *NPCPropertyConfigConf) GetNPCPropertyConfigs() map[int32]*NPCPropertyConfig {
	if m != nil {
		return m.NPCPropertyConfigs
	}
	return nil
}

func init() {
	proto.RegisterType((*NPCPropertyConfigConf)(nil), "rawdata.NPCPropertyConfig_conf")
	proto.RegisterMapType((map[int32]*NPCPropertyConfig)(nil), "rawdata.NPCPropertyConfig_conf.NPCPropertyConfigsEntry")
}

func init() {
	proto.RegisterFile("rawdata/NPCPropertyConfig_conf.proto", fileDescriptor_9a6b4b2b10bac436)
}

var fileDescriptor_9a6b4b2b10bac436 = []byte{
	// 193 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xe2, 0x52, 0x29, 0x4a, 0x2c, 0x4f,
	0x49, 0x2c, 0x49, 0xd4, 0xf7, 0x0b, 0x70, 0x0e, 0x28, 0xca, 0x2f, 0x48, 0x2d, 0x2a, 0xa9, 0x74,
	0xce, 0xcf, 0x4b, 0xcb, 0x4c, 0x8f, 0x4f, 0xce, 0xcf, 0x4b, 0xd3, 0x2b, 0x28, 0xca, 0x2f, 0xc9,
	0x17, 0x62, 0x87, 0xaa, 0x92, 0x92, 0xc7, 0xa9, 0x1c, 0xa2, 0x52, 0xe9, 0x09, 0x23, 0x97, 0x18,
	0x76, 0xa3, 0x84, 0xd2, 0xb9, 0x84, 0x30, 0x64, 0x8a, 0x25, 0x18, 0x15, 0x98, 0x35, 0xb8, 0x8d,
	0xcc, 0xf5, 0xa0, 0x06, 0xeb, 0xe1, 0x70, 0x07, 0xa6, 0x4e, 0xd7, 0xbc, 0x92, 0xa2, 0xca, 0x20,
	0xa1, 0x3c, 0x0c, 0x09, 0xa9, 0x44, 0x2e, 0x71, 0x1c, 0xca, 0x85, 0x04, 0xb8, 0x98, 0xb3, 0x53,
	0x2b, 0x25, 0x18, 0x15, 0x18, 0x35, 0x58, 0x83, 0x40, 0x4c, 0x21, 0x03, 0x2e, 0xd6, 0xb2, 0xc4,
	0x9c, 0xd2, 0x54, 0x09, 0x26, 0x05, 0x46, 0x0d, 0x6e, 0x23, 0x29, 0xdc, 0x0e, 0x09, 0x82, 0x28,
	0xb4, 0x62, 0xb2, 0x60, 0x74, 0x52, 0x8a, 0x12, 0x2b, 0x4e, 0x2d, 0x2a, 0x4b, 0x2d, 0xd2, 0x2f,
	0xc8, 0x4e, 0xd7, 0x4f, 0x4f, 0xcd, 0xd3, 0x87, 0x6a, 0x5b, 0xc5, 0x04, 0x0b, 0xab, 0x24, 0x36,
	0x70, 0x88, 0x18, 0x03, 0x02, 0x00, 0x00, 0xff, 0xff, 0xc3, 0x65, 0x5a, 0xd0, 0x63, 0x01, 0x00,
	0x00,
}
