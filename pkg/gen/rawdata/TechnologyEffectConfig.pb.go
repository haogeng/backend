// Code generated by protoc-gen-go. DO NOT EDIT.
// source: rawdata/TechnologyEffectConfig.proto

package rawdata

import (
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

type TechnologyEffectConfig struct {
	XTags_ []string `protobuf:"bytes,1,rep,name=_tags_,json=tags,proto3" json:"_tags_,omitempty"`
	// 科技效果id
	Id int32 `protobuf:"varint,2,opt,name=id,proto3" json:"id,omitempty"`
	// 科技实例列表
	TechnologyId         []int32  `protobuf:"varint,3,rep,packed,name=technologyId,proto3" json:"technologyId,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *TechnologyEffectConfig) Reset()         { *m = TechnologyEffectConfig{} }
func (m *TechnologyEffectConfig) String() string { return proto.CompactTextString(m) }
func (*TechnologyEffectConfig) ProtoMessage()    {}
func (*TechnologyEffectConfig) Descriptor() ([]byte, []int) {
	return fileDescriptor_32108493f513447d, []int{0}
}

func (m *TechnologyEffectConfig) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_TechnologyEffectConfig.Unmarshal(m, b)
}
func (m *TechnologyEffectConfig) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_TechnologyEffectConfig.Marshal(b, m, deterministic)
}
func (m *TechnologyEffectConfig) XXX_Merge(src proto.Message) {
	xxx_messageInfo_TechnologyEffectConfig.Merge(m, src)
}
func (m *TechnologyEffectConfig) XXX_Size() int {
	return xxx_messageInfo_TechnologyEffectConfig.Size(m)
}
func (m *TechnologyEffectConfig) XXX_DiscardUnknown() {
	xxx_messageInfo_TechnologyEffectConfig.DiscardUnknown(m)
}

var xxx_messageInfo_TechnologyEffectConfig proto.InternalMessageInfo

func (m *TechnologyEffectConfig) GetXTags_() []string {
	if m != nil {
		return m.XTags_
	}
	return nil
}

func (m *TechnologyEffectConfig) GetId() int32 {
	if m != nil {
		return m.Id
	}
	return 0
}

func (m *TechnologyEffectConfig) GetTechnologyId() []int32 {
	if m != nil {
		return m.TechnologyId
	}
	return nil
}

func init() {
	proto.RegisterType((*TechnologyEffectConfig)(nil), "rawdata.TechnologyEffectConfig")
}

func init() {
	proto.RegisterFile("rawdata/TechnologyEffectConfig.proto", fileDescriptor_32108493f513447d)
}

var fileDescriptor_32108493f513447d = []byte{
	// 161 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xe2, 0x52, 0x29, 0x4a, 0x2c, 0x4f,
	0x49, 0x2c, 0x49, 0xd4, 0x0f, 0x49, 0x4d, 0xce, 0xc8, 0xcb, 0xcf, 0xc9, 0x4f, 0xaf, 0x74, 0x4d,
	0x4b, 0x4b, 0x4d, 0x2e, 0x71, 0xce, 0xcf, 0x4b, 0xcb, 0x4c, 0xd7, 0x2b, 0x28, 0xca, 0x2f, 0xc9,
	0x17, 0x62, 0x87, 0xaa, 0x52, 0x4a, 0xe3, 0x12, 0xc3, 0xae, 0x50, 0x48, 0x84, 0x8b, 0x2d, 0xbe,
	0x24, 0x31, 0xbd, 0x38, 0x5e, 0x82, 0x51, 0x81, 0x59, 0x83, 0x33, 0x88, 0x05, 0xc4, 0x11, 0xe2,
	0xe3, 0x62, 0xca, 0x4c, 0x91, 0x60, 0x52, 0x60, 0xd4, 0x60, 0x0d, 0x62, 0xca, 0x4c, 0x11, 0x52,
	0xe3, 0xe2, 0x29, 0x81, 0xeb, 0xf7, 0x4c, 0x91, 0x60, 0x56, 0x60, 0xd6, 0x60, 0x75, 0x62, 0x12,
	0x60, 0x0c, 0x42, 0x11, 0x77, 0x52, 0x8a, 0x12, 0x2b, 0x4e, 0x2d, 0x2a, 0x4b, 0x2d, 0xd2, 0x2f,
	0xc8, 0x4e, 0xd7, 0x4f, 0x4f, 0xcd, 0xd3, 0x87, 0xba, 0x60, 0x15, 0x13, 0xcc, 0x2d, 0x49, 0x6c,
	0x60, 0xb7, 0x19, 0x03, 0x02, 0x00, 0x00, 0xff, 0xff, 0xc6, 0xb3, 0x27, 0xeb, 0xc3, 0x00, 0x00,
	0x00,
}
