// Code generated by protoc-gen-go. DO NOT EDIT.
// source: rawdata/MazeRefreshConfig.proto

package rawdata

import (
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

type MazeRefreshConfig struct {
	XTags_ []string `protobuf:"bytes,1,rep,name=_tags_,json=tags,proto3" json:"_tags_,omitempty"`
	// 随机圣器库（掉落表ID）
	ArtifactDropId int32 `protobuf:"varint,12,opt,name=artifactDropId,proto3" json:"artifactDropId,omitempty"`
	// 随机遗物库（掉落表ID）
	DropId int32                                         `protobuf:"varint,10,opt,name=dropId,proto3" json:"dropId,omitempty"`
	Event  map[int32]*MazeRefreshConfigEventComplexValue `protobuf:"bytes,2,rep,name=event,proto3" json:"event,omitempty" protobuf_key:"varint,1,opt,name=key,proto3" protobuf_val:"bytes,2,opt,name=value,proto3"`
	// 困难战斗奖励
	HardReward string `protobuf:"bytes,7,opt,name=hardReward,proto3" json:"hardReward,omitempty"`
	// 困难难度系数
	// (除1W)
	HardValue int32 `protobuf:"varint,3,opt,name=hardValue,proto3" json:"hardValue,omitempty"`
	// 关卡ID，同时也是层数，必须连续且从1开始
	Id int32 `protobuf:"varint,4,opt,name=id,proto3" json:"id,omitempty"`
	// 简单战斗奖励
	NormalReward string `protobuf:"bytes,8,opt,name=normalReward,proto3" json:"normalReward,omitempty"`
	// 简单难度系数(除1W)
	NormalValue int32 `protobuf:"varint,6,opt,name=normalValue,proto3" json:"normalValue,omitempty"`
	// 前N次机器人阵容
	// (times,id)
	RobotGet             map[int32]int32 `protobuf:"bytes,9,rep,name=robotGet,proto3" json:"robotGet,omitempty" protobuf_key:"varint,1,opt,name=key,proto3" protobuf_val:"varint,2,opt,name=value,proto3"`
	XXX_NoUnkeyedLiteral struct{}        `json:"-"`
	XXX_unrecognized     []byte          `json:"-"`
	XXX_sizecache        int32           `json:"-"`
}

func (m *MazeRefreshConfig) Reset()         { *m = MazeRefreshConfig{} }
func (m *MazeRefreshConfig) String() string { return proto.CompactTextString(m) }
func (*MazeRefreshConfig) ProtoMessage()    {}
func (*MazeRefreshConfig) Descriptor() ([]byte, []int) {
	return fileDescriptor_758f567e635d4ed8, []int{0}
}

func (m *MazeRefreshConfig) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_MazeRefreshConfig.Unmarshal(m, b)
}
func (m *MazeRefreshConfig) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_MazeRefreshConfig.Marshal(b, m, deterministic)
}
func (m *MazeRefreshConfig) XXX_Merge(src proto.Message) {
	xxx_messageInfo_MazeRefreshConfig.Merge(m, src)
}
func (m *MazeRefreshConfig) XXX_Size() int {
	return xxx_messageInfo_MazeRefreshConfig.Size(m)
}
func (m *MazeRefreshConfig) XXX_DiscardUnknown() {
	xxx_messageInfo_MazeRefreshConfig.DiscardUnknown(m)
}

var xxx_messageInfo_MazeRefreshConfig proto.InternalMessageInfo

func (m *MazeRefreshConfig) GetXTags_() []string {
	if m != nil {
		return m.XTags_
	}
	return nil
}

func (m *MazeRefreshConfig) GetArtifactDropId() int32 {
	if m != nil {
		return m.ArtifactDropId
	}
	return 0
}

func (m *MazeRefreshConfig) GetDropId() int32 {
	if m != nil {
		return m.DropId
	}
	return 0
}

func (m *MazeRefreshConfig) GetEvent() map[int32]*MazeRefreshConfigEventComplexValue {
	if m != nil {
		return m.Event
	}
	return nil
}

func (m *MazeRefreshConfig) GetHardReward() string {
	if m != nil {
		return m.HardReward
	}
	return ""
}

func (m *MazeRefreshConfig) GetHardValue() int32 {
	if m != nil {
		return m.HardValue
	}
	return 0
}

func (m *MazeRefreshConfig) GetId() int32 {
	if m != nil {
		return m.Id
	}
	return 0
}

func (m *MazeRefreshConfig) GetNormalReward() string {
	if m != nil {
		return m.NormalReward
	}
	return ""
}

func (m *MazeRefreshConfig) GetNormalValue() int32 {
	if m != nil {
		return m.NormalValue
	}
	return 0
}

func (m *MazeRefreshConfig) GetRobotGet() map[int32]int32 {
	if m != nil {
		return m.RobotGet
	}
	return nil
}

type MazeRefreshConfigEventComplexValue struct {
	Value                []int32  `protobuf:"varint,1,rep,packed,name=value,proto3" json:"value,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *MazeRefreshConfigEventComplexValue) Reset()         { *m = MazeRefreshConfigEventComplexValue{} }
func (m *MazeRefreshConfigEventComplexValue) String() string { return proto.CompactTextString(m) }
func (*MazeRefreshConfigEventComplexValue) ProtoMessage()    {}
func (*MazeRefreshConfigEventComplexValue) Descriptor() ([]byte, []int) {
	return fileDescriptor_758f567e635d4ed8, []int{0, 0}
}

func (m *MazeRefreshConfigEventComplexValue) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_MazeRefreshConfigEventComplexValue.Unmarshal(m, b)
}
func (m *MazeRefreshConfigEventComplexValue) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_MazeRefreshConfigEventComplexValue.Marshal(b, m, deterministic)
}
func (m *MazeRefreshConfigEventComplexValue) XXX_Merge(src proto.Message) {
	xxx_messageInfo_MazeRefreshConfigEventComplexValue.Merge(m, src)
}
func (m *MazeRefreshConfigEventComplexValue) XXX_Size() int {
	return xxx_messageInfo_MazeRefreshConfigEventComplexValue.Size(m)
}
func (m *MazeRefreshConfigEventComplexValue) XXX_DiscardUnknown() {
	xxx_messageInfo_MazeRefreshConfigEventComplexValue.DiscardUnknown(m)
}

var xxx_messageInfo_MazeRefreshConfigEventComplexValue proto.InternalMessageInfo

func (m *MazeRefreshConfigEventComplexValue) GetValue() []int32 {
	if m != nil {
		return m.Value
	}
	return nil
}

func init() {
	proto.RegisterType((*MazeRefreshConfig)(nil), "rawdata.MazeRefreshConfig")
	proto.RegisterMapType((map[int32]*MazeRefreshConfigEventComplexValue)(nil), "rawdata.MazeRefreshConfig.EventEntry")
	proto.RegisterMapType((map[int32]int32)(nil), "rawdata.MazeRefreshConfig.RobotGetEntry")
	proto.RegisterType((*MazeRefreshConfigEventComplexValue)(nil), "rawdata.MazeRefreshConfig.eventComplexValue")
}

func init() { proto.RegisterFile("rawdata/MazeRefreshConfig.proto", fileDescriptor_758f567e635d4ed8) }

var fileDescriptor_758f567e635d4ed8 = []byte{
	// 369 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0x7c, 0x52, 0xcf, 0x4e, 0xfa, 0x40,
	0x18, 0x4c, 0x5b, 0x5a, 0xe8, 0x07, 0x3f, 0xf2, 0x63, 0x43, 0xc8, 0x86, 0x18, 0x6d, 0x48, 0x34,
	0x35, 0x31, 0x34, 0xc1, 0x8b, 0x91, 0x1b, 0x7f, 0x62, 0x3c, 0x78, 0xd9, 0x83, 0x07, 0x2f, 0x64,
	0xb1, 0xdb, 0xd2, 0x00, 0xdd, 0x66, 0x59, 0x41, 0x7c, 0x24, 0x9f, 0xc6, 0x47, 0x32, 0xdd, 0x16,
	0x29, 0x62, 0xb8, 0x75, 0x66, 0xbf, 0xf9, 0x66, 0xa6, 0xbb, 0x70, 0x21, 0xe8, 0xc6, 0xa7, 0x92,
	0x7a, 0x4f, 0xf4, 0x83, 0x11, 0x16, 0x08, 0xb6, 0x9a, 0x0d, 0x79, 0x1c, 0x44, 0x61, 0x37, 0x11,
	0x5c, 0x72, 0x54, 0xce, 0x07, 0x3a, 0x5f, 0x25, 0x68, 0x1c, 0x0d, 0xa1, 0x26, 0x58, 0x13, 0x49,
	0xc3, 0xd5, 0x04, 0x6b, 0x8e, 0xe1, 0xda, 0xa4, 0x94, 0x02, 0x74, 0x05, 0x75, 0x2a, 0x64, 0x14,
	0xd0, 0x57, 0x39, 0x12, 0x3c, 0x79, 0xf4, 0x71, 0xcd, 0xd1, 0x5c, 0x93, 0xfc, 0x62, 0x51, 0x0b,
	0x2c, 0x3f, 0x3b, 0x07, 0x75, 0x9e, 0x23, 0xd4, 0x07, 0x93, 0xad, 0x59, 0x2c, 0xb1, 0xee, 0x18,
	0x6e, 0xb5, 0x77, 0xd9, 0xcd, 0x43, 0x74, 0x8f, 0x53, 0x8e, 0xd3, 0xb9, 0x71, 0x2c, 0xc5, 0x96,
	0x64, 0x1a, 0x74, 0x0e, 0x30, 0xa3, 0xc2, 0x27, 0x6c, 0x43, 0x85, 0x8f, 0xcb, 0x8e, 0xe6, 0xda,
	0xa4, 0xc0, 0xa0, 0x33, 0xb0, 0x53, 0xf4, 0x4c, 0x17, 0x6f, 0x0c, 0x1b, 0xca, 0x77, 0x4f, 0xa0,
	0x3a, 0xe8, 0x91, 0x8f, 0x4b, 0x8a, 0xd6, 0x23, 0x1f, 0x75, 0xa0, 0x16, 0x73, 0xb1, 0xa4, 0x8b,
	0x7c, 0x5f, 0x45, 0xed, 0x3b, 0xe0, 0x90, 0x03, 0xd5, 0x0c, 0x67, 0x3b, 0x2d, 0x25, 0x2e, 0x52,
	0x68, 0x04, 0x15, 0xc1, 0xa7, 0x5c, 0x3e, 0x30, 0x89, 0x6d, 0xd5, 0xc9, 0x3d, 0xd1, 0x89, 0xe4,
	0xa3, 0x59, 0xad, 0x1f, 0x65, 0xfb, 0x1a, 0x1a, 0xaa, 0xe2, 0x90, 0x2f, 0x93, 0x05, 0x7b, 0xcf,
	0x56, 0x37, 0xc1, 0x5c, 0x2b, 0xdb, 0xf4, 0x02, 0x4c, 0x92, 0x81, 0x76, 0x00, 0xb0, 0xff, 0x33,
	0xe8, 0x3f, 0x18, 0x73, 0xb6, 0xc5, 0x9a, 0x0a, 0x96, 0x7e, 0xa2, 0xc1, 0x4e, 0xa5, 0x3b, 0x9a,
	0x5b, 0xed, 0xdd, 0x9c, 0x48, 0x73, 0x64, 0x99, 0x7b, 0xdc, 0xeb, 0x77, 0x5a, 0xbb, 0x0f, 0xff,
	0x0e, 0xd2, 0xfe, 0x61, 0xd5, 0x2c, 0x5a, 0x99, 0x05, 0xf1, 0xa0, 0xf3, 0xd2, 0x5a, 0x31, 0xb1,
	0x66, 0xc2, 0x4b, 0xe6, 0xa1, 0x17, 0xb2, 0xd8, 0xcb, 0x53, 0x7c, 0xea, 0xbb, 0x67, 0x37, 0xb5,
	0xd4, 0x33, 0xbc, 0xfd, 0x0e, 0x00, 0x00, 0xff, 0xff, 0x5d, 0x75, 0x20, 0x64, 0xa9, 0x02, 0x00,
	0x00,
}
