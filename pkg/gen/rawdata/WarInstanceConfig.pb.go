// Code generated by protoc-gen-go. DO NOT EDIT.
// source: rawdata/WarInstanceConfig.proto

package rawdata

import (
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

type WarInstanceConfig struct {
	XTags_ []string `protobuf:"bytes,1,rep,name=_tags_,json=tags,proto3" json:"_tags_,omitempty"`
	// 所属功能
	FuncId int32 `protobuf:"varint,6,opt,name=funcId,proto3" json:"funcId,omitempty"`
	// 难度ICON
	Icon []string `protobuf:"bytes,7,rep,name=icon,proto3" json:"icon,omitempty"`
	// 难度id
	Id int32 `protobuf:"varint,2,opt,name=id,proto3" json:"id,omitempty"`
	// 包含章节
	Map []int32 `protobuf:"varint,3,rep,packed,name=map,proto3" json:"map,omitempty"`
	// 难度名
	Name                 int32    `protobuf:"varint,4,opt,name=name,proto3" json:"name,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *WarInstanceConfig) Reset()         { *m = WarInstanceConfig{} }
func (m *WarInstanceConfig) String() string { return proto.CompactTextString(m) }
func (*WarInstanceConfig) ProtoMessage()    {}
func (*WarInstanceConfig) Descriptor() ([]byte, []int) {
	return fileDescriptor_2c66ea9e82c798b4, []int{0}
}

func (m *WarInstanceConfig) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_WarInstanceConfig.Unmarshal(m, b)
}
func (m *WarInstanceConfig) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_WarInstanceConfig.Marshal(b, m, deterministic)
}
func (m *WarInstanceConfig) XXX_Merge(src proto.Message) {
	xxx_messageInfo_WarInstanceConfig.Merge(m, src)
}
func (m *WarInstanceConfig) XXX_Size() int {
	return xxx_messageInfo_WarInstanceConfig.Size(m)
}
func (m *WarInstanceConfig) XXX_DiscardUnknown() {
	xxx_messageInfo_WarInstanceConfig.DiscardUnknown(m)
}

var xxx_messageInfo_WarInstanceConfig proto.InternalMessageInfo

func (m *WarInstanceConfig) GetXTags_() []string {
	if m != nil {
		return m.XTags_
	}
	return nil
}

func (m *WarInstanceConfig) GetFuncId() int32 {
	if m != nil {
		return m.FuncId
	}
	return 0
}

func (m *WarInstanceConfig) GetIcon() []string {
	if m != nil {
		return m.Icon
	}
	return nil
}

func (m *WarInstanceConfig) GetId() int32 {
	if m != nil {
		return m.Id
	}
	return 0
}

func (m *WarInstanceConfig) GetMap() []int32 {
	if m != nil {
		return m.Map
	}
	return nil
}

func (m *WarInstanceConfig) GetName() int32 {
	if m != nil {
		return m.Name
	}
	return 0
}

func init() {
	proto.RegisterType((*WarInstanceConfig)(nil), "rawdata.WarInstanceConfig")
}

func init() { proto.RegisterFile("rawdata/WarInstanceConfig.proto", fileDescriptor_2c66ea9e82c798b4) }

var fileDescriptor_2c66ea9e82c798b4 = []byte{
	// 196 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xe2, 0x92, 0x2f, 0x4a, 0x2c, 0x4f,
	0x49, 0x2c, 0x49, 0xd4, 0x0f, 0x4f, 0x2c, 0xf2, 0xcc, 0x2b, 0x2e, 0x49, 0xcc, 0x4b, 0x4e, 0x75,
	0xce, 0xcf, 0x4b, 0xcb, 0x4c, 0xd7, 0x2b, 0x28, 0xca, 0x2f, 0xc9, 0x17, 0x62, 0x87, 0x2a, 0x50,
	0xea, 0x67, 0xe4, 0x12, 0xc4, 0x50, 0x24, 0x24, 0xc2, 0xc5, 0x16, 0x5f, 0x92, 0x98, 0x5e, 0x1c,
	0x2f, 0xc1, 0xa8, 0xc0, 0xac, 0xc1, 0x19, 0xc4, 0x02, 0xe2, 0x08, 0x89, 0x71, 0xb1, 0xa5, 0x95,
	0xe6, 0x25, 0x7b, 0xa6, 0x48, 0xb0, 0x29, 0x30, 0x6a, 0xb0, 0x06, 0x41, 0x79, 0x42, 0x42, 0x5c,
	0x2c, 0x99, 0xc9, 0xf9, 0x79, 0x12, 0xec, 0x10, 0xb5, 0x20, 0xb6, 0x10, 0x1f, 0x17, 0x53, 0x66,
	0x8a, 0x04, 0x13, 0x58, 0x1d, 0x53, 0x66, 0x8a, 0x90, 0x08, 0x17, 0x73, 0x6e, 0x62, 0x81, 0x04,
	0xb3, 0x02, 0xb3, 0x06, 0xab, 0x13, 0x93, 0x00, 0x63, 0x10, 0x88, 0x0b, 0xd2, 0x99, 0x97, 0x98,
	0x9b, 0x2a, 0xc1, 0x02, 0x56, 0x07, 0x66, 0x3b, 0x29, 0x45, 0x89, 0x15, 0xa7, 0x16, 0x95, 0xa5,
	0x16, 0xe9, 0x17, 0x64, 0xa7, 0xeb, 0xa7, 0xa7, 0xe6, 0xe9, 0x43, 0xdd, 0xba, 0x8a, 0x09, 0xe6,
	0xea, 0x24, 0x36, 0xb0, 0x2f, 0x8c, 0x01, 0x01, 0x00, 0x00, 0xff, 0xff, 0xff, 0xbc, 0x3a, 0x77,
	0xe8, 0x00, 0x00, 0x00,
}
