// Code generated by protoc-gen-go. DO NOT EDIT.
// source: rawdata/WeightConfig.proto

package rawdata

import (
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

type WeightConfig struct {
	XTags_ []string `protobuf:"bytes,1,rep,name=_tags_,json=tags,proto3" json:"_tags_,omitempty"`
	// 活动权重1
	ActivityWeight1 int32 `protobuf:"varint,2,opt,name=activityWeight1,proto3" json:"activityWeight1,omitempty"`
	// 活动权重2
	ActivityWeight2 int32 `protobuf:"varint,3,opt,name=activityWeight2,proto3" json:"activityWeight2,omitempty"`
	// 活动权重3
	ActivityWeight3 int32 `protobuf:"varint,4,opt,name=activityWeight3,proto3" json:"activityWeight3,omitempty"`
	// 先知抽权重
	AdvanceWeight int32 `protobuf:"varint,5,opt,name=advanceWeight,proto3" json:"advanceWeight,omitempty"`
	// 文明类型
	Civilization int32 `protobuf:"varint,6,opt,name=civilization,proto3" json:"civilization,omitempty"`
	// 普通抽权重
	CommonWeight int32 `protobuf:"varint,7,opt,name=commonWeight,proto3" json:"commonWeight,omitempty"`
	// 置换权重
	ExchangeWeight int32 `protobuf:"varint,8,opt,name=exchangeWeight,proto3" json:"exchangeWeight,omitempty"`
	// ID
	Id int32 `protobuf:"varint,9,opt,name=id,proto3" json:"id,omitempty"`
	// 模板ID
	Model string `protobuf:"bytes,10,opt,name=model,proto3" json:"model,omitempty"`
	// 卡包ID
	Pack int32 `protobuf:"varint,11,opt,name=pack,proto3" json:"pack,omitempty"`
	// 随机卡牌权重
	RandomWeight int32 `protobuf:"varint,12,opt,name=randomWeight,proto3" json:"randomWeight,omitempty"`
	// 碎片数量
	ShardNum int32 `protobuf:"varint,13,opt,name=shardNum,proto3" json:"shardNum,omitempty"`
	// 卡牌星级
	Star []int32 `protobuf:"varint,14,rep,packed,name=star,proto3" json:"star,omitempty"`
	// 军团类型
	TroopType            string   `protobuf:"bytes,15,opt,name=troopType,proto3" json:"troopType,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *WeightConfig) Reset()         { *m = WeightConfig{} }
func (m *WeightConfig) String() string { return proto.CompactTextString(m) }
func (*WeightConfig) ProtoMessage()    {}
func (*WeightConfig) Descriptor() ([]byte, []int) {
	return fileDescriptor_04eed34ae0c77fd3, []int{0}
}

func (m *WeightConfig) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_WeightConfig.Unmarshal(m, b)
}
func (m *WeightConfig) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_WeightConfig.Marshal(b, m, deterministic)
}
func (m *WeightConfig) XXX_Merge(src proto.Message) {
	xxx_messageInfo_WeightConfig.Merge(m, src)
}
func (m *WeightConfig) XXX_Size() int {
	return xxx_messageInfo_WeightConfig.Size(m)
}
func (m *WeightConfig) XXX_DiscardUnknown() {
	xxx_messageInfo_WeightConfig.DiscardUnknown(m)
}

var xxx_messageInfo_WeightConfig proto.InternalMessageInfo

func (m *WeightConfig) GetXTags_() []string {
	if m != nil {
		return m.XTags_
	}
	return nil
}

func (m *WeightConfig) GetActivityWeight1() int32 {
	if m != nil {
		return m.ActivityWeight1
	}
	return 0
}

func (m *WeightConfig) GetActivityWeight2() int32 {
	if m != nil {
		return m.ActivityWeight2
	}
	return 0
}

func (m *WeightConfig) GetActivityWeight3() int32 {
	if m != nil {
		return m.ActivityWeight3
	}
	return 0
}

func (m *WeightConfig) GetAdvanceWeight() int32 {
	if m != nil {
		return m.AdvanceWeight
	}
	return 0
}

func (m *WeightConfig) GetCivilization() int32 {
	if m != nil {
		return m.Civilization
	}
	return 0
}

func (m *WeightConfig) GetCommonWeight() int32 {
	if m != nil {
		return m.CommonWeight
	}
	return 0
}

func (m *WeightConfig) GetExchangeWeight() int32 {
	if m != nil {
		return m.ExchangeWeight
	}
	return 0
}

func (m *WeightConfig) GetId() int32 {
	if m != nil {
		return m.Id
	}
	return 0
}

func (m *WeightConfig) GetModel() string {
	if m != nil {
		return m.Model
	}
	return ""
}

func (m *WeightConfig) GetPack() int32 {
	if m != nil {
		return m.Pack
	}
	return 0
}

func (m *WeightConfig) GetRandomWeight() int32 {
	if m != nil {
		return m.RandomWeight
	}
	return 0
}

func (m *WeightConfig) GetShardNum() int32 {
	if m != nil {
		return m.ShardNum
	}
	return 0
}

func (m *WeightConfig) GetStar() []int32 {
	if m != nil {
		return m.Star
	}
	return nil
}

func (m *WeightConfig) GetTroopType() string {
	if m != nil {
		return m.TroopType
	}
	return ""
}

func init() {
	proto.RegisterType((*WeightConfig)(nil), "rawdata.WeightConfig")
}

func init() { proto.RegisterFile("rawdata/WeightConfig.proto", fileDescriptor_04eed34ae0c77fd3) }

var fileDescriptor_04eed34ae0c77fd3 = []byte{
	// 325 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0x6c, 0x92, 0xcb, 0x6a, 0xe3, 0x30,
	0x14, 0x86, 0xf1, 0x2d, 0x89, 0xcf, 0xe4, 0x32, 0x88, 0x10, 0x44, 0x98, 0x85, 0x09, 0xc3, 0xe0,
	0xd5, 0x98, 0x26, 0x6f, 0x90, 0xee, 0xbb, 0x30, 0x85, 0x42, 0x37, 0xe1, 0xd4, 0x52, 0x1d, 0x91,
	0x58, 0x32, 0xb2, 0xea, 0x36, 0x7d, 0xa4, 0x3e, 0x59, 0x1f, 0xa3, 0x44, 0x76, 0xda, 0xdc, 0x76,
	0xfa, 0xbf, 0xff, 0xe3, 0xe8, 0x2c, 0x0e, 0x4c, 0x35, 0xbe, 0x32, 0x34, 0x98, 0x3c, 0x70, 0x91,
	0xaf, 0xcd, 0xad, 0x92, 0xcf, 0x22, 0xff, 0x5f, 0x6a, 0x65, 0x14, 0xe9, 0xb6, 0xdd, 0xec, 0xd3,
	0x83, 0xfe, 0x71, 0x4f, 0xc6, 0xd0, 0x59, 0x19, 0xcc, 0xab, 0x15, 0x75, 0x22, 0x2f, 0x0e, 0x53,
	0x7f, 0x1f, 0x48, 0x0c, 0x23, 0xcc, 0x8c, 0xa8, 0x85, 0xd9, 0x35, 0xf6, 0x0d, 0x75, 0x23, 0x27,
	0x0e, 0xd2, 0x73, 0x7c, 0x69, 0xce, 0xa9, 0x77, 0xcd, 0x9c, 0x5f, 0x9a, 0x0b, 0xea, 0x5f, 0x33,
	0x17, 0xe4, 0x2f, 0x0c, 0x90, 0xd5, 0x28, 0x33, 0xde, 0x10, 0x1a, 0x58, 0xef, 0x14, 0x92, 0x19,
	0xf4, 0x33, 0x51, 0x8b, 0xad, 0x78, 0x47, 0x23, 0x94, 0xa4, 0x1d, 0x2b, 0x9d, 0x30, 0xeb, 0xa8,
	0xa2, 0x50, 0xb2, 0x1d, 0xd4, 0x6d, 0x9d, 0x23, 0x46, 0xfe, 0xc1, 0x90, 0xbf, 0x65, 0x6b, 0x94,
	0xf9, 0xe1, 0xbb, 0x9e, 0xb5, 0xce, 0x28, 0x19, 0x82, 0x2b, 0x18, 0x0d, 0x6d, 0xe7, 0x0a, 0x46,
	0xc6, 0x10, 0x14, 0x8a, 0xf1, 0x2d, 0x85, 0xc8, 0x89, 0xc3, 0xb4, 0x09, 0x84, 0x80, 0x5f, 0x62,
	0xb6, 0xa1, 0xbf, 0xac, 0x67, 0xdf, 0xfb, 0x2d, 0x34, 0x4a, 0xa6, 0x8a, 0x76, 0x7e, 0xbf, 0xd9,
	0xe2, 0x98, 0x91, 0x29, 0xf4, 0xaa, 0x35, 0x6a, 0x76, 0xf7, 0x52, 0xd0, 0x81, 0xed, 0xbf, 0x33,
	0x99, 0x80, 0x5f, 0x19, 0xd4, 0x74, 0x18, 0x79, 0x71, 0xb0, 0x74, 0x7f, 0x3b, 0xa9, 0xcd, 0xe4,
	0x0f, 0x84, 0x46, 0x2b, 0x55, 0xde, 0xef, 0x4a, 0x4e, 0x47, 0x76, 0x8b, 0x1f, 0xb0, 0x9c, 0x3d,
	0x4e, 0x2a, 0xae, 0x6b, 0xae, 0x93, 0x72, 0x93, 0x27, 0x39, 0x97, 0x49, 0x7b, 0x04, 0x1f, 0xee,
	0xe1, 0x1c, 0x9e, 0x3a, 0xf6, 0x3c, 0x16, 0x5f, 0x01, 0x00, 0x00, 0xff, 0xff, 0xce, 0xfa, 0xa8,
	0x4e, 0x3c, 0x02, 0x00, 0x00,
}
