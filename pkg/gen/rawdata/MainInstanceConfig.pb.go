// Code generated by protoc-gen-go. DO NOT EDIT.
// source: rawdata/MainInstanceConfig.proto

package rawdata

import (
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

type MainInstanceConfig struct {
	XTags_ []string `protobuf:"bytes,1,rep,name=_tags_,json=tags,proto3" json:"_tags_,omitempty"`
	// 主线章节数，0是序章
	Chapter int32 `protobuf:"varint,2,opt,name=chapter,proto3" json:"chapter,omitempty"`
	// 副本简介，主界面显示
	Des int32 `protobuf:"varint,3,opt,name=des,proto3" json:"des,omitempty"`
	// 副本详细介绍，第一次进入显示
	DetailedDec int32 `protobuf:"varint,4,opt,name=detailedDec,proto3" json:"detailedDec,omitempty"`
	// 本章主线最终关卡
	FinalBattleStronghold int32 `protobuf:"varint,16,opt,name=finalBattleStronghold,proto3" json:"finalBattleStronghold,omitempty"`
	// 主线副本id
	Id int32 `protobuf:"varint,5,opt,name=id,proto3" json:"id,omitempty"`
	// 副本名
	Name int32 `protobuf:"varint,6,opt,name=name,proto3" json:"name,omitempty"`
	// 本章进度达到多少后解锁下一章，主线章节id,解锁下一章进度百分比0,0=没有要求
	OpenNextCondition map[int32]int32 `protobuf:"bytes,14,rep,name=openNextCondition,proto3" json:"openNextCondition,omitempty" protobuf_key:"varint,1,opt,name=key,proto3" protobuf_val:"varint,2,opt,name=value,proto3"`
	// 主线开启前点击提示
	OpenTip int32 `protobuf:"varint,8,opt,name=openTip,proto3" json:"openTip,omitempty"`
	// 主线总进度值
	ProgressValue int32 `protobuf:"varint,9,opt,name=progressValue,proto3" json:"progressValue,omitempty"`
	// 主线目标，格式：据点1,据点2，据点3
	// 主界面由上至下显示3个目标，据点完成后目标达成
	Purpose []int32 `protobuf:"varint,10,rep,packed,name=purpose,proto3" json:"purpose,omitempty"`
	// 主线目标介绍，格式：据点1介绍,据点2介绍，据点3介绍
	// 与前面据点一一对应
	PurposeDes []int32 `protobuf:"varint,11,rep,packed,name=purposeDes,proto3" json:"purposeDes,omitempty"`
	// 主线奖励展示，纯展示效果无实际意义
	RewardShow string `protobuf:"bytes,12,opt,name=rewardShow,proto3" json:"rewardShow,omitempty"`
	// 场景信息调用
	Scene string `protobuf:"bytes,15,opt,name=scene,proto3" json:"scene,omitempty"`
	// 章节全部所属据点
	StrongholdId         []int32  `protobuf:"varint,13,rep,packed,name=strongholdId,proto3" json:"strongholdId,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *MainInstanceConfig) Reset()         { *m = MainInstanceConfig{} }
func (m *MainInstanceConfig) String() string { return proto.CompactTextString(m) }
func (*MainInstanceConfig) ProtoMessage()    {}
func (*MainInstanceConfig) Descriptor() ([]byte, []int) {
	return fileDescriptor_1e9d1758bbca23b8, []int{0}
}

func (m *MainInstanceConfig) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_MainInstanceConfig.Unmarshal(m, b)
}
func (m *MainInstanceConfig) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_MainInstanceConfig.Marshal(b, m, deterministic)
}
func (m *MainInstanceConfig) XXX_Merge(src proto.Message) {
	xxx_messageInfo_MainInstanceConfig.Merge(m, src)
}
func (m *MainInstanceConfig) XXX_Size() int {
	return xxx_messageInfo_MainInstanceConfig.Size(m)
}
func (m *MainInstanceConfig) XXX_DiscardUnknown() {
	xxx_messageInfo_MainInstanceConfig.DiscardUnknown(m)
}

var xxx_messageInfo_MainInstanceConfig proto.InternalMessageInfo

func (m *MainInstanceConfig) GetXTags_() []string {
	if m != nil {
		return m.XTags_
	}
	return nil
}

func (m *MainInstanceConfig) GetChapter() int32 {
	if m != nil {
		return m.Chapter
	}
	return 0
}

func (m *MainInstanceConfig) GetDes() int32 {
	if m != nil {
		return m.Des
	}
	return 0
}

func (m *MainInstanceConfig) GetDetailedDec() int32 {
	if m != nil {
		return m.DetailedDec
	}
	return 0
}

func (m *MainInstanceConfig) GetFinalBattleStronghold() int32 {
	if m != nil {
		return m.FinalBattleStronghold
	}
	return 0
}

func (m *MainInstanceConfig) GetId() int32 {
	if m != nil {
		return m.Id
	}
	return 0
}

func (m *MainInstanceConfig) GetName() int32 {
	if m != nil {
		return m.Name
	}
	return 0
}

func (m *MainInstanceConfig) GetOpenNextCondition() map[int32]int32 {
	if m != nil {
		return m.OpenNextCondition
	}
	return nil
}

func (m *MainInstanceConfig) GetOpenTip() int32 {
	if m != nil {
		return m.OpenTip
	}
	return 0
}

func (m *MainInstanceConfig) GetProgressValue() int32 {
	if m != nil {
		return m.ProgressValue
	}
	return 0
}

func (m *MainInstanceConfig) GetPurpose() []int32 {
	if m != nil {
		return m.Purpose
	}
	return nil
}

func (m *MainInstanceConfig) GetPurposeDes() []int32 {
	if m != nil {
		return m.PurposeDes
	}
	return nil
}

func (m *MainInstanceConfig) GetRewardShow() string {
	if m != nil {
		return m.RewardShow
	}
	return ""
}

func (m *MainInstanceConfig) GetScene() string {
	if m != nil {
		return m.Scene
	}
	return ""
}

func (m *MainInstanceConfig) GetStrongholdId() []int32 {
	if m != nil {
		return m.StrongholdId
	}
	return nil
}

func init() {
	proto.RegisterType((*MainInstanceConfig)(nil), "rawdata.MainInstanceConfig")
	proto.RegisterMapType((map[int32]int32)(nil), "rawdata.MainInstanceConfig.OpenNextConditionEntry")
}

func init() { proto.RegisterFile("rawdata/MainInstanceConfig.proto", fileDescriptor_1e9d1758bbca23b8) }

var fileDescriptor_1e9d1758bbca23b8 = []byte{
	// 404 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0x6c, 0x92, 0x4f, 0x6f, 0xd3, 0x40,
	0x10, 0xc5, 0x65, 0x3b, 0x7f, 0xc8, 0xa4, 0x2d, 0x61, 0x54, 0xaa, 0x11, 0x42, 0xc8, 0x8a, 0x10,
	0xf2, 0x29, 0x91, 0x0a, 0x07, 0xc4, 0x31, 0x0d, 0x87, 0x1e, 0x00, 0xc9, 0x45, 0x1c, 0xb8, 0x94,
	0x25, 0x3b, 0x75, 0x56, 0x31, 0xbb, 0xd6, 0xee, 0xb6, 0x21, 0x5f, 0x89, 0x8f, 0xc4, 0xa7, 0x41,
	0x5e, 0x3b, 0x51, 0x4a, 0x72, 0x9b, 0xf7, 0x7b, 0xcf, 0x6b, 0xed, 0xdb, 0x81, 0xd4, 0x8a, 0xb5,
	0x14, 0x5e, 0x4c, 0x3f, 0x09, 0xa5, 0xaf, 0xb5, 0xf3, 0x42, 0x2f, 0xf8, 0xca, 0xe8, 0x3b, 0x55,
	0x4c, 0x2a, 0x6b, 0xbc, 0xc1, 0x7e, 0x9b, 0x18, 0xff, 0xed, 0x00, 0x1e, 0xa6, 0xf0, 0x1c, 0x7a,
	0xb7, 0x5e, 0x14, 0xee, 0x96, 0xa2, 0x34, 0xc9, 0x06, 0x79, 0xa7, 0x16, 0x48, 0xd0, 0x5f, 0x2c,
	0x45, 0xe5, 0xd9, 0x52, 0x9c, 0x46, 0x59, 0x37, 0xdf, 0x4a, 0x1c, 0x41, 0x22, 0xd9, 0x51, 0x12,
	0x68, 0x3d, 0x62, 0x0a, 0x43, 0xc9, 0x5e, 0xa8, 0x92, 0xe5, 0x9c, 0x17, 0xd4, 0x09, 0xce, 0x3e,
	0xc2, 0x77, 0xf0, 0xfc, 0x4e, 0x69, 0x51, 0xce, 0x84, 0xf7, 0x25, 0xdf, 0x78, 0x6b, 0x74, 0xb1,
	0x34, 0xa5, 0xa4, 0x51, 0xc8, 0x1e, 0x37, 0xf1, 0x0c, 0x62, 0x25, 0xa9, 0x1b, 0x22, 0xb1, 0x92,
	0x88, 0xd0, 0xd1, 0xe2, 0x17, 0x53, 0x2f, 0x90, 0x30, 0xe3, 0x0f, 0x78, 0x66, 0x2a, 0xd6, 0x9f,
	0xf9, 0xb7, 0xbf, 0x32, 0x5a, 0x2a, 0xaf, 0x8c, 0xa6, 0xb3, 0x34, 0xc9, 0x86, 0x97, 0x97, 0x93,
	0xf6, 0xe6, 0x93, 0x23, 0xdd, 0x7c, 0xf9, 0xff, 0xa3, 0x8f, 0xda, 0xdb, 0x4d, 0x7e, 0x78, 0x58,
	0xdd, 0x44, 0x0d, 0xbf, 0xaa, 0x8a, 0x9e, 0x34, 0x4d, 0xb4, 0x12, 0x5f, 0xc3, 0x69, 0x65, 0x4d,
	0x61, 0xd9, 0xb9, 0x6f, 0xa2, 0xbc, 0x67, 0x1a, 0x04, 0xff, 0x31, 0xc4, 0x97, 0xd0, 0xaf, 0xee,
	0x6d, 0x65, 0x1c, 0x13, 0xa4, 0x49, 0xd6, 0x9d, 0xc5, 0xa3, 0x28, 0xdf, 0x22, 0x1c, 0x03, 0xb4,
	0xe3, 0x9c, 0x1d, 0x0d, 0x77, 0x81, 0x3d, 0x8a, 0xaf, 0x00, 0x2c, 0xaf, 0x85, 0x95, 0x37, 0x4b,
	0xb3, 0xa6, 0x93, 0x34, 0xca, 0x06, 0xf9, 0x1e, 0xc1, 0x73, 0xe8, 0xba, 0x05, 0x6b, 0xa6, 0xa7,
	0xc1, 0x6a, 0x04, 0xbe, 0x81, 0x13, 0xb7, 0xeb, 0xf2, 0x5a, 0xd2, 0xe9, 0xee, 0xec, 0x47, 0xfc,
	0xc5, 0x1c, 0x2e, 0x8e, 0x97, 0x51, 0xbf, 0xf4, 0x8a, 0x37, 0x14, 0x35, 0x2f, 0xbd, 0xe2, 0x4d,
	0xfd, 0xa7, 0x87, 0x70, 0xd3, 0x66, 0x27, 0x1a, 0xf1, 0x21, 0x7e, 0x1f, 0xcd, 0xc6, 0xdf, 0x2f,
	0x1c, 0xdb, 0x07, 0xb6, 0xd3, 0x6a, 0x55, 0x4c, 0x0b, 0xd6, 0xd3, 0xb6, 0xfc, 0x3f, 0xf1, 0x76,
	0x01, 0x7f, 0xf6, 0xc2, 0x42, 0xbe, 0xfd, 0x17, 0x00, 0x00, 0xff, 0xff, 0x1a, 0xf7, 0xd0, 0x1e,
	0xb4, 0x02, 0x00, 0x00,
}
