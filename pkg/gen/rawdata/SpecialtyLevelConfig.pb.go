// Code generated by protoc-gen-go. DO NOT EDIT.
// source: rawdata/SpecialtyLevelConfig.proto

package rawdata

import (
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

type SpecialtyLevelConfig struct {
	// 专长升级建筑条件
	BuildingCondition int32    `protobuf:"varint,1,opt,name=BuildingCondition,json=buildingCondition,proto3" json:"BuildingCondition,omitempty"`
	XTags_            []string `protobuf:"bytes,2,rep,name=_tags_,json=tags,proto3" json:"_tags_,omitempty"`
	// 专长升级消耗
	Cost string `protobuf:"bytes,3,opt,name=cost,proto3" json:"cost,omitempty"`
	// 模板ID
	Id int32 `protobuf:"varint,4,opt,name=id,proto3" json:"id,omitempty"`
	// 专长重置返还道具
	ResetCost            string   `protobuf:"bytes,5,opt,name=resetCost,proto3" json:"resetCost,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *SpecialtyLevelConfig) Reset()         { *m = SpecialtyLevelConfig{} }
func (m *SpecialtyLevelConfig) String() string { return proto.CompactTextString(m) }
func (*SpecialtyLevelConfig) ProtoMessage()    {}
func (*SpecialtyLevelConfig) Descriptor() ([]byte, []int) {
	return fileDescriptor_71087bccbf32e9f6, []int{0}
}

func (m *SpecialtyLevelConfig) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_SpecialtyLevelConfig.Unmarshal(m, b)
}
func (m *SpecialtyLevelConfig) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_SpecialtyLevelConfig.Marshal(b, m, deterministic)
}
func (m *SpecialtyLevelConfig) XXX_Merge(src proto.Message) {
	xxx_messageInfo_SpecialtyLevelConfig.Merge(m, src)
}
func (m *SpecialtyLevelConfig) XXX_Size() int {
	return xxx_messageInfo_SpecialtyLevelConfig.Size(m)
}
func (m *SpecialtyLevelConfig) XXX_DiscardUnknown() {
	xxx_messageInfo_SpecialtyLevelConfig.DiscardUnknown(m)
}

var xxx_messageInfo_SpecialtyLevelConfig proto.InternalMessageInfo

func (m *SpecialtyLevelConfig) GetBuildingCondition() int32 {
	if m != nil {
		return m.BuildingCondition
	}
	return 0
}

func (m *SpecialtyLevelConfig) GetXTags_() []string {
	if m != nil {
		return m.XTags_
	}
	return nil
}

func (m *SpecialtyLevelConfig) GetCost() string {
	if m != nil {
		return m.Cost
	}
	return ""
}

func (m *SpecialtyLevelConfig) GetId() int32 {
	if m != nil {
		return m.Id
	}
	return 0
}

func (m *SpecialtyLevelConfig) GetResetCost() string {
	if m != nil {
		return m.ResetCost
	}
	return ""
}

func init() {
	proto.RegisterType((*SpecialtyLevelConfig)(nil), "rawdata.SpecialtyLevelConfig")
}

func init() { proto.RegisterFile("rawdata/SpecialtyLevelConfig.proto", fileDescriptor_71087bccbf32e9f6) }

var fileDescriptor_71087bccbf32e9f6 = []byte{
	// 200 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xe2, 0x52, 0x2a, 0x4a, 0x2c, 0x4f,
	0x49, 0x2c, 0x49, 0xd4, 0x0f, 0x2e, 0x48, 0x4d, 0xce, 0x4c, 0xcc, 0x29, 0xa9, 0xf4, 0x49, 0x2d,
	0x4b, 0xcd, 0x71, 0xce, 0xcf, 0x4b, 0xcb, 0x4c, 0xd7, 0x2b, 0x28, 0xca, 0x2f, 0xc9, 0x17, 0x62,
	0x87, 0xaa, 0x51, 0x9a, 0xc3, 0xc8, 0x25, 0x82, 0x4d, 0x9d, 0x90, 0x0e, 0x97, 0xa0, 0x53, 0x69,
	0x66, 0x4e, 0x4a, 0x66, 0x5e, 0xba, 0x73, 0x7e, 0x5e, 0x4a, 0x66, 0x49, 0x66, 0x7e, 0x9e, 0x04,
	0xa3, 0x02, 0xa3, 0x06, 0x6b, 0x90, 0x60, 0x12, 0xba, 0x84, 0x90, 0x08, 0x17, 0x5b, 0x7c, 0x49,
	0x62, 0x7a, 0x71, 0xbc, 0x04, 0x93, 0x02, 0xb3, 0x06, 0x67, 0x10, 0x0b, 0x88, 0x23, 0x24, 0xc4,
	0xc5, 0x92, 0x9c, 0x5f, 0x5c, 0x22, 0xc1, 0xac, 0xc0, 0x08, 0x12, 0x03, 0xb1, 0x85, 0xf8, 0xb8,
	0x98, 0x32, 0x53, 0x24, 0x58, 0xc0, 0x06, 0x31, 0x65, 0xa6, 0x08, 0xc9, 0x70, 0x71, 0x16, 0xa5,
	0x16, 0xa7, 0x96, 0x38, 0x83, 0x14, 0xb2, 0x82, 0x15, 0x22, 0x04, 0x9c, 0x94, 0xa2, 0xc4, 0x8a,
	0x53, 0x8b, 0xca, 0x52, 0x8b, 0xf4, 0x0b, 0xb2, 0xd3, 0xf5, 0xd3, 0x53, 0xf3, 0xf4, 0xa1, 0x0e,
	0x5f, 0xc5, 0x04, 0xf3, 0x42, 0x12, 0x1b, 0xd8, 0x4b, 0xc6, 0x80, 0x00, 0x00, 0x00, 0xff, 0xff,
	0x41, 0x5c, 0x3a, 0x42, 0xf8, 0x00, 0x00, 0x00,
}
