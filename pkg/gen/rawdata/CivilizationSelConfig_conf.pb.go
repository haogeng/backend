// Code generated by protoc-gen-go. DO NOT EDIT.
// source: rawdata/CivilizationSelConfig_conf.proto

package rawdata

import (
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

type CivilizationSelConfigConf struct {
	CivilizationSelConfigs map[int32]*CivilizationSelConfig `protobuf:"bytes,1,rep,name=CivilizationSelConfigs,json=civilizationSelConfigs,proto3" json:"CivilizationSelConfigs,omitempty" protobuf_key:"varint,1,opt,name=key,proto3" protobuf_val:"bytes,2,opt,name=value,proto3"`
	XXX_NoUnkeyedLiteral   struct{}                         `json:"-"`
	XXX_unrecognized       []byte                           `json:"-"`
	XXX_sizecache          int32                            `json:"-"`
}

func (m *CivilizationSelConfigConf) Reset()         { *m = CivilizationSelConfigConf{} }
func (m *CivilizationSelConfigConf) String() string { return proto.CompactTextString(m) }
func (*CivilizationSelConfigConf) ProtoMessage()    {}
func (*CivilizationSelConfigConf) Descriptor() ([]byte, []int) {
	return fileDescriptor_3a2fd3f5d704b0c5, []int{0}
}

func (m *CivilizationSelConfigConf) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_CivilizationSelConfigConf.Unmarshal(m, b)
}
func (m *CivilizationSelConfigConf) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_CivilizationSelConfigConf.Marshal(b, m, deterministic)
}
func (m *CivilizationSelConfigConf) XXX_Merge(src proto.Message) {
	xxx_messageInfo_CivilizationSelConfigConf.Merge(m, src)
}
func (m *CivilizationSelConfigConf) XXX_Size() int {
	return xxx_messageInfo_CivilizationSelConfigConf.Size(m)
}
func (m *CivilizationSelConfigConf) XXX_DiscardUnknown() {
	xxx_messageInfo_CivilizationSelConfigConf.DiscardUnknown(m)
}

var xxx_messageInfo_CivilizationSelConfigConf proto.InternalMessageInfo

func (m *CivilizationSelConfigConf) GetCivilizationSelConfigs() map[int32]*CivilizationSelConfig {
	if m != nil {
		return m.CivilizationSelConfigs
	}
	return nil
}

func init() {
	proto.RegisterType((*CivilizationSelConfigConf)(nil), "rawdata.CivilizationSelConfig_conf")
	proto.RegisterMapType((map[int32]*CivilizationSelConfig)(nil), "rawdata.CivilizationSelConfig_conf.CivilizationSelConfigsEntry")
}

func init() {
	proto.RegisterFile("rawdata/CivilizationSelConfig_conf.proto", fileDescriptor_3a2fd3f5d704b0c5)
}

var fileDescriptor_3a2fd3f5d704b0c5 = []byte{
	// 197 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xe2, 0xd2, 0x28, 0x4a, 0x2c, 0x4f,
	0x49, 0x2c, 0x49, 0xd4, 0x77, 0xce, 0x2c, 0xcb, 0xcc, 0xc9, 0xac, 0x4a, 0x2c, 0xc9, 0xcc, 0xcf,
	0x0b, 0x4e, 0xcd, 0x71, 0xce, 0xcf, 0x4b, 0xcb, 0x4c, 0x8f, 0x4f, 0xce, 0xcf, 0x4b, 0xd3, 0x2b,
	0x28, 0xca, 0x2f, 0xc9, 0x17, 0x62, 0x87, 0xaa, 0x94, 0x52, 0xc6, 0xab, 0x05, 0xa2, 0x5a, 0xa9,
	0x81, 0x89, 0x4b, 0x0a, 0xb7, 0x91, 0x42, 0xe5, 0x5c, 0x62, 0x58, 0x65, 0x8b, 0x25, 0x18, 0x15,
	0x98, 0x35, 0xb8, 0x8d, 0xec, 0xf5, 0xa0, 0x96, 0xe8, 0xe1, 0x71, 0x17, 0x76, 0x13, 0x5c, 0xf3,
	0x4a, 0x8a, 0x2a, 0x83, 0xc4, 0x92, 0xb1, 0x4a, 0x4a, 0x65, 0x72, 0x49, 0xe3, 0xd1, 0x26, 0x24,
	0xc0, 0xc5, 0x9c, 0x9d, 0x5a, 0x29, 0xc1, 0xa8, 0xc0, 0xa8, 0xc1, 0x1a, 0x04, 0x62, 0x0a, 0x99,
	0x70, 0xb1, 0x96, 0x25, 0xe6, 0x94, 0xa6, 0x4a, 0x30, 0x29, 0x30, 0x6a, 0x70, 0x1b, 0xc9, 0xe1,
	0x77, 0x58, 0x10, 0x44, 0xb1, 0x15, 0x93, 0x05, 0xa3, 0x93, 0x52, 0x94, 0x58, 0x71, 0x6a, 0x51,
	0x59, 0x6a, 0x91, 0x7e, 0x41, 0x76, 0xba, 0x7e, 0x7a, 0x6a, 0x9e, 0x3e, 0x54, 0xeb, 0x2a, 0x26,
	0x58, 0x58, 0x26, 0xb1, 0x81, 0x43, 0xcb, 0x18, 0x10, 0x00, 0x00, 0xff, 0xff, 0xc5, 0x4d, 0xa0,
	0xf4, 0x87, 0x01, 0x00, 0x00,
}
