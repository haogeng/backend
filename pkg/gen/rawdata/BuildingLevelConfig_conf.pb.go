// Code generated by protoc-gen-go. DO NOT EDIT.
// source: rawdata/BuildingLevelConfig_conf.proto

package rawdata

import (
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

type BuildingLevelConfigConf struct {
	BuildingLevelConfigs map[int32]*BuildingLevelConfig `protobuf:"bytes,1,rep,name=BuildingLevelConfigs,json=buildingLevelConfigs,proto3" json:"BuildingLevelConfigs,omitempty" protobuf_key:"varint,1,opt,name=key,proto3" protobuf_val:"bytes,2,opt,name=value,proto3"`
	XXX_NoUnkeyedLiteral struct{}                       `json:"-"`
	XXX_unrecognized     []byte                         `json:"-"`
	XXX_sizecache        int32                          `json:"-"`
}

func (m *BuildingLevelConfigConf) Reset()         { *m = BuildingLevelConfigConf{} }
func (m *BuildingLevelConfigConf) String() string { return proto.CompactTextString(m) }
func (*BuildingLevelConfigConf) ProtoMessage()    {}
func (*BuildingLevelConfigConf) Descriptor() ([]byte, []int) {
	return fileDescriptor_318c6c50f28fdd70, []int{0}
}

func (m *BuildingLevelConfigConf) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_BuildingLevelConfigConf.Unmarshal(m, b)
}
func (m *BuildingLevelConfigConf) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_BuildingLevelConfigConf.Marshal(b, m, deterministic)
}
func (m *BuildingLevelConfigConf) XXX_Merge(src proto.Message) {
	xxx_messageInfo_BuildingLevelConfigConf.Merge(m, src)
}
func (m *BuildingLevelConfigConf) XXX_Size() int {
	return xxx_messageInfo_BuildingLevelConfigConf.Size(m)
}
func (m *BuildingLevelConfigConf) XXX_DiscardUnknown() {
	xxx_messageInfo_BuildingLevelConfigConf.DiscardUnknown(m)
}

var xxx_messageInfo_BuildingLevelConfigConf proto.InternalMessageInfo

func (m *BuildingLevelConfigConf) GetBuildingLevelConfigs() map[int32]*BuildingLevelConfig {
	if m != nil {
		return m.BuildingLevelConfigs
	}
	return nil
}

func init() {
	proto.RegisterType((*BuildingLevelConfigConf)(nil), "rawdata.BuildingLevelConfig_conf")
	proto.RegisterMapType((map[int32]*BuildingLevelConfig)(nil), "rawdata.BuildingLevelConfig_conf.BuildingLevelConfigsEntry")
}

func init() {
	proto.RegisterFile("rawdata/BuildingLevelConfig_conf.proto", fileDescriptor_318c6c50f28fdd70)
}

var fileDescriptor_318c6c50f28fdd70 = []byte{
	// 195 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xe2, 0x52, 0x2b, 0x4a, 0x2c, 0x4f,
	0x49, 0x2c, 0x49, 0xd4, 0x77, 0x2a, 0xcd, 0xcc, 0x49, 0xc9, 0xcc, 0x4b, 0xf7, 0x49, 0x2d, 0x4b,
	0xcd, 0x71, 0xce, 0xcf, 0x4b, 0xcb, 0x4c, 0x8f, 0x4f, 0xce, 0xcf, 0x4b, 0xd3, 0x2b, 0x28, 0xca,
	0x2f, 0xc9, 0x17, 0x62, 0x87, 0xaa, 0x93, 0x52, 0xc4, 0xa3, 0x01, 0xa2, 0x56, 0xe9, 0x13, 0x23,
	0x97, 0x04, 0x2e, 0xe3, 0x84, 0xf2, 0xb9, 0x44, 0xb0, 0xc8, 0x15, 0x4b, 0x30, 0x2a, 0x30, 0x6b,
	0x70, 0x1b, 0x59, 0xeb, 0x41, 0x8d, 0xd7, 0xc3, 0xe9, 0x1e, 0x6c, 0xba, 0x5d, 0xf3, 0x4a, 0x8a,
	0x2a, 0x83, 0x44, 0x92, 0xb0, 0x48, 0x49, 0xa5, 0x72, 0x49, 0xe2, 0xd4, 0x22, 0x24, 0xc0, 0xc5,
	0x9c, 0x9d, 0x5a, 0x29, 0xc1, 0xa8, 0xc0, 0xa8, 0xc1, 0x1a, 0x04, 0x62, 0x0a, 0x19, 0x71, 0xb1,
	0x96, 0x25, 0xe6, 0x94, 0xa6, 0x4a, 0x30, 0x29, 0x30, 0x6a, 0x70, 0x1b, 0xc9, 0xe0, 0x73, 0x50,
	0x10, 0x44, 0xa9, 0x15, 0x93, 0x05, 0xa3, 0x93, 0x52, 0x94, 0x58, 0x71, 0x6a, 0x51, 0x59, 0x6a,
	0x91, 0x7e, 0x41, 0x76, 0xba, 0x7e, 0x7a, 0x6a, 0x9e, 0x3e, 0x54, 0xe3, 0x2a, 0x26, 0x58, 0xd8,
	0x25, 0xb1, 0x81, 0xc3, 0xc7, 0x18, 0x10, 0x00, 0x00, 0xff, 0xff, 0x5c, 0x5a, 0xd9, 0xb8, 0x75,
	0x01, 0x00, 0x00,
}
