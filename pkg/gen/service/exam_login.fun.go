// Code generated by protoc-gen-fun. DO NOT EDIT.
// source: service/exam_login.proto

package service

import proto "github.com/golang/protobuf/proto"
import fmt "fmt"
import math "math"
import msg5 "server/pkg/gen/msg"

import (
	ark "bitbucket.org/funplus/ark"
	err2 "bitbucket.org/funplus/golib/err2"
	link "bitbucket.org/funplus/golib/net/link"
	sandwich "bitbucket.org/funplus/sandwich"
	current "bitbucket.org/funplus/sandwich/current"
	message "bitbucket.org/funplus/sandwich/message"
	netutils "bitbucket.org/funplus/sandwich/protocol/netutils"
	context "context"
	reflect "reflect"
	strings "strings"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf
var _ = msg5.ExamLoginReq{}

// Reference imports to suppress errors if they are not otherwise used.
var _ ark.Context
var _ netutils.RawPacket
var _ message.Message
var _ reflect.Type
var _ context.Context
var _ sandwich.Codec
var _ strings.Builder
var _ = current.NewContext(context.Background())
var _ link.Session
var _ err2.AppError

func init() {
	if _, err := sandwich.RegisterMessage(&msg5.ExamLoginReq{}); err != nil {
		panic(fmt.Errorf("register message failed: msg5.ExamLoginReq, err:%w", err))
	}
	if _, err := sandwich.RegisterMessage(&msg5.ExamLoginRes{}); err != nil {
		panic(fmt.Errorf("register message failed: msg5.ExamLoginReq, err:%w", err))
	}
}

// gen service ExamLoginService
type ExamLoginService interface {
	ExamLogin(context.Context, *msg5.ExamLoginReq) (*msg5.ExamLoginRes, error)
}

// gen server side proxy
type serverProxyExamLoginService struct {
	ExamLoginService
}

func (h *serverProxyExamLoginService) ExamLoginProxy(ctx context.Context, in interface{}) (interface{}, error) {
	if req, ok := in.(*msg5.ExamLoginReq); ok {
		return h.ExamLoginService.ExamLogin(ctx, req)
	} else {
		return nil, err2.AppErrorFromCode(int32(netutils.ErrorCode_MessageCastError))
	}
}

// tcp handler
func RegisterExamLoginServiceTcpHandler(r sandwich.Router, s ExamLoginService) {
	h := &serverProxyExamLoginService{ExamLoginService: s}

	if uri, err := sandwich.RegisterMessage(&msg5.ExamLoginReq{}); err != nil {
		panic(fmt.Errorf("service register failed with RegisterMessage: msg5.ExamLoginReq, err:%w", err))
	} else {
		r.SetMessageHandler(uri, h.ExamLoginProxy)
	}
}

// http handler
func RegisterExamLoginServiceHttpHandler(r ark.Router, s ExamLoginService) {
	h := &serverProxyExamLoginService{ExamLoginService: s}

	// base on proto uri
	{
		t := &msg5.ExamLoginReq{}
		m := h.ExamLoginProxy
		if err := sandwich.RegisterMessageUnder(r, t, m); err != nil {
			panic(fmt.Errorf("tcp service register failed with RegisterMessage:msg5.ExamLoginReq, err:%w", err))
		}
		path := strings.TrimPrefix("/auto/msg5.ExamLoginReq", r.BasePath())
		r.Get(path, sandwich.ArkCommonHandle(t, m))
		r.Post(path, sandwich.ArkCommonHandle(t, m))
	}
}
