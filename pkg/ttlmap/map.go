package ttlmap

import (
	"sync"
	"time"
)

type item struct {
	raw    interface{}
	expire time.Time
}
type TTLMap struct {
	m   sync.Map
	ttl time.Duration
}

func New(ttl time.Duration) *TTLMap {
	m := &TTLMap{ttl: ttl}
	if ttl > 0 {
		go func() {
			for now := range time.Tick(time.Minute) {
				m.m.Range(func(key, value interface{}) bool {
					it := value.(*item)
					if it.expire.Before(now) {
						m.m.Delete(key)
					}
					return true
				})
			}
		}()
	}
	return m
}

func (m *TTLMap) Load(id interface{}) (interface{}, bool) {
	if d, ok := m.m.Load(id); ok {
		if d.(*item).expire.Before(time.Now()) {
			return nil, false
		}
		return d.(*item).raw, true
	}
	return nil, false
}

func (m *TTLMap) Store(id interface{}, raw interface{}) {
	m.m.Store(id, &item{raw: raw, expire: time.Now().Add(m.ttl)})
}
