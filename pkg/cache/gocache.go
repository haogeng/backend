package cache

import (
	gocache "github.com/patrickmn/go-cache"
	"sync"
	"time"
)

var _goCache *gocache.Cache
var once sync.Once

func GetGoCache() *gocache.Cache {
	once.Do(func() {
		if _goCache == nil {
			_goCache = gocache.New(time.Minute*5, time.Minute*10)
		}
	})
	return _goCache
}
