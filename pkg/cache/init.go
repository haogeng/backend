package cache

import (
	"github.com/go-redis/redis/v8"
)

// cache层暂时支持redis，，其他的以后再扩展
type RedisConfig struct {
	// 如果集群地址不为空，则优先使用集群地址
	ClusterAddrs    []string `json:"clusteraddrs"`
	ClusterPassword string   `json:"clusterpassword"`

	//
	Addr     string `json:"addr"`
	Password string `json:"password"`

	Db int `json:"db"`
}

// 和目前框架兼容，，错误值使用bool
//type keyCache interface {
//	Get(key string) (value []byte, bool)
//	Set(key string, value []byte, expiration time.Duration) bool
//}

var Redis *redisKeyCache

// 单机内存缓存
//var BigCache *bigcache.BigCache

func MustInit(o *RedisConfig) {
	// 优先cluster 然后是单点
	var clusterO *redis.ClusterOptions
	var singleO *redis.Options
	if len(o.ClusterAddrs) > 0 {
		clusterO = &redis.ClusterOptions{
			Addrs:    o.ClusterAddrs,
			Password: o.ClusterPassword,
		}
	} else {
		singleO = &redis.Options{
			Addr:      o.Addr,
			OnConnect: nil,
			Password:  o.Password,
			DB:        o.Db,
			//IdleTimeout: time.Second,
			//PoolSize:   1, // 缺省10/per cpu
			MaxRetries: 0}
	}
	Redis = NewRedisKeyCache(singleO, clusterO)

	//BigCache, _ = bigcache.NewBigCache(bigcache.DefaultConfig(10 * time.Minute))
}
