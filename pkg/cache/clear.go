package cache

import "context"

func ClearOnRelogin(uid uint64) {
	clearResponseCache(uid)
	clearLastRequestId(uid)
	clearLastRequestLock(uid)
}

func clearResponseCache(uid uint64) {
	key := GetResponseKey(uid)
	ctx, _ := context.WithTimeout(context.Background(), KRedisTimeOutLong)
	Redis.Cmd.Del(ctx, key)
}

func clearLastRequestId(uid uint64) {
	if key := GetSeqIdKey(uid); key != "" {
		ctx, _ := context.WithTimeout(context.Background(), KRedisTimeOutLong)
		Redis.Cmd.Del(ctx, key)
	}
}

func clearLastRequestLock(uid uint64) {
	if key := GetLockKeyFor(GetSeqIdKey(uid)); key != "" {
		ctx, _ := context.WithTimeout(context.Background(), KRedisTimeOutLong)
		Redis.Cmd.Del(ctx, key)
	}
}
