package cache

import (
	"fmt"
)

//const (
//	Prefix = "gmx"
//)

// 包括tokenMd5信息等更新频率不高的
func GetTokenUuidKey(pid uint64) string {
	return fmt.Sprintf("gmx:tkuuid:{%d}", pid)
}

func GetSeqIdKey(pid uint64) string {
	return fmt.Sprintf("gmx:seqid:{%d}", pid)
}

func GetResponseKey(pid uint64) string {
	return fmt.Sprintf("gmx:resp:{%d}", pid)
}

// dbVer = ServerMainVer.ServerMinorVer
func GetDbKey(dbVer string, table string, id uint64) string {
	return fmt.Sprintf("gmx:db.{%s}.%s:{%d}", dbVer, table, id)
}

func GetLockKeyFor(forWhich string) string {
	return "gmx:lock:" + forWhich
}
