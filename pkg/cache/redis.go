package cache

import (
	"context"
	"fmt"
	"github.com/go-redis/redis/v8"
	"time"
)

// Redis cluster test env: https://github.com/Grokzen/docker-redis-cluster

type redisKeyCache struct {
	// 不要直接使用client & clusterClient
	client        *redis.Client
	clusterClient *redis.ClusterClient

	// 命令执行者，会指向client or clusterClient
	Cmd redis.Cmdable
}

const (
	KRedisTimeOutShort = time.Second * 10
	KRedisTimeOutLong  = time.Second * 30
)

func NewRedisKeyCache(o *redis.Options, clusterO *redis.ClusterOptions) *redisKeyCache {
	ret := &redisKeyCache{
		client:        nil,
		clusterClient: nil,
		Cmd:           nil,
	}

	// 优先cluster 然后是单点
	if clusterO != nil {
		c := redis.NewClusterClient(clusterO)
		ret.clusterClient = c
		ret.Cmd = c
	} else if o != nil {
		c := redis.NewClient(o)
		ret.client = c
		ret.Cmd = c
	} else {
		panic(fmt.Errorf("not found redis config"))
	}

	ctx, cancel := context.WithTimeout(context.Background(), KRedisTimeOutLong)
	defer cancel()
	_, err := ret.Cmd.Ping(ctx).Result()
	if err != nil {
		panic(fmt.Errorf("%v\n", err))
		return nil
	}

	return ret
}

func (m *redisKeyCache) Expire(key string, d time.Duration) {
	ctx, cancel := context.WithTimeout(context.Background(), KRedisTimeOutShort)
	defer cancel()

	m.Cmd.Expire(ctx, key, d)
	if ctx.Err() != nil {
		panic(ctx.Err())
	}
}

func (m *redisKeyCache) Del(keys ...string) {
	ctx, cancel := context.WithTimeout(context.Background(), KRedisTimeOutLong)
	defer cancel()

	m.Cmd.Del(ctx, keys...)
	if ctx.Err() != nil {
		panic(ctx.Err())
	}
}

// 原子删除
var KDelXLuaScript = `
if redis.call("get",KEYS[1]) == ARGV[1] then
	return redis.call("del",KEYS[1])
else
	return 0
end`

func (m *redisKeyCache) DelX(key string, value string) bool {
	ctx, cancel := context.WithTimeout(context.Background(), KRedisTimeOutLong)
	defer cancel()

	// lua support
	val, err := m.Cmd.Eval(ctx, KDelXLuaScript, []string{key}, value).Result()
	if err != nil {
		fmt.Printf("err %+v", err)
		return false
	}
	// 不关心类型了，直接做字符串比较
	if fmt.Sprintf("%v", val) == "0" {
		return false
	}
	return true
}

func (m *redisKeyCache) Get(key string) ([]byte, bool) {
	ctx, cancel := context.WithTimeout(context.Background(), KRedisTimeOutShort)
	defer cancel()

	val, err := m.Cmd.Get(ctx, key).Result()
	if ctx.Err() != nil {
		panic(ctx.Err())
	}
	return []byte(val), err == nil
}

func (m *redisKeyCache) Set(key string, val []byte, expiration time.Duration) bool {
	ctx, cancel := context.WithTimeout(context.Background(), KRedisTimeOutShort)
	defer cancel()

	err := m.Cmd.Set(ctx, key, val, expiration).Err()
	if ctx.Err() != nil {
		panic(ctx.Err())
	}
	return err == nil
}
func (m *redisKeyCache) SetNX(ctx context.Context, key string, value interface{}, expiration time.Duration) bool {
	ctx, cancel := context.WithTimeout(context.Background(), KRedisTimeOutShort)
	defer cancel()

	ok, _ := m.Cmd.SetNX(ctx, key, value, expiration).Result()
	if ctx.Err() != nil {
		panic(ctx.Err())
	}
	return ok
}
