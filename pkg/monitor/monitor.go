package monitor

import (
	"bitbucket.org/funplus/golib/xdb"
	"context"
	"errors"
	"fmt"
	"server/internal/log"
	"server/pkg/cache"
	"server/pkg/persistredis"
	"sync"
	"time"
)

// 监控redis和db连接
type PingFunc func() error

type monitorTarget struct {
	Name     string
	Interval time.Duration

	IsOk bool
	PingFunc
	RttWindow []int64
}

var monitorTargets = make(map[string]*monitorTarget)

func newMonitorTarget(name string, interval time.Duration, pingFunc PingFunc) *monitorTarget {
	m := &monitorTarget{
		Name:      name,
		Interval:  interval,
		IsOk:      false,
		PingFunc:  pingFunc,
		RttWindow: make([]int64, WindowSize),
	}
	return m
}

func (m *monitorTarget) AvgWindow() float64 {
	w := m.RttWindow

	if len(w) == 0 {
		return 0
	}

	var s int64
	for i := 0; i < len(w); i++ {
		s += w[i]
	}

	return float64(s*1000) / float64(time.Millisecond) / float64(1000) / float64(len(w))
}

func (m *monitorTarget) EnqueueWindow(duration int64) {
	m.RttWindow = append(m.RttWindow[1:len(m.RttWindow)], duration)
}

const (
	WindowSize           = 10
	RedisMonitorInterval = time.Second * 2
	MysqlMonitorInterval = time.Second * 2
)

var (
	NameMysql        = "mysql"
	NameCacheRedis   = "cache-redis"
	NamePersistRedis = "persist-redis"

	ErrRedisConLost = errors.New("redis con lost")
	ErrMysqlConLost = errors.New("mysql con lost")

	errConLost = errors.New("con lost")
)

var once sync.Once
var dbConn xdb.DB

func MustInit(db xdb.DB) {
	dbConn = db

	once.Do(doMonitor)
}

func doMonitor() {
	// init monitorTarget
	t_db := newMonitorTarget(NameMysql, MysqlMonitorInterval, func() error {
		if dbConn == nil {
			return errConLost
		}
		rows, err := dbConn.Query("select now();")
		if err == nil {
			// do consume (no meaning..)
			for rows.Next() {
			}

			defer func() { _ = rows.Close() }()
		}

		return err
	})
	monitorTargets[NameMysql] = t_db

	t_redis := newMonitorTarget(NameCacheRedis, RedisMonitorInterval, func() error {
		ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
		defer cancel()
		_, err := cache.Redis.Cmd.Ping(ctx).Result()
		return err
	})
	monitorTargets[NameCacheRedis] = t_redis

	t_persistRedis := newMonitorTarget(NamePersistRedis, MysqlMonitorInterval, func() error {
		ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
		defer cancel()
		_, err := persistredis.Cmd.Ping(ctx).Result()
		return err
	})
	monitorTargets[NamePersistRedis] = t_persistRedis

	for _, t := range monitorTargets {
		go monitorT(t)
	}
}

func monitorT(t *monitorTarget) {
	lastReportTime := time.Now()

	for {
		curTime := time.Now().UnixNano()

		err := t.PingFunc()

		diffTime := time.Now().UnixNano() - curTime

		if err != nil {
			fmt.Printf("%s con: %v\n", t.Name, err)
			t.IsOk = false

			if time.Now().Sub(lastReportTime) > time.Minute*10/60 {
				fmt.Printf("TODO!!!!!! report to operation cause %s conn failed.", t.Name)
				lastReportTime = time.Now()
				log.Error("found %s connect lost in monitor", t.Name)
			}

			// failed
			//diffTime = int64(time.Second * 5)
		} else {
			t.IsOk = true
		}

		t.EnqueueWindow(diffTime)

		time.Sleep(t.Interval)
	}
}

func Check() error {
	if !monitorTargets[NameCacheRedis].IsOk {
		return ErrRedisConLost
	}

	if !monitorTargets[NameMysql].IsOk {
		return ErrMysqlConLost
	}

	// 不用管排行榜的！

	return nil
}

func GetAvgRtt(name string) float64 {
	t, ok := monitorTargets[name]
	if ok {
		return t.AvgWindow()
	}

	return -1
}
