// Code generated by gen_redis. DO NOT EDIT.
// Note .redis.go 代码是根据gen_db/dao/*.dao.go 生成

package redisdb

import (
	"database/sql"
	"encoding/binary"
	"fmt"
	"hash/crc32"
	"server/internal/log"
	"server/pkg/cache"
	"server/pkg/determinproto"
	"server/pkg/gen/msg"
	"server/pkg/gen_db/dao"
	"strings"
)

// Example:
func examplePlayerInfoCache() {
	var c PlayerInfoCache
	id := uint64(33)
	r := c.Find(id)
	if r != nil {
		// do sth

		// store if needed
		c.MustUpdate(id, r)
	}
}

// 没必要单独设ttl
type PlayerInfoCache struct {
}

// Hook
type OnBeforePlayerInfoMustUpdate func(_data *msg.PlayerInfo)

var onBeforePlayerInfoMustUpdate OnBeforePlayerInfoMustUpdate

func SetOnBeforePlayerInfoMustUpdate(o OnBeforePlayerInfoMustUpdate) {
	onBeforePlayerInfoMustUpdate = o
}

// 配合LockForWhich使用
func LockKeyPostfixOfPlayerInfo(id uint64) string {
	return fmt.Sprintf("player_info:{%d}", id)
}

func (m *PlayerInfoCache) key(id uint64) string {
	return cache.GetDbKey(Config.DbVer, "player_info", id)
}

// 从redis中删除(失效)
func (m *PlayerInfoCache) ExpireRedis(id uint64) {
	cache.Redis.Del(m.key(id))
}

func (m *PlayerInfoCache) MustCreate(id uint64, _data *msg.PlayerInfo) (_id uint64) {
	if id == 0 {
		panic("id should not be 0 in PlayerInfo")
	}

	var _err error
	// fix data id
	if id != 0 && _data.Id == 0 {
		_data.Id = id
	}

	_id, _err = dao.PlayerInfo.Create(id, _data)
	if _err == nil {
		// Cache it.
		m.Find(_id)
	}
	// 直接panic，第一时间发现问题
	if _err != nil {
		panic(_err)
	}
	return _id
}

// 注意，Find每次拿回去的都是一个新的对象
// 在一个处理流程里，只应该拿一次Find，然后通过参数传递或context传递
// 每一个处理流程里，几乎会对应一个Update存档
func (m *PlayerInfoCache) Find(id uint64) (ret *msg.PlayerInfo) {
	if !Config.inited {
		panic(fmt.Errorf("Pls init gen_redis config first\n"))
	}

	key := m.key(id)
	// load from redis
	// bx由两部分组成，msg (crc=0) 和独立的crc
	bx, ok := cache.Redis.Get(key)
	if ok && len(bx) > 0 {
		var err error
		if len(bx) < 4 {
			panic("len err")
		}
		if err == nil {
			b1 := bx[:len(bx)-4]
			b2 := bx[len(bx)-4:]

			tempCrc := binary.LittleEndian.Uint32(b2)

			ret = &msg.PlayerInfo{}
			err = determinproto.Unmarshal(b1, ret)
			if err != nil {
				panic(err)
			}

			ret.Crc32 = int32(tempCrc)

			if Config.debugMode {
				fmt.Printf("Loaded %s from redis\n", key)
			}
			// update redis expire
			cache.Redis.Expire(key, Config.Ttl)
			return ret
		} else {
			// reset to nil
			ret = nil
		}
	}

	// load from db
	ret, err := dao.PlayerInfo.Find(id)
	if err != nil {
		if !strings.Contains(err.Error(), sql.ErrNoRows.Error()) {
			fmt.Printf("Find %d err:%v\n", id, err)
			panic(err)
		}
	} else if ret != nil {
		b1, err := determinproto.Marshal(ret)
		b2 := make([]byte, 4)
		binary.LittleEndian.PutUint32(b2, uint32(ret.Crc32))
		bx := append(b1, b2...)

		if err == nil && bx != nil {
			if Config.debugMode {
				fmt.Printf("Loaded %s from db\n", key)
			}
			// store into redis
			cache.Redis.Set(key, bx, Config.Ttl)
		}
	}

	// Anyway store into redis even if it's null.
	if ret == nil {
		if Config.debugMode {
			fmt.Printf("Update %s expire when its nil\n", key)
		}
		cache.Redis.Set(key, []byte(""), Config.Ttl)
	}

	return
}

func (m *PlayerInfoCache) MustUpdate(id uint64, _data *msg.PlayerInfo) {
	if !Config.inited {
		panic(fmt.Errorf("Pls init gen_redis config first\n"))
	}

	if id == 0 {
		panic("id should not be 0 in PlayerInfo")
	}

	if onBeforePlayerInfoMustUpdate != nil {
		onBeforePlayerInfoMustUpdate(_data)
	}

	extActivity := _data.Activity
	_data.Activity = nil

	extArena := _data.Arena
	_data.Arena = nil

	extClan := _data.Clan
	_data.Clan = nil

	extEquip := _data.Equip
	_data.Equip = nil

	extExt := _data.Ext
	_data.Ext = nil

	extFriend := _data.Friend
	_data.Friend = nil

	extMaze := _data.Maze
	_data.Maze = nil

	extTroop := _data.Troop
	_data.Troop = nil

	// crc 不应该循环计算，但需要存档，独立追加一个4字节的buf存档crc
	tempCrc := _data.Crc32
	_data.Crc32 = 0
	b1, err := determinproto.Marshal(_data)

	if err != nil {
		panic(err)
	}

	dataLen := len(b1)
	// 默认存档
	dirty := true
	newCrcVal := int32(0)
	// 10K benchmark 20W; dataLen smaller, performance better.
	// 数据小于某个长度，而且没变化的时候，不触发存档
	if dataLen > 0 && dataLen < 1024*32 {
		newCrcVal = int32(crc32.ChecksumIEEE(b1))
		// data no changed
		if tempCrc == newCrcVal {
			dirty = false
		} else {
			tempCrc = newCrcVal
		}
	}

	// 序列化后恢复crc
	_data.Crc32 = newCrcVal
	if Config.debugLogDataLen {
		log.Debugf("[RedisUpdate] PlayerInfo dataLen: %v newCrc: %v dirty: %v", dataLen, newCrcVal, dirty)
	}

	// store into db
	if dirty {
		b2 := make([]byte, 4)
		binary.LittleEndian.PutUint32(b2, uint32(_data.Crc32))
		bx := append(b1, b2...)

		err = dao.PlayerInfo.Update(id, _data)
		if err != nil {
			panic(err)
		}

		// store into redis
		if bx != nil {
			key := m.key(id)
			cache.Redis.Set(key, bx, Config.Ttl)
		}
	}

	_data.Activity = extActivity

	_data.Arena = extArena

	_data.Clan = extClan

	_data.Equip = extEquip

	_data.Ext = extExt

	_data.Friend = extFriend

	_data.Maze = extMaze

	_data.Troop = extTroop

	if _data.Activity != nil {
		var extActivity PlayerInfoActivityCache
		extActivity.MustUpdate(id, _data.Activity)
	}

	if _data.Arena != nil {
		var extArena PlayerInfoArenaCache
		extArena.MustUpdate(id, _data.Arena)
	}

	if _data.Clan != nil {
		var extClan PlayerInfoClanCache
		extClan.MustUpdate(id, _data.Clan)
	}

	if _data.Equip != nil {
		var extEquip PlayerInfoEquipCache
		extEquip.MustUpdate(id, _data.Equip)
	}

	if _data.Ext != nil {
		var extExt PlayerInfoExtCache
		extExt.MustUpdate(id, _data.Ext)
	}

	if _data.Friend != nil {
		var extFriend PlayerInfoFriendCache
		extFriend.MustUpdate(id, _data.Friend)
	}

	if _data.Maze != nil {
		var extMaze PlayerInfoMazeCache
		extMaze.MustUpdate(id, _data.Maze)
	}

	if _data.Troop != nil {
		var extTroop PlayerInfoTroopCache
		extTroop.MustUpdate(id, _data.Troop)
	}

}
