// Code generated by gen_redis. DO NOT EDIT.
// Note .redis.go 代码是根据gen_db/dao/*.dao.go 生成

package redisdb

import (
	"time"
)

type config struct {
	inited          bool
	Ttl             time.Duration
	DbVer           string
	debugMode       bool
	debugLogDataLen bool
}

// default
var Config = &config{}

func MustInit(ttl time.Duration, dbVer string) {
	Config.Ttl = ttl
	Config.DbVer = dbVer

	Config.inited = true

	//Config.debugMode = true
	Config.debugMode = false
	Config.debugLogDataLen = true
}
