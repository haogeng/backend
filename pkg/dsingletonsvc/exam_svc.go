package dsingletonsvc

import (
	"fmt"
	"math/rand"
	"time"
)

type ExamSvc struct {
	// 开始运行服务
	// 通过chan关闭服
	// closeChan 调用者通知svc 该关闭了，比如程序退出
	// svcDoneChan 当svc自身情况，比如panic了，需要通知mgr自身状态变化了
}

func (e *ExamSvc) Run(closeChan chan struct{}) (svcDoneChan chan struct{}) {
	svcDoneChan = make(chan struct{})

	go func(closeChan chan struct{}, svcDoneChan chan struct{}) {
		defer func() {
			if r := recover(); r != nil {
				fmt.Printf("catch panic in ExamSvc:defer\n")
			}
			close(svcDoneChan)
		}()

		// do your logic
		e.run(closeChan)
	}(closeChan, svcDoneChan)

	return
}

func (e *ExamSvc) run(closeChan chan struct{}) {
	t := time.NewTicker(time.Second * 1)
	defer t.Stop()

	// 测试，定期退出
	count := 0
loopFor:
	for {
		count++
		if count > 6 {
			time.Sleep(time.Millisecond * time.Duration(rand.Int31n(1000)))

			if rand.Int()%2 == 0 {
				fmt.Printf("close examsvc by logic normal exit\n")
				break
			} else {
				fmt.Printf("close examsvc by logic normal panic\n")
				panic("examsvc panic exit")
			}
		}

		select {
		case _ = <-t.C:
			break
		case <-closeChan:
			fmt.Printf("close examsvc by closeChan\n")
			break loopFor
		}
	}
}

type ExamSvcProvider struct {
}

func (e *ExamSvcProvider) NewSvc() Svc {
	return &ExamSvc{}
}
