// 需求背景：分布式环境下，需要一个简单的单一运行实例
// 寄生于主进程的单例服务，包括单例状态机等
// 满足bitbucket.org/funplus/golib/module接口
package dsingletonsvc

import (
	"bytes"
	"fmt"
	uuid2 "github.com/hashicorp/go-uuid"
	"sync"
	"time"
)

var (
	// 服务扫描间隔
	scanInterval = time.Second * 10

	// 服务
	svcTtlDuration = time.Second * 20

	// 服务保活间隔
	svcKeepAliveIntervalDuration = time.Second * 2
)

// 每个服务，伴随一个自动的心跳维护，，如果外界挂掉的话，则取消心跳维护
// 相对module，起到监控作用，如果没有发现对应节点，则自动拉起
// 当服务很多的时候，再考虑拆到单独的微服务里
type dSingletonSvcMgr struct {
	// 依托的进程唯一uuid
	uuid  []byte
	sUuid string

	kvsProvider  KvsProvider
	lockProvider LockProvider

	// 运行时不能改变，所以不用加锁
	svcProviders map[string]SvcProvider

	// map: serverName -> closeChan
	svcCloseChans sync.Map

	printf PrintFunc
}

var Mgr *dSingletonSvcMgr

// lock
type LockProvider interface {
	AcquireLockWithTimeoutX(key string, ttl time.Duration, timeOut time.Duration) (lockValue string, ok bool)
	ReleaseLockX(key string, lockValue string)
}

type KvsProvider interface {
	Get(key string) ([]byte, bool)
	Set(key string, val []byte, expiration time.Duration) bool
	Expire(key string, d time.Duration)
}

type PrintFunc func(format string, v ...interface{})

func newSingletonSvnMgr(lockProvider LockProvider, kvsProvider KvsProvider,
	svcProviders map[string]SvcProvider, printFunc PrintFunc, nodeId string) *dSingletonSvcMgr {
	if len(nodeId) == 0 {
		uuid, err := uuid2.GenerateUUID()
		if err != nil {
			panic(err)
		}
		nodeId = uuid
	}

	p := &dSingletonSvcMgr{
		uuid:         []byte(nodeId),
		sUuid:        nodeId,
		lockProvider: lockProvider,
		kvsProvider:  kvsProvider,
		svcProviders: svcProviders,
		printf:       printFunc,
	}

	for k, _ := range p.svcProviders {
		p.setCloseSvcChan(k, nil)
	}

	return p
}

func Init(lockProvider LockProvider, kvsProvider KvsProvider,
	svcProviders map[string]SvcProvider, printFunc PrintFunc, nodeId string) {
	// repeat create
	if Mgr != nil {
		panic("repeated Init")
	}

	Mgr = newSingletonSvnMgr(lockProvider, kvsProvider, svcProviders, printFunc, nodeId)
}

func (d *dSingletonSvcMgr) OnInit() {
	d.printf("dSingletonSvcMgr OnInit(%s)", d.sUuid)
}

func (d *dSingletonSvcMgr) OnClose() {
	d.printf("dSingletonSvcMgr OnClose(%s)", d.sUuid)

	// expire all svc at once
	for k, _ := range d.svcProviders {
		serverKeyName := d.serverNameKey(k)
		d.printf("remove svc %s(%s)", k, serverKeyName)
		d.kvsProvider.Expire(serverKeyName, 0)
	}
}

func (d *dSingletonSvcMgr) Run(closeChan chan struct{}) {
	d.printf("dSingletonSvcMgr Run(%s)", d.sUuid)

	for _k, _v := range d.svcProviders {
		k, v := _k, _v
		go func() {
			for {
				// 会挂起
				d.scanSvc(k, v, closeChan)

				// 间隔扫描
				time.Sleep(scanInterval)

				needClose := false
				select {
				case <-closeChan:
					needClose = true
					break
				default:
					break
				}

				if needClose {
					d.printf("close scan svc[%s]", d.sUuid)
					break
				}
			}
		}()
	}

	<-closeChan

	// 关闭所有的svcCloseChan
	for k, _ := range d.svcProviders {
		d.tryCloseSvcChan(k)
	}
}

func (d *dSingletonSvcMgr) tryCloseSvcChan(k string) {
	cc := d.getCloseSvcChan(k)
	if cc != nil {
		close(cc)
		d.svcCloseChans.Store(k, nil)
	}
}

func (d *dSingletonSvcMgr) getCloseSvcChan(k string) chan struct{} {
	tempValue, ok := d.svcCloseChans.Load(k)
	if ok {
		cc, ok := tempValue.(chan struct{})
		if ok {
			return cc
		}
	}
	return nil
}
func (d *dSingletonSvcMgr) setCloseSvcChan(k string, c chan struct{}) {
	// 注意区分纯的nil 和有特定类型的nil值
	if c == nil {
		d.svcCloseChans.Store(k, nil)
	} else {
		d.svcCloseChans.Store(k, c)
	}
}

func (d *dSingletonSvcMgr) scanSvc(k string, v SvcProvider, closeChan chan struct{}) {
	//d.printf("node[%s] scanSvc %s", d.sUuid, k)

	defer func() {
		// 需要足够的健壮
		if r := recover(); r != nil {
			d.printf("scanSvc catchErr name:[%s] err:[%v]", k, r)
		}
	}()

	// redis的get set超时等也会返回失败，但没有区分
	// 通过乐观锁及状态一致性保证强一致性
	// 宁可错杀，不要多起
	serverKeyName := d.serverNameKey(k)
	nodeId, ok := d.kvsProvider.Get(serverKeyName)
	if ok {
		if bytes.Compare(nodeId, d.uuid) != 0 {
			// 关闭不一致的服，保持单点性
			cc := d.getCloseSvcChan(k)
			if cc != nil {
				d.printf("close svc[%s] cause repeated", d.sUuid)
				d.tryCloseSvcChan(k)
			}
		}
		return
	}

	// 如果没有服务，获取锁后，启动服务
	lockKey := d.serverNameLockKey(k)
	lockValue, ok := d.lockProvider.AcquireLockWithTimeoutX(lockKey, time.Second*20, time.Second*10)
	if !ok {
		return
	}
	defer d.lockProvider.ReleaseLockX(lockKey, lockValue)

	// double check: 已注册服务
	nodeId, ok = d.kvsProvider.Get(serverKeyName)
	if ok {
		// 此处不再尝试关闭冗余服，直接返回
		return
	}

	// 注册服务
	d.kvsProvider.Set(serverKeyName, d.uuid, svcTtlDuration)

	// 启动服务
	d.printf("node[%s] start svc[%s]", d.sUuid, k)
	if d.getCloseSvcChan(k) != nil {
		panic(fmt.Errorf("d.svcCloseChans[%s] != nil", d.sUuid))
	}
	closeSvcChan := make(chan struct{})
	d.setCloseSvcChan(k, closeSvcChan)
	svcDoneChan := v.NewSvc().Run(closeSvcChan)

	// 定时保活
	keepAliveTick := time.NewTicker(svcKeepAliveIntervalDuration)
	defer keepAliveTick.Stop()

loopForKeepAlive:
	for {
		//d.printf("svc[%s] keepAlive", k)
		d.kvsProvider.Expire(serverKeyName, svcTtlDuration)

		select {
		case _ = <-keepAliveTick.C:
			break
		case <-svcDoneChan:
			// reset 保持状态一致性
			d.tryCloseSvcChan(k)
			// 直接通知kvs 服务关停了
			d.kvsProvider.Expire(serverKeyName, 0)
			break loopForKeepAlive
		}
	}
}

func (d *dSingletonSvcMgr) serverNameKey(name string) string {
	return fmt.Sprintf("dsingleton:svc:{%s}", name)
}
func (d *dSingletonSvcMgr) serverNameLockKey(name string) string {
	return fmt.Sprintf("dsingleton:svclock:{%s}", name)
}

func (d *dSingletonSvcMgr) Name() string {
	return "module-dsingleton-svc"
}
