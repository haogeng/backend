package dsingletonsvc

type Svc interface {
	// 开始运行服务
	// 通过chan关闭服
	// closeChan 调用者通知svc 该关闭了，比如程序退出
	// svcDoneChan 当svc自身情况，比如panic了，需要通知mgr自身状态变化了
	Run(closeChan chan struct{}) (svcDoneChan chan struct{})
}

type SvcProvider interface {
	NewSvc() Svc
}
