package dsingletonsvc

import (
	"fmt"
	"server/pkg/cache"
	"server/pkg/dlock"
	"testing"
	"time"
)

var mgr1 *dSingletonSvcMgr
var mgr2 *dSingletonSvcMgr

func printf(format string, v ...interface{}) {
	fmt.Printf(format+"\n", v...)
}

func init() {
	c := &cache.RedisConfig{
		Addr: "127.0.0.1:6379",
	}
	cache.MustInit(c)
	dlock.Init(cache.Redis)

	Init(&dlock.DLockWrapper{},
		cache.Redis,
		map[string]SvcProvider{
			"examsvc": &ExamSvcProvider{},
		},
		printf, "")

	mgr1 = newSingletonSvnMgr(&dlock.DLockWrapper{},
		cache.Redis,
		map[string]SvcProvider{
			"examsvc": &ExamSvcProvider{},
		},
		printf, "")

	mgr2 = newSingletonSvnMgr(&dlock.DLockWrapper{},
		cache.Redis,
		map[string]SvcProvider{
			"examsvc": &ExamSvcProvider{},
		},
		printf, "")
}

//func TestDSingleton(t *testing.T) {
//	m := Mgr
//	m.OnInit()
//	c := make(chan struct{})
//	m.Run(c)
//}

func TestDSingletonMulti(t *testing.T) {
	// 强化测试
	scanInterval = time.Second / 2

	mgr1.OnInit()
	mgr2.OnInit()
	Mgr.OnInit()

	c := make(chan struct{})

	go Mgr.Run(c)
	go mgr1.Run(c)
	go mgr2.Run(c)

	time.Sleep(time.Hour * 2)
}
