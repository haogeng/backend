# dsingletonsvc
> 分布式环境下的单件服务(状态机)  
> 如无状态服里的赛季状态

## 背景
* 分布式环境和无状态服务天然契合
* 但偶尔因业务需求，需要运行分布式环境下的单件状态机
* 可选方案有(A)单独部署为微服务，(B)寄宿于主业务服
* 考虑开发和运维部署的便利性，以及游戏业务里主业务服和状态机的轻重关系，选择B方案

## 方案描述
* 主业务服上寄宿一个该模块，做为单件状态机的维护程序
* 可注册多种类型的分布式单件状态机
* 通过分布式锁和kvs，保证特定类型的状态机在多个分布式主业务服上有且只运行一个实例

## 使用
```
svcData := map[string]dsingletonsvc.SvcProvider{} 
svcData["examsvc"] = &dsingletonsvc.ExamSvcProvider{} 
dsingletonsvc.Init(&dlock.DLockWrapper{}, cache.Redis, svcData, log.Infof)
```