package persistredis

import (
	"github.com/go-redis/redis/v8"
)

// cache层暂时支持redis，，其他的以后再扩展
type PersistRedisConfig struct {
	// 如果集群地址不为空，则优先使用集群地址
	ClusterAddrs    []string `json:"clusteraddrs"`
	ClusterPassword string   `json:"clusterpassword"`

	//
	Addr     string `json:"addr"`
	Password string `json:"password"`

	Db int `json:"db"`
}

func MustInit(o *PersistRedisConfig) {
	// 优先cluster 然后是单点
	var clusterO *redis.ClusterOptions
	var singleO *redis.Options
	if len(o.ClusterAddrs) > 0 {
		clusterO = &redis.ClusterOptions{
			Addrs:    o.ClusterAddrs,
			Password: o.ClusterPassword,
		}
	} else {
		singleO = &redis.Options{
			Addr:       o.Addr,
			OnConnect:  nil,
			Password:   o.Password,
			DB:         o.Db,
			MaxRetries: 0}
	}
	New(singleO, clusterO)
}
