package persistredis

import (
	"context"
	"fmt"
	"github.com/go-redis/redis/v8"
	"strconv"
	"time"
)

// Redis cluster test env: https://github.com/Grokzen/docker-redis-cluster

type persistRedis struct {
	// 不要直接使用client & clusterClient
	client        *redis.Client
	clusterClient *redis.ClusterClient

	// 命令执行者，会指向client or clusterClient
	Cmd redis.Cmdable
}

var (
	Redis *persistRedis
	// 提供给外界访问
	Cmd redis.Cmdable
)

const (
	KRedisTimeOutShort = time.Second * 10
	KRedisTimeOutLong  = time.Second * 30
)

func New(o *redis.Options, clusterO *redis.ClusterOptions) {
	if Redis != nil {
		panic("repeated New")
	}

	ret := &persistRedis{
		client:        nil,
		clusterClient: nil,
		Cmd:           nil,
	}

	// 优先cluster 然后是单点
	if clusterO != nil {
		c := redis.NewClusterClient(clusterO)
		ret.clusterClient = c
		ret.Cmd = c
	} else if o != nil {
		c := redis.NewClient(o)
		ret.client = c
		ret.Cmd = c
	} else {
		panic(fmt.Errorf("not found redis config"))
	}

	ctx, cancel := context.WithTimeout(context.Background(), KRedisTimeOutLong)
	defer cancel()

	_, err := ret.Cmd.Ping(ctx).Result()
	if err != nil {
		panic(fmt.Errorf("%v\n", err))
	}

	// store in local store!
	Redis = ret
	Cmd = ret.Cmd
}

func (m *persistRedis) Expire(key string, d time.Duration) {
	ctx, cancel := context.WithTimeout(context.Background(), KRedisTimeOutShort)
	defer cancel()

	m.Cmd.Expire(ctx, key, d)
	if ctx.Err() != nil {
		panic(ctx.Err())
	}
}

func (m *persistRedis) Del(keys ...string) {
	ctx, cancel := context.WithTimeout(context.Background(), KRedisTimeOutLong)
	defer cancel()

	m.Cmd.Del(ctx, keys...)
	if ctx.Err() != nil {
		panic(ctx.Err())
	}
}

func (m *persistRedis) Get(key string) ([]byte, bool) {
	ctx, cancel := context.WithTimeout(context.Background(), KRedisTimeOutShort)
	defer cancel()

	val, err := m.Cmd.Get(ctx, key).Result()
	if ctx.Err() != nil {
		panic(ctx.Err())
	}
	return []byte(val), err == nil
}

func (m *persistRedis) Set(key string, val []byte, expiration time.Duration) bool {
	ctx, cancel := context.WithTimeout(context.Background(), KRedisTimeOutShort)
	defer cancel()

	err := m.Cmd.Set(ctx, key, val, expiration).Err()
	if ctx.Err() != nil {
		panic(ctx.Err())
	}
	return err == nil
}

//zadd
func (m *persistRedis) ZAdd(key string, members *redis.Z) bool {
	ctx, cancel := context.WithTimeout(context.Background(), KRedisTimeOutShort)
	defer cancel()

	err := m.Cmd.ZAdd(ctx, key, members).Err()
	if ctx.Err() != nil {
		panic(ctx.Err())
	}
	return err == nil
}

//查区间count
func (m *persistRedis) ZCount(key string, min, max string) (int64, bool) {
	ctx, cancel := context.WithTimeout(context.Background(), KRedisTimeOutShort)
	defer cancel()

	val, err := m.Cmd.ZCount(ctx, key, min, max).Result()
	if ctx.Err() != nil {
		panic(ctx.Err())
	}
	return val, err == nil
}

//根据序号查某个member
func (m *persistRedis) ZRevRange(key string, start, stop int64) ([]string, bool) {
	ctx, cancel := context.WithTimeout(context.Background(), KRedisTimeOutShort)
	defer cancel()

	val, err := m.Cmd.ZRevRange(ctx, key, start, stop).Result()
	if ctx.Err() != nil {
		panic(ctx.Err())
	}
	return val, err == nil
}

//根据分数区间查n个
func (m *persistRedis) ZRevRangeByScore(key string, opt *redis.ZRangeBy) ([]string, bool) {
	ctx, cancel := context.WithTimeout(context.Background(), KRedisTimeOutShort)
	defer cancel()

	val, err := m.Cmd.ZRevRangeByScore(ctx, key, opt).Result()
	if ctx.Err() != nil {
		panic(ctx.Err())
	}
	return val, err == nil
}

// 根据排名区间查询排行榜用户和分数
func (m *persistRedis) GetRankInfosByRank(key string, startRank, stopRank int64) ([]redis.Z, bool) {
	ctx, cancel := context.WithTimeout(context.Background(), KRedisTimeOutShort)
	defer cancel()

	// 反序
	val, err := m.Cmd.ZRevRangeWithScores(ctx, key, startRank, stopRank).Result()
	if ctx.Err() != nil {
		panic(ctx.Err())
	}
	return val, err == nil
}

// 根据分数区间查询排行榜用户和分数
func (m *persistRedis) GetRankInfosByScoreRev(key string, opt *redis.ZRangeBy) ([]redis.Z, bool) {
	ctx, cancel := context.WithTimeout(context.Background(), KRedisTimeOutShort)
	defer cancel()

	// 反序
	val, err := m.Cmd.ZRevRangeByScoreWithScores(ctx, key, opt).Result()
	if ctx.Err() != nil {
		panic(ctx.Err())
	}
	return val, err == nil
}

// 根据用户Id查排名信息
func (m *persistRedis) ZRankInfoById(key string, id uint64) (int64, float64) {
	ctx, cancel := context.WithTimeout(context.Background(), KRedisTimeOutShort)
	defer cancel()

	rank, err := m.Cmd.ZRevRank(ctx, key, strconv.FormatUint(id, 10)).Result()
	if err != nil {
		return 0, -1
	}
	score, err := m.Cmd.ZScore(ctx, key, strconv.FormatUint(id, 10)).Result()
	if err != nil {
		return 0, -1
	}
	// Redis排名从0开始,这里配合客户端做一个+1
	rank = rank + 1
	return rank, score
}

//删除
func (m *persistRedis) ZRemove(key string, members *redis.Z) bool {
	ctx, cancel := context.WithTimeout(context.Background(), KRedisTimeOutShort)
	defer cancel()

	err := m.Cmd.ZRem(ctx, key, members.Member).Err()
	if ctx.Err() != nil {
		panic(ctx.Err())
	}
	return err == nil
}

// 合并多个key的内容到一个key/可用于复制
func (m *persistRedis) ZUnionStore(dest string, src ...string) bool {
	ctx, cancel := context.WithTimeout(context.Background(), KRedisTimeOutShort)
	defer cancel()

	keys := make([]string, len(src))
	for i, v := range src {
		keys[i] = v
	}
	zStore := &redis.ZStore{
		Keys: keys,
	}
	err := m.Cmd.ZUnionStore(ctx, dest, zStore)
	if err != nil {
		return false
	}
	return true
}

//更新分数
func (m *persistRedis) ZIncrBy(key string, increment float64, member string) (floatValue float64) {
	ctx, cancel := context.WithTimeout(context.Background(), KRedisTimeOutShort)
	defer cancel()
	floatValue, err := m.Cmd.ZIncrBy(ctx, key, increment, member).Result()
	if ctx.Err() != nil {
		panic(ctx.Err())
	}
	if err != nil {
		panic(err)
	}
	return
}
