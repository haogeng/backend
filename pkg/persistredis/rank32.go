package persistredis

import (
	"fmt"
	"github.com/go-redis/redis/v8"
	"server/internal/util"
	"strconv"
	"time"
)

// ！！不可随意更改！！
var kScoreStopTime = time.Date(2120, 8, 1, 0, 0, 0, 0, time.Local).Unix()

type debugFunc func(format string, v ...interface{})

var DebugFunc debugFunc

// 用途：
// 满足分数由高到底排行时，优先分数高，分数相同比谁先到
// 分数为int32类型

// 术语：
// key			排行榜的名字
// id			用户id
// score		积分
// deltaScore	增量积分
type rank32 struct {
}

var Rank32 = &rank32{}

// 用于数组返回用
type Rank32Member struct {
	Id    uint64
	Score int32
	// 到达这个分数的时间戳
	Timestamp int64
}

func (m *rank32) DelAll(key string) {
	if DebugFunc != nil {
		DebugFunc("[Rank32] DelAll %v\n", key)
	}
	Redis.Del(key)
}

func (m *rank32) Expire(key string, expire time.Duration) {
	if DebugFunc != nil {
		DebugFunc("[Rank32] Expire %v\n", key)
	}
	Redis.Expire(key, expire)
}

func (m *rank32) Add(key string, id uint64, score32 int32) bool {
	if DebugFunc != nil {
		DebugFunc("[Rank32] Add %v %v %v\n", key, id, score32)
	}

	score := util.Marshal64(m.getScoreDeltaTime(), score32)
	members := &redis.Z{
		Score:  float64(score),
		Member: id,
	}
	return Redis.ZAdd(key, members)
}

func (m *rank32) Remove(key string, id uint64) {
	if DebugFunc != nil {
		DebugFunc("[Rank32] Remove %v %v\n", key, id)
	}

	members := &redis.Z{
		Score:  float64(0),
		Member: id,
	}
	Redis.ZRemove(key, members)
}

//ret: 排名，积分，获得时间戳
func (m *rank32) InfoById(srcKey string, id uint64) (rank int64, score32 int32, ts int64) {
	var score float64
	rank, score = Redis.ZRankInfoById(srcKey, id)
	score32, ts = m.unmarshalScore(score)
	return
}

func (m *rank32) RangeByRank(key string, startRank, stopRank int64) ([]Rank32Member, bool) {
	val, ok := Redis.GetRankInfosByRank(key, startRank, stopRank)
	rets := make([]Rank32Member, len(val))

	for i, v := range val {
		m.fillRank32Member(&v, &rets[i])
	}
	return rets, ok
}

// [) 半开半闭区间
func (m *rank32) RevRangeByScore(key string, startScore32, stopScore32 int32, count int64) ([]Rank32Member, bool) {
	// 特殊问题-ZSet的排序规则为积分相同按Number的字典顺序排序,和排行榜的预期不符
	// 解决方案-通过积分+时间戳的组合让Score尽量不重复,先使用秒级时间戳.如果还不能满足需求将改为毫秒级
	startScore, stopScore := util.Marshal64(0, int32(startScore32)),
		util.Marshal64(0, int32(stopScore32))
	values := &redis.ZRangeBy{
		Min:    strconv.FormatInt(startScore, 10),
		Max:    strconv.FormatInt(stopScore, 10),
		Offset: 0,
		Count:  count,
	}
	val, ok := Redis.GetRankInfosByScoreRev(key, values)
	rets := make([]Rank32Member, len(val))
	for i, v := range val {
		m.fillRank32Member(&v, &rets[i])
	}
	return rets, ok
}

// 复制一整个排行榜
func (m *rank32) CopyAll(key string, destKey string) bool {
	if DebugFunc != nil {
		DebugFunc("[Rank32] CopyAll %v %v\n", key, destKey)
	}
	return Redis.ZUnionStore(destKey, key)
}

// === help function
// 特殊问题-ZSet的排序规则为积分相同按Number的字典顺序排序,和排行榜的预期不符
// 解决方案-通过积分+时间戳的组合让Score尽量不重复,先使用秒级时间戳.如果还不能满足需求将改为毫秒级
func (m *rank32) getTimestamp(scoreDeltaTime int32) int64 {
	s64 := int64(scoreDeltaTime)
	s64 = kScoreStopTime - s64
	return s64
}
func (m *rank32) getScoreDeltaTime() int32 {
	return int32(kScoreStopTime - time.Now().Unix())
}

//
func (m *rank32) unmarshalScore(score float64) (score32 int32, timestamp int64) {
	scoreDeltaTime, score32 := util.Unmarshal64(int64(score))
	timestamp = m.getTimestamp(scoreDeltaTime)
	return
}

// 如果失败返回0，并打印日志
func (m *rank32) getId(member interface{}) uint64 {
	if s, ok := member.(string); ok {
		id, err := strconv.ParseUint(s, 10, 64)
		if err != nil {
			fmt.Printf("[ERROR] getId err:%v\n", err)
			return 0
		}
		return id
	} else {
		// maybe never go here
		if id, ok := member.(uint64); ok {
			return id
		}
	}

	fmt.Printf("[ERROR] getId failed\n")
	return 0
}

func (m *rank32) fillRank32Member(z *redis.Z, member *Rank32Member) {
	member.Id = m.getId(z.Member)
	member.Score, member.Timestamp = m.unmarshalScore(z.Score)
}
