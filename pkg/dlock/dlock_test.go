package dlock

import (
	"fmt"
	uuid2 "github.com/hashicorp/go-uuid"
	"github.com/stretchr/testify/assert"
	"os"
	"server/pkg/cache"
	"strconv"
	"strings"
	"testing"
	"time"
)

func initProvider() {
	c := &cache.RedisConfig{
		Addr: "127.0.0.1:6379",
	}
	cache.MustInit(c)

	Init(cache.Redis)
}

func TestMain(m *testing.M) {
	initProvider()
	code := m.Run()
	os.Exit(code)
}

const (
	prefix = "testdlock:"
)

func BenchmarkNamedMutex(b *testing.B) {
	for i := 0; i < b.N; i++ {
		//uuid, _ := uuid2.GenerateUUID()
		uuid := strconv.Itoa(int(time.Now().Unix()))
		namedMutex.Lock(uuid)
		namedMutex.Unlock(uuid)
	}
}

func TestNamedMutex(t *testing.T) {
	go func() {
		namedMutex.Lock("a")
		// 同一个goroutine里会触发死锁
		//namedMutex.Lock("a")
		time.Sleep(time.Second * 2)
		namedMutex.Unlock("a")
	}()

	time.Sleep(time.Second * 1)
	t.Log(fmt.Sprintf("req lock %v", time.Now()))

	namedMutex.Lock("a")
	t.Log(fmt.Sprintf("got lock %v", time.Now()))

	namedMutex.Unlock("a")
	t.Log(fmt.Sprintf("unlock lock %v", time.Now()))

	assert.Equal(t, 0, len(namedMutex.Data))
}

func TestDelX(t *testing.T) {
	key := "testdelx"
	v, ok := AcquireLockX(key, time.Second*10)
	assert.True(t, ok)

	ok = provider.DelX(key, "aa")
	assert.False(t, ok)

	ok = provider.DelX(key, v)
	assert.True(t, ok)
}

func TestLongKey(t *testing.T) {
	key := strings.Repeat("a", 1024*8)

	assert.Panics(t, func() { AcquireLock(key, time.Second) }, "")
	assert.Panics(t, func() { AcquireLockX(key, time.Second) }, "")
	// 嵌套go程不管用
	//assert.Panics(t, func() { AcquireLockWithTimeoutX(key, time.Second, time.Second) }, "")
	//assert.Panics(t, func() { AcquireLockWithTimeout(key, time.Second, time.Second) }, "")
}

func TestAcquireLock(t *testing.T) {
	key := prefix + "TestAcquireLock" + "1"
	key2 := prefix + "TestAcquireLock" + "2"

	// acquire -> wait -> acquire
	ok := AcquireLock(key, time.Second)
	assert.True(t, ok, "")

	ok = AcquireLock(key, time.Second)
	assert.False(t, ok, "")

	ok = AcquireLock(key2, time.Second)
	assert.True(t, ok, "")

	ReleaseLock(key2)

	ok = AcquireLock(key2, time.Second)
	assert.True(t, ok, "")

	ok = AcquireLock(key, time.Second)
	assert.False(t, ok, "")

	time.Sleep(time.Second / 2)
	ok = AcquireLock(key, time.Second)
	assert.False(t, ok, "")

	time.Sleep(time.Second / 2)
	ok = AcquireLock(key, time.Second)
	assert.True(t, ok, "")
}

func TestAcquireLockX(t *testing.T) {
	key := prefix + "TestAcquireLockX" + "1"
	//key2 := prefix + "TestAcquireLockX" + "2"

	lockValue, ok := AcquireLockX(key, time.Second)
	assert.True(t, ok, "")

	ReleaseLockX(key, "x")
	_, ok = AcquireLockX(key, time.Second)
	assert.False(t, ok, "")

	ReleaseLockX(key, lockValue)
	_, ok = AcquireLockX(key, time.Second)
	assert.True(t, ok, "")
}

func TestAcquireLockWithTimeoutX(t *testing.T) {
	key := prefix + "TestAcquireLockWithTimeoutX" + "1"
	localValue, ok := AcquireLockWithTimeoutX(key, time.Second*5, time.Second)
	assert.True(t, ok)
	assert.NotEmpty(t, localValue)

	tm := time.Now()

	go func() {
		_, ok := AcquireLockWithTimeoutX(key, time.Second*5, time.Second)
		assert.False(t, ok)

		diff := time.Now().Sub(tm)
		assert.GreaterOrEqual(t, int64(diff), int64(time.Second))
	}()

	localValue, ok = AcquireLockWithTimeoutX(key, time.Second*5, time.Second*2)
	assert.False(t, ok)

	diff := time.Now().Sub(tm)
	assert.GreaterOrEqual(t, int64(diff), int64(time.Second)*2)

	localValue, ok = AcquireLockWithTimeoutX(key, time.Second*5, time.Second*3+time.Millisecond*10)
	assert.True(t, ok)

	diff = time.Now().Sub(tm)
	assert.GreaterOrEqual(t, int64(diff), int64(time.Second)*5)
}

func TestAcquireLockWithTimeout(t *testing.T) {
	key := prefix + "TestAcquireLockWithTimeout" + "1"
	ok := AcquireLockWithTimeout(key, time.Second*5, time.Second)
	assert.True(t, ok)

	tm := time.Now()

	go func() {
		ok := AcquireLockWithTimeout(key, time.Second*5, time.Second)
		assert.False(t, ok)

		diff := time.Now().Sub(tm)
		assert.GreaterOrEqual(t, int64(diff), int64(time.Second))
	}()

	ok = AcquireLockWithTimeout(key, time.Second*5, time.Second*2)
	assert.False(t, ok)

	diff := time.Now().Sub(tm)
	assert.GreaterOrEqual(t, int64(diff), int64(time.Second)*2)

	ok = AcquireLockWithTimeout(key, time.Second*5, time.Second*3+time.Millisecond*10)
	assert.True(t, ok)

	diff = time.Now().Sub(tm)
	assert.GreaterOrEqual(t, int64(diff), int64(time.Second)*5)
}

func BenchmarkAcquireLockWithTimeoutX(b *testing.B) {
	b.ReportAllocs()

	b.Log("Call BenchmarkAcquireLockWithTimeoutX")
	for i := 0; i < b.N; i++ {
		uuid, _ := uuid2.GenerateUUID()
		//uuid := fmt.Sprintf("%d", rand.Int() % 100)
		key := prefix + "BenchmarkAcquireLockWithTimeoutX" + uuid
		_, ok := AcquireLockWithTimeoutX(key, time.Second*5, time.Second)

		//b.Log("--", i, ok)

		assert.True(b, ok)
	}
}

func BenchmarkUuid(b *testing.B) {
	b.ReportAllocs()

	b.Log("Call BenchmarkAcquireLockWithTimeoutX")
	for i := 0; i < b.N; i++ {
		uuid2.GenerateUUID()
	}
}
