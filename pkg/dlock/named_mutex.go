package dlock

import "sync"

type mutexEx struct {
	M sync.Mutex
	C int
}

type sNamedMutex struct {
	M    sync.Mutex
	Data map[string]*mutexEx
}

var namedMutex = &sNamedMutex{
	Data: make(map[string]*mutexEx),
}

// Benchmark: 单机mac 2.3 GHz 八核Intel Core i9 (200w+ ops)
func (p *sNamedMutex) Lock(s string) {
	var m *mutexEx

	p.M.Lock()

	m, ok := p.Data[s]
	if !ok {
		// default zero..
		m = &mutexEx{}
	}
	m.C++
	p.Data[s] = m

	p.M.Unlock()

	m.M.Lock()
}

func (p *sNamedMutex) Unlock(s string) {
	var m *mutexEx

	p.M.Lock()

	m, ok := p.Data[s]
	if ok {
		m.C--
		if m.C <= 0 {
			// never less than 0
			delete(p.Data, s)
		} else {
			p.Data[s] = m
		}
	}
	p.M.Unlock()

	if m != nil {
		m.M.Unlock()
	}
}
