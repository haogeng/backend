package dlock

import "time"

type DLockWrapper struct {
}

func (d *DLockWrapper) AcquireLockWithTimeoutX(key string, ttl time.Duration, timeOut time.Duration) (lockValue string, ok bool) {
	return AcquireLockWithTimeoutX(key, ttl, timeOut)
}
func (d *DLockWrapper) ReleaseLockX(key string, lockValue string) {
	ReleaseLockX(key, lockValue)
	return
}
