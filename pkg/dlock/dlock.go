// 分布式锁
package dlock

import (
	"context"
	"fmt"
	uuid2 "github.com/hashicorp/go-uuid"
	"time"
)

type KvStoreProvider interface {
	Del(keys ...string)
	// 原子释放， compare & del
	DelX(key string, value string) bool
	Get(key string) ([]byte, bool)
	Set(key string, val []byte, expiration time.Duration) bool
	SetNX(ctx context.Context, key string, value interface{}, expiration time.Duration) bool
}

var provider KvStoreProvider

func Init(_provider KvStoreProvider) {
	provider = _provider
}

// 尝试立即获取非等待锁
// timeout 自动释放时间
func AcquireLock(key string, ttl time.Duration) bool {
	if len(key) == 0 {
		return true
	}

	if len(key) > 4096 {
		panic("AcquireLock key too long")
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()

	ok := provider.SetNX(ctx, key, "1", ttl)
	return ok
}

func ReleaseLock(key string) {
	if len(key) == 0 {
		return
	}

	provider.Del(key)
}

// lockValue的作用：A，B都在获取锁；A got, 假设ttl过期后，逻辑还在执行，这时B也got锁了，A逻辑后defer里
// 释放锁，，就会把B的锁误释放了，用lockValue可以避免这种情况
// 可重入式分布式锁
func AcquireLockX(key string, ttl time.Duration) (lockValue string, ok bool) {
	if len(key) == 0 {
		return "", true
	}

	if len(key) > 4096 {
		panic("AcquireLockX key too long")
	}

	var err error
	lockValue, err = uuid2.GenerateUUID()
	if err != nil {
		panic(fmt.Errorf("AcquirelockX err: %v", err))
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()

	ok = provider.SetNX(ctx, key, lockValue, ttl)
	if ctx.Err() != nil {
		panic(ctx.Err())
	}

	return
}

// lockValue 确保释放的是上次获得的锁
func ReleaseLockX(key string, lockValue string) {
	if len(lockValue) > 0 {
		//v, ok := provider.Get(key)
		//if ok {
		//	// compare then set 有极小概率出问题，，细分析！
		//	// 在这一瞬间，碰巧本处理器锁的时间刚好到，而别的处理器又获得了新锁，，概率小到可以忽略
		//	// 原则上，应该使用原则的redis lua去释放锁
		//	if strings.Compare(lockValue, string(v)) == 0 {
		//		provider.Del(key)
		//	}
		//}
		provider.DelX(key, lockValue)
	} else {
		provider.Del(key)
	}
}

// 优化方案1：采用本地锁协助方式，加backoff延迟时间递进方式
// 通过锁，将本地机器的请求串行化，并发主要来自于每个分布式机器
// 特定时间内获得锁，最长等待timeOut (会有timeOut / 3的误差)
// Benchmark: 单机mac 2.3 GHz 八核Intel Core i9 (2w ops)
// TODO: 优化方案2：redis pub/sub + list + setnx, 暂时优先级不高
func AcquireLockWithTimeoutX(key string, ttl time.Duration, timeOut time.Duration) (lockValue string, ok bool) {
	// 只是优化用
	namedMutex.Lock(key)
	defer namedMutex.Unlock(key)

	if timeOut < time.Second {
		timeOut = time.Second
	}

	// 以key为主键，获得本地互斥量
	c := make(chan int)
	lockValue, ok = "", false

	retryTimeout := time.Duration(0)

	// 指数增长
	maxRetryInterval := timeOut / 3
	retryInterval := time.Millisecond * 5

	go func() {
		defer func() {
			close(c)
		}()

		for {
			//fmt.Printf("tryAcquireLockX %v\n", retryInterval)
			lockValue, ok = AcquireLockX(key, ttl)
			if ok {
				break
			} else {
				if retryTimeout >= timeOut {
					break
				}

				retryTimeout += retryInterval
				retryInterval = retryInterval * 2
				if retryInterval > maxRetryInterval {
					retryInterval = maxRetryInterval
				}

				time.Sleep(retryInterval)
			}
		}
	}()

	<-c
	return lockValue, ok
}

// TODO 优化
func AcquireLockWithTimeout(key string, ttl time.Duration, timeOut time.Duration) (ok bool) {
	c := make(chan int)
	ok = false

	retryTimeout := time.Duration(0)
	retryInterval := time.Millisecond * 100

	// TODO: 此处timeout应该有更优的写法
	go func() {
		defer func() {
			close(c)
		}()

		for {
			ok = AcquireLock(key, ttl)
			if ok {
				break
			} else {
				if retryTimeout > timeOut {
					break
				}
				retryTimeout += retryInterval
				time.Sleep(retryInterval)
			}
		}
	}()

	<-c
	return ok
}
