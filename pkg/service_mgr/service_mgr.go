package service_mgr

import (
	"context"
	"fmt"
	"go.etcd.io/etcd/v3/clientv3"
	"google.golang.org/grpc"
	"runtime"
	"server/internal/log"
	"strings"
	"time"
)

var cli *clientv3.Client

func Init(endpoints []string) (retCli *clientv3.Client, err error) {
	defer func() {
		if r := recover(); r != nil {
			stack := make([]byte, 512)
			length := runtime.Stack(stack, true)
			log.Infof("etcd fail: %+v stack:%s", r, string(stack[:length]))

			retCli = nil
			err = clientv3.ErrNoAvailableEndpoints
			return
		}
	}()
	retCli = MustInit(endpoints)
	return
}

// call in main & close by caller!
func MustInit(endpoints []string) *clientv3.Client {
	log.Infof("Start init etcd %s", strings.Join(endpoints, ";"))

	if len(endpoints) == 0 {
		panic("endpoints need config")
	}

	// 连接服务
	var err error
	cli, err = clientv3.New(clientv3.Config{
		Endpoints:   endpoints,
		DialTimeout: time.Second * 10,
		DialOptions: []grpc.DialOption{grpc.WithBlock()},
	})
	if err != nil {
		panic(err)
	}

	// 如果重用ctx，，则时间也是总时长
	// 另一个方案，没一个地方都搞一个ctx，这样就可以分开计时，稍微有些麻烦
	ctx, cancel := context.WithTimeout(context.Background(), 20*time.Second)
	defer cancel()

	statusResponse, err := cli.Status(ctx, endpoints[0])
	if err != nil {
		panic(err)
	}

	log.Infof("Etcd status: %v", statusResponse)

	return cli
}

func Login(key string, addr string) error {
	log.Infof("Service register  %s:%s\n", key, addr)
	if cli == nil {
		log.Errorf("Service register failed\n")
		return clientv3.ErrNoAvailableEndpoints
	}

	lease := clientv3.NewLease(cli)

	ctx, cancel := context.WithTimeout(context.Background(), 20*time.Second)
	defer cancel()
	resp, err := lease.Grant(ctx, 30)
	if err != nil {
		return err
	}

	// 保活，一直中
	// KeepAlive 不能传timeout类的context
	c, err := lease.KeepAlive(context.Background(), resp.ID)
	if err != nil {
		return err
	}

	// check keep alive response
	go func() {
	loop:
		for {
			select {
			case resp, ok := <-c:
				if !ok {
					log.Errorf("etcd service interrupt: %v", resp)
					break loop
				} else {
					_ = resp
					//log.Info(resp)
				}
			}
		}
	}()

	_, err = cli.Put(ctx, key, addr, clientv3.WithLease(resp.ID))
	if err != nil {
		return err
	}

	return nil
}

func Logout(key string) error {
	log.Info("service_mgr Logout")
	if cli != nil {
		ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
		defer cancel()
		_, err := cli.Delete(ctx, key)
		return err
	}
	return nil
}

func GetValues(rootKey string) (ret map[string]string, err error) {
	ret = make(map[string]string)

	if cli == nil {
		err = clientv3.ErrNoAvailableEndpoints
		return
	}

	if strings.LastIndex(rootKey, "/") != len(rootKey)-1 {
		rootKey += "/"
	}
	endKey := rootKey[:len(rootKey)-1] + "0"
	fmt.Printf("key: %s %s\n", rootKey, endKey)

	ctx, cancel := context.WithTimeout(context.Background(), 20*time.Second)
	defer cancel()
	resp, err := cli.Get(ctx, rootKey, clientv3.WithRange(endKey))
	if err == nil {
		for _, kv := range resp.Kvs {
			ret[string(kv.Key)] = string(kv.Value)
		}
	}

	return
}

func Close() error {
	if cli != nil {
		return cli.Close()
	}
	return nil
}
