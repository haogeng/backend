package xwatcher

import (
	"fmt"
	. "github.com/smartystreets/goconvey/convey"
	"testing"
	"time"
)

type logger struct {
}

func (l *logger) Printf(format string, v ...interface{}) {
	fmt.Printf(format, v...)
}

type testProcessor struct {
}

func (tp *testProcessor) OnDataLoaded(key string, base string, data []byte) {
	fmt.Printf("\nreload from key:%s, base:%s:\n%s\n", key, base, string(data))
}

var testWhich string = "" // file or etcd

func TestFileWatch(t *testing.T) {
	if testWhich != "file" {
		return
	}

	Convey("", t, func() {
		w := MustNewFileWatcher(&logger{})
		var err error
		//err = w.Register("/Users/Sprite/test/watcher/a", &testProcessor{})
		//So(err, ShouldBeNil)

		err = w.Register("/Users/Sprite/test/watcher/b", &testProcessor{})
		So(err, ShouldBeNil)

		err = w.ForceReloadAll()
		t.Log(err)

		time.Sleep(time.Hour)
	})

}

func TestEtcdWatch(t *testing.T) {
	if testWhich != "etcd" {
		return
	}
	Convey("", t, func() {
		w := MustNewEtcdWatcher([]string{"http://localhost:2379"}, &logger{})
		var err error
		//err = w.Register("/Users/Sprite/test/watcher/a", &testProcessor{})
		//So(err, ShouldBeNil)

		err = w.Register("/b", &testProcessor{})
		So(err, ShouldBeNil)

		err = w.ForceReloadAll()
		t.Log(err)

		time.Sleep(time.Hour)
	})
}
