package xwatcher

import (
	"fmt"
	"github.com/fsnotify/fsnotify"
	"io/ioutil"
	"os"
	"path/filepath"
	"time"
)

// 嵌套目录的频繁创建删除，会导致偶尔的阻塞，当删除的文件再次创建时，
// 会再次正确运行
func waitUntilFind(filename string) error {
	for {
		time.Sleep(1 * time.Second)
		_, err := os.Stat(filename)
		fmt.Printf("Wait %s\n", filename)
		if err != nil {
			if os.IsNotExist(err) {
				continue
			} else {
				return err
			}
		}
		break
	}
	return nil
}

const (
	// rename or remove 排队watch的buffer大小
	KBufferSize = 256
)

type fileLoader struct {
	w *fsnotify.Watcher
	Logger
	processor watchEventProcessor

	toBeWatchedQueue chan string
}

func newFileLoader(l Logger, p watchEventProcessor) *fileLoader {
	fl := &fileLoader{
		Logger:           l,
		processor:        p,
		toBeWatchedQueue: make(chan string, KBufferSize),
	}
	err := fl.init()
	if err != nil {
		panic(err)
	}
	fl.goScanAutoAdd()

	return fl
}

// 备注：
/*
1. AddWatch 时机无所谓，可以在启动watch之后
2. 事件是枚举值，不要简单的等待
3. 不要想当然的理解create，类似的创建可能是rename形式。另外修改，也有可能是以chmod方式，而不是write，诡异
4. 诡异的事情，实际是操作系统的实现方式不一致
*/
func (l *fileLoader) init() error {
	if l.w != nil {
		return fmt.Errorf("has inited")
	}

	var err error
	l.w, err = fsnotify.NewWatcher()
	if err != nil {
		return err
	}

	go func() {
		defer l.w.Close()

		errCh := make(chan error)

		go func(watcher *fsnotify.Watcher) {
			for {
				select {
				case event := <-watcher.Events:
					l.Logger.Printf("event:%v\n", event)

					// 冗余处理，上层需要是幂等函数 chmod可能和write一起出现
					if event.Op&fsnotify.Write == fsnotify.Write ||
						event.Op&fsnotify.Create == fsnotify.Create ||
						event.Op&fsnotify.Chmod == fsnotify.Chmod {
						l.onChanged(event)
					}

					// create -> remove 需要顺序执行
					if event.Op&fsnotify.Rename == fsnotify.Rename ||
						event.Op&fsnotify.Remove == fsnotify.Remove {
						_ = l.AddWatch(event.Name)
					}

				case err = <-watcher.Errors:
					errCh <- err
				}
			}
		}(l.w)

		// When err, then trigger close.
		err := <-errCh
		if err != nil {
			l.Logger.Printf("errCh: %v, watcher will close.\n", err)
		}
	}()

	return nil
}

func (l *fileLoader) onChanged(event fsnotify.Event) {
	stat, err := os.Stat(event.Name)
	if err != nil {
		return
	}

	if stat.IsDir() {
		if event.Op&fsnotify.Create == fsnotify.Create {
			_ = l.AddWatch(event.Name)
		}
		l.processor.OnWatchEvent(event.Name, nil)
	} else {
		if b, err := l.Read(event.Name); err == nil {
			l.processor.OnWatchEvent(event.Name, b)
		} else {
			l.Logger.Printf("read %s err\n", event.Name)
		}
	}
}

// Dont call AddWatch
func (l *fileLoader) goScanAutoAdd() {
	go func() {
		for {
			var err error
			select {
			case filename := <-l.toBeWatchedQueue:
				err = waitUntilFind(filename)
				if err == nil {
					err = l.doAddWatch(filename)
				}
			}
			if err != nil {
				l.Logger.Printf("rename or remove err %v\n", err)
				break
			}
		}
	}()
}

func (l *fileLoader) AddWatch(name string) error {
	l.Logger.Printf("WillAddWatch %s \n", name)

	l.toBeWatchedQueue <- name
	return nil
}

func (l *fileLoader) doAddWatch(name string) error {
	l.Logger.Printf("doAddWatch %s\n", name)
	err := l.w.Add(name)
	return err
}

func (l *fileLoader) Read(name string) (data []byte, err error) {
	data, err = ioutil.ReadFile(name)
	return
}

// 读取文件或目录当前所有信息
func (l *fileLoader) ForceReload(watchKey string) error {
	l.Logger.Printf("forceReload %s\n", watchKey)

	// 如果是目录，则遍历目录所有文件，推送给上层
	stat, err := os.Stat(watchKey)
	if err != nil {
		return err
	}

	if stat.IsDir() {
		// It walks a file tree calling a function of type filepath.WalkFunc for each file or directory in the tree, including the root.
		// The files are walked in lexical order.
		// Symbolic links are not followed.
		err := filepath.Walk(watchKey, func(path string, info os.FileInfo, err error) error {
			if err != nil {
				return err
			}

			if info.IsDir() {
				// do nth.
			} else {
				l.ForceReload(path)
			}
			return nil
		})
		return err
	} else {
		data, err := l.Read(watchKey)
		if err == nil {
			l.processor.OnWatchEvent(watchKey, data)
		}

		return err
	}
}
