package xwatcher

import (
	"context"
	"errors"
	"fmt"
	"go.etcd.io/etcd/v3/clientv3"
	"google.golang.org/grpc"
	"time"
)

type etcdLoader struct {
	cli *clientv3.Client
	Logger
	processor watchEventProcessor
}

func newEtcdLoader(endPoints []string, l Logger, p watchEventProcessor) *etcdLoader {
	fl := &etcdLoader{
		Logger:    l,
		processor: p,
	}
	err := fl.init(endPoints)
	if err != nil {
		panic(err)
	}
	return fl
}

func (l *etcdLoader) getEtcdClient(hosts []string) (*clientv3.Client, error) {
	if len(hosts) > 0 {
		cfg := clientv3.Config{
			Endpoints: hosts,
			// set timeout per request to fail fast when the target endpoint is unavailable
			DialTimeout: 5 * time.Second,
			DialOptions: []grpc.DialOption{grpc.WithBlock()},
		}
		cli, err := clientv3.New(cfg)
		if err == nil {
			return cli, nil
		} else {
			panic(err)
		}
	}
	return nil, errors.New("etcd conf err")
}

func (l *etcdLoader) init(endPoints []string) error {
	if l.cli != nil {
		return fmt.Errorf("has inited")
	}

	var err error
	l.cli, err = l.getEtcdClient(endPoints)
	return err
}

func (l *etcdLoader) AddWatch(name string) error {
	// 无限检测，不加超时
	//ctx, _ := context.WithTimeout(context.Background(), time.Second*5)
	ctx := context.Background()
	watchChan := l.cli.Watch(ctx, name, clientv3.WithPrefix())
	//l.Logger.Printf("etcd add watch: %v\n", name)
	go func() {
		//l.Logger.Printf("start process watchChan\n")
		for w := range watchChan {
			//l.Logger.Printf("etcd events: %v\n", w.Events)
			for _, e := range w.Events {
				l.processor.OnWatchEvent(string(e.Kv.Key), e.Kv.Value)
			}
		}
		//l.Logger.Printf("end process watchChan\n")
	}()
	return nil
}

// 读取单个节点内容
func (l *etcdLoader) Read(name string) (data []byte, err error) {
	ctx, _ := context.WithTimeout(context.Background(), time.Second*5)
	res, err := l.cli.Get(ctx, name)
	if err != nil {
		return
	}
	data = res.Kvs[0].Value
	return
}

// 读取文件或目录当前所有信息
func (l *etcdLoader) ForceReload(watchKey string) error {
	// 如果是目录，则遍历目录所有文件，推送给上层
	ctx, _ := context.WithTimeout(context.Background(), time.Second*5)
	res, err := l.cli.Get(ctx, watchKey)
	if err != nil {
		return err
	}

	// 目录下的所有文件
	for i := 0; i < len(res.Kvs); i++ {
		l.processor.OnWatchEvent(string(res.Kvs[i].Key), res.Kvs[i].Value)
	}
	return nil
}
