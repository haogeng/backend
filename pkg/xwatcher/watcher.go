package xwatcher

import (
	"errors"
	"fmt"
	"path/filepath"
	"strings"
	"sync"
)

// 检测目录或etcd里的特定目录或特定文件
// 如果是etcd，初始化时etcd必须在，运行的时候etcd重启能平滑处理(依赖于etcd api自身)
// etcd会监听with prefix
// 建议使用时候一个文件一个监听，最安全，无论是file还是etcd，文件的删除都可以正确处理

// 支持文件目录以及单个文件的监听
type Processor interface {
	// dont care error
	// 需要是幂等函数，可重入
	// 因为兼容swp，文件的一次修改，可能会导致该函数多次调用
	// watchKey 是watch的键值，而baseName是具体的文件名，data是读取到的数据，如果是remove等事件，data可能是空
	OnDataLoaded(watchKey string, baseName string, data []byte)
}

type SourceLoader interface {
	// read data from fullName(file or etcd...)
	Read(fullName string) (data []byte, err error)
	// watch key. when data changed, it will reload and trigger Processor.OnDataLoaded
	AddWatch(watchKey string) error

	// 读取文件或目录当前所有信息
	ForceReload(watchKey string) error
}

type Logger interface {
	Printf(format string, v ...interface{})
}

type watchEventProcessor interface {
	OnWatchEvent(watchKey string, data []byte)
}

type watchUnit struct {
	key string
	Processor
}

type Watcher struct {
	m sync.RWMutex
	// key -> unit
	units  map[string]*watchUnit
	loader SourceLoader
	Logger
}

func MustNewFileWatcher(logger Logger) *Watcher {
	w := &Watcher{
		units:  make(map[string]*watchUnit),
		Logger: logger,
	}

	w.loader = newFileLoader(logger, w)

	return w
}

func MustNewEtcdWatcher(endPoints []string, logger Logger) *Watcher {
	w := &Watcher{
		units:  make(map[string]*watchUnit),
		Logger: logger,
	}

	w.loader = newEtcdLoader(endPoints, logger, w)

	return w
}

// fullname -> loader
// watchKey 可以是目录或文件，如果是目录则监听整个目录
func (w *Watcher) Register(watchKey string, r Processor) error {
	w.m.Lock()
	defer w.m.Unlock()

	u := &watchUnit{
		key:       watchKey,
		Processor: r,
	}
	if _, ok := w.units[watchKey]; ok {
		return errors.New("repeat register " + watchKey)
	}
	w.units[watchKey] = u

	// no conflict with lock?
	tempErr := w.loader.AddWatch(u.key)
	if tempErr != nil {
		w.Logger.Printf("Watch %s err %v\n", u.key, tempErr)
	}
	return nil
}

func (w *Watcher) ForceReload(key string) error {
	w.m.RLock()
	defer w.m.RUnlock()

	return w.doForceReload(key)
}

func (w *Watcher) doForceReload(key string) error {
	if _, ok := w.units[key]; !ok {
		return fmt.Errorf("not found %s", key)
	}

	return w.loader.ForceReload(key)
}

func (w *Watcher) ForceReloadAll() error {
	w.m.RLock()
	defer w.m.RUnlock()

	errStr := ""
	for _, u := range w.units {
		err := w.doForceReload(u.key)
		if err != nil {
			errStr += err.Error()
		}
		// continue...
	}
	if len(errStr) > 0 {
		return errors.New(errStr)
	}
	return nil
}

func (w *Watcher) OnWatchEvent(key string, data []byte) {
	w.m.RLock()
	u, ok := w.units[key]
	if !ok {
		// find a sub key
		for k, v := range w.units {
			if strings.Contains(key, k) {
				u = v
				break
			}
		}
	}
	w.m.RUnlock()

	w.Printf("OnWatchEvent %s\n", key)

	// 如果是目录，(path, baseName, data)
	if u != nil {
		base := filepath.Base(key)
		// 极端 base名会重复
		u.Processor.OnDataLoaded(u.key, base, data)
	}
}
