package xsnowflake

import (
	"fmt"
	"math/rand"
	"sync"
	"testing"
	"time"
)

func TestSnowFlake_Node(t *testing.T) {
	m := make(map[uint16][]byte)
	for i := 0; i < 256; i++ {
		for j := 0; j < 64; j++ {
			ip := []byte{0, 0, byte(j), byte(i)}
			nodeId, err := NodeIdFromLowerNodeBitsOfPrivateIPv4(ip)

			if err != nil {
				t.Errorf(err.Error())
			}

			if d, ok := m[nodeId]; ok {
				t.Errorf("Conflicts %d:%s with %s", nodeId, string(d), string(ip))
			} else {
				m[nodeId] = ip
			}
		}
	}
}

func BenchmarkGenNode(b *testing.B) {
	b.ReportAllocs()
	b.ResetTimer()

	n := MustInit(0, nil)
	_ = n

	//m := make(map[uint64]bool)
	for i := 0; i < b.N; i++ {
		id := NextIdUint64()
		_ = id
		//if _, ok := m[id]; ok {
		//	panic(0)
		//} else {
		//	m[id] = true
		//}
	}
}

func TestXSnowFlake(t *testing.T) {
	//var waitgroup sync.WatiGroup
	var syncMap sync.Map
	var syncMap2 sync.Map
	defer func() {
		count := 0
		f := func(k, v interface{}) bool {
			//fmt.Println(k, v)
			count = count + 1
			return true
		}

		syncMap.Range(f)
		fmt.Println(count)

		f2 := func(k, v interface{}) bool {
			fmt.Println("node is:", k, "has id count:", v)
			//_map[k] = v
			return true
		}
		syncMap2.Range(f2)
	}()

	bt := time.Now()
	var et time.Duration

	curTime := time.Now().UnixNano()
	rand.Seed(curTime)
	nodeCount := 4

	dbCount := 10
	for i := 0; i < nodeCount; i++ {
		tempNodeId := uint16(i + 1)

		go func() {
			//nodeHasIdCount := 0
			curNode := MustInit(tempNodeId, nil)

			for {
				_id := curNode.Generate()

				v, ok := syncMap.Load(_id)
				if ok {
					panic(fmt.Errorf("map1 id duplicated:v:%v,id:%v\n", v, _id))
				}

				toDbId := uint64(_id) % uint64(dbCount)
				toDbCount := int(1)
				v2, ok2 := syncMap2.Load(toDbId)
				if ok2 {
					toDbCount = v2.(int)
					toDbCount++
				}

				//nodeHasIdCount++
				syncMap.Store(_id, toDbId)
				syncMap2.Store(toDbId, toDbCount)
			}
		}()
	}

	select {
	case <-time.After(time.Duration(1) * time.Second):
		fmt.Println("time out")
	}
	et = time.Since(bt)
	fmt.Printf("runtime is:%v\n", et)
}
