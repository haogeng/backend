package version

import "fmt"

// 用于build的时候注入信息
var (
	Version   string
	Branch    string
	BuildDate string

	SvnVersion string
	SvnUrl     string
)

func Info() string {
	return fmt.Sprintf(`

----------------------------------
[Build info] 
Git-Version: 	%s
Git-Branch: 	%s
Git-BuildDate: 	%s
Svn-Version: 	%s
Svn-Url:		%s
----------------------------------

`,
		Version,
		Branch,
		BuildDate,
		SvnVersion,
		SvnUrl)
}
