package main

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"os"
	"strings"
)

func ReadSqlVersion(file string) string {
	f, _ := os.Open(file)
	defer f.Close()
	buf := bufio.NewReader(f)
	var lastVersion string = ""
	for {
		line, err := buf.ReadString('\n')
		lastVersion = line
		if err != nil || lastVersion != "" {
			break
		}
	}
	lastVersion = strings.Replace(lastVersion, "\n", "", -1)
	return lastVersion
}

func WriteFile(file string, value string) {
	content := []byte(value)
	err := ioutil.WriteFile(file, content, 0666)
	if err != nil {
		fmt.Println("WriteFile error: ", err)
	}
}

func ReadSqlLog(fileName string, lastVersion string, dataBaseName string, gmxConfigPostfix string) (nextVersion string, execSql string) {
	f, _ := os.Open(fileName)
	defer f.Close()
	buf := bufio.NewReader(f)
	// 最终执行
	var results []string
	// 临时保存
	var tmpResult []string
	// 临时版本号
	var tmpVersion string
	// 初始化数据库
	databaseName := dataBaseName + "_" + gmxConfigPostfix
	if lastVersion == "" {
		// 创建数据库
		results = append(results, "CREATE DATABASE "+databaseName+" DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;\n")
	}
	// 进入数据库
	results = append(results, "use "+databaseName+";\n")
	for {
		line, err := buf.ReadString('\n')
		if err != nil {
			// 判断是否达到上次指定位置
			if tmpVersion != "" {
				results = append(results, tmpResult...)
				tmpResult = tmpResult[0:0]
			} else {
				tmpVersion = lastVersion
			}
			break
		}
		if line == "\n" {
			continue
		}
		// 解析是否为注释行
		index := strings.Index(line, "--")
		if index == -1 {
			// 内容行
			tmpResult = append(tmpResult, line)
			continue
		}
		tmpStr := "-- Running upgrade"
		// 注释行
		index = strings.Index(line, tmpStr)
		if index == -1 {
			// 标准注释
			continue
		}
		// 版本注释
		tmpBetween := strings.Index(line, "->")
		beginVersion := strings.Replace(line[len(tmpStr):tmpBetween], " ", "", -1)
		endVersion := strings.Replace(line[tmpBetween+len("->"):len(line)], " ", "", -1)
		endVersion = strings.Replace(endVersion, "\n", "", -1)
		if lastVersion == "" {
			// 上次版本为空,全部执行
			results = append(results, tmpResult...)
			tmpResult = tmpResult[0:0]
			tmpVersion = endVersion
			continue
		}
		if lastVersion != beginVersion && tmpVersion == "" {
			// 未到达上次执行位置
			tmpResult = tmpResult[0:0]
			continue
		}
		// 到达上次执行位置
		if tmpVersion == "" {
			// 见怪不怪
			tmpResult = tmpResult[0:0]
		} else {
			results = append(results, tmpResult...)
			tmpResult = tmpResult[0:0]
		}
		// 更新版本号
		tmpVersion = endVersion
	}
	//fmt.Printf("最新版本号:%s\n", tmpVersion)
	//fmt.Println(results)
	var temSql strings.Builder
	for _, v := range results {
		temSql.WriteString(v)
	}
	nextVersion = tmpVersion
	execSql = temSql.String()
	return
}

func main() {
	var fileUrl = os.Args[1] + "sqlversion.txt"
	var dataBaseName = os.Args[2]
	var gmxConfigPostfix = os.Args[3]
	var lastVersion = ReadSqlVersion(fileUrl)
	nextVersion, execSql := ReadSqlLog("./init/sqllog-dev-develop", lastVersion, dataBaseName, gmxConfigPostfix)
	// 写入版本
	WriteFile(fileUrl, nextVersion)
	// 写入SQL
	WriteFile("./init/execSql.sql", execSql)
}
