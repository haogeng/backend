module bitbucket.org/haogeng/backend

go 1.13

require (
	bitbucket.org/funplus/ark v0.0.1
	bitbucket.org/funplus/arkmid v0.0.2
	bitbucket.org/funplus/golib v0.2.4
	bitbucket.org/funplus/sandwich v0.0.6
	github.com/Masterminds/semver v1.5.0
	github.com/ThinkingDataAnalytics/go-sdk v1.2.0
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/fsnotify/fsnotify v1.4.9
	github.com/gin-gonic/gin v1.4.0
	github.com/go-redis/redis/v8 v8.2.2
	github.com/go-sql-driver/mysql v1.4.1
	github.com/gogo/protobuf v1.3.1
	github.com/golang/protobuf v1.4.2
	github.com/google/uuid v1.1.2
	github.com/hashicorp/go-uuid v1.0.1
	github.com/highras/rtm-server-sdk-go v0.3.1
	github.com/jmoiron/sqlx v1.2.1-0.20190426154859-38398a30ed85
	github.com/json-iterator/go v1.1.10
	github.com/patrickmn/go-cache v2.1.0+incompatible
	github.com/smartystreets/goconvey v1.6.4
	github.com/stretchr/testify v1.6.1
	github.com/vmihailenco/msgpack/v4 v4.3.11
	github.com/zero-yy/sensitive v0.0.0-20200106142752-42d1c505be7b
	github.com/zero-yy/viper v1.7.2-0.20200925055616-009ce409fdc5
	go.etcd.io/etcd/v3 v3.3.0-rc.0.0.20200720071305-89da79188f73
	go.uber.org/atomic v1.6.0
	go.uber.org/zap v1.16.0
	google.golang.org/grpc v1.32.0
	gopkg.in/errgo.v2 v2.1.0
	gotest.tools v2.2.0+incompatible
)

replace google.golang.org/grpc => google.golang.org/grpc v1.26.0

replace git.apache.org/thrift.git => github.com/apache/thrift v0.0.0-20180902110319-2566ecd5d999

replace github.com/coreos/bbolt => go.etcd.io/bbolt v1.3.5

replace github.com/coreos/go-systemd => github.com/coreos/go-systemd/v22 v22.1.0
