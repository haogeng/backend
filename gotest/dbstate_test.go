package gotest

import (
	"fmt"
	"server/pkg/gen/msg"
	_ "server/pkg/gen/msg"
	"server/pkg/gen_db"
	"server/pkg/gen_db/dao"
	"testing"

	_ "github.com/stretchr/testify/assert"
)

func TestDbState(t *testing.T) {
	var c gen_db.Conf
	c.MysqlConf.MysqlPass = "3cu8rt"
	c.MysqlConf.MysqlDbName = "gmx_server_parent"
	c.MysqlConf.MysqlUser = "root"
	c.MysqlConf.MysqlPort = 3306
	c.MysqlConf.MysqlHost = "127.0.0.1"

	// Remember: MustCreate table using SQL.

	//fmt.Println(c.String())

	_, err := gen_db.ConnectWithConf(c)
	if err != nil {
		fmt.Errorf("con err %v", err)
	}

	testMgr := dao.TestState

	p := &msg.TestState{}

	KNum := int32(6)

	for i := int32(1); i < KNum; i++ {
		testMgr.Delete(uint64(i))
	}

	for i := int32(1); i < KNum; i++ {
		p.Gold = i * 1000
		p.Alias = fmt.Sprintf("name_%d", i)
		testMgr.Create(uint64(i), p)
	}

	for i := int32(1); i < KNum; i++ {
		p, err := testMgr.Find(uint64(i))
		if err != nil {
			fmt.Errorf("find err %v", err)
		} else {
			fmt.Printf("%v\n", p)
		}

		//assert.Equal(t, fmt.Sprintf("name_%d", i), p.Alias, "Name should equal")
	}
}
