#!/usr/bin/env bash

source "$(dirname "${0}")/shlib.sh"
echo "==> ${term_yellow}Cleaning... ${term_reset}"

SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ] ; do SOURCE="$(readlink "$SOURCE")"; done

DIR="$( cd -P "$( dirname "$SOURCE" )/../cmd" && pwd )"

# Change into that directory
cd "$DIR"
did=false
for F in $(find ${DIR} -mindepth 1 -maxdepth 1 -type d); do
    rmdir="$F"/bin
    if [ -d "$rmdir" ]; then
        echo "==> Removing directory: $rmdir"
        rm -rf "$rmdir"
        did=true
    fi
done