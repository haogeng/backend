#!/usr/bin/env bash
# author:nanjun.li

ZIP_NAME="gmx_server_pro"
BASE_URL="/home/dev/gmx/"
TMP_URL="tmp/deploy/"
ZIP_URL=${BASE_URL}${ZIP_NAME}.zip
WORK_SPACE=${BASE_URL}${TMP_URL}
ABS_INSTALL_TO=${WORK_SPACE}${ZIP_NAME}

rm -rf "${ABS_INSTALL_TO}"
mkdir -p "${ABS_INSTALL_TO}"
mkdir -p "${BASE_URL}back/"

# 根据系统时间做版本备份
time=$(date "+%Y%m%d%H%M%S")
sudo cp "${ZIP_URL}" "${BASE_URL}back/${ZIP_NAME}.zip_${time}"
sudo rm -rf "${ZIP_URL}"
curl -O https://makerx-static.akamaized.net/docker/"${ZIP_NAME}.zip"
unzip "${ZIP_URL}" -d "${ABS_INSTALL_TO}"

cd "${ABS_INSTALL_TO}"
chmod -R 775 ./scripts/pro/docker-start-stack-pro.sh
./scripts/pro/docker-start-stack-pro.sh