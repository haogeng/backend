#!/usr/bin/env bash
# author:nanjun.li

SERVER_IP="10.0.84.38"
SERVER_NAME="gmx_server_x"
ZIP_NAME="gmx_server_`whoami`"
DATABASE_BASE="gmx_server"
DATABASE_USER="root"
DATABASE_PASS="3cu8rt"
BASE_URL="/data/usr/`whoami`/"
TMP_URL="tmp/deploy/"
WORK_SPACE=${BASE_URL}${TMP_URL}
ABS_INSTALL_TO=${WORK_SPACE}${ZIP_NAME}

if [ `uname -s` == "Darwin" ];then
  output="${SERVER_NAME}_darwin"
  echo "检查Docker......"
  docker -v
  if [ $? -eq  0 ]; then
    echo "检查到Docker已安装!"
  else
    echo "安装docker环境..."
    brew cask install docker
    echo "安装docker环境...安装完成!"
  fi
    echo "mac start docker ......"
    open /Applications/Docker.app
else
  output="${SERVER_NAME}"
  echo "检查Docker......"
  docker -v
  if [ $? -eq  0 ]; then
    echo "检查到Docker已安装!"
  else
    echo "安装docker环境..."
    sudo -s
    curl -sSL https://get.daocloud.io/docker | sh
    echo "安装docker环境...安装完成!"
  fi
    echo "linux start docker ......"
    sudo systemctl start docker
fi

mkdir -p ${TMP_URL}
# sudo rm -rf "${WORK_SPACE}${ZIP_NAME}"
# 根据系统时间做版本备份
time=$(date "+%Y%m%d%H%M%S")
sudo cp "${BASE_URL}${ZIP_NAME}.zip" "${BASE_URL}${ZIP_NAME}.zip_${time}"
sudo rm -rf "${BASE_URL}${ZIP_NAME}.zip"
cp "/data/usr/workspace/${ZIP_NAME}.zip" "${BASE_URL}"
unzip "${BASE_URL}${ZIP_NAME}.zip" -d "${WORK_SPACE}${ZIP_NAME}"
cd "${ABS_INSTALL_TO}"

# 执行sql语句
go run ./build-sql.go "${BASE_URL}" "${DATABASE_BASE}" "${GMX_CONFIG_POSTFIX}"
mysql -h ${SERVER_IP} -u${DATABASE_USER} -p${DATABASE_PASS} < ./init/execSql.sql

echo "FROM alpine:latest
ENV GMX_CONFIG_POSTFIX=${GMX_CONFIG_POSTFIX}
ENV GMX_ENV_HTTP_PORT=${GMX_ENV_HTTP_PORT}
WORKDIR /${SERVER_NAME}
COPY ./ /${SERVER_NAME}
EXPOSE ${GMX_ENV_HTTP_PORT}
ENTRYPOINT [\"./${output}\"]" > "$ABS_INSTALL_TO"/Dockerfile

# 删除容器
#sudo docker stop "${ZIP_NAME}"
#sudo docker rm "${ZIP_NAME}"
#sudo docker rmi "${ZIP_NAME}"
sudo docker rm -f "${ZIP_NAME}"

# 生成容器
sudo docker build -t "${ZIP_NAME}" .

# 导出容器
# docker save "${DIR_INSTALL}"_"${ENV_NAME}" > "${DIR_INSTALL}"_"${ENV_NAME}".tar

# 从指定链接下载容器文件
# wget https://makerx-static.akamaized.net/docker/$1.tar

# 导入容器
# sudo docker load -i $1.tar

# rm -rf $1.tar

# 单节点服务
sudo docker run --name "$ZIP_NAME" --net=host -it -d -p "${GMX_ENV_HTTP_PORT}":"${GMX_ENV_HTTP_PORT}" -v ${ABS_INSTALL_TO}:/${SERVER_NAME} -v /etc/localtime:/etc/localtime -v /etc/timezone:/etc/timezone "${ZIP_NAME}"

# 集群服务
# cd ${BASE_URL}
# sudo docker stack deploy -c ./docker-compose.yml "${ZIP_NAME}"

# 查看集群状态
# sudo docker stack ps "${ZIP_NAME}"