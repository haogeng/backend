#!/usr/bin/env bash
# author:nanjun.li

echo "DOCKER_START_SERVER=$1"

if [ -z "$1" ]; then
    echo "DOCKER_START_SERVER is empty"
    exit
fi

if [ `uname -s` == "Darwin" ];then
  echo "检查Docker......"
  docker -v
  if [ $? -eq  0 ]; then
    echo "检查到Docker已安装!"
  else
    echo "安装docker环境..."
    brew cask install docker
    echo "安装docker环境...安装完成!"
  fi
    echo "mac start docker ......"
    open /Applications/Docker.app
else
  echo "检查Docker......"
  docker -v
  if [ $? -eq  0 ]; then
    echo "检查到Docker已安装!"
  else
    echo "安装docker环境..."
    curl -sSL https://get.daocloud.io/docker | sh
    echo "安装docker环境...安装完成!"
  fi
    echo "linux start docker ......"
    systemctl start docker
fi

# 从指定链接下载容器文件
wget https://makerx-static.akamaized.net/docker/$1.tar

# 移除旧容器
docker stop $1
docker rm $1
docker rmi $1

# 导入容器
docker load -i $1.tar

rm -rf $1.tar

# 单节点服务
# docker run --name $1 -it -d -p 80:28086 -v /$1log:/log $1
# 集群服务
docker stack deploy -c docker-compose.yml $1

# 查看集群状态
docker stack ps $1