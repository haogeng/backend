#!/usr/bin/env bash
# author:nanjun.li

SERVER_NAME="gmx_server_x"
ZIP_NAME="gmx_server_pro"

BASE_URL="/home/dev/gmx/"
TMP_URL="tmp/deploy/"
WORK_SPACE=${BASE_URL}${TMP_URL}
ABS_INSTALL_TO=${WORK_SPACE}${ZIP_NAME}

if [ `uname -s` == "Darwin" ];then
  output="${SERVER_NAME}_darwin"
  echo "检查Docker......"
  docker -v
  if [ $? -eq  0 ]; then
    echo "检查到Docker已安装!"
  else
    echo "安装docker环境..."
    brew cask install docker
    echo "安装docker环境...安装完成!"
  fi
    echo "mac start docker ......"
    open /Applications/Docker.app
else
  output="${SERVER_NAME}"
  echo "检查Docker......"
  docker -v
  if [ $? -eq  0 ]; then
    echo "检查到Docker已安装!"
  else
    echo "安装docker环境..."
    sudo -s
    curl -sSL https://get.daocloud.io/docker | sh
    echo "安装docker环境...安装完成!"
  fi
    echo "linux start docker ......"
    sudo systemctl start docker
fi

echo "FROM alpine:latest
ENV GMX_CONFIG_POSTFIX=${GMX_CONFIG_POSTFIX}
ENV GMX_ENV_HTTP_PORT=${GMX_ENV_HTTP_PORT}
WORKDIR /${SERVER_NAME}
COPY ./ /${SERVER_NAME}
EXPOSE ${GMX_ENV_HTTP_PORT}
ENTRYPOINT [\"./${output}\"]" > "$ABS_INSTALL_TO"/Dockerfile

# 删除集群
# sudo docker stack rm "${ZIP_NAME}"
sudo docker service rm "${ZIP_NAME}_web"
# 删除容器
sudo docker rm -f "${ZIP_NAME}"
# 延迟3秒执行
sleep 3s
# 生成容器
sudo docker build -t "${ZIP_NAME}" .

# 单节点服务
sudo docker run --name "$ZIP_NAME" --net=host \
 -it -d -p "${GMX_ENV_HTTP_PORT}":"${GMX_ENV_HTTP_PORT}" \
 -v "${ABS_INSTALL_TO}":/"${SERVER_NAME}" \
 -v /etc/localtime:/etc/localtime \
 -v /etc/timezone:/etc/timezone \
 "${ZIP_NAME}"