#!/usr/bin/env bash
# author:nanjun.li

server_ip="10.0.84.38"
des_user="dev"
des_pass="dev!23"
workspace="/tmp/deploy/"

DIR_INSTALL=$ROOT_NAME
ABS_INSTALL_TO=${workspace}${DIR_INSTALL}
ZIP_NAME="${SERVER_NAME}_${USER_NAME}.zip"

# Get the parent directory of where this script is.
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ] ; do SOURCE="$(readlink "$SOURCE")"; done
DIR_CURR="$( cd -P "$( dirname "$SOURCE" )/" && pwd )"
DIR_ROOT="$( cd -P "${DIR_CURR}/../" && pwd )"

# Copy our depends to the bin/ directory
dirs=("scripts/${USER_NAME}")
cd ${DIR_ROOT}
for dir in ${dirs[@]};do
    if [ -d ${dir} ]; then
        dst="${ABS_INSTALL_TO}/${dir}"
        mkdir -p "${dst}"
        echo "==> Copy dir ${dir} to ${dst}/"
        cp -rf ${dir}/* ${dst}
    fi
done

cd "${ABS_INSTALL_TO}"
# zip
rm -rf ./"${SERVER_NAME}_*.zip"

# 将文件移动到上一级
mv ./init/build-sql.go ./
# 压缩
zip -r "${ZIP_NAME}" ./*

# 创建文件夹
sshpass -p $des_pass ssh ${des_user}@${server_ip} mkdir -p /data/usr/workspace/
# 上传
sshpass -p $des_pass scp "${ZIP_NAME}" ${des_user}@${server_ip}:/data/usr/workspace