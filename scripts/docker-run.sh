#!/usr/bin/env bash
# author:nanjun.li

ZIP_NAME="gmx_server_`whoami`"
BASE_URL="/data/usr/`whoami`/"
TMP_URL="tmp/deploy/"
ZIP_URL="${BASE_URL}${ZIP_NAME}.zip"
WORK_SPACE=${BASE_URL}${TMP_URL}
ABS_INSTALL_TO=${WORK_SPACE}${ZIP_NAME}

cd /data/gmx_dev/trunk/backend
make ${GMX_CONFIG_POSTFIX}deploy

sudo chown -R dev:dev /tmp/deploy
sudo chown -R dev:dev /data/usr/dev/go/pkg/mod
sudo chmod -R 775 /tmp/deploy
sudo chmod -R 775 /data/usr/dev/go/pkg/mod

cd /data/usr/${whoami}
rm -rf ${ABS_INSTALL_TO}
mkdir -p ${ABS_INSTALL_TO}
mkdir -p "${BASE_URL}back/"

# 根据系统时间做版本备份
time=$(date "+%Y%m%d%H%M%S")
sudo cp "${ZIP_URL}" "${BASE_URL}back/${ZIP_NAME}.zip_${time}"
sudo rm -rf "${ZIP_URL}"
cp "/data/usr/workspace/${ZIP_NAME}.zip" "${BASE_URL}"
unzip "${ZIP_URL}" -d "${ABS_INSTALL_TO}"
sudo rm -rf "/data/usr/workspace/${ZIP_NAME}.zip"

cd "${ABS_INSTALL_TO}"
chmod -R 755 ./scripts/`whoami`/docker-start.sh
./scripts/`whoami`/docker-start.sh