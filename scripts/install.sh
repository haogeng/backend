#!/usr/bin/env bash

source "$(dirname "${0}")/shlib.sh"

echo "==> ${term_yellow}Install to ... ${term_reset}"

# Get the parent directory of where this script is.
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ] ; do SOURCE="$(readlink "$SOURCE")"; done

DIR_CURR="$( cd -P "$( dirname "$SOURCE" )/" && pwd )"
DIR_ROOT="$( cd -P "${DIR_CURR}/../" && pwd )"
if [ "${ABS_INSTALL_TO}x" = "x" ]; then
    if [ "${DIR_INSTALL}x" = "x" ]; then
        DIR_INSTALL=${ROOT_NAME}
    fi
    ABS_INSTALL_TO="/tmp/deploy/${DIR_INSTALL}"
fi

# Create out path if it's doesn't exists
if [ -d ${ABS_INSTALL_TO} ]; then
    if [ "${ABS_INSTALL_TO}" == "*" ];then
      echo "？？？"
    else
      echo "==> Removing old directory:$ABS_INSTALL_TO"
      rm -rf ${ABS_INSTALL_TO}
    fi
fi

echo "==> Creating directory: ${ABS_INSTALL_TO}"
mkdir -p ${ABS_INSTALL_TO}

# Copy our OS/Arch to the bin/ directory
for F in $(find "${DIR_ROOT}/cmd" -mindepth 1 -maxdepth 1 -type d); do
    binDir="$F"/bin
    if [ -d ${binDir} ]; then
        for tmp in $(find ${binDir} -mindepth 1 -maxdepth 1 -type f); do
            echo "==> copy ${tmp} to ${ABS_INSTALL_TO}"
            cp -f ${tmp} ${ABS_INSTALL_TO}
        done
    fi
done


# Copy our depends to the bin/ directory
dirs=("init" "configs" "pkg/gen/conf/rawdata")

cd ${DIR_ROOT}
for dir in ${dirs[@]};do
    if [ -d ${dir} ]; then
        dst="${ABS_INSTALL_TO}/${dir}"
        mkdir -p "${dst}"
        echo "==> Copy dir ${dir} to ${dst}/"
        cp -rf ${dir}/* ${dst}
    fi
done
