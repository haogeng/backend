#!/bin/bash

# This script builds the application from source for multiple platforms.
source "$(dirname "${0}")/shlib.sh"
echo "==> ${term_yellow}Building... ${term_reset}"

if [ "${APP}x" = "x" ]; then
    echo  "${term_red}==> cmd/{APP}/main.go${term_reset}"
    echo  "${term_red}==> APP not defined, perhaps you should call 'make bin' or 'make vendorbin' under build dir${term_reset}"
    exit 1
fi

# git config for bitbucket
git config --global url."git@bitbucket.org:".insteadOf "https://bitbucket.org/"

# Get the parent directory of where this script is.
SOURCE="${BASH_SOURCE[0]}"

while [ -h "$SOURCE" ] ; do SOURCE="$(readlink "$SOURCE")"; done

DIR="$( cd -P "$( dirname "$SOURCE" )/../cmd/${APP}" && pwd )"

# Change into that directory
cd "$DIR"

# Get the git commit
GIT_COMMIT=$(git rev-parse HEAD)
GIT_DIRTY=$(test -n "`git status --porcelain`" && echo "+CHANGES" || true)

# Determine the arch/os combos we're building for
#ARR_OS_ARCH=("linux/amd64" "darwin/amd64")
ARR_OS_ARCH=("linux/amd64")
# 改配置的时候需要改这里: cgo 交叉编译特殊配置
# mac: brew install FiloSottile/musl-cross/musl-cross
CC_SETTING="x86_64-linux-musl-gcc"

# Delete the old dir
echo "==> Removing old directory..."
rm -f bin/*
mkdir -p bin/

# If its dev mode, only build for local
# -n == no zero
if [[ -n "${BIN_DEV}" ]]; then
    ARR_OS_ARCH=("$(go env GOOS)/$(go env GOARCH)")
fi
# In release mode we don't want debug information in the binary
if [[ -n "${BIN_RELEASE}" ]]; then
    LD_FLAGS="-s -w"
fi

flag_vendor="-mod=vendor"
# if MOD_VENDOR is nil
if [ "${MOD_VENDOR}x" = "x" ]; then
    # Ensure all remote modules are downloaded and cached before build
    # 此处没有用vendor，这个注释有问题..
    echo "==> go mod downloading to vendor..."
    go mod download
    flag_vendor=""
fi

# Build!
echo "==> Building..."

# version=$( git rev-parse --short HEAD 2> /dev/null || echo 'unknown' )
VERSION=$( git describe --tags 2> /dev/null || echo 'unknown' )
BRANCH=$( git rev-parse --abbrev-ref HEAD 2> /dev/null || echo 'unknown' )
BUILD_DATE=$( date +.%Y%m%d.%H%M%S )
SVNVERSION=$(cd "$DIR"/../../../conf/ && svn info | grep '^Revision')
cd "$DIR"
SVNURL=$(cd "$DIR"/../../../conf/ && svn info | grep '^URL')
cd "$DIR"
version="-X 'server/pkg/version.Version=$VERSION'"
version="$version -X 'server/pkg/version.Branch=$BRANCH'"
version="$version -X 'server/pkg/version.SvnVersion=$SVNVERSION'"
version="$version -X 'server/pkg/version.SvnUrl=$SVNURL'"
echo $version

basename=${BASE_NAME}
for OS_ARCH in ${ARR_OS_ARCH[@]};do
    IFS=/ read OS ARCH <<< "${OS_ARCH}"
    if [[ -n "${ROOT_NAME}" ]]; then
        output="${ROOT_NAME}_${APP}"
        if [[ -n "${ENV_NAME}" ]]; then
            output="${ROOT_NAME}_${APP}_${ENV_NAME}"
        fi
        if [[ "${OS}" == "darwin" ]]; then 
            output=$output"_darwin"
        fi 
    else
        if [[ "${OS}" == "linux" ]]; then
            output="${APP}"
            if [[ -n "${ENV_NAME}" ]]; then
                output="${ROOT_NAME}_${APP}_${ENV_NAME}"
            fi
        elif [[ -n "${BASE_NAME}" ]]; then
            output="${BASE_NAME}_${OS}_${ARCH}"
        else
            output="${PWD##*/}_${OS}_${ARCH}"
        fi
    fi
    echo "==> Build for ${OS_ARCH} with bin name: ${output}"
    echo "${version}"
    # musl for docker
    CC="${CC_SETTING}" GOOS="${OS}" GOARCH="${ARCH}" CGO_ENABLED=1 \
    go build ${flag_vendor} -o ./bin/"${output}" -tags queue -ldflags="${version}" .
done


