#!/usr/bin/env bash
ENV_NAME=''
source "$(dirname "${0}")/shlib.sh"
source "$(dirname "${0}")/../env"
if [ "x"$ENV_NAME != "x" ]
then
    env=_$ENV_NAME
elif [ $# -gt 0 ]
then
    env=_$1
fi
echo "==> ${term_yellow}Init Conf ... ${term_reset}"
# Get the parent directory of where this script is.
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ] ; do SOURCE="$(readlink "$SOURCE")"; done
mkdir -p $APP_PATH/log
DIR_CURR="$( cd -P "$( dirname "$SOURCE" )/" && pwd )"
DIR_ROOT="$( cd -P "${DIR_CURR}/../" && pwd )"
for dir in $DIR_ROOT/cmd/*/; do
    app_name=`basename $dir`
    full_name=$app_name
    if [ "x"$ROOT_NAME != "x" ]
    then
        full_name=${ROOT_NAME}_${app_name}
    fi
    bin_name=$full_name
    if [ "x"$ENV_NAME != "x" ]
    then
        bin_name=${full_name}_${ENV_NAME}
    fi
    echo "[program:$app_name]
command=$APP_PATH/$bin_name --_auto_conf_files_=$APP_PATH/configs/${app_name}${env}.toml
directory=$APP_PATH/
stdout_logfile=$APP_PATH/log/${full_name}${env}.log
stderr_logfile=$APP_PATH/log/${full_name}${env}_err.log
autorestart=true
redirect_stderr=false
stopsignal=QUIT
startretries=3000
environment=HOME=/root" > /tmp/deploy/${ROOT_NAME}/init/sv_${full_name}${env}.conf
done