TEST?=./...
ifeq (.env, $(wildcard .env))
	include .env
endif
include env
GOFMT_FILES?=$$(find . -name '*.go' | grep -v vendor)
APPS?=$(shell ls -d cmd/*/ | sed "s@cmd/@@g;s@/@@g")
# release只编译服务器
APPSX="server"
default: test

tools:
	GO111MODULE=off go get -u golang.org/x/tools/cmd/cover
	GO111MODULE=off go get -u golang.org/x/tools/cmd/goimports
	GO111MODULE=off go get -u github.com/alecthomas/gometalinter

# bin generates the releaseable binaries
bin: fmt
	$(foreach APP,$(APPSX),BIN_RELEASE=1 APP=$(APP) ENV_NAME=$(ENV_NAME) ROOT_NAME=$(ROOT_NAME) sh -c "'$(CURDIR)/scripts/build.sh'";)

vendorbin:
	$(foreach APP,$(APPS),MOD_VENDOR=1 BIN_RELEASE=1 APP=$(APP) ENV_NAME=$(ENV_NAME) ROOT_NAME=$(ROOT_NAME) sh -c "'$(CURDIR)/scripts/build.sh'";)

# dev creates binaries for testing locally.
dev: fmtcheck
	$(foreach APP,$(APPS),go install -tags queue ./cmd/$(APP);)

# creates binaries which name has arch as post for testing locally. (yy added)
local: fmt
	$(foreach APP,$(APPS),go build -o local_$(APP)_"$$(go env GOOS)" -tags queue ./cmd/$(APP);)

vendordev: fmtcheck
	$(foreach APP,$(APPS),go install -mod=vendor -tags queue ./cmd/$(APP);)

fmt:
	gofmt -w $(GOFMT_FILES)

fmtcheck:
	@sh -c "'$(CURDIR)/scripts/gofmtcheck.sh'"

# test runs the unit tests
test: fmtcheck
	go list -mod=vendor $(TEST) | xargs -t -n4 go test $(TESTARGS) -mod=vendor -timeout=2m -parallel=4

testrace: fmtcheck
	go test -mod=vendor -race $(TEST) $(TESTARGS)

install:
	@ROOT_NAME=$(ROOT_NAME) sh -c "'$(CURDIR)/scripts/install.sh'"

pro:
	ROOT_NAME=$(ROOT_NAME) SERVER_NAME=$(SERVER_NAME) USER_NAME=pro sh -c "'$(CURDIR)/scripts/docker-build-pro.sh'"
dev3:
	ROOT_NAME=$(ROOT_NAME) ENV_NAME=$(ENV_NAME) SERVER_NAME=$(SERVER_NAME) USER_NAME=dev sh -c "'$(CURDIR)/scripts/docker-build.sh'"
qa7:
	ROOT_NAME=$(ROOT_NAME) ENV_NAME=$(ENV_NAME) SERVER_NAME=$(SERVER_NAME) USER_NAME=qa sh -c "'$(CURDIR)/scripts/docker-build.sh'"
xbase9:
	ROOT_NAME=$(ROOT_NAME) ENV_NAME=$(ENV_NAME) SERVER_NAME=$(SERVER_NAME) USER_NAME=xbase sh -c "'$(CURDIR)/scripts/docker-build.sh'"
geng5:
	ROOT_NAME=$(ROOT_NAME) ENV_NAME=$(ENV_NAME) SERVER_NAME=$(SERVER_NAME) USER_NAME=geng sh -c "'$(CURDIR)/scripts/docker-build.sh'"
lnj6:
	ROOT_NAME=$(ROOT_NAME) ENV_NAME=$(ENV_NAME) SERVER_NAME=$(SERVER_NAME) USER_NAME=lnj sh -c "'$(CURDIR)/scripts/docker-build.sh'"
daily1:
	ROOT_NAME=$(ROOT_NAME) ENV_NAME=$(ENV_NAME) SERVER_NAME=$(SERVER_NAME) USER_NAME=daily sh -c "'$(CURDIR)/scripts/docker-build.sh'"

prodeploy: bin install clean pro
dev3deploy: bin install clean dev3
qa7deploy: bin install clean qa7
xbase9deploy: bin install clean xbase9
geng5deploy: bin install clean geng5
lnj6deploy: bin install clean lnj6
daily1deploy: bin install clean daily1

deploy: bin install clean

vendordeploy: vendorbin install clean

clean:
	@sh -c "'$(CURDIR)/scripts/clean.sh'"

cover:
	@go tool cover 2>/dev/null; if [ $$? -eq 3 ]; then \
		go get -u golang.org/x/tools/cmd/cover; \
	fi
	go test $(TEST) -coverprofile=coverage.out
	go tool cover -html=coverage.out
	rm coverage.out

protobuf:
	@echo "Not implement yet."

generate: tools
	GOFLAGS=-mod=vendor go generate ./...

.NOTPARALLEL:

.PHONY: deploy install cover protobuf clean bin vendorbin dev vendordev fmt fmtcheck generate  test tools testrace docker
